package sysEntityColumn;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.output.SysEntityColumnTO;
import com.talentpact.business.application.transport.output.SysEntityResponseTO;
import com.talentpact.business.application.transport.output.SysEntityTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.sysEntity.ISysEntityRemote;
import com.talentpact.remote.sysEntityColumn.ISysEntityColumnRemote;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysEntity.constants.ISysEntityConstants;
import com.talentpact.ui.sysEntityColumn.common.transport.input.SysEntityColumnRequestTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.constants.ISysEntityColumnConstants;

public class SysEntityColumnController extends CommonController implements Serializable, IDefaultAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(SysEntityColumnController.class.toString());

	@Inject
	UserSessionBean userSessionBean;
	
	@EJB(beanName = "SysEntityColumnFacade")
	ISysEntityColumnRemote iSysEntityColumnRemote;
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
	
	
	/**
	 * adding SysEntityColumn
	 * @param sysEntityColumnTO
	 * @author raghvendra.mishra
	 */
	public void addSysEntityColumn(SysEntityColumnTO sysEntityColumnTO) {
        ServiceRequest serviceRequest = null;
        SysEntityColumnResponseTO sysEntityColumnResponseTO = null;
        ServiceResponse serviceResponse = null;
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
        try {
            serviceRequest = getServiceRequest(sysEntityColumnTO);
            serviceResponse = iSysEntityColumnRemote.saveOrUpdateSysEntityColumn(serviceRequest);
            sysEntityColumnResponseTO = (SysEntityColumnResponseTO) serviceResponse.getResponseTransferObject();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysEntityConstants.SYSENTITY_SAVE_ERROR_MSG, ""));
            LOGGER.error("", e);
        }
    }
	
	public ServiceRequest getServiceRequest(SysEntityColumnTO sysEntityColumnTO) {
		ServiceRequest serviceRequest = null;
		SysEntityColumnRequestTO sysEntityColumnRequestTO = null;
		serviceRequest = new ServiceRequest();
        sysEntityColumnRequestTO = new SysEntityColumnRequestTO();
        if (sysEntityColumnRequestTO.getEntityColumnID() == null) {
        	sysEntityColumnRequestTO.setEntityColumnID(null);
        }else{
        	sysEntityColumnRequestTO.setEntityColumnID(sysEntityColumnTO.getEntityColumnID());
        }
        sysEntityColumnRequestTO.setEntityColumnName(sysEntityColumnTO.getEntityColumnName());;
        sysEntityColumnRequestTO.setDescription(sysEntityColumnTO.getDescription());
        sysEntityColumnRequestTO.setDbColumnName(sysEntityColumnTO.getDbColumnName());
        sysEntityColumnRequestTO.setLabel(sysEntityColumnTO.getLabel());
        sysEntityColumnRequestTO.setTenantID(sysEntityColumnTO.getTenantID());
        sysEntityColumnRequestTO.setCreatedBy((userSessionBean.getUserID()).intValue());
        sysEntityColumnRequestTO.setModifiedBy((userSessionBean.getUserID()).intValue());
        sysEntityColumnRequestTO.setEntityColumnType(sysEntityColumnTO.getEntityColumnType());
        sysEntityColumnRequestTO.setDbColumnType(sysEntityColumnTO.getDbColumnType());
        sysEntityColumnRequestTO.setSysEntityId(sysEntityColumnTO.getSysEntityId());       
        serviceRequest.setRequestTransferObjectRevised(sysEntityColumnRequestTO);
        return serviceRequest;
    }

}
