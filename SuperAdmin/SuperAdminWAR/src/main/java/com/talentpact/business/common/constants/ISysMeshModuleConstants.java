package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysMeshModuleConstants
{

    public static String SYSMESHMODULE_ADD_DIALOG_HIDE = "PF('sysMeshModuleModal').hide();";

    public static String SYSMESHMODULE_EDIT_DIALOG_HIDE = "PF('sysMeshModuleEditModal').hide();";

    public static String SYSMESHMODULE_SAVE_ERROR_MSG = "Error occured while saving SysMeshModule.";

    public static String SYSMESHMODULE_UPDATE_ERROR_MSG = "Error occured while updating SysMeshModule.";

    public static String SYSMESHMODULE_SAVE_SUCCESS_MSG = "SysMeshModule saved successfully.";

    public static String SYSMESHMODULE_UPDATE_SUCCESS_MSG = "SysMeshModule updated successfully.";

    public static String SYSMESHMODULE_DATATABLE_RESET = "PF('sysMeshModuleDataTable').clearFilters()";
    																
    public static String SYSMESHMODULE_PAGINATION_DATATABLE_RESET = "PF('sysMeshModuleDataTable').getPaginator().setPage(0);";

    public static String SYSMESHMODULE_DUPLICATE_ERROR_MSG = "SysMeshModule already present in system.";
}
