/**
 * 
 */
package com.talentpact.business.dataservice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.admin.model.dataupload.SysUploadTable;
import com.admin.model.dataupload.SysUploadTableColumn;
import com.admin.model.dataupload.SysUploadTemplateType;
import com.alt.common.helper.ValidatorUtil;
import com.alt.common.to.SelectItemTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysUploadAttribute;
import com.talentpact.model.SysValidationType;
import com.talentpact.ui.dataupload.to.SysUploadTemplateTypeTO;
import com.talentpact.ui.dataupload.to.TableColumnTO;
import com.talentpact.ui.dataupload.to.ValidationTypeTO;

/**
 * @author radhamadhab.dalai
 * 
 */
@Stateless
public class DataUploadService extends CommonService
{

    @EJB
    ValidationTypeDS validationTypeDS;

    @EJB
    SysUploadTableDS uploadTableDS;

    @EJB
    DataUploadDS dataUploadDS;

    public List<SelectItemTO> getSysUploadTemplateTypeDropDown()
        throws Exception
    {
        List<SelectItemTO> list = null;
        SelectItemTO SelectItemTO = null;
        List<SysUploadTemplateType> sysUploadTemplateTypeList = null;
        try {
            sysUploadTemplateTypeList = dataUploadDS.getSysUploadTemplateTypeList();
            list = new ArrayList<SelectItemTO>();
            for (SysUploadTemplateType sysUploadTemplateType : sysUploadTemplateTypeList) {
                SelectItemTO = new SelectItemTO();
                SelectItemTO.setItemID(sysUploadTemplateType.getTemplateTypeId());
                SelectItemTO.setItemlabel(sysUploadTemplateType.getTemplateTypeName());
                list.add(SelectItemTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SysUploadTemplateTypeTO> getSysUploadTemplateTypeList()
        throws Exception
    {
        List<SysUploadTemplateTypeTO> list = null;
        SysUploadTemplateTypeTO sysUploadTemplateTypeTO = null;
        List<SysUploadTemplateType> sysUploadTemplateTypeList = null;
        try {
            sysUploadTemplateTypeList = dataUploadDS.getSysUploadTemplateTypeList();
            list = new ArrayList<SysUploadTemplateTypeTO>();
            for (SysUploadTemplateType sysUploadTemplateType : sysUploadTemplateTypeList) {
                sysUploadTemplateTypeTO = new SysUploadTemplateTypeTO();
                sysUploadTemplateTypeTO.setTemplateTypeID(sysUploadTemplateType.getTemplateTypeId());
                sysUploadTemplateTypeTO.setTemplateTypeName(sysUploadTemplateType.getTemplateTypeName());
                sysUploadTemplateTypeTO.setUploadTableID(sysUploadTemplateType.getUploadTableID());
                SysUploadTable sysUploadTable = sysUploadTemplateType.getSysUploadTable();
                sysUploadTemplateTypeTO.setUploadTableName(sysUploadTable.getTableName());
                sysUploadTemplateTypeTO.setPersistanceUnitName(sysUploadTable.getPersistenceUnitName());
                sysUploadTemplateTypeTO.setDatabaseName(sysUploadTable.getPersistenceUnitName());
                list.add(sysUploadTemplateTypeTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SelectItemTO> getSysUploadTableList()
        throws Exception
    {
        List<SelectItemTO> list = null;
        SelectItemTO SelectItemTO = null;
        List<SysUploadTable> SysUploadTableList = null;
        try {
            SysUploadTableList = dataUploadDS.getSysUploadTableList();
            list = new ArrayList<SelectItemTO>();
            for (SysUploadTable sysUploadTable : SysUploadTableList) {
                SelectItemTO = new SelectItemTO();
                SelectItemTO.setItemID(sysUploadTable.getUploadTableID());
                SelectItemTO.setItemlabel(sysUploadTable.getTableName());
                list.add(SelectItemTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SelectItemTO> getSysUploadTableColumnList(Integer tableID)
        throws Exception
    {
        List<SelectItemTO> list = null;
        SelectItemTO SelectItemTO = null;
        List<SysUploadTableColumn> sysUploadTableColumnList = null;
        try {
            sysUploadTableColumnList = dataUploadDS.getSysUploadTableColumnList(tableID);
            list = new ArrayList<SelectItemTO>();
            for (SysUploadTableColumn sysUploadTableColumn : sysUploadTableColumnList) {
                SelectItemTO = new SelectItemTO();
                SelectItemTO.setItemID(sysUploadTableColumn.getUploadTableColumnID());
                SelectItemTO.setItemlabel(sysUploadTableColumn.getColumnName());
                list.add(SelectItemTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<TableColumnTO> getTableColumnTOList(Integer tableID)
        throws Exception
    {
        List<SysUploadTableColumn> list = null;
        List<TableColumnTO> tableColumnTOList = null;
        TableColumnTO tableColumnTO = null;
        try {
            list = dataUploadDS.getSysUploadTableColumnList(tableID);
            tableColumnTOList = new ArrayList<TableColumnTO>();
            for (SysUploadTableColumn sysUploadTableColumn : list) {
                tableColumnTO = new TableColumnTO();
                tableColumnTO.setUploadTableColumnID(sysUploadTableColumn.getUploadTableColumnID());
                tableColumnTO.setColumnName(sysUploadTableColumn.getColumnName());
                tableColumnTO.setUploadTableID(sysUploadTableColumn.getUploadTableID());
                tableColumnTO.setAttributeName(sysUploadTableColumn.getColumnName());
                tableColumnTO.setDataUploadColumnName(sysUploadTableColumn.getDataUploadColumnName());
                tableColumnTO.setDataType(sysUploadTableColumn.getDataType());
                if (sysUploadTableColumn.getValidationType() != null && !sysUploadTableColumn.getValidationType().isEmpty()) {
                    tableColumnTO.setValidationType(sysUploadTableColumn.getValidationType());
                    String[] str = sysUploadTableColumn.getValidationType().split(",");
                    Set<Integer> set = new HashSet<Integer>();
                    List<ValidationTypeTO> validationList = new ArrayList<ValidationTypeTO>();
                    for (String st : str) {
                        if (ValidatorUtil.isNumeric(st)) {
                            set.add(Integer.parseInt(st));

                            ValidationTypeTO to = new ValidationTypeTO();
                            to.setValidationTypeID(Integer.parseInt(st));
                            validationList.add(to);
                        }
                    }
                    tableColumnTO.setValidationList(validationList);
                    tableColumnTO.setValidationSet(set);
                }
                tableColumnTOList.add(tableColumnTO);
            }
            return tableColumnTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    public void saveTemplateType(SysUploadTemplateTypeTO sysUploadTemplateTypeTO, List<TableColumnTO> tableColumnTOList)
        throws Exception
    {
        try {
            dataUploadDS.saveTemplateType(sysUploadTemplateTypeTO, tableColumnTOList);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<String> getTableNames(String persistenceUnitName)
        throws Exception
    {
        try {
            List<String> listTable = dataUploadDS.getTableNames(persistenceUnitName);
            return listTable;
        } catch (Exception e) {
            throw e;
        }
    }

    public String insertTableAndColumn(String persistenceUnitName, Integer tableId, String tableName, List<TableColumnTO> columnList)
        throws Exception
    {
        try {
            return dataUploadDS.insertTableAndColumn(persistenceUnitName, tableId, tableName, columnList);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<TableColumnTO> getTemplateTypeDetail(SysUploadTemplateTypeTO sysUploadTemplateTypeTO)
        throws Exception
    {
        List<TableColumnTO> tableColumnTOList = null;
        List<SysUploadAttribute> sysUploadAttributeList = null;
        SysUploadAttribute sysUploadAttribute = null;
        try {
            tableColumnTOList = getTableColumnTOList(sysUploadTemplateTypeTO.getUploadTableID());
            sysUploadAttributeList = dataUploadDS.getSysUploadTemplateAttributes(sysUploadTemplateTypeTO.getTemplateTypeID());
            for (TableColumnTO tableColumnTO : tableColumnTOList) {
                for (SysUploadAttribute sysUploadAttributetemp : sysUploadAttributeList) {
                    if (tableColumnTO.getUploadTableColumnID().equals(sysUploadAttributetemp.getUploadTableColumnID())) {
                        sysUploadAttribute = sysUploadAttributetemp;
                        break;
                    }
                }
                if (sysUploadAttribute != null) {
                    tableColumnTO.setTemplateTypeID(sysUploadTemplateTypeTO.getTemplateTypeID());
                    tableColumnTO.setTemplateTypeName(sysUploadTemplateTypeTO.getTemplateTypeName());
                    tableColumnTO.setAttributeID(sysUploadAttribute.getAttributeId());
                    tableColumnTO.setAttributeName(sysUploadAttribute.getAttributeName());
                }
            }
            return tableColumnTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<TableColumnTO> getTableColumnsNames(String persistenceUnitName, String tableName)
        throws Exception
    {
        try {
            return dataUploadDS.getTableColumnsNames(persistenceUnitName, tableName);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<ValidationTypeTO> getSysValidationTypeList(boolean groupValidation)
        throws Exception
    {
        List<ValidationTypeTO> returnList = null;
        List<SysValidationType> list = validationTypeDS.getSysValidationTypeList(groupValidation);
        if (list != null && !list.isEmpty()) {
            returnList = new ArrayList<ValidationTypeTO>();
            for (SysValidationType entity : list) {
                ValidationTypeTO to = new ValidationTypeTO();
                to.setValidationTypeID(entity.getValidationTypeId());
                to.setValidationType(entity.getValidationType());
                to.setValidationMessage(entity.getValidationMessage());
                to.setLabel(entity.getLabel());
                to.setValidationExpression(entity.getValidationExpression());
                returnList.add(to);
            }
        }
        return returnList;
    }


    public boolean saveTableColumn(String persistenceUnitName, String tableName, List<TableColumnTO> columnLst)
    {
        return dataUploadDS.saveTableColumn(persistenceUnitName, tableName, columnLst);
    }

    public List<TableColumnTO> getExistingTableColumnsNames(String persistenceUnitName, String tableName)
        throws Exception
    {
        try {
            List<SysUploadTable> tableLst = uploadTableDS.getExistingTableColumnsNames(tableName);
            if (tableLst != null && !tableLst.isEmpty()) {
                SysUploadTable table = tableLst.get(0);
                return getTableColumnTOList(table.getUploadTableID());
            }
        } catch (Exception e) {
            throw e;
        }
        return null;
    }


}
