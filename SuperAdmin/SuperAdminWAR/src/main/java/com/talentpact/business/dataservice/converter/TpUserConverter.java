/**
 * 
 */
package com.talentpact.business.dataservice.converter;

import com.talentpact.model.SysUser;
import com.talentpact.ui.survey.user.common.transport.output.TpUserCreatedResponseTO;

/**
 * @author vaibhav.kashyap
 * 
 */
public class TpUserConverter {

	public static TpUserCreatedResponseTO getCreatedUserdetails(SysUser sysUser)
			throws Exception {

		TpUserCreatedResponseTO tpUserCreatedResponseTO = null;
		try {
			if (sysUser != null) {
				tpUserCreatedResponseTO = new TpUserCreatedResponseTO();
				tpUserCreatedResponseTO.setAdmin(true);
				tpUserCreatedResponseTO.setOrganizationID(new Long(sysUser
						.getHrOrganizationId()));
				tpUserCreatedResponseTO.setSurveyUserID(new Long(sysUser
						.getUserId()));
				tpUserCreatedResponseTO.setTenantID(new Long(sysUser
						.getSysTenantId()));
				tpUserCreatedResponseTO.setUserName(sysUser.getUserName());
			}

		} catch (Exception ex) {
			throw ex;
		}
		
		return tpUserCreatedResponseTO;
	}
}
