/**
 * 
 */
package com.talentpact.business.service.passwordpolicy;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.common.constants.CoreConstants;
import com.alt.passwordPolicy.constants.IPasswordPolicyConstant;
import com.alt.passwordPolicy.constants.PasswordType;
import com.alt.passwordPolicy.to.PasswordPolicyTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.passwordpolicy.SysPasswordPolicyDS;
import com.talentpact.business.dataservice.passwordpolicy.cache.PasswordPolicyCache;
import com.talentpact.business.dataservice.passwordpolicy.cache.PasswordPolicyKeyConstants;
import com.talentpact.model.passwordPolicy.SysPasswordPolicy;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class SysPasswordPolicyService extends CommonService
{
    @EJB
    SysPasswordPolicyDS sysPasswordPolicyDS;

    @EJB
    PasswordPolicyCache passwordPolicyCache;

    /**
     * 
     * @return
     * @throws Exception
     */
    public PasswordPolicyTO getPasswordPolicySetting()
        throws Exception
    {
        List<SysPasswordPolicy> passwordPoliciesList = null;
        PasswordPolicyTO passwordPolicyTO = null;
        Map<String, Boolean> passwordTypeMap = null;
        String[] passwordTypes = null;
        int i = 0;
        List<String>passwordPolicyList=null;
        try {
            passwordPolicyList=new ArrayList<String>();  try {
                passwordPolicyTO = passwordPolicyCache.getPasswordPolicySettings();
                if (passwordPolicyTO == null) {
                    throw new Exception(PasswordPolicyKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    passwordPoliciesList = sysPasswordPolicyDS.getPasswordPolicySetting();
                    if (passwordPoliciesList != null && !passwordPoliciesList.isEmpty()) {
                        passwordPolicyTO = new PasswordPolicyTO();
                        passwordPolicyTO.setEdit(true);
                        passwordTypes = new String[PasswordType.values().length];
                        passwordTypeMap = new LinkedHashMap<String, Boolean>();
                        for (SysPasswordPolicy sysPasswordPolicy : passwordPoliciesList) {
                            if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.BLOCKING_DURATION_STRING)) {
                                passwordPolicyTO.setBlockingDuration(sysPasswordPolicy.getValue());
                            } else
                            if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.MINI_CHARACTERS_STRING)) {
                                passwordPolicyTO.setMinimumCharacters(sysPasswordPolicy.getValue());
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.CAPCHA_AFTER_INVALID_ATTEMP__STRING)) {
                                passwordPolicyTO.setShowCapchaAfter(sysPasswordPolicy.getValue());
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.RESET_PASSWORD_ON_FIRST_LOGIN_STRING)) {
                                passwordPolicyTO.setResetPasswordOnFirstLogin(new Boolean(sysPasswordPolicy.getValue()));
                            }

                            else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.FORCE_PASSWORD_CHANGE__STRING)) {
                                passwordPolicyTO.setForcePasswordChangeFrequency(sysPasswordPolicy.getValue());
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.PASSWORD_ATTEMP__STRING)) {
                                passwordPolicyTO.setNoOfAttempts(sysPasswordPolicy.getValue());
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.PASSWORD_REPEAT__STRING)) {
                                passwordPolicyTO.setAllowRepeatOldPasswordAfter(sysPasswordPolicy.getValue());
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(IPasswordPolicyConstant.PASSWORD_TYPES__STRING)) {
                                passwordTypes[i++] = sysPasswordPolicy.getValue();
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(
                                    IPasswordPolicyConstant.PASSWORD_TYPES__STRING + CoreConstants.COLON_SPERATOR + PasswordType.LOWERCASE.getPasswordType())) {
                                passwordTypeMap.put(PasswordType.LOWERCASE.getPasswordType(), sysPasswordPolicy.getValue().equalsIgnoreCase(IPasswordPolicyConstant.MANDATORY__STRING) ? true
                                        : false);
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(
                                    IPasswordPolicyConstant.PASSWORD_TYPES__STRING + CoreConstants.COLON_SPERATOR + PasswordType.UPPERCASE.getPasswordType())) {
                                passwordTypeMap.put(PasswordType.UPPERCASE.getPasswordType(), sysPasswordPolicy.getValue().equalsIgnoreCase(IPasswordPolicyConstant.MANDATORY__STRING) ? true
                                        : false);
                            } else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(
                                    IPasswordPolicyConstant.PASSWORD_TYPES__STRING + CoreConstants.COLON_SPERATOR + PasswordType.SPECIALCHAR.getPasswordType())) {
                                passwordTypeMap.put(PasswordType.SPECIALCHAR.getPasswordType(), sysPasswordPolicy.getValue().equalsIgnoreCase(IPasswordPolicyConstant.MANDATORY__STRING) ? true
                                        : false);
                            }

                            else if (sysPasswordPolicy.getParameterName().equalsIgnoreCase(
                                    IPasswordPolicyConstant.PASSWORD_TYPES__STRING + CoreConstants.COLON_SPERATOR + PasswordType.NUMBERS.getPasswordType())) {
                                passwordTypeMap.put(PasswordType.NUMBERS.getPasswordType(), sysPasswordPolicy.getValue().equalsIgnoreCase(IPasswordPolicyConstant.MANDATORY__STRING )? true
                                        : false);
                            }


                        }
                        passwordPolicyTO.setPasswordTypeMap(passwordTypeMap);
                        passwordPolicyTO.setPasswordTypes(passwordTypes);
                        
                        passwordPolicyList.add(IPasswordPolicyConstant.MINIMUMCHARS_ONLY+passwordPolicyTO.getMinimumCharacters()+" characters long");
                        passwordTypeMap = passwordPolicyTO.getPasswordTypeMap();
                        if (passwordTypeMap != null) {
                            if (passwordTypeMap.containsKey(PasswordType.UPPERCASE.getPasswordType())
                                    && passwordTypeMap.containsKey(PasswordType.LOWERCASE.getPasswordType())) {
                                if (passwordTypeMap.get(PasswordType.UPPERCASE.getPasswordType()) && passwordTypeMap.get(PasswordType.LOWERCASE.getPasswordType())) {
                                    passwordPolicyList.add(IPasswordPolicyConstant.UPPER__LOWER_CHARS_BOTH);
                                } else {
                                    if (passwordTypeMap.containsKey(PasswordType.LOWERCASE.getPasswordType())) {
                                        if (passwordTypeMap.get(PasswordType.LOWERCASE.getPasswordType())) {
                                            passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS_ONLY);
                                        }else{
                                            passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS);
                                            
                                        }
                                        
                                    }else{
                                        passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS_NOT);
                                        
                                    }
                                    if (passwordTypeMap.containsKey(PasswordType.UPPERCASE.getPasswordType())) {
                                        if (passwordTypeMap.get(PasswordType.UPPERCASE.getPasswordType())) {
                                            passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS_NOT);
                                        }else{
                                            passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS);
                                            
                                        }
                                    }else{
                                        passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS_NOT);
                                        
                                    }

                                }
                            } else {
                                if (passwordTypeMap.containsKey(PasswordType.LOWERCASE.getPasswordType())) {
                                    if (passwordTypeMap.get(PasswordType.LOWERCASE.getPasswordType())) {
                                        passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS_ONLY);
                                    }else{
                                        passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS);
                                        
                                    }
                                }else{
                                    passwordPolicyList.add(IPasswordPolicyConstant.LOWER_CHARS_NOT);
                                    
                                }
                                if (passwordTypeMap.containsKey(PasswordType.UPPERCASE.getPasswordType())) {
                                    if (passwordTypeMap.get(PasswordType.UPPERCASE.getPasswordType())) {
                                        passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS_ONLY);
                                    }else{
                                        passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS);
                                        
                                    }
                                }else{
                                    passwordPolicyList.add(IPasswordPolicyConstant.UPPER_CHARS_NOT);
                                    
                                }
                            }
                            if (passwordTypeMap.containsKey(PasswordType.NUMBERS.getPasswordType())) {
                                if (passwordTypeMap.get(PasswordType.NUMBERS.getPasswordType())) {
                                    passwordPolicyList.add(IPasswordPolicyConstant.NUMBERS_ONLY);
                                }else{
                                    passwordPolicyList.add(IPasswordPolicyConstant.NUMBERS);
                                    
                                }
                            }else{
                                passwordPolicyList.add(IPasswordPolicyConstant.NUMBERS_NOT);
                                
                            }
                            if (passwordTypeMap.containsKey(PasswordType.SPECIALCHAR.getPasswordType())) {
                                if (passwordTypeMap.get(PasswordType.SPECIALCHAR.getPasswordType())) {
                                    passwordPolicyList.add(IPasswordPolicyConstant.SPECIAL_CHARS_ONLY);
                                }else{
                                    passwordPolicyList.add(IPasswordPolicyConstant.SPECIAL_CHARS);
                                    
                                }
                            }else{
                                passwordPolicyList.add(IPasswordPolicyConstant.SPECIAL_CHARS_NOT);
                                
                            }
                        }
                        passwordPolicyTO.setPasswordPolicyList(passwordPolicyList);
                        passwordPolicyCache.putPasswordPolicySettings(passwordPolicyTO);
                    }
                } catch (Exception e) {
                    throw e;
                }
            }

            return passwordPolicyTO;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
    * 
    * @param passwordPolicyTO
    */
    public void savePasswordPolicySetting(PasswordPolicyTO passwordPolicyTO, Long userID) throws Exception
    {
        SysPasswordPolicy sysPasswordPolicy = null;
        List<SysPasswordPolicy> passwordPoliciesList = null;
        try {
            passwordPoliciesList = new ArrayList<>();
            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.MINI_CHARACTERS_STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getMinimumCharacters());
            passwordPoliciesList.add(sysPasswordPolicy);

            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.FORCE_PASSWORD_CHANGE__STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getForcePasswordChangeFrequency());
            passwordPoliciesList.add(sysPasswordPolicy);

            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.CAPCHA_AFTER_INVALID_ATTEMP__STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getShowCapchaAfter());
            passwordPoliciesList.add(sysPasswordPolicy);

            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.PASSWORD_ATTEMP__STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getNoOfAttempts());
            passwordPoliciesList.add(sysPasswordPolicy);

            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.BLOCKING_DURATION_STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getBlockingDuration());
            passwordPoliciesList.add(sysPasswordPolicy);

            
            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.PASSWORD_REPEAT__STRING);
            sysPasswordPolicy.setValue(passwordPolicyTO.getAllowRepeatOldPasswordAfter());
            passwordPoliciesList.add(sysPasswordPolicy);

            sysPasswordPolicy = new SysPasswordPolicy();
            sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.RESET_PASSWORD_ON_FIRST_LOGIN_STRING);
            sysPasswordPolicy.setValue(new Boolean(passwordPolicyTO.isResetPasswordOnFirstLogin()).toString());
            passwordPoliciesList.add(sysPasswordPolicy);


            if (passwordPolicyTO.getPasswordTypeMap() != null && !passwordPolicyTO.getPasswordTypeMap().isEmpty()) {
                for (Entry<String, Boolean> type : passwordPolicyTO.getPasswordTypeMap().entrySet()) {
                    sysPasswordPolicy = new SysPasswordPolicy();
                    sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.PASSWORD_TYPES__STRING);
                    sysPasswordPolicy.setValue(type.getKey());
                    passwordPoliciesList.add(sysPasswordPolicy);

                    sysPasswordPolicy = new SysPasswordPolicy();
                    sysPasswordPolicy.setParameterName(IPasswordPolicyConstant.PASSWORD_TYPES__STRING + CoreConstants.COLON_SPERATOR + type.getKey());
                    sysPasswordPolicy.setValue(type.getValue() ? IPasswordPolicyConstant.MANDATORY__STRING : IPasswordPolicyConstant.NON_MANDATORY__STRING);
                    passwordPoliciesList.add(sysPasswordPolicy);
                }

            }
            if(passwordPolicyTO.isEdit()){
            sysPasswordPolicyDS.deletePasswordPolicySetting();
            }
            sysPasswordPolicyDS.savePasswordPolicySetting(passwordPoliciesList, passwordPolicyTO.isEdit(), userID.intValue());
            passwordPolicyCache.deletePasswordPolicySettingsRedisKey();
        } catch (Exception e) {
            throw e;
        }
    }

}
