/**
 * 
 */
package com.talentpact.business.dataservice;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.talentpact.business.startup.StartupService;
import com.talentpact.model.HrModule;
import com.talentpact.ui.common.bean.UserSessionBean;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class FormSecurityDS extends AbstractDS<HrModule>
{
    @EJB
    StartupService startupService;

    @Inject
    UserSessionBean userSessionBean;

    public FormSecurityDS()
    {
        super(HrModule.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getOrganizationModuleData(Long orgId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> moduleList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select hm.moduleId, hm.moduleName from HrModule hm where hm.organizationId = :orgId ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("orgId", ((int) orgId.intValue()));
            moduleList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return moduleList;
    }
}