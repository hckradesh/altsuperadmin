package com.talentpact.business.common.constants;

/**
 * 
 * @author prachi.bansal
 *
 */

public interface ISysSchedulerConstants
{
    public static String SYSSCHEDULER_ADD_DIALOG_HIDE = "PF('sysSchedulerAddModal').hide();";

    public static String SYSSCHEDULER_EDIT_DIALOG_HIDE = "PF('sysSchedulerEditModal').hide();";

    public static String SYSSCHEDULER_SAVE_ERROR_MSG = "Error occured while saving SysScheduler.";

    public static String SYSSCHEDULER_UPDATE_ERROR_MSG = "Error occured while updating SysScheduler.";

    public static String SYSSCHEDULER_SAVE_SUCCESS_MSG = "SysScheduler saved successfully.";

    public static String SYSSCHEDULER_UPDATE_SUCCESS_MSG = "SysScheduler updated successfully.";

    public static String SYSSCHEDULER_DATATABLE_RESET = "PF('sysSchedulerDataTable').clearFilters();";

    public static String SYSSCHEDULER_PAGINATION_DATATABLE_RESET = "PF('sysSchedulerDataTable').getPaginator().setPage(0);";

    public static String SYSSCHEDULER_DUPLICATE_ERROR_MSG = "SysScheduler already present in system.";

}
