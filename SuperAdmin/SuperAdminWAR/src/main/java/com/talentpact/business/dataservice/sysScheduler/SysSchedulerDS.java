package com.talentpact.business.dataservice.sysScheduler;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysSchedulerType;
import com.talentpact.model.organization.HrOrganization;
import com.talentpact.model.organization.SysTenant;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysScheduler.to.SysSchedulerTO;

/**
 * @author prachi.bansal
 * 
 */

@Stateless
public class SysSchedulerDS extends AbstractDS<SysSchedulerType>
{   
    @Inject
    UserSessionBean userSessionBean;
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysSchedulerDS.class);

    public SysSchedulerDS() {
        super(SysSchedulerType.class);
      
    }

    @SuppressWarnings("unchecked")
    public List<SysSchedulerType> getSysSchedulerList() {
  
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysSchedulerType sysSchedulerType ");

            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public void saveSysScheduler(SysSchedulerTO sysSchedulerTO) {
        SysSchedulerType sysSchedulerType=null;
        SysTenant sysTenant=null;
        HrOrganization hrOrg=null;
        try{
            sysTenant = getEntityManager("TalentPact").find(SysTenant.class, 1);
            hrOrg=getEntityManager("TalentPact").find(HrOrganization.class, 1);
            sysSchedulerType=new SysSchedulerType();
            sysSchedulerType.setSchedulerName(sysSchedulerTO.getSysSchedulerName());
            sysSchedulerType.setSchedulerDescription(sysSchedulerTO.getSysSchedulerDescription());
            sysSchedulerType.setHrOrganization(hrOrg);
            sysSchedulerType.setSysTenant(sysTenant);
            getEntityManager("TalentPact").persist(sysSchedulerType);
          }catch (Exception e) {
              LOGGER_.error("", e);
          }
        
    }

    public void updateSysScheduler(SysSchedulerTO sysSchedulerTO) {
        SysSchedulerType sysSchedulerType=null;
        try{
            sysSchedulerType=new SysSchedulerType();
            sysSchedulerType=findSysScheduler(sysSchedulerTO.getSysSchedulerTypeID());
            if(sysSchedulerType.getSchedulerName().trim().equalsIgnoreCase(sysSchedulerTO.getSysSchedulerName().trim())
                    &&sysSchedulerType.getSchedulerDescription().trim().equalsIgnoreCase(sysSchedulerTO.getSysSchedulerDescription().trim())){
                
            }
            else{
                sysSchedulerType.setSchedulerName(sysSchedulerTO.getSysSchedulerName());
                sysSchedulerType.setSchedulerDescription(sysSchedulerTO.getSysSchedulerDescription());
                getEntityManager("TalentPact").merge(sysSchedulerType);
                getEntityManager("TalentPact").flush();
            }
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
        
    }

    private SysSchedulerType findSysScheduler(Integer sysSchedulerTypeID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysSchedulerType> list = null;
        SysSchedulerType sysSchedulerType = null;
        try{
            sysSchedulerType=new SysSchedulerType();
            hqlQuery=new StringBuilder();
            hqlQuery.append("from SysSchedulerType sst where sst.schedulerTypeID=:sysSchedulerTypeID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysSchedulerTypeID", sysSchedulerTypeID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
                sysSchedulerType = list.get(0);
            }
            return sysSchedulerType;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }

    @SuppressWarnings("unchecked")
    public List<SysSchedulerType> getUpdateSysSchedulerList(Integer sysSchedulerTypeID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysSchedulerType sst where sst.schedulerTypeID!=:sysSchedulerTypeID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysSchedulerTypeID", sysSchedulerTypeID);
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }
    }