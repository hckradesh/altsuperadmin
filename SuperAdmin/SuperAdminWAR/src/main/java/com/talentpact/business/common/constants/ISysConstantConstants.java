/**
 * 
 */
package com.talentpact.business.common.constants;

/**
 * @author pankaj.sharma1
 *
 */
public interface ISysConstantConstants
{
    public static String SYSCONSTANT_ADD_DIALOG_HIDE = "PF('sysConstantAddModal').hide();";

    public static String SYSCONSTANT_EDIT_DIALOG_HIDE = "PF('sysConstantEditModal').hide();";

    public static String SYSCONSTANT_SAVE_ERROR_MSG = "Error occured while saving SysConstant.";

    public static String SYSCONSTANT_UPDATE_ERROR_MSG = "Error occured while updating SysConstant.";

    public static String SYSCONSTANT_SAVE_SUCCESS_MSG = "SysConstant saved successfully.";

    public static String SYSCONSTANT_UPDATE_SUCCESS_MSG = "SysConstant updated successfully.";

    public static String SYSCONSTANT_DATATABLE_RESET = "PF('myDataTable').clearFilters()";

    public static String SYSCONSTANT_DUPLICATE_ERROR_MSG = "SysConstant already present in system.";

}
