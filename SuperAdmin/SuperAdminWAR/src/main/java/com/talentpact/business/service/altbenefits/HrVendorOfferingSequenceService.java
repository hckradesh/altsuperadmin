/**
 * 
 */
package com.talentpact.business.service.altbenefits;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altbenefits.to.BannerOfferingSequenceTO;
import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.HrOfferingTO;
import com.alt.altbenefits.to.HrVendorOfferingTO;
import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.altbenefits.transport.output.BenefitResponseTO;
import com.alt.common.constants.CoreConstants;
import com.alt.common.helper.CommonUtilHelper;
import com.alt.role.constants.IRoleConstants;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.HrEmployeeGroupOfferingSequenceDS;
import com.talentpact.business.dataservice.altbenefits.HrVendorOfferingSequenceDS;
import com.talentpact.business.dataservice.altbenefits.HrVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsKeyConstants;
import com.talentpact.business.dataservice.timeDimension.TimeDimensionDS;
import com.talentpact.model.altbenefit.HrEmployeeGroupOfferingSequence;
import com.talentpact.model.altbenefit.HrVendorOfferingSequence;
import com.talentpact.model.altbenefit.HrVendorOfferingTemplate;
import com.talentpact.model.altbenefit.OfferingCode;
import com.talentpact.model.altbenefit.SysBenefitsVendor;
import com.talentpact.model.altbenefit.SysOfferingCodeMaximumLimit;
import com.talentpact.model.altbenefit.SysVendorOfferingPeakDetail;
import com.talentpact.model.altbenefit.SysVendorOfferingTemplate;

/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class HrVendorOfferingSequenceService extends CommonService
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(HrVendorOfferingSequenceService.class);

    @EJB
    HrVendorOfferingTemplateDS hrOfferingTemplateDS;

    @EJB
    SysVendorOfferingTemplateDS offeringTemplateDS;

    @EJB
    TimeDimensionDS timeDimensionDS;

    @EJB
    AltBenefitsCache altBenefitsCache;

    @EJB
    HrEmployeeGroupOfferingSequenceDS hrEmployeeGroupOfferingSequenceDS;

    @EJB
    HrVendorOfferingSequenceDS hrVendorOfferingSequenceDS;

    /**
     * 
     * @return
     * @throws Exception
     */
    public BenefitResponseTO getBenefitsHrBasedList()
        throws Exception
    {
        BenefitResponseTO benefitResponseTO = null;
        try {
            try {
                benefitResponseTO = altBenefitsCache.getOrganizationWiseVendorOfferings();
                if (benefitResponseTO == null) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    benefitResponseTO = getBenefitsHrBasedListFromDB();
                    if (benefitResponseTO != null) {
                        try {
                            altBenefitsCache.putOrganizationWiseVendorOfferings(benefitResponseTO);
                        } catch (Exception ex1) {
                            throw ex1;
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            return benefitResponseTO;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     *  
     * @return
     * @throws Exception
     */
    public BenefitResponseTO getBenefitsHrBasedListFromDB()
        throws Exception
    {
        BenefitResponseTO benefitResponseTO = null;
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap = null;
        SysBenefitsVendor vendor = null;
        BenefitVendorTO benefitVendorTO = null;
        List<HrVendorOfferingSequence> hrVendorOfferingSequencesList = null;
        List<HrVendorOfferingTO> hrVendorOfferingTOList = null;
        HrVendorOfferingTO hrVendorOfferingTO = null;
        vendorOfferingTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            hrVendorOfferingTOList = new ArrayList<HrVendorOfferingTO>();
            hrVendorOfferingmap = new LinkedHashMap<String, List<VendorOfferingTemplateTO>>();
            hrVendorOfferingSequencesList = hrVendorOfferingSequenceDS.getHrOfferingTemplateList();
            processHrOfferingTOList(hrVendorOfferingSequencesList);
            if (hrVendorOfferingSequencesList != null && !hrVendorOfferingSequencesList.isEmpty()) {
                for (HrVendorOfferingSequence hrSequence : hrVendorOfferingSequencesList) {
                    HrVendorOfferingTemplate offerSequence = hrSequence.getHrVendorOfferingTemplate();
                    hrVendorOfferingTO = new HrVendorOfferingTO();
                    hrVendorOfferingTO.setHrVendorOfferingSequenceID(hrSequence.getHrVendorOfferingSequenceID());
                    hrVendorOfferingTO.setHrVendorOfferingID(offerSequence.getHrVendorOfferingID());
                    hrVendorOfferingTO.setOrgCode(offerSequence.getHrOrganization().getOrganizationCode());
                    hrVendorOfferingTO.setOrgName(offerSequence.getHrOrganization().getOrganizationName());
                    hrVendorOfferingTO.setOrgID(offerSequence.getOrganizationID());
                    hrVendorOfferingTO.setVendorOfferingID(offerSequence.getVendorOfferingID());
                    hrVendorOfferingTO.setSectorName(offerSequence.getHrOrganization().getSysSector().getSectorLabel());
                    hrVendorOfferingTO.setOfferingVendorTemplate(offerSequence.getSysVendorOfferingTemplate().getOfferTemplateName());
                    hrVendorOfferingTO.setVendorName(offerSequence.getSysVendorOfferingTemplate().getSysBenefitsVendor().getVendorName());
                    hrVendorOfferingTO.setOfferingType(offerSequence.getSysVendorOfferingTemplate().getSysOfferingType().getOfferingType());
                    hrVendorOfferingTO.setBanner(offerSequence.getSysVendorOfferingTemplate().getBanner());
                    hrVendorOfferingTO.setSequence(hrSequence.getOfferingSequence());

                    /**
                     * adding offerVendering Template Details
                     */
                    SysVendorOfferingTemplate svO = offerSequence.getSysVendorOfferingTemplate();
                    VendorOfferingTemplateTO oTempTO = new VendorOfferingTemplateTO();
                    vendor = svO.getSysBenefitsVendor();
                    oTempTO.setBanner(svO.getBanner());
                    oTempTO.setVendorOfferingID(svO.getVendorOfferingID());
                    oTempTO.setVendorID(svO.getVendorID());
                    oTempTO.setVendorName(svO.getSysBenefitsVendor().getVendorName());
                    oTempTO.setOfferingTypeID(svO.getOfferingTypeID());
                    oTempTO.setOfferingType(svO.getSysOfferingType().getOfferingType());
                    oTempTO.setOfferingSequence(svO.getOfferingSequence());
                    oTempTO.setText(svO.getText());
                    oTempTO.setOfferingLink(svO.getOfferingLink());
                    oTempTO.setOfferTemplateName(svO.getOfferTemplateName());
                    oTempTO.setActive(svO.getActive());
                    if (svO.getContractSignedDateID() != null) {
                        oTempTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractSignedDateID()));
                    }
                    if (svO.getContractStartDateID() != null) {
                        oTempTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractStartDateID()));
                    }
                    if (svO.getContractEndDateID() != null) {
                        oTempTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractEndDateID()));
                    }
                    if (svO.getEffectiveStartDateID() != null) {
                        oTempTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveStartDateID()));
                    }
                    if (svO.getEffectiveEndDateID() != null) {
                        oTempTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveEndDateID()));
                    }
                    oTempTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractSignedDate()));
                    oTempTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractStartDate()));
                    oTempTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractEndDate()));
                    oTempTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveStartDate()));
                    oTempTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveEndDate()));
                    /**
                     * offering peak details
                     * 
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        oTempTO.setPeakBasedOffering(svO.getOfferingPeakBased());
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            peakRecurringDetailTO = new PeakRecurringDetailTO();
                            peakRecurringDetailTO.setPeakTimeRecurringTypeLabel(peakRecurringDetail.getSysPeakRecurringType().getSysType());
                            peakRecurringDetailTO.setPeakTimeRecurringTypeID(peakRecurringDetail.getPeakTimeRecurringTypeID().intValue());
                            peakRecurringDetailTO.setVendorOfferingPeakDetailID(peakRecurringDetail.getVendorOfferingPeakDetailID());
                            peakRecurringDetailTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            peakRecurringDetailTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            peakRecurringDetailTO.setPeakStartDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakStartDateTime()));
                            peakRecurringDetailTO.setPeakEndDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakEndDateTime()));
                            peakRecurringDetailTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            peakRecurringDetailTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            peakRecurringDetailTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            peakRecurringDetailTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                        }
                        oTempTO.setPeakRecurringDetailTO(peakRecurringDetailTO);
                    }

                    /**
                     * Offering Code Details
                     */
                    oTempTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));


                    /**
                     * Adding vendor details
                     */
                    benefitVendorTO = new BenefitVendorTO();
                    benefitVendorTO.setVendorID(vendor.getVendorID());
                    benefitVendorTO.setVendorName(vendor.getVendorName());
                    benefitVendorTO.setVendorCode(vendor.getVendorCode());
                    benefitVendorTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractSignedDate()));
                    benefitVendorTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate()));
                    if (vendor.getContractEndDate() != null) {
                        benefitVendorTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate()));
                    }
                    benefitVendorTO.setActive(vendor.getActive());
                    if (vendor.getEffectiveStartDate() != null) {
                        benefitVendorTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveStartDate()));
                    }
                    if (vendor.getEffectiveEndDate() != null) {
                        benefitVendorTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveEndDate()));
                    }
                    if (vendor.getActive() != null && vendor.getActive()) {
                        benefitVendorTO.setStatus(IRoleConstants.ACTIVE_STRING);
                    } else {
                        benefitVendorTO.setStatus(IRoleConstants.INACTIVE_STRING);
                    }
                    benefitVendorTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractSignedDate()));
                    benefitVendorTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractStartDate()));
                    benefitVendorTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractEndDate()));
                    benefitVendorTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveStartDate()));
                    benefitVendorTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveEndDate()));
                    benefitVendorTO.setTermsAndConditions(vendor.getTermAndConditions());
                    benefitVendorTO.setVendorLogo(vendor.getVendorLogo());
                    oTempTO.setBenefitVendorTO(benefitVendorTO);
                    oTempTO.setHrVendorOfferingID(hrVendorOfferingTO.getHrVendorOfferingID());
                    hrVendorOfferingTO.setVendorOfferingTemplateTO(oTempTO);
                    hrVendorOfferingTOList.add(hrVendorOfferingTO);

                    /**
                     * processing group Map for vendorOffering
                     */
                    if (hrVendorOfferingmap.containsKey(offerSequence.getOrganizationID() + CoreConstants.AT_SPERATOR
                            + offerSequence.getHrOrganization().getSysSector().getSectorLabel() + CoreConstants.AT_SPERATOR
                            + offerSequence.getHrOrganization().getOrganizationName())) {
                        hrVendorOfferingmap.get(
                                offerSequence.getOrganizationID() + CoreConstants.AT_SPERATOR + offerSequence.getHrOrganization().getSysSector().getSectorLabel()
                                        + CoreConstants.AT_SPERATOR + offerSequence.getHrOrganization().getOrganizationName()).add(oTempTO);
                    } else {
                        vendorOfferingTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
                        vendorOfferingTemplateTOList.add(oTempTO);
                        hrVendorOfferingmap.put(offerSequence.getOrganizationID() + CoreConstants.AT_SPERATOR + offerSequence.getHrOrganization().getSysSector().getSectorLabel()
                                + CoreConstants.AT_SPERATOR + offerSequence.getHrOrganization().getOrganizationName(), vendorOfferingTemplateTOList);
                    }
                }
            }

            benefitResponseTO = new BenefitResponseTO();
            benefitResponseTO.setHrVendorOfferingmap(hrVendorOfferingmap);
            benefitResponseTO.setHrVendorOfferingTOList(hrVendorOfferingTOList);
            return benefitResponseTO;

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param benefitRequestTO
     * @return
     * @throws Exception
     */
    public boolean saveHrVendorOfferSequence(BenefitRequestTO benefitRequestTO)
        throws Exception
    {
        boolean success = false;
        Map<Integer, Long> hrVendorOfferSequenceMap = null;
        List<Integer> sequenceList = null;
        Set<Integer> sequenceSet = null;
        try {
            hrVendorOfferSequenceMap = benefitRequestTO.getHrVendorOfferSequenceMap();
            sequenceList = new ArrayList<Integer>();
            sequenceSet = hrVendorOfferSequenceMap.keySet();
            for (int seq : sequenceSet) {
                sequenceList.add(seq);
            }
            hrVendorOfferingSequenceDS.saveHrVendorOfferSequence(sequenceList, benefitRequestTO);
            success = true;
            try {
                altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
            } catch (Exception e) {
                LOGGER_.error(AltBenefitsKeyConstants.DELETE_ORG_WISE_REDIS_KEY);
            }
        } catch (Exception e) {
            throw e;
        }
        return success;
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    public List<VendorOfferingTemplateTO> getOfferingTemplateList(Long selectedOrganizations)
        throws Exception
    {
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        try {
            try {
                vendorOfferingTemplateTOList = altBenefitsCache.getHrVendorOfferingsTemplatesByOrg(selectedOrganizations);
                if (vendorOfferingTemplateTOList == null) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    vendorOfferingTemplateTOList = getOfferingTemplateListFromDB(selectedOrganizations);
                    if (vendorOfferingTemplateTOList != null) {
                        try {
                            altBenefitsCache.putSystemVendorOfferingsTemplatesByOrg(vendorOfferingTemplateTOList, selectedOrganizations);
                        } catch (Exception ex1) {
                            throw ex1;
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            return vendorOfferingTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<VendorOfferingTemplateTO> getOfferingTemplateListFromDB(Long selectedOrganizations)
        throws Exception
    {
        List<HrVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        try {
            sysVendorOfferingTemplateList = hrOfferingTemplateDS.getHrOfferingTemplateListByOrgID(selectedOrganizations);
            vendorOfferingTemplateTOList = processOfferingTemplateList(sysVendorOfferingTemplateList);
            return vendorOfferingTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param vendorOfferingID
     * @param status
     * @return
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void changeTheStatusHrOfferingTemplate(Long vendorOfferingID, boolean status)
        throws Exception
    {
        try {
            hrOfferingTemplateDS.changeTheStatusHrOfferingTemplate(vendorOfferingID, status);
            getAllHrOfferingTOList();
        } catch (Exception e) {
            throw e;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void changeTheStatusHrOfferingTemplate()
        throws Exception
    {
        try {
            hrOfferingTemplateDS.changeTheStatusHrOfferingTemplateByVendor();
            getAllHrOfferingTOList();
        } catch (Exception e) {
            throw e;
        }
    }

    public void getAllHrOfferingTOList()
        throws Exception
    {
        List<HrVendorOfferingSequence> sysVendorOfferingTemplateList = null;
        try {
            sysVendorOfferingTemplateList = hrVendorOfferingSequenceDS.getHrOfferingTemplateList();
            processHrOfferingTOList(sysVendorOfferingTemplateList);
        } catch (Exception e) {
            throw e;
        }
    }


    public List<HrOfferingTO> processHrOfferingTOList(List<HrVendorOfferingSequence> sysVendorOfferingTemplateList)
        throws Exception
    {
        List<HrOfferingTO> hrOfferingTOList = null;
        HrOfferingTO hrOfferingTO = null;
        BannerOfferingSequenceTO bannerOfferingSequenceTO = null;
        Map<Long, List<HrOfferingTO>> hrOfferSequenceMap = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        Date startDate = null;
        Date endDate = null;
        SysBenefitsVendor vendor = null;
        try {
            hrOfferingTOList = new ArrayList<HrOfferingTO>();
            hrOfferSequenceMap = new LinkedHashMap<Long, List<HrOfferingTO>>();
            Date now = new Date();
            if (sysVendorOfferingTemplateList != null && !sysVendorOfferingTemplateList.isEmpty()) {
                for (HrVendorOfferingSequence OfferingSequence : sysVendorOfferingTemplateList) {
                    HrVendorOfferingTemplate hvO = OfferingSequence.getHrVendorOfferingTemplate();
                    SysVendorOfferingTemplate svO = hvO.getSysVendorOfferingTemplate();
                    vendor = svO.getSysBenefitsVendor();
                    startDate = CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate());
                    endDate = CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate());
                    if (CommonUtilHelper.CompareDateWithNullCheck(now, endDate) <= 0 && CommonUtilHelper.CompareDateWithNullCheck(now, startDate) >= 0) {
                        /**
                         * 
                         */
                        hrOfferingTO = new HrOfferingTO();
                        bannerOfferingSequenceTO = new BannerOfferingSequenceTO();
                        hrOfferingTO.setHrVendorOfferingID(hvO.getHrVendorOfferingID());
                        hrOfferingTO.setOfferingSequence(svO.getOfferingSequence());
                        bannerOfferingSequenceTO.setBanner(svO.getBanner());
                        bannerOfferingSequenceTO.setOfferingSequence(OfferingSequence.getOfferingSequence());
                        bannerOfferingSequenceTO.setText(svO.getText());
                        bannerOfferingSequenceTO.setOfferingLink(svO.getOfferingLink());
                        bannerOfferingSequenceTO.setOfferTemplateName(svO.getOfferTemplateName());
                        bannerOfferingSequenceTO.setTermsAndCondition(vendor.getTermAndConditions());
                        bannerOfferingSequenceTO.setVendorLogo(vendor.getVendorLogo());
                        /**
                         * offering peak details
                         * 
                         */
                        if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                            if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                                peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                                bannerOfferingSequenceTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                                bannerOfferingSequenceTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                                bannerOfferingSequenceTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                                bannerOfferingSequenceTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                                bannerOfferingSequenceTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                                bannerOfferingSequenceTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                                bannerOfferingSequenceTO.setPeakDetails(true);
                            }
                        }
                        /**
                         * adding offering code details
                         */
                        bannerOfferingSequenceTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));
                        hrOfferingTO.setBannerOfferingSequenceTO(bannerOfferingSequenceTO);
                        if (!hrOfferSequenceMap.isEmpty() && hrOfferSequenceMap.containsKey(hvO.getOrganizationID())) {
                            hrOfferSequenceMap.get(hvO.getOrganizationID()).add(hrOfferingTO);
                        } else {
                            hrOfferingTOList = new ArrayList<HrOfferingTO>();
                            hrOfferingTOList.add(hrOfferingTO);
                            hrOfferSequenceMap.put(hvO.getOrganizationID(), hrOfferingTOList);
                        }

                    }

                }
            }
            altBenefitsCache.deleteHrVendorOfferingsMap();
            altBenefitsCache.putHrVendorOfferingsMap(hrOfferSequenceMap);
            return hrOfferingTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    public List<VendorOfferingTemplateTO> processOfferingTemplateList(List<HrVendorOfferingTemplate> sysVendorOfferingTemplateList)
        throws Exception
    {
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        vendorOfferingTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        SysBenefitsVendor vendor = null;
        BenefitVendorTO benefitVendorTO = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            if (sysVendorOfferingTemplateList != null && !sysVendorOfferingTemplateList.isEmpty()) {
                for (HrVendorOfferingTemplate hvO : sysVendorOfferingTemplateList) {
                    vendor = hvO.getSysVendorOfferingTemplate().getSysBenefitsVendor();
                    SysVendorOfferingTemplate svO = hvO.getSysVendorOfferingTemplate();
                    VendorOfferingTemplateTO oTempTO = new VendorOfferingTemplateTO();
                    oTempTO.setHrVendorOfferingID(hvO.getHrVendorOfferingID());
                    oTempTO.setBanner(svO.getBanner());
                    oTempTO.setVendorOfferingID(svO.getVendorOfferingID());
                    oTempTO.setVendorID(svO.getVendorID());
                    oTempTO.setVendorName(svO.getSysBenefitsVendor().getVendorName());
                    oTempTO.setOfferingTypeID(svO.getOfferingTypeID());
                    oTempTO.setOfferingSequence(svO.getOfferingSequence());
                    oTempTO.setText(svO.getText());
                    oTempTO.setOfferingLink(svO.getOfferingLink());
                    oTempTO.setOfferTemplateName(svO.getOfferTemplateName());
                    oTempTO.setActive(svO.getActive());
                    if (svO.getContractSignedDateID() != null) {
                        oTempTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractSignedDateID()));
                    }
                    if (svO.getContractStartDateID() != null) {
                        oTempTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractStartDateID()));
                    }
                    if (svO.getContractEndDateID() != null) {
                        oTempTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractEndDateID()));
                    }
                    if (svO.getEffectiveStartDateID() != null) {
                        oTempTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveStartDateID()));
                    }
                    if (svO.getEffectiveEndDateID() != null) {
                        oTempTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveEndDateID()));
                    }
                    oTempTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractSignedDate()));
                    oTempTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractStartDate()));
                    oTempTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractEndDate()));
                    oTempTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveStartDate()));
                    oTempTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveEndDate()));
                    /**
                     * offering peak details
                     * 
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        oTempTO.setPeakBasedOffering(svO.getOfferingPeakBased());
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            peakRecurringDetailTO = new PeakRecurringDetailTO();
                            peakRecurringDetailTO.setPeakTimeRecurringTypeLabel(peakRecurringDetail.getSysPeakRecurringType().getSysType());
                            peakRecurringDetailTO.setPeakTimeRecurringTypeID(peakRecurringDetail.getPeakTimeRecurringTypeID().intValue());
                            peakRecurringDetailTO.setVendorOfferingPeakDetailID(peakRecurringDetail.getVendorOfferingPeakDetailID());
                            peakRecurringDetailTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            peakRecurringDetailTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            peakRecurringDetailTO.setPeakStartDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakStartDateTime()));
                            peakRecurringDetailTO.setPeakEndDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakEndDateTime()));
                            peakRecurringDetailTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            peakRecurringDetailTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            peakRecurringDetailTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            peakRecurringDetailTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                        }
                        oTempTO.setPeakRecurringDetailTO(peakRecurringDetailTO);
                    }

                    /**
                     * Offering Code Details
                     */
                    oTempTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));

                    /**
                     * Adding vendor details
                     */
                    benefitVendorTO = new BenefitVendorTO();
                    benefitVendorTO.setVendorID(vendor.getVendorID());
                    benefitVendorTO.setVendorName(vendor.getVendorName());
                    benefitVendorTO.setVendorCode(vendor.getVendorCode());
                    benefitVendorTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractSignedDate()));
                    benefitVendorTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate()));
                    if (vendor.getContractEndDate() != null) {
                        benefitVendorTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate()));
                    }
                    benefitVendorTO.setActive(vendor.getActive());
                    if (vendor.getEffectiveStartDate() != null) {
                        benefitVendorTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveStartDate()));
                    }
                    if (vendor.getEffectiveEndDate() != null) {
                        benefitVendorTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveEndDate()));
                    }
                    if (vendor.getActive() != null && vendor.getActive()) {
                        benefitVendorTO.setStatus(IRoleConstants.ACTIVE_STRING);
                    } else {
                        benefitVendorTO.setStatus(IRoleConstants.INACTIVE_STRING);
                    }
                    benefitVendorTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractSignedDate()));
                    benefitVendorTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractStartDate()));
                    benefitVendorTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractEndDate()));
                    benefitVendorTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveStartDate()));
                    benefitVendorTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveEndDate()));
                    benefitVendorTO.setTermsAndConditions(vendor.getTermAndConditions());
                    benefitVendorTO.setVendorLogo(vendor.getVendorLogo());
                    oTempTO.setBenefitVendorTO(benefitVendorTO);

                    vendorOfferingTemplateTOList.add(oTempTO);
                }

            }
            return vendorOfferingTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void getAllEmpGroupBasedOfferingList()
        throws Exception
    {
        List<HrEmployeeGroupOfferingSequence> empGroupVendorOfferingTemplateList = null;
        try {
            empGroupVendorOfferingTemplateList = hrEmployeeGroupOfferingSequenceDS.getAllEmpGroupBasedOfferingList();
            processEmpGroupOfferingTOList(empGroupVendorOfferingTemplateList);
        } catch (Exception e) {
            throw e;
        }
    }

    public void processEmpGroupOfferingTOList(List<HrEmployeeGroupOfferingSequence> empGroupVendorOfferingTemplateList)
        throws Exception
    {
        List<HrOfferingTO> hrOfferingTOList = null;
        HrOfferingTO hrOfferingTO = null;
        BannerOfferingSequenceTO bannerOfferingSequenceTO = null;
        Map<String, List<HrOfferingTO>> empGroupOfferSequenceMap = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            hrOfferingTOList = new ArrayList<HrOfferingTO>();
            empGroupOfferSequenceMap = new LinkedHashMap<String, List<HrOfferingTO>>();
            if (empGroupVendorOfferingTemplateList != null && !empGroupVendorOfferingTemplateList.isEmpty()) {
                for (HrEmployeeGroupOfferingSequence offeringSequence : empGroupVendorOfferingTemplateList) {
                    HrVendorOfferingTemplate hvO = offeringSequence.getHrVendorOfferingTemplate();
                    SysVendorOfferingTemplate svO = hvO.getSysVendorOfferingTemplate();
                    /**
                     * 
                     */
                    hrOfferingTO = new HrOfferingTO();
                    bannerOfferingSequenceTO = new BannerOfferingSequenceTO();
                    hrOfferingTO.setHrVendorOfferingID(hvO.getHrVendorOfferingID());
                    hrOfferingTO.setOfferingSequence(hvO.getOfferingSequence());
                    bannerOfferingSequenceTO.setBanner(svO.getBanner());
                    bannerOfferingSequenceTO.setOfferingSequence(svO.getOfferingSequence());
                    bannerOfferingSequenceTO.setText(svO.getText());
                    bannerOfferingSequenceTO.setOfferingLink(svO.getOfferingLink());
                    bannerOfferingSequenceTO.setOfferTemplateName(svO.getOfferTemplateName());
                    /**
                     * offering peak details
                     * 
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            bannerOfferingSequenceTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            bannerOfferingSequenceTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            bannerOfferingSequenceTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            bannerOfferingSequenceTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            bannerOfferingSequenceTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            bannerOfferingSequenceTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                            bannerOfferingSequenceTO.setPeakDetails(true);
                        }
                    }
                    /**
                     * adding offering code details
                     */
                    bannerOfferingSequenceTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));


                    hrOfferingTO.setBannerOfferingSequenceTO(bannerOfferingSequenceTO);
                    if (!empGroupOfferSequenceMap.isEmpty()
                            && empGroupOfferSequenceMap.containsKey(offeringSequence.getOrganizationID() + CoreConstants.AT_SPERATOR + offeringSequence.getEmployeeGroupID())) {
                        empGroupOfferSequenceMap.get(offeringSequence.getOrganizationID() + CoreConstants.AT_SPERATOR + offeringSequence.getEmployeeGroupID()).add(hrOfferingTO);
                    } else {
                        hrOfferingTOList = new ArrayList<HrOfferingTO>();
                        hrOfferingTOList.add(hrOfferingTO);
                        empGroupOfferSequenceMap.put(offeringSequence.getOrganizationID() + CoreConstants.AT_SPERATOR + offeringSequence.getEmployeeGroupID(), hrOfferingTOList);
                    }

                }

            }
            altBenefitsCache.deleteEmpGroupVendorOfferingsMap();
            altBenefitsCache.putEmpGroupVendorOfferingsMap(empGroupOfferSequenceMap);
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @param offeringCodesSet
     * @return
     * @throws Exception
     */

    public List<OfferingCodeTO> getOfferingCodeListBySet(Set<OfferingCode> offeringCodesSet)
        throws Exception
    {
        List<OfferingCodeTO> offeringCodeTOList = null;
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        OfferingCodeTO offeringCodeTO = null;
        SysOfferingCodeMaximumLimit codeMaximumLimit = null;
        try {
            offeringCodeTOList = new ArrayList<OfferingCodeTO>();
            if (offeringCodesSet != null && !offeringCodesSet.isEmpty()) {
                for (Iterator<OfferingCode> fieldIterator = offeringCodesSet.iterator(); fieldIterator.hasNext();) {
                    OfferingCode offeringCode = fieldIterator.next();
                    offeringCodeTO = new OfferingCodeTO();
                    offeringCodeTO.setOfferingCodeID(offeringCode.getOfferingCodeID());
                    offeringCodeTO.setOfferingCode(offeringCode.getOfferingCode());
                    offeringCodeTO.setAccessTypeID(offeringCode.getAccessTypeID());
                    offeringCodeTO.setActive(offeringCode.getActive());
                    offeringCodeTO.setVendorOfferingID(offeringCode.getVendorOfferingID());
                    offeringCodeTO.setAccessType(offeringCode.getSysContentType().getSysType());
                    offeringCodeTO.setLogicBased(offeringCode.getLogicBased());
                    offeringCodeTO.setMaximumLimitApplicable(offeringCode.getMaximumLimitApplicable());
                    if (offeringCode.getOfferingCodeStartDateID() != null) {
                        offeringCodeTO.setOfferingCodeStartDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeStartDateID()));
                        offeringCodeTO.setOfferingCodeStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeStartDate()));
                    }
                    if (offeringCode.getOfferingCodeEndDateID() != null) {
                        offeringCodeTO.setOfferingCodeEndDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeEndDateID()));
                        offeringCodeTO.setOfferingCodeEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeEndDate()));
                    }
                    /**
                     * offer code maximum limit details
                     */
                    if (offeringCode.getMaximumLimitApplicable()) {
                        if (offeringCode.getMapCodeMaximumLimits() != null && !offeringCode.getMapCodeMaximumLimits().isEmpty()) {
                            codeMaximumLimit = offeringCode.getMapCodeMaximumLimits().iterator().next();
                            offeringCodeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                            offeringCodeMaximumLimitTO.setOfferingCodeMaximumLimitID(codeMaximumLimit.getOfferingCodeMaximumLimitID());
                            offeringCodeMaximumLimitTO.setMaximumUsageCount(codeMaximumLimit.getMaximumUsageCount());
                            offeringCodeMaximumLimitTO.setMaximumUsageType(codeMaximumLimit.getMaximumUsageType().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageTypeID(codeMaximumLimit.getMaximumUsageTypeID());
                            offeringCodeMaximumLimitTO.setMaximumUsageDuration(codeMaximumLimit.getMaximumUsageDuration().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageDurationID(codeMaximumLimit.getMaximumUsageDurationID());
                        }
                        offeringCodeTO.setCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                    }
                    offeringCodeTOList.add(offeringCodeTO);
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return offeringCodeTOList;
    }

}