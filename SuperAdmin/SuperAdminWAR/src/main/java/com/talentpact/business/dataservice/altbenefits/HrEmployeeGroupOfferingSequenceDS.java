package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.HrEmployeeGroupOfferingSequence;


@Stateless
public class HrEmployeeGroupOfferingSequenceDS extends AbstractDS<HrEmployeeGroupOfferingSequence>
{
    public HrEmployeeGroupOfferingSequenceDS()
    {
        super(HrEmployeeGroupOfferingSequence.class);
    }


    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public List<HrEmployeeGroupOfferingSequence> getAllEmpGroupBasedOfferingList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrEmployeeGroupOfferingSequence> employeeGroupOfferingSequencesList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrEmployeeGroupOfferingSequence s where s.hrVendorOfferingTemplate.active=1 order by s.sequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            employeeGroupOfferingSequencesList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return employeeGroupOfferingSequencesList;
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public List<HrEmployeeGroupOfferingSequence> changeTheStatusHrOfferingTemplate(Long hrVendorOfferingID, boolean status)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrEmployeeGroupOfferingSequence> HrEmployeeGroupOfferingSequenceList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" update  HrEmployeeGroupOfferingSequence s  set s.active=:status  where  s.hrVendorOfferingID=:hrVendorOfferingID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("status", status);
            query.setParameter("hrVendorOfferingID", hrVendorOfferingID);
            query.executeUpdate();
            HrEmployeeGroupOfferingSequenceList = getAllEmpGroupBasedOfferingList();
        } catch (Exception ex) {
            throw ex;
        }
        return HrEmployeeGroupOfferingSequenceList;
    }

}
