package com.talentpact.business.common.constants;

/**
 * 
 * @author prachi.bansal
 *
 */

public interface ISysPinCodeConstants{
    
    public static String SYSPINCODE_ADD_DIALOG_HIDE = "PF('sysPinCodeAddModal').hide();";

    public static String SYSPINCODE_EDIT_DIALOG_HIDE = "PF('sysPinCodeEditModal').hide();";

    public static String SYSPINCODE_SAVE_ERROR_MSG = "Error occured while saving SysPinCode.";

    public static String SYSPINCODE_UPDATE_ERROR_MSG = "Error occured while updating SysPinCode.";

    public static String SYSPINCODE_SAVE_SUCCESS_MSG = "SysPinCode saved successfully.";

    public static String SYSPINCODE_UPDATE_SUCCESS_MSG = "SysPinCode updated successfully.";

    public static String SYSPINCODE_DATATABLE_RESET = "PF('sysPinCodeDataTable').clearFilters();";

    public static String SYSPINCODE_PAGINATION_DATATABLE_RESET = "PF('sysPinCodeDataTable').getPaginator().setPage(0);";

    public static String SYSPINCODE_DUPLICATE_ERROR_MSG = "SysPinCode already present in system.";

}