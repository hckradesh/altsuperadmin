package com.talentpact.business.sysEntityColumn.facade;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.sysEntity.service.impl.SysEntityService;
import com.talentpact.business.sysEntityColumn.service.impl.SysEntityColumnService;
import com.talentpact.remote.sysEntityColumn.ISysEntityColumnRemote;
import com.talentpact.ui.sysEntityColumn.exception.SysEntityColumnException;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
@Local(ISysEntityColumnRemote.class)
public class SysEntityColumnFacade extends CommonFacade implements ISysEntityColumnRemote {

	@EJB
	SysEntityColumnService sysEntityColumnService;
	
	@EJB
	SysEntityService sysEntityService;
	
	@Override
	public ServiceResponse saveOrUpdateSysEntityColumn(
			ServiceRequest serviceRequest) throws SysEntityColumnException {
		ServiceResponse response = null;
		try{
		response = sysEntityColumnService.saveOrUpdateSysEntityColumn(serviceRequest);
		}catch(Exception e){
			throw new SysEntityColumnException();
		}
		return response;
	}

	@Override
	public ServiceResponse getAllSysEntityColumnListBySysEntityID(ServiceRequest serviceRequest) throws SysEntityColumnException {	
		ServiceResponse response = null;
		try{
		response = sysEntityColumnService.getSysEntityColumnsByEntityID(serviceRequest);
		}catch(Exception e){
			throw new SysEntityColumnException();
		}
		return response;
	}

}
