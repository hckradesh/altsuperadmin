/**
 * 
 */
package com.talentpact.business.dataservice.SysOfferingType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysOfferingTypeService extends CommonService
{

    @EJB
    SysOfferingTypeDS sysOfferingTypeDS;

    public List<SysOfferingTypeTO> getSysOfferingTypeList()
        throws Exception
    {
        List<SysOfferingTypeTO> list = null;
        SysOfferingTypeTO sysOfferingTypeTO = null;
        List<Object[]> sysOfferingTypeList = null;

        try {
            sysOfferingTypeList = sysOfferingTypeDS.getSysOfferingTypeList();
            list = new ArrayList<SysOfferingTypeTO>();
            for (Object[] sysOfferingType : sysOfferingTypeList) {
                sysOfferingTypeTO = new SysOfferingTypeTO();
                sysOfferingTypeTO.setOfferingTypeID((Long) sysOfferingType[0]);
                sysOfferingTypeTO.setOfferingType((String) sysOfferingType[1]);
                sysOfferingTypeTO.setDescription((String) sysOfferingType[2]);
                sysOfferingTypeTO.setOfferingCategoryID((Long) sysOfferingType[3]);
                sysOfferingTypeTO.setOfferingCategory((String) sysOfferingType[4]);

                list.add(sysOfferingTypeTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<SysOfferingTypeTO> getNewSysOfferingCategoryList()
        throws Exception
    {
        List<SysOfferingTypeTO> list = null;
        SysOfferingTypeTO sysOfferingTypeTO = null;
        List<Object[]> sysOfferingTypeList = null;
        try {
            sysOfferingTypeList = sysOfferingTypeDS.getNewSysOfferingCategoryList();
            list = new ArrayList<SysOfferingTypeTO>();
            for (Object[] sysOfferingType : sysOfferingTypeList) {
                sysOfferingTypeTO = new SysOfferingTypeTO();
                sysOfferingTypeTO.setOfferingCategoryID((Long) sysOfferingType[0]);
                if (sysOfferingType[1] != null)
                    sysOfferingTypeTO.setOfferingCategory(sysOfferingType[1].toString());
                list.add(sysOfferingTypeTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
        throws Exception
    {
        try {
            sysOfferingTypeDS.addSysOfferingType(sysOfferingTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
        throws Exception
    {
        try {
            sysOfferingTypeDS.saveSysOfferingType(sysOfferingTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}