package com.talentpact.business.application.transport.output;

import com.alt.common.transport.impl.ResponseTransferObject;

public class SysRuleResponseTO extends ResponseTransferObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer ruleID;
	
	private String ruleClassMethodName;

	public Integer getRuleID() {
		return ruleID;
	}

	public void setRuleID(Integer ruleID) {
		this.ruleID = ruleID;
	}

	public String getRuleClassMethodName() {
		return ruleClassMethodName;
	}

	public void setRuleClassMethodName(String ruleClassMethodName) {
		this.ruleClassMethodName = ruleClassMethodName;
	}

	
	

}
