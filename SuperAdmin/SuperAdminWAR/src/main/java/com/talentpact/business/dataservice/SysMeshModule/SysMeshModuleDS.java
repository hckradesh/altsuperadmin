/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshModule;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysMeshModule;
import com.talentpact.model.altbenefit.SysOfferingCategory;
import com.talentpact.model.group.SysPolicy;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysMeshModuleDS extends AbstractDS<SysOfferingCategory>
{
    public SysMeshModuleDS()
    {
        super(SysOfferingCategory.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysMeshModuleList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysMeshModule.SysMeshModuleID, sysMeshModule.SysMeshModuleName, sysMeshModule.SysMeshModuleDescription, ");
            hqlQuery.append(" sysSubModule.ModuleID, sysSubModule.ModuleName from altdatamanager..SysMeshModule sysMeshModule inner join talentpact..SysSubModule sysSubModule ");
            hqlQuery.append(" on  sysMeshModule.ModuleID = sysSubModule.ModuleID ");
            hqlQuery.append(" order by sysMeshModule.SysMeshModuleName");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    public boolean validateSysMeshModuleList(SysMeshModuleTO sysMeshModuleTO)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysMeshModule.SysMeshModuleID, sysMeshModule.SysMeshModuleName, sysMeshModule.SysMeshModuleDescription, ");
            hqlQuery.append(" sysSubModule.ModuleID, sysSubModule.ModuleName from altdatamanager..SysMeshModule sysMeshModule inner join talentpact..SysSubModule sysSubModule ");
            hqlQuery.append(" on  sysMeshModule.ModuleID = sysSubModule.ModuleID where sysMeshModule.SysMeshModuleName = :sysMeshModuleName ");
            if(sysMeshModuleTO.getSysMeshModuleID() != null)
            {
            	hqlQuery.append(" and sysMeshModule.SysMeshModuleID != :sysMeshModuleID and sysMeshModule.ModuleID = :sysSubModuleID");
            }
            else if(sysMeshModuleTO.getModuleID() !=0)
            {
            	hqlQuery.append(" and sysMeshModule.ModuleID = :sysSubModuleID ");
            }
            hqlQuery.append(" order by sysMeshModule.SysMeshModuleName");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            if(sysMeshModuleTO.getSysMeshModuleID() != null)
            {
            	query.setParameter("sysMeshModuleID", sysMeshModuleTO.getSysMeshModuleID());
            	query.setParameter("sysSubModuleID", sysMeshModuleTO.getModuleID());
            }
            else if(sysMeshModuleTO.getModuleID() !=0)
            {
            	query.setParameter("sysSubModuleID", sysMeshModuleTO.getModuleID());
            }
            query.setParameter("sysMeshModuleName", sysMeshModuleTO.getSysMeshModuleName().trim());
            list = query.getResultList();
            if(list != null && list.size() > 0)
            {
            	return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    public List<Object[]> getSysSubModuleList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysSubModule.moduleId, sysSubModule.moduleName from SysSubModule sysSubModule where sysSubModule.parentModuleId is null order by sysSubModule.moduleName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean saveSysMeshModule(SysMeshModuleTO sysMeshModuleTO)
    {
        SysMeshModule sysMeshModule = new SysMeshModule();
        try {
            sysMeshModule.setModuleID(sysMeshModuleTO.getModuleID());
            sysMeshModule.setSysMeshModuleName(sysMeshModuleTO.getSysMeshModuleName());
            sysMeshModule.setSysMeshModuleDescription(sysMeshModuleTO.getSysMeshModuleDescription());
            sysMeshModule.setCreatedBy(1);
            sysMeshModule.setModifiedBy(1);
            sysMeshModule.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysMeshModule.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("AltDataManager").persist(sysMeshModule);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void updateSysMeshModule(SysMeshModuleTO sysMeshModuleTO) 
	    {
	    	SysMeshModule sysMeshModule = new SysMeshModule();
	        try {
	        	sysMeshModule = findSysMeshModule(sysMeshModuleTO);
	        	sysMeshModule.setSysMeshModuleName(sysMeshModuleTO.getSysMeshModuleName());
	        	sysMeshModule.setSysMeshModuleDescription(sysMeshModuleTO.getSysMeshModuleDescription());
	        	sysMeshModule.setModuleID(sysMeshModuleTO.getModuleID());
	        	sysMeshModule.setModifiedBy(1);
	        	sysMeshModule.setModifiedDate(new Date());
	            getEntityManager("AltDataManager").merge(sysMeshModule);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	    }
	
	    
	    @SuppressWarnings("unchecked")
	    private SysMeshModule findSysMeshModule(SysMeshModuleTO sysMeshModuleTO)
	        throws Exception
	    {
	        Query query = null;
	        StringBuilder hqlQuery = null;
	        List<SysMeshModule> sysMeshModule = null;
	        SysMeshModule result = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select smm from SysMeshModule smm  where smm.sysMeshModuleID=:sysMeshModuleID ");
	            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
	            query.setParameter("sysMeshModuleID", sysMeshModuleTO.getSysMeshModuleID());
	            sysMeshModule = query.getResultList();
	            if (sysMeshModule != null && sysMeshModule.size() > 0) {
	                result = sysMeshModule.get(0);
	            }
	            return result;
	        } catch (Exception ex) {
	            throw ex;
	        } finally {
	            hqlQuery = null;
	            query = null;
	        }
	    }
}