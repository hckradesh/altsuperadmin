package com.talentpact.business.dataservice.sysDSOPlaceHolder;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.model.SysFieldType;
import com.talentpact.model.SysTemplateConfig;
import com.talentpact.ui.DSPTemplateConfig.to.DSOTemplateConfigTO;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;
import com.talentpact.ui.sysFieldType.to.SysFieldTypeTO;

@Stateless
public class DSOTemplateConfigService extends CommonService{

	@EJB
	DSOTemplateConfigDS dsoTemplateConfigDS;
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(DSOTemplateConfigService.class);
	
	public List<DSOTemplateConfigTO> getDSOTemplateConfigList() throws Exception{
        List<SysTemplateConfig> sysTemplateConfigList=null;
        List<DSOTemplateConfigTO> dsoTemplateConfigTOList=null;
        dsoTemplateConfigTOList=new ArrayList<DSOTemplateConfigTO>();
        try{
        	sysTemplateConfigList=dsoTemplateConfigDS.getDSOTemplateConfigList();
            if(sysTemplateConfigList!=null && !sysTemplateConfigList.isEmpty()){
                for(SysTemplateConfig sTC : sysTemplateConfigList){
                	DSOTemplateConfigTO dsoTemplateConfigTO=new DSOTemplateConfigTO();
                	
                	dsoTemplateConfigTO.setBcc(sTC.getBcc());
                	dsoTemplateConfigTO.setCc(sTC.getCc());
                	dsoTemplateConfigTO.setTemplateId(sTC.getTemplateId());
                	dsoTemplateConfigTO.setTemplateName(sTC.getTemplateName());
                	dsoTemplateConfigTO.setDescription(sTC.getDescription());
                	dsoTemplateConfigTO.setContent(sTC.getContent());
                	dsoTemplateConfigTO.setTo(sTC.getTo());
                	dsoTemplateConfigTO.setSubject(sTC.getSubject());
                	dsoTemplateConfigTO.setFrom(sTC.getFrom());
                	dsoTemplateConfigTOList.add(dsoTemplateConfigTO);
                }
            }
            return dsoTemplateConfigTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }
	
   
    public int saveDSOTemplateConfig(DSOTemplateConfigTO dsoTemplateConfigTO) {
    	int templateID = 0;
        try{
        	templateID = dsoTemplateConfigDS.setDSOTemplateConfig(dsoTemplateConfigTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return templateID;
    }
    
    public int updateDSOTemplateConfig(DSOTemplateConfigTO dsoTemplateConfigTO) {
    	int templateID = 0;
    	try{
    		templateID = dsoTemplateConfigDS.setDSOTemplateConfig(dsoTemplateConfigTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
		return templateID;
    }

}
