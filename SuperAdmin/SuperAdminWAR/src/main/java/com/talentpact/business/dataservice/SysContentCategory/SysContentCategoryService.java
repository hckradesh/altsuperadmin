/**
 * 
 */
package com.talentpact.business.dataservice.SysContentCategory;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysContentCategoryService extends CommonService
{

    @EJB
    SysContentCategoryDS sysContentCategoryDS;

    public List<SysContentCategoryTO> getSysContentCategoryList()
        throws Exception
    {
        List<SysContentCategoryTO> list = null;
        SysContentCategoryTO sysContentCategoryTO = null;
        List<Object[]> sysContentCategoryList = null;

        try {
            sysContentCategoryList = sysContentCategoryDS.getSysContentCategoryList();
            list = new ArrayList<SysContentCategoryTO>();
            for (Object[] sysContentCategory : sysContentCategoryList) {
                sysContentCategoryTO = new SysContentCategoryTO();
                sysContentCategoryTO.setContentCategoryID((Integer) sysContentCategory[0]);
                sysContentCategoryTO.setCategoryName(sysContentCategory[1].toString());
                list.add(sysContentCategoryTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addSysContentCategory(SysContentCategoryTO sysContentCategoryTO)
        throws Exception
    {
        try {
            sysContentCategoryDS.addSysContentCategory(sysContentCategoryTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editSysContentCategory(SysContentCategoryTO sysContentCategoryTO)
        throws Exception
    {
        try {
            sysContentCategoryDS.editSysContentCategory(sysContentCategoryTO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}