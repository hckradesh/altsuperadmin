package com.talentpact.business.dataservice.replicateFormSecurity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.ui.replicateFormSecurity.to.ReplicateFormSecurityTO;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class ReplicateFormSecurityService extends CommonService
{

    @EJB
    ReplicateFormSecurityDS replicateFormSecurityDS;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(ReplicateFormSecurityService.class);

    public List<ReplicateFormSecurityTO> getSourceOrganizationList()
        throws Exception
    {
        List<ReplicateFormSecurityTO> list = null;
        ReplicateFormSecurityTO replicateFormSecurityTO = null;
        List<Object[]> sourceOrganizationList = null;

        try {
            sourceOrganizationList = replicateFormSecurityDS.getSourceOrganizationList();
            list = new ArrayList<ReplicateFormSecurityTO>();
            for (Object[] sourceOrganization : sourceOrganizationList) {
                replicateFormSecurityTO = new ReplicateFormSecurityTO();
                replicateFormSecurityTO.setOrganizationID((Integer) sourceOrganization[0]);
                replicateFormSecurityTO.setOrganizationName((String) sourceOrganization[1]);
                list.add(replicateFormSecurityTO);
            }
            return list;
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public List<ReplicateFormSecurityTO> getDestinationOrganizationList(Integer orgID)
        throws Exception
    {
        List<ReplicateFormSecurityTO> list = null;
        ReplicateFormSecurityTO replicateFormSecurityTO = null;
        List<Object[]> destinationOrganizationList = null;

        try {
            destinationOrganizationList = replicateFormSecurityDS.getDestinationOrganizationList(orgID);
            list = new ArrayList<ReplicateFormSecurityTO>();
            for (Object[] destinationOrganization : destinationOrganizationList) {
                replicateFormSecurityTO = new ReplicateFormSecurityTO();
                replicateFormSecurityTO.setOrganizationID((Integer) destinationOrganization[0]);
                replicateFormSecurityTO.setOrganizationName((String) destinationOrganization[1]);
                list.add(replicateFormSecurityTO);
            }
            return list;
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public List<ReplicateFormSecurityTO> getSourceModuleList(Integer sourceOrgID, Integer destOrgID)
        throws Exception
    {
        List<ReplicateFormSecurityTO> list = null;
        ReplicateFormSecurityTO replicateFormSecurityTO = null;
        List<Object[]> sourceModuleList = null;

        try {
            sourceModuleList = replicateFormSecurityDS.getSourceModuleList(sourceOrgID, destOrgID);
            list = new ArrayList<ReplicateFormSecurityTO>();
            for (Object[] sourceModule : sourceModuleList) {
                replicateFormSecurityTO = new ReplicateFormSecurityTO();
                replicateFormSecurityTO.setModuleID((Integer) sourceModule[0]);
                replicateFormSecurityTO.setModuleName((String) sourceModule[1]);
                replicateFormSecurityTO.setSysModuleID((Integer) sourceModule[2]);
                list.add(replicateFormSecurityTO);
            }
            return list;
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public boolean validateExistingFormSecurity(Integer orgID, Integer sysModuleID)
    {
        try {
            return replicateFormSecurityDS.validateExistingFormSecurity(orgID, sysModuleID);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return false;
    }

    public List<ReplicateFormSecurityTO> getReplicateFormSecurityHistory()
        throws Exception
    {
        List<ReplicateFormSecurityTO> list = null;
        ReplicateFormSecurityTO replicateFormSecurityTO = null;
        List<Object[]> historyList = null;

        try {
            historyList = replicateFormSecurityDS.getReplicateFormSecurityHistory();
            list = new ArrayList<ReplicateFormSecurityTO>();
            for (Object[] history : historyList) {
                replicateFormSecurityTO = new ReplicateFormSecurityTO();
                replicateFormSecurityTO.setSourceOrganizationName((String) history[0]);
                replicateFormSecurityTO.setDestinationOrganizationName((String) history[1]);
                replicateFormSecurityTO.setModuleName((String) history[2]);
                replicateFormSecurityTO.setUserName((String) history[3]);
                replicateFormSecurityTO.setCreatedDate((Timestamp) history[4]);
                list.add(replicateFormSecurityTO);
            }
            return list;
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public boolean deactivateFormSecurity(Integer orgID, Integer moduleID)
    {
        return replicateFormSecurityDS.deactivateExistingFormSecurity(orgID, moduleID);

    }

    public boolean replicateFormSecurity(Integer sourceOrgID, Integer destOrgID, Integer sysModuleID, Long userID)
    {
        return replicateFormSecurityDS.replicateFormSecurity(sourceOrgID, destOrgID, sysModuleID, userID);
    }

    public boolean isWorkFlowEnabled(Integer sourceOrgID, Integer sysModuleID)
    {
        return replicateFormSecurityDS.isWorkFlowEnabled(sourceOrgID, sysModuleID);
    }
}