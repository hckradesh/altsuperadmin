package com.talentpact.business.dataservice.productAnnouncement;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.alt.utility.CommonUtil;
import com.talentpact.business.application.transport.input.SysProductAnnouncementRequestTO;
import com.talentpact.business.application.transport.output.SysProductAnnouncementTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.AppModuleDS;
import com.talentpact.business.model.AltCheckList;
import com.talentpact.model.organization.HrProductAnnouncement;
import com.talentpact.model.organization.SysProductAnnouncement;
import com.talentpact.ui.common.bean.UserSessionBean;

/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class ProductAnnouncementDS extends AbstractDS<SysProductAnnouncement>
{

    public ProductAnnouncementDS()
    {
        super(SysProductAnnouncement.class);
    }

    private static final Logger LOGGER_ = LoggerFactory.getLogger(ProductAnnouncementDS.class);

    @EJB
    AppModuleDS appModuleDS;

    @Inject
    UserSessionBean userSessionBean;

    public List<SysProductAnnouncement> getAllSysProductAnnouncement()
        throws Exception
    {
        StringBuilder queryStr = null;
        Query query = null;
        Date currentDate = new Date();
        try {
            queryStr = new StringBuilder();
            queryStr.append(" from SysProductAnnouncement sysProductAnnouncement where isActive = true");
            query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        } finally {
            queryStr = null;
            query = null;
        }
    }


    public List<HrProductAnnouncement> getAllHrProductAnnouncement(Integer orgID)
        throws Exception
    {
        StringBuilder queryStr = null;
        Query query = null;
        Date currentDate = new Date();
        try {
            queryStr = new StringBuilder();
            queryStr.append(" from HrProductAnnouncement hrProductAnnouncement where hrProductAnnouncement.active=1 and hrProductAnnouncement.hrOrganization=:orgID");
            queryStr.append(" and CONVERT(date, getdate()) between  hrProductAnnouncement.validFromID.theDate and hrProductAnnouncement.validToID.theDate ");
            query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            query.setParameter("orgID", orgID);
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw ex;
        } finally {
            queryStr = null;
            query = null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void copyHrProductAnnouncement(SysProductAnnouncementRequestTO sysProductAnnouncementRequestTO)
        throws Exception
    {
        HrProductAnnouncement hrProductAnnouncement = null;
        List<SysProductAnnouncementTO> sysProductAnnouncementTOList = null;
        AltCheckList altCheckList = null;
        Date date = new Date();
        try {
            sysProductAnnouncementTOList = sysProductAnnouncementRequestTO.getSysProductAnnouncementTOList();
            hrProductAnnouncement = new HrProductAnnouncement();
            if (sysProductAnnouncementTOList != null && !sysProductAnnouncementTOList.isEmpty()) {
                for (SysProductAnnouncementTO sysProductAnnouncementTO : sysProductAnnouncementTOList) {
                    hrProductAnnouncement = new HrProductAnnouncement();
                    hrProductAnnouncement = findHrProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID(), sysProductAnnouncementRequestTO.getOrganizationID()
                            .intValue());
                    if (hrProductAnnouncement != null) {

                        if (sysProductAnnouncementTO.isSelected()
                                && Integer.valueOf(CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidFrom())).equals(hrProductAnnouncement.getValidFrom())
                                && Integer.valueOf(CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidTo())).equals(hrProductAnnouncement.getValidTo())) {
                            //do nothing
                        } else if (!sysProductAnnouncementTO.isSelected()) {
                            hrProductAnnouncement.setActive(false);
                            hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                            hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                            getEntityManager("Talentpact").merge(hrProductAnnouncement);
                        } else if (sysProductAnnouncementTO.getValidTo().compareTo(date) < 0) {
                            hrProductAnnouncement.setActive(false);
                            hrProductAnnouncement.setValidFrom((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidFrom())));
                            hrProductAnnouncement.setValidTo((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidTo())));
                            hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                            hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                            getEntityManager("Talentpact").merge(hrProductAnnouncement);
                        } else {
                            hrProductAnnouncement.setValidFrom((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidFrom())));
                            hrProductAnnouncement.setValidTo((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidTo())));
                            hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                            hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                            getEntityManager("Talentpact").merge(hrProductAnnouncement);
                        }
                    } else {
                        if (sysProductAnnouncementTO.isSelected()) {
                            hrProductAnnouncement = new HrProductAnnouncement();
                            hrProductAnnouncement.setSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID());
                            hrProductAnnouncement.setValidFrom((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidFrom())));
                            hrProductAnnouncement.setValidTo((CommonUtil.getTimeDimensionId(sysProductAnnouncementTO.getValidTo())));
                            hrProductAnnouncement.setHrOrganization(sysProductAnnouncementRequestTO.getOrganizationID().intValue());
                            hrProductAnnouncement.setSysTenant(sysProductAnnouncementRequestTO.getTenantID().intValue());
                            if (sysProductAnnouncementTO.getValidTo().compareTo(date) < 0) {
                                hrProductAnnouncement.setActive(false);
                            } else {
                                hrProductAnnouncement.setActive(true);
                            }
                            hrProductAnnouncement.setCreatedBy(userSessionBean.getUserID().intValue());
                            hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                            hrProductAnnouncement.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                            hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                            getEntityManager("Talentpact").persist(hrProductAnnouncement);
                        }
                    }
                }
            }

            //saving checklist in altCheckList table 
            altCheckList = findAltCheckList(sysProductAnnouncementRequestTO.getSysCheckListID().intValue(), sysProductAnnouncementRequestTO.getOrganizationID());
            if (altCheckList == null) {
                altCheckList = new AltCheckList();
                altCheckList.setSysCheckListID(sysProductAnnouncementRequestTO.getSysCheckListID().intValue());
                altCheckList.setOrganizationID(sysProductAnnouncementRequestTO.getOrganizationID());
                altCheckList.setTenantID(sysProductAnnouncementRequestTO.getTenantID());
                altCheckList.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                altCheckList.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                altCheckList.setIsCompleted(true);
                altCheckList.setCreatedBy(userSessionBean.getUserID());
                altCheckList.setModifiedBy(userSessionBean.getUserID());

                getEntityManager("AltCommon").persist(altCheckList);
            }
            getEntityManager("AltCommon").flush();
            getEntityManager("TalentPact").flush();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw ex;
        }

    }

    public HrProductAnnouncement findHrProductAnnouncement(Integer sysProductAnnouncementID, Integer orgID)
        throws Exception
    {
        StringBuilder queryStr = null;
        Query query = null;
        List<HrProductAnnouncement> hrProductAnnouncementList = null;
        HrProductAnnouncement hrProductAnnouncement = null;
        try {
            queryStr = new StringBuilder();
            queryStr.append(" from HrProductAnnouncement hrProductAnnouncement where hrProductAnnouncement.active=1 and ");
            queryStr.append(" hrProductAnnouncement.sysProductAnnouncement=:sysProductAnnouncementID and hrProductAnnouncement.hrOrganization=:orgID ");
            queryStr.append(" and CONVERT(date, getdate()) between hrProductAnnouncement.validFromID.theDate and hrProductAnnouncement.validToID.theDate ");
            query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            query.setParameter("sysProductAnnouncementID", sysProductAnnouncementID);
            query.setParameter("orgID", orgID);
            hrProductAnnouncementList = query.getResultList();

            if (hrProductAnnouncementList != null && !hrProductAnnouncementList.isEmpty()) {
                hrProductAnnouncement = hrProductAnnouncementList.get(0);
            }
            return hrProductAnnouncement;
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw ex;
        }
    }

    public AltCheckList findAltCheckList(int sysCheckListId, Long OrgId)
        throws Exception
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<AltCheckList> altCheckList = null;
        AltCheckList result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append(" from AltCheckList altCheckList where altCheckList.sysCheckListID=:SysCheckListID and altCheckList.organizationID=:OrgID ");

            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("SysCheckListID", sysCheckListId);
            query.setParameter("OrgID", OrgId);
            altCheckList = query.getResultList();
            if (altCheckList != null && altCheckList.size() > 0) {
                result = altCheckList.get(0);
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw ex;
        }
        return result;
    }
}