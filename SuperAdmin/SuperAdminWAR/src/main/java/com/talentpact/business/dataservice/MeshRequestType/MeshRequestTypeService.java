/**
 * 
 */
package com.talentpact.business.dataservice.MeshRequestType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class MeshRequestTypeService extends CommonService
{

    @EJB
    MeshRequestTypeDS meshRequestTypeDS;

    public List<MeshRequestTypeTO> getMeshRequestTypeList()
        throws Exception
    {
        List<MeshRequestTypeTO> list = null;
        MeshRequestTypeTO meshRequestTypeTO = null;
        List<Object[]> meshRequestTypeList = null;

        try {
        	meshRequestTypeList = meshRequestTypeDS.getMeshRequestTypeList();
            list = new ArrayList<MeshRequestTypeTO>();
            for (Object[] meshRequestType : meshRequestTypeList) {
            	meshRequestTypeTO = new MeshRequestTypeTO();
            	meshRequestTypeTO.setRequestTypeId((Long) meshRequestType[0]);
                if (meshRequestType[1] != null)
                	meshRequestTypeTO.setRequestTypeName((String) meshRequestType[1]);
                list.add(meshRequestTypeTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addMeshRequestType(MeshRequestTypeTO meshRequestTypeTO)
        throws Exception
    {
        try {
        	meshRequestTypeDS.addMeshRequestType(meshRequestTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editMeshRequestType(MeshRequestTypeTO meshRequestTypeTO)
        throws Exception
    {
        try {
        	meshRequestTypeDS.editMeshRequestType(meshRequestTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}