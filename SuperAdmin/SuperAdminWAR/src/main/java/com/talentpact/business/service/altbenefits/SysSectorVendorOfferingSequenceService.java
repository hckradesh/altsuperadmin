/**
 * 
 */
package com.talentpact.business.service.altbenefits;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altbenefits.to.BannerOfferingSequenceTO;
import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.SectorVendorOfferingTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.altbenefits.transport.output.BenefitResponseTO;
import com.alt.common.constants.CoreConstants;
import com.alt.common.helper.CommonUtilHelper;
import com.alt.role.constants.IRoleConstants;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.SysSectorVendorOfferingSequenceDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsKeyConstants;
import com.talentpact.business.dataservice.timeDimension.TimeDimensionDS;
import com.talentpact.model.altbenefit.HrSectorVendorOfferingSequence;
import com.talentpact.model.altbenefit.OfferingCode;
import com.talentpact.model.altbenefit.SysBenefitsVendor;
import com.talentpact.model.altbenefit.SysOfferingCodeMaximumLimit;
import com.talentpact.model.altbenefit.SysVendorOfferingPeakDetail;
import com.talentpact.model.altbenefit.SysVendorOfferingTemplate;

/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class SysSectorVendorOfferingSequenceService extends CommonService
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysSectorVendorOfferingSequenceService.class);

    @EJB
    SysSectorVendorOfferingSequenceDS offeringSequenceDS;

    @EJB
    SysVendorOfferingTemplateDS offeringTemplateDS;

    @EJB
    TimeDimensionDS timeDimensionDS;

    @EJB
    AltBenefitsCache altBenefitsCache;

    /**
     * 
     * @return
     * @throws Exception
     */
    public BenefitResponseTO getBenefitsSectorBasedList()
        throws Exception
    {
        BenefitResponseTO benefitResponseTO = null;
        try {
            try {
                benefitResponseTO = altBenefitsCache.getSectorWiseVendorOfferings();
                if (benefitResponseTO == null) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    benefitResponseTO = getBenefitsSectorBasedListFromDB();
                    if (benefitResponseTO != null) {
                        try {
                            altBenefitsCache.putSectorWiseVendorOfferings(benefitResponseTO);
                        } catch (Exception ex1) {
                            throw ex1;
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            return benefitResponseTO;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @return
     * @throws Exception
     */
    public BenefitResponseTO getBenefitsSectorBasedListFromDB()
        throws Exception
    {
        BenefitResponseTO benefitResponseTO = null;
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap = null;
        SysBenefitsVendor vendor = null;
        BenefitVendorTO benefitVendorTO = null;
        List<HrSectorVendorOfferingSequence> hrSectorVendorOfferingSequencesList = null;
        List<SectorVendorOfferingTO> sectorVendorOfferingTOList = null;
        SectorVendorOfferingTO sectorVendorOfferingTO = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {

            sectorVendorOfferingTOList = new ArrayList<SectorVendorOfferingTO>();
            sectorVendorOfferingmap = new LinkedHashMap<String, List<VendorOfferingTemplateTO>>();
            hrSectorVendorOfferingSequencesList = offeringSequenceDS.getBenefitsSectorBasedList();
            processSectorOfferingTOList(hrSectorVendorOfferingSequencesList);
            if (hrSectorVendorOfferingSequencesList != null && !hrSectorVendorOfferingSequencesList.isEmpty()) {
                for (HrSectorVendorOfferingSequence offerSequence : hrSectorVendorOfferingSequencesList) {
                    sectorVendorOfferingTO = new SectorVendorOfferingTO();
                    sectorVendorOfferingTO.setSectorVendorSequenceID(offerSequence.getSectorVendorSequenceID());
                    sectorVendorOfferingTO.setSectorCode(offerSequence.getSysSector().getSectorCode());
                    sectorVendorOfferingTO.setSectorLabel(offerSequence.getSysSector().getSectorLabel());
                    sectorVendorOfferingTO.setSectorID(offerSequence.getSectorID());
                    sectorVendorOfferingTO.setVendorOfferingID(offerSequence.getVendorOfferingID());
                    sectorVendorOfferingTO.setOfferingVendorTemplate(offerSequence.getSysVendorOfferingTemplate().getOfferTemplateName());
                    sectorVendorOfferingTO.setVendorName(offerSequence.getSysVendorOfferingTemplate().getSysBenefitsVendor().getVendorName());
                    sectorVendorOfferingTO.setOfferingType(offerSequence.getSysVendorOfferingTemplate().getSysOfferingType().getOfferingType());
                    sectorVendorOfferingTO.setBanner(offerSequence.getSysVendorOfferingTemplate().getBanner());
                    sectorVendorOfferingTO.setSequence(offerSequence.getSequence());

                    /**
                     * adding offerVendering Template Details
                     */
                    SysVendorOfferingTemplate svO = offerSequence.getSysVendorOfferingTemplate();
                    VendorOfferingTemplateTO oTempTO = new VendorOfferingTemplateTO();
                    vendor = svO.getSysBenefitsVendor();
                    oTempTO.setBanner(svO.getBanner());
                    oTempTO.setVendorOfferingID(svO.getVendorOfferingID());
                    oTempTO.setVendorID(svO.getVendorID());
                    oTempTO.setVendorName(svO.getSysBenefitsVendor().getVendorName());
                    oTempTO.setOfferingTypeID(svO.getOfferingTypeID());
                    oTempTO.setOfferingType(svO.getSysOfferingType().getOfferingType());
                    oTempTO.setOfferingSequence(offerSequence.getSequence());
                    oTempTO.setText(svO.getText());
                    oTempTO.setOfferingLink(svO.getOfferingLink());
                    oTempTO.setOfferTemplateName(svO.getOfferTemplateName());
                    oTempTO.setActive(svO.getActive());
                    if (svO.getContractSignedDateID() != null) {
                        oTempTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractSignedDateID()));
                    }
                    if (svO.getContractStartDateID() != null) {
                        oTempTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractStartDateID()));
                    }
                    if (svO.getContractEndDateID() != null) {
                        oTempTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractEndDateID()));
                    }
                    if (svO.getEffectiveStartDateID() != null) {
                        oTempTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveStartDateID()));
                    }
                    if (svO.getEffectiveEndDateID() != null) {
                        oTempTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveEndDateID()));
                    }
                    oTempTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractSignedDate()));
                    oTempTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractStartDate()));
                    oTempTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractEndDate()));
                    oTempTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveStartDate()));
                    oTempTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveEndDate()));
                    /**
                     * adding peakRecurring details
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        oTempTO.setPeakBasedOffering(svO.getOfferingPeakBased());
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            peakRecurringDetailTO = new PeakRecurringDetailTO();
                            peakRecurringDetailTO.setPeakTimeRecurringTypeLabel(peakRecurringDetail.getSysPeakRecurringType().getSysType());
                            peakRecurringDetailTO.setPeakTimeRecurringTypeID(peakRecurringDetail.getPeakTimeRecurringTypeID().intValue());
                            peakRecurringDetailTO.setVendorOfferingPeakDetailID(peakRecurringDetail.getVendorOfferingPeakDetailID());
                            peakRecurringDetailTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            peakRecurringDetailTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            peakRecurringDetailTO.setPeakStartDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakStartDateTime()));
                            peakRecurringDetailTO.setPeakEndDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakEndDateTime()));
                            peakRecurringDetailTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            peakRecurringDetailTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            peakRecurringDetailTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            peakRecurringDetailTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                        }
                        oTempTO.setPeakRecurringDetailTO(peakRecurringDetailTO);
                    }


                    /**
                     * Offering Code Details
                     */
                    oTempTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));


                    /**
                    * Adding vendor details
                    */
                    benefitVendorTO = new BenefitVendorTO();
                    benefitVendorTO.setVendorID(vendor.getVendorID());
                    benefitVendorTO.setVendorName(vendor.getVendorName());
                    benefitVendorTO.setVendorCode(vendor.getVendorCode());
                    benefitVendorTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractSignedDate()));
                    benefitVendorTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate()));
                    if (vendor.getContractEndDate() != null) {
                        benefitVendorTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate()));
                    }
                    benefitVendorTO.setActive(vendor.getActive());
                    if (vendor.getEffectiveStartDate() != null) {
                        benefitVendorTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveStartDate()));
                    }
                    if (vendor.getEffectiveEndDate() != null) {
                        benefitVendorTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveEndDate()));
                    }
                    if (vendor.getActive() != null && vendor.getActive()) {
                        benefitVendorTO.setStatus(IRoleConstants.ACTIVE_STRING);
                    } else {
                        benefitVendorTO.setStatus(IRoleConstants.INACTIVE_STRING);
                    }
                    benefitVendorTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractSignedDate()));
                    benefitVendorTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractStartDate()));
                    benefitVendorTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractEndDate()));
                    benefitVendorTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveStartDate()));
                    benefitVendorTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveEndDate()));
                    benefitVendorTO.setTermsAndConditions(vendor.getTermAndConditions());
                    benefitVendorTO.setVendorLogo(vendor.getVendorLogo());
                    oTempTO.setBenefitVendorTO(benefitVendorTO);
                    sectorVendorOfferingTO.setVendorOfferingTemplateTO(oTempTO);
                    sectorVendorOfferingTOList.add(sectorVendorOfferingTO);

                    /**
                     * processing group Map for vendorOffering
                     */
                    if (sectorVendorOfferingmap.containsKey(offerSequence.getSysSector().getSectorId() + CoreConstants.AT_SPERATOR + offerSequence.getSysSector().getSectorLabel())) {
                        sectorVendorOfferingmap.get(offerSequence.getSysSector().getSectorId() + CoreConstants.AT_SPERATOR + offerSequence.getSysSector().getSectorLabel()).add(
                                oTempTO);
                    } else {
                        vendorOfferingTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
                        vendorOfferingTemplateTOList.add(oTempTO);
                        sectorVendorOfferingmap.put(offerSequence.getSysSector().getSectorId() + CoreConstants.AT_SPERATOR + offerSequence.getSysSector().getSectorLabel(),
                                vendorOfferingTemplateTOList);
                    }
                }
            }

            benefitResponseTO = new BenefitResponseTO();
            benefitResponseTO.setSectorVendorOfferingmap(sectorVendorOfferingmap);
            benefitResponseTO.setSectorVendorOfferingTOList(sectorVendorOfferingTOList);
            return benefitResponseTO;

        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @param benefitRequestTO
     * @return
     * @throws Exception
     */
    public boolean saveSectorVendorOfferSequence(BenefitRequestTO benefitRequestTO)
        throws Exception
    {
        boolean success = false;
        Map<Integer, Long> sectorVendorOfferSequenceMap = null;
        List<Integer> sequenceList = null;
        Set<Integer> sequenceSet = null;
        try {
            sectorVendorOfferSequenceMap = benefitRequestTO.getSectorVendorOfferSequenceMap();
            sequenceList = new ArrayList<Integer>();
            sequenceSet = sectorVendorOfferSequenceMap.keySet();
            for (int seq : sequenceSet) {
                sequenceList.add(seq);
            }
            offeringSequenceDS.saveSectorVendorOfferSequence(sequenceList, benefitRequestTO);
            success = true;
            try {
                altBenefitsCache.deleteSectorWiseVendorOfferingsRedisKey();
            } catch (Exception e) {
                LOGGER_.error(AltBenefitsKeyConstants.DELETE_SECTOR_WISE_REDIS_KEY);

            }

        } catch (Exception e) {
            throw e;
        }
        return success;
    }

    public void getAllSectorBasedOfferingList()
        throws Exception
    {
        List<HrSectorVendorOfferingSequence> sectorVendorOfferingTemplateList = null;
        try {
            sectorVendorOfferingTemplateList = offeringSequenceDS.getBenefitsSectorBasedList();
            processSectorOfferingTOList(sectorVendorOfferingTemplateList);

        } catch (Exception e) {
            throw e;
        }
    }

    public void processSectorOfferingTOList(List<HrSectorVendorOfferingSequence> sectorVendorOfferingTemplateList)
        throws Exception
    {
        List<BannerOfferingSequenceTO> sectorOfferingTOList = null;
        BannerOfferingSequenceTO bannerOfferingSequenceTO = null;
        Map<Integer, List<BannerOfferingSequenceTO>> sectorOfferSequenceMap = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        Date startDate = null;
        Date endDate = null;
        SysBenefitsVendor vendor = null;
        try {
            sectorOfferSequenceMap = new LinkedHashMap<Integer, List<BannerOfferingSequenceTO>>();
            sectorOfferingTOList = new ArrayList<BannerOfferingSequenceTO>();
            Date now = new Date();
            if (sectorVendorOfferingTemplateList != null && !sectorVendorOfferingTemplateList.isEmpty()) {
                for (HrSectorVendorOfferingSequence offeringSequence : sectorVendorOfferingTemplateList) {
                    SysVendorOfferingTemplate svO = offeringSequence.getSysVendorOfferingTemplate();
                    vendor = svO.getSysBenefitsVendor();
                    startDate = CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate());
                    endDate = CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate());
                    if (CommonUtilHelper.CompareDateWithNullCheck(now, endDate) <= 0 && CommonUtilHelper.CompareDateWithNullCheck(now, startDate) >= 0) {
                        /**
                         * 
                         */
                        bannerOfferingSequenceTO = new BannerOfferingSequenceTO();
                        bannerOfferingSequenceTO.setVendorOfferingID(svO.getVendorOfferingID());
                        bannerOfferingSequenceTO.setOfferingSequence(offeringSequence.getSequence());
                        bannerOfferingSequenceTO.setBanner(svO.getBanner());
                        bannerOfferingSequenceTO.setOfferingSequence(svO.getOfferingSequence());
                        bannerOfferingSequenceTO.setText(svO.getText());
                        bannerOfferingSequenceTO.setOfferingLink(svO.getOfferingLink());
                        bannerOfferingSequenceTO.setOfferTemplateName(svO.getOfferTemplateName());
                        bannerOfferingSequenceTO.setTermsAndCondition(vendor.getTermAndConditions());
                        bannerOfferingSequenceTO.setVendorLogo(vendor.getVendorLogo());
                        /**
                         * offering peak details
                         * 
                         */
                        if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                            if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                                peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                                bannerOfferingSequenceTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                                bannerOfferingSequenceTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                                bannerOfferingSequenceTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                                bannerOfferingSequenceTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                                bannerOfferingSequenceTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                                bannerOfferingSequenceTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                                bannerOfferingSequenceTO.setPeakDetails(true);
                            }

                        }
                        /**
                         * adding offering code details
                         */
                        bannerOfferingSequenceTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));

                        if (!sectorOfferSequenceMap.isEmpty() && sectorOfferSequenceMap.containsKey(offeringSequence.getSectorID())) {
                            sectorOfferSequenceMap.get(offeringSequence.getSectorID()).add(bannerOfferingSequenceTO);
                        } else {
                            sectorOfferingTOList = new ArrayList<BannerOfferingSequenceTO>();
                            sectorOfferingTOList.add(bannerOfferingSequenceTO);
                            sectorOfferSequenceMap.put(offeringSequence.getSectorID(), sectorOfferingTOList);
                        }

                    }
                }
            }
            altBenefitsCache.deleteSectorVendorOfferingsMap();
            altBenefitsCache.putSectorVendorOfferingsMap(sectorOfferSequenceMap);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param offeringCodesSet
     * @return
     * @throws Exception
     */

    public List<OfferingCodeTO> getOfferingCodeListBySet(Set<OfferingCode> offeringCodesSet)
        throws Exception
    {
        List<OfferingCodeTO> offeringCodeTOList = null;
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        OfferingCodeTO offeringCodeTO = null;
        SysOfferingCodeMaximumLimit codeMaximumLimit = null;
        try {
            offeringCodeTOList = new ArrayList<OfferingCodeTO>();
            if (offeringCodesSet != null && !offeringCodesSet.isEmpty()) {
                for (Iterator<OfferingCode> fieldIterator = offeringCodesSet.iterator(); fieldIterator.hasNext();) {
                    OfferingCode offeringCode = fieldIterator.next();
                    offeringCodeTO = new OfferingCodeTO();
                    offeringCodeTO.setOfferingCodeID(offeringCode.getOfferingCodeID());
                    offeringCodeTO.setOfferingCode(offeringCode.getOfferingCode());
                    offeringCodeTO.setAccessTypeID(offeringCode.getAccessTypeID());
                    offeringCodeTO.setActive(offeringCode.getActive());
                    offeringCodeTO.setVendorOfferingID(offeringCode.getVendorOfferingID());
                    offeringCodeTO.setAccessType(offeringCode.getSysContentType().getSysType());
                    offeringCodeTO.setLogicBased(offeringCode.getLogicBased());
                    offeringCodeTO.setMaximumLimitApplicable(offeringCode.getMaximumLimitApplicable());
                    if (offeringCode.getOfferingCodeStartDateID() != null) {
                        offeringCodeTO.setOfferingCodeStartDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeStartDateID()));
                        offeringCodeTO.setOfferingCodeStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeStartDate()));
                    }
                    if (offeringCode.getOfferingCodeEndDateID() != null) {
                        offeringCodeTO.setOfferingCodeEndDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeEndDateID()));
                        offeringCodeTO.setOfferingCodeEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeEndDate()));
                    }
                    /**
                     * offer code maximum limit details
                     */
                    if (offeringCode.getMaximumLimitApplicable()) {
                        if (offeringCode.getMapCodeMaximumLimits() != null && !offeringCode.getMapCodeMaximumLimits().isEmpty()) {
                            codeMaximumLimit = offeringCode.getMapCodeMaximumLimits().iterator().next();
                            offeringCodeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                            offeringCodeMaximumLimitTO.setOfferingCodeMaximumLimitID(codeMaximumLimit.getOfferingCodeMaximumLimitID());
                            offeringCodeMaximumLimitTO.setMaximumUsageCount(codeMaximumLimit.getMaximumUsageCount());
                            offeringCodeMaximumLimitTO.setMaximumUsageType(codeMaximumLimit.getMaximumUsageType().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageTypeID(codeMaximumLimit.getMaximumUsageTypeID());
                            offeringCodeMaximumLimitTO.setMaximumUsageDuration(codeMaximumLimit.getMaximumUsageDuration().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageDurationID(codeMaximumLimit.getMaximumUsageDurationID());
                        }
                        offeringCodeTO.setCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                    }
                    offeringCodeTOList.add(offeringCodeTO);
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return offeringCodeTOList;
    }

}