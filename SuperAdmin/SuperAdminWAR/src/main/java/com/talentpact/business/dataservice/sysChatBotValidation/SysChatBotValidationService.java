package com.talentpact.business.dataservice.sysChatBotValidation;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.model.SysChatBotValidation;
import com.talentpact.ui.sysChatBotValidation.to.SysChatBotValidationTO;


@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysChatBotValidationService extends CommonService {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationService.class);

	@EJB
	SysChatBotValidationDS sysChatBotValidationDS;

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveSysChatBotValidationType(
			SysChatBotValidationTO sysChatBotValidationTO) {
		try {
			sysChatBotValidationDS.saveSysChatBotValidation(sysChatBotValidationTO);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}

	}
	
	public List<SysChatBotValidationTO> getSysChatBotValidationByUiFormFieldID(Integer uiFormFieldID) {
		List<SysChatBotValidation> sysChatBotValidationList = null;
		List<SysChatBotValidationTO> sysChatBotValidationTOList = new ArrayList<SysChatBotValidationTO>();
		try {
			sysChatBotValidationList = sysChatBotValidationDS.getSysChatBotValidationByUiFormFieldID(uiFormFieldID);
			
			if (sysChatBotValidationList != null
					&& !sysChatBotValidationList.isEmpty()) {
				for (SysChatBotValidation sCBT : sysChatBotValidationList) {
					SysChatBotValidationTO sysChatBotValidationTO = new SysChatBotValidationTO();
					sysChatBotValidationTO.setSysValidationID(sCBT.getSysValidationID());
					sysChatBotValidationTO.setSysValidationTypeID(sCBT.getSysValidationTypeID());
					sysChatBotValidationTO.setParamValues(sCBT.getParamValues());
					sysChatBotValidationTO.setUiFormFieldID(sCBT.getUiFormFieldID());
					sysChatBotValidationTOList.add(sysChatBotValidationTO);
				}
				return sysChatBotValidationTOList;
			}
			
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
		return sysChatBotValidationTOList;
		
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void deleteSysBotValidation(
			Integer uiformFieldId) {
		try {
			sysChatBotValidationDS.deleteSysBotValidation(uiformFieldId);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}

	}
	
}// end of class