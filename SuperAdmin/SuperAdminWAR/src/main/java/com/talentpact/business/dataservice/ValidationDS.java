package com.talentpact.business.dataservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.model.SysValidation;

@Stateless
public class ValidationDS extends AbstractDS<SysValidation>
{

    public ValidationDS()
    {
        super(SysValidation.class);
    }

    public List<SysValidation> getSysValidationList(boolean groupValidation)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder("select STY from SysValidation STY where STY.groupValidation=:groupValidation  ");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("groupValidation", groupValidation);
            return query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
    }


}
