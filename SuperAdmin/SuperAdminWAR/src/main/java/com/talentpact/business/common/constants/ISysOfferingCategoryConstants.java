package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysOfferingCategoryConstants
{

    public static String SYSOFFERINGCATEGORY_ADD_DIALOG_HIDE = "PF('sysOfferingCategoryAddModal').hide();";

    public static String SYSOFFERINGCATEGORY_EDIT_DIALOG_HIDE = "PF('sysOfferingCategoryEditModal').hide();";

    public static String SYSOFFERINGCATEGORY_SAVE_ERROR_MSG = "Error occured while saving SysOfferingCategory.";

    public static String SYSOFFERINGCATEGORY_UPDATE_ERROR_MSG = "Error occured while updating SysOfferingCategory.";

    public static String SYSOFFERINGCATEGORY_SAVE_SUCCESS_MSG = "SysOfferingCategory saved successfully.";

    public static String SYSOFFERINGCATEGORY_UPDATE_SUCCESS_MSG = "SysOfferingCategory updated successfully.";

    public static String SYSOFFERINGCATEGORY_DATATABLE_RESET = "PF('sysOfferingCategoryDataTable').clearFilters()";

    public static String SYSOFFERINGCATEGORY_PAGINATION_DATATABLE_RESET = "PF('sysOfferingCategoryDataTable').getPaginator().setPage(0);";

    public static String SYSOFFERINGCATEGORY_DUPLICATE_ERROR_MSG = "SysOfferingCategory already present in system.";
}
