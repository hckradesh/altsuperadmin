/**
 * 
 */
package com.talentpact.business.dataservice.sysPolicy;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysPolicyService extends CommonService
{

    @EJB
    SysPolicyDS sysPolicyDS;

    public List<SysPolicyTO> getSysPolicyList()
        throws Exception
    {
        List<SysPolicyTO> list = null;
        SysPolicyTO sysPolicyTO = null;
        List<Object[]> sysPolicyList = null;

        try {
            sysPolicyList = sysPolicyDS.getSysPolicyList();
            list = new ArrayList<SysPolicyTO>();
            for (Object[] sysPolicy : sysPolicyList) {
                sysPolicyTO = new SysPolicyTO();
                sysPolicyTO.setModuleID((Integer) sysPolicy[0]);
                sysPolicyTO.setModuleName((String) sysPolicy[1]);
                sysPolicyTO.setEntityID((Integer) sysPolicy[2]);
                sysPolicyTO.setEntityName((String) sysPolicy[3]);
                sysPolicyTO.setPolicyCode((String) sysPolicy[4]);
                sysPolicyTO.setPolicyName((String) sysPolicy[5]);
                sysPolicyTO.setPolicyLabel((String) sysPolicy[6]);
                sysPolicyTO.setSysPolicyID((Long) sysPolicy[7]);
                sysPolicyTO.setPolicyTypeID((Integer) sysPolicy[8]);
                sysPolicyTO.setPolicyType((String) sysPolicy[9]);
                list.add(sysPolicyTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    @SuppressWarnings("null")
    public List<SysPolicyTO> getNewSysModuleList()
        throws Exception
    {
        List<SysPolicyTO> list = null;
        SysPolicyTO sysPolicyTO = null;
        List<Object[]> sysModuleList = null;
        try {
            sysModuleList = sysPolicyDS.getNewSysModuleList();
            list = new ArrayList<SysPolicyTO>();
            for (Object[] sysModule : sysModuleList) {
                sysPolicyTO = new SysPolicyTO();
                if (sysModule[0] != null) {
                    sysPolicyTO.setModuleID((Integer) sysModule[0]);
                    sysPolicyTO.setModuleName((String) sysModule[1]);
                    list.add(sysPolicyTO);
                }
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    @SuppressWarnings("null")
    public List<SysPolicyTO> getNewSysEntityList()
        throws Exception
    {
        List<SysPolicyTO> list = null;
        SysPolicyTO sysPolicyTO = null;
        List<Object[]> sysEntityList = null;
        try {
            sysEntityList = sysPolicyDS.getNewSysEntityList();
            list = new ArrayList<SysPolicyTO>();
            for (Object[] sysEntity : sysEntityList) {
                sysPolicyTO = new SysPolicyTO();
                sysPolicyTO.setEntityID((Integer) sysEntity[0]);
                sysPolicyTO.setEntityName((String) sysEntity[1]);
                list.add(sysPolicyTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveSysPolicy(SysPolicyTO sysPolicyTO)
        throws Exception
    {
        try {
            sysPolicyDS.saveSysPolicy(sysPolicyTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSysPolicy(SysPolicyTO sysPolicyTO)
    {
        sysPolicyDS.updateSysPolicy(sysPolicyTO);
    }


}