/**
 * 
 */
package com.talentpact.business.authorization.constants;

/**
 * @author radhamadhab.dalai
 *
 */
public interface IAuthMessages
{
    public static final String LOGIN_FAILURE_MESSAGE = "Login Failed. Please verify your username and password, or contact your system administrator.";

}
