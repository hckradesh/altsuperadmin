package com.talentpact.business.dataservice.sysPinCode;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.organization.SysPinCode;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysPinCode.to.PostOfficeTO;
import com.talentpact.ui.sysPinCode.to.SysPinCodeTO;

/**
 * 
 * @author prachi.bansal
 *
 */

@Stateless
public class SysPinCodeDS extends AbstractDS<SysPinCode>
{
    @Inject
    UserSessionBean userSessionBean;
    
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPinCodeDS.class);

    public SysPinCodeDS() {
        super(SysPinCode.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysPinCodeList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysPinCodeTO> returnList= null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append("select sp.pinCodeID , sp.pinCode ,  sp.cityID,sp.postOfficeName , sc.Name from SysPinCode sp , SysCity sc where sc.cityID = sp.cityID  ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;      
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysCityList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append(" select sc.cityID , sc.name from SysCity sc order by sc.name ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public void saveSysPincode(SysPinCodeTO sysPinCodeTO) {
        SysPinCode sysPinCode=null;
       // List<PostOfficeTO> postOffice=sysPinCodeTO.getPostOfficeList();
        try{
             sysPinCode=new SysPinCode();
             sysPinCode.setCityID(sysPinCodeTO.getCityID());
             sysPinCode.setPinCode(sysPinCodeTO.getPinCode());
             sysPinCode.setPostOfficeName(sysPinCodeTO.getPostOfficeName());
             getEntityManager("Talentpact").persist(sysPinCode);      
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
      
        
    }

    @SuppressWarnings("unchecked")
    public List<SysPinCode> getPostOfficeList(String pinCode) {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append(" from SysPinCode sp where sp.pinCode=:pinCode");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("pinCode", pinCode);
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public void updateSysPinCode(SysPinCodeTO sysPinCodeTO) {
        
        SysPinCode sysPinCode=null;
        List<PostOfficeTO> postOffice=sysPinCodeTO.getPostOfficeList();
        try{
            sysPinCode=new SysPinCode();
            sysPinCode=findSysPinCode(sysPinCodeTO.getPinCodeID());
            sysPinCode.setCityID(sysPinCodeTO.getCityID());
            sysPinCode.setPostOfficeName(sysPinCodeTO.getPostOfficeName());
            sysPinCode.setPinCode(sysPinCodeTO.getPinCode());
            getEntityManager("Talentpact").merge(sysPinCode);
            getEntityManager("TalentPact").flush();
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    private SysPinCode findSysPinCode(Long pinCodeID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPinCode> list=null;
        SysPinCode sysPinCode=null;
        try{
            sysPinCode=new SysPinCode();
            hqlQuery=new StringBuilder();
            hqlQuery.append(" from SysPinCode sp where sp.pinCodeID=:pinCodeID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("pinCodeID", pinCodeID);
            list=query.getResultList();
            if (list != null && !list.isEmpty()) {
                sysPinCode = list.get(0);
            }
            return sysPinCode;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
       
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getUpdatePinCodeList(Long pinCodeID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysPinCodeTO> returnList= null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append("select sp.pinCodeID , sp.pinCode ,  sp.cityID,sp.postOfficeName , sc.Name from SysPinCode sp , SysCity sc where sc.cityID = sp.cityID and sp.pinCodeID!=:pinCodeID ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            query.setParameter("pinCodeID",pinCodeID);
            resultList = query.getResultList();
            return resultList;      
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

   
    }