package com.talentpact.business.sysEntityColumn.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.sysEntity.SysEntityDS;
import com.talentpact.model.SysEntity;
import com.talentpact.model.SysEntityColumn;
import com.talentpact.ui.sysEntityColumn.common.transport.input.SysEntityColumnRequestTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.converter.SysEntityColumnConverter;
import com.talentpact.ui.sysEntityColumn.dataservice.DS.SysEntityColumnDS;
import com.talentpact.ui.sysEntityColumn.exception.SysEntityColumnException;

/**
 * @author raghvendra.mishra
 */

@Stateless
public class SysEntityColumnService extends CommonService {
	
	private static final Logger LOGGER = Logger.getLogger(SysEntityColumnService.class.toString());

	@EJB
	SysEntityColumnDS sysEntityColumnDS;
	
	@EJB
	SysEntityDS sysEntityDS;
	
	/*
	 * method to save a SysEntityColumn 
	 * @param serviceRequest
	 * @author raghvendra.mishra
	 */
	public ServiceResponse saveOrUpdateSysEntityColumn(ServiceRequest serviceRequest) throws SysEntityColumnException {
		SysEntityColumnRequestTO sysEntityColumnRequestTO = null;
		ServiceResponse serviceResponse = null;
		SysEntityColumn sysEntityColumn = null;
		SysEntity sysEntity = null;
		SysEntityColumnConverter sysEntityColumnConverter = null;
		List<SysEntityColumnResponseTO> result = null;
		List<SysEntityColumn> sysEntityColumnList = null;
		try {
			sysEntityColumnList = new ArrayList<SysEntityColumn>();
			sysEntityColumnConverter = new SysEntityColumnConverter();
			serviceResponse = new ServiceResponse();
			sysEntityColumnRequestTO = (SysEntityColumnRequestTO) serviceRequest.getRequestTransferObjectRevised();
            /* Save SysEntityColumn*/
			/*sysEntity = sysEntityDS.findSysEntity(sysEntityColumnRequestTO.getSysEntity().getEntityId());
			sysEntityColumnRequestTO.setSysEntity(sysEntity);*/
			sysEntityColumn =  sysEntityColumnDS.saveOrUpdateSysEntityColumn(sysEntityColumnRequestTO);
			sysEntityColumnList.add(sysEntityColumn);
			result = sysEntityColumnConverter.convertSysEntityColumnToDataTO(sysEntityColumnList);
			serviceResponse.setResponseTransferObject(result.get(0));
        } catch (Exception ex) {
        	LOGGER.error("", ex);
        }
        return serviceResponse;
	}

	public ServiceResponse getSysEntityColumnsByEntityID(ServiceRequest serviceRequest) {
		SysEntityColumnRequestTO sysEntityColumnRequestTO = null;
		ServiceResponse serviceResponse = null;
		SysEntityColumn sysEntityColumn = null;
		SysEntityColumnConverter sysEntityColumnConverter = null;
		List<SysEntityColumnResponseTO> result = null;
		List<SysEntityColumn> sysEntityColumnList = null;
		sysEntityColumnList = new ArrayList<SysEntityColumn>();
		sysEntityColumnConverter = new SysEntityColumnConverter();
		serviceResponse = new ServiceResponse();
		SysEntity sysEntity=null;
		try{
		sysEntityColumnRequestTO = (SysEntityColumnRequestTO) serviceRequest.getRequestTransferObjectRevised();
		sysEntity=sysEntityDS.findSysEntity(sysEntityColumnRequestTO.getSysEntityId());
		sysEntityColumnList = sysEntityColumnDS.getSysEntityColumnsByEntityID(sysEntity);
		result = sysEntityColumnConverter.convertSysEntityColumnToDataTO(sysEntityColumnList);
		serviceResponse.setResponseTransferObjectObjectList(result);
		}catch (Exception ex) {
        	LOGGER.error("", ex);
        }
		return serviceResponse;
	}	
}
