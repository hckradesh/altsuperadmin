package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.to.OperatorTo;

@FacesConverter(value = "operatorToConverter", forClass = OperatorTo.class)
public class OperatorToConverter implements Converter, Serializable {

	private static final long serialVersionUID = 4551384433724833123L;
	private static final Logger LOGGER = Logger.getLogger(OperatorToConverter.class);
	private static final String LABEL_CONSTANT = "label:";
	private static final String VALUE_CONSTANT = ",value:";
	private static final String SHOWINPUTVALUE_CONSTANT=",showInputValue:";
	private static final String NUMBEROFREQUIREDINPUTS_CONSTANT=",numberOfRequiredInputs:";
	private static final String SINGLESELECT_CONSTANT=",singleSelect:";
	private static final String OPERATORID_CONSTANT=",operatorId:";
	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && !value.equals("")) {
            OperatorTo item = new OperatorTo();
            try {
                if (value.contains(LABEL_CONSTANT)){
                    item.setOperatorLabel(value.substring(value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length(), 
                    		value.indexOf(VALUE_CONSTANT, value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length())));
                }
                if (value.contains(VALUE_CONSTANT)){
                    item.setOperatorValue(value.substring(value.indexOf(VALUE_CONSTANT) + VALUE_CONSTANT.length(), 
                    		value.indexOf(SHOWINPUTVALUE_CONSTANT, value.indexOf(VALUE_CONSTANT) + VALUE_CONSTANT.length())));
                }
                if (value.contains(SHOWINPUTVALUE_CONSTANT)){
                    item.setShowInputValue(Boolean.valueOf(value.substring(value.indexOf(SHOWINPUTVALUE_CONSTANT) + SHOWINPUTVALUE_CONSTANT.length(), 
                    		value.indexOf(NUMBEROFREQUIREDINPUTS_CONSTANT, value.indexOf(SHOWINPUTVALUE_CONSTANT) + SHOWINPUTVALUE_CONSTANT.length()))));
                }
                if (value.contains(NUMBEROFREQUIREDINPUTS_CONSTANT)){
                    item.setNumberOfRequiredInputs(Integer.valueOf(value.substring(value.indexOf(NUMBEROFREQUIREDINPUTS_CONSTANT) + NUMBEROFREQUIREDINPUTS_CONSTANT.length(), 
                    		value.indexOf(SINGLESELECT_CONSTANT, value.indexOf(NUMBEROFREQUIREDINPUTS_CONSTANT) + NUMBEROFREQUIREDINPUTS_CONSTANT.length()))));
                }
                if (value.contains(SINGLESELECT_CONSTANT)){
                    item.setSingleSelect(Boolean.valueOf(value.substring(value.indexOf(SINGLESELECT_CONSTANT) + SINGLESELECT_CONSTANT.length(), 
                    		value.indexOf(OPERATORID_CONSTANT, value.indexOf(SINGLESELECT_CONSTANT) + SINGLESELECT_CONSTANT.length()))));
                }
                if (value.contains(OPERATORID_CONSTANT)){
                    item.setOperatorId(Integer.valueOf(value.substring(value.indexOf(OPERATORID_CONSTANT) + OPERATORID_CONSTANT.length(),value.length())));
                }
                return item;
            } catch (Exception e) {
            	LOGGER.error("Operator TO Converter for value : " + value + " error.",e);
            }
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof OperatorTo) {
            return LABEL_CONSTANT + ((OperatorTo) value).getOperatorLabel() + VALUE_CONSTANT + ((OperatorTo) value).getOperatorValue() +
            		SHOWINPUTVALUE_CONSTANT+ ((OperatorTo) value).isSingleSelect() + NUMBEROFREQUIREDINPUTS_CONSTANT + ((OperatorTo) value).getNumberOfRequiredInputs() +
            		SINGLESELECT_CONSTANT + ((OperatorTo) value).isSingleSelect() + OPERATORID_CONSTANT + ((OperatorTo) value).getOperatorId();
        } else
            return "";
    }
}
