package com.talentpact.business.dataservice.timeDimension;



import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.TimeDimension;

/**
 * @author javed.ali
 */

@Stateless
public class TimeDimensionDS extends AbstractDS<TimeDimension>
{

    private static final Logger LOGGER_ = Logger.getLogger(TimeDimensionDS.class.getName());

    public TimeDimensionDS()
    {
        super(TimeDimension.class);
    }

    @SuppressWarnings("unchecked")
    public TimeDimension getTimeDimensionFromDate(Date date)
    {
        Query query = null;
        TimeDimension timeDimension = null;
        List<TimeDimension> timeDimensions = null;
        try {
            if(date!=null){
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);
            query = getEntityManager("Talentpact").createNamedQuery("TimeDimension.findTimeDimensionFromDate");
            query.setParameter("day", gregorianCalendar.get(Calendar.DAY_OF_MONTH));
            query.setParameter("month", gregorianCalendar.get(Calendar.MONTH) + 1);
            query.setParameter("year", gregorianCalendar.get(Calendar.YEAR));
            timeDimensions = query.getResultList();
            if (timeDimensions != null && timeDimensions.size() > 0) {
                timeDimension = timeDimensions.get(0);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {

        }
        return timeDimension;
    }

    /**
     * 
     * @param timeDimensionId
     * @return
     * @throws Exception
     */
    public TimeDimension getTimeDimensionFrmId(int timeDimensionId)
        throws Exception
    {
        TimeDimension timeDimension = null;
        try {
            timeDimension = getEntityManager().find(TimeDimension.class, timeDimensionId);
        } catch (Exception e) {
            throw e;
        }
        return timeDimension;
    }

    public TimeDimension convertDateToTimeDimension(Date proxyDate)
        throws Exception
    {
        TimeDimension proxyTD = null;
        try {

            String query = " select td from TimeDimension td where dayOfMonth=:dom and monthOfYear=:moy and theYear=:ty";
            GregorianCalendar gr = new GregorianCalendar();
            gr.setTime(proxyDate);
            proxyTD = (TimeDimension) getEntityManager().createQuery(query).setParameter("dom", gr.get(Calendar.DAY_OF_MONTH)).setParameter("moy", gr.get(Calendar.MONTH) + 1)
                    .setParameter("ty", gr.get(Calendar.YEAR)).getSingleResult();
            return proxyTD;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER_.info("exception occured while converting date to timesdimention");
            throw new Exception("exception occured while converting date to timesdimention");
        } finally {
            proxyTD = null;
        }
    }

}