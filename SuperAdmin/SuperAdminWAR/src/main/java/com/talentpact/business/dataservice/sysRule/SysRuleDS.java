/**
 * 
 */
package com.talentpact.business.dataservice.sysRule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysRule;
import com.talentpact.model.configuration.SysContentType;
import com.talentpact.ui.rule.to.RuleListRequestTO;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 * 
 */
@Stateless
public class SysRuleDS extends AbstractDS<SysRule>
{

    public SysRuleDS()
    {
        super(SysRule.class);
    }

    public List<RuleResponseTO> getAllRules()
        throws Exception
    {
        List<RuleResponseTO> ruleResponseTOList = null;
        List<Object[]> resultList = null;
        List<SysRule> ruleList = null;
        StringBuilder hqlQuery = null;
        Query query = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("from SysRule");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            ruleList = query.getResultList();
            if (ruleList != null && !ruleList.isEmpty()) {
                ruleResponseTOList = getRuleListRevised(ruleList);
            }
        } catch (Exception ex) {
            ruleResponseTOList = null;
            throw ex;
        }
        return ruleResponseTOList;
    }

    @SuppressWarnings("unchecked")
    public List<RuleResponseTO> getSysRuleListByTenant()
        throws Exception
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<RuleResponseTO> ruleTypelist = null;
        int tenantID = 1;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sr.ruleID,sr.ruleMethod,sysContentType.sysTypeId,sr.ruleClass,sr.ruleClassName,sr.methodParameters,sr.methodReturnType,sr.classPathConvention,sr.description,sysContentType.sysType ");
            hqlQuery.append("from  SysRule sr ");
            hqlQuery.append("left join sr.sysContentType sysContentType ");
            hqlQuery.append("where  sr.sysTenantId = :tenantID order by sr.ruleMethod ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("tenantID", tenantID);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                ruleTypelist = getRuleList(resultList);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
        return ruleTypelist;

    }

    public static List<RuleResponseTO> getRuleListRevised(List<SysRule> resultList)
        throws Exception
    {
        List<RuleResponseTO> ruleList = null;
        RuleResponseTO responseTO = null;
        String ruleMethod = null;
        Integer ruleID = null;
        Integer ruleTypeID = null;
        String ruleClass = null;
        String methodParameters = null;
        String methodReturnType = null;
        String classPathConvention = null;
        String ruleClassName = null;
        String description = null;
        String sysType = null;

        try {
            ruleList = new ArrayList<RuleResponseTO>();
            for (SysRule object : resultList) {
                responseTO = new RuleResponseTO();

                if (object.getRuleID() != null) {

                    responseTO.setRuleID(object.getRuleID());
                }
                if (object.getRuleMethod() != null) {

                    responseTO.setRuleMethod(object.getRuleMethod());
                }


                if (object.getRuleClass() != null) {

                    responseTO.setRuleClass(object.getRuleClass());
                }

                if (object.getRuleClassName() != null) {

                    responseTO.setRuleClassName(object.getRuleClassName());
                }

                if (object.getMethodParameters() != null) {

                    responseTO.setMethodParameters(object.getMethodParameters());
                }

                if (object.getMethodReturnType() != null) {

                    responseTO.setMethodReturnType(object.getMethodReturnType());
                }


                ruleList.add(responseTO);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            responseTO = null;
        }
        return ruleList;

    }

    public static List<RuleResponseTO> getRuleList(List<Object[]> resultList)
        throws Exception
    {
        List<RuleResponseTO> ruleList = null;
        RuleResponseTO responseTO = null;
        String ruleMethod = null;
        Integer ruleID = null;
        Integer ruleTypeID = null;
        String ruleClass = null;
        String methodParameters = null;
        String methodReturnType = null;
        String classPathConvention = null;
        String ruleClassName = null;
        String description = null;
        String sysType = null;

        try {
            ruleList = new ArrayList<RuleResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new RuleResponseTO();

                if (object[0] != null) {
                    ruleID = (Integer) object[0];
                    responseTO.setRuleID(ruleID);
                }
                if (object[1] != null) {
                    ruleMethod = (String) object[1];
                    responseTO.setRuleMethod(ruleMethod);
                }

                if (object[2] != null) {
                    ruleTypeID = (Integer) object[2];
                    responseTO.setRuleTypeID(ruleTypeID);
                }

                if (object[3] != null) {
                    ruleClass = (String) object[3];
                    responseTO.setRuleClass(ruleClass);
                }

                if (object[4] != null) {
                    ruleClassName = (String) object[4];
                    responseTO.setRuleClassName(ruleClassName);
                }

                if (object[5] != null) {
                    methodParameters = (String) object[5];
                    responseTO.setMethodParameters(methodParameters);
                }

                if (object[6] != null) {
                    methodReturnType = (String) object[6];
                    responseTO.setMethodReturnType(methodReturnType);
                }

                if (object[7] != null) {
                    classPathConvention = (String) object[7];
                    responseTO.setClassPathConvention(classPathConvention);
                }

                if (object[8] != null) {
                    description = (String) object[8];
                    responseTO.setDescription(description);
                }

                if (object[9] != null) {
                    sysType = (String) object[9];
                    responseTO.setSysType(sysType);
                }

                ruleList.add(responseTO);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            responseTO = null;
        }
        return ruleList;
    }

    @SuppressWarnings("unchecked")
    public List<RuleResponseTO> getRuleTypeIDList()
        throws Exception
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<RuleResponseTO> ruleTypeIDlist = null;
        int tenantID = 1;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sct.sysTypeId,sct.sysType ");
            hqlQuery.append("from  SysContentType sct ");
            hqlQuery.append("where  sct.sysTenantId = :tenantID order by sct.sysType ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("tenantID", tenantID);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                ruleTypeIDlist = getRuleTypeIDList(resultList);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
        return ruleTypeIDlist;

    }

    public static List<RuleResponseTO> getRuleTypeIDList(List<Object[]> resultList)
    {
        List<RuleResponseTO> ruleTypeIDList = null;
        RuleResponseTO responseTO = null;
        String sysType = null;
        Integer ruleTypeID = null;
        try {
            ruleTypeIDList = new ArrayList<RuleResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new RuleResponseTO();

                if (object[0] != null) {
                    ruleTypeID = (Integer) object[0];
                    responseTO.setRuleTypeID(ruleTypeID);
                }
                if (object[1] != null) {
                    sysType = (String) object[1];
                    responseTO.setSysType(sysType);
                }
                ruleTypeIDList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return ruleTypeIDList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void setSysRuleListByTenant(RuleListRequestTO ruleTO)
        throws Exception
    {
        SysRule sysRule = null;
        int TenantID = 1;
        try {
            sysRule = findSysRule(ruleTO);
            Date date = new Date();
            if (sysRule == null) {
                sysRule = new SysRule();
                sysRule.setSysTenantId(TenantID);
                if (ruleTO.getRuleMethod() != null && !ruleTO.getRuleMethod().isEmpty()) {
                    sysRule.setRuleMethod(ruleTO.getRuleMethod());
                } else {
                    sysRule.setRuleMethod(null);
                }
                if (ruleTO.getClassPathConvention() != null && !ruleTO.getClassPathConvention().isEmpty()) {
                    sysRule.setClassPathConvention(ruleTO.getClassPathConvention());
                } else {
                    sysRule.setClassPathConvention(null);
                }
                if (ruleTO.getDescription() != null && !ruleTO.getDescription().isEmpty()) {
                    sysRule.setDescription(ruleTO.getDescription());
                } else {
                    sysRule.setDescription(null);
                }
                if (ruleTO.getMethodParameters() != null && !ruleTO.getMethodParameters().isEmpty()) {
                    sysRule.setMethodParameters(ruleTO.getMethodParameters());
                } else {
                    sysRule.setMethodParameters(null);
                }
                if (ruleTO.getMethodReturnType() != null && !ruleTO.getMethodReturnType().isEmpty()) {
                    sysRule.setMethodReturnType(ruleTO.getMethodReturnType());
                } else {
                    sysRule.setMethodReturnType(null);
                }
                if (ruleTO.getRuleClass() != null && !ruleTO.getRuleClass().isEmpty()) {
                    sysRule.setRuleClass(ruleTO.getRuleClass());
                } else {
                    sysRule.setRuleClass(null);
                }
                if (ruleTO.getRuleClassName() != null && !ruleTO.getRuleClassName().isEmpty()) {
                    sysRule.setRuleClassName(ruleTO.getRuleClassName());
                } else {
                    sysRule.setRuleClassName(null);
                }

                if (ruleTO.getRuleTypeID() != 0) {
                    SysContentType sysContentType=getEntityManager("TalentPact").find(SysContentType.class, ruleTO.getRuleTypeID());
                    sysRule.setSysContentType(sysContentType);
                } else {
                    sysRule.setSysContentType(null);
                }

                getEntityManager("TalentPact").persist(sysRule);
            } else {
                if (ruleTO.getRuleMethod() != null) {
                    sysRule.setRuleMethod(ruleTO.getRuleMethod());
                }
                sysRule.setSysTenantId(TenantID);
                if (ruleTO.getClassPathConvention() != null && !ruleTO.getClassPathConvention().isEmpty()) {
                    sysRule.setClassPathConvention(ruleTO.getClassPathConvention());
                } else {
                    sysRule.setClassPathConvention(null);
                }
                if (ruleTO.getDescription() != null && !ruleTO.getDescription().isEmpty()) {
                    sysRule.setDescription(ruleTO.getDescription());
                } else {
                    sysRule.setDescription(null);
                }
                if (ruleTO.getMethodParameters() != null && !ruleTO.getMethodParameters().isEmpty()) {
                    sysRule.setMethodParameters(ruleTO.getMethodParameters());
                } else {
                    sysRule.setMethodParameters(null);
                }
                if (ruleTO.getMethodReturnType() != null && !ruleTO.getMethodReturnType().isEmpty()) {
                    sysRule.setMethodReturnType(ruleTO.getMethodReturnType());
                } else {
                    sysRule.setMethodReturnType(null);
                }
                if (ruleTO.getRuleClass() != null && !ruleTO.getRuleClass().isEmpty()) {
                    sysRule.setRuleClass(ruleTO.getRuleClass());
                } else {
                    sysRule.setRuleClass(null);
                }
                if (ruleTO.getRuleClassName() != null && !ruleTO.getRuleClassName().isEmpty()) {
                    sysRule.setRuleClassName(ruleTO.getRuleClassName());
                } else {
                    sysRule.setRuleClassName(null);
                }
                if (ruleTO.getRuleTypeID() != 0) {
                    SysContentType sysContentType=getEntityManager("TalentPact").find(SysContentType.class, ruleTO.getRuleTypeID());
                    sysRule.setSysContentType(sysContentType);
                } else {
                    sysRule.setSysContentType(null);
                }
                getEntityManager("TalentPact").merge(sysRule);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private SysRule findSysRule(RuleListRequestTO ruleTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysRule> ruleList = null;
        SysRule result = null;
        int tenantID = 1;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sr from SysRule  sr  where sr.ruleID=:ruleID  and sr.sysTenantId = :tenantID ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("ruleID", ruleTO.getRuleID());
            query.setParameter("tenantID", tenantID);
            ruleList = query.getResultList();
            if (ruleList != null && ruleList.size() > 0) {
                result = ruleList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

}
