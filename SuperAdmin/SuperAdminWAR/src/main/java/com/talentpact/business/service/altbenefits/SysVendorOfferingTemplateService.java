/**
 * 
 */
package com.talentpact.business.service.altbenefits;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.alt.altbenefits.to.BannerOfferingSequenceTO;
import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.common.helper.CommonUtilHelper;
import com.alt.role.constants.IRoleConstants;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.HrVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.OfferingCodeDS;
import com.talentpact.business.dataservice.altbenefits.SysOfferingCodeMaximumLimitDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingPeakDetailDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsKeyConstants;
import com.talentpact.business.dataservice.timeDimension.TimeDimensionDS;
import com.talentpact.model.altbenefit.OfferingCode;
import com.talentpact.model.altbenefit.SysBenefitsVendor;
import com.talentpact.model.altbenefit.SysOfferingCodeMaximumLimit;
import com.talentpact.model.altbenefit.SysVendorOfferingPeakDetail;
import com.talentpact.model.altbenefit.SysVendorOfferingTemplate;
import com.talentpact.ui.altbenefits.transport.input.SaveOfferTempReqTO;
import com.talentpact.ui.altbenefits.transport.input.VendorOfferTemplateRequestTO;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class SysVendorOfferingTemplateService extends CommonService
{
    @EJB
    TimeDimensionDS timeDimensionDS;

    @EJB
    OfferingCodeDS offeringCodeDS;

    @EJB
    SysVendorOfferingPeakDetailDS sysVendorOfferingPeakDetailDS;

    @EJB
    SysVendorOfferingTemplateDS offeringTemplateDS;

    @EJB
    HrVendorOfferingTemplateDS hrVendorOfferingTemplateDS;

    @EJB
    SysOfferingCodeMaximumLimitDS codeMaximumLimitDS;


    @EJB
    AltBenefitsCache altBenefitsCache;

    @EJB
    HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

    @EJB
    SysSectorVendorOfferingSequenceService sysSectorVendorOfferingSequenceService;


    public List<VendorOfferingTemplateTO> getOfferingTemplateList(VendorOfferTemplateRequestTO vOffTemRequestTO)
        throws Exception
    {
        List<SysVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        List<VendorOfferingTemplateTO> offeringTemplateTOList = null;
        offeringTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            sysVendorOfferingTemplateList = offeringTemplateDS.getOfferingTemplateList(vOffTemRequestTO.getSysBenefitVendorTO().getVendorID());
            if (sysVendorOfferingTemplateList != null && !sysVendorOfferingTemplateList.isEmpty()) {
                for (SysVendorOfferingTemplate svO : sysVendorOfferingTemplateList) {
                    VendorOfferingTemplateTO oTempTO = new VendorOfferingTemplateTO();
                    oTempTO.setBanner(svO.getBanner());
                    oTempTO.setVendorOfferingID(svO.getVendorOfferingID());
                    oTempTO.setVendorID(svO.getVendorID());
                    oTempTO.setOfferingTypeID(svO.getOfferingTypeID());
                    oTempTO.setOfferingSequence(svO.getOfferingSequence());
                    oTempTO.setOfferingType(svO.getSysOfferingType().getOfferingType());
                    oTempTO.setText(svO.getText());
                    oTempTO.setOfferingLink(svO.getOfferingLink());
                    oTempTO.setOfferTemplateName(svO.getOfferTemplateName());
                    oTempTO.setActive(svO.getActive());
                    if (svO.getContractSignedDateID() != null) {
                        oTempTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractSignedDateID()));
                    }
                    if (svO.getContractStartDateID() != null) {
                        oTempTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractStartDateID()));
                    }
                    if (svO.getContractEndDateID() != null) {
                        oTempTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractEndDateID()));
                    }
                    if (svO.getEffectiveStartDateID() != null) {
                        oTempTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveStartDateID()));
                    }
                    if (svO.getEffectiveEndDateID() != null) {
                        oTempTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveEndDateID()));
                    }
                    oTempTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractSignedDate()));
                    oTempTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractStartDate()));
                    oTempTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractEndDate()));
                    oTempTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveStartDate()));
                    oTempTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveEndDate()));

                    /**
                     * offering peak details
                     * 
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        oTempTO.setPeakBasedOffering(svO.getOfferingPeakBased());
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            peakRecurringDetailTO = new PeakRecurringDetailTO();
                            peakRecurringDetailTO.setPeakTimeRecurringTypeLabel(peakRecurringDetail.getSysPeakRecurringType().getSysType());
                            peakRecurringDetailTO.setPeakTimeRecurringTypeID(peakRecurringDetail.getPeakTimeRecurringTypeID().intValue());
                            peakRecurringDetailTO.setVendorOfferingPeakDetailID(peakRecurringDetail.getVendorOfferingPeakDetailID());
                            peakRecurringDetailTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            peakRecurringDetailTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            peakRecurringDetailTO.setPeakStartDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakStartDateTime()));
                            peakRecurringDetailTO.setPeakEndDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakEndDateTime()));
                            peakRecurringDetailTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            peakRecurringDetailTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            peakRecurringDetailTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            peakRecurringDetailTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                        }
                        oTempTO.setPeakRecurringDetailTO(peakRecurringDetailTO);
                    }
                    /**
                     * Offering Code Details
                     */
                    oTempTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));

                    offeringTemplateTOList.add(oTempTO);
                }
            }

            return offeringTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveVendorOfferTemplate(SaveOfferTempReqTO saveOfferTempReqTO)
        throws Exception
    {

        VendorOfferingTemplateTO offeringTemplateTO = saveOfferTempReqTO.getOfferingTemplateTO();
        SysVendorOfferingPeakDetail offeringPeakDetail = null;
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        Long vendorOfferingID = null;
        try {
            peakRecurringDetailTO = saveOfferTempReqTO.getPeakRecurringDetailTO();

            offeringTemplateTO.setContractStartDateId(timeDimensionDS.getTimeDimensionFromDate(offeringTemplateTO.getContractStartDate()).getTimeDimensionId());
            offeringTemplateTO.setContractSignedDateId(timeDimensionDS.getTimeDimensionFromDate(offeringTemplateTO.getContractSignedDate()).getTimeDimensionId());
            if (offeringTemplateTO.getContractEndDate() != null) {
                offeringTemplateTO.setContractEndDateId(timeDimensionDS.getTimeDimensionFromDate(offeringTemplateTO.getContractEndDate()).getTimeDimensionId());
            }
            if (offeringTemplateTO.getEffectiveStartDate() != null) {
                offeringTemplateTO.setEffectiveStartDateId(timeDimensionDS.getTimeDimensionFromDate(offeringTemplateTO.getEffectiveStartDate()).getTimeDimensionId());
            }
            if (offeringTemplateTO.getEffectiveEndDate() != null) {
                offeringTemplateTO.setEffectiveEndDateId(timeDimensionDS.getTimeDimensionFromDate(offeringTemplateTO.getEffectiveEndDate()).getTimeDimensionId());
            }

            saveOfferTempReqTO.setOfferingTemplateTO(offeringTemplateTO);
            vendorOfferingID = offeringTemplateDS.saveVendorOfferTemplate(saveOfferTempReqTO);
            if (saveOfferTempReqTO.isPeakRecurringDetails()) {
                offeringTemplateTO.setPeakBasedOffering(true);
                offeringPeakDetail = new SysVendorOfferingPeakDetail();
                offeringPeakDetail.setVendorOfferingPeakDetailID(peakRecurringDetailTO.getVendorOfferingPeakDetailID());
                offeringPeakDetail.setPeakTimeRecurringTypeID(peakRecurringDetailTO.getPeakTimeRecurringTypeID().longValue());
                offeringPeakDetail.setPeakTimeStartDateID(timeDimensionDS.getTimeDimensionFromDate(peakRecurringDetailTO.getPeakStartDateTime()).getTimeDimensionId());
                offeringPeakDetail.setPeakTimeEndDateID(timeDimensionDS.getTimeDimensionFromDate(peakRecurringDetailTO.getPeakEndDateTime()).getTimeDimensionId());
                offeringPeakDetail.setPeakTimeStartHour(peakRecurringDetailTO.getPeakTimeStartHour());
                offeringPeakDetail.setPeakTimeEndHour(peakRecurringDetailTO.getPeakTimeEndHour());
                offeringPeakDetail.setPeakTimeStartMin(peakRecurringDetailTO.getPeakTimeStartMin());
                offeringPeakDetail.setPeakTimeEndMin(peakRecurringDetailTO.getPeakTimeEndMin());
                offeringPeakDetail.setVendorOfferingID(vendorOfferingID);
                if (peakRecurringDetailTO.getVendorOfferingPeakDetailID() == null) {
                    sysVendorOfferingPeakDetailDS.saveVendorOfferingPeakDetail(offeringPeakDetail);
                } else {
                    sysVendorOfferingPeakDetailDS.updateVendorOfferingPeakDetail(offeringPeakDetail);
                }
            }

            /**
             * deleting redis keys
             */
            altBenefitsCache.deleteSystemVendorOfferingsTemplatesRedisKey();
            altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSectorWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSystemVendorOfferingsList();
            altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();


        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * 
     * @return
     * @throws Exception
     */
    public List<VendorOfferingTemplateTO> getOfferingTemplateList()
        throws Exception
    {
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        try {
            try {
                vendorOfferingTemplateTOList = altBenefitsCache.getSystemVendorOfferingsTemplates();
                if (vendorOfferingTemplateTOList == null) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    vendorOfferingTemplateTOList = getOfferingTemplateListFromDB();
                    if (vendorOfferingTemplateTOList != null) {
                        try {
                            altBenefitsCache.putSystemVendorOfferingsTemplates(vendorOfferingTemplateTOList);
                        } catch (Exception ex1) {
                            throw ex1;
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            return vendorOfferingTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @param protocolTO
     * @param benefitRequestTO
     * @return
     * @throws Exception
     */
    public List<VendorOfferingTemplateTO> getOfferingTemplateListFromDB()
        throws Exception
    {
        List<SysVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        vendorOfferingTemplateTOList = new ArrayList<VendorOfferingTemplateTO>();
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        SysBenefitsVendor vendor = null;
        BenefitVendorTO benefitVendorTO = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            sysVendorOfferingTemplateList = offeringTemplateDS.getVendorOfferingTemplateList();
            if (sysVendorOfferingTemplateList != null && !sysVendorOfferingTemplateList.isEmpty()) {
                for (SysVendorOfferingTemplate svO : sysVendorOfferingTemplateList) {
                    VendorOfferingTemplateTO oTempTO = new VendorOfferingTemplateTO();
                    vendor = svO.getSysBenefitsVendor();
                    oTempTO.setBanner(svO.getBanner());
                    oTempTO.setVendorOfferingID(svO.getVendorOfferingID());
                    oTempTO.setVendorID(svO.getVendorID());
                    oTempTO.setVendorName(svO.getSysBenefitsVendor().getVendorName());
                    oTempTO.setOfferingTypeID(svO.getOfferingTypeID());
                    oTempTO.setOfferingSequence(svO.getOfferingSequence());
                    oTempTO.setOfferingType(svO.getSysOfferingType().getOfferingType());
                    oTempTO.setText(svO.getText());
                    oTempTO.setOfferingLink(svO.getOfferingLink());
                    oTempTO.setOfferTemplateName(svO.getOfferTemplateName());
                    oTempTO.setActive(svO.getActive());
                    if (svO.getContractSignedDateID() != null) {
                        oTempTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractSignedDateID()));
                    }
                    if (svO.getContractStartDateID() != null) {
                        oTempTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractStartDateID()));
                    }
                    if (svO.getContractEndDateID() != null) {
                        oTempTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getContractEndDateID()));
                    }
                    if (svO.getEffectiveStartDateID() != null) {
                        oTempTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveStartDateID()));
                    }
                    if (svO.getEffectiveEndDateID() != null) {
                        oTempTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(svO.getEffectiveEndDateID()));
                    }
                    oTempTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractSignedDate()));
                    oTempTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractStartDate()));
                    oTempTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getContractEndDate()));
                    oTempTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveStartDate()));
                    oTempTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(oTempTO.getEffectiveEndDate()));
                    /**
                     * offering peak details
                     * 
                     */
                    if (svO.getOfferingPeakBased() != null && svO.getOfferingPeakBased()) {
                        oTempTO.setPeakBasedOffering(svO.getOfferingPeakBased());
                        if (svO.getSysVendorOfferingPeakDetail() != null && !svO.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = svO.getSysVendorOfferingPeakDetail().iterator().next();
                            peakRecurringDetailTO = new PeakRecurringDetailTO();
                            peakRecurringDetailTO.setPeakTimeRecurringTypeLabel(peakRecurringDetail.getSysPeakRecurringType().getSysType());
                            peakRecurringDetailTO.setPeakTimeRecurringTypeID(peakRecurringDetail.getPeakTimeRecurringTypeID().intValue());
                            peakRecurringDetailTO.setVendorOfferingPeakDetailID(peakRecurringDetail.getVendorOfferingPeakDetailID());
                            peakRecurringDetailTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            peakRecurringDetailTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            peakRecurringDetailTO.setPeakStartDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakStartDateTime()));
                            peakRecurringDetailTO.setPeakEndDateTimeString(CommonUtilHelper.changeDateFormat_MMMddYYYY(peakRecurringDetailTO.getPeakEndDateTime()));
                            peakRecurringDetailTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            peakRecurringDetailTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            peakRecurringDetailTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            peakRecurringDetailTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                        }
                        oTempTO.setPeakRecurringDetailTO(peakRecurringDetailTO);
                    }

                    /**
                     * Offering Code Details
                     */
                    oTempTO.setOfferingCodeTOList(getOfferingCodeListBySet(svO.getOfferingCodes()));

                    /**
                    * Adding vendor details
                    */
                    benefitVendorTO = new BenefitVendorTO();
                    benefitVendorTO.setVendorID(vendor.getVendorID());
                    benefitVendorTO.setVendorName(vendor.getVendorName());
                    benefitVendorTO.setVendorCode(vendor.getVendorCode());
                    benefitVendorTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractSignedDate()));
                    benefitVendorTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate()));
                    if (vendor.getContractEndDate() != null) {
                        benefitVendorTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate()));
                    }
                    benefitVendorTO.setActive(vendor.getActive());
                    if (vendor.getEffectiveStartDate() != null) {
                        benefitVendorTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveStartDate()));
                    }
                    if (vendor.getEffectiveEndDate() != null) {
                        benefitVendorTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveEndDate()));
                    }
                    if (vendor.getActive() != null && vendor.getActive()) {
                        benefitVendorTO.setStatus(IRoleConstants.ACTIVE_STRING);
                    } else {
                        benefitVendorTO.setStatus(IRoleConstants.INACTIVE_STRING);
                    }
                    benefitVendorTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractSignedDate()));
                    benefitVendorTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractStartDate()));
                    benefitVendorTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getContractEndDate()));
                    benefitVendorTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveStartDate()));
                    benefitVendorTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(benefitVendorTO.getEffectiveEndDate()));
                    benefitVendorTO.setTermsAndConditions(vendor.getTermAndConditions());
                    benefitVendorTO.setVendorLogo(vendor.getVendorLogo());
                    oTempTO.setBenefitVendorTO(benefitVendorTO);

                    vendorOfferingTemplateTOList.add(oTempTO);
                }
            }

            return vendorOfferingTemplateTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    public void setAllHrOfferingTOList()
        throws Exception
    {
        List<BannerOfferingSequenceTO> systemOfferingTOList = null;
        try {
            try {
                systemOfferingTOList = altBenefitsCache.getSystemVendorOfferingsList();
                if (systemOfferingTOList == null || systemOfferingTOList.isEmpty()) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    getAllHrOfferingTOListFromDB();
                } catch (Exception e) {
                    throw e;
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }


    public void getAllHrOfferingTOListFromDB()
        throws Exception
    {
        List<SysVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        try {
            sysVendorOfferingTemplateList = offeringTemplateDS.getVendorOfferingTemplateList();
            processSystemOfferingTOList(sysVendorOfferingTemplateList);

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param systemVendorOfferingTemplateList
     * @throws Exception
     */

    public void processSystemOfferingTOList(List<SysVendorOfferingTemplate> systemVendorOfferingTemplateList)
        throws Exception
    {
        List<BannerOfferingSequenceTO> systemOfferingTOList = null;
        BannerOfferingSequenceTO bannerOfferingSequenceTO = null;
        SysVendorOfferingPeakDetail peakRecurringDetail = null;
        try {
            systemOfferingTOList = new ArrayList<BannerOfferingSequenceTO>();
            if (systemVendorOfferingTemplateList != null && !systemVendorOfferingTemplateList.isEmpty()) {
                for (SysVendorOfferingTemplate offeringSequence : systemVendorOfferingTemplateList) {
                    /**
                     * 
                     */
                    bannerOfferingSequenceTO = new BannerOfferingSequenceTO();
                    bannerOfferingSequenceTO.setBanner(offeringSequence.getBanner());
                    bannerOfferingSequenceTO.setOfferingSequence(offeringSequence.getOfferingSequence());
                    bannerOfferingSequenceTO.setText(offeringSequence.getText());
                    bannerOfferingSequenceTO.setOfferingLink(offeringSequence.getOfferingLink());
                    bannerOfferingSequenceTO.setOfferTemplateName(offeringSequence.getOfferTemplateName());

                    /**
                     * offering peak details
                     * 
                     */
                    if (offeringSequence.getOfferingPeakBased() != null && offeringSequence.getOfferingPeakBased()) {
                        if (offeringSequence.getSysVendorOfferingPeakDetail() != null && !offeringSequence.getSysVendorOfferingPeakDetail().isEmpty()) {
                            peakRecurringDetail = offeringSequence.getSysVendorOfferingPeakDetail().iterator().next();
                            bannerOfferingSequenceTO.setPeakStartDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeStartDateID()));
                            bannerOfferingSequenceTO.setPeakEndDateTime(CommonUtilHelper.getDateFromTimeDimensionId(peakRecurringDetail.getPeakTimeEndDateID()));
                            bannerOfferingSequenceTO.setPeakTimeStartHour(peakRecurringDetail.getPeakTimeStartHour());
                            bannerOfferingSequenceTO.setPeakTimeEndHour(peakRecurringDetail.getPeakTimeEndHour());
                            bannerOfferingSequenceTO.setPeakTimeStartMin(peakRecurringDetail.getPeakTimeStartMin());
                            bannerOfferingSequenceTO.setPeakTimeEndMin(peakRecurringDetail.getPeakTimeEndMin());
                            bannerOfferingSequenceTO.setPeakDetails(true);
                        }

                    }

                    /**
                     * adding offering code details
                     */
                    bannerOfferingSequenceTO.setOfferingCodeTOList(getOfferingCodeListBySet(offeringSequence.getOfferingCodes()));

                    systemOfferingTOList.add(bannerOfferingSequenceTO);
                }

            }
            altBenefitsCache.deleteSystemVendorOfferingsList();
            altBenefitsCache.putSystemVendorOfferingsList(systemOfferingTOList);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param vendorOfferingID
     * @return
     * @throws Exception
     */
    public List<OfferingCodeTO> getOfferingCodeListByVendorOffering(Long vendorOfferingID)
        throws Exception
    {
        List<OfferingCodeTO> offeringCodeTOList = null;
        try {
            try {
                offeringCodeTOList = altBenefitsCache.getVendorOfferingCode(vendorOfferingID);
                if (offeringCodeTOList == null) {
                    throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    offeringCodeTOList = getOfferingCodeListByVendorOfferingFromDB(vendorOfferingID);
                    if (offeringCodeTOList != null) {
                        try {
                            altBenefitsCache.putVendorOfferingCode(offeringCodeTOList, vendorOfferingID);
                        } catch (Exception ex1) {
                            throw ex1;
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            return offeringCodeTOList;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @param vendorOfferingID
     * @return
     */
    public List<OfferingCodeTO> getOfferingCodeListByVendorOfferingFromDB(Long vendorOfferingID)
        throws Exception
    {
        List<OfferingCode> offeringCodeList = null;
        List<OfferingCodeTO> offeringCodeTOList = null;
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        OfferingCodeTO offeringCodeTO = null;
        SysOfferingCodeMaximumLimit codeMaximumLimit = null;
        try {
            offeringCodeTOList = new ArrayList<OfferingCodeTO>();
            offeringCodeList = offeringCodeDS.getOfferingCodeListByVendorOffering(vendorOfferingID);
            if (offeringCodeList != null && !offeringCodeList.isEmpty()) {
                for (OfferingCode offeringCode : offeringCodeList) {
                    offeringCodeTO = new OfferingCodeTO();
                    offeringCodeTO.setOfferingCodeID(offeringCode.getOfferingCodeID());
                    offeringCodeTO.setOfferingCode(offeringCode.getOfferingCode());
                    offeringCodeTO.setAccessTypeID(offeringCode.getAccessTypeID());
                    offeringCodeTO.setActive(offeringCode.getActive());
                    offeringCodeTO.setVendorOfferingID(offeringCode.getVendorOfferingID());
                    offeringCodeTO.setAccessType(offeringCode.getSysContentType().getSysType());
                    offeringCodeTO.setLogicBased(offeringCode.getLogicBased());
                    offeringCodeTO.setMaximumLimitApplicable(offeringCode.getMaximumLimitApplicable());
                    if (offeringCode.getOfferingCodeStartDateID() != null) {
                        offeringCodeTO.setOfferingCodeStartDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeStartDateID()));
                        offeringCodeTO.setOfferingCodeStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeStartDate()));
                    }
                    if (offeringCode.getOfferingCodeEndDateID() != null) {
                        offeringCodeTO.setOfferingCodeEndDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeEndDateID()));
                        offeringCodeTO.setOfferingCodeEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeEndDate()));
                    }
                    /**
                     * offer code maximum limit details
                     */
                    if (offeringCode.getMaximumLimitApplicable()) {
                        if (offeringCode.getMapCodeMaximumLimits() != null && !offeringCode.getMapCodeMaximumLimits().isEmpty()) {
                            codeMaximumLimit = offeringCode.getMapCodeMaximumLimits().iterator().next();
                            offeringCodeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                            offeringCodeMaximumLimitTO.setOfferingCodeMaximumLimitID(codeMaximumLimit.getOfferingCodeMaximumLimitID());
                            offeringCodeMaximumLimitTO.setMaximumUsageCount(codeMaximumLimit.getMaximumUsageCount());
                            offeringCodeMaximumLimitTO.setMaximumUsageType(codeMaximumLimit.getMaximumUsageType().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageTypeID(codeMaximumLimit.getMaximumUsageTypeID());
                            offeringCodeMaximumLimitTO.setMaximumUsageDuration(codeMaximumLimit.getMaximumUsageDuration().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageDurationID(codeMaximumLimit.getMaximumUsageDurationID());
                        }
                        offeringCodeTO.setCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                    }
                    offeringCodeTOList.add(offeringCodeTO);
                }

                altBenefitsCache.putVendorOfferingCode(offeringCodeTOList, vendorOfferingID);
            }

        } catch (Exception e) {
            throw e;
        }
        return offeringCodeTOList;
    }

    /**
     * 
     * @param offeringCodeTO
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveOfferCode(OfferingCodeTO offeringCodeTO)
        throws Exception
    {
        Long offeringCodeID = null;
        try {
            offeringCodeTO.setOfferingCodeStartDateID(timeDimensionDS.getTimeDimensionFromDate(offeringCodeTO.getOfferingCodeStartDate()).getTimeDimensionId());
            offeringCodeTO.setOfferingCodeEndDateID(timeDimensionDS.getTimeDimensionFromDate(offeringCodeTO.getOfferingCodeEndDate()).getTimeDimensionId());
            offeringCodeID = offeringCodeDS.saveOfferCode(offeringCodeTO);
            if (offeringCodeTO.isMaximumLimitApplicable()) {
                offeringCodeTO.getCodeMaximumLimitTO().setOfferingCodeID(offeringCodeID);
                codeMaximumLimitDS.saveOfferingCodeMaximumLimit(offeringCodeTO.getCodeMaximumLimitTO(), offeringCodeTO.isEdit());
            }
            altBenefitsCache.deleteSystemVendorOfferingsTemplatesRedisKey();
            altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSectorWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSystemVendorOfferingsList();
            altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();
            altBenefitsCache.deleteVendorOfferingCodeRedisKey(offeringCodeTO.getVendorOfferingID());
        } catch (Exception e) {
            throw e;
        }

    }

    /**
     * 
     * @param offeringCodesSet
     * @return
     * @throws Exception
     */

    public List<OfferingCodeTO> getOfferingCodeListBySet(Set<OfferingCode> offeringCodesSet)
        throws Exception
    {
        List<OfferingCodeTO> offeringCodeTOList = null;
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        OfferingCodeTO offeringCodeTO = null;
        SysOfferingCodeMaximumLimit codeMaximumLimit = null;
        try {
            offeringCodeTOList = new ArrayList<OfferingCodeTO>();
            if (offeringCodesSet != null && !offeringCodesSet.isEmpty()) {
                for (Iterator<OfferingCode> fieldIterator = offeringCodesSet.iterator(); fieldIterator.hasNext();) {
                    OfferingCode offeringCode = fieldIterator.next();
                    offeringCodeTO = new OfferingCodeTO();
                    offeringCodeTO.setOfferingCodeID(offeringCode.getOfferingCodeID());
                    offeringCodeTO.setOfferingCode(offeringCode.getOfferingCode());
                    offeringCodeTO.setAccessTypeID(offeringCode.getAccessTypeID());
                    offeringCodeTO.setActive(offeringCode.getActive());
                    offeringCodeTO.setVendorOfferingID(offeringCode.getVendorOfferingID());
                    offeringCodeTO.setAccessType(offeringCode.getSysContentType().getSysType());
                    offeringCodeTO.setLogicBased(offeringCode.getLogicBased());
                    offeringCodeTO.setMaximumLimitApplicable(offeringCode.getMaximumLimitApplicable());
                    if (offeringCode.getOfferingCodeStartDateID() != null) {
                        offeringCodeTO.setOfferingCodeStartDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeStartDateID()));
                        offeringCodeTO.setOfferingCodeStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeStartDate()));
                    }
                    if (offeringCode.getOfferingCodeEndDateID() != null) {
                        offeringCodeTO.setOfferingCodeEndDate(CommonUtilHelper.getDateFromTimeDimensionId(offeringCode.getOfferingCodeEndDateID()));
                        offeringCodeTO.setOfferingCodeEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeEndDate()));
                    }
                    /**
                     * offer code maximum limit details
                     */
                    if (offeringCode.getMaximumLimitApplicable()) {
                        if (offeringCode.getMapCodeMaximumLimits() != null && !offeringCode.getMapCodeMaximumLimits().isEmpty()) {
                            codeMaximumLimit = offeringCode.getMapCodeMaximumLimits().iterator().next();
                            offeringCodeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                            offeringCodeMaximumLimitTO.setOfferingCodeMaximumLimitID(codeMaximumLimit.getOfferingCodeMaximumLimitID());
                            offeringCodeMaximumLimitTO.setMaximumUsageCount(codeMaximumLimit.getMaximumUsageCount());
                            offeringCodeMaximumLimitTO.setMaximumUsageType(codeMaximumLimit.getMaximumUsageType().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageTypeID(codeMaximumLimit.getMaximumUsageTypeID());
                            offeringCodeMaximumLimitTO.setMaximumUsageDuration(codeMaximumLimit.getMaximumUsageDuration().getSysType());
                            offeringCodeMaximumLimitTO.setMaximumUsageDurationID(codeMaximumLimit.getMaximumUsageDurationID());
                        }
                        offeringCodeTO.setCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                    }
                    offeringCodeTOList.add(offeringCodeTO);
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return offeringCodeTOList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public List<OfferingCodeTO> saveOfferCodeMasterUpload(List<OfferingCodeTO> offeringCodeTOList, Long vendorOfferingId)
        throws Exception
    {
        Long offeringCodeID = null;
        try {
            for (OfferingCodeTO offeringCodeTO : offeringCodeTOList) {
                offeringCodeTO.setOfferingCodeStartDateID(timeDimensionDS.getTimeDimensionFromDate(offeringCodeTO.getOfferingCodeStartDate()).getTimeDimensionId());
                offeringCodeTO.setOfferingCodeEndDateID(timeDimensionDS.getTimeDimensionFromDate(offeringCodeTO.getOfferingCodeEndDate()).getTimeDimensionId());
                offeringCodeID = offeringCodeDS.saveOfferCode(offeringCodeTO);
                if (offeringCodeTO.isMaximumLimitApplicable()) {
                    offeringCodeTO.getCodeMaximumLimitTO().setOfferingCodeID(offeringCodeID);
                    codeMaximumLimitDS.saveOfferingCodeMaximumLimit(offeringCodeTO.getCodeMaximumLimitTO(), offeringCodeTO.isEdit());
                }
            }
            altBenefitsCache.deleteSystemVendorOfferingsTemplatesRedisKey();
            altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSectorWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSystemVendorOfferingsList();
            altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();
            altBenefitsCache.deleteVendorOfferingCodeRedisKey(vendorOfferingId);
            return offeringCodeTOList;
        } catch (Exception e) {
            throw e;
        }

    }

}