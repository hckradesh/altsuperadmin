/**
 * 
 */
package com.talentpact.business.tenants.tpModule.exception;

/**
 * @author radhamadhab.dalai
 *
 */
public class TpModuleException extends Exception
{

    private String message;


    public TpModuleException()
    {

    }

    public TpModuleException(String message)
    {
        this.message = message;
    }


    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

}
