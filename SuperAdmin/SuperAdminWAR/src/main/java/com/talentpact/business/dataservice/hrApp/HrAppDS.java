package com.talentpact.business.dataservice.hrApp;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.auth.entity.HrApp;
import com.talentpact.auth.entity.TpApp;
import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.application.transport.input.TpAppRequestTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.tpApp.TpAppDS;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class HrAppDS extends AbstractDS<HrApp> {

	@EJB
	TpAppDS tpAppDS;

	public HrAppDS() {
		super(HrApp.class);
	}

	private static final Logger LOGGER_ = LoggerFactory.getLogger(HrAppDS.class);

	public List<HrAppTO> findAllAppForOrg(HrAppTO hrAppRequestTO) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		List<HrAppTO> tpAppList = null;
		long orgId = hrAppRequestTO.getOrgId();
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" select ");
			hqlQuery.append(" h.appID, h.orgID, h.tenantID from HrApp h");
			hqlQuery.append(" where h.orgID=:orgID");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			query.setParameter("orgID", orgId);
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				tpAppList = getHrApp(resultList);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return tpAppList;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveHrApp(HrAppTO requestTO) {
		HrApp hrApp = null;
		try {
			hrApp = new HrApp();
			hrApp.setAppID(getTpAppForId(requestTO.getAppId()));
			hrApp.setOrgID(requestTO.getOrgId());
			hrApp.setTenantID(requestTO.getTenantId());
			hrApp.setCreatedBy(1);
			hrApp.setModifiedBy(1);
			hrApp.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
			hrApp.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			hrApp.setIsActive(requestTO.getIsActive());
			getEntityManager("TalentPactAuth").persist(hrApp);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private TpApp getTpAppForId(long appId) throws Exception {
		TpAppRequestTO tpAppRequestTO = new TpAppRequestTO();
		tpAppRequestTO.setAppID(appId);
		return tpAppDS.findTpApp(tpAppRequestTO);

	}

	public static List<HrAppTO> getHrApp(List<Object[]> resultList) {
		List<HrAppTO> list = null;
		HrAppTO responseTO = null;

		try {
			list = new ArrayList<>();
			for (Object[] object : resultList) {
				responseTO = new HrAppTO((Long) object[0], (Long) object[1], (Long) object[2], (Boolean) object[3]);
				list.add(responseTO);
			}
		} catch (Exception ex) {
			LOGGER_.warn("Error : " + ex.toString());

		} finally {
			responseTO = null;
		}
		return list;
	}

	public List<HrAppTO> findAllAppForOrg(Long orgId, Long tenantId, Integer appTypeId) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		List<HrAppTO> tpAppList = new ArrayList<>();
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" select");
			hqlQuery.append(" h.appID , h.isActive from HrApp h ");
			hqlQuery.append(" where h.orgID=:orgID and h.tenantID=:tenantID and h.appID.appTypeID=:appTypeID");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			query.setParameter("orgID", orgId);
			query.setParameter("tenantID", tenantId);
			query.setParameter("appTypeID", appTypeId);
			resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				for (Object[] object : resultList) {
					HrAppTO hrApp = new HrAppTO(((TpApp) object[0]).getAppID(), 0, 0, (Boolean) object[1]);
					tpAppList.add(hrApp);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return tpAppList;
	}

	public void updateHrAppStatus(HrAppTO app) {
		Query query = null;
		StringBuilder hqlQuery = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" update HrApp h set h.isActive =:isActive, h.modifiedDate=:modifiedDate, h.modifiedBy=:modifiedBy");
			hqlQuery.append(" where h.orgID=:orgID and h.tenantID=:tenantID and h.appID=:appID");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			query.setParameter("orgID", app.getOrgId());
			query.setParameter("tenantID", app.getTenantId());
			query.setParameter("appID", getTpAppForId(app.getAppId()));
			query.setParameter("isActive", app.getIsActive());
			query.setParameter("modifiedDate", SQLDateHelper.getSqlTimeStamp());
			query.setParameter("modifiedBy", 1);
			query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void createOrUpdateApps(List<HrAppTO> createFinalAppList) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		for(HrAppTO app: createFinalAppList){
			try{
				hqlQuery = new StringBuilder();
				hqlQuery.append(" select");
				hqlQuery.append(" h.appID , h.isActive from HrApp h ");
				hqlQuery.append(" where h.orgID=:orgID and h.tenantID=:tenantID and h.appID=:appID");
				query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
				query.setParameter("orgID", app.getOrgId());
				query.setParameter("tenantID", app.getTenantId());
				query.setParameter("appID", getTpAppForId(app.getAppId()));
				resultList = query.getResultList();
				if (resultList != null && !resultList.isEmpty()) {
					updateHrAppStatus(app);
				}else{
					saveHrApp(app);
				}
			}catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
}
