/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysVendorOfferingPeakDetail;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysVendorOfferingPeakDetailDS extends AbstractDS<SysVendorOfferingPeakDetail>
{
    public SysVendorOfferingPeakDetailDS()
    {
        super(SysVendorOfferingPeakDetail.class);
    }

    /**
     * 
     * @param offeringPeakDetail
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveVendorOfferingPeakDetail(SysVendorOfferingPeakDetail offeringPeakDetail)
        throws Exception
    {
        try {
            getEntityManager("Talentpact").persist(offeringPeakDetail);
        } catch (Exception e) {
            throw e;
        }

    }


    /**
     * 
     * @param offeringPeakDetail
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void updateVendorOfferingPeakDetail(SysVendorOfferingPeakDetail offeringPeakDetail)
        throws Exception
    {
        try {
            getEntityManager("Talentpact").merge(offeringPeakDetail);
            getEntityManager("Talentpact").flush();

        } catch (Exception e) {
            throw e;
        }

    }


}