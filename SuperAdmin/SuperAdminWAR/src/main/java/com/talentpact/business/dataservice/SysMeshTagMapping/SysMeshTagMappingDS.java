/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshTagMapping;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import com.talentpact.business.application.transport.output.SysMeshTagMappingTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysMeshModule;
import com.talentpact.model.SysMeshTagMapping;


@Stateless
public class SysMeshTagMappingDS extends AbstractDS<SysMeshTagMapping>
{
    public SysMeshTagMappingDS()
    {
        super(SysMeshTagMapping.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysMeshTagMappingList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysMeshTagMappingID, sysMeshTagType, sysMeshModule.sysMeshModuleName, sysMeshModule.sysMeshModuleID from SysMeshTagMapping order by sysMeshTagMappingID");
            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO)
    {
    	SysMeshTagMapping sysMeshTagMapping = new SysMeshTagMapping();
        try {
        	sysMeshTagMapping.setSysMeshTagType(sysMeshTagMappingTO.getSysMeshTagType());
        	sysMeshTagMapping.setSysMeshModule(findModule(sysMeshTagMappingTO.getSysMeshModuleID()));
        	sysMeshTagMapping.setSysMeshModuleID(sysMeshTagMappingTO.getSysMeshModuleID());
            getEntityManager("AltDataManager").persist(sysMeshTagMapping);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private SysMeshModule findModule(Integer sysMeshModuleID) {
		// TODO Auto-generated method stub
		return null;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean editSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO)
    {
    	SysMeshTagMapping sysMeshTagMapping = new SysMeshTagMapping();
        try {
        	sysMeshTagMapping = findSysMeshTagMapping(sysMeshTagMappingTO);
        	sysMeshTagMapping.setSysMeshTagType(sysMeshTagMappingTO.getSysMeshTagType());
        	sysMeshTagMapping.setSysMeshModule(findModule(sysMeshTagMappingTO.getSysMeshModuleID()));
        	sysMeshTagMapping.setSysMeshModuleID(sysMeshTagMappingTO.getSysMeshModuleID());
            getEntityManager("AltDataManager").merge(sysMeshTagMapping);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysMeshTagMapping findSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysMeshTagMapping> SysMeshTagMappingList = null;
        SysMeshTagMapping result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select soc from SysMeshTagMapping soc  where soc.sysMeshTagMappingID=:sysMeshTagMappingID ");
            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
            query.setParameter("sysMeshTagMappingID", sysMeshTagMappingTO.getSysMeshTagMappingID());
            SysMeshTagMappingList = query.getResultList();
            if (SysMeshTagMappingList != null && SysMeshTagMappingList.size() > 0) {
                result = SysMeshTagMappingList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
    
    
    @SuppressWarnings("unchecked")
	public List<Object[]> getSysMeshModules()
            throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> modules = null;
        List<Object[]> result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select smm.sysMeshModuleID, smm.sysMeshModuleName from SysMeshModule smm ");
            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
            modules = query.getResultList();
            if (modules != null && modules.size() > 0) {
                result = modules;
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
    
    
    public List<String> getSysMeshModuleNames()
            throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<String> moduleNames = null;
        List<String> result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select soc from SysMeshTagMapping soc  where soc.sysMeshTagMappingID=:sysMeshTagMappingID");
            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
            moduleNames = query.getResultList();
            if (moduleNames != null && moduleNames.size() > 0) {
                result = moduleNames;
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

	@SuppressWarnings("unchecked")
	public String checkExist(Integer sysMeshModuleID, String sysMeshTagType) 
	{
		Query query = null;
        StringBuilder hqlQuery = null;
        List<String> result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select smm.sysMeshModule.sysMeshModuleName from SysMeshTagMapping smm where smm.sysMeshModuleID=:sysMeshModuleID and smm.sysMeshTagType=:sysMeshTagType ");
            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
            query.setParameter("sysMeshModuleID", sysMeshModuleID);
            query.setParameter("sysMeshTagType", sysMeshTagType);
            result = query.getResultList();
            if (result != null && result.size() > 0) {
                return result.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
		return null;
	}
}