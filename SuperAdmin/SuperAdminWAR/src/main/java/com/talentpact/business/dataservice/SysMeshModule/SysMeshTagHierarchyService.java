/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshModule;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagHierarchyTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysMeshTagHeirarchy;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysMeshTagHierarchyService extends CommonService
{

    @EJB
    SysMeshTagHierarchyDS sysMeshTagHierarchyDS;

    public List<SysMeshTagHierarchyTO> getSysMeshTagHierarchyList()
        throws Exception
    {
        List<SysMeshTagHierarchyTO> list = null;
        SysMeshTagHierarchyTO sysMeshTagHierarchyTO = null;
        List<Object[]> sysMeshTagHierarchyList = null;

        try {
            sysMeshTagHierarchyList = sysMeshTagHierarchyDS.getSysMeshTagHierarchyList();
            list = new ArrayList<SysMeshTagHierarchyTO>();
            for (Object[] sysMeshTagHierarchy : sysMeshTagHierarchyList) {
                sysMeshTagHierarchyTO = new SysMeshTagHierarchyTO();
                sysMeshTagHierarchyTO.setSysTagID((int) sysMeshTagHierarchy[0]);
                if (sysMeshTagHierarchy[1] != null)
                	sysMeshTagHierarchyTO.setModuleID((int) sysMeshTagHierarchy[1]);
                if (sysMeshTagHierarchy[2] != null)
                	sysMeshTagHierarchyTO.setParentID((int) sysMeshTagHierarchy[2]);
                if (sysMeshTagHierarchy[3] != null)
                	sysMeshTagHierarchyTO.setTagName((String) sysMeshTagHierarchy[3]);
                if (sysMeshTagHierarchy[4] != null)
                	sysMeshTagHierarchyTO.setDependentTagID((int) sysMeshTagHierarchy[4]);
                if (sysMeshTagHierarchy[5] != null)
                	sysMeshTagHierarchyTO.setMandatory((boolean) sysMeshTagHierarchy[5]);
                if (sysMeshTagHierarchy[6] != null)
                	sysMeshTagHierarchyTO.setUniqueSourceCodeNode((boolean) sysMeshTagHierarchy[6]);
                if (sysMeshTagHierarchy[7] != null)
                	sysMeshTagHierarchyTO.setDependentUponItself((boolean) sysMeshTagHierarchy[7]);
                if (sysMeshTagHierarchy[8] != null)
                	sysMeshTagHierarchyTO.setInboundSupported((boolean) sysMeshTagHierarchy[8]);
                if (sysMeshTagHierarchy[9] != null)
                	sysMeshTagHierarchyTO.setOutboundSupported((boolean) sysMeshTagHierarchy[9]);
                if (sysMeshTagHierarchy[10] != null)
                	sysMeshTagHierarchyTO.setTagHierarchy((String) sysMeshTagHierarchy[10]);
                if (sysMeshTagHierarchy[11] != null)
                	sysMeshTagHierarchyTO.setResourceTypeID((Integer) sysMeshTagHierarchy[11]);
                list.add(sysMeshTagHierarchyTO);
            }
            
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean validateSysMeshTagHierarchy(SysMeshTagHierarchyTO sysMeshTagHierarchyTO)
            throws Exception
        {
                return sysMeshTagHierarchyDS.validateSysMeshTagHierarchy(sysMeshTagHierarchyTO);
        }
    
    public void saveSysMeshModule(SysMeshTagHierarchyTO sysMeshTagHierarchyTO)
            throws Exception
        {
            try {
            	sysMeshTagHierarchyDS.saveSysMeshTagHierarchy(sysMeshTagHierarchyTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

	public void editSysMeshModule(SysMeshTagHierarchyTO sysMeshTagHierarchyTO)  {
        try {
        	sysMeshTagHierarchyDS.updateSysMeshTagHierarchy(sysMeshTagHierarchyTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public List<SysMeshModuleTO> getHierarchyList(int moduleID, int sysTagID) {
		List<Object[]> sysMeshTagHeirarchyList = null;
		List<SysMeshModuleTO> sysMeshModuleTOList = new ArrayList<SysMeshModuleTO>();
		SysMeshModuleTO sysMeshModuleTO = null;
		try
		{
			sysMeshTagHeirarchyList = sysMeshTagHierarchyDS.getHierarchyList(moduleID, sysTagID);
			if(sysMeshTagHeirarchyList != null)
			{
				for (Object[] sysMeshTagHeirarchy : sysMeshTagHeirarchyList) {
					sysMeshModuleTO = new SysMeshModuleTO();
					sysMeshModuleTO.setModuleID((Integer)sysMeshTagHeirarchy[0]);
					sysMeshModuleTO.setModuleName((String)sysMeshTagHeirarchy[1]);
					sysMeshModuleTOList.add(sysMeshModuleTO);
				}
			}
		} catch (Exception e) {
	            e.printStackTrace();
	    }
		return sysMeshModuleTOList;
	}

	public List<SysMeshModuleTO> getSysMeshModuleList() {
		List<Object[]> meshModuleTOList = null;
		List<SysMeshModuleTO> sysMeshModuleTOList = new ArrayList<SysMeshModuleTO>();
		SysMeshModuleTO sysMeshModuleTO = null;
		try
		{
			meshModuleTOList = sysMeshTagHierarchyDS.getSysMeshModuleList();
			if(meshModuleTOList != null)
			{
				for (Object[] result : meshModuleTOList) {
					sysMeshModuleTO = new SysMeshModuleTO();
					sysMeshModuleTO.setModuleID((Integer)result[0]);
					sysMeshModuleTO.setModuleName((String)result[1]);
					sysMeshModuleTOList.add(sysMeshModuleTO);
				}
			}
		} catch (Exception e) {
	            e.printStackTrace();
	    }
		return sysMeshModuleTOList;
	}

	public List<SysMeshModuleTO> getSysEntityList() {
		List<Object[]> meshModuleTOList = null;
		List<SysMeshModuleTO> sysMeshModuleTOList = new ArrayList<SysMeshModuleTO>();
		SysMeshModuleTO sysMeshModuleTO = null;
		try
		{
			meshModuleTOList = sysMeshTagHierarchyDS.getSysEntityList();
			if(meshModuleTOList != null)
			{
				for (Object[] result : meshModuleTOList) {
					sysMeshModuleTO = new SysMeshModuleTO();
					sysMeshModuleTO.setModuleID((Integer)result[0]);
					sysMeshModuleTO.setModuleName((String)result[1]);
					sysMeshModuleTOList.add(sysMeshModuleTO);
				}
			}
		} catch (Exception e) {
	            e.printStackTrace();
	    }
		return sysMeshModuleTOList;
	}
	
	public List<SysMeshModuleTO> getSysEntityColumnList(int entityID) {
		List<Object[]> meshModuleTOList = null;
		List<SysMeshModuleTO> sysMeshModuleTOList = new ArrayList<SysMeshModuleTO>();
		SysMeshModuleTO sysMeshModuleTO = null;
		try
		{
			meshModuleTOList = sysMeshTagHierarchyDS.getSysEntityColumnList(entityID);
			if(meshModuleTOList != null)
			{
				for (Object[] result : meshModuleTOList) {
					sysMeshModuleTO = new SysMeshModuleTO();
					sysMeshModuleTO.setModuleID(((Long)result[0]).intValue());
					sysMeshModuleTO.setModuleName((String)result[1]);
					sysMeshModuleTOList.add(sysMeshModuleTO);
				}
			}
		} catch (Exception e) {
	            e.printStackTrace();
	    }
		return sysMeshModuleTOList;
	}
}