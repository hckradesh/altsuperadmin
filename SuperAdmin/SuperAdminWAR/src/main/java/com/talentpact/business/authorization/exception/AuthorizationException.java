/**
 * 
 */
package com.talentpact.business.authorization.exception;

/**
 * @author radhamadhab.dalai
 * 
 */
public class AuthorizationException extends Exception
{

    private String message;

    public AuthorizationException()
    {

    }

    public AuthorizationException(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
