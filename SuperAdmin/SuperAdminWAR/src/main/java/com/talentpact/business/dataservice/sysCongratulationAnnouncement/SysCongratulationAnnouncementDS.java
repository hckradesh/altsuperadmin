/**
 * 
 */
package com.talentpact.business.dataservice.sysCongratulationAnnouncement;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysCongratulationAnnouncementResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysCongratulationAnnouncement;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCongratulationAnnouncementDS extends AbstractDS<SysCongratulationAnnouncement>
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysCongratulationAnnouncementDS.class);

    public SysCongratulationAnnouncementDS()
    {
        super(SysCongratulationAnnouncement.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysCongratulationAnnouncementResponseTO> getSysCongratulationAnnouncementTOList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysCongratulationAnnouncementResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" s.sysCongratulationAnnouncementID, ");
            hqlQuery.append(" s.sysAnnouncementType, s.sysCongratulationAnnouncement, ");
            hqlQuery.append(" s.sysCongratulationUploadPath ");
            hqlQuery.append(" from SysCongratulationAnnouncement s ");
            hqlQuery.append(" order by  s.sysCongratulationAnnouncementID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = converter(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public static List<SysCongratulationAnnouncementResponseTO> converter(List<Object[]> resultList)
    {
        List<SysCongratulationAnnouncementResponseTO> list = null;
        SysCongratulationAnnouncementResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysCongratulationAnnouncementResponseTO();
                if (object[0] != null) {
                    responseTO.setSysCongratulationAnnouncementID((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setSysAnnouncementType((String) object[1]);
                }
                if (object[2] != null) {
                    responseTO.setSysCongratulationAnnouncement((String) object[2]);
                }

                if (object[3] != null) {
                    responseTO.setSysCongratulationUploadPath((String) object[3]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void save(SysCongratulationAnnouncementResponseTO to)
    {
        SysCongratulationAnnouncement sysCongratulationAnnouncement = null;
        try {
            sysCongratulationAnnouncement = findSysCongratulationAnnouncement(to);

            if (sysCongratulationAnnouncement == null) {
                sysCongratulationAnnouncement = new SysCongratulationAnnouncement();

                sysCongratulationAnnouncement.setSysAnnouncementType(to.getSysAnnouncementType());
                sysCongratulationAnnouncement.setSysCongratulationAnnouncement(to.getSysCongratulationAnnouncement());
                sysCongratulationAnnouncement.setSysCongratulationUploadPath(to.getSysCongratulationUploadPath());

                getEntityManager("TalentPact").persist(sysCongratulationAnnouncement);
            } else {
                sysCongratulationAnnouncement.setSysAnnouncementType(to.getSysAnnouncementType());
                sysCongratulationAnnouncement.setSysCongratulationAnnouncement(to.getSysCongratulationAnnouncement());
                sysCongratulationAnnouncement.setSysCongratulationUploadPath(to.getSysCongratulationUploadPath());
                getEntityManager("TalentPact").merge(sysCongratulationAnnouncement);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private SysCongratulationAnnouncement findSysCongratulationAnnouncement(SysCongratulationAnnouncementResponseTO to)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysCongratulationAnnouncement> list = null;
        SysCongratulationAnnouncement result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select s from SysCongratulationAnnouncement s  where s.sysCongratulationAnnouncementID=:sysCongratulationAnnouncementID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("sysCongratulationAnnouncementID", to.getSysCongratulationAnnouncementID());
            list = query.getResultList();
            if (list != null && list.size() > 0) {
                result = list.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }


    public List<SysContentTypeTO> getSysContentTypeDropDown()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysContentTypeTO> list = null;
        String categoryName = "Announcement type";

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" a.sysTypeId, a.sysType ");
            hqlQuery.append(" from ");
            hqlQuery.append("  SysContentType  a where a.contentCategory.categoryName=:categoryName");
            query = getEntityManager("TalentPact").createQuery((hqlQuery.toString()));
            query.setParameter("categoryName", categoryName);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                list = sysContentTypeDropDownConverter(resultList);
            }
            return list;
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysContentTypeTO> sysContentTypeDropDownConverter(List<Object[]> resultList)
    {
        List<SysContentTypeTO> list = null;
        SysContentTypeTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysContentTypeTO();
                if (object[0] != null) {
                    responseTO.setSysTypeID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysType((String) object[1]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

}
