package com.talentpact.business.common.constants;


public interface ISysMeshTagMapping
{

    public static String SYSMESHTAGMAPPING_ADD_DIALOG_HIDE = "PF('sysMeshTagMappingAddModal').hide();";

    public static String SYSMESHTAGMAPPING_EDIT_DIALOG_HIDE = "PF('sysMeshTagMappingEditModal').hide();";

    public static String SYSMESHTAGMAPPING_SAVE_ERROR_MSG = "Error occured while saving SysMeshTagMapping.";

    public static String SYSMESHTAGMAPPING_UPDATE_ERROR_MSG = "Error occured while updating SysMeshTagMapping.";

    public static String SYSMESHTAGMAPPING_SAVE_SUCCESS_MSG = "SysMeshTagMapping saved successfully.";

    public static String SYSMESHTAGMAPPING_UPDATE_SUCCESS_MSG = "SysMeshTagMapping updated successfully.";

    public static String SYSMESHTAGMAPPING_DATATABLE_RESET = "PF('sysMeshTagMappingDataTable').clearFilters()";

    public static String SYSMESHTAGMAPPING_PAGINATION_DATATABLE_RESET = "PF('sysMeshTagMappingDataTable').getPaginator().setPage(0);";

    public static String SYSMESHTAGMAPPING_DUPLICATE_ERROR_MSG = "SysMeshTagMapping already present in system.";
}
