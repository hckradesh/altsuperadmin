package com.talentpact.business.dataservice;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.utility.CommonUtil;
import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.application.transport.helper.UserConverter;
import com.talentpact.business.application.transport.input.UserRequestTO;
import com.talentpact.business.application.transport.output.UserResponseTO;
import com.talentpact.business.authorization.exception.AuthenticateException;
import com.talentpact.business.dataservice.converter.TpUserConverter;
import com.talentpact.model.SysUser;
import com.talentpact.model.SysUserRole;
import com.talentpact.model.SysUserRoleId;
import com.talentpact.ui.survey.user.common.transport.output.TpUserCreatedResponseTO;

@Stateless
public class TpUserDS extends AbstractDS<TpUser> {

	public TpUserDS() {
		super(TpUser.class);
	}

	public List<TpUser> validateUser(String userName, String userPassword,
			String uri, String appname, String tenantCode, String orgCode)
			throws AuthenticateException {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<TpUser> users = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select tpuser from TpUser tpuser where tpuser.username=:userName and tpuser.password=:userPassword");
			hqlQuery.append(" and tpuser.organizationID.organizationCode=:orgCode");
			hqlQuery.append(" and tpuser.organizationID.tenantID.tenantCode=:tenantCode");
			hqlQuery.append(" and tpuser.enabled='1'");
			query = getEntityManager().createQuery(hqlQuery.toString());
			query.setParameter("userName", userName);
			query.setParameter("userPassword", userPassword);
			query.setParameter("tenantCode", tenantCode);
			query.setParameter("orgCode", orgCode);
			users = query.getResultList();
			return users;
		} catch (Exception ex) {
			throw new AuthenticateException(ex.getMessage());
		}
	}

	public List<TpUser> getUserData(Long orgId) {

		StringBuilder hqlQuery = null;
		List<TpUser> users = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select tpuser  from TpUser tpuser where tpuser.enabled =:active and  tpuser.admin = :admin ");
			if (orgId != null) {
				hqlQuery.append(" and  tpuser.organizationID.organizationID = :orgId ");
			}
			Query query = getEntityManager().createQuery(hqlQuery.toString());
			query.setParameter("active", true);
			query.setParameter("admin", true);
			if (orgId != null) {
				query.setParameter("orgId", orgId);
			}
			users = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return users;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public TpUserCreatedResponseTO setUserListByOrganization(
			UserRequestTO userTO) throws Exception {
		TpUserCreatedResponseTO tpUserCreatedResponseTO = null;
		TpUser tpUser = null;
		SysUser sysUser = null;
		SysUserRole sysUserRole = null;
		TpOrganization tpOrganization = null;
		String hashPassword = null;
		Integer k = null;
		if (userTO.getPassword() != null
				&& !userTO.getPassword().trim().equals("")) {
			hashPassword = CommonUtil.convertToHash(userTO.getPassword());
		}

		try {
			tpUser = findTpUser(userTO);
			tpOrganization = new TpOrganization();
			Date date = new Date();
			if (tpUser == null) {
				tpUser = new TpUser();
				tpUser.setCreatedDate(date);
				tpUser.setModifiedDate(date);
				tpUser.setLoginCount(0L);
				tpUser.setLoginFlag(false);
				tpOrganization.setOrganizationID((long) userTO
						.getOrganizationID());
				tpUser.setCreatedBy(userTO.getUserID().intValue());
				tpUser.setModifiedBy(userTO.getUserID().intValue());
				tpUser.setWelcomeMailSend(0L);
				tpUser.setAdmin(true);
				tpUser.setPassword(hashPassword);
				if (userTO.getUserName() != null
						&& !userTO.getUserName().trim().equals("")) {
					tpUser.setUsername(userTO.getUserName().trim());
				}
				tpUser.setEnabled(userTO.getStatusID());
				tpUser.setOrganizationID(tpOrganization);
				getEntityManager("TalentPactAuth").persist(tpUser);
				sysUser = new SysUser();
				sysUser.setCreatedDate(date);
				sysUser.setModifiedDate(date);
				sysUser.setLoginCount(0);
				sysUser.setLoginFlag(false);
				sysUser.setWelcomeMailSend(0);
				sysUser.setHrOrganizationId(userTO.getOrganizationID().intValue());
				sysUser.setSysTenantId(userTO.getTenantID().intValue());
				sysUser.setCreatedBy(userTO.getUserID().intValue());
				sysUser.setModifiedBy(userTO.getUserID().intValue());
				sysUser.setPasswordHash(hashPassword);
				if (userTO.getUserName() != null
						&& !userTO.getUserName().trim().equals("")) {
					sysUser.setUserName(userTO.getUserName().trim());
				}
				sysUser.setUserId(tpUser.getUserID().intValue());
				sysUser.setEnabled(userTO.getStatusID());
				getEntityManager("TalentPact").persist(sysUser);
				
				String hql = "select sr.roleId from SysRole  sr where sr.sysTenantId =:tenantID and sr.organizationId =:orgID and sr.roleName=:role";
				Query query = getEntityManager("TalentPact").createQuery(hql);
				query.setParameter("tenantID", userTO.getTenantID().intValue());
				query.setParameter("orgID", userTO.getOrganizationID()
						.intValue());
				query.setParameter("role", "Organization Admin");
				List<Integer> role = query.getResultList();
				SysUserRoleId sysUserRoleId = null;
				sysUserRoleId = new SysUserRoleId();
				sysUserRoleId.setRoleId(role.get(0));
				sysUserRoleId.setUserId(tpUser.getUserID().intValue());
				sysUserRole = new SysUserRole();
				sysUserRole.setSysUserID(tpUser.getUserID().intValue());
				sysUserRole.setId(sysUserRoleId);
				sysUserRole.setCreatedBy(tpUser.getUserID().intValue());
				sysUserRole.setModifiedBy(tpUser.getUserID().intValue());
				sysUserRole.setCreatedDate(date);
				sysUserRole.setModifiedDate(date);
				sysUserRole.setSysTenantId(userTO.getTenantID().intValue());
				getEntityManager("TalentPact").persist(sysUserRole);
				tpUserCreatedResponseTO = TpUserConverter.getCreatedUserdetails(sysUser);
			} else {
				if (userTO.getUserName() != null) {
					tpUser.setUsername(userTO.getUserName());
				}
				tpUser.setModifiedDate(date);
				tpUser.setLoginFlag(false);
				tpUser.setModifiedBy(userTO.getUserID().intValue());
				if (hashPassword != null) {
					tpUser.setPassword(hashPassword);
				}
				tpUser.setEnabled(userTO.getStatusID());
				getEntityManager("TalentPactAuth").merge(tpUser);

				sysUser = new SysUser();
				// sysUser.setUserId(protocolTO.getLoggedInUserId());

				sysUser = findSysUser(userTO, tpUser);
				if (sysUser != null) {
					if (userTO.getUserName() != null) {
						sysUser.setUserName(userTO.getUserName());
					}
					sysUser.setModifiedDate(date);
					sysUser.setLoginFlag(false);
					sysUser.setModifiedBy(userTO.getUserID().intValue());
					sysUser.setPasswordHash(hashPassword);
					sysUser.setEnabled(userTO.getStatusID());
					getEntityManager("TalentPact").merge(sysUser);
				} else {
					sysUser = new SysUser();
					sysUser.setCreatedDate(date);
					sysUser.setModifiedDate(date);
					sysUser.setLoginFlag(false);
					sysUser.setHrOrganizationId(userTO.getOrganizationID()
							.intValue());
					sysUser.setSysTenantId(userTO.getTenantID().intValue());
					sysUser.setCreatedBy(userTO.getUserID().intValue());
					sysUser.setModifiedBy(userTO.getUserID().intValue());
					sysUser.setPasswordHash(hashPassword);
					sysUser.setUserName(userTO.getUserName());
					sysUser.setUserId(tpUser.getUserID().intValue());
					sysUser.setEnabled(userTO.getStatusID());
					sysUser.setLoginCount(0);
					getEntityManager("TalentPact").persist(sysUser);
				}
				tpUserCreatedResponseTO = TpUserConverter.getCreatedUserdetails(sysUser);
				getEntityManager("TalentPactAuth").flush();
			}
		} catch (Exception ex) {
			// LOGGER_.warn("Error : " + ex.toString());
			throw ex;
		}
		return tpUserCreatedResponseTO;
	}

	@SuppressWarnings("unchecked")
	private TpUser findTpUser(UserRequestTO userTO) throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<TpUser> userList = null;
		TpUser result = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append("select tpu from TpUser  tpu  where tpu.userID=:UserID and  tpu.organizationID.organizationID = :organizationID and tpu.organizationID.tenantID.tenantID = :tenantID");
			query = getEntityManager("TalentPactAuth").createQuery(
					hqlQuery.toString());
			query.setParameter("UserID", userTO.getUserID());
			query.setParameter("organizationID",
					new Long(userTO.getOrganizationID()));
			query.setParameter("tenantID", new Long(userTO.getTenantID()));
			userList = query.getResultList();
			if (userList != null && userList.size() > 0) {
				result = userList.get(0);
			}
			return result;
		} catch (Exception ex) {
			// LOGGER_.warn("Error : " + ex.toString());
			throw ex;

		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	@SuppressWarnings("unchecked")
	private SysUser findSysUser(UserRequestTO userTO, TpUser tpUser)
			throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysUser> userList = null;
		SysUser result = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append("select sysu from SysUser  sysu  where sysu.userId=:UserID and  sysu.hrOrganizationId = :organizationID and sysu.sysTenantId = :tenantID");

			query = getEntityManager("TalentPact").createQuery(
					hqlQuery.toString());
			query.setParameter("UserID", tpUser.getUserID().intValue());
			query.setParameter("organizationID", userTO.getOrganizationID());
			query.setParameter("tenantID", userTO.getTenantID());
			userList = query.getResultList();
			if (userList != null && userList.size() > 0) {
				result = userList.get(0);
			}
			return result;
		} catch (Exception ex) {
			// LOGGER_.warn("Error : " + ex.toString());
			throw ex;

		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	public List<UserResponseTO> getUserByTenant(UserRequestTO userTO)
			throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		List<UserResponseTO> userlist = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select tpUser.userID,tpUser.username,tpUser.enabled,tpUser.password from TpUser   tpUser "
					+ "where tpUser.organizationID.organizationID = :organizationID and tpUser.organizationID.tenantID.tenantID = :tenantID and tpUser.admin = 1 ");
			query = getEntityManager("TalentPactAuth").createQuery(
					hqlQuery.toString());
			query.setParameter("organizationID",
					new Long(userTO.getOrganizationID()));
			query.setParameter("tenantID", new Long(userTO.getTenantID()));
			resultList = query.getResultList();
			if ((resultList != null) && !resultList.isEmpty()) {
				userlist = UserConverter.getUserList(resultList);
			}
			return userlist;
		} catch (Exception ex) {
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
			resultList = null;
		}

	}

}
