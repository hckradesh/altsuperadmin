package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysProductAnnouncementConstants
{

    public static String SYSPRODUCTANNOUNCEMENT_ADD_DIALOG_HIDE = "PF('sysProductAnnouncementAddModal').hide();";

    public static String SYSPRODUCTANNOUNCEMENT_EDIT_DIALOG_HIDE = "PF('sysProductAnnouncementEditModal').hide();";

    public static String SYSPRODUCTANNOUNCEMENT_SAVE_ERROR_MSG = "Error occured while saving SysProductAnnouncement.";

    public static String SYSPRODUCTANNOUNCEMENT_UPDATE_ERROR_MSG = "Error occured while updating SysProductAnnouncement.";

    public static String SYSPRODUCTANNOUNCEMENT_SAVE_SUCCESS_MSG = "SysProductAnnouncement saved successfully.";

    public static String SYSPRODUCTANNOUNCEMENT_UPDATE_SUCCESS_MSG = "SysProductAnnouncement updated successfully.";

    public static String SYSPRODUCTANNOUNCEMENT_DATATABLE_RESET = "PF('sysProductAnnouncementDataTable').clearFilters();";

    public static String SYSPRODUCTANNOUNCEMENT_PAGINATION_DATATABLE_RESET = "PF('sysProductAnnouncementDataTable').getPaginator().setPage(0);";

    public static String SYSPRODUCTANNOUNCEMENT_DUPLICATE_ERROR_MSG = "SysProductAnnouncement already present in system.";
    public static final String SYSPRODUCTANNOUNCEMENT_BANNER_PATH = "product_announcement_banner_upload_path";
    public static final String SYSPRODUCTANNOUNCEMENT_BANNER_SERVER_PATH = "product_announcement_banner_upload_server_path";
    
}
