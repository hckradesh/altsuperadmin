/**
 * 
 */
package com.talentpact.business.dataservice.loginserver.cache;


import java.util.Map;

import javax.ejb.Stateless;

import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.dataservice.loginserver.cache.constants.LoginServerKeyConstants;

/**
 * @author javed.ali
 *
 */
@Stateless
public class LoginServerRedisCache
{
    public Map<String, SocialAppSettingTO> getLoginServerSettingMap(Long urlID, Long organizationid)
        throws Exception
    {
        String key = LoginServerKeyConstants.LOGIINSERVER_ACCESS_ROOT + LoginServerKeyConstants.COLLON + organizationid + LoginServerKeyConstants.COLLON + urlID;
        return null;//RedisClient.getValueAsMapTO(key, SocialAppSettingTO.class);
    }

    public void putgetLoginServerSettingMap(Map<String, SocialAppSettingTO> socialAppSettingTOMap, Long urlID, Long organizationid)
        throws Exception
    {
        String key = LoginServerKeyConstants.LOGIINSERVER_ACCESS_ROOT + LoginServerKeyConstants.COLLON + organizationid + LoginServerKeyConstants.COLLON + urlID;
        RedisClient.putValueAsTO(key, socialAppSettingTOMap);
    }

    public void deleteLoginServerAccessTO(Long urlID, Long organizationid)
        throws Exception
    {
        String key = LoginServerKeyConstants.LOGIINSERVER_ACCESS_ROOT + LoginServerKeyConstants.COLLON + organizationid + LoginServerKeyConstants.COLLON + urlID;
        RedisClient.delete(key);
    }
}
