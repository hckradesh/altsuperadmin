/**
 * 
 */
package com.talentpact.business.dataservice.tpApp;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.TpAppRequestTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class TpAppService extends CommonService {

	private static final Logger LOGGER_ = LoggerFactory.getLogger(TpAppService.class);

	@EJB
	TpAppDS tpAppDS;

	public List<TpAppResponseTO> getTpAppList() throws Exception

	{
		List<TpAppResponseTO> list = null;

		try {
			list = tpAppDS.getTpAppList();

			return list;
		} catch (Exception ex) {
			LOGGER_.error(ex.getMessage());
			throw ex;
		}
	}

	public void saveTpApp(TpAppRequestTO requestTO) throws Exception {
		try {
			tpAppDS.saveTpApp(requestTO);
			RedisClient.delete("ALTCOMMON:LOGIN:APP:IMAGEMAP");

		} catch (Exception e) {
			LOGGER_.info(e.getMessage());

		}

	}

	public List<TpAppResponseTO> getCustomAppList() {
		List<TpAppResponseTO> appList = null;
		try {
			appList = tpAppDS.getAppsForGivenAppType(TpAppConstants.TP_APP_TYPE_CUSTOM);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return appList;
	}

	public List<TpAppResponseTO> getPartnerAppList() {
		List<TpAppResponseTO> appList = null;
		try {
			appList = tpAppDS.getAppsForGivenAppType(TpAppConstants.TP_APP_TYPE_PARTNER);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return appList;
	}

}
