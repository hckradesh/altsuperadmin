package com.talentpact.business.cache.cacheprovider;

public interface RedisCacheKeyConstant
{

    /*Alt Admin Portal Keys Start*/
    
    /*For Authentication*/

    public static final  String ALT_ADMIN_LOGIN_KEY = "ALTADMIN:LOGINPAGE:";
    
    public static final  String ALT_ADMIN_USER_KEY = "ALTADMIN:USER:";
   
    /*Alt Admin Portal Keys  End*/
    
    
/*For Alt Suite (Organise, Infer,Recruit) Keys*/
    
    /*For Authentication*/    
    
    public static final  String ALT_SUITE_LOGIN_KEY = "ALT:ORGLOGINDATA:";
    
    /* check List Keys------start   */
    public static final String  ORGANIZE_ALLROLE = "ORGANIZE:ALLROLE:" ;
    /*  finaly key will be like : ORGANIZE:ALLROLE:organizationID-tenantID */
    public static final String  COLLON = ":" ;
    public static final String HYPHEN  ="-" ;
    public static final String ALTADMIN_MENULIST = "ALTADMIN:MENULIST:" ;
    /*  finaly key will be like : ALTADMIN:MENULIST:organizationID-tenantID */
	public static final String REDIS_ORGANIZATION_URL_KEY_PREFIX = "ALTCOMMON:LOGIN:URL:";
	public static final String REDIS__URL_KEY_PREFIX = "ALTCOMMON:LOGIN:URL:";
	public static final String REDIS__MOBILEURL_KEY_PREFIX = "ALTCOMMON:MOBILELOGIN:URLINFO:";
	public static final String REDIS_ALL_URL_MAP_KEY = "ALTCOMMON:LOGIN:ALLURLMAP";
    
}
