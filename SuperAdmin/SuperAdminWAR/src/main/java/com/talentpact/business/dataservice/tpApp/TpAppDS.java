/**
 * 
 */
package com.talentpact.business.dataservice.tpApp;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.auth.entity.TpApp;
import com.talentpact.business.application.transport.input.TpAppRequestTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.dataservice.AbstractDS;

/**
 * @author pankaj.sharma1
 *
 */

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
// @TransactionAttribute(value = TransactionAttributeType.NEVER)
public class TpAppDS extends AbstractDS<TpApp> {
	private static final Logger LOGGER_ = LoggerFactory.getLogger(TpAppDS.class);

	public TpAppDS() {
		super(TpApp.class);
	}

	@SuppressWarnings("unchecked")
	public List<TpAppResponseTO> getTpAppList() {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		List<TpAppResponseTO> tpAppList = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" select ");
			hqlQuery.append(" t.appID, t.name, t.code, t.appImage ");
			hqlQuery.append(" from TpApp t ");
			hqlQuery.append(" order by  t.name ");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				tpAppList = getTpApp(resultList);
			}
		} catch (Exception ex) {
			LOGGER_.error("Error : " + ex.toString());
		}
		return tpAppList;
	}

	public static List<TpAppResponseTO> getTpApp(List<Object[]> resultList) {
		List<TpAppResponseTO> list = null;
		TpAppResponseTO responseTO = null;

		try {
			list = new ArrayList<>();
			for (Object[] object : resultList) {
				responseTO = new TpAppResponseTO();
				if (object[0] != null) {
					responseTO.setAppID((Long) object[0]);
				}

				if (object[1] != null) {
					responseTO.setName((String) object[1]);
				}
				if (object[2] != null) {
					responseTO.setCode((String) object[2]);
				}

				if (object[3] != null) {
					responseTO.setImage((String) object[3]);
				}

				list.add(responseTO);
			}
		} catch (Exception ex) {
			LOGGER_.warn("Error : " + ex.toString());
		} finally {
			responseTO = null;
		}
		return list;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveTpApp(TpAppRequestTO requestTO) {
		TpApp tpApp = null;
		try {
			tpApp = findTpApp(requestTO);

			if (tpApp == null) {
				tpApp = new TpApp();

				tpApp.setName(requestTO.getName());
				tpApp.setCode(requestTO.getCode());
				tpApp.setAppImage(requestTO.getImage());
				tpApp.setCreatedBy(1);
				tpApp.setModifiedBy(1);
				tpApp.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
				tpApp.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
				tpApp.setAppTypeID(3);
				tpApp.setAndroidEnabled(false);
				tpApp.setWebEnabled(false);
				tpApp.setIosEnabled(false);
				getEntityManager("TalentPactAuth").persist(tpApp);
			} else {
				tpApp.setName(requestTO.getName());
				tpApp.setCode(requestTO.getCode());
				tpApp.setAppImage(requestTO.getImage());
				tpApp.setModifiedBy(1);
				tpApp.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
				tpApp.setAppImage(requestTO.getImage());
				getEntityManager("TalentPactAuth").merge(tpApp);
			}
		} catch (Exception ex) {
			LOGGER_.error("Error : " + ex.toString());
		}
	}

	@SuppressWarnings("unchecked")
	public TpApp findTpApp(TpAppRequestTO requestTO) throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<TpApp> list = null;
		TpApp result = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append("select t from TpApp t  where t.appID=:appID ");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			query.setParameter("appID", requestTO.getAppID());
			list = query.getResultList();
			if (list != null && list.size() > 0) {
				result = list.get(0);
			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TpAppResponseTO> getAppsForGivenAppType(Integer appTypeId) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> resultList = null;
		List<TpAppResponseTO> tpAppList = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" select ");
			hqlQuery.append(" t.appID, t.name, t.code, t.appImage from TpApp t where t.appTypeID=:appTypeID");
			hqlQuery.append(" order by  t.name ");
			query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
			query.setParameter("appTypeID", appTypeId);
			resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty()) {
				tpAppList = getTpApp(resultList);
			}
		} catch (Exception ex) {
			LOGGER_.error("Error : " + ex.toString());
		}
		return tpAppList;
	}

}
