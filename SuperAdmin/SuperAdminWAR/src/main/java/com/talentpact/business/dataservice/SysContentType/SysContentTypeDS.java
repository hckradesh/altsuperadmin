/**
 * 
 */
package com.talentpact.business.dataservice.SysContentType;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.SysContentCategory.SysContentCategoryDS;
import com.talentpact.model.configuration.SysContentCategory;
import com.talentpact.model.configuration.SysContentType;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysContentTypeDS extends AbstractDS<SysContentType>
{
    
    SysContentCategoryDS sysContentCategoryDS;
    
    public SysContentTypeDS()
    {
        super(SysContentType.class);
    }

    public List<Object[]> getSysContentTypeList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysTypeId, sysType, contentCategory.categoryId, contentCategory.categoryName from SysContentType "
                    + "order by contentCategory.categoryName, sysType");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getNewSysContentCategoryList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct scc.categoryId, scc.categoryName from SysContentCategory scc order by scc.categoryName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysContentType(SysContentTypeTO sysContentTypeTO)
    {
        SysContentType sysContentType = new SysContentType();
        try {
            SysContentCategory sCC=getEntityManager("TalentPact").find(SysContentCategory.class, sysContentTypeTO.getContentCategoryID());
            sysContentType.setContentCategory(sCC);
            sysContentType.setSysType(sysContentTypeTO.getSysType());
            sysContentType.setSysTenantId(1);
            sysContentType.setCreatedBy(1);
            sysContentType.setModifiedBy(1);
            sysContentType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysContentType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());

            getEntityManager("TalentPact").persist(sysContentType);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean saveSysContentType(SysContentTypeTO sysContentTypeTO)
    {
        SysContentType sysContentType = null;
        try {
            SysContentCategory sCC=getEntityManager("TalentPact").find(SysContentCategory.class, sysContentTypeTO.getContentCategoryID());
            sysContentType = new SysContentType();
            sysContentType = findSysContentType(sysContentTypeTO);
            sysContentType.setContentCategory(sCC);
            sysContentType.setSysType(sysContentTypeTO.getSysType());
            sysContentType.setModifiedBy(1);
            sysContentType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").merge(sysContentType);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public SysContentType findSysContentType(SysContentTypeTO sysContentTypeTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysContentType> sysContentTypeList = null;
        SysContentType result = null;
        int tenantID = 1;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sct from SysContentType sct  where sct.sysTypeId=:sysTypeId and sct.sysTenantId=:sysTenant ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("sysTypeId", sysContentTypeTO.getSysTypeID());
            query.setParameter("sysTenant", tenantID);
            sysContentTypeList = query.getResultList();
            if (sysContentTypeList != null && sysContentTypeList.size() > 0) {
                result = sysContentTypeList.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
/**
 * 
 * @param contentCategoryName
 * @return
 * @throws Exception
 */
    @SuppressWarnings("unchecked")
    public List<Object[]> getSysTypeListByCategoryName(String contentCategoryName)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select distinct sct.sysTypeId, sct.sysType from SysContentType sct ");
            hqlQuery.append(" where sct.contentCategory.categoryName=:contentCategoryName  ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("contentCategoryName", contentCategoryName);
            resultList = query.getResultList();

        } catch (Exception ex) {
            throw ex;
        }
        return resultList;
    }

}