package com.talentpact.business.dataservice.system;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.alt.altreports.constants.IConstant;
import com.alt.system.constant.InstanceMigrationIConstant;
import com.alt.system.transport.input.InstanceMigrationRequestTO;
import com.talentpact.business.model.AltSyncUrl;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class InstanceMigrationDS
{
    @PersistenceContext(unitName = "TalentPactAuth")
    private EntityManager emTalentPactAuth;


    public InstanceMigrationDS()
    {

    }

    

    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<AltSyncUrl> getAltSyncUrlList()
        throws Exception
    {
        List<AltSyncUrl> resultList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select altSnyc from AltSyncUrl altSnyc where altSnyc.active =1 ");
            query = emTalentPactAuth.createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception e) {
            throw e;
        }
    }


    
    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> getUrlList(Integer dbServerID,Long appID)
        throws Exception
    {
        List<Object[]> resultList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(IConstant.PROC_COMMAND + InstanceMigrationIConstant.ALL_TP_URL_PROC+dbServerID+IConstant.COMMA_SPERATOR_STRING+appID);
            query = emTalentPactAuth.createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> getAppList()
        throws Exception
    {
        List<Object[]> resultList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select AppID,Name from tp_app ");
            query = emTalentPactAuth.createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> getEnvironmentList()
        throws Exception
    {
        List<Object[]> resultList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select EnvID,Name from SysEnvironment where IsActive=1");
            query = emTalentPactAuth.createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception e) {
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getDatabaseConfigList()
        throws Exception
    {
        List<Object[]> resultList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select DBConfigID,ServerName from SysDatabaseConfig where IsActive=1 ");
            query = emTalentPactAuth.createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception e) {
            throw e;
        }
    }
    
    
    @SuppressWarnings("unchecked")
    public Integer syncAltURL(InstanceMigrationRequestTO instanceMigrationRequestTO)
    {
        List<Integer> resultStatus = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        Integer result=null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(IConstant.PROC_COMMAND + InstanceMigrationIConstant.ALT_SYNC_INSERT_PROC+instanceMigrationRequestTO.getOrganizatonID()+IConstant.COMMA_SPERATOR_STRING +" '"+instanceMigrationRequestTO.getUrlName()+"'"+IConstant.COMMA_SPERATOR_STRING+instanceMigrationRequestTO.getAppId()+IConstant.COMMA_SPERATOR_STRING+instanceMigrationRequestTO.getEnvID()+IConstant.COMMA_SPERATOR_STRING+instanceMigrationRequestTO.getDbConfigId());
            query = emTalentPactAuth.createNativeQuery(hqlQuery.toString());
            resultStatus = query.getResultList();
            if(resultStatus!=null&&!resultStatus.isEmpty()){
                result= resultStatus.get(0);
            }
            return result;
        } catch (Exception e) {
            throw e;
        }
    }


}
