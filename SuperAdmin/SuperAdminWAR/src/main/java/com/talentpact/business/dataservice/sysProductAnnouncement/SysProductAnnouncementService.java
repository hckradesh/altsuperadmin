package com.talentpact.business.dataservice.sysProductAnnouncement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.HrOrganisationDS;
import com.talentpact.model.organization.HrProductAnnouncement;
import com.talentpact.model.organization.SysProductAnnouncement;
import com.talentpact.ui.sysProductAnnouncement.to.SysProductAnnouncementTO;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysProductAnnouncementService extends CommonService
{

    @EJB
    SysProductAnnouncementDS sysProductAnnouncementDS;

    @EJB
    HrOrganisationDS hrOrganisationDS;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysProductAnnouncementService.class);

    public List<SysProductAnnouncementTO> getSysProductAnnouncement()
        throws Exception
    {
        List<SysProductAnnouncement> sysProductAnnouncementList = null;
        List<SysProductAnnouncementTO> sysProductAnnouncementTOList = null;
        sysProductAnnouncementTOList = new ArrayList<SysProductAnnouncementTO>();
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            sysProductAnnouncementList = sysProductAnnouncementDS.getSysProductAnnouncementList();

            if (sysProductAnnouncementList != null && !sysProductAnnouncementList.isEmpty()) {
                for (SysProductAnnouncement sysProductAnnouncement : sysProductAnnouncementList) {
                    SysProductAnnouncementTO sysProductAnnouncementTO = new SysProductAnnouncementTO();
                    sysProductAnnouncementTO.setSysProductAnnouncementID(sysProductAnnouncement.getSysProductAnnouncementID());
                    sysProductAnnouncementTO.setSysProductAnnouncement(sysProductAnnouncement.getSysProductAnnouncement());
                    sysProductAnnouncementTO.setSysProductAnnouncementLabel(sysProductAnnouncement.getSysProductAnnouncementLabel());
                    sysProductAnnouncementTO.setValidFrom(sysProductAnnouncement.getValidFromid().getTheDate());
                    sysProductAnnouncementTO.setStrValidFrom(dateFormat.format((Date) sysProductAnnouncement.getValidFromid().getTheDate()));
                    sysProductAnnouncementTO.setValidTo(sysProductAnnouncement.getValidtoid().getTheDate());
                    sysProductAnnouncementTO.setStrValidTo(dateFormat.format((Date) sysProductAnnouncement.getValidtoid().getTheDate()));
                    sysProductAnnouncementTO.setActive(sysProductAnnouncement.isActive());
                    sysProductAnnouncementTO.setAnnouncementBanner(sysProductAnnouncement.getSysProductAnnouncementBanner());
                    sysProductAnnouncementTO.setBannerUrl(sysProductAnnouncement.getSysProductAnnouncementBannerUrl());
                    if (sysProductAnnouncement.isActive()) {
                        sysProductAnnouncementTO.setStatus("Active");
                    } else {
                        sysProductAnnouncementTO.setStatus("InActive");
                    }
                    sysProductAnnouncementTOList.add(sysProductAnnouncementTO);
                }
            }
            return sysProductAnnouncementTOList;
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } finally {
        }
    }

    public void saveSysProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO, Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        try {
            sysProductAnnouncementDS.saveSysProductAnnouncement(sysProductAnnouncementTO, organizationTOMap);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void updateSysProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO)
    {
        Map<Integer, SysProductAnnouncementTO> organizationTOMap = new HashMap<Integer, SysProductAnnouncementTO>();
        int[] selectedOrganizations;
        try {
            organizationTOMap = sysProductAnnouncementTO.getOrganizationTOMap();
            selectedOrganizations = sysProductAnnouncementTO.getSelectedOrganizations();

            if (organizationTOMap != null && !organizationTOMap.isEmpty()) {

                for (Entry<Integer, SysProductAnnouncementTO> mapVal : organizationTOMap.entrySet()) {
                    SysProductAnnouncementTO sysProductAnnouncementTO1 = mapVal.getValue();
                    for (int i = 0; i < selectedOrganizations.length; i++) {
                        if (sysProductAnnouncementTO1.getOrganizationAnnouncementActive() && selectedOrganizations[i] != sysProductAnnouncementTO1.getOrganizationID().intValue()) {
                            sysProductAnnouncementTO1.setDeleted(true);
                            break;
                        }
                    }
                }
            }

            sysProductAnnouncementDS.updateSysProductAnnouncement(sysProductAnnouncementTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public Map<Integer, SysProductAnnouncementTO> getEditOrganizationList(Integer sysProductAnnouncementID)
        throws Exception
    {
        List<SysProductAnnouncementTO> list = null;
        SysProductAnnouncementTO sysProductAnnouncementTO1 = null;
        List<Object[]> organizationList = null;
        List<HrProductAnnouncement> hrOrganizationList = null;
        Map<Integer, SysProductAnnouncementTO> organizationTOMap = null;

        try {
            organizationList = hrOrganisationDS.getAllOrganizationList();
            list = new ArrayList<SysProductAnnouncementTO>();
            organizationTOMap = new HashMap<Integer, SysProductAnnouncementTO>();
            for (Object[] sysProductAnnouncement : organizationList) {
                sysProductAnnouncementTO1 = new SysProductAnnouncementTO();
                sysProductAnnouncementTO1.setOrganizationID((Integer) sysProductAnnouncement[0]);
                sysProductAnnouncementTO1.setOrganizationName((String) sysProductAnnouncement[1]);
                sysProductAnnouncementTO1.setTenantID((Integer) sysProductAnnouncement[2]);
                sysProductAnnouncementTO1.setOrganizationAnnouncementActive(false);
                sysProductAnnouncementTO1.setDeleted(false);
                list.add(sysProductAnnouncementTO1);
                organizationTOMap.put(sysProductAnnouncementTO1.getOrganizationID(), sysProductAnnouncementTO1);
            }

            hrOrganizationList = sysProductAnnouncementDS.getAllHrProductAnnouncement(sysProductAnnouncementID);

            if (hrOrganizationList != null && !hrOrganizationList.isEmpty()) {
                if (organizationTOMap != null && !organizationTOMap.isEmpty()) {

                    for (Entry<Integer, SysProductAnnouncementTO> mapVal : organizationTOMap.entrySet()) {
                        SysProductAnnouncementTO sysProductAnnouncementTO2 = mapVal.getValue();
                        for (HrProductAnnouncement hrProductAnnouncement : hrOrganizationList) {
                            if (hrProductAnnouncement.getHrOrganization() == sysProductAnnouncementTO2.getOrganizationID().intValue()) {
                                sysProductAnnouncementTO2.setOrganizationAnnouncementActive(true);
                                break;
                            }
                        }
                    }
                }
            }
            return sortOrganizationTOMap(organizationTOMap);
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public Map<Integer, SysProductAnnouncementTO> getAllOrganizationList()
        throws Exception
    {
        List<SysProductAnnouncementTO> list = null;
        SysProductAnnouncementTO sysProductAnnouncementTO = null;
        List<Object[]> organizationList = null;
        Map<Integer, SysProductAnnouncementTO> organizationTOMap = null;

        try {
            organizationList = hrOrganisationDS.getAllOrganizationList();
            list = new ArrayList<SysProductAnnouncementTO>();
            organizationTOMap = new HashMap<Integer, SysProductAnnouncementTO>();
            for (Object[] sysProductAnnouncement : organizationList) {
                sysProductAnnouncementTO = new SysProductAnnouncementTO();
                sysProductAnnouncementTO.setOrganizationID((Integer) sysProductAnnouncement[0]);
                sysProductAnnouncementTO.setOrganizationName((String) sysProductAnnouncement[1]);
                sysProductAnnouncementTO.setTenantID((Integer) sysProductAnnouncement[2]);
                list.add(sysProductAnnouncementTO);
                organizationTOMap.put(sysProductAnnouncementTO.getOrganizationID(), sysProductAnnouncementTO);
            }
            return sortOrganizationTOMap(organizationTOMap);
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    private Map<Integer, SysProductAnnouncementTO> sortOrganizationTOMap(Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        try {
            Set<Entry<Integer, SysProductAnnouncementTO>> set = organizationTOMap.entrySet();
            LinkedList<Entry<Integer, SysProductAnnouncementTO>> list = new LinkedList<Entry<Integer, SysProductAnnouncementTO>>(set);
            Collections.sort(list, new Comparator<Map.Entry<Integer, SysProductAnnouncementTO>>() {
                public int compare(Map.Entry<Integer, SysProductAnnouncementTO> obj1, Map.Entry<Integer, SysProductAnnouncementTO> obj2)
                {
                    return (obj1.getValue().getOrganizationName().compareTo(obj2.getValue().getOrganizationName()));
                }
            });
            Map<Integer, SysProductAnnouncementTO> organizationTOMapNew = new LinkedHashMap<Integer, SysProductAnnouncementTO>();
            for (Entry<Integer, SysProductAnnouncementTO> entry : list) {
                organizationTOMapNew.put(entry.getValue().getOrganizationID(), entry.getValue());
            }
            return organizationTOMapNew;
        } catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
    }
}