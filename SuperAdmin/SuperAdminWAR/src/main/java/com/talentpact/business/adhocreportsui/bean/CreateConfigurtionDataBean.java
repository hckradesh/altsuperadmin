package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.ui.business.to.IntermediateProcessingLogicTemplateTo;
import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.business.adhocreportsui.controller.ConfigurationDataController;

@SessionScoped
@Named("createConfigurationDataBean")
public class CreateConfigurtionDataBean implements Serializable {

	private static final long serialVersionUID = 4312259147001004874L;

	@Inject
	ConfigurationDataController configurationDataController;

	private IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo;

	private String applicationInputNameForCreation;

	private String constantNameForCreation;

	private String constantValue;

	private String organizationId;

	private SelectItem selectedDataType;

	private List<ReportDetail> selectedNonDefaultReports;

	private ReportDetail selectedReportDetailsToSetRowLimit;

	@PostConstruct
	public void initialize() {
		configurationDataController.initialize(this);
	}

	public IntermediateProcessingLogicTemplateTo getIntermediateProcessingLogicTemplateTo() {
		return intermediateProcessingLogicTemplateTo;
	}

	public void setIntermediateProcessingLogicTemplateTo(
			IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo) {
		this.intermediateProcessingLogicTemplateTo = intermediateProcessingLogicTemplateTo;
	}

	public String getApplicationInputNameForCreation() {
		return applicationInputNameForCreation;
	}

	public void setApplicationInputNameForCreation(
			String applicationInputNameForCreation) {
		this.applicationInputNameForCreation = applicationInputNameForCreation;
	}

	public String getConstantNameForCreation() {
		return constantNameForCreation;
	}

	public void setConstantNameForCreation(String constantNameForCreation) {
		this.constantNameForCreation = constantNameForCreation;
	}

	public String getConstantValue() {
		return constantValue;
	}

	public void setConstantValue(String constantValue) {
		this.constantValue = constantValue;
	}

	public SelectItem getSelectedDataType() {
		return selectedDataType;
	}

	public ReportDetail getSelectedReportDetailsToSetRowLimit() {
		return selectedReportDetailsToSetRowLimit;
	}

	public void setSelectedReportDetailsToSetRowLimit(
			ReportDetail selectedReportDetailsToSetRowLimit) {
		this.selectedReportDetailsToSetRowLimit = selectedReportDetailsToSetRowLimit;
	}

	public void setSelectedDataType(SelectItem selectedDataType) {
		this.selectedDataType = selectedDataType;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public List<ReportDetail> getSelectedNonDefaultReports() {
		return selectedNonDefaultReports;
	}

	public void setSelectedNonDefaultReports(
			List<ReportDetail> selectedNonDefaultReports) {
		this.selectedNonDefaultReports = selectedNonDefaultReports;
	}

}
