package com.talentpact.business.dataservice.sysFieldType;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysFieldType;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysFieldType.to.SysFieldTypeTO;

@Stateless
public class SysFieldTypeDS  extends AbstractDS<SysFieldType>{

	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysFieldTypeDS.class);

	@Inject
    UserSessionBean userSessionBean;
	
	public SysFieldTypeDS() {
		super(SysFieldType.class);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<SysFieldType> getSysFieldTypeList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysFieldType sft");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }
	
	public void saveSysFieldType(SysFieldTypeTO sysFieldTypeTO) {
		SysFieldType sysFieldType=null;
        try{
        	sysFieldType=new SysFieldType();
        	sysFieldType.setFieldType(sysFieldTypeTO.getFieldType());
        	sysFieldType.setCreatedBy(userSessionBean.getUserID().intValue());
        	sysFieldType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
        	sysFieldType.setModifiedBy(userSessionBean.getUserID().intValue());
        	sysFieldType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
        	sysFieldType.setTenantID(1);
        	getEntityManager("TalentPact").persist(sysFieldType);
        	
            
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
	
	
	public List<SysFieldType> getUpdateSysFieldType(Integer sysFieldTypeID){
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysFieldType sft where sft.sysFieldTypeID!=:sysFieldTypeID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysFieldTypeID", sysFieldTypeID);
            return query.getResultList();
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
    }
	
    public void updateSysFieldType(SysFieldTypeTO sysFieldTypeTO){
        try{
        	SysFieldType sysFieldType=findSysFieldType(sysFieldTypeTO.getSysFieldTypeID());
        	sysFieldType.setFieldType(sysFieldTypeTO.getFieldType());
        	sysFieldType.setModifiedBy(userSessionBean.getUserID().intValue());
        	sysFieldType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").merge(sysFieldType);
            getEntityManager("TalentPact").flush();
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
    
    private SysFieldType findSysFieldType(Integer sysFieldTypeID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysFieldType> list=null;
        SysFieldType sysFieldType=null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysFieldType sysFieldType where sysFieldType.sysFieldTypeID=:sysFieldTypeID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysFieldTypeID", sysFieldTypeID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
            	sysFieldType = list.get(0);
            }
            return sysFieldType;
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
        
    }
}
