/**
 * 
 */
package com.talentpact.business.tpOrganization.constants;

/**
 * @author radhamadhab.dalai
 *
 */
public interface ITPOrganizationConstants
{
    public static final String TP_ORGANIZATION_DEFAULTTHEME = "gray-color-theme";

    public static final String TP_ORGANIZATION_LEFTLOGO = "\\Tenant_2\\Org_3\\Logo\\Maersk_Out.gif";

    public static final String TP_ORGANIZATION_RIGHTLOGO = "\\Tenant_2\\Org_3\\Logo\\Maersk_Out.gif";

    public static final String TP_ORGANIZATION_LDAPSERVER = "";

    public static final String TP_ORGANIZATION_LDAPSEARCHBASE = "OU=PeopleStrong,DC=psnet,DC=com";

    public static final String TP_ORGANIZATION_LDAPDOMAIN = "@psnet.com";


}
