package com.talentpact.business.adhocreportsui.controller;

import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_EMPTY;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.common.exception.CreateQueryException;
import com.talentpact.adhocreports.business.controller.CreateQuery;
import com.talentpact.adhocreports.ui.business.to.IntermediateProcessingLogicTemplateTo;
import com.talentpact.business.adhocreportsui.bean.CommonReportDataBean;
import com.talentpact.business.adhocreportsui.bean.CreateConfigurtionDataBean;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;

@SessionScoped
@Named("configurtionDataController")
public class ConfigurationDataController implements Serializable {

	private static final long serialVersionUID = -518965467720605011L;

	private static final Logger LOGGER = Logger.getLogger(ConfigurationDataController.class);

	@Inject
	CommonReportDataBean commonReportDataBean;

	@Inject
	UserSessionBean userSessionBean;

	public void initialize(CreateConfigurtionDataBean configurationDataBean) {
		IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
		intermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
		intermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
		configurationDataBean.setIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo);
	}

	private void createConstant(CreateConfigurtionDataBean configurationDataBean){
		if(configurationDataBean.getConstantNameForCreation().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateConstantDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Constant Name", "Constant Name is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if(configurationDataBean.getConstantValue().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateConstantDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Constant Value", "Constant Value is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if(configurationDataBean.getOrganizationId().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateConstantDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Organization Value", "Organization Value is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		try {
			CreateQuery.createConstant(configurationDataBean.getConstantNameForCreation(),configurationDataBean.getConstantValue(),configurationDataBean.getSelectedDataType(),configurationDataBean.getOrganizationId(),
					userSessionBean.getUserID().intValue());
		} catch (CreateQueryException e) {
			LOGGER.error("Error while creating constant inside createConstant()" , e);
			FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateConstantDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Constant", "Constant with same name already exists."));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateConstantDialog",new FacesMessage(FacesMessage.SEVERITY_INFO,"Constant Creation Successful ",configurationDataBean.getConstantNameForCreation() + " Constant created successfully"));
		resetValues(configurationDataBean);
		try {
			commonReportDataBean.setConstantList(CreateQuery.getConstantList());
			commonReportDataBean.setConstantToList(CreateQuery.getConstantToList());
		} catch (CreateQueryException e) {
			LOGGER.error("Error inside createConstant() while updating ConstantList after Constant creation." , e);
		}
	}

	private void createApplicationInput(CreateConfigurtionDataBean configurationDataBean){
		if(configurationDataBean.getApplicationInputNameForCreation().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateApplicationInputDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty ApplicationInput Name", "ApplicationInput Name is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		for(SelectItem item: commonReportDataBean.getApplicationInputList()){
			if(item.getLabel().equalsIgnoreCase(configurationDataBean.getApplicationInputNameForCreation())){
				FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateApplicationInputDialog",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate ApplicationInput Name", "ApplicationInput Name with same name already exists."));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}

		try {
			CreateQuery.createApplicationInput(configurationDataBean.getApplicationInputNameForCreation(),configurationDataBean.getSelectedDataType(),
					userSessionBean.getUserID().intValue());
		} catch (CreateQueryException e) {
			LOGGER.error("Error while creating application input inside createApplicationInput()" , e);
			return;
		}
		FacesContext.getCurrentInstance().addMessage("configurationForm:forCreateApplicationInputDialog",new FacesMessage(FacesMessage.SEVERITY_INFO,"ApplicationInput Creation Successful",configurationDataBean.getApplicationInputNameForCreation() + " ApplicationInput created successfully"));
		resetValues(configurationDataBean);
		try {
			commonReportDataBean.setApplicationInputList(CreateQuery.getApplicationInputList());
			commonReportDataBean.setApplicationInputToList(CreateQuery.getApplicationInputToList());
		} catch (CreateQueryException e) {
			LOGGER.error("Error inside createApplicationInput() while updating ApplicationInputList after ApplicationInput creation." , e);
		}
	}

	private void createProcessingLogicTemplate(CreateConfigurtionDataBean configurationDataBean){
		IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = configurationDataBean.getIntermediateProcessingLogicTemplateTo();
		if(checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo)){
			FacesContext.getCurrentInstance().addMessage("configurationForm:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Name is Required.", "ProcessingElementTemplate Name is Required."));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		try{
			if(CreateQuery.createProcessingLogicTemplate(intermediateProcessingLogicTemplateTo,
					userSessionBean.getUserID().intValue())){
				FacesContext.getCurrentInstance().addMessage("configurationForm:forTabViewMessage",new FacesMessage(FacesMessage.SEVERITY_INFO,"ProcessingLogicTemplate Creation Successful",intermediateProcessingLogicTemplateTo.getProcessingElementName() + " ProcessingLogicTemplate created successfully"));
			}else{
				FacesContext.getCurrentInstance().addMessage("configurationForm:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Creation Failure", "ProcessingElementTemplate creation failed."));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}catch(Exception e){
			LOGGER.error("ProcessingLogicTemplate creation failed.", e);
			FacesContext.getCurrentInstance().addMessage("configurationForm:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Creation Failure", "ProcessingElementTemplate creation failed."));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		resetValues(configurationDataBean);
		try{
			commonReportDataBean.setProcessingLogicTemplatesMap(CreateQuery.getProcessingLogicTemplatesMap());
			commonReportDataBean.setProcessingLogicTemplatesMapById(CreateQuery.getProcessingLogicTemplatesMapById());
		}catch (CreateQueryException e) {
			LOGGER.error("Error inside createProcessingLogicTemplate() while updating ProcessingLogicTemplateTypeList after ProcessingLogicTemplate creation." , e);
		}
	}

	private boolean checkEmptyStringIntermediateProcessingLogicTemplateTo(IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo){
		if(intermediateProcessingLogicTemplateTo!=null && intermediateProcessingLogicTemplateTo.getProcessingElementName().equals(CONSTANT_EMPTY)){
			return true;
		}else if(intermediateProcessingLogicTemplateTo!=null){
				return checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getFirstTypeProcessingLogicTo()) ||
						checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getSecondTypeProcessingLogicTo()) ||
							checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getThirdTypeProcessingLogicTo());
		}
		return false;
	}

	public void preRenderView(CreateConfigurtionDataBean configurationDataBean){
		if (!FacesContext.getCurrentInstance().isPostback()) {
			resetValues(configurationDataBean);
		}
	}

	public void resetValues(CreateConfigurtionDataBean configurationDataBean){
		configurationDataBean.setConstantNameForCreation(null);
		configurationDataBean.setApplicationInputNameForCreation(null);
		configurationDataBean.setConstantValue(null);
		configurationDataBean.setOrganizationId(null);
		configurationDataBean.setSelectedDataType(null);
		IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
		intermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
		intermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
		configurationDataBean.setIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo);
	}

	public void assignReport(String reportId, String orgId){
		try{
			CreateQuery.updateOrgReportMapping(Integer.valueOf(reportId), Integer.valueOf(orgId),
					userSessionBean.getUserID().intValue());
		}catch (CreateQueryException e) {
			LOGGER.error("Error inside assignReport() while updateOrgReportMapping(reportId, orgId) for reportId : " + reportId , e);
		}
	}

	public void activateDeactivateReport(String reportId, ReportDetailBean reportDetailBean){
		try{
			CreateQuery.activateDeactivateReport(Integer.valueOf(reportId),
					userSessionBean.getUserID().intValue());
			reportDetailBean.setActiveInactiveReportDetailList(CreateQuery.getAllActiveInactiveReportDetails());
		}catch (CreateQueryException e) {
			LOGGER.error("Error inside activateDeactivateReport(String reportId) for reportId : " + reportId , e);
		}
	}

	public void assignTable(Integer id, String orgId){
		try{
			CreateQuery.updateOrgDerivedTableMapping(id, Integer.valueOf(orgId),
					userSessionBean.getUserID().intValue());
		}catch (CreateQueryException e) {
			LOGGER.error("Error inside assignTable() while updateOrgDerivedTableMapping(id, orgId) for id : " + id , e);
		}
	}

	public void setSelectedReportDetailsToSetRowLimit(ReportDetail report, CreateConfigurtionDataBean createConfigurationDataBean)
	{
		createConfigurationDataBean.setSelectedReportDetailsToSetRowLimit(report);

	}

	public void updateReportRowsLimit(CreateConfigurtionDataBean createConfigurationDataBean) {

		ReportDetail reportDetail=createConfigurationDataBean.getSelectedReportDetailsToSetRowLimit();
		if(reportDetail.getRowLimit().equals(CONSTANT_EMPTY) || reportDetail.getRowLimit()==null || reportDetail.getRowLimit()<=0){
			FacesContext.getCurrentInstance().addMessage("editRowsLimitFormErrorMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Rows Limit", "Rows Limit is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		try {
			CreateQuery.updateReportRowsLimit(reportDetail.getReportId(),reportDetail.getRowLimit());
		} catch (CreateQueryException e) {
			LOGGER.error("Error inside updateReportRowsLimit() for reportId : " + reportDetail.getReportId() , e);
		}

		RequestContext.getCurrentInstance().execute("PF('editRowsLimitDialog').hide();");
        return;

	}


}
