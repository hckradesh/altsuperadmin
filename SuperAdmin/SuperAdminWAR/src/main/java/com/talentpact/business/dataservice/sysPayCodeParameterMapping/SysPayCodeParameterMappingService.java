/**
 * 
 */
package com.talentpact.business.dataservice.sysPayCodeParameterMapping;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.SysConstantRequestTO;
import com.talentpact.business.application.transport.input.SysPayCodeParameterMappingRequestTO;
import com.talentpact.business.application.transport.output.SysPayCodeParameterMappingResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.SysConstant.SysConstantService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPayCodeParameterMappingService extends CommonService
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantService.class);

    @EJB
    SysPayCodeParameterMappingCache sysPayCodeParameterMappingCache;

    @EJB
    SysPayCodeParameterMappingDS sysPayCodeParameterMappingDS;

    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMappingList()
        throws Exception

    {
        List<SysPayCodeParameterMappingResponseTO> sysPayCodeParameterMappingList = null;
        try {
            sysPayCodeParameterMappingList = sysPayCodeParameterMappingCache.getSysPayCodeParameterMappingList();
            if (sysPayCodeParameterMappingList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return sysPayCodeParameterMappingList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                sysPayCodeParameterMappingList = sysPayCodeParameterMappingDS.getSysPayCodeParameterMappingList();
                if (sysPayCodeParameterMappingList != null && !sysPayCodeParameterMappingList.isEmpty()) {
                    try {
                        sysPayCodeParameterMappingCache.persistSysPayCodeParameterMappingListByTenant(sysPayCodeParameterMappingList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistSysPayCodeParameterMappingList");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return sysPayCodeParameterMappingList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }


    public List<SysPayCodeParameterMappingResponseTO> getSysParameterDropDownList()
        throws Exception

    {
        List<SysPayCodeParameterMappingResponseTO> sysParameterDropDownList = null;
        try {
            sysParameterDropDownList = sysPayCodeParameterMappingCache.getSysParameterDropDownList();
            if (sysParameterDropDownList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return sysParameterDropDownList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                sysParameterDropDownList = sysPayCodeParameterMappingDS.getSysParameterDropDownList();
                if (sysParameterDropDownList != null && !sysParameterDropDownList.isEmpty()) {
                    try {
                        sysPayCodeParameterMappingCache.persistgetSysParameterDropDownList(sysParameterDropDownList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistgetSysParameterDropDownList");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return sysParameterDropDownList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }


    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeDropDownList()
        throws Exception

    {
        List<SysPayCodeParameterMappingResponseTO> sysPayCodeDropDownList = null;
        try {
            sysPayCodeDropDownList = sysPayCodeParameterMappingCache.getSysPayCodeDropDownList();
            if (sysPayCodeDropDownList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return sysPayCodeDropDownList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                sysPayCodeDropDownList = sysPayCodeParameterMappingDS.getSysPayCodeDropDownList();
                if (sysPayCodeDropDownList != null && !sysPayCodeDropDownList.isEmpty()) {
                    try {
                        sysPayCodeParameterMappingCache.persistgetSysPayCodeDropDownList(sysPayCodeDropDownList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistgetSysPayCodeDropDownList");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return sysPayCodeDropDownList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }

    public List<Integer> getSysPayCodeParameterMappingIdsFromHrPayrollParameterPolicy()
        throws Exception
    {
        List<Integer> list = null;
        try {
            list = sysPayCodeParameterMappingDS.getSysPayCodeParameterMappingIdsFromHrPayrollParameterPolicy();
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
        return list;
    }


    public void saveSysPayCodeParameterMappingList(SysPayCodeParameterMappingRequestTO sysPayCodeParameterMappingRequestTO)
        throws Exception
    {
        try {
            sysPayCodeParameterMappingDS.saveSysPayCodeParameterMapping(sysPayCodeParameterMappingRequestTO);
            sysPayCodeParameterMappingCache.removeSysPayCodeParameterMappingListByTenant();
            RedisClient.deleteByPattern("ALTADMIN:HRPAYROLLPARAMETERPOLICYSYSPAYCODEPARAMETERMAPPINGDROPDROWN:" + "*");

        } catch (Exception e) {
            LOGGER_.info(e.getMessage());

        }

    }


}
