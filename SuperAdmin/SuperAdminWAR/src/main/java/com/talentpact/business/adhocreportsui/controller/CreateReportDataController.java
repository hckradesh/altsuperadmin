package com.talentpact.business.adhocreportsui.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;

import com.talentpact.adhocreports.ui.business.to.IntermediateProcessingLogicTemplateTo;
import com.talentpact.adhocreports.ui.business.to.IntermediateFilterDataTo;
import com.talentpact.adhocreports.ui.business.to.IntermediateFilterGroupTo;
import com.talentpact.adhocreports.ui.business.to.IntermediateJoinDataTo;
import com.talentpact.adhocreports.business.common.controller.EnumFilterCombinationOperator;
import com.talentpact.adhocreports.business.common.controller.EnumFilterType;
import com.talentpact.adhocreports.business.common.controller.EnumJoinType;
import com.talentpact.adhocreports.business.common.controller.EnumSQLFunctionAndKeyword;
import com.talentpact.adhocreports.business.common.controller.EnumTableType;
import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.common.exception.CreateQueryException;
import com.talentpact.adhocreports.business.common.exception.ExecuteQueryException;
import com.talentpact.adhocreports.business.common.exception.FilterDisplayException;
import com.talentpact.adhocreports.business.common.exception.InputMissingException;
import com.talentpact.adhocreports.business.common.exception.MakeQueryException;
import com.talentpact.adhocreports.business.controller.CreateQuery;
import com.talentpact.adhocreports.business.controller.ExecuteQuery;
import com.talentpact.adhocreports.business.controller.Filter;
import com.talentpact.adhocreports.business.controller.MakeQuery;
import com.talentpact.adhocreports.business.to.FilterGroupTo;
import com.talentpact.adhocreports.business.to.FilterInputTo;
import com.talentpact.adhocreports.business.to.OperatorTo;
import com.talentpact.adhocreports.ui.business.common.ReportList;
import com.talentpact.adhocreports.ui.business.to.ColumnTo;
import com.talentpact.adhocreports.ui.business.to.IntermediateColumnDataTo;
import com.talentpact.adhocreports.ui.business.to.JoinDataTo;
import com.talentpact.adhocreports.ui.business.to.ProcessingLogicDefinitionTo;
import com.talentpact.adhocreports.ui.business.to.ProcessingLogicTemplateTo;
import com.talentpact.adhocreports.ui.business.to.ReportDataTo;
import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.business.adhocreportsui.bean.CommonReportDataBean;
import com.talentpact.business.adhocreportsui.bean.CreateReportDataBean;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;
import com.talentpact.ui.common.bean.UserSessionBean;

import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_SPACE;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_EMPTY;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_PROCESSINGLOGIC;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_COLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_DERIVEDGENERICCOLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_DERIVEDGROUPINGCOLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_UNDERSCORE;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_DOT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_SQLSYNTAX;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_APPLICTIONINPUT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_CONSTANT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_SYSTEMINPUT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_USERINPUT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_LIST;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_STRING;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_OPERATOR;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_STAR;

@SessionScoped
@Named("createReportDataController")
public class CreateReportDataController implements Serializable {

	private static final long serialVersionUID = 2048770992940325565L;
	private static final Logger LOGGER = Logger.getLogger(CreateReportDataController.class);

	@Inject
	CreateReportDataBean createReportDataBean;

	@Inject
	CommonReportDataBean commonReportDataBean;

	@Inject
	ReportDetailBean reportDetailBean;

	@Inject
	UserSessionBean userSessionBean;

	public void initialize(Boolean homeButtonClicked) {
			ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = new ReportList.ReportListNode<ReportDataTo>();
			ReportDataTo reportDataTo = new ReportDataTo();
			reportDataTo.setColumnToList(new ArrayList<ColumnTo>());
			List<IntermediateJoinDataTo> joinDataList = new ArrayList<IntermediateJoinDataTo>();
			IntermediateJoinDataTo intermediateJoinDataTo = new IntermediateJoinDataTo();
			intermediateJoinDataTo.setRenderUpdatedTableListFlag(false);
			joinDataList.add(intermediateJoinDataTo);
			reportDataTo.setUserId(userSessionBean.getUserID().intValue());
			reportDataTo.setJoinDataList(joinDataList);
			reportDataTo.setName(reportDetailBean.getReportNameForCreation());
			reportDataTo.setDescription(reportDetailBean.getReportDescriptionForCreation());
			reportDataTo.setModule(reportDetailBean.getSelectedModule());
			reportDataTo.setDefaultFlag(Boolean.valueOf(reportDetailBean.getDefaultFlag()));
			String reportType = reportDetailBean.getSelectedReportType();
			if(reportType.toLowerCase().equals(EnumTableType.REPORTGENERIC.getName().toLowerCase())){
				reportDataTo.setTableType(EnumTableType.REPORTGENERIC);
				reportDataTo.setTableTypeName("Simple Report");
			}else if(reportType.toLowerCase().equals(EnumTableType.REPORTGROUPING.getName().toLowerCase())){
				reportDataTo.setTableType(EnumTableType.REPORTGROUPING);
				reportDataTo.setTableTypeName("Grouping Report");
			}
			reportDataTo.setRenderAddJoinFlag(false);
			reportDataTo.setState(0);
			reportDataTo.setCurrentTab(0);
			reportDataTo.setSelectedColumnsDataMap(new HashMap<String,IntermediateColumnDataTo>());
			reportDataTo.setUpdatedColumnProcessingLogicTemplateNameList(new ArrayList<String>());
			reportDataTo.setUpdatedFilterValueColumnProcessingLogicTemplateNameList(new ArrayList<String>());
			reportDataTo.setSelectedOrderedColumnList(new ArrayList<String>());
			try {
				commonReportDataBean.setNormalTables(new DualListModel<TableTo>(CreateQuery.getNormalTablesExceptDefault(), new ArrayList<TableTo>()));
				if(reportDetailBean.getOrgID() == null){
					commonReportDataBean.setDerivedTables(new DualListModel<TableTo>(CreateQuery.getDerivedTables(), new ArrayList<TableTo>()));
				}else{
					commonReportDataBean.setDerivedTables(new DualListModel<TableTo>(CreateQuery.getDerivedTables(reportDetailBean.getOrgID().intValue()), new ArrayList<TableTo>()));
				}
			} catch (CreateQueryException e) {
				LOGGER.error("Error inside initialize() method while initializing CreateReportDataController()", e);
			}
			List<TableTo> systemTables = commonReportDataBean.getNormalTables().getSource();
			List<TableTo> derivedTables = commonReportDataBean.getDerivedTables().getSource();
			reportDataTo.setNormalTables(new DualListModel<TableTo>(new ArrayList<TableTo>(systemTables),new ArrayList<TableTo>(commonReportDataBean.getNormalTables().getTarget())));
			reportDataTo.setDerivedTables(new DualListModel<TableTo>(new ArrayList<TableTo>(derivedTables),new ArrayList<TableTo>(commonReportDataBean.getDerivedTables().getTarget())));
			TreeNode sourceTableRoot = new DefaultTreeNode("Root1", null);
			if(reportDetailBean.getOrgID() == null){
				TreeNode systemTablesNode = new DefaultTreeNode("System Objects", sourceTableRoot);
				systemTablesNode.setSelectable(false);
				for(TableTo tableTo : systemTables){
					systemTablesNode.getChildren().add(new DefaultTreeNode(tableTo,systemTablesNode));
				}
			}else{
				reportDataTo.setOrganization(reportDetailBean.getOrgID());
			}

			TreeNode derivedTablesNode = new DefaultTreeNode("Derived Objects", sourceTableRoot);
	        derivedTablesNode.setSelectable(false);
	        for(TableTo tableTo : derivedTables){
	        	derivedTablesNode.getChildren().add(new DefaultTreeNode(tableTo,derivedTablesNode));
	        }
			reportDataTo.setSourceTablesRoot(sourceTableRoot);
			TreeNode selectedTableRoot = new DefaultTreeNode("Root2", null);
			if(reportDetailBean.getOrgID() == null){
				TreeNode selectedSystemTablesNode = new DefaultTreeNode("System Objects", selectedTableRoot);
				selectedSystemTablesNode.setSelectable(false);
				selectedSystemTablesNode.setExpanded(true);
			}
			TreeNode selectedDerivedTablesNode = new DefaultTreeNode("Derived Objects", selectedTableRoot);
			selectedDerivedTablesNode.setSelectable(false);
			selectedDerivedTablesNode.setExpanded(true);
	        reportDataTo.setSelectedTablesRoot(selectedTableRoot);
			currentPopulatedListNode.setData(reportDataTo);
			IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
			intermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
			intermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			intermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			intermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
			createReportDataBean.setIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo);
			createReportDataBean.setCurrentPopulatedListNode(currentPopulatedListNode);
			createReportDataBean.setReportList(new ReportList<ReportDataTo>(currentPopulatedListNode));
			createReportDataBean.setSearchColumnRegex("");
			if (homeButtonClicked==true) {
				reportDetailBean.setReportNameForCreation(null);
				reportDetailBean.setReportDescriptionForCreation(null);
				reportDetailBean.setDefaultFlag(null);
				reportDetailBean.setSelectedReportType(null);
				reportDetailBean.setSelectedModule(null);
			}
	}

	private void updateOperatorList(long val){
		int index = (new Long(val)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		SelectItem operatorDataType = reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedOperatorDataType();
		reportDataTo.getIntermediateFilterDataToList().get(index).setOperatorsList(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(operatorDataType.getValue())));
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedOperatorsList(reportDataTo.getIntermediateFilterDataToList().get(index).getOperatorsList());
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedDefaultOperator(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedOperatorsList().get(0));
	}

	private void updateIntermediateSelectedTableList(long index){
		int elementNumber = (new Long(index)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(index == -1)
		{
			List<IntermediateJoinDataTo> joinDataList = new ArrayList<IntermediateJoinDataTo>();
			IntermediateJoinDataTo intermediateJoinDataTo = new IntermediateJoinDataTo();
			intermediateJoinDataTo.setRenderUpdatedTableListFlag(false);
			joinDataList.add(intermediateJoinDataTo);
			reportDataTo.setJoinDataList(joinDataList);
			List<TableTo> usedSelectedTables = new ArrayList<TableTo>();
			usedSelectedTables.add(reportDataTo.getSelectedTable());
			reportDataTo.setUsedSelectedTables(usedSelectedTables);
			reportDataTo.setRenderAddJoinFlag(true);
			reportDataTo.setRenderRemoveJoinFlag(true);
			elementNumber=0;
		}

		List<IntermediateJoinDataTo> joinDataList = reportDataTo.getJoinDataList();
		List<TableTo> usedSelectedTables = new ArrayList<TableTo>();
		usedSelectedTables.add(reportDataTo.getSelectedTable());
		int i=0;
		for(IntermediateJoinDataTo joinData: joinDataList){
			if(i!=joinDataList.size()-1){
				usedSelectedTables.add(joinData.getSelectedJoiningTable());
			}
			i++;
		}
		reportDataTo.setUsedSelectedTables(usedSelectedTables);

		if(elementNumber != reportDataTo.getJoinDataList().size()-1){
			List<IntermediateJoinDataTo> joinDataList1 = reportDataTo.getJoinDataList();
			List<IntermediateJoinDataTo> updatedJoinDataList = new ArrayList<IntermediateJoinDataTo>();
			List<TableTo> usedSelectedTables1 = new ArrayList<TableTo>();
			usedSelectedTables1.add(reportDataTo.getSelectedTable());
			i=0;
			for(IntermediateJoinDataTo joinData : joinDataList1){
				if(i++ <= elementNumber){
					updatedJoinDataList.add(joinData);
					usedSelectedTables1.add(joinData.getSelectedJoiningTable());
				}
			}
			reportDataTo.setJoinDataList(updatedJoinDataList);
			reportDataTo.setUsedSelectedTables(usedSelectedTables1);
		}

		List<TableTo> selectedIntermediateTableList = createReportDataBean.getCurrentPopulatedListNode().getData().getSelectedTables();
		List<TableTo> updatedIntermediateTableList = new ArrayList<TableTo>();
		for(TableTo selectedIntermediateTable : selectedIntermediateTableList){
				updatedIntermediateTableList.add(selectedIntermediateTable);
		}
		updatedIntermediateTableList.removeAll(reportDataTo.getUsedSelectedTables());
		if(index!=-1 && reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable() != null &&
				!updatedIntermediateTableList.contains(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable())) {
			updatedIntermediateTableList.add(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable());
		}
		reportDataTo.getJoinDataList().get(elementNumber).setUpdatedIntermediateSelectedTables(updatedIntermediateTableList);
		if((updatedIntermediateTableList.size()!=0 && reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable()==null) ||
				(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable()!=null && reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable().equals(reportDataTo.getSelectedTable()))){
			reportDataTo.getJoinDataList().get(elementNumber).setSelectedJoiningTable(updatedIntermediateTableList.get(0));
		}

		reportDataTo.getJoinDataList().get(elementNumber).setRenderUpdatedTableListFlag(true);
		reportDataTo.getJoinDataList().get(elementNumber).setRenderSecondConditionFlag(false);

		Map<TableTo,List<ColumnTo>> columnList = commonReportDataBean.getAllTableColumnsMap();
		if(elementNumber==0){
			List<ColumnTo> joinFirstConditionColumns = new ArrayList<ColumnTo>();
			if(columnList.get(reportDataTo.getSelectedTable())!=null){
				joinFirstConditionColumns.addAll(columnList.get(reportDataTo.getSelectedTable()));
			}
			reportDataTo.getJoinDataList().get(elementNumber).setJoinFirstConditionColumns(joinFirstConditionColumns);
		}else{
			List<TableTo> usedSelectedTables1 = reportDataTo.getUsedSelectedTables();
			reportDataTo.getJoinDataList().get(elementNumber).setJoinFirstConditionColumns(new ArrayList<ColumnTo>());
			for(TableTo table: usedSelectedTables1){
				if(columnList.get(table)!=null){
					reportDataTo.getJoinDataList().get(elementNumber).getJoinFirstConditionColumns().addAll(columnList.get(table));
				}
			}
		}

		if(!reportDataTo.getUsedSelectedTables().contains(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable())){
			reportDataTo.getUsedSelectedTables().add(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable());
		}
		List<ColumnTo> joinSecondConditionColumns = new ArrayList<ColumnTo>();
		if(columnList.get(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable())!=null){
			joinSecondConditionColumns.addAll(columnList.get(reportDataTo.getJoinDataList().get(elementNumber).getSelectedJoiningTable()));
		}
		reportDataTo.getJoinDataList().get(elementNumber).setJoinSecondConditionColumns(joinSecondConditionColumns);

		if(updatedIntermediateTableList.size() == 1){
			reportDataTo.setRenderAddJoinFlag(false);
		}
	}

	private void changeState(long state){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		if(state==-1){
			ReportDataTo reportDataTo = currentPopulatedListNode.getData();
			List<TableTo> selectedTables = new ArrayList<TableTo>();
			for(TableTo tableTo : reportDataTo.getNormalTables().getTarget()){
				selectedTables.add(tableTo);
			}
			selectedTables.addAll(commonReportDataBean.getDefaultTables());
			selectedTables.addAll(reportDataTo.getDerivedTables().getTarget());
			reportDataTo.setSelectedTables(selectedTables);
			//resetToState(0);
		}else if(state==1){
			ReportDataTo reportDataTo = currentPopulatedListNode.getData();
			List<TableTo> selectedTables = new ArrayList<TableTo>();
			for(TableTo tableTo : reportDataTo.getNormalTables().getTarget()){
				selectedTables.add(tableTo);
			}
			selectedTables.addAll(commonReportDataBean.getDefaultTables());
			selectedTables.addAll(reportDataTo.getDerivedTables().getTarget());
			if(selectedTables.size() == 0){
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forTabViewMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"No Object Selected", "Object Selection is required."));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
			reportDataTo.setSelectedTables(selectedTables);
			resetToState(0);
			reportDataTo.setState(1);
		}else if(state==2){
			resetToState(1);
			createReportDataBean.setSearchColumnRegex("");
			ReportDataTo reportDataTo = currentPopulatedListNode.getData();
			reportDataTo.setState(2);
			List<JoinDataTo> joinDataToList = new ArrayList<JoinDataTo>();
			List<IntermediateJoinDataTo> joinDataList = reportDataTo.getJoinDataList();
			JoinDataTo noJoinDataTo = new JoinDataTo();
			noJoinDataTo.setJoinSequence(0);
			noJoinDataTo.setTableID(reportDataTo.getSelectedTable().getId());
			noJoinDataTo.setJoinTableType(reportDataTo.getSelectedTable().getTableType());
			noJoinDataTo.setJoinType(EnumJoinType.NOJOIN);
			joinDataToList.add(noJoinDataTo);
			if(joinDataList.size() == 0){
				reportDataTo.setUsedSelectedTables(new ArrayList<TableTo>());
				reportDataTo.getUsedSelectedTables().add(reportDataTo.getSelectedTable());
			}
			int i=1;
			for(IntermediateJoinDataTo intermediateJoinData: joinDataList){
				JoinDataTo joinDataTo = new JoinDataTo();
				joinDataTo.setJoinTableType(intermediateJoinData.getSelectedJoiningTable().getTableType());
				joinDataTo.setJoinType(EnumJoinType.valueOf(intermediateJoinData.getSelectedJoinType().getLabel().toUpperCase().replaceAll(CONSTANT_SPACE,CONSTANT_EMPTY)));
				joinDataTo.setTableID(intermediateJoinData.getSelectedJoiningTable().getId());
				joinDataTo.setFirstConditionFirstSysColumnID(Integer.valueOf(intermediateJoinData.getJoinFirstConditionFirstSelectedColumn().getColumn().getValue()));
				joinDataTo.setFirstConditionFirstTableTypeID(intermediateJoinData.getJoinFirstConditionFirstSelectedColumn().getColumnTableTo().getTableType());
				joinDataTo.setFirstConditionSecondSysColumnID(Integer.valueOf(intermediateJoinData.getJoinFirstConditionSecondSelectedColumn().getColumn().getValue()));
				joinDataTo.setFirstConditionSecondTableTypeID(intermediateJoinData.getJoinFirstConditionSecondSelectedColumn().getColumnTableTo().getTableType());
				if(intermediateJoinData.isRenderSecondConditionFlag() == true){
					if(intermediateJoinData.getJoinSecondConditionFirstSelectedColumn()!=null){
						joinDataTo.setSecondConditionFirstSysColumnID(Integer.valueOf(intermediateJoinData.getJoinSecondConditionFirstSelectedColumn().getColumn().getValue()));
						joinDataTo.setSecondConditionFirstTableTypeID(intermediateJoinData.getJoinSecondConditionFirstSelectedColumn().getColumnTableTo().getTableType());
					}
					if(intermediateJoinData.getJoinSecondConditionSecondSelectedColumn()!=null){
						joinDataTo.setSecondConditionSecondSysColumnID(Integer.valueOf(intermediateJoinData.getJoinSecondConditionSecondSelectedColumn().getColumn().getValue()));
						joinDataTo.setSecondConditionSecondTableTypeID(intermediateJoinData.getJoinSecondConditionSecondSelectedColumn().getColumnTableTo().getTableType());
					}
				}
				joinDataTo.setJoinSequence(i);
				joinDataToList.add(joinDataTo);
				i++;
			}
			reportDataTo.setJoinDataToList(joinDataToList);
			Map<TableTo,List<ColumnTo>> columnList = commonReportDataBean.getAllTableColumnsMap();
			List<TableTo> usedSelectedTables = reportDataTo.getUsedSelectedTables();
			reportDataTo.setUsedSelectedTablesColumns(new ArrayList<ColumnTo>());
			IntermediateColumnDataTo intermediateColumnDataTo = new IntermediateColumnDataTo();
			intermediateColumnDataTo.setGroupingFunction(EnumSQLFunctionAndKeyword.GROUPBY);
			reportDataTo.setCurrentSelectedColumnsData(intermediateColumnDataTo);
			List<EnumTableType> selectedTableTypes = new ArrayList<EnumTableType>();
			TreeNode selectedTablesAndColumnsTreeRoot = new DefaultTreeNode("Root",null);
			if(usedSelectedTables != null){
				for(TableTo table: usedSelectedTables){
					if(!selectedTableTypes.contains(table.getTableType())){
						selectedTableTypes.add(table.getTableType());
					}
					if(columnList.get(table)!=null){
						reportDataTo.getUsedSelectedTablesColumns().addAll(columnList.get(table));
					}

			        TreeNode tableNode = new DefaultTreeNode(table, selectedTablesAndColumnsTreeRoot);
			        tableNode.setSelectable(false);
			        tableNode.setExpanded(true);
			        for(ColumnTo column : columnList.get(table)){
			        	new DefaultTreeNode(column, tableNode);
			        }
				}
			}

			reportDataTo.setSelectedTablesAndColumnsTreeRoot(selectedTablesAndColumnsTreeRoot);
			reportDataTo.setSelectedTablesColumns(new ArrayList<ColumnTo>());
			for(TableTo table: reportDataTo.getSelectedTables()){
				if(columnList.get(table)!=null){
					reportDataTo.getSelectedTablesColumns().addAll(columnList.get(table));
				}
			}
			reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().clear();
			reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().clear();
			for(String template: commonReportDataBean.getProcessingLogicTemplatesMap().keySet()){
				if(template.contains(CONSTANT_PROCESSINGLOGIC) || template.contains(CONSTANT_APPLICTIONINPUT) ||
						template.contains(CONSTANT_CONSTANT) || template.contains(CONSTANT_OPERATOR)){
					reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().add(template);
				}else{
					for(EnumTableType selectedTableType: selectedTableTypes){
						String comparisonString = CONSTANT_EMPTY;
						if(selectedTableType==EnumTableType.NORMAL){
							comparisonString = CONSTANT_COLUMN;
						}else if(selectedTableType==EnumTableType.GENERIC){
							comparisonString = CONSTANT_DERIVEDGENERICCOLUMN;
						}else if(selectedTableType==EnumTableType.GROUPING){
							comparisonString = CONSTANT_DERIVEDGROUPINGCOLUMN;
						}
						if(template.equals(comparisonString)||template.startsWith(comparisonString+CONSTANT_UNDERSCORE)||template.endsWith(CONSTANT_UNDERSCORE+comparisonString)||
								template.contains(CONSTANT_UNDERSCORE+comparisonString+CONSTANT_UNDERSCORE)){
							reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().add(template);
							reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().add(template);
							break;
						}
					}
				}
			}
			changeState(3);
		}else if(state == 3){
			ReportDataTo reportDataTo = currentPopulatedListNode.getData();
			reportDataTo.setSelectedFilterGroupMap(new HashMap<String,IntermediateFilterGroupTo>());
			reportDataTo.setSelectedOrderedFilterList(new ArrayList<String>());
			reportDataTo.setSelectedOrderedFilterListValue(null);
			reportDataTo.setSelectedFilterGroupHeader(null);
			reportDataTo.setSelectedNumberFilters(1);
			reportDataTo.setOptionalFilterGroupFlag(false);
			reportDataTo.setFilterGroupToList(new ArrayList<FilterGroupTo>());
			List<Map<String,FilterGroupTo>> filterGroupToMapListForTable = new ArrayList<Map<String,FilterGroupTo>>();
			reportDataTo.setFilterGroupToMapForTable(filterGroupToMapListForTable);
			reportDataTo.setState(3);
			reportDataTo.setSelectedFilterCombinationOperator(commonReportDataBean.getFilterCombinationOperatorList().get(0));
			createReportDataBean.setPreviewFlag(false);
			setInitialIntermediateFilterDataTo();
		}
	}

	private void resetToState(int state) {
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(state == 0){
			reportDataTo.setColumnToList(new ArrayList<ColumnTo>());
			reportDataTo.setSelectedTable(null);
			List<IntermediateJoinDataTo> joinDataList = new ArrayList<IntermediateJoinDataTo>();
			IntermediateJoinDataTo intermediateJoinDataTo = new IntermediateJoinDataTo();
			intermediateJoinDataTo.setRenderUpdatedTableListFlag(false);
			reportDataTo.setJoinDataList(joinDataList);
			reportDataTo.setUsedSelectedTables(null);
			reportDataTo.setSelectedTable(null);
			if(reportDataTo.getSelectedTables()!=null && reportDataTo.getSelectedTables().size() > 1){
				reportDataTo.setRenderAddJoinFlag(true);
			}
			reportDataTo.setRenderRemoveJoinFlag(false);
			reportDataTo.setSelectedColumnsDataMap(new HashMap<String,IntermediateColumnDataTo>());
			reportDataTo.setState(0);
			reportDataTo.setCurrentTab(0);
			reportDataTo.setUsedSelectedTablesColumns(null);
			reportDataTo.getSelectedColumnsDataMap().clear();
			reportDataTo.setSelectedOrderedColumnList(new ArrayList<String>());
			reportDataTo.setSelectedOrderedColumnListValue(null);
			resetAddDerivedTableDialogValues();
		}else if(state == 1){
			reportDataTo.setUsedSelectedTablesColumns(null);
			reportDataTo.getSelectedColumnsDataMap().clear();
			reportDataTo.setSelectedOrderedColumnList(new ArrayList<String>());
			reportDataTo.setSelectedOrderedColumnListValue(null);
		}
	}

	private void renderSecondCondition(long index){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.getJoinDataList().get((new Long(index)).intValue()).setRenderSecondConditionFlag(true);
	}

	private void removeSecondCondition(long index){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.getJoinDataList().get((new Long(index)).intValue()).setRenderSecondConditionFlag(false);
	}

	private void addJoin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		IntermediateJoinDataTo intermediateJoinDataTo = new IntermediateJoinDataTo();
		intermediateJoinDataTo.setRenderUpdatedTableListFlag(true);
		intermediateJoinDataTo.setRenderSecondConditionFlag(false);
		reportDataTo.setRenderRemoveJoinFlag(true);
		reportDataTo.getJoinDataList().add(intermediateJoinDataTo);
		updateIntermediateSelectedTableList(reportDataTo.getJoinDataList().size()-1);
	}

	private void removeJoin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		IntermediateJoinDataTo intermediateJoinDataTo = reportDataTo.getJoinDataList().remove(reportDataTo.getJoinDataList().size()-1);
		if(reportDataTo.getJoinDataList().size() == 0){
			reportDataTo.setRenderRemoveJoinFlag(false);
		}
		reportDataTo.setRenderAddJoinFlag(true);
		reportDataTo.getUsedSelectedTables().remove(intermediateJoinDataTo.getSelectedJoiningTable());
	}

	private void removeSelectedColumn(String column){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.getSelectedColumnsDataMap().remove(column);
		reportDataTo.getSelectedOrderedColumnList().remove(column);
		reportDataTo.setSelectedOrderedColumnListValue("");
	}

	private void viewSelectedColumn(String column){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.setCurrentSelectedColumnsData(reportDataTo.getSelectedColumnsDataMap().get(column));
	}

	private void addColumn(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedColumnHeader().equals("")){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnHeaderError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Column Header", "Column header value is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if(reportDataTo.getSelectedColumnsDataMap().keySet().contains(reportDataTo.getSelectedColumnHeader())){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnHeaderError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Column Header", "Already same name column is selected"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		IntermediateColumnDataTo intermediateColumnDataTo = new IntermediateColumnDataTo();
		intermediateColumnDataTo.setProcessingLogicDefination(reportDataTo.getSelectedColumnProcessingLogicDefinationTo());
		intermediateColumnDataTo.setName(reportDataTo.getSelectedColumnHeader());
		intermediateColumnDataTo.setVisibility(reportDataTo.isColumnVisible());
		intermediateColumnDataTo.setValue(reportDataTo.getSelectedColumnValue());
		intermediateColumnDataTo.setMasterCss(reportDataTo.getSelectedColumnCss());
		intermediateColumnDataTo.setOperator(reportDataTo.getSelectedColumnCssOperator());
		intermediateColumnDataTo.setOrderByFlag(reportDataTo.getOrderByFlag());
		intermediateColumnDataTo.setOrderByFunction(reportDataTo.getOrderByFunction());

		if(reportDataTo.getSelectedGroupingFunction()!=null){
			intermediateColumnDataTo.setGroupingFunction(EnumSQLFunctionAndKeyword.valueOf(reportDataTo.getSelectedGroupingFunction().toUpperCase()));
		}
		if(reportDataTo.getSelectedColumnDataType()!=null){
			intermediateColumnDataTo.setDataType(reportDataTo.getSelectedColumnDataType());
		}
		reportDataTo.getSelectedColumnsDataMap().put(reportDataTo.getSelectedColumnHeader(), intermediateColumnDataTo);
		reportDataTo.getSelectedOrderedColumnList().add(reportDataTo.getSelectedColumnHeader());
	}

	private void onSelectedColumnReorder(){
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String droppedColumnId = params.get("droppedColumnId");
        String draggedColumnId = params.get("draggedColumnId");
        String[] droppedColumnTokens = droppedColumnId.split(":");
        int droppedColumnIndex = Integer.parseInt(droppedColumnTokens[droppedColumnTokens.length - 1]);
        String[] draggedColumnTokens = draggedColumnId.split(":");
        int draggedColumnIndex = Integer.parseInt(draggedColumnTokens[draggedColumnTokens.length - 1]);

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		String columnRemoved = reportDataTo.getSelectedOrderedColumnList().remove(draggedColumnIndex);
		reportDataTo.getSelectedOrderedColumnList().add(droppedColumnIndex,columnRemoved);
	}

	private void onSelectedFilterReorder(){
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String droppedColumnId = params.get("droppedColumnId");
        String draggedColumnId = params.get("draggedColumnId");
        String[] droppedColumnTokens = droppedColumnId.split(":");
        int droppedColumnIndex = Integer.parseInt(droppedColumnTokens[droppedColumnTokens.length - 1]);
        String[] draggedColumnTokens = draggedColumnId.split(":");
        int draggedColumnIndex = Integer.parseInt(draggedColumnTokens[draggedColumnTokens.length - 1]);

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		String filterRemoved = reportDataTo.getSelectedOrderedFilterList().remove(draggedColumnIndex);
		reportDataTo.getSelectedOrderedFilterList().add(droppedColumnIndex,filterRemoved);
	}

	private void addNewUIColumn(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedReportColumn() == null){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"No Column Selection", "Please select some Column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if(reportDataTo.getAutoSelectedColumnHeader().equals("")){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Column Header", "Column header value is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		if(reportDataTo.getSelectedColumnsDataMap().keySet().contains(reportDataTo.getAutoSelectedColumnHeader())){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Column", "Already same name column is selected"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		IntermediateColumnDataTo intermediateColumnDataTo = new IntermediateColumnDataTo();
		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		ColumnTo selectedColumn = (ColumnTo)reportDataTo.getSelectedReportColumn().getData();
		if(selectedColumn.getColumnTableTo().getTableType() == EnumTableType.NORMAL){
			processingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getColumnProcessingLogicTemplate());
		}else if(selectedColumn.getColumnTableTo().getTableType() == EnumTableType.GENERIC){
			processingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getGenericColumnProcessingLogicTemplate());
		}else if(selectedColumn.getColumnTableTo().getTableType() == EnumTableType.GROUPING){
			processingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getGroupingColumnProcessingLogicTemplate());
		}else if(selectedColumn.getColumnTableTo().getTableType() == EnumTableType.FUNCTION){
			processingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getGenericColumnProcessingLogicTemplate());
			intermediateColumnDataTo.setFunctionColumnFlag(true);
		}
		createProcessingLogicDefinition(processingLogicDefinitionTo);
		processingLogicDefinitionTo.setFirstLogicValue(selectedColumn);
		reportDataTo.setSelectedColumnProcessingLogicDefinationTo(processingLogicDefinitionTo);

		ProcessingLogicDefinitionTo processingLogicDefinitionToClone = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionToClone.setProcessingLogicTemplate(processingLogicDefinitionTo.getProcessingLogicTemplate());
		createProcessingLogicDefinition(processingLogicDefinitionToClone);
		processingLogicDefinitionToClone.setFirstLogicValue(selectedColumn);
		intermediateColumnDataTo.setProcessingLogicDefination(processingLogicDefinitionToClone);
		intermediateColumnDataTo.setName(reportDataTo.getAutoSelectedColumnHeader());
		intermediateColumnDataTo.setVisibility(reportDataTo.isAutoSelectedColumnVisible());
		if(reportDataTo.getAutoSelectedGroupingFunction()!=null){
			intermediateColumnDataTo.setGroupingFunction(EnumSQLFunctionAndKeyword.valueOf(reportDataTo.getAutoSelectedGroupingFunction().toUpperCase()));
		}

		intermediateColumnDataTo.setDataType(reportDataTo.getAutoSelectedColumnDataType());
		reportDataTo.getSelectedColumnsDataMap().put(reportDataTo.getAutoSelectedColumnHeader(), intermediateColumnDataTo);
		if(createReportDataBean.getDroppedColumnIndex()==null){
			reportDataTo.getSelectedOrderedColumnList().add(reportDataTo.getAutoSelectedColumnHeader());
		}else{
			reportDataTo.getSelectedOrderedColumnList().add(createReportDataBean.getDroppedColumnIndex(), reportDataTo.getAutoSelectedColumnHeader());
			createReportDataBean.setDroppedColumnIndex(null);
		}
	}

	private void onSelectFilter(SelectEvent event) {
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		IntermediateFilterGroupTo intermediateFilterGroupTo = reportDataTo.getSelectedFilterGroupMap().get(event.getObject().toString());
		String filterGroupString = "";
		int i=0;
		for(IntermediateFilterDataTo intermediateFilterDataTo : intermediateFilterGroupTo.getIntermediateFilterDataToList()){
			filterGroupString += "Filter "+ (++i) + " : ";
			filterGroupString += intermediateFilterDataTo.getSelectedFilterName() + " ";
			filterGroupString += "FilterType : " + intermediateFilterDataTo.getSelectedFilterType() + " ";
			filterGroupString += "FilterValueType : " + intermediateFilterDataTo.getSelectedFilterValueType() + " ";
		}
		reportDataTo.setSelectedOrderedFilterListValue(filterGroupString);
	}

	private void removeSelectedFilterGroup(String filter){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.getFilterGroupToList().remove(filter);
		reportDataTo.getSelectedOrderedFilterList().remove(filter);
		reportDataTo.setSelectedOrderedFilterListValue("");
		reportDataTo.getFilterGroupToMapForTable().get(0).remove(filter);
		reportDataTo.getSelectedFilterGroupMap().remove(filter);
	}

	private void viewSelectedFilterGroup(String filter){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		IntermediateFilterGroupTo intermediateFilterGroupTo = reportDataTo.getSelectedFilterGroupMap().get(filter);
		reportDataTo.setCurentlySelectedFilterDataToList(intermediateFilterGroupTo.getIntermediateFilterDataToList());
		createReportDataBean.setDisableFilterFlag(true);
		reportDataTo.setSelectedFilterCombinationOperator(intermediateFilterGroupTo.getSelectedFilterCombinationOperator());
		reportDataTo.setSelectedFilterGroupHeader(intermediateFilterGroupTo.getSelectedFilterGroupHeader());
		reportDataTo.setSelectedNumberFilters(intermediateFilterGroupTo.getSelectedNumberFilters());
		reportDataTo.setOptionalFilterGroupFlag(intermediateFilterGroupTo.isOptionalFilterGroupFlag());
	}

	private void addFilterGroup(){
		int index=0;
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		for(IntermediateFilterDataTo intermediateFilterDataTo : reportDataTo.getIntermediateFilterDataToList()){
			intermediateFilterDataTo.setSelectedFilterLabel(intermediateFilterDataTo.getSelectedFilterName());
			if(intermediateFilterDataTo.getSelectedFilterName().equals(CONSTANT_EMPTY)){
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:tabView2:filterLabelName"+index,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Filter Name Empty",
						"Filter Name is required for Filter"+(index+1)));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}
		if(reportDataTo.getSelectedFilterGroupMap().keySet().contains(reportDataTo.getSelectedFilterGroupHeader())){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forFilterGroupHeaderError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Filter Group Header", "Already same name filter group header is created"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		IntermediateFilterGroupTo intermediateFilterGroupTo = new IntermediateFilterGroupTo();
		intermediateFilterGroupTo.setIntermediateFilterDataToList(reportDataTo.getIntermediateFilterDataToList());
		intermediateFilterGroupTo.setOptionalFilterGroupFlag(reportDataTo.isOptionalFilterGroupFlag());
		intermediateFilterGroupTo.setSelectedFilterCombinationOperator(reportDataTo.getSelectedFilterCombinationOperator());
		intermediateFilterGroupTo.setSelectedFilterGroupHeader(reportDataTo.getSelectedFilterGroupHeader());
		intermediateFilterGroupTo.setSelectedNumberFilters(reportDataTo.getSelectedNumberFilters());
		reportDataTo.getSelectedFilterGroupMap().put(reportDataTo.getSelectedFilterGroupHeader(), intermediateFilterGroupTo);
		reportDataTo.getSelectedOrderedFilterList().add(reportDataTo.getSelectedFilterGroupHeader());

		List<FilterGroupTo> filterGroupToList = new ArrayList<FilterGroupTo>();
		Map<String,IntermediateFilterGroupTo> filterGroupToMap = reportDataTo.getSelectedFilterGroupMap();
		Map<String,FilterGroupTo> filterGroupToMapForTable = new HashMap<String,FilterGroupTo>();
		for(Map.Entry<String, IntermediateFilterGroupTo> entry: filterGroupToMap.entrySet()){
			IntermediateFilterGroupTo tempIntermediateFilterGroupTo = entry.getValue();
			FilterGroupTo filterGroupTo = new FilterGroupTo();
			filterGroupTo.setFilterDefinitionHeader(tempIntermediateFilterGroupTo.getSelectedFilterGroupHeader());
			filterGroupTo.setOptional(tempIntermediateFilterGroupTo.isOptionalFilterGroupFlag());
			if(tempIntermediateFilterGroupTo.getSelectedFilterCombinationOperator()!=null){
				filterGroupTo.setFilterCombinationOperator(EnumFilterCombinationOperator.valueOf(tempIntermediateFilterGroupTo.getSelectedFilterCombinationOperator().getLabel().toUpperCase()));
			}
			List<FilterInputTo> filterValues = new ArrayList<FilterInputTo>();
			for(IntermediateFilterDataTo intermediateFilterDataTo : tempIntermediateFilterGroupTo.getIntermediateFilterDataToList()){
				FilterInputTo filterInputTo = new FilterInputTo();
				filterInputTo.setDefaultOperator(intermediateFilterDataTo.getSelectedDefaultOperator());
				filterInputTo.setFilterType(EnumFilterType.valueOf(intermediateFilterDataTo.getSelectedFilterType().toUpperCase()));
				filterInputTo.setOperatorVisible(intermediateFilterDataTo.isSelectedOperatorVisibility());
				filterInputTo.setInputOperators(intermediateFilterDataTo.getSelectedOperatorsList());
				filterInputTo.setSysFilterLabel(intermediateFilterDataTo.getSelectedFilterLabel());
				filterInputTo.setSysFilterName(intermediateFilterDataTo.getSelectedFilterName());
				filterInputTo.setSelectedOperatorDataType(intermediateFilterDataTo.getSelectedOperatorDataType());
				filterInputTo.setApplyOnProcessingLogicDefinition(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicDefinationTo());
				if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_SYSTEMINPUT) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
					ProcessingLogicDefinitionTo valueProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
					valueProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getApplicationInputProcessingLogicTemplate());
					valueProcessingLogicDefinitionTo.setFirstLogicValue(intermediateFilterDataTo.getSelectedApplicationInput());
					filterInputTo.setValueProcessingLogicDefinition(valueProcessingLogicDefinitionTo);
					filterGroupTo.setOptional(false);
				}else if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_SYSTEMINPUT) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_CONSTANT)){
					ProcessingLogicDefinitionTo constantProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
					constantProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getConstantInputProcessingLogicTemplate());
					constantProcessingLogicDefinitionTo.setFirstLogicValue(intermediateFilterDataTo.getSelectedConstant());
					filterInputTo.setValueProcessingLogicDefinition(constantProcessingLogicDefinitionTo);
				}
				if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_LIST) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_USERINPUT)){
					filterInputTo.setValueProcessingLogicDefinition(intermediateFilterDataTo.getSelectedValueProcessingLogicDefinationTo());
					filterInputTo.setLabelProcessingLogicDefinition(intermediateFilterDataTo.getSelectedLabelProcessingLogicDefinationTo());
					filterInputTo.setFilterValuesTableId(intermediateFilterDataTo.getSelectedFilterValueTable().getId());
					filterInputTo.setFilterValuesTablesType(intermediateFilterDataTo.getSelectedFilterValueTable().getTableType());
				}
				filterValues.add(filterInputTo);
			}
			filterGroupTo.setFilterValues(filterValues);
			filterGroupToMapForTable.put(filterGroupTo.getFilterDefinitionHeader(), filterGroupTo);
			filterGroupToList.add(filterGroupTo);
		}
		reportDataTo.getFilterGroupToList().clear();
		reportDataTo.getFilterGroupToList().addAll(filterGroupToList);
		reportDataTo.getFilterGroupToMapForTable().clear();
		reportDataTo.getFilterGroupToMapForTable().add(filterGroupToMapForTable);
	}

	private void onSelectColumn(SelectEvent event) {
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		ProcessingLogicDefinitionTo processingLogicDefinitionTo = reportDataTo.getSelectedColumnsDataMap().get(event.getObject().toString()).getProcessingLogicDefination();
		reportDataTo.setSelectedOrderedColumnListValue("");
		createSelectedColumnValue(processingLogicDefinitionTo);
    }

	private void createSelectedColumnValue(ProcessingLogicDefinitionTo processingLogicDefinitionTo){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
			reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicValue()));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT) ||
				processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
			reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((SelectItem)processingLogicDefinitionTo.getFirstLogicValue()).getLabel());
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
			reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((OperatorTo)processingLogicDefinitionTo.getFirstLogicValue()).getOperatorLabel());
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			createSelectedColumnValue((ProcessingLogicDefinitionTo)processingLogicDefinitionTo.getFirstLogicValue());
		}else{
			reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((ColumnTo)processingLogicDefinitionTo.getFirstLogicValue()).getColumnTableTo().getName()+CONSTANT_DOT+
					((ColumnTo)processingLogicDefinitionTo.getFirstLogicValue()).getColumn().getLabel());
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicValue()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT) ||
					processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((SelectItem)processingLogicDefinitionTo.getSecondLogicValue()).getLabel());
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((OperatorTo)processingLogicDefinitionTo.getSecondLogicValue()).getOperatorLabel());
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				createSelectedColumnValue((ProcessingLogicDefinitionTo)processingLogicDefinitionTo.getSecondLogicValue());
			}else{
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((ColumnTo)processingLogicDefinitionTo.getSecondLogicValue()).getColumnTableTo().getName()+CONSTANT_DOT+
						((ColumnTo)processingLogicDefinitionTo.getSecondLogicValue()).getColumn().getLabel());
			}
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicValue()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT) ||
					processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((SelectItem)processingLogicDefinitionTo.getThirdLogicValue()).getLabel());
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((OperatorTo)processingLogicDefinitionTo.getThirdLogicValue()).getOperatorLabel());
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				createSelectedColumnValue((ProcessingLogicDefinitionTo)processingLogicDefinitionTo.getThirdLogicValue());
			}else{
				reportDataTo.setSelectedOrderedColumnListValue(reportDataTo.getSelectedOrderedColumnListValue()+((ColumnTo)processingLogicDefinitionTo.getThirdLogicValue()).getColumnTableTo().getName()+CONSTANT_DOT+
						((ColumnTo)processingLogicDefinitionTo.getThirdLogicValue()).getColumn().getLabel());
			}
		}
	}

	private void setInitialIntermediateFilterDataTo(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.setSelectedFilterGroupHeader(null);
		reportDataTo.setOptionalFilterGroupFlag(true);
		reportDataTo.setSelectedNumberFilters(1);
		reportDataTo.setSelectedFilterCombinationOperator(null);
		int filterNumber = 1;
		reportDataTo.setIntermediateFilterDataToList(new ArrayList<IntermediateFilterDataTo>());
		createReportDataBean.setDisableFilterFlag(true);
		for(int i=0;i<filterNumber;i++){
			IntermediateFilterDataTo intermediateFilterDataTo = new IntermediateFilterDataTo();
			intermediateFilterDataTo.setSelectedOperatorDataType(commonReportDataBean.getDataTypes().get(0));
			intermediateFilterDataTo.setOperatorsList(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(intermediateFilterDataTo.getSelectedOperatorDataType().getValue())));
			intermediateFilterDataTo.setSelectedOperatorsList(intermediateFilterDataTo.getOperatorsList());
			intermediateFilterDataTo.setSelectedDefaultOperator(intermediateFilterDataTo.getSelectedOperatorsList().get(0));
			intermediateFilterDataTo.setSelectedFilterValueType(CONSTANT_APPLICTIONINPUT);
			intermediateFilterDataTo.setSelectedFilterType(CONSTANT_SYSTEMINPUT);
			String processingLogicTemplateName = reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().get(0);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicTemplateName(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedLabelProcessingLogicTemplateName(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedValueProcessingLogicTemplateName(processingLogicTemplateName);
			List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			intermediateFilterDataTo.setSelectedValueProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			intermediateFilterDataTo.setSelectedLabelProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			ProcessingLogicDefinitionTo applyOnProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			applyOnProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinition(applyOnProcessingLogicDefinitionTo);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicDefinationTo(applyOnProcessingLogicDefinitionTo);
			intermediateFilterDataTo.setSelectedFilterValueTable(reportDataTo.getSelectedTables().get(0));
			ProcessingLogicDefinitionTo labelProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			labelProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinitionForLabelValue(labelProcessingLogicDefinitionTo,intermediateFilterDataTo.getSelectedFilterValueTable());
			intermediateFilterDataTo.setSelectedLabelProcessingLogicDefinationTo(labelProcessingLogicDefinitionTo);
			ProcessingLogicDefinitionTo valueProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			valueProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinitionForLabelValue(valueProcessingLogicDefinitionTo,intermediateFilterDataTo.getSelectedFilterValueTable());
			intermediateFilterDataTo.setSelectedValueProcessingLogicDefinationTo(valueProcessingLogicDefinitionTo);
			reportDataTo.getIntermediateFilterDataToList().add(intermediateFilterDataTo);
		}
	}

	private void updateIntermediateFilterDataTo(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedFilterGroupHeader().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forFilterGroupHeaderError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Filter Group Header", "Filter Group Header value is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		createReportDataBean.setDisableFilterFlag(false);
		int filterNumber = reportDataTo.getSelectedNumberFilters();
		reportDataTo.setIntermediateFilterDataToList(new ArrayList<IntermediateFilterDataTo>());
		for(int i=0;i<filterNumber;i++){
			IntermediateFilterDataTo intermediateFilterDataTo = new IntermediateFilterDataTo();
			intermediateFilterDataTo.setSelectedOperatorDataType(commonReportDataBean.getDataTypes().get(0));
			intermediateFilterDataTo.setOperatorsList(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(intermediateFilterDataTo.getSelectedOperatorDataType().getValue())));
			intermediateFilterDataTo.setSelectedOperatorsList(intermediateFilterDataTo.getOperatorsList());
			intermediateFilterDataTo.setSelectedDefaultOperator(intermediateFilterDataTo.getSelectedOperatorsList().get(0));
			intermediateFilterDataTo.setSelectedFilterValueType(CONSTANT_APPLICTIONINPUT);
			intermediateFilterDataTo.setSelectedFilterType(CONSTANT_SYSTEMINPUT);
			String processingLogicTemplateName = reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().get(0);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicTemplateName(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedLabelProcessingLogicTemplateName(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedValueProcessingLogicTemplateName(processingLogicTemplateName);
			List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(processingLogicTemplateName);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			intermediateFilterDataTo.setSelectedValueProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			intermediateFilterDataTo.setSelectedLabelProcessingLogicTemplate(processingLogicTemplateToList.get(0));
			ProcessingLogicDefinitionTo applyOnProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			applyOnProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinition(applyOnProcessingLogicDefinitionTo);
			intermediateFilterDataTo.setSelectedApplyOnProcessingLogicDefinationTo(applyOnProcessingLogicDefinitionTo);
			intermediateFilterDataTo.setSelectedFilterValueTable(reportDataTo.getSelectedTables().get(0));
			ProcessingLogicDefinitionTo labelProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			labelProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinitionForLabelValue(labelProcessingLogicDefinitionTo,intermediateFilterDataTo.getSelectedFilterValueTable());
			intermediateFilterDataTo.setSelectedLabelProcessingLogicDefinationTo(labelProcessingLogicDefinitionTo);
			ProcessingLogicDefinitionTo valueProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			valueProcessingLogicDefinitionTo.setProcessingLogicTemplate(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicTemplate());
			createProcessingLogicDefinitionForLabelValue(valueProcessingLogicDefinitionTo,intermediateFilterDataTo.getSelectedFilterValueTable());
			intermediateFilterDataTo.setSelectedValueProcessingLogicDefinationTo(valueProcessingLogicDefinitionTo);
			reportDataTo.getIntermediateFilterDataToList().add(intermediateFilterDataTo);
		}
	}

	private void updateApplyOnFilterSelectedProcessingLogicTemplate(long val){
		int index = (new Long(val)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedApplyOnProcessingLogicTemplateName());
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedApplyOnProcessingLogicTemplate(processingLogicTemplateToList.get(0));

		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionTo.setProcessingLogicTemplate(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedApplyOnProcessingLogicTemplate());
		createProcessingLogicDefinition(processingLogicDefinitionTo);
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedApplyOnProcessingLogicDefinationTo(processingLogicDefinitionTo);

	}

	private void updateFilterValueLabelSelectedProcessingLogicTemplate(long val){
		int index = (new Long(val)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().clear();
		for(String template: commonReportDataBean.getProcessingLogicTemplatesMap().keySet()){
			if(template.contains(CONSTANT_PROCESSINGLOGIC) || template.contains(CONSTANT_APPLICTIONINPUT) ||
					template.contains(CONSTANT_CONSTANT) || template.contains(CONSTANT_OPERATOR)){
				reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().add(template);
			}else{
				EnumTableType selectedTableType = reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedFilterValueTable().getTableType();
				String comparisonString = CONSTANT_EMPTY;
				if(selectedTableType==EnumTableType.NORMAL){
					comparisonString = CONSTANT_COLUMN;
				}else if(selectedTableType==EnumTableType.GENERIC){
					comparisonString = CONSTANT_DERIVEDGENERICCOLUMN;
				}else if(selectedTableType==EnumTableType.GROUPING){
					comparisonString = CONSTANT_DERIVEDGROUPINGCOLUMN;
				}
				if(template.equals(comparisonString)||template.startsWith(comparisonString+CONSTANT_UNDERSCORE)||template.endsWith(CONSTANT_UNDERSCORE+comparisonString)||
						template.contains(CONSTANT_UNDERSCORE+comparisonString+CONSTANT_UNDERSCORE)){
					reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().add(template);
					continue;
				}
			}
		}
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedLabelProcessingLogicTemplateName((commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().get(0))).get(0).getProcessingElementName());
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedValueProcessingLogicTemplateName((commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getUpdatedFilterValueColumnProcessingLogicTemplateNameList().get(0))).get(0).getProcessingElementName());
		updateLabelFilterSelectedProcessingLogicTemplate(val,reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedFilterValueTable());
		updateValueFilterSelectedProcessingLogicTemplate(val,reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedFilterValueTable());
	}

	private void updateLabelFilterSelectedProcessingLogicTemplate(long val,TableTo table){
		int index = (new Long(val)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedLabelProcessingLogicTemplateName());
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedLabelProcessingLogicTemplate(processingLogicTemplateToList.get(0));

		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionTo.setProcessingLogicTemplate(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedLabelProcessingLogicTemplate());
		createProcessingLogicDefinitionForLabelValue(processingLogicDefinitionTo,table);
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedLabelProcessingLogicDefinationTo(processingLogicDefinitionTo);
	}

	private void updateValueFilterSelectedProcessingLogicTemplate(long val,TableTo table){
		int index = (new Long(val)).intValue();
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedValueProcessingLogicTemplateName());
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedValueProcessingLogicTemplate(processingLogicTemplateToList.get(0));

		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionTo.setProcessingLogicTemplate(reportDataTo.getIntermediateFilterDataToList().get(index).getSelectedValueProcessingLogicTemplate());
		createProcessingLogicDefinitionForLabelValue(processingLogicDefinitionTo,table);
		reportDataTo.getIntermediateFilterDataToList().get(index).setSelectedValueProcessingLogicDefinationTo(processingLogicDefinitionTo);
	}

	private void createProcessingLogicDefinition(ProcessingLogicDefinitionTo processingLogicDefinitionTo){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
					processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicDataType().getValue()))));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			ProcessingLogicDefinitionTo firstProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			firstProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
					Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicValue())));
			createProcessingLogicDefinition(firstProcessingLogicDefinitionTo);
			processingLogicDefinitionTo.setFirstLogicValue(firstProcessingLogicDefinitionTo);
		}else{
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>());
			ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
			for(ColumnTo columnTo: reportDataTo.getUsedSelectedTablesColumns()){
				if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
					processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
				}
				if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
					processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
				}
				if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
					processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
				}
			}
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
						processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicDataType().getValue()))));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				ProcessingLogicDefinitionTo secondProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
				secondProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
						Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicValue())));
				createProcessingLogicDefinition(secondProcessingLogicDefinitionTo);
				processingLogicDefinitionTo.setSecondLogicValue(secondProcessingLogicDefinitionTo);
			}else{
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>());
				ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
				for(ColumnTo columnTo: reportDataTo.getUsedSelectedTablesColumns()){
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
						processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
						processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
						processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
					}
				}
			}
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
						processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicDataType().getValue()))));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				ProcessingLogicDefinitionTo thirdProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
				thirdProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
						Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicValue())));
				createProcessingLogicDefinition(thirdProcessingLogicDefinitionTo);
				processingLogicDefinitionTo.setThirdLogicValue(thirdProcessingLogicDefinitionTo);
			}else{
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>());
				ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
				for(ColumnTo columnTo: reportDataTo.getUsedSelectedTablesColumns()){
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
						processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
						processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
						processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
					}
				}
			}
		}
	}

	private void createProcessingLogicDefinitionForLabelValue(ProcessingLogicDefinitionTo processingLogicDefinitionTo,TableTo table){
		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
					processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicDataType().getValue()))));
		}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			ProcessingLogicDefinitionTo firstProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
			firstProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
					Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getFirstLogicValue())));
			createProcessingLogicDefinitionForLabelValue(firstProcessingLogicDefinitionTo,table);
			processingLogicDefinitionTo.setFirstLogicValue(firstProcessingLogicDefinitionTo);
		}else{
			processingLogicDefinitionTo.setFirstLogicFilteredValuesList(new ArrayList<Object>());
			ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
			if(commonReportDataBean.getAllTableColumnsMap().get(table)!=null){
				for(ColumnTo columnTo: commonReportDataBean.getAllTableColumnsMap().get(table)){
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
						processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
						processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
					}
					if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
						processingLogicDefinitionTo.getFirstLogicFilteredValuesList().add(columnTo);
					}
				}
			}
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
						processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicDataType().getValue()))));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				ProcessingLogicDefinitionTo secondProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
				secondProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
						Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getSecondLogicValue())));
				createProcessingLogicDefinitionForLabelValue(secondProcessingLogicDefinitionTo,table);
				processingLogicDefinitionTo.setSecondLogicValue(secondProcessingLogicDefinitionTo);
			}else{
				processingLogicDefinitionTo.setSecondLogicFilteredValuesList(new ArrayList<Object>());
				ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
				if(commonReportDataBean.getAllTableColumnsMap().get(table)!=null){
					for(ColumnTo columnTo: commonReportDataBean.getAllTableColumnsMap().get(table)){
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
							processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
						}
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
							processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
						}
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
							processingLogicDefinitionTo.getSecondLogicFilteredValuesList().add(columnTo);
						}
					}
				}
			}
		}

		if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType()!=null){
			if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getApplicationInputList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_CONSTANT)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getConstantList()));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_OPERATOR)){
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>(commonReportDataBean.getOperatorDataTypeMap().get(Integer.valueOf(
						processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicDataType().getValue()))));
			}else if(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
				ProcessingLogicDefinitionTo thirdProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
				thirdProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMapById().get(
						Integer.valueOf(processingLogicDefinitionTo.getProcessingLogicTemplate().getThirdLogicValue())));
				createProcessingLogicDefinitionForLabelValue(thirdProcessingLogicDefinitionTo,table);
				processingLogicDefinitionTo.setThirdLogicValue(thirdProcessingLogicDefinitionTo);
			}else{
				processingLogicDefinitionTo.setThirdLogicFilteredValuesList(new ArrayList<Object>());
				ProcessingLogicTemplateTo processingLogicTemplateTo = processingLogicDefinitionTo.getProcessingLogicTemplate();
				if(commonReportDataBean.getAllTableColumnsMap().get(table)!=null){
					for(ColumnTo columnTo: commonReportDataBean.getAllTableColumnsMap().get(table)){
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
							processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
						}
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
							processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
						}
						if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
							processingLogicDefinitionTo.getThirdLogicFilteredValuesList().add(columnTo);
						}
					}
				}
			}
		}
	}

	private void updateSelectedProcessingLogicTemplate(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(reportDataTo.getSelectedProcessingLogicTemplateName());
		reportDataTo.setSelectedProcessingLogicTemplate(processingLogicTemplateToList.get(0));

		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionTo.setProcessingLogicTemplate(reportDataTo.getSelectedProcessingLogicTemplate());
		createProcessingLogicDefinition(processingLogicDefinitionTo);
		reportDataTo.setSelectedColumnProcessingLogicDefinationTo(processingLogicDefinitionTo);
	}

	private void resetColumnSelectedValues(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		String processingLogicTemplateName = reportDataTo.getUpdatedColumnProcessingLogicTemplateNameList().get(0);
		reportDataTo.setSelectedProcessingLogicTemplateName(processingLogicTemplateName);
		List<ProcessingLogicTemplateTo> processingLogicTemplateToList=commonReportDataBean.getProcessingLogicTemplatesMap().get(processingLogicTemplateName);
		reportDataTo.setSelectedProcessingLogicTemplate(processingLogicTemplateToList.get(0));
		reportDataTo.setSelectedColumnHeader(null);
		reportDataTo.setColumnVisible(true);
		reportDataTo.setSelectedGroupingFunction(null);
		reportDataTo.setSelectedColumnDataType(null);
		reportDataTo.setOrderByFlag(false);
		reportDataTo.setOrderByFunction(null);
		ProcessingLogicDefinitionTo processingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
		processingLogicDefinitionTo.setProcessingLogicTemplate(reportDataTo.getSelectedProcessingLogicTemplate());
		createProcessingLogicDefinition(processingLogicDefinitionTo);
		reportDataTo.setSelectedColumnProcessingLogicDefinationTo(processingLogicDefinitionTo);

		reportDataTo.setFilteredUsedSelectedTablesColumns(new ArrayList<ColumnTo>());
		ProcessingLogicTemplateTo processingLogicTemplateTo = reportDataTo.getSelectedProcessingLogicTemplate();
		for(ColumnTo columnTo: reportDataTo.getUsedSelectedTablesColumns()){
			if(columnTo.getColumnTableTo().getTableType()==EnumTableType.NORMAL){
				if(processingLogicTemplateTo.getFirstLogicType()!=null && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getSecondLogicType()!=null && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getThirdLogicType()!=null && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_COLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}
			}
			if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GENERIC){
				if(processingLogicTemplateTo.getFirstLogicType()!=null && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getSecondLogicType()!=null && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getThirdLogicType()!=null && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGENERICCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}
			}
			if(columnTo.getColumnTableTo().getTableType()==EnumTableType.GROUPING){
				if(processingLogicTemplateTo.getFirstLogicType()!=null && processingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getSecondLogicType()!=null && processingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}else if(processingLogicTemplateTo.getThirdLogicType()!=null && processingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_DERIVEDGROUPINGCOLUMN)){
					reportDataTo.getFilteredUsedSelectedTablesColumns().add(columnTo);
				}
			}
		}
		createReportDataBean.setAddCssFlag(false);
		reportDataTo.setSelectedColumnCss(null);
		reportDataTo.setSelectedColumnCssOperator(null);
		reportDataTo.setSelectedColumnDataType(null);
	}

	private void resetAddDerivedTableDialogValues(){
		createReportDataBean.setTableDescription("");
		createReportDataBean.setDerivedTableNameForCreation("");
		createReportDataBean.setSelectedTableType("Generic");
		createReportDataBean.setOrgAdminFlag("false");
		createReportDataBean.setDefaultDerivedTableFlag("false");
		createReportDataBean.setSelectedDerivedTableModule(commonReportDataBean.getCommonModuleList().get(0));
	}

	private void addNewDerivedTable(){
		List<TableTo> tableList = new ArrayList<TableTo>();
		tableList.addAll(commonReportDataBean.getNormalTables().getSource());
		tableList.addAll(commonReportDataBean.getDerivedTables().getSource());
		tableList.addAll(commonReportDataBean.getDefaultTables());

		if(createReportDataBean.getDerivedTableNameForCreation().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forDerivedTableAdditionErrorMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Object Name", "Object Name is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		if(createReportDataBean.getTableDescription().equals(CONSTANT_EMPTY)){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forDerivedTableAdditionErrorMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Object Description", "Object Description is required"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		for(TableTo table : tableList){
			if(table.getName().equalsIgnoreCase(createReportDataBean.getDerivedTableNameForCreation())){
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forDerivedTableAdditionErrorMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Object","Object with name " + createReportDataBean.getDerivedTableNameForCreation() + " already exists."));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = new ReportList.ReportListNode<ReportDataTo>();
		ReportDataTo reportDataTo = new ReportDataTo();
		reportDataTo.setUserId(userSessionBean.getUserID().intValue());
		reportDataTo.setColumnToList(new ArrayList<ColumnTo>());
		List<IntermediateJoinDataTo> joinDataList = new ArrayList<IntermediateJoinDataTo>();
		IntermediateJoinDataTo intermediateJoinDataTo = new IntermediateJoinDataTo();
		intermediateJoinDataTo.setRenderUpdatedTableListFlag(false);
		joinDataList.add(intermediateJoinDataTo);
		reportDataTo.setJoinDataList(joinDataList);
		reportDataTo.setName(createReportDataBean.getDerivedTableNameForCreation());
		String tableType = createReportDataBean.getSelectedTableType();
		if(tableType.toLowerCase().equals(EnumTableType.GENERIC.getName().toLowerCase())){
			reportDataTo.setTableType(EnumTableType.GENERIC);
			reportDataTo.setTableTypeName("Simple Derived Object");
		}else if(tableType.toLowerCase().equals(EnumTableType.GROUPING.getName().toLowerCase())){
			reportDataTo.setTableType(EnumTableType.GROUPING);
			reportDataTo.setTableTypeName("Grouping Derived Object");
		}
		reportDataTo.setDescription(createReportDataBean.getTableDescription());
		reportDataTo.setDefaultFlag(Boolean.valueOf(createReportDataBean.getOrgAdminFlag()));
		if(createReportDataBean.getDefaultDerivedTableFlag() != null){
			reportDataTo.setDefaultDerivedTableFlag(Boolean.valueOf(createReportDataBean.getDefaultDerivedTableFlag()));
		}else{
			reportDataTo.setDefaultDerivedTableFlag(Boolean.valueOf("false"));
		}
		reportDataTo.setRenderAddJoinFlag(false);
		reportDataTo.setRenderRemoveJoinFlag(false);
		reportDataTo.setState(0);
		reportDataTo.setCurrentTab(0);
		reportDataTo.setSelectedColumnsDataMap(new HashMap<String,IntermediateColumnDataTo>());
		reportDataTo.setUpdatedColumnProcessingLogicTemplateNameList(new ArrayList<String>());
		reportDataTo.setUpdatedFilterValueColumnProcessingLogicTemplateNameList(new ArrayList<String>());
		reportDataTo.setSelectedOrderedColumnList(new ArrayList<String>());
		List<TableTo> systemTables = commonReportDataBean.getNormalTables().getSource();
		List<TableTo> derivedTables = commonReportDataBean.getDerivedTables().getSource();
		reportDataTo.setNormalTables(new DualListModel<TableTo>(new ArrayList<TableTo>(systemTables),new ArrayList<TableTo>(commonReportDataBean.getNormalTables().getTarget())));
		reportDataTo.setDerivedTables(new DualListModel<TableTo>(new ArrayList<TableTo>(derivedTables),new ArrayList<TableTo>(commonReportDataBean.getDerivedTables().getTarget())));
		TreeNode sourceTableRoot = new DefaultTreeNode("Root1", null);

		if(reportDetailBean.getOrgID() == null){
			TreeNode systemTablesNode = new DefaultTreeNode("System Tables", sourceTableRoot);
			systemTablesNode.setSelectable(false);
			for(TableTo tableTo : systemTables){
				systemTablesNode.getChildren().add(new DefaultTreeNode(tableTo,systemTablesNode));
			}
			systemTablesNode.setExpanded(true);
		}else{
			reportDataTo.setOrganization(reportDetailBean.getOrgID());
		}

        TreeNode derivedTablesNode = new DefaultTreeNode("Derived Objects", sourceTableRoot);
        derivedTablesNode.setSelectable(false);
        for(TableTo tableTo : derivedTables){
        	derivedTablesNode.getChildren().add(new DefaultTreeNode(tableTo,derivedTablesNode));
        }
        derivedTablesNode.setExpanded(true);

        reportDataTo.setSourceTablesRoot(sourceTableRoot);
		TreeNode selectedTableRoot = new DefaultTreeNode("Root2", null);
		if(reportDetailBean.getOrgID() == null){
			TreeNode selectedSystemTablesNode = new DefaultTreeNode("System Objects", selectedTableRoot);
			selectedSystemTablesNode.setSelectable(false);
			selectedSystemTablesNode.setExpanded(true);
		}
		TreeNode selectedDerivedTablesNode = new DefaultTreeNode("Derived Objects", selectedTableRoot);
		selectedDerivedTablesNode.setSelectable(false);
		selectedDerivedTablesNode.setExpanded(true);
        reportDataTo.setSelectedTablesRoot(selectedTableRoot);
        if(createReportDataBean.getSelectedDerivedTableModule() == null){
        	reportDataTo.setDerivedTableModule(commonReportDataBean.getCommonModuleList().get(0));
        }else{
        	reportDataTo.setDerivedTableModule(createReportDataBean.getSelectedDerivedTableModule());
        }
		currentPopulatedListNode.setData(reportDataTo);
		createReportDataBean.getCurrentPopulatedListNode().getData().setCurrentTab(0);
		createReportDataBean.setCurrentPopulatedListNode(currentPopulatedListNode);
		createReportDataBean.getReportList().add(currentPopulatedListNode);
	}

	private int persistInDatabase(ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode){
		int result = -1;
		try{
			result = CreateQuery.persistInDatabase(currentPopulatedListNode);
		}catch(Exception e){
			LOGGER.error("Error inside persistInDatabase(ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode)"
					+ " while persisting in database", e);
		}
		return result;
	}

	private void createTableOrReport(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedOrderedColumnList().size()==0){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Columns", "Please select at least one column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		List<FilterGroupTo> filterGroupToList = new ArrayList<FilterGroupTo>();
		Map<String,IntermediateFilterGroupTo> filterGroupToMap = reportDataTo.getSelectedFilterGroupMap();
		for(Map.Entry<String, IntermediateFilterGroupTo> entry: filterGroupToMap.entrySet()){
			IntermediateFilterGroupTo intermediateFilterGroupTo = entry.getValue();
			FilterGroupTo filterGroupTo = new FilterGroupTo();
			filterGroupTo.setFilterDefinitionHeader(intermediateFilterGroupTo.getSelectedFilterGroupHeader());
			filterGroupTo.setOptional(intermediateFilterGroupTo.isOptionalFilterGroupFlag());
			if(intermediateFilterGroupTo.getSelectedFilterCombinationOperator() != null){
				filterGroupTo.setFilterCombinationOperator(EnumFilterCombinationOperator.valueOf(intermediateFilterGroupTo.getSelectedFilterCombinationOperator().getLabel().toUpperCase()));
			}
			List<FilterInputTo> filterValues = new ArrayList<FilterInputTo>();
			for(IntermediateFilterDataTo intermediateFilterDataTo : intermediateFilterGroupTo.getIntermediateFilterDataToList()){
				FilterInputTo filterInputTo = new FilterInputTo();
				filterInputTo.setDefaultOperator(intermediateFilterDataTo.getSelectedDefaultOperator());
				filterInputTo.setFilterType(EnumFilterType.valueOf(intermediateFilterDataTo.getSelectedFilterType().toUpperCase()));
				filterInputTo.setOperatorVisible(intermediateFilterDataTo.isSelectedOperatorVisibility());
				filterInputTo.setInputOperators(intermediateFilterDataTo.getSelectedOperatorsList());
				filterInputTo.setSysFilterLabel(intermediateFilterDataTo.getSelectedFilterLabel());
				filterInputTo.setSysFilterName(intermediateFilterDataTo.getSelectedFilterName());
				filterInputTo.setSelectedOperatorDataType(intermediateFilterDataTo.getSelectedOperatorDataType());
				filterInputTo.setApplyOnProcessingLogicDefinition(intermediateFilterDataTo.getSelectedApplyOnProcessingLogicDefinationTo());
				if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_SYSTEMINPUT) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_APPLICTIONINPUT)){
					ProcessingLogicDefinitionTo valueProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
					valueProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getApplicationInputProcessingLogicTemplate());
					valueProcessingLogicDefinitionTo.setFirstLogicValue(intermediateFilterDataTo.getSelectedApplicationInput());
					filterInputTo.setValueProcessingLogicDefinition(valueProcessingLogicDefinitionTo);
				}else if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_SYSTEMINPUT) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_CONSTANT)){
					ProcessingLogicDefinitionTo constantProcessingLogicDefinitionTo = new ProcessingLogicDefinitionTo();
					constantProcessingLogicDefinitionTo.setProcessingLogicTemplate(commonReportDataBean.getConstantInputProcessingLogicTemplate());
					constantProcessingLogicDefinitionTo.setFirstLogicValue(intermediateFilterDataTo.getSelectedConstant());
					filterInputTo.setValueProcessingLogicDefinition(constantProcessingLogicDefinitionTo);
				}
				if(intermediateFilterDataTo.getSelectedFilterType().equalsIgnoreCase(CONSTANT_LIST) &&
						intermediateFilterDataTo.getSelectedFilterValueType().equalsIgnoreCase(CONSTANT_USERINPUT)){
					filterInputTo.setValueProcessingLogicDefinition(intermediateFilterDataTo.getSelectedValueProcessingLogicDefinationTo());
					filterInputTo.setLabelProcessingLogicDefinition(intermediateFilterDataTo.getSelectedLabelProcessingLogicDefinationTo());
					filterInputTo.setFilterValuesTableId(intermediateFilterDataTo.getSelectedFilterValueTable().getId());
					filterInputTo.setFilterValuesTablesType(intermediateFilterDataTo.getSelectedFilterValueTable().getTableType());
				}
				filterValues.add(filterInputTo);
			}
			filterGroupTo.setFilterValues(filterValues);
			filterGroupToList.add(filterGroupTo);
		}
		reportDataTo.setFilterGroupToList(filterGroupToList);

		if(reportDataTo.isDefaultFlag()){
			if(!checkFiltersForOrganizationApplicationInput(filterGroupToList, currentPopulatedListNode.getData().getUsedSelectedTables())){
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"OrganizationID Application Input Required", "No OrganizationID Application Input Filter Present"));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}

		int result = -1;
		result = persistInDatabase(currentPopulatedListNode);
		if(result!=-1){
			if(currentPopulatedListNode.getParent()!=null){
				try {
					TableTo newlyCreatedTable = CreateQuery.getDerivedTable(result);
					currentPopulatedListNode.getParent().getData().getDerivedTables().getSource().add(newlyCreatedTable);
					currentPopulatedListNode.getParent().getData().getSourceTablesRoot().getChildren().get(1).getChildren().add(new DefaultTreeNode(newlyCreatedTable,currentPopulatedListNode.getParent().getData().getSourceTablesRoot().getChildren().get(1)));
					commonReportDataBean.getAllTableColumnsMap().put(newlyCreatedTable, CreateQuery.getTableColumnsList(newlyCreatedTable));
				} catch (CreateQueryException e) {
					LOGGER.error("Error inside createTableOrReport() while adding to parentnode newly created derived table with id : "+ result, e);
				}
				createReportDataBean.setCurrentPopulatedListNode(currentPopulatedListNode.getParent());
				changeState(-1);
			}else{
				try {
					ReportDetail createdReportDetail = CreateQuery.getReportDetails(reportDataTo.getName());
					reportDetailBean.getReportDetailList().add(createdReportDetail);
					if(reportDetailBean.getReports().get(reportDataTo.getModule().getLabel())!=null){
						reportDetailBean.getReports().get(reportDataTo.getModule().getLabel()).add(createdReportDetail);
					}else{
						List<ReportDetail> reportDetailList = new ArrayList<ReportDetail>();
						reportDetailList.add(createdReportDetail);
						reportDetailBean.getReports().put(reportDataTo.getModule().getLabel(), reportDetailList);
					}
				} catch (CreateQueryException e) {
					LOGGER.error("Error inside createTableOrReport() while adding to reportDetailList newly created report with name : "+ reportDataTo.getName(), e);
				}
				//FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			}
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forTabViewMessage",new FacesMessage(FacesMessage.SEVERITY_INFO,"Report or Table Created",currentPopulatedListNode.getData().getName() + " created successfully"));
		}else{
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forTabViewMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report or Table Creation Failed","Unable to create "+currentPopulatedListNode.getData().getName()));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
	}

	private boolean checkFiltersForOrganizationApplicationInput(List<FilterGroupTo> filterGroupToList, List<TableTo> usedSelectedTables) {

		for(FilterGroupTo filterGroupTo: filterGroupToList){
			List<FilterInputTo> filterInputToList = filterGroupTo.getFilterValues();
			for(FilterInputTo filterInputTo : filterInputToList){
				if(filterInputTo.getValueProcessingLogicDefinition() != null && filterInputTo.getValueProcessingLogicDefinition().getProcessingLogicTemplate().equals(commonReportDataBean.getApplicationInputProcessingLogicTemplate()) &&
						(Integer.valueOf(((SelectItem)filterInputTo.getValueProcessingLogicDefinition().getFirstLogicValue()).getValue())).equals(commonReportDataBean.getApplicationInputOrganizationId())){
					return true;
				}
			}
		}

		List<TableTo> selectedDerivedTables = new ArrayList<TableTo>();
		List<TableTo> selectedNormalTables = new ArrayList<TableTo>();

		for(TableTo table : usedSelectedTables){
			if(table.getTableType() == EnumTableType.GENERIC || table.getTableType() == EnumTableType.GROUPING){
				selectedDerivedTables.add(table);
			}else if(table.getTableType() == EnumTableType.NORMAL){
				selectedNormalTables.add(table);
			}
		}

		if(selectedNormalTables.size()!=0){
			return false;
		}

		for(TableTo table: selectedDerivedTables){
			try {
				if(!checkFiltersForOrganizationApplicationInput(Filter.getDerivedTableFilters(table, reportDetailBean.getApplicationInputMap()), Filter.getUsedTables(table))){
					return false;
				}
			} catch (Exception e) {
				LOGGER.error("Error inside checkFiltersForOrganizationApplicationInput(List<FilterGroupTo> filterGroupToList, List<TableTo> usedSelectedTables) while "
						+ "getting (Filter.getDerivedTableFilters(table, reportDetailBean.getApplicationInputMap()) : ", e);
			}
		}

		return true;
	}

	private void resetIntermediateProcessingLogicTemplateTo(){
		IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
		intermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
		intermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
		intermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
		createReportDataBean.setIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo);
	}

	private void checkFirstProcessingLogicType(IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo){
		if(intermediateProcessingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			IntermediateProcessingLogicTemplateTo newIntermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
			newIntermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicDataType(commonReportDataBean.getDataTypes().get(0));
			intermediateProcessingLogicTemplateTo.setFirstTypeProcessingLogicTo(newIntermediateProcessingLogicTemplateTo);
		}else if(intermediateProcessingLogicTemplateTo.getFirstLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
			SelectItem firstLogicDataType = null;
			for(SelectItem dataType : commonReportDataBean.getDataTypes()){
				if(dataType.getLabel().equalsIgnoreCase(CONSTANT_STRING)){
					firstLogicDataType = dataType;
				}
			}
			intermediateProcessingLogicTemplateTo.setFirstLogicDataType(firstLogicDataType);
		}
	}

	private void checkSecondProcessingLogicType(IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo){
		if(intermediateProcessingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			IntermediateProcessingLogicTemplateTo newIntermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
			newIntermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicDataType(commonReportDataBean.getDataTypes().get(0));
			intermediateProcessingLogicTemplateTo.setSecondTypeProcessingLogicTo(newIntermediateProcessingLogicTemplateTo);
		}else if(intermediateProcessingLogicTemplateTo.getSecondLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
			SelectItem secondLogicDataType = null;
			for(SelectItem dataType : commonReportDataBean.getDataTypes()){
				if(dataType.getLabel().equalsIgnoreCase(CONSTANT_STRING)){
					secondLogicDataType = dataType;
				}
			}
			intermediateProcessingLogicTemplateTo.setSecondLogicDataType(secondLogicDataType);
		}
	}

	private void checkThirdProcessingLogicType(IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo){
		if(intermediateProcessingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
			IntermediateProcessingLogicTemplateTo newIntermediateProcessingLogicTemplateTo = new IntermediateProcessingLogicTemplateTo();
			newIntermediateProcessingLogicTemplateTo.setFirstLogicType(commonReportDataBean.getProcessingLogicTemplateTypeList().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicType(commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().get(0));
			newIntermediateProcessingLogicTemplateTo.setFirstLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setSecondLogicDataType(commonReportDataBean.getDataTypes().get(0));
			newIntermediateProcessingLogicTemplateTo.setThirdLogicDataType(commonReportDataBean.getDataTypes().get(0));
			intermediateProcessingLogicTemplateTo.setThirdTypeProcessingLogicTo(newIntermediateProcessingLogicTemplateTo);
		}else if(intermediateProcessingLogicTemplateTo.getThirdLogicType().getLabel().equalsIgnoreCase(CONSTANT_SQLSYNTAX)){
			SelectItem thirdLogicDataType = null;
			for(SelectItem dataType : commonReportDataBean.getDataTypes()){
				if(dataType.getLabel().equalsIgnoreCase(CONSTANT_STRING)){
					thirdLogicDataType = dataType;
				}
			}
			intermediateProcessingLogicTemplateTo.setThirdLogicDataType(thirdLogicDataType);
		}
	}

	private void createProcessingLogicTemplate(){
		IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo = createReportDataBean.getIntermediateProcessingLogicTemplateTo();
		if(checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo)){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Name is Required.", "ProcessingElementTemplate Name is Required."));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		try{
			if(CreateQuery.createProcessingLogicTemplate(intermediateProcessingLogicTemplateTo,userSessionBean.getUserID().intValue())){
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forTabViewMessage",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingLogicTemplate Creation ",intermediateProcessingLogicTemplateTo.getProcessingElementName() + " ProcessingLogicTemplate created successfully"));
			}else{
				FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Creation Failure", "ProcessingElementTemplate creation failed."));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}catch(Exception e){
			LOGGER.error("ProcessingLogicTemplate creation failed.", e);
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forProcessingLogicTemplateId",new FacesMessage(FacesMessage.SEVERITY_ERROR,"ProcessingElementTemplate Creation Failure", "ProcessingElementTemplate creation failed."));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		try{
			commonReportDataBean.setProcessingLogicTemplatesMap(CreateQuery.getProcessingLogicTemplatesMap());
			commonReportDataBean.setProcessingLogicTemplatesMapById(CreateQuery.getProcessingLogicTemplatesMapById());
		}catch (CreateQueryException e) {
			LOGGER.error("Error inside createProcessingLogicTemplate() while updating ProcessingLogicTemplateTypeList after ProcessingLogicTemplate creation." , e);
		}
	}

	private boolean checkEmptyStringIntermediateProcessingLogicTemplateTo(IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo){
		if(intermediateProcessingLogicTemplateTo!=null && intermediateProcessingLogicTemplateTo.getProcessingElementName().equals(CONSTANT_EMPTY)){
			return true;
		}else if(intermediateProcessingLogicTemplateTo!=null){
				return checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getFirstTypeProcessingLogicTo()) ||
						checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getSecondTypeProcessingLogicTo()) ||
							checkEmptyStringIntermediateProcessingLogicTemplateTo(intermediateProcessingLogicTemplateTo.getThirdTypeProcessingLogicTo());
		}
		return false;
	}

	private void onDragDropSourceTablesTree(TreeDragDropEvent event){
		TreeNode dragNode = event.getDragNode();
		TreeNode dropNode = event.getDropNode();

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		if(dragNode.getData() instanceof TableTo){
			if(((TableTo)dragNode.getData()).getTableType()==EnumTableType.NORMAL){
				new DefaultTreeNode((TableTo)dragNode.getData(),reportDataTo.getSourceTablesRoot().getChildren().get(0));
				reportDataTo.getNormalTables().getSource().add((TableTo)dragNode.getData());
				reportDataTo.getNormalTables().getTarget().remove((TableTo)dragNode.getData());
			}else if(((TableTo)dragNode.getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)dragNode.getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)dragNode.getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)dragNode.getData(),reportDataTo.getSourceTablesRoot().getChildren().get(1));
				reportDataTo.getDerivedTables().getSource().add((TableTo)dragNode.getData());
				reportDataTo.getDerivedTables().getTarget().remove((TableTo)dragNode.getData());
			}
			dropNode.getChildren().remove(dragNode);
		}
	}

	private void onDragDropSelectedTablesTree(TreeDragDropEvent event){
		TreeNode dragNode = event.getDragNode();
		TreeNode dropNode = event.getDropNode();

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		if(dragNode.getData() instanceof TableTo){
			if(((TableTo)dragNode.getData()).getTableType()==EnumTableType.NORMAL){
				new DefaultTreeNode((TableTo)dragNode.getData(),reportDataTo.getSelectedTablesRoot().getChildren().get(0));
				reportDataTo.getNormalTables().getSource().remove((TableTo)dragNode.getData());
				reportDataTo.getNormalTables().getTarget().add((TableTo)dragNode.getData());
			}else if(((TableTo)dragNode.getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)dragNode.getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)dragNode.getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)dragNode.getData(),reportDataTo.getSelectedTablesRoot().getChildren().get(1));
				reportDataTo.getDerivedTables().getSource().remove((TableTo)dragNode.getData());
				reportDataTo.getDerivedTables().getTarget().add((TableTo)dragNode.getData());
			}
			dropNode.getChildren().remove(dragNode);
		}
	}

	private void onTreeNodeSelection(NodeSelectEvent event){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		String description = "<div><ul><li><h4>";

		if(event.getTreeNode().getData() instanceof TableTo){
			TableTo selectedTableTo = (TableTo)event.getTreeNode().getData();
			description += selectedTableTo.getName();
			if(selectedTableTo.getTableType() == EnumTableType.GENERIC){
				description += "(SIMPLE)";
			}else if(selectedTableTo.getTableType() == EnumTableType.GROUPING){
				description += "(GROUPING)";
			}
			description += "</h4></li><li>";
			description += selectedTableTo.getDesc();
			description += "</li>";
			description += "<li><h4>Columns</h4></li>";

			for(ColumnTo column : commonReportDataBean.getAllTableColumnsMap().get(selectedTableTo)){
				description += "<li>";
				description += column.getColumn().getLabel();
				description += "</li>";
			}
			description += "</ul></div>";
		}

		reportDataTo.setSelectedTableDescription(description);
	}

	private void moveSourceSelectedTableToSelectedTables(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedNodeSourceTables()!=null && reportDataTo.getSelectedNodeSourceTables().getData() instanceof TableTo){
			if(((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.NORMAL){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSourceTables().getData(),reportDataTo.getSelectedTablesRoot().getChildren().get(0));
				reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().remove(reportDataTo.getSelectedNodeSourceTables());
				reportDataTo.getNormalTables().getSource().remove((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
				reportDataTo.getNormalTables().getTarget().add((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
			}else if(((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSourceTables().getData(),reportDataTo.getSelectedTablesRoot().getChildren().get(1));
				reportDataTo.getSourceTablesRoot().getChildren().get(1).getChildren().remove(reportDataTo.getSelectedNodeSourceTables());
				reportDataTo.getDerivedTables().getTarget().add((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
				reportDataTo.getDerivedTables().getSource().remove((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
			}
		}
		reportDataTo.setSelectedNodeSourceTables(null);
	}

	private void moveSourceSelectedTableToSelectedTablesOrgAdmin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedNodeSourceTables()!=null && reportDataTo.getSelectedNodeSourceTables().getData() instanceof TableTo){
			if(((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)reportDataTo.getSelectedNodeSourceTables().getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSourceTables().getData(),reportDataTo.getSelectedTablesRoot().getChildren().get(0));
				reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().remove(reportDataTo.getSelectedNodeSourceTables());
				reportDataTo.getDerivedTables().getTarget().add((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
				reportDataTo.getDerivedTables().getSource().remove((TableTo)reportDataTo.getSelectedNodeSourceTables().getData());
			}
		}
		reportDataTo.setSelectedNodeSourceTables(null);
	}

	private void moveAllSourceTablesToSelectedTables(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		for(TreeNode node : reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren()){
			reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getNormalTables().getTarget().add((TableTo)node.getData());
			reportDataTo.getNormalTables().getSource().remove((TableTo)node.getData());
		}
		reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().clear();

		for(TreeNode node : reportDataTo.getSourceTablesRoot().getChildren().get(1).getChildren()){
			reportDataTo.getSelectedTablesRoot().getChildren().get(1).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getDerivedTables().getTarget().add((TableTo)node.getData());
			reportDataTo.getDerivedTables().getSource().remove((TableTo)node.getData());
		}
		reportDataTo.getSourceTablesRoot().getChildren().get(1).getChildren().clear();
	}

	private void moveAllSourceTablesToSelectedTablesOrgAdmin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		for(TreeNode node : reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren()){
			reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getDerivedTables().getTarget().add((TableTo)node.getData());
			reportDataTo.getDerivedTables().getSource().remove((TableTo)node.getData());
		}
		reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().clear();
	}

	private void moveSelectedSelectedTablesToSourceTables(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedNodeSelectedTables()!=null && reportDataTo.getSelectedNodeSelectedTables().getData() instanceof TableTo){
			if(((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.NORMAL){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData(),reportDataTo.getSourceTablesRoot().getChildren().get(0));
				reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().remove(reportDataTo.getSelectedNodeSelectedTables());
				reportDataTo.getNormalTables().getTarget().remove((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
				reportDataTo.getNormalTables().getSource().add((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
			}else if(((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData(),reportDataTo.getSourceTablesRoot().getChildren().get(1));
				reportDataTo.getSelectedTablesRoot().getChildren().get(1).getChildren().remove(reportDataTo.getSelectedNodeSelectedTables());
				reportDataTo.getDerivedTables().getSource().add((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
				reportDataTo.getDerivedTables().getTarget().remove((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
			}
		}
		reportDataTo.setSelectedNodeSelectedTables(null);
	}

	private void moveSelectedSelectedTablesToSourceTablesOrgAdmin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedNodeSelectedTables()!=null && reportDataTo.getSelectedNodeSelectedTables().getData() instanceof TableTo){
			if(((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.GENERIC || ((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.GROUPING ||
					((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData()).getTableType()==EnumTableType.FUNCTION){
				new DefaultTreeNode((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData(),reportDataTo.getSourceTablesRoot().getChildren().get(0));
				reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().remove(reportDataTo.getSelectedNodeSelectedTables());
				reportDataTo.getDerivedTables().getSource().add((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
				reportDataTo.getDerivedTables().getTarget().remove((TableTo)reportDataTo.getSelectedNodeSelectedTables().getData());
			}
		}
		reportDataTo.setSelectedNodeSelectedTables(null);
	}

	private void moveAllSelectedTablesToSourceTables(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		for(TreeNode node : reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren()){
			reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getNormalTables().getSource().add((TableTo)node.getData());
			reportDataTo.getNormalTables().getTarget().remove((TableTo)node.getData());
		}
		reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().clear();

		for(TreeNode node : reportDataTo.getSelectedTablesRoot().getChildren().get(1).getChildren()){
			reportDataTo.getSourceTablesRoot().getChildren().get(1).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getDerivedTables().getSource().add((TableTo)node.getData());
			reportDataTo.getDerivedTables().getTarget().remove((TableTo)node.getData());
		}
		reportDataTo.getSelectedTablesRoot().getChildren().get(1).getChildren().clear();
	}

	private void moveAllSelectedTablesToSourceTablesOrgAdmin(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		for(TreeNode node : reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren()){
			reportDataTo.getSourceTablesRoot().getChildren().get(0).getChildren().add(new DefaultTreeNode(node.getData()));
			reportDataTo.getDerivedTables().getSource().add((TableTo)node.getData());
			reportDataTo.getDerivedTables().getTarget().remove((TableTo)node.getData());
		}
		reportDataTo.getSelectedTablesRoot().getChildren().get(0).getChildren().clear();
	}

	private void addColumnToSelectedColumnDataTable(){
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String droppedColumnId = params.get("droppedColumnId");
        String dropPos = params.get("dropPos");
        String property = params.get("property");
        String[] droppedColumnTokens;

        if(droppedColumnId!=null){
        	droppedColumnTokens = droppedColumnId.split(":");
        	int draggedColumnIndex = Integer.parseInt(droppedColumnTokens[droppedColumnTokens.length - 1]);
        	int dropColumnIndex = draggedColumnIndex + Integer.parseInt(dropPos);
        	createReportDataBean.setDroppedColumnIndex(dropColumnIndex);
        }

		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
        reportDataTo.setAutoSelectedColumnHeader(property);
	}

	private void resetAutoSelectedColumnDialog(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedReportColumn() == null){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Please Select Column", "Please select some Column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		reportDataTo.setAutoSelectedColumnHeader(null);
        reportDataTo.setAutoSelectedColumnVisible(true);
        reportDataTo.setAutoSelectedColumnDataType(null);
        reportDataTo.setAutoSelectedGroupingFunction(null);
	}

	private void setAutoSelectedColumnDialog(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedReportColumn() == null){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Please Select Column", "Please select any Column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		reportDataTo.setAutoSelectedColumnHeader(reportDataTo.getSelectedReportColumn().toString());
        reportDataTo.setAutoSelectedColumnVisible(true);
        reportDataTo.setAutoSelectedColumnDataType(((ColumnTo)reportDataTo.getSelectedReportColumn().getData()).getDataType());
        reportDataTo.setAutoSelectedGroupingFunction(EnumSQLFunctionAndKeyword.GROUPBY.getName());
	}

	private void closePreviewFilterSelection(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		reportDataTo.setPreviewReportFilters(null);
	}

	private void getPreviewReportFilters(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();

		if(reportDataTo.getSelectedOrderedColumnList().size()==0){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Columns", "Please select at least one column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}

		List<TableTo> selectedDerivedTables = new ArrayList<TableTo>();
		for(TableTo table : reportDataTo.getUsedSelectedTables()){
			if(table.getTableType() == EnumTableType.GENERIC || table.getTableType() == EnumTableType.GROUPING ||
					table.getTableType() == EnumTableType.FUNCTION){
				selectedDerivedTables.add(table);
			}
		}

		List<FilterGroupTo> filters = null;
		try {
			filters = Filter.getDerivedTablesFilters(selectedDerivedTables,reportDetailBean.getApplicationInputMap());
		} catch (FilterDisplayException e) {
			LOGGER.error("Error inside getPreviewReportFilters() while getting Filter.getDerivedTablesFilters(selectedDerivedTables)" , e);
		} catch (MakeQueryException e) {
			LOGGER.error("Error inside getPreviewReportFilters() while getting Filter.getDerivedTablesFilters(selectedDerivedTables)" , e);
		} catch (InputMissingException e) {
			LOGGER.error("Error inside getPreviewReportFilters() while getting Filter.getDerivedTablesFilters(selectedDerivedTables)" , e);
		}

		List<FilterGroupTo> previewFilters = null;
		if(reportDataTo.getFilterGroupToMapForTable().size()!=0){
			previewFilters = new ArrayList<FilterGroupTo>(reportDataTo.getFilterGroupToMapForTable().get(0).values());
		}
		if(filters != null && previewFilters != null){
			previewFilters.addAll(filters);
			reportDataTo.setPreviewReportFilters(previewFilters);
		}else if(filters != null && previewFilters == null){
			reportDataTo.setPreviewReportFilters(filters);
		}else{
			reportDataTo.setPreviewReportFilters(previewFilters);
		}
	}

	private void createAndExecutePreviewReportQuery(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		if(reportDataTo.getSelectedOrderedColumnList().size()==0){
			FacesContext.getCurrentInstance().addMessage("reportForm:tabView1:forColumnFilterSelectionTab",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Empty Columns", "Please select at least one column"));
			FacesContext.getCurrentInstance().validationFailed();
			return;
		}
		String nativeQuery = null;
		try {
			nativeQuery = MakeQuery.generateQuery(reportDataTo);
			if(nativeQuery!=null){
				String finalNativeQuery = MakeQuery.insertInputsInQuery(new StringBuilder(nativeQuery),reportDetailBean.getApplicationInputMap(),reportDataTo.getPreviewReportFilters());
				List<Object[]> reportData = ExecuteQuery.executeNativeQuery(finalNativeQuery);
				createReportDataBean.setReportData(reportData);
			}
		} catch (MakeQueryException e) {
			LOGGER.error("Error inside createAndExecutePreviewReportQuery() while getting query in MakeQuery.generateQuery(reportDataTo)" , e);
		} catch (InputMissingException e) {
			LOGGER.error("Error inside createAndExecutePreviewReportQuery() while getting query in MakeQuery.insertInputsInQuery(new StringBuilder(nativeQuery),reportDetailBean.getApplicationInputMap(),reportDataTo.getPreviewReportFilters())" , e);
		} catch (ExecuteQueryException e) {
			LOGGER.error("Error inside createAndExecutePreviewReportQuery() while getting reportData in ExecuteQuery.executeNativeQuery(finalNativeQuery)" , e);
		}
		List<String> reportHeaders = reportDataTo.getSelectedOrderedColumnList();
		createReportDataBean.setReportHeaders(reportHeaders);

		List<String> previewReportHeaders = new ArrayList<String>();
		for(String header: reportHeaders){
			previewReportHeaders.add(header);
		}
		previewReportHeaders.remove(CONSTANT_STAR);
		createReportDataBean.setPreviewReportHeaders(previewReportHeaders);
		createReportDataBean.setPreviewFlag(true);
		reportDataTo.setPreviewReportFilters(null);
	}

	private void preRenderView(CreateReportDataBean createReportDataBean){
		if (!FacesContext.getCurrentInstance().isPostback()) {
			ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
			ReportDataTo reportDataTo = currentPopulatedListNode.getData();
			reportDataTo.setName(reportDetailBean.getReportNameForCreation());
			reportDataTo.setDescription(reportDetailBean.getReportDescriptionForCreation());
			reportDataTo.setModule(reportDetailBean.getSelectedModule());
			reportDataTo.setDefaultFlag(Boolean.valueOf(reportDetailBean.getDefaultFlag()));
		}
	}

	private void addCss(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		createReportDataBean.setAddCssFlag(true);
		createReportDataBean.setCssOperators(commonReportDataBean.getOperatorDataTypeMap().get(new Integer(reportDataTo.getSelectedColumnDataType().getValue())));
	}

	private void updateSelectedColumnCssOperators(){
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		createReportDataBean.setCssOperators(commonReportDataBean.getOperatorDataTypeMap().get(new Integer(reportDataTo.getSelectedColumnDataType().getValue())));
	}

	private void filterColumnTreeNode(String searchStr) {
		ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode = createReportDataBean.getCurrentPopulatedListNode();
		ReportDataTo reportDataTo = currentPopulatedListNode.getData();
		List<TableTo> usedSelectedTables = reportDataTo.getUsedSelectedTables();
		TreeNode selectedTablesAndColumnsTreeRoot = new DefaultTreeNode("Root",null);
		List<EnumTableType> selectedTableTypes = new ArrayList<EnumTableType>();
		Map<TableTo,List<ColumnTo>> columnList = commonReportDataBean.getAllTableColumnsMap();
		if(usedSelectedTables != null){
			for(TableTo table: usedSelectedTables){
				if(!selectedTableTypes.contains(table.getTableType())){
					selectedTableTypes.add(table.getTableType());
				}
				if(columnList.get(table)!=null){
					reportDataTo.getUsedSelectedTablesColumns().addAll(columnList.get(table));
				}

				TreeNode tableNode = new DefaultTreeNode(table, selectedTablesAndColumnsTreeRoot);
				tableNode.setSelectable(true);
				tableNode.setExpanded(true);
				for(ColumnTo column : columnList.get(table)){
					if(searchStr!=null && !searchStr.isEmpty())
					{
						if(column.getColumn().getLabel().toLowerCase().contains(searchStr.toLowerCase()))
							new DefaultTreeNode(column, tableNode);
					}
					else {
						new DefaultTreeNode(column, tableNode);
					}
				}
			}
		}
		reportDataTo.setSelectedTablesAndColumnsTreeRoot(selectedTablesAndColumnsTreeRoot);
	}

}
