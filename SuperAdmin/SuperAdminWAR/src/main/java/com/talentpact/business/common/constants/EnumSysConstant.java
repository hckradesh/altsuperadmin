package com.talentpact.business.common.constants;

public enum EnumSysConstant {

    Login_Page(IConstants.Login_Page, 1), Home_Page(IConstants.Home_Page, 2), Login_Page_Theme(IConstants.Login_Page_Theme, 3), Default_Theme(IConstants.Default_Theme, 4), Left_Logo(
            IConstants.Left_Logo, 5), Right_Logo(IConstants.Right_Logo, 6), Footer(IConstants.Footer, 7), UsernameTxt(IConstants.UsernameTxt, 8), PasswordTxt(
            IConstants.PasswordTxt, 9), BtnTxt(IConstants.BtnTxt, 10), Banner(IConstants.Banner, 11), SamlHeader(IConstants.SamlHeader, 12), SamlBtnTxt(IConstants.SamlBtnTxt, 13), HeadingTxt(
            IConstants.HeadingTxt, 14);


    private String categoryName;

    private Integer categoryId;


    EnumSysConstant(String categoryName, Integer categoryId)
    {
        this.categoryName = categoryName;
        this.categoryId = categoryId;

    }


    public static Integer getCatgId(String CategoryName)
    {
        for (EnumSysConstant enumCon : EnumSysConstant.values()) {

            if (enumCon.getCategoryName().equalsIgnoreCase(CategoryName)) {
                return enumCon.getCategoryId();
            }
        }

        return null;
    }

    public String getCategoryName()
    {
        return categoryName;
    }


    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }


    public Integer getCategoryId()
    {
        return categoryId;
    }


    public void setCategoryId(Integer categoryId)
    {
        this.categoryId = categoryId;
    }

}
