package com.talentpact.business.dataservice.sysPinCode;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.organization.SysPinCode;
import com.talentpact.ui.sysPinCode.to.SysPinCodeTO;


/**
 * 
 * @author prachi.bansal
 *
 */
@Stateless
public class SysPinCodeService extends CommonService{
    
    @EJB
    SysPinCodeDS sysPinCodeDS;
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPinCodeService.class);

    public List<SysPinCodeTO> getSysPinCodeList()throws Exception {
        List<SysPinCode> sysPinCodeList=null;
        List<SysPinCodeTO> sysPinCodeTOList=null;
        sysPinCodeTOList=new ArrayList<SysPinCodeTO>();
        try{
            List<Object[]> list=sysPinCodeDS.getSysPinCodeList();
            if(list!=null && !list.isEmpty()){
                for(Object[] object : list){
                    SysPinCodeTO responseTO = new SysPinCodeTO();
                    if (object[0] != null) {
                        BigInteger a = (BigInteger)object[0];
                        responseTO.setPinCodeID(a.longValue());
                    }
                    responseTO.setPinCode((String) object[1]);
                    BigInteger cId=(BigInteger)object[2];
                    responseTO.setCityID(cId.longValue());
                    responseTO.setPostOfficeName((String) object[3]);
                    responseTO.setCityName((String) object[4]);
                    sysPinCodeTOList.add(responseTO);
                }
            }
          
            return sysPinCodeTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        finally{   
        }
    }

    public List<SysPinCodeTO> getSysCityList() throws Exception {
       
        List<SysPinCodeTO> sysPinCodeTOList=null;
        sysPinCodeTOList=new ArrayList<SysPinCodeTO>();
        try{
           List<Object[]> cityList=sysPinCodeDS.getSysCityList();
            if(cityList!=null && !cityList.isEmpty())
            {
                for(Object[] sysPinCode : cityList)
                {
                    SysPinCodeTO sysPinCodeTO=new SysPinCodeTO();
                    if(sysPinCode[0]!=null){
                        //BigInteger cID= (BigInteger)sysPinCode[0];
                        sysPinCodeTO.setCityID((Long) sysPinCode[0]);
                     }
                    sysPinCodeTO.setCityName((String)sysPinCode[1]);
                    sysPinCodeTOList.add(sysPinCodeTO);
                }
            }
            return sysPinCodeTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        finally{   
        }
        
    }

    public void saveSysPinCode(SysPinCodeTO sysPinCodeTO) {
        try{
            sysPinCodeDS.saveSysPincode(sysPinCodeTO);
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public List<SysPinCodeTO> getPostOfficeList(String pinCode) throws Exception {
        List<SysPinCode> sysPinCodeList=null;
        List<SysPinCodeTO> sysPinCodeTOList=null;
        sysPinCodeTOList=new ArrayList<SysPinCodeTO>();
        try{
            sysPinCodeList=sysPinCodeDS.getPostOfficeList(pinCode);
            if(sysPinCodeList!=null && !sysPinCodeList.isEmpty())
            {
                for(SysPinCode sysPinCode : sysPinCodeList)
                {
                   SysPinCodeTO sysPinCodeTO=new SysPinCodeTO();
                   sysPinCodeTO.setPinCodeID(sysPinCode.getPinCodeID());
                   sysPinCodeTO.setCityID(sysPinCode.getCityID());
                   sysPinCodeTO.setPinCode(sysPinCode.getPinCode());
                   sysPinCodeTO.setPostOfficeName(sysPinCode.getPostOfficeName());
                   sysPinCodeTOList.add(sysPinCodeTO);
                }
            }
            return sysPinCodeTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
    }

    public void updateSysPinCode(SysPinCodeTO sysPinCodeTO) throws Exception {
        try{
            sysPinCodeDS.updateSysPinCode(sysPinCodeTO);
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        
    }

    public List<SysPinCodeTO> getUpdatePinCodeList(Long pinCodeID) throws Exception {
        List<SysPinCode> sysPinCodeList=null;
        List<SysPinCodeTO> sysPinCodeTOList=null;
        sysPinCodeTOList=new ArrayList<SysPinCodeTO>();
        try{
            List<Object[]> list=sysPinCodeDS.getUpdatePinCodeList(pinCodeID);
            if(list!=null & !list.isEmpty()){
                for(Object[] object : list){
                    SysPinCodeTO responseTO = new SysPinCodeTO();
                    if (object[0] != null) {
                        BigInteger a = (BigInteger)object[0];
                        responseTO.setPinCodeID(a.longValue());
                    }
                    responseTO.setPinCode((String) object[1]);
                    BigInteger cId=(BigInteger)object[2];
                    responseTO.setCityID(cId.longValue());
                    responseTO.setPostOfficeName((String) object[3]);
                    responseTO.setCityName((String) object[4]);
                    sysPinCodeTOList.add(responseTO);
                }
            }
          
            return sysPinCodeTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        
    }

    
    
}