/**
 * 
 */
package com.talentpact.business.dataservice.SysOfferingCategory;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysOfferingCategoryService extends CommonService
{

    @EJB
    SysOfferingCategoryDS sysOfferingCategoryDS;

    public List<SysOfferingCategoryTO> getSysOfferingCategoryList()
        throws Exception
    {
        List<SysOfferingCategoryTO> list = null;
        SysOfferingCategoryTO sysOfferingCategoryTO = null;
        List<Object[]> sysOfferingCategoryList = null;

        try {
            sysOfferingCategoryList = sysOfferingCategoryDS.getSysOfferingCategoryList();
            list = new ArrayList<SysOfferingCategoryTO>();
            for (Object[] sysOfferingCategory : sysOfferingCategoryList) {
                sysOfferingCategoryTO = new SysOfferingCategoryTO();
                sysOfferingCategoryTO.setOfferingCategoryID((Long) sysOfferingCategory[0]);
                if (sysOfferingCategory[1] != null)
                    sysOfferingCategoryTO.setOfferingCategory((String) sysOfferingCategory[1]);
                if (sysOfferingCategory[2] != null)
                    sysOfferingCategoryTO.setDescription((String) sysOfferingCategory[2]);
                list.add(sysOfferingCategoryTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO)
        throws Exception
    {
        try {
            sysOfferingCategoryDS.addSysOfferingCategory(sysOfferingCategoryTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO)
        throws Exception
    {
        try {
            sysOfferingCategoryDS.editSysOfferingCategory(sysOfferingCategoryTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}