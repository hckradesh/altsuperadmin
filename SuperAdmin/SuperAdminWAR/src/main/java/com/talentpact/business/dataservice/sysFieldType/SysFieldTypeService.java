package com.talentpact.business.dataservice.sysFieldType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.model.SysFieldType;
import com.talentpact.ui.sysFieldType.to.SysFieldTypeTO;

@Stateless
public class SysFieldTypeService extends CommonService{

	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysFieldTypeService.class);
	
	@EJB
	SysFieldTypeDS sysFieldTypeDS;
	
	public List<SysFieldTypeTO> getSysFieldTypeList() throws Exception{
        List<SysFieldType> sysFieldTypeList=null;
        List<SysFieldTypeTO> sysFieldTypeTOList=null;
        sysFieldTypeTOList=new ArrayList<SysFieldTypeTO>();
        try{
        	sysFieldTypeList=sysFieldTypeDS.getSysFieldTypeList();
            if(sysFieldTypeList!=null && !sysFieldTypeList.isEmpty()){
                for(SysFieldType sFT : sysFieldTypeList){
                	SysFieldTypeTO sysFieldTypeTO=new SysFieldTypeTO();
                	sysFieldTypeTO.setSysFieldTypeID(sFT.getSysFieldTypeID());
                	sysFieldTypeTO.setFieldType(sFT.getFieldType());
                	sysFieldTypeTOList.add(sysFieldTypeTO);
                }
            }
            return sysFieldTypeTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }
	
    public List<SysFieldTypeTO> getUpdateSysFieldType(Integer sysFieldTypeID) throws Exception{
        List<SysFieldType> sysFieldTypeList=null;
        List<SysFieldTypeTO> sysFieldTypeTOList=null;
        sysFieldTypeTOList = new ArrayList<SysFieldTypeTO>();
        try{
        	sysFieldTypeList=sysFieldTypeDS.getUpdateSysFieldType(sysFieldTypeID);
            if(sysFieldTypeList!=null && !sysFieldTypeList.isEmpty()){
                for(SysFieldType sFT : sysFieldTypeList){
                	SysFieldTypeTO sysFieldTypeTO=new SysFieldTypeTO();
                	sysFieldTypeTO.setSysFieldTypeID(sFT.getSysFieldTypeID());
                	sysFieldTypeTO.setFieldType(sFT.getFieldType());
                	sysFieldTypeTOList.add(sysFieldTypeTO);
                }
            }
            return sysFieldTypeTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }
	
	 public void saveSysFieldType(SysFieldTypeTO sysFieldTypeTO) {
	        try{
	        	sysFieldTypeDS.saveSysFieldType(sysFieldTypeTO);
	        } catch (Exception e) {
	            LOGGER_.error("", e);
	        }
	        
	    }
	    
	    public void updateSysFieldType(SysFieldTypeTO sysFieldTypeTO){
	        try{
	        	sysFieldTypeDS.updateSysFieldType(sysFieldTypeTO);
	        } catch (Exception e) {
	            LOGGER_.error("", e);
	        }
	    }
}
