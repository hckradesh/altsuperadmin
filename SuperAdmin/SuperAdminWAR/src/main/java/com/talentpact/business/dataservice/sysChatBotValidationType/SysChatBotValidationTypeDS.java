package com.talentpact.business.dataservice.sysChatBotValidationType;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysChatBotValidationType;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysChatBotValidationTypeDS extends
AbstractDS<SysChatBotValidationType> {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationTypeDS.class);

	@Inject
	UserSessionBean userSessionBean;

	public SysChatBotValidationTypeDS() {
		// TODO Auto-generated constructor stub
		super(SysChatBotValidationType.class);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getSysChatBotValidationTypeListing() {
		Query query = null;
		StringBuilder hqlQuery = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select scbvt.sysValidationTypeID, scbvt.sysFieldTypeID, sft.fieldType, scbvt.noOfParameters, scbvt.label  from SysChatBotValidationType scbvt inner join scbvt.sysFieldType sft");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			return query.getResultList();
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			return null;
		}

	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveSysChatBotValidationType(
			SysChatBotValidationTypeTO sysChatBotValidationTypeTO) {
			SysChatBotValidationType sysChatBotValidationType = null;
		try {
			sysChatBotValidationType = new SysChatBotValidationType();
			
			sysChatBotValidationType.setNoOfParameters(sysChatBotValidationTypeTO.getNoOfParameters());
			sysChatBotValidationType.setLabel(sysChatBotValidationTypeTO.getLabel());
			sysChatBotValidationType.setSysFieldTypeID(sysChatBotValidationTypeTO.getSysFieldTypeID());
			sysChatBotValidationType.setCreatedBy(userSessionBean.getUserID().intValue());
			sysChatBotValidationType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
			sysChatBotValidationType.setModifiedBy(userSessionBean.getUserID().intValue());
			sysChatBotValidationType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			sysChatBotValidationType.setTenantID(1);
			getEntityManager("TalentPact").persist(sysChatBotValidationType);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
	}

	public List<SysChatBotValidationType> getUpdateSysChatBotValidationType(Integer sysValidationTypeID) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> list = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" from SysChatBotValidationType sft where sft.sysValidationTypeID!=:sysValidationTypeID");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			query.setParameter("sysValidationTypeID", sysValidationTypeID);
			return query.getResultList();
		} catch (Exception e) {
			LOGGER_.error("", e);
			return null;
		}
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void updateSysChatBotValidationType(SysChatBotValidationTypeTO sysChatBotValidationTypeTO) {
		try {
			SysChatBotValidationType sysChatBotValidationType = findSysChatBotValidationType(sysChatBotValidationTypeTO
					.getSysValidationTypeID());

			sysChatBotValidationType.setNoOfParameters(sysChatBotValidationTypeTO.getNoOfParameters());
			sysChatBotValidationType.setLabel(sysChatBotValidationTypeTO.getLabel());
			sysChatBotValidationType.setSysFieldTypeID(sysChatBotValidationTypeTO.getSysFieldTypeID());
			sysChatBotValidationType.setModifiedBy(userSessionBean.getUserID().intValue());
			sysChatBotValidationType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			getEntityManager("TalentPact").merge(sysChatBotValidationType);
			getEntityManager("TalentPact").flush();
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
	}

	@SuppressWarnings("unchecked")
	public SysChatBotValidationType findSysChatBotValidationType(Integer sysValidationTypeID) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysChatBotValidationType> list = null;
		SysChatBotValidationType sysChatBotValidationType = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" from SysChatBotValidationType sysChatBotValidationType where SysChatBotValidationType.sysValidationTypeID=:sysValidationTypeID");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			query.setParameter("sysValidationTypeID", sysValidationTypeID);
			list = query.getResultList();
			if (list != null && !list.isEmpty()) {
				sysChatBotValidationType = list.get(0);
			}
			return sysChatBotValidationType;
		} catch (Exception e) {
			LOGGER_.error("", e);
			return null;
		}

	}
	
	public List<SysChatBotValidationType> ChatBotValidationTypeDropdown(Integer sysFieldTypeID)
            throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> list = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" from SysChatBotValidationType sft where sft.sysFieldTypeID =:sysFieldTypeID");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			query.setParameter("sysFieldTypeID", sysFieldTypeID);
			return query.getResultList();
		} catch (Exception e) {
			LOGGER_.error("", e);
			return null;
		}
        } 

} // end of class
