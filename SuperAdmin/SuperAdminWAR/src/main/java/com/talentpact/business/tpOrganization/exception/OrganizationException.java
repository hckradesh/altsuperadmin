/**
 * 
 */
package com.talentpact.business.tpOrganization.exception;

/**
 * @author radhamadhab.dalai
 *
 */
public class OrganizationException extends Exception
{

    private String message;

    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    public  OrganizationException()
    {

    }

    public  OrganizationException(String message)
    {
        this.message = message;
    }


}
