/**
 * 
 */
package com.talentpact.business.authorization.converter;

import java.security.MessageDigest;

import sun.misc.BASE64Encoder;


/**
 * @author radhamadhab.dalai
 *
 */
public class PassWordEncryptionHelper
{

    public static String convetToHash(String password)
    {
        String hashPassword = null;
        String scheme = "SHA-1";
        String byteCode = "UTF-8";
        try {

            MessageDigest md = MessageDigest.getInstance(scheme);
            md.update(password.getBytes("UTF-8"));
            byte[] hash = md.digest();
            hashPassword = new BASE64Encoder().encode(hash);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return hashPassword;
    }
}
