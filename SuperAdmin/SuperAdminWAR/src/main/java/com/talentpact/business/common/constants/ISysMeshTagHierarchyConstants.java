package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysMeshTagHierarchyConstants
{

    public static String SYSMESHTAGHIERARCHY_ADD_DIALOG_HIDE = "PF('sysMeshTagHierarchyAddModal').hide();";

    public static String SYSMESHTAGHIERARCHY_EDIT_DIALOG_HIDE = "PF('sysMeshTagHierarchyEditModal').hide();";

    public static String SYSMESHTAGHIERARCHY_SAVE_ERROR_MSG = "Error occured while saving SysMeshTagHierarchy.";

    public static String SYSMESHTAGHIERARCHY_UPDATE_ERROR_MSG = "Error occured while updating SysMeshTagHierarchy.";

    public static String SYSMESHTAGHIERARCHY_SAVE_SUCCESS_MSG = "SysMeshTagHierarchy saved successfully.";

    public static String SYSMESHTAGHIERARCHY_UPDATE_SUCCESS_MSG = "SysMeshTagHierarchy updated successfully.";

    public static String SYSMESHTAGHIERARCHY_DATATABLE_RESET = "PF('sysMeshTagHierarchyDataTable').clearFilters()";

    public static String SYSMESHTAGHIERARCHY_PAGINATION_DATATABLE_RESET = "PF('sysMeshTagHierarchyDataTable').getPaginator().setPage(0);";

    public static String SYSMESHTAGHIERARCHY_DUPLICATE_ERROR_MSG = "SysMeshTagHierarchy already present in system.";
}
