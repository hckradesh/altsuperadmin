package com.talentpact.business.adhocreportsui.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.controller.CreateQuery;
import com.talentpact.adhocreports.business.controller.DeleteQuery;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;

@SessionScoped
@Named("deleteReportDataController")
public class DeleteReportDataController implements Serializable {
	
	private static final long serialVersionUID = 6718019297164890400L;
	private static final Logger LOGGER = Logger.getLogger(DeleteReportDataController.class);
	
	@Inject
	ReportDetailBean reportDetailBean;
	
	public void initialize() {

	}

	private void deleteReport(){
		ReportDetail report = reportDetailBean.getSelectedReportForDeletion();
		
		try{
			boolean result = DeleteQuery.deleteReport(report.getReportId());
			if(result == false){
				FacesContext.getCurrentInstance().addMessage("displayReportsForm:reportDeletionError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report Deletion Failed", "Failed to delete report"));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
			reportDetailBean.setReportDetailList(CreateQuery.getAllReportDetails());
			FacesContext.getCurrentInstance().addMessage("displayReportsForm:reportDeletionError",new FacesMessage(FacesMessage.SEVERITY_INFO,"Report Deletion Successful", "Report Successfully Deleted"));
		}catch(Exception e){
			LOGGER.error("Error inside deleteReport(ReportDetail report) method for report Id : "+report.getReportId(), e);
			FacesContext.getCurrentInstance().addMessage("displayReportsForm:reportDeletionError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report Deletion Failed", "Failed to delete report"));
			FacesContext.getCurrentInstance().validationFailed();
		} 
	}
	
	private void setReportForDeletion(ReportDetail report){
		reportDetailBean.setSelectedReportForDeletion(report);
	}
	
}
