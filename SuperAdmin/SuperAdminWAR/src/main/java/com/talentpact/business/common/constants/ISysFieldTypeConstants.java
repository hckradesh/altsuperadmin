package com.talentpact.business.common.constants;

public class ISysFieldTypeConstants {

    public static String SYSFIELDTYPE_ADD_DIALOG_HIDE = "PF('sysFieldTypeAddModal').hide();";

    public static String SYSFIELDTYPE_EDIT_DIALOG_HIDE = "PF('sysFieldTypeEditModal').hide();";

    public static String SYSFIELDTYPE_SAVE_ERROR_MSG = "Error occured while saving SysFieldType.";

    public static String SYSFIELDTYPE_UPDATE_ERROR_MSG = "Error occured while updating SysFieldType.";

    public static String SYSFIELDTYPE_SAVE_SUCCESS_MSG = "SysFieldType saved successfully.";

    public static String SYSFIELDTYPE_UPDATE_SUCCESS_MSG = "SysFieldType updated successfully.";

    public static String SYSFIELDTYPE_DATATABLE_RESET = "PF('sysFieldTypeDataTable').clearFilters();";

    public static String SYSFIELDTYPE_PAGINATION_DATATABLE_RESET = "PF('sysFieldTypeDataTable').getPaginator().setPage(0);";

    public static String SYSFIELDTYPE_ERROR_MSG = "SysFieldType already present in system.";
}
