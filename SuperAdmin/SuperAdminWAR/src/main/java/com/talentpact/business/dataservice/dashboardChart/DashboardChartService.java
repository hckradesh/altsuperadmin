/**
 * 
 */
package com.talentpact.business.dataservice.dashboardChart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.talentpact.business.application.transport.input.DashboardChartRequestTO;
import com.talentpact.business.application.transport.output.DashboardChartResponseTO;
import com.talentpact.insights.dashboardchart.to.DashboardChartTO;
import com.talentpact.model.altone.HrDashBoard;

/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class DashboardChartService
{

    @EJB
    SysDashboardChartDS sysDashboardChartDS;

    @EJB
    HrDashboardChartDS hrDashboardChartDS;

    /**
     * 
     * @return
     * @throws Exception
     */
    public List<DashboardChartTO> getSysDashboardList()
        throws Exception
    {
        List<DashboardChartTO> sysDashboardTOList = null;
        DashboardChartTO dashboardChartTO = null;
        List<Object[]> sysDashboardList = null;
        try {
            sysDashboardList = sysDashboardChartDS.getSysDashboardList();
            sysDashboardTOList = new ArrayList<DashboardChartTO>();
            for (Object[] dashboard : sysDashboardList) {
                dashboardChartTO = new DashboardChartTO();
                dashboardChartTO.setSysDashboardID(((Integer) dashboard[0]));
                dashboardChartTO.setDashboardName(dashboard[1].toString());
                dashboardChartTO.setStyleClass(dashboard[2].toString());
                sysDashboardTOList.add(dashboardChartTO);
            }

            return sysDashboardTOList;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * @param dashboardChartTO
     * @throws Exception
     */

    public void saveSysDashboard(DashboardChartTO dashboardChartTO)
        throws Exception
    {
        sysDashboardChartDS.saveSysDashboard(dashboardChartTO);
    }

    /**
     * 
     * @param dashboardChartRequestTO
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void copyAllDashboardChartList(DashboardChartRequestTO dashboardChartRequestTO)
        throws Exception
    {
        List<DashboardChartTO> sysDashboardTOList = null;
        HrDashBoard hrDashBoard = null;
        try {
            sysDashboardTOList = dashboardChartRequestTO.getDashBoardChartTOlist();

            if (sysDashboardTOList != null && !sysDashboardTOList.isEmpty()) {
                for (DashboardChartTO sysdashbaordTO : sysDashboardTOList) {
                    hrDashBoard = hrDashboardChartDS.findHrDashboard(sysdashbaordTO.getSysDashboardID(), dashboardChartRequestTO.getOrganizationID(),dashboardChartRequestTO.getSelectedPortalTypeID());
                    if (hrDashBoard == null) {
                        hrDashBoard = new HrDashBoard();
                        hrDashBoard.setActive(sysdashbaordTO.isSelected());
                        if (sysdashbaordTO.getHrDashboardName() == null||sysdashbaordTO.getHrDashboardName().trim()=="")
                            hrDashBoard.setHrDashboardName(sysdashbaordTO.getDashboardName());
                        else
                            hrDashBoard.setHrDashboardName(sysdashbaordTO.getHrDashboardName());
                        hrDashBoard.setOrganizationID(dashboardChartRequestTO.getOrganizationID().intValue());
                        hrDashBoard.setSequence(sysdashbaordTO.getSequence());
                        hrDashBoard.setSysDashboardID(sysdashbaordTO.getSysDashboardID());
                        hrDashBoard.setCreatedBy(new Long(dashboardChartRequestTO.getCreatedBy()).toString());
                        hrDashBoard.setPortalTypeID(dashboardChartRequestTO.getSelectedPortalTypeID());
                        hrDashBoard.setCreatedDate(new Date());
                        hrDashBoard.setModifiedBy(new Long(dashboardChartRequestTO.getCreatedBy()).toString());
                        hrDashBoard.setModifiedDate(new Date());
                        hrDashboardChartDS.create("TalentPactInsightsAgg", hrDashBoard);
                    } else {
                        if (sysdashbaordTO.getHrDashboardName() == null||sysdashbaordTO.getHrDashboardName().trim()=="")
                            hrDashBoard.setHrDashboardName(sysdashbaordTO.getDashboardName());
                        else
                            hrDashBoard.setHrDashboardName(sysdashbaordTO.getHrDashboardName());
                        hrDashBoard.setSequence(sysdashbaordTO.getSequence());
                        hrDashBoard.setActive(sysdashbaordTO.isSelected());
                        hrDashBoard.setModifiedBy(new Long(dashboardChartRequestTO.getCreatedBy()).toString());
                        hrDashBoard.setModifiedDate(new Date());
                        hrDashboardChartDS.edit("TalentPactInsightsAgg", hrDashBoard);
                    }

                }
            }
           // hrDashboardChartDS.popluateDate(dashboardChartRequestTO.getOrganizationID());
        } catch (Exception e) {
            throw e;
        }

    }

    public DashboardChartResponseTO getSysDashboardList(Long orgId,int portalTypeID)
        throws Exception
    {
        List<DashboardChartTO> sysDashboardTOList = null;
        DashboardChartTO dashboardChartTO = null;
        List<Object[]> sysDashboardList = null;
        List<HrDashBoard> hrDashboardList = null;
        try {
            sysDashboardList = sysDashboardChartDS.getSysDashboardList();
            hrDashboardList = hrDashboardChartDS.getHrDashboardList(orgId.intValue(),portalTypeID);
            sysDashboardTOList = new ArrayList<DashboardChartTO>();
            DashboardChartResponseTO dashboardChartResponseTO = new DashboardChartResponseTO();
            for (Object[] dashboard : sysDashboardList) {
                dashboardChartTO = new DashboardChartTO();
                dashboardChartTO.setSysDashboardID(((Integer) dashboard[0]));
                dashboardChartTO.setDashboardName(dashboard[1].toString());
                dashboardChartTO.setStyleClass(dashboard[2].toString());
                if (hrDashboardList != null && !hrDashboardList.isEmpty()) {
                    for (HrDashBoard hrDashBoard : hrDashboardList) {
                        if (hrDashBoard.getSysDashBoard().getSysDashboardID().equals((Integer) dashboard[0])) {
                            if (hrDashBoard.getActive())
                                dashboardChartTO.setSelected(hrDashBoard.getActive());
                            dashboardChartTO.setSequence(hrDashBoard.getSequence());
                            dashboardChartTO.setHrDashboardName(hrDashBoard.getHrDashboardName());
                            break;
                        }
                    }
                }
                sysDashboardTOList.add(dashboardChartTO);
            }

            
            dashboardChartResponseTO.setSysDashboardChartTOList(sysDashboardTOList);
            return dashboardChartResponseTO;
        } catch (Exception e) {
            throw e;
        }
    }
}
