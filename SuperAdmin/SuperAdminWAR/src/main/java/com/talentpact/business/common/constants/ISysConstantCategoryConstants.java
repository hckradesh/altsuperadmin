/**
 * 
 */
package com.talentpact.business.common.constants;

/**
 * @author pankaj.sharma1
 *
 */
public interface ISysConstantCategoryConstants
{
    public static String SYSCONSTANTCATEGORY_ADD_DIALOG_HIDE = "PF('sysConstantCategoryAddModal').hide();";

    public static String SYSCONSTANTCATEGORY_EDIT_DIALOG_HIDE = "PF('sysConstantCategoryEditModal').hide();";

    public static String SYSCONSTANTCATEGORY_SAVE_ERROR_MSG = "Error occured while saving SysConstantCategory.";

    public static String SYSCONSTANTCATEGORY_UPDATE_ERROR_MSG = "Error occured while updating SysConstantCategory.";

    public static String SYSCONSTANTCATEGORY_SAVE_SUCCESS_MSG = "SysConstantCategory saved successfully.";

    public static String SYSCONSTANTCATEGORY_UPDATE_SUCCESS_MSG = "SysConstantCategory updated successfully.";

    public static String SYSCONSTANTCATEGORY_DATATABLE_RESET = "myDataTable.clearFilters()";

    public static String SYSCONSTANTCATEGORY_DUPLICATE_ERROR_MSG = "SysConstantCategory already present in system.";

}
