package com.talentpact.business.common.constants;

/**
 * 
 * @author prachi.bansal
 *
 */

public interface ISysPtZoneConstants{
    
    public static String SYSPTZONE_ADD_DIALOG_HIDE = "PF('sysPtZoneAddModal').hide();";

    public static String SYSPTZONE_EDIT_DIALOG_HIDE = "PF('sysPtZoneEditModal').hide();";

    public static String SYSPTZONE_SAVE_ERROR_MSG = "Error occured while saving SysPtZone.";

    public static String SYSPTZONE_UPDATE_ERROR_MSG = "Error occured while updating SysPtZone.";

    public static String SYSPTZONE_SAVE_SUCCESS_MSG = "SysPtZone saved successfully.";

    public static String SYSPTZONE_UPDATE_SUCCESS_MSG = "SysPtZone updated successfully.";

    public static String SYSPTZONE_DATATABLE_RESET = "PF('sysPtZoneDataTable').clearFilters();";

    public static String SYSPTZONE_PAGINATION_DATATABLE_RESET = "PF('sysPtZoneDataTable').getPaginator().setPage(0);";

    public static String SYSPTZONE_DUPLICATE_ERROR_MSG = "SysPtZone already present in system.";

}