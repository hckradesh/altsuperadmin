package com.talentpact.business.dataservice.SysWorkflowType;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.talentpact.business.application.transport.output.SysWorkflowStageTypeGroupTO;
import com.talentpact.business.application.transport.output.SysWorkflowStageTypeTO;
import com.talentpact.business.application.transport.output.SysWorkflowTypeTO;
import com.talentpact.business.dataservice.SysTenantDS;
import com.talentpact.business.dataservice.UiForm.UiFormDS;
import com.talentpact.business.model.SysWorkflowStageTypeGroup;
import com.talentpact.business.model.SysWorkflowStageType_Revamp;
import com.talentpact.business.model.SysWorkflowType_Revamp;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysSubModule.dataprovider.DS.SysSubModuleDS;

@Stateless
public class SysWorkflowTypeService {

    @Inject
    SysWorkflowTypeDS sysWorkflowTypeDS;

    @Inject
    SysSubModuleDS sysSubModuleDS;

    @Inject
    SysWorkflowStageTypeDS sysWorkflowStageTypeDS;

    @Inject
    SysWorkflowStageTypeGroupDS sysWorkflowStageTypeGroupDS;

    @Inject
    SysTenantDS sysTenantDS;

    @Inject
    UiFormDS uiFormDS;

    @Inject
    SysSubModuleDS subModuleDS;

    @Inject
    UserSessionBean userSessionBean;

    public List<SysWorkflowTypeTO> getSysWorkflowTypeTOList() {
        SysWorkflowTypeTO sysWorkflowTypeTO = null;
        SysWorkflowStageTypeTO sysWorkflowStageTypeTO = null;
        SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO = null;
        List<SysWorkflowTypeTO> sysWorkflowTypeTOs = new ArrayList<SysWorkflowTypeTO>();
        Map<Integer, SysWorkflowTypeTO> sysWorkflowTypeMap = new HashMap<Integer, SysWorkflowTypeTO>();
        for (Object[] row : sysWorkflowTypeDS.getSysWorkflowTypeList()) {
            sysWorkflowTypeTO = new SysWorkflowTypeTO();
            sysWorkflowTypeTO.setWorkflowTypeID((Integer) row[0]);
            sysWorkflowTypeTO.setWorkflowType((String) row[1]);
            sysWorkflowTypeTO.setModuleId((Integer) row[2]);
            sysWorkflowTypeTO.setModuleName((String) row[3]);
            sysWorkflowTypeTO.setSysWorkflowStageTypeTOs(new ArrayList<SysWorkflowStageTypeTO>());
            sysWorkflowTypeTO.setSysWorkflowStageTypeGroupTOs(new ArrayList<SysWorkflowStageTypeGroupTO>());
            sysWorkflowTypeTOs.add(sysWorkflowTypeTO);
            sysWorkflowTypeMap.put(sysWorkflowTypeTO.getWorkflowTypeID(), sysWorkflowTypeTO);
        }
        for (Object[] row : sysWorkflowTypeDS.getSysWorkflowStageTypeGroupList()) {
            sysWorkflowStageTypeGroupTO = new SysWorkflowStageTypeGroupTO();
            sysWorkflowStageTypeGroupTO.setWorkflowStageTypeGroupID((Integer) row[0]);
            sysWorkflowStageTypeGroupTO.setWorkflowStageTypeGroupName((String) row[1]);
            sysWorkflowStageTypeGroupTO.setWorkflowTypeID((Integer) row[2]);
            sysWorkflowTypeMap.get(sysWorkflowStageTypeGroupTO.getWorkflowTypeID()).getSysWorkflowStageTypeGroupTOs().add(sysWorkflowStageTypeGroupTO);
        }
        for (Object[] row : sysWorkflowTypeDS.getSysWorkflowStageTypeList()) {
            sysWorkflowStageTypeTO = new SysWorkflowStageTypeTO();
            sysWorkflowStageTypeTO.setWorkflowStageTypeID((Integer) row[0]);
            sysWorkflowStageTypeTO.setWorkflowStageType((String) row[1]);
            sysWorkflowStageTypeTO.setUiFormID((Integer) row[2]);
            sysWorkflowStageTypeTO.setFormName((String) row[3]);
            sysWorkflowStageTypeTO.setWorkflowTypeID((Integer) row[4]);
            if (row[5] != null) {
                sysWorkflowStageTypeTO.setWorkflowStageTypeGroupID((Integer) row[5]);
                sysWorkflowStageTypeTO.setWorkflowStageTypeGroupName((String) row[6]);
            } else {
                sysWorkflowStageTypeTO.setWorkflowStageTypeGroupID(0);
            }
            sysWorkflowTypeMap.get(sysWorkflowStageTypeTO.getWorkflowTypeID()).getSysWorkflowStageTypeTOs().add(sysWorkflowStageTypeTO);
        }
        return sysWorkflowTypeTOs;
    }

    public void addSysWorkflowType(SysWorkflowTypeTO sysWorkflowTypeTO) {
        SysWorkflowType_Revamp sysWorkflowType = new SysWorkflowType_Revamp();

        sysWorkflowType.setWorkflowType(sysWorkflowTypeTO.getWorkflowType());
        sysWorkflowType.setSysTenantId(1);
        sysWorkflowType.setSysSubModuleId(sysWorkflowTypeTO.getModuleId());
        sysWorkflowType.setCreatedBy(userSessionBean.getUserID().intValue());
        sysWorkflowType.setModifiedBy(userSessionBean.getUserID().intValue());

        Date currentDate = new Date();
        sysWorkflowType.setCreatedDate(currentDate);
        sysWorkflowType.setModifiedDate(currentDate);

        sysWorkflowTypeDS.create("TalentPact", sysWorkflowType);

        sysWorkflowTypeTO.setWorkflowTypeID(sysWorkflowType.getWorkflowTypeID());
    }

    public void updateSysWorkflowType(SysWorkflowTypeTO sysWorkflowTypeTO) {
        SysWorkflowType_Revamp sysWorkflowType = sysWorkflowTypeDS.find("TalentPact", sysWorkflowTypeTO.getWorkflowTypeID());

        sysWorkflowType.setWorkflowType(sysWorkflowTypeTO.getWorkflowType());
        sysWorkflowType.setSysSubModuleId(sysWorkflowTypeTO.getModuleId());
        sysWorkflowType.setModifiedBy(userSessionBean.getUserID().intValue());

        Date currentDate = new Date();
        sysWorkflowType.setModifiedDate(currentDate);

        sysWorkflowTypeDS.edit("TalentPact", sysWorkflowType);
    }

    public void addOrUpdateSysWorkflowStageType(SysWorkflowStageTypeTO sysWorkflowStageTypeTO) {
        SysWorkflowStageType_Revamp sysWorkflowStageType = null;

        if (sysWorkflowStageTypeTO.getWorkflowStageTypeID() == null) {
            sysWorkflowStageType = new SysWorkflowStageType_Revamp();
            sysWorkflowStageType.setSysTenantId(1);
            sysWorkflowStageType.setCreatedBy(userSessionBean.getUserID().intValue());
            sysWorkflowStageType.setModifiedBy(sysWorkflowStageType.getCreatedBy());
            sysWorkflowStageType.setCreatedDate(new Date());
            sysWorkflowStageType.setModifiedDate(sysWorkflowStageType.getCreatedDate());
            sysWorkflowStageType.setSysWorkflowTypeId(sysWorkflowStageTypeTO.getWorkflowTypeID());
        } else {
            sysWorkflowStageType = sysWorkflowStageTypeDS.find("TalentPact", sysWorkflowStageTypeTO.getWorkflowStageTypeID());
            sysWorkflowStageType.setModifiedBy(userSessionBean.getUserID().intValue());
            sysWorkflowStageType.setModifiedDate(new Date());
        }

        sysWorkflowStageType.setWorkflowStageType(sysWorkflowStageTypeTO.getWorkflowStageType());
        sysWorkflowStageType.setUiFormId(sysWorkflowStageTypeTO.getUiFormID());
        if (sysWorkflowStageTypeTO.getWorkflowStageTypeGroupID() != 0) {
            sysWorkflowStageType.setSysWorkflowStageTypeGroupId(sysWorkflowStageTypeTO.getWorkflowStageTypeGroupID());
        } else {
            sysWorkflowStageType.setSysWorkflowStageTypeGroupId(null);
        }

        if (sysWorkflowStageTypeTO.getWorkflowStageTypeID() == null) {
            sysWorkflowStageTypeDS.create("TalentPact", sysWorkflowStageType);
            sysWorkflowStageTypeTO.setWorkflowStageTypeID(sysWorkflowStageType.getWorkflowStageTypeID());
        } else {
            sysWorkflowStageTypeDS.edit("TalentPact", sysWorkflowStageType);
        }
    }

    public void addOrUpdateSysWorkflowStageTypeGroup(SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO) {
        SysWorkflowStageTypeGroup sysWorkflowStageTypeGroup = null;

        if (sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID() == null) {
            sysWorkflowStageTypeGroup = new SysWorkflowStageTypeGroup();
            sysWorkflowStageTypeGroup.setSysTenantId(1);
            sysWorkflowStageTypeGroup.setCreatedBy(userSessionBean.getUserID().intValue());
            sysWorkflowStageTypeGroup.setModifiedBy(sysWorkflowStageTypeGroup.getCreatedBy());
            sysWorkflowStageTypeGroup.setCreatedDate(new Date());
            sysWorkflowStageTypeGroup.setModifiedDate(sysWorkflowStageTypeGroup.getCreatedDate());
            sysWorkflowStageTypeGroup.setWorkflowTypeId(sysWorkflowStageTypeGroupTO.getWorkflowTypeID());
        } else {
            sysWorkflowStageTypeGroup = sysWorkflowStageTypeGroupDS.find("TalentPact", sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID());
            sysWorkflowStageTypeGroup.setModifiedBy(userSessionBean.getUserID().intValue());
            sysWorkflowStageTypeGroup.setModifiedDate(new Date());
        }

        sysWorkflowStageTypeGroup.setWorkflowStageTypeGroupName(sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupName());

        if (sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID() == null) {
            sysWorkflowStageTypeGroupDS.create("TalentPact", sysWorkflowStageTypeGroup);
            sysWorkflowStageTypeGroupTO.setWorkflowStageTypeGroupID(sysWorkflowStageTypeGroup.getWorkflowStageTypeGroupId());
        } else {
            sysWorkflowStageTypeGroupDS.edit("TalentPact", sysWorkflowStageTypeGroup);
        }
    }
}
