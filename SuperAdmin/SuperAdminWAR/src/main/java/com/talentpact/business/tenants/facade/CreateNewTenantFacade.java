/**
 * 
 */
package com.talentpact.business.tenants.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.tenants.exception.TenantException;
import com.talentpact.business.tenants.service.impl.CreateTenantService;
import com.talentpact.remote.newtenant.ICreateNewTenantRemote;

/**
 * @author radhamadhab.dalai
 *
 */
@Stateless
public class CreateNewTenantFacade extends CommonFacade implements ICreateNewTenantRemote
{
    /* method to update modules for new tenant */

    @EJB
    CreateTenantService createTenantService;

    public ServiceResponse updateModules(ServiceRequest serviceRequest)
        throws TenantException
    {
        ServiceResponse response = null;
        try {

            return response;
        } catch (Exception e) {
            throw new TenantException();
        }
    }


    /* method to create a new tenant */
    public ServiceResponse saveOrUpdateTenant(ServiceRequest serviceRequest)
        throws TenantException
    {
        ServiceResponse response = null;
        try {

            response = createTenantService.saveOrUpdateTenant(serviceRequest);

            return response;
        } catch (Exception e) {
            throw new TenantException();
        }
    }

    /* method to get All tennats */


    public ServiceResponse getAllTenants(ServiceRequest serviceRequest)
        throws TenantException
    {
        ServiceResponse response = null;
        try {

            response = createTenantService.getAllTenants(serviceRequest);

            return response;
        } catch (Exception e) {
            throw new TenantException();
        }
    }
}
