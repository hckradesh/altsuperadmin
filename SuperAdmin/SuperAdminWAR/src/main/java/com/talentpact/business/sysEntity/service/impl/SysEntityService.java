package com.talentpact.business.sysEntity.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.talentpact.business.application.transport.input.SysEntityRequestTO;
import com.talentpact.business.application.transport.output.SysEntityResponseTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.sysEntity.SysEntityDS;
import com.talentpact.business.sysEntity.converter.SysEntityConverter;
import com.talentpact.model.SysEntity;
import com.talentpact.ui.sysEntity.Exception.SysEntityException;
import com.talentpact.ui.sysEntity.constants.ISysEntityConstants;

/**
 * @author raghvendra.mishra
 */

@Stateless
public class SysEntityService extends CommonService {

	private static final Logger LOGGER = Logger.getLogger(SysEntityService.class.toString());
	
	@EJB
	SysEntityDS sysEntityDS;

	public List<com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO> getAllEntities() throws Exception {
		return sysEntityDS.getAllEntities();
	}

	/**
	 * 
	 * @param serviceRequest
	 * @return
	 * @author raghvendra.mishra
	 */
	public ServiceResponse getSysEntityList(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		List<SysEntityResponseTO> result = null;
		List<SysEntity> sysEntityList = null;
		SysEntityResponseTO sysEntityResponseTO = null;
		SysEntityConverter sysEntityConverter = null;
		try {
			serviceResponse = new ServiceResponse();
			sysEntityResponseTO = new SysEntityResponseTO();
			sysEntityConverter = new SysEntityConverter();
			sysEntityList = sysEntityDS.getSysEntityDetails();
			result = sysEntityConverter.convertSysEntityToDataTO(sysEntityList);
			sysEntityResponseTO.setSysEntityResponseTOList(result);
			serviceResponse.setResponseTransportObject(sysEntityResponseTO);
		} catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return serviceResponse;
	}
	
	/*
	 * method to save a SysEntity 
	 * @param serviceRequest
	 * @author raghvendra.mishra
	 */
	public ServiceResponse saveOrUpdateSysEntity(ServiceRequest serviceRequest){      
		SysEntityRequestTO sysEntityRequestTO = null;
		ServiceResponse serviceResponse = null;
		SysEntity sysEntity = null;
		SysEntityConverter sysEntityConverter = null;
		List<SysEntityResponseTO> result = null;
		List<SysEntity> sysEntityList = null;
		try {
			sysEntityConverter = new SysEntityConverter();
			serviceResponse = new ServiceResponse();
			sysEntityList = new ArrayList<SysEntity>();
			sysEntityRequestTO = (SysEntityRequestTO) serviceRequest.getRequestTansportObject();
			/* Save SysEntity*/
			sysEntity =  sysEntityDS.saveOrUpdateSysEntity(sysEntityRequestTO);
			sysEntityList.add(sysEntity);
			result = sysEntityConverter.convertSysEntityToDataTO(sysEntityList);
			serviceResponse.setResponseTransportObject(result.get(0));
		} catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return serviceResponse;
	}
	
	public ServiceResponse updatePrimaryKeyColumnDetailsInSysEntity(ServiceRequest serviceRequest){      
		SysEntityRequestTO sysEntityRequestTO = null;
		ServiceResponse serviceResponse = null;
		SysEntity sysEntity = null;
		SysEntityConverter sysEntityConverter = null;
		List<SysEntityResponseTO> result = null;
		List<SysEntity> sysEntityList = null;
		try {
			sysEntityConverter = new SysEntityConverter();
			serviceResponse = new ServiceResponse();
			sysEntityList = new ArrayList<SysEntity>();
			sysEntityRequestTO = (SysEntityRequestTO) serviceRequest.getRequestTansportObject();
			/* Save SysEntity*/
			sysEntity =  sysEntityDS.updatePrimaryKeyColumnDetailsInSysEntity(sysEntityRequestTO);
			sysEntityList.add(sysEntity);
			result = sysEntityConverter.convertSysEntityToDataTO(sysEntityList);
			serviceResponse.setResponseTransportObject(result.get(0));
		} catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return serviceResponse;
	}

	/**
	 * to find table name list from INFORMATION_SCHEMA.TABLES which are not in SysEntity
	 * @return
	 * @author raghvendra.mishra
	 */
	public List<String> getAllTablesFromInformationSchema() {
		List<String> tableNameListNotPresentInSysEntity = null;
		try{
		tableNameListNotPresentInSysEntity = sysEntityDS.getTableNameList();
		}catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return tableNameListNotPresentInSysEntity;
		
	}
	
	/**
	 * method to obtain COLUMN_NAME and DATA_TYPE of all columns in a table
	 * @param tableName
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public List<Object[]> getColumnNamesAndDataTypeOfAllColumnsOfATable(String tableName) throws SysEntityException{
		String[] columnNameDataTypeArray = new String[2];
		List<Object[]> columnNameDataTypeList = null;
		try{
			columnNameDataTypeList = sysEntityDS.getColumnNamesAndDataTypeOfAllColumnsOfATable(tableName);
			
			}catch (Exception ex) {
				LOGGER.error("", ex);
				throw new SysEntityException(ISysEntityConstants.SYSENTITY_COLUMN_NAME_DATA_TYPE_LIST_ERROR_MSG, ex);
			}
			return columnNameDataTypeList;
	}
	
	/**
	 * method to get primaryColumnNames on the basis of tableName( Table may also contain composite key( i.e. combination of more than two columns as primary key
	 * , in that case this method will return more than two column as primary key column name)) 
	 * @param tableName
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public List<String> getTablesPrimaryKeyColumnNames(String tableName) throws SysEntityException{
  		List<String> primaryKeyColumnNameList = null;
  		try{
  		primaryKeyColumnNameList = sysEntityDS.getTablesPrimaryKeyColumnNames(tableName);
  		}catch (Exception ex) {
			LOGGER.error("", ex);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_TABLES_PRIMARY_COLUMN_NAMES_RETRIEVE_ERROR_MSG, ex);
		}
		return primaryKeyColumnNameList;
  	}
	
	public ServiceResponse findSysEntity(int sysEntityId) throws SysEntityException{
		ServiceResponse serviceResponse = null;
	    SysEntityResponseTO result = null;
		SysEntity sysEntity = null;
		List<SysEntity> sysEntityList = null;
		SysEntityConverter sysEntityConverter = null;
		try {
			sysEntityList = new ArrayList<SysEntity>();
			serviceResponse = new ServiceResponse();
			sysEntityConverter = new SysEntityConverter();
			sysEntity = sysEntityDS.findSysEntity(sysEntityId);
			sysEntityList.add(sysEntity);
			result = (sysEntityConverter.convertSysEntityToDataTO(sysEntityList)).get(0);
			serviceResponse.setResponseTransportObject(result);
		} catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return serviceResponse;
	}
		
}
