package com.talentpact.business.sysModule.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.sysModule.SysModuleDS;
import com.talentpact.business.sysModule.converter.SysModuleConverter;
import com.talentpact.model.SysModule;
import com.talentpact.ui.sysModule.common.transport.output.SysModuleResponseTO;
import com.talentpact.ui.sysModule.constants.ISysModuleConstants;
import com.talentpact.ui.sysModule.exception.SysModuleException;

/**
 * 
 * @author raghvendra.mishra
 *
 */
@Stateless
public class SysModuleService extends CommonService {

	private static final Logger LOGGER = Logger.getLogger(SysModuleService.class.toString());

	@EJB
	SysModuleDS sysModuleDS;
	
	/**
	 * 
	 * @param serviceRequest
	 * @return
	 * @author raghvendra.mishra
	 */
	public ServiceResponse getAllSysModuleList(ServiceRequest serviceRequest) throws SysModuleException{
		ServiceResponse serviceResponse = null;
		List<SysModuleResponseTO> sysModuleResponseTOList = null;
		List<SysModule> sysModuleList = null;
		SysModuleConverter sysModuleConverter = null;
		try {
			serviceResponse = new ServiceResponse();
			sysModuleConverter = new SysModuleConverter();
			sysModuleList = sysModuleDS.getAllModules();
			sysModuleResponseTOList = sysModuleConverter.convertSysEntityToDataTO(sysModuleList);
			serviceResponse.setResponseTransferObjectObjectList(sysModuleResponseTOList);
		} catch (Exception ex) {
			LOGGER.error("", ex);
			throw new SysModuleException(ISysModuleConstants.SYSMODULE_RETRIEVE_ERROR_MSG);
		}
		return serviceResponse;
	}
}
