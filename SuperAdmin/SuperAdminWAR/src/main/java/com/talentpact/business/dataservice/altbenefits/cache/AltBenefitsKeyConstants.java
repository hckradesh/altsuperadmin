package com.talentpact.business.dataservice.altbenefits.cache;

/**
 * 
 * @author javed.ali
 *
 */
public interface AltBenefitsKeyConstants
{
 public static final String ALT_BENEFIT_KEY = "SUPERADMIN:ALTBENEFIT:";
    
    public static final String COLLON = ":";
    public static final String ALT_BENEFIT_ORG_OFFERING_TEMPLATE = "ALTADMIN:ALTBENEFIT:ORG:OFFERINGS";
    public static final String ALT_BENEFIT_HR_OFFERING = "ALTADMIN:ALTBENEFIT:HROFFERINGSMAP";
    public static final String ALT_BENEFIT_EMPGROUP_OFFERING = "ALTADMIN:ALTBENEFIT:EMPGROUPOFFERINGSMAP";
    public static final String ALT_BENEFIT_SECTOR_OFFERING = "ALTADMIN:ALTBENEFIT:SECTOROFFERINGSMAP";
    public static final String ALT_ORG_SECTOR_MAP = "ALTADMIN:ALTBENEFIT:ORGSECTORSMAP";
    public static final String ALT_BENEFIT_SYSTEM_OFFERING_LIST = "ALTADMIN:ALTBENEFIT:SYTEMOFFERINGSLIST";
    public static final String ALT_BENEFIT_OFFERING_TEMPLATE_CHECKLIST = "ALTADMIN:ALTBENEFIT:SYSTEM:OFFERINGS:CHECKLIST";
    public static final String ALT_BENEFIT_SECTOR_OFFERINGS_TEMPLATE = "ALTADMIN:ALTBENEFIT:SECTOR:OFFERINGS";
    public static final String ALT_BENEFIT_EMPGROUP_OFFERINGS_TEMPLATE = "ALTADMIN:ALTBENEFIT:EMPGROUP:OFFERINGS";
    public static final String ALT_BENEFIT_HR_VENDOR_OFFERING_TEMPLATE = "ALTADMIN:ALTBENEFIT:HR:OFFERINGS";
    
    public static final String NO_DATA_IN_REDIS ="Data not found in Redis. Moving to DB";
    
    
    public static final String ALT_BENEFIT_SYSTEM_OFFERING_TEMPLATE = "ALTADMIN:ALTBENEFIT:SYSTEM:OFFERINGS";
    public static final String ALT_BENEFIT_SYSTEN_VENDOR_OFFERING_CODE = "ALTADMIN:ALTBENEFIT:OFFERINGS:OFFERINGCODE";
    public static final String ALT_BENEFIT_HR_OFFERING_TEMPALTE = "ALTADMIN:ALTBENEFIT:HR:OFFERINGS";
    public static final String DELETE_EMP_GROUP_WISE_REDIS_KEY ="Error occured while deleting employee group wise offerings Redis key.";
    public static final String DELETE_ORG_WISE_REDIS_KEY ="Error occured while deleting organization wise offerings Redis key.";
    public static final String DELETE_SECTOR_WISE_REDIS_KEY ="Error occured while deleting sector wise offerings Redis key.";
    public static final String DELETE_SYSTEM_OFFERING_TEMPLATE_REDIS_KEY ="Error occured while deleting system offerings template Redis key.";
    
}
