/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysVendorOfferingTemplate;
import com.talentpact.ui.altbenefits.transport.input.SaveOfferTempReqTO;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysVendorOfferingTemplateDS extends AbstractDS<SysVendorOfferingTemplate>
{
    public SysVendorOfferingTemplateDS()
    {
        super(SysVendorOfferingTemplate.class);
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public Long saveVendorOfferTemplate(SaveOfferTempReqTO saveOfferTempReqTO)
        throws Exception
    {
        SysVendorOfferingTemplate sVOT = null;
        VendorOfferingTemplateTO offeringTemplateTO = saveOfferTempReqTO.getOfferingTemplateTO();
        String fileLocation = saveOfferTempReqTO.getFileLocation();
        try {
            if (offeringTemplateTO.getVendorOfferingID() == null) {
                sVOT = new SysVendorOfferingTemplate();
                sVOT.setVendorID(offeringTemplateTO.getVendorID());
                sVOT.setOfferingTypeID(offeringTemplateTO.getOfferingTypeID());
                sVOT.setActive(offeringTemplateTO.getActive());
                sVOT.setOfferingLink(offeringTemplateTO.getOfferingLink().trim());
                sVOT.setOfferingSequence(getMaxVendorOfferingSequence(offeringTemplateTO.getVendorID()));
                sVOT.setOfferTemplateName(offeringTemplateTO.getOfferTemplateName().trim());
                sVOT.setContractSignedDateID(offeringTemplateTO.getContractSignedDateId());
                sVOT.setContractStartDateID(offeringTemplateTO.getContractStartDateId());
                sVOT.setContractEndDateID(offeringTemplateTO.getContractEndDateId());
                sVOT.setEffectiveStartDateID(offeringTemplateTO.getEffectiveStartDateId());
                sVOT.setEffectiveEndDateID(offeringTemplateTO.getEffectiveEndDateId());
                sVOT.setBanner(fileLocation);
                sVOT.setText(offeringTemplateTO.getText().trim());
                sVOT.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                sVOT.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                sVOT.setOfferingPeakBased(saveOfferTempReqTO.isPeakRecurringDetails());
                sVOT.setCreatedBy(1);
                sVOT.setModifiedBy(1);
                getEntityManager("Talentpact").persist(sVOT);

            } else {
                sVOT = getEntityManager("Talentpact").find(SysVendorOfferingTemplate.class, offeringTemplateTO.getVendorOfferingID());
                sVOT.setVendorID(offeringTemplateTO.getVendorID());
                sVOT.setOfferingTypeID(offeringTemplateTO.getOfferingTypeID());
                if (fileLocation != null) {
                    sVOT.setBanner(fileLocation);
                }
                sVOT.setActive(offeringTemplateTO.getActive());
                sVOT.setOfferingLink(offeringTemplateTO.getOfferingLink().trim());
                sVOT.setOfferTemplateName(offeringTemplateTO.getOfferTemplateName().trim());
                sVOT.setContractSignedDateID(offeringTemplateTO.getContractSignedDateId());
                sVOT.setContractStartDateID(offeringTemplateTO.getContractStartDateId());
                sVOT.setContractEndDateID(offeringTemplateTO.getContractEndDateId());
                sVOT.setEffectiveStartDateID(offeringTemplateTO.getEffectiveStartDateId());
                sVOT.setEffectiveEndDateID(offeringTemplateTO.getEffectiveEndDateId());
                sVOT.setOfferingPeakBased(saveOfferTempReqTO.isPeakRecurringDetails());
                sVOT.setText(offeringTemplateTO.getText());
                sVOT.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                sVOT.setCreatedBy(1);
                sVOT.setModifiedBy(1);
                getEntityManager("Talentpact").merge(sVOT);

            }

            getEntityManager("Talentpact").flush();
            return sVOT.getVendorOfferingID();
        } catch (Exception e) {
            throw e;
        }

    }


    @SuppressWarnings("unchecked")
    public List<SysVendorOfferingTemplate> getOfferingTemplateList(Long vendorID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  SysVendorOfferingTemplate s where s.vendorID=:vendorID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorID", vendorID);
            sysVendorOfferingTemplateList = query.getResultList();

        } catch (Exception ex) {
            throw ex;
        }
        return sysVendorOfferingTemplateList;
    }


    /**
     * 
     * @param vendorId
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public int getMaxVendorOfferingSequence(Long vendorId)
        throws Exception
    {
        List<Integer> typeResultList = null;
        StringBuilder hqlquery = null;
        Query query = null;
        int maxSequence = 0;
        try {
            hqlquery = new StringBuilder();
            hqlquery.append("select MAX(sv.offeringSequence) from SysVendorOfferingTemplate sv ");
            hqlquery.append("where sv.vendorID = :vendorId and sv.active=1");
            query = getEntityManager("Talentpact").createQuery(hqlquery.toString());
            query.setParameter("vendorId", vendorId);
            typeResultList = query.getResultList();
            if (!typeResultList.isEmpty()) {
                if (typeResultList.get(0) != null) {
                    maxSequence = typeResultList.get(0);
                }
            }
            return ++maxSequence;
        } catch (Exception e) {
            throw e;
        }
    }


    @SuppressWarnings("unchecked")
    public List<SysVendorOfferingTemplate> getVendorOfferingTemplateList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysVendorOfferingTemplate> sysVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select s from  SysVendorOfferingTemplate s where s.active=1 and s.sysBenefitsVendor.active=1 order by s.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            sysVendorOfferingTemplateList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return sysVendorOfferingTemplateList;
    }


}