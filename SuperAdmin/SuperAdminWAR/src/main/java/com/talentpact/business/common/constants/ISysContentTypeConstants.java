package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysContentTypeConstants
{

    public static String SYSCONTENTTYPE_ADD_DIALOG_HIDE = "PF('sysContentTypeAddModal').hide();";

    public static String SYSCONTENTTYPE_EDIT_DIALOG_HIDE = "PF('sysContentTypeEditModal').hide();";

    public static String SYSCONTENTTYPE_SAVE_ERROR_MSG = "Error occured while saving SysContentType.";

    public static String SYSCONTENTTYPE_UPDATE_ERROR_MSG = "Error occured while updating SysContentType.";

    public static String SYSCONTENTTYPE_SAVE_SUCCESS_MSG = "SysContentType saved successfully.";

    public static String SYSCONTENTTYPE_UPDATE_SUCCESS_MSG = "SysContentType updated successfully.";

    public static String SYSCONTENTTYPE_DATATABLE_RESET = "PF('sysContentTypeDataTable').clearFilters();";

    public static String SYSCONTENTTYPE_PAGINATION_DATATABLE_RESET = "PF('sysContentTypeDataTable').getPaginator().setPage(0);";

    public static String SYSCONTENTTYPE_DUPLICATE_ERROR_MSG = "SysContentType already present in system.";
}
