/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.HrVendorOfferingSequence;
import com.talentpact.model.altbenefit.HrVendorOfferingTemplate;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class HrVendorOfferingSequenceDS extends AbstractDS<HrVendorOfferingSequence>
{
    public HrVendorOfferingSequenceDS()
    {
        super(HrVendorOfferingSequence.class);
    }


    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingSequence> getHrOfferingSequenceList(Long orgId, Long tenantId)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingSequence> hrVendorOfferingSequenceList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingSequence s  where s.hrVendorOfferingTemplate.organizationID=:orgId and s.hrVendorOfferingTemplate.tenantID=:tenantId and s.active=true order by s.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgId", orgId);
            query.setParameter("tenantId", tenantId.intValue());
            hrVendorOfferingSequenceList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequenceList;
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveHrVendorOfferSequence(List<Integer> sequenceList, BenefitRequestTO benefitRequestTO)
        throws Exception
    {
        Long hrVendorOfferingId = null;
        HrVendorOfferingSequence offeringSequence = null;
        Map<Integer, Long> hrVendorOfferSequenceMap = null;
        try {
            hrVendorOfferSequenceMap = benefitRequestTO.getHrVendorOfferSequenceMap();
            if (!benefitRequestTO.isEdit()) {
                for (int i = 0; i < hrVendorOfferSequenceMap.size(); i++) {
                    hrVendorOfferingId = hrVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence = new HrVendorOfferingSequence();
                    offeringSequence.setHrVendorOfferingID(hrVendorOfferingId);
                    offeringSequence.setOfferingSequence(i + 1);
                    offeringSequence.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                    offeringSequence.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                    offeringSequence.setCreatedBy(1);
                    offeringSequence.setModifiedBY(1);
                    getEntityManager("Talentpact").persist(offeringSequence);
                }
            } else {
                for (int i = 0; i < hrVendorOfferSequenceMap.size(); i++) {
                    hrVendorOfferingId = hrVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence = findBenefitsHrBasedByVendorOfferingId(hrVendorOfferingId);
                    if (offeringSequence != null) {
                        offeringSequence.setOfferingSequence(i + 1);
                        offeringSequence.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                        offeringSequence.setModifiedBY(1);
                        getEntityManager("Talentpact").merge(offeringSequence);
                    } else {
                        offeringSequence = new HrVendorOfferingSequence();
                        offeringSequence.setHrVendorOfferingID(hrVendorOfferingId);
                        offeringSequence.setOfferingSequence(i + 1);
                        offeringSequence.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                        offeringSequence.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                        offeringSequence.setCreatedBy(1);
                        offeringSequence.setModifiedBY(1);
                        getEntityManager("Talentpact").persist(offeringSequence);
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrVendorOfferingTemplate findBenefitsHrBasedByOfferingId(Long hrVendorOfferingId, Long orgId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingSequenceList = null;
        HrVendorOfferingTemplate hrVendorOfferingSequence = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where s.hrVendorOfferingID=:hrVendorOfferingId and s.organizationID=:orgId  ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("hrVendorOfferingId", hrVendorOfferingId);
            query.setParameter("orgId", orgId);
            hrVendorOfferingSequenceList = query.getResultList();
            if (hrVendorOfferingSequenceList != null && !hrVendorOfferingSequenceList.isEmpty()) {
                hrVendorOfferingSequence = hrVendorOfferingSequenceList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequence;
    }


    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrVendorOfferingSequence findBenefitsHrBasedByVendorOfferingId(Long hrVendorOfferingId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingSequence> hrVendorOfferingSequenceList = null;
        HrVendorOfferingSequence hrVendorOfferingSequence = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingSequence s  where s.hrVendorOfferingID=:hrVendorOfferingId ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("hrVendorOfferingId", hrVendorOfferingId);
            hrVendorOfferingSequenceList = query.getResultList();
            if (hrVendorOfferingSequenceList != null && !hrVendorOfferingSequenceList.isEmpty()) {
                hrVendorOfferingSequence = hrVendorOfferingSequenceList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequence;
    }

    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingSequence> getHrOfferingSequenceListByOrgID(Long orgID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingSequence> hrVendorOfferingSequenceList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingSequence s  where s.hrVendorOfferingTemplate.organizationID =:orgID and s.hrVendorOfferingTemplate.active=true order by s.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            hrVendorOfferingSequenceList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequenceList;
    }

    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingSequence> getHrOfferingTemplateList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingSequence> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingSequence s  where  s.hrVendorOfferingTemplate.active=true order by s.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            hrVendorOfferingTemplateList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }

}