/**
 * 
 */
package com.talentpact.business.service.loginserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.common.constants.CoreConstants;
import com.talentpact.ats.core.entity.LoginServerDetail;
import com.talentpact.ats.core.entity.LoginServerType;
import com.talentpact.ats.core.entity.TpSocialAppSettings;
import com.talentpact.business.application.loginserver.LoginServerTypeTO;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.loginserver.LoginServerDetailDS;
import com.talentpact.business.dataservice.loginserver.LoginServerTypeDS;
import com.talentpact.business.dataservice.loginserver.SocialAppSettingDS;
import com.talentpact.business.dataservice.loginserver.cache.LoginServerRedisCache;
import com.talentpact.business.dataservice.loginserver.cache.constants.LoginServerKeyConstants;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class LoginServerService extends CommonService
{
    @EJB
    LoginServerTypeDS loginServerTypeDS;

    @EJB
    LoginServerDetailDS loginServerDetailDS;

    @EJB
    SocialAppSettingDS socialAppSettingDS;

    @EJB
    LoginServerRedisCache loginServerRedisCache;

    /**
     * 
     * @return
     * @throws Exception
     */
    public List<LoginServerTypeTO> getLoginServerType()
        throws Exception
    {
        List<LoginServerTypeTO> loginServerTypeTOList = null;
        List<LoginServerType> loginServerTypelist = null;
        LoginServerTypeTO loginServerTypeTO = null;
        try {
            loginServerTypelist = loginServerTypeDS.getLoginServerType();
            if (loginServerTypelist != null && !loginServerTypelist.isEmpty()) {
                loginServerTypeTOList = new ArrayList<LoginServerTypeTO>();
                for (LoginServerType loginServerType : loginServerTypelist) {
                    loginServerTypeTO = new LoginServerTypeTO();
                    loginServerTypeTO.setLoginTypeID(loginServerType.getLoginServerTypeID());
                    loginServerTypeTO.setTypeName(loginServerType.getTypeName());
                    loginServerTypeTOList.add(loginServerTypeTO);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return loginServerTypeTOList;
    }

    /**
     * 
     * @param organizationId
     * @param urlID
     * @return
     * @throws Exception
     */
    public Map<String, SocialAppSettingTO> getLoginServerDetail(Long organizationId, Long urlID)
        throws Exception
    {
        List<LoginServerDetail> loginServerDetailsResultList = null;
        Map<String, SocialAppSettingTO> socialAppSettingTOMap = null;
        SocialAppSettingTO socialAppSettingTO = null;
        try {

            try {
                socialAppSettingTOMap = loginServerRedisCache.getLoginServerSettingMap(urlID, organizationId);
                if (socialAppSettingTOMap == null) {
                    throw new Exception(LoginServerKeyConstants.NO_DATA_IN_REDIS);
                }
            } catch (Exception ex) {
                try {
                    loginServerDetailsResultList = loginServerDetailDS.getLoginServerDetailByOrgID(organizationId.intValue());
                    if (loginServerDetailsResultList != null && !loginServerDetailsResultList.isEmpty()) {
                        socialAppSettingTOMap = new HashMap<String, SocialAppSettingTO>();
                        for (LoginServerDetail loginServerDetail : loginServerDetailsResultList) {
                            socialAppSettingTO = new SocialAppSettingTO();
                            socialAppSettingTO.setIsdefault(loginServerDetail.getIsDefault());
                            socialAppSettingTO.setSocialAppType(loginServerDetail.getLoginServerType().getTypeName());
                            socialAppSettingTO.setActive(loginServerDetail.getActive());
                            socialAppSettingTO.setDefaultLoginServer(loginServerDetail.getIsDefault());
                            socialAppSettingTO.setLogoutServerUrl(loginServerDetail.getLogoutServerUrl());
                            socialAppSettingTO.setServerName(loginServerDetail.getServerName());
                            List<TpSocialAppSettings> appSettingsList = socialAppSettingDS.getSocialAppSettingsByLoginServerType(socialAppSettingTO.getSocialAppType(), urlID);
                            for (TpSocialAppSettings appSettings : appSettingsList) {
                                socialAppSettingTO.setAccessLevel(appSettings.getAccessLevel());
                                socialAppSettingTO.setAppID(appSettings.getSocialAppID());
                                socialAppSettingTO.setAppKey(appSettings.getApiKey());
                                socialAppSettingTO.setAppName(appSettings.getSocialAppName());
                                socialAppSettingTO.setClientID(appSettings.getClientID());
                                socialAppSettingTO.setAppSecretKey(appSettings.getSecretKey());
                                socialAppSettingTO.setForwardUrl(appSettings.getForwardUri());
                                socialAppSettingTO.setDefaultCSS(appSettings.getDefaultCSS());
                                socialAppSettingTO.setRedirectUrl(appSettings.getRedirectUri());
                                socialAppSettingTO.setUrlID(urlID);
                            }
                            socialAppSettingTOMap.put(socialAppSettingTO.getSocialAppType() + CoreConstants.AT_SPERATOR
                                    + loginServerDetail.getLoginServerType().getLoginServerTypeID(), socialAppSettingTO);
                        }

                    }
                 //   loginServerRedisCache.putgetLoginServerSettingMap(socialAppSettingTOMap, urlID, organizationId);
                } catch (Exception e) {
                    throw e;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return socialAppSettingTOMap;
    }

}
