/**
 * 
 */
package com.talentpact.business.dataservice.sysPeakRecurringType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.altbenefits.to.SysPeakRecurringTypeTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.SysOfferingCodeMaximumLimitDS;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysPeakRecurringTypeService extends CommonService
{

    @EJB
    SysPeakRecurringTypeDS sysPeakRecurringTypeDS;

    @EJB
    SysOfferingCodeMaximumLimitDS codeMaximumLimitDS;

    public List<SysPeakRecurringTypeTO> getSysPeakRecurringTypeList()
        throws Exception
    {
        List<SysPeakRecurringTypeTO> list = null;
        SysPeakRecurringTypeTO sysPeakRecurringTypeTO = null;
        List<Object[]> sysPeakRecurringTypeList = null;

        try {
            sysPeakRecurringTypeList = sysPeakRecurringTypeDS.getSysPeakRecurringTypeList();
            list = new ArrayList<SysPeakRecurringTypeTO>();
            for (Object[] sysPeakRecurringType : sysPeakRecurringTypeList) {
                sysPeakRecurringTypeTO = new SysPeakRecurringTypeTO();
                sysPeakRecurringTypeTO.setRecurringTypeID((Long) sysPeakRecurringType[0]);
                if (sysPeakRecurringType[1] != null)
                    sysPeakRecurringTypeTO.setRecurringTypeLabel((String) sysPeakRecurringType[1]);
                list.add(sysPeakRecurringTypeTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addSysPeakRecurringType(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
        throws Exception
    {
        try {
            sysPeakRecurringTypeDS.addSysPeakRecurringType(sysPeakRecurringTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editSysPeakRecurringType(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
        throws Exception
    {
        try {
            sysPeakRecurringTypeDS.editSysPeakRecurringType(sysPeakRecurringTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}