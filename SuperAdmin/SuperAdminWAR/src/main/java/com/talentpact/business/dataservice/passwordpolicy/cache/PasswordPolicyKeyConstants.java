package com.talentpact.business.dataservice.passwordpolicy.cache;

/**
 * 
 * @author javed.ali
 *
 */
public interface PasswordPolicyKeyConstants
{
    public static final String COLLON = ":";

    public static final String NO_DATA_IN_REDIS = "Data not found in Redis. Moving to DB";
  
    public static final String SYS_PASSWORD_POLICY_KEY = "SUPERADMIN:SYSPASSWORDPOLICY";
}
