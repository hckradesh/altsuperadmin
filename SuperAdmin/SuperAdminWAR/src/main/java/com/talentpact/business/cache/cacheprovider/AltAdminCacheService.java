package com.talentpact.business.cache.cacheprovider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.authorization.transport.output.UserTO;
import com.alt.loginpage.transport.output.AppTO;
import com.alt.loginpage.transport.output.LoginPageResponseTO;
import com.talentpact.auth.entity.TpApp;
import com.talentpact.auth.entity.TpURL;
import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.cache.cacheprovider.converter.AltAdminCacheConverter;
import com.talentpact.business.common.exceptions.RedisConnectionException;
import com.talentpact.business.dataservice.TPOrgDS;
import com.talentpact.business.dataservice.TpUserDS;


@Stateless
public class AltAdminCacheService
{

    @EJB
    TPOrgDS tpOrgDs;

    @EJB
    TpUserDS tpUserDS;


    public void populateLoginCache(Long orgId, Long portalTypeId)
        throws CacheUpdateException
    {
        List<LoginPageResponseTO> urlTOList = null;
        List<Object[]> urlObjList = tpOrgDs.getOrganizationData(orgId, portalTypeId);
        List<TpURL> activeUrlList = tpOrgDs.getActiveTpUrl(orgId, null);
        List<AppTO> appToList = getActiveAppForOrg(activeUrlList);
        try {
            urlTOList = AltAdminCacheConverter.traverseDbResultSetInAuthenticationTOList(urlObjList);
        } catch (Exception ex) {
            System.out.print("Error Occurred while traversing TpUrlPortalLoginTO " + ex.getLocalizedMessage());
            throw new CacheUpdateException(ex);
        }

        /* key = Orgdata:PortalName:url */
        if (urlTOList != null) {
            String key = null;
            try {
                for (int i = 0; i < urlTOList.size(); i++) {
                    StringBuilder keybuilder = null;
                    LoginPageResponseTO loginResponseTO = null;
                    try {
                        loginResponseTO = urlTOList.get(i);
                        loginResponseTO.setAppList(appToList);
                        keybuilder = new StringBuilder();
                        /*Add Key = ALTADMIN:LOGIN:urlName  This TO comprises Cache data for Organization required for Custom page and Login Page */
                        keybuilder.append(RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY).append(loginResponseTO.getDomainName());
                        key = keybuilder.toString().toUpperCase();
                        RedisClient.putValueAsTO(key, loginResponseTO);
                    } catch (Exception ex) {
                        System.out.println("Exception Occurred for  " + ex.getMessage());
                        ex.printStackTrace();
                        throw new CacheUpdateException(ex);

                    }
                    keybuilder = null;
                    loginResponseTO = null;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CacheUpdateException(ex);
            }

        }

    }


    /*Load key = ALTADMIN:USER:userName_OrgID    value=TPUserTO */
    public void loadUserLoginDataInCache(Long orgId)
        throws CacheUpdateException

    {
        List<TpUser> userList = tpUserDS.getUserData(orgId);
        List<UserTO> userTOList = null;
        try {
            userTOList = traverseDBResultSetIntoTpUserTOList(userList);
        } catch (Exception ex) {
            System.out.print("Error Occurred while traversing TpUrlPortalLoginTO " + ex.getLocalizedMessage());
            throw new CacheUpdateException(ex);
        }
        if (userTOList != null) {
            String key = null;
            try {
                for (UserTO userTO : userTOList) {
                    StringBuilder keybuilder = new StringBuilder();
                    keybuilder.append(RedisCacheKeyConstant.ALT_ADMIN_USER_KEY).append(userTO.getUserName()).append("_").append(userTO.getOrganizationCode());
                    key = keybuilder.toString().toUpperCase();
                    RedisClient.putValueAsTO(key, userTO);
                }

            } catch (RedisConnectionException ex) {
                System.out.print("Error Occurred while Saving in Redis " + ex.getLocalizedMessage());
                throw new CacheUpdateException(ex);
            }

        }


    }


    private List<AppTO> getActiveAppForOrg(List<TpURL> activeUrlList)
    {
        Map<Long, AppTO> appMap = new HashMap<Long, AppTO>();
        List<AppTO> appList = new ArrayList<AppTO>();
        if (activeUrlList != null) {

            for (TpURL tpurl : activeUrlList) {
                TpApp app = tpurl.getTpPortalType().getTpAppId();
                if (!(appMap.containsKey(app.getAppID()))) {
                    AppTO appTo = new AppTO();
                    appTo.setAppId(app.getAppID());
                    appTo.setAppName(app.getName());
                    appTo.setAppCode(app.getCode());
                    appMap.put(app.getAppID(), appTo);
                    appList.add(appTo);
                }

            }
        }
        return appList;

    }


    private List<UserTO> traverseDBResultSetIntoTpUserTOList(List<TpUser> userListTo)
    {
        List<UserTO> tpUserList = null;
        if (userListTo != null) {
            tpUserList = new ArrayList<UserTO>();
            for (int i = 0; i < userListTo.size(); i++) {
                try {
                    TpUser tpuser = userListTo.get(i);
                    Long userId = 0l;
                    String userName = null;
                    String password = null;
                    Long organizationId = null;
                    Boolean enabled = false;
                    Boolean loginFlag = false;
                    String defaultImage = null;
                    String defaultTheme = null;
                    Long loginCount = 0l;
                    Long welcomeMailSend = 0l;
                    Integer createdBy = 0;
                    String organizationCode = null;
                    if (tpuser != null) {

                        userId = tpuser.getUserID();
                        userName = tpuser.getUsername();
                        organizationId = tpuser.getOrganizationID().getOrganizationID();
                        enabled = tpuser.getEnabled();
                        loginFlag = tpuser.getLoginFlag();
                        defaultImage = tpuser.getDefaultImage();
                        loginCount = tpuser.getLoginCount();
                        welcomeMailSend = tpuser.getWelcomeMailSend();
                        createdBy = tpuser.getCreatedBy();
                        organizationCode = tpuser.getOrganizationID().getOrganizationCode();

                        password = tpuser.getPassword();
                        userName = tpuser.getUsername();
                        userId = tpuser.getUserID();


                        UserTO tpuserTO = new UserTO();
                        tpuserTO.setUserID(userId);
                        tpuserTO.setUserName(userName);
                        tpuserTO.setPassword(password);
                        tpuserTO.setOrganizationId(organizationId);
                        tpuserTO.setEnabled(enabled);
                        tpuserTO.setLoginFlag(loginFlag);
                        tpuserTO.setDefaultImage(defaultImage);
                        tpuserTO.setDefaultTheme(defaultTheme);
                        tpuserTO.setLoginCount(loginCount);
                        tpuserTO.setWelcomeMailSend(welcomeMailSend);
                        tpuserTO.setCreatedBy(createdBy);
                        tpuserTO.setOrganizationCode(organizationCode);
                        tpUserList.add(tpuserTO);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }
        return tpUserList;
    }


}
