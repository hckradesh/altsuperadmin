package com.talentpact.business.service.system;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.altreports.constants.IConstant;
import com.alt.common.constants.CoreConstants;
import com.alt.system.transport.AltSyncUrlTO;
import com.alt.system.transport.SysDatabaseConfigTO;
import com.alt.system.transport.SysEnvTO;
import com.alt.system.transport.TpAppTO;
import com.alt.system.transport.UrlTO;
import com.alt.system.transport.input.InstanceMigrationRequestTO;
import com.alt.system.transport.output.InstanceMigrationResponseTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.system.InstanceMigrationDS;
import com.talentpact.business.model.AltSyncUrl;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class InstanceMigrationService extends CommonService
{
    @EJB
    InstanceMigrationDS instanceMigrationDS;


    /**
     * 
     * @return
     * @throws Exception
     */
    public List<AltSyncUrlTO> getAltSyncUrlIst()
        throws Exception
    {
        List<AltSyncUrlTO> altSyncUrlTOList = null;
        List<AltSyncUrl> altSyncUrlResultList = null;
        try {
            altSyncUrlResultList = instanceMigrationDS.getAltSyncUrlList();
            altSyncUrlTOList = new ArrayList<AltSyncUrlTO>();
            for (AltSyncUrl altSyncUrl : altSyncUrlResultList) {
                AltSyncUrlTO altSyncUrlTO = new AltSyncUrlTO();
                altSyncUrlTO.setUrlID(altSyncUrl.getUrlID());
                altSyncUrlTO.setUrlName(altSyncUrl.getUrl());
                altSyncUrlTO.setEnvID(altSyncUrl.getEnvID());
                altSyncUrlTO.setEnvName(altSyncUrl.getSysEnvironment().getName());
                altSyncUrlTO.setAppID(altSyncUrl.getAppID());
                altSyncUrlTO.setAppName(altSyncUrl.getTpApp().getName());
                altSyncUrlTO.setDbConfigID(altSyncUrl.getDbConfigID());
                altSyncUrlTO.setServerName(altSyncUrl.getSysDatabaseConfig().getServerName());
                if (altSyncUrl.getActive()) {
                    altSyncUrlTO.setStatus(CoreConstants.ACTIVE_STRING);
                } else {
                    altSyncUrlTO.setStatus(CoreConstants.INACTIVE_STRING);
                }
                altSyncUrlTOList.add(altSyncUrlTO);
            }
        } catch (Exception e) {
            throw e;
        }
        return altSyncUrlTOList;
    }

    public InstanceMigrationResponseTO fetchAllInfo()
        throws Exception
    {
        InstanceMigrationResponseTO instanceMigrationResponseTO = null;
        List<Object[]> envResultList = null;
        List<Object[]> appResultList = null;
        List<Object[]> serverResultList = null;
        List<SysEnvTO> sysEnvTOList = null;
        List<TpAppTO> appTOList = null;
        List<SysDatabaseConfigTO> serverTOList = null;
        try {
            envResultList = instanceMigrationDS.getEnvironmentList();
            appResultList = instanceMigrationDS.getAppList();
            serverResultList = instanceMigrationDS.getDatabaseConfigList();
            sysEnvTOList = processEnvResultLIst(envResultList);
            appTOList = processAppResultLIst(appResultList);
            serverTOList = processServerResultLIst(serverResultList);
            instanceMigrationResponseTO = new InstanceMigrationResponseTO();
            instanceMigrationResponseTO.setSysEnvTOList(sysEnvTOList);
            instanceMigrationResponseTO.setAppTOList(appTOList);
            instanceMigrationResponseTO.setDatabaseConfigTOList(serverTOList);
        } catch (Exception e) {
            throw e;
        }
        return instanceMigrationResponseTO;
    }

    /**
     * 
     * @param envResultList
     * @return
     * @throws Exception
     */
    public List<SysEnvTO> processEnvResultLIst(List<Object[]> envResultList)
        throws Exception
    {
        List<SysEnvTO> sysEnvTOList = null;
        try {
            sysEnvTOList = new ArrayList<SysEnvTO>();
            if (envResultList != null && !envResultList.isEmpty()) {
                for (Object[] ob : envResultList) {
                    SysEnvTO envTO = new SysEnvTO();
                    envTO.setEnvID((Integer) ob[0]);
                    envTO.setEnvName(ob[1].toString());
                    sysEnvTOList.add(envTO);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return sysEnvTOList;
    }

    /**
     * 
     * @param appResultList
     * @return
     * @throws Exception
     */
    public List<TpAppTO> processAppResultLIst(List<Object[]> appResultList)
        throws Exception
    {
        List<TpAppTO> appTOList = null;
        try {
            appTOList = new ArrayList<TpAppTO>();
            if (appResultList != null && !appResultList.isEmpty()) {
                for (Object[] ob : appResultList) {
                    TpAppTO appTO = new TpAppTO();
                    appTO.setAppID(((BigInteger) ob[0]).longValue());
                    appTO.setName(ob[1].toString());
                    appTOList.add(appTO);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return appTOList;
    }

    /**
     * 
     * @param serverResultList
     * @return
     * @throws Exception
     */
    public List<SysDatabaseConfigTO> processServerResultLIst(List<Object[]> serverResultList)
        throws Exception
    {
        List<SysDatabaseConfigTO> serverTOList = null;
        try {
            serverTOList = new ArrayList<SysDatabaseConfigTO>();
            if (serverResultList != null && !serverResultList.isEmpty()) {
                for (Object[] ob : serverResultList) {
                    SysDatabaseConfigTO server = new SysDatabaseConfigTO();
                    server.setDbConfigID((Integer) ob[0]);
                    server.setServerName(ob[1].toString());
                    serverTOList.add(server);
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return serverTOList;
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    public List<UrlTO> fetchUrlList(Integer dbServerID, Long appID)
        throws Exception
    {
        List<UrlTO> urlTOList = null;
        List<Object[]> urlResultList = null;
        try {
            urlResultList = instanceMigrationDS.getUrlList(dbServerID, appID);
            urlTOList = new ArrayList<UrlTO>();
            for (Object[] ob : urlResultList) {
                UrlTO urlTO = new UrlTO();
                urlTO.setUrlID(((BigInteger) ob[0]).intValue());
                urlTO.setUrl((String) ob[1]);
                urlTO.setOrganizatonID(((BigInteger) ob[2]).intValue());
                urlTO.setCompositeID(urlTO.getUrlID() + IConstant.HASH_SPERATOR_STRING + urlTO.getUrl() + IConstant.HASH_SPERATOR_STRING + urlTO.getOrganizatonID());
                urlTOList.add(urlTO);
            }
        } catch (Exception e) {
            throw e;
        }
        return urlTOList;
    }

    public InstanceMigrationResponseTO saveAltUrl(InstanceMigrationRequestTO instanceMigrationRequestTO)
    {
        InstanceMigrationResponseTO instanceMigrationResponseTO = null;
        Integer urlResultListValue = null;
        try {
            urlResultListValue = instanceMigrationDS.syncAltURL(instanceMigrationRequestTO);
            if (urlResultListValue == 1) {
                instanceMigrationResponseTO = new InstanceMigrationResponseTO();
            }

        } catch (Exception e) {
            throw e;
        }
        return instanceMigrationResponseTO;
    }


}