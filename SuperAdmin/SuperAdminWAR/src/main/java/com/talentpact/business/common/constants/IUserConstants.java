/**
 * 
 */
package com.talentpact.business.common.constants;

/**
 * @author pankaj.sharma1
 *
 */
public interface IUserConstants
{
    public static String USER_ADD_SUCCESS_MSG = "User created successfully.";

    public static String USER_ADD_ERROR_MSG = "Unexpected error occured while creating new user.";


    public static String USER_UPDATE_SUCCESS_MSG = "User details updated successfully.";

    public static String USER_UPDATE_ERROR_MSG = "Unexpected error occured while updating user details.";

    public static String ACTIVE_STRING = "Active";

    public static String INACTIVE_STRING = "Inactive";
    
}
