/**
 * 
 */
package com.talentpact.business.dataservice.sysCongratulationAnnouncement;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysCongratulationAnnouncementResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.SysConstant.SysConstantService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCongratulationAnnouncementService extends CommonService
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantService.class);

    @EJB
    SysCongratulationAnnouncementDS sysCongratulationAnnouncementDS;

    public List<SysCongratulationAnnouncementResponseTO> getSysCongratulationAnnouncementTOList()
        throws Exception
    {
        List<SysCongratulationAnnouncementResponseTO> list = null;


        try {
            list = sysCongratulationAnnouncementDS.getSysCongratulationAnnouncementTOList();
            return list;
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
    }

    public List<SysContentTypeTO> getSysContentTypeDropDown()
        throws Exception
    {
        List<SysContentTypeTO> list = null;


        try {
            list = sysCongratulationAnnouncementDS.getSysContentTypeDropDown();
            return list;
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
    }

    public void save(SysCongratulationAnnouncementResponseTO to)
        throws Exception
    {
        try {
            sysCongratulationAnnouncementDS.save(to);

        } catch (Exception e) {
            LOGGER_.info(e.getMessage());

        }

    }

}
