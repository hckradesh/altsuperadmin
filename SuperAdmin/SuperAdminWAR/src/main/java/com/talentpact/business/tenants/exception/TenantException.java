/**
 * 
 */
package com.talentpact.business.tenants.exception;

/**
 * @author radhamadhab.dalai
 *
 */
public class TenantException extends Exception
{

    private String message;

    public TenantException()
    {

    }

    public TenantException(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
