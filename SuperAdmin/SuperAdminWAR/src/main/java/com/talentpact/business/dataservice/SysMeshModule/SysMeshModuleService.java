/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshModule;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysMeshModule;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysMeshModuleService extends CommonService
{

    @EJB
    SysMeshModuleDS sysMeshModuleDS;

    public List<SysMeshModuleTO> getSysMeshModuleList()
        throws Exception
    {
        List<SysMeshModuleTO> list = null;
        SysMeshModuleTO sysMeshModuleTO = null;
        List<Object[]> sysMEshModuleList = null;

        try {
            sysMEshModuleList = sysMeshModuleDS.getSysMeshModuleList();
            list = new ArrayList<SysMeshModuleTO>();
            for (Object[] sysOfferingCategory : sysMEshModuleList) {
            	sysMeshModuleTO = new SysMeshModuleTO();
            	if(sysOfferingCategory[0] != null)
            	{
            		sysMeshModuleTO.setSysMeshModuleID((int)sysOfferingCategory[0]);
            	}
            	if(sysOfferingCategory[1] != null)
            	{
            		sysMeshModuleTO.setSysMeshModuleName((String)sysOfferingCategory[1]);
            	}
            	if(sysOfferingCategory[2] != null)
            	{
            		sysMeshModuleTO.setSysMeshModuleDescription((String)sysOfferingCategory[2]);
            	}
            	if(sysOfferingCategory[3] != null)
            	{
            		sysMeshModuleTO.setModuleID((int)sysOfferingCategory[3]);
            	}
            	if(sysOfferingCategory[4] != null)
            	{
            		sysMeshModuleTO.setModuleName((String)sysOfferingCategory[4]);
            	}
            	
            	
                list.add(sysMeshModuleTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    public boolean validateSysMeshModuleList(SysMeshModuleTO sysMeshModuleTO)
            throws Exception
        {
                return sysMeshModuleDS.validateSysMeshModuleList(sysMeshModuleTO);
        }
    
    public List<SysMeshModuleTO> getSysSubModuleList()
            throws Exception
        {
            List<SysMeshModuleTO> list = null;
            SysMeshModuleTO sysMeshModuleTO = null;
            List<Object[]> sysOfferingCategoryList = null;

            try {
                sysOfferingCategoryList = sysMeshModuleDS.getSysSubModuleList();
                list = new ArrayList<SysMeshModuleTO>();
                for (Object[] sysOfferingCategory : sysOfferingCategoryList) {
                	sysMeshModuleTO = new SysMeshModuleTO();
                	if(sysOfferingCategory[0] != null)
                	{
                		sysMeshModuleTO.setModuleID((int)sysOfferingCategory[0]);
                	}
                	if(sysOfferingCategory[1] != null)
                	{
                		sysMeshModuleTO.setModuleName((String)sysOfferingCategory[1]);
                	}
                    list.add(sysMeshModuleTO);
                }
                return list;
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }

    public void saveSysMeshModule(SysMeshModuleTO sysMeshModuleTO)
            throws Exception
        {
            try {
            	sysMeshModuleDS.saveSysMeshModule(sysMeshModuleTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

	public void editSysMeshModule(SysMeshModuleTO sysMeshModuleTO)  {
        try {
        	sysMeshModuleDS.updateSysMeshModule(sysMeshModuleTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}