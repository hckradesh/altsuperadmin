package com.talentpact.business.dataservice;


import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.model.configuration.SysContentType;
import com.talentpact.model.organization.HrOrganization;


@Stateless
public class HrOrganisationDS extends AbstractDS<HrOrganization>
{

    public HrOrganisationDS()
    {
        super(HrOrganization.class);

    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrOrganization saveHrOrganization(TpOrganization tpOrganization, OrganizationrequestTO reqTO)
        throws Exception
    {
        HrOrganization hrOrganization = null;
        boolean alreadyExist = false;
        try {
            hrOrganization = getEntityManager("TalentPact").find(HrOrganization.class, tpOrganization.getOrganizationID().intValue());
            if (hrOrganization == null) {
                alreadyExist = false;
                hrOrganization = new HrOrganization();
                hrOrganization.setOrganizationId(tpOrganization.getOrganizationID().intValue());
                hrOrganization.setSectorCode(tpOrganization.getSectorCode());
                tpOrganization.setSectorID(tpOrganization.getSectorID());
                hrOrganization.setWelcomeMailSend(reqTO.isWelcomeMail());
                hrOrganization.setCreatedDate(new Date());
                hrOrganization.setTenantOrg(false);
                hrOrganization.setSysContentType(getEntityManager("TalentPact").find(SysContentType.class, reqTO.getSysContentTypeID()));
                hrOrganization.setAttendanceDistanceLimit(reqTO.getAttendanceDistanceLimit());
            } else {
                alreadyExist = true;
                hrOrganization.setSectorId(tpOrganization.getSectorID());
                hrOrganization.setWelcomeMailSend(reqTO.isWelcomeMail());
                hrOrganization.setModifiedDate(new Date());
            }
            hrOrganization.setHeadCount(tpOrganization.getEmployeeCount());
            hrOrganization.setOrganizationCode(tpOrganization.getOrganizationCode());
            hrOrganization.setOrganizationName(tpOrganization.getName());
            hrOrganization.setTenantID(tpOrganization.getTpTenantID().intValue());
            hrOrganization.setSysContentType(getEntityManager("TalentPact").find(SysContentType.class, reqTO.getSysContentTypeID()));
            hrOrganization.setAttendanceDistanceLimit(reqTO.getAttendanceDistanceLimit());
            hrOrganization.setTenantOrg(false);

            if (alreadyExist) {
                getEntityManager("TalentPact").merge(hrOrganization);
            } else {

                getEntityManager("TalentPact").persist(hrOrganization);
            }
            return hrOrganization;

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getAllOrganizationList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> orgObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct o.organizationId, o.organizationName, o.tenantID from HrOrganization o where o.tenantOrg=0 order by o.organizationName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            orgObjectList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return orgObjectList;
    }

    @SuppressWarnings("unchecked")
    public Boolean getHrOrganizationWelcomeMail(int orgId)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Boolean> orgObjectList = null;
        boolean flag = false;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select hrOrg.welcomeMailSend from HrOrganization hrOrg where hrOrg.organizationId= :orgId ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgId", orgId);
            orgObjectList = query.getResultList();
            if (orgObjectList != null && !orgObjectList.isEmpty())
                flag = orgObjectList.get(0);
        } catch (Exception e) {
            throw e;
        }
        return flag;
    }
}