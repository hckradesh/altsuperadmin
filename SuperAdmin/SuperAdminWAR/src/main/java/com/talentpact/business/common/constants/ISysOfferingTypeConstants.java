package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysOfferingTypeConstants
{

    public static String SYSOFFERINGTYPE_ADD_DIALOG_HIDE = "PF('sysOfferingTypeAddModal').hide();";

    public static String SYSOFFERINGTYPE_EDIT_DIALOG_HIDE = "PF('sysOfferingTypeEditModal').hide();";

    public static String SYSOFFERINGTYPE_SAVE_ERROR_MSG = "Error occured while saving SysOfferingType.";

    public static String SYSOFFERINGTYPE_UPDATE_ERROR_MSG = "Error occured while updating SysOfferingType.";

    public static String SYSOFFERINGTYPE_SAVE_SUCCESS_MSG = "SysOfferingType saved successfully.";

    public static String SYSOFFERINGTYPE_UPDATE_SUCCESS_MSG = "SysOfferingType updated successfully.";

    public static String SYSOFFERINGTYPE_DATATABLE_RESET = "PF('sysOfferingTypeDataTable').clearFilters();";

    public static String SYSOFFERINGTYPE_PAGINATION_DATATABLE_RESET = "PF('sysOfferingTypeDataTable').getPaginator().setPage(0);";

    public static String SYSOFFERINGTYPE_DUPLICATE_ERROR_MSG = "SysOfferingType already present in system.";
}
