/**
 * 
 */
package com.talentpact.business.authorization.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.authorization.exception.AuthorizationException;
import com.talentpact.business.authorization.service.impl.AuthorizationService;
import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.authorization.IAuthorizationRemote;

/**
 * @author radhamadhab.dalai
 * 
 */
@Stateless
public class AuthorizationFacade extends CommonFacade implements IAuthorizationRemote
{
    @EJB
    AuthorizationService authorizationService;

    public ServiceResponse authorizeUser(ServiceRequest serviceRequest)
        throws AuthorizationException
    {
        ServiceResponse response = null;
        try {
            response = authorizationService.authorizeUser(serviceRequest);
            return response;
        } catch (Exception e) {
            throw new AuthorizationException();
        }
    }

}
