/**
 * 
 */
package com.talentpact.business.tpOrganization.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.security.transport.output.UiFormTO;
import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.output.CheckDuplicateValueTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.hrApp.HrAppService;
import com.talentpact.business.tenants.service.impl.CreateTenantService;
import com.talentpact.business.tpOrganization.exception.OrganizationException;
import com.talentpact.business.tpOrganization.service.impl.OrganizationService;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.remote.tpOrganization.INewOrganizationFacadeRemote;

import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

/**
 * @author radhamadhab.dalai
 * 
 */

@Stateless
public class NewOrganizationFacade extends CommonFacade implements INewOrganizationFacadeRemote {
	@EJB
	CreateTenantService createTenantService;

	@EJB
	OrganizationService organizationService;

	@EJB
	HrAppService hrAppService;

	public ServiceResponse getAllTenants(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = createTenantService.getAllTenants(serviceRequest);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return serviceResponse;
	}

	public ServiceResponse getCompleteAppTree(ServiceRequest serviceRequest) throws OrganizationException {

		return organizationService.getCompleteAppTree(serviceRequest);

	}

	public ServiceResponse getAllApps(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getAllApps(serviceRequest);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return serviceResponse;
	}

	public ServiceResponse saveOrganization(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.saveOrganization(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;
	}

	public ServiceResponse getPortalTypebyApp(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getPortalsTypebyApp(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;
	}

	public ServiceResponse getTenant(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = createTenantService.getTenant(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;
	}

	public ServiceResponse getAllOrganizations(ServiceRequest serviceRequest) throws OrganizationException {

		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getAllOrganizations(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	public void saveSelectedPartnerApp(List<String> selectedPartnerAppList, Long orgId, Long tenantId) {
		try {
			organizationService.saveselectedPartnerApp(selectedPartnerAppList, orgId, tenantId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void saveSelectedCustomApp(List<String> selectedCustomAppList, Long orgId, Long tenantId) {
		try {
			organizationService.saveselectedCustomApp(selectedCustomAppList, orgId, tenantId);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public void saveSelectedApps(List<HrAppTO> createFinalAppList){
		try{
			organizationService.saveSelectedApps(createFinalAppList);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public List<HrAppTO> getSelectedPartnerApps(Long orgId, Long tenantID) {
		List<HrAppTO> responseTO = new ArrayList<>();
		try {
			responseTO = organizationService.getSelectedPartnerApps(orgId, tenantID);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseTO;
	}

	public List<HrAppTO> getSelectedCustomApps(Long orgId, Long tenantID) {
		List<HrAppTO> responseTO = new ArrayList<>();
		try {
			responseTO = organizationService.getSelectedCustomApps(orgId, tenantID);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseTO;
	}

	public ServiceResponse getCompleteMergedAppTreeForOrg(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getCompleteMergedAppTreeForOrg(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	/* code for retrieving modules for an organization */

	public ServiceResponse getAllModules(ServiceRequest serviceRequest) throws OrganizationException {
		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getAllModulesFromAppID(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	public ServiceResponse getOrgMoreInfoListByAppIdOrgIdTenantID(ServiceRequest serviceRequest)
			throws OrganizationException {

		ServiceResponse serviceResponse = null;
		try {

			serviceResponse = organizationService.getOrgMoreInfoListByAppIdOrgIdTenantID(serviceRequest);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;
	}

	public List<SysBundleResponseTO> getSysBundle() throws OrganizationException {
		try {
			return organizationService.getSysBundle();
		} catch (Exception ex) {
			throw new OrganizationException();
		}
	}

	@Override
	public List<Integer> getOrgBundle(ServiceRequest serviceRequest) throws OrganizationException {
		try {
			return organizationService.getOrgBundle(serviceRequest);
		} catch (Exception ex) {
			throw new OrganizationException();
		}
	}

	@Override
	public List<AddressResponseTO> getCountryDropDown() throws OrganizationException {

		List<AddressResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getCountryDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<AddressResponseTO> getStateDropDown(String country) throws OrganizationException {

		List<AddressResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getStateDropDown(country);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<AddressResponseTO> getDistrictDropDown(String state) throws OrganizationException {

		List<AddressResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getDistrictDropDown(state);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<AddressResponseTO> getCityDropDown(String district) throws OrganizationException {

		List<AddressResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getCityDropDown(district);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<AddressResponseTO> getZipCodeDropDown(Integer cityID) throws OrganizationException {

		List<AddressResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getZipCodeDropDown(cityID);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<SectorCostResponseTO> getSectorDropDown() throws OrganizationException {

		List<SectorCostResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getSectorDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<FinancialYearResponseTO> getFinancialYearList() throws OrganizationException {

		List<FinancialYearResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getFinancialYearList();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<FinancialYearResponseTO> getGovtFinancialYearList() throws OrganizationException {

		List<FinancialYearResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getGovtFinancialYearList();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	public void saveOrgWebPayStatus(String OrgCode, boolean status) throws OrganizationException {
		try {

			organizationService.saveOrgWebPayStatus(OrgCode, status);

		} catch (Exception ex) {
			throw new OrganizationException();
		}
	}

	@Override
	public List<SysSubModuleResponseTO> getModuleListDropDown() throws OrganizationException {

		List<SysSubModuleResponseTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getModuleListDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<SysContentTypeTO> getSysContentTypeDropDown() throws OrganizationException {

		List<SysContentTypeTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getSysContentTypeDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown() throws OrganizationException {

		List<SysConstantAndSysConstantCategoryTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getSysConstantCategoryDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown() throws OrganizationException {

		List<SysConstantAndSysConstantCategoryTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getSysConstantDropDown();

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;

	}

	@Override
	public List<UiFormTO> getLandingPageList(Long orgID) throws OrganizationException {
		// TODO Auto-generated method stub
		List<UiFormTO> serviceResponse = null;
		try {

			serviceResponse = organizationService.getLandingPageList(orgID);

		} catch (Exception ex) {
			throw new OrganizationException();
		}

		return serviceResponse;
	}
}
