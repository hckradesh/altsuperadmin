package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.to.OperatorTo;
import com.talentpact.adhocreports.ui.business.common.ReportList;
import com.talentpact.adhocreports.ui.business.to.ReportDataTo;
import com.talentpact.adhocreports.ui.business.to.IntermediateProcessingLogicTemplateTo;
import com.talentpact.business.adhocreportsui.controller.CreateReportDataController;

@SessionScoped
@Named("createReportDataBean")
public class CreateReportDataBean implements Serializable {

	private static final long serialVersionUID = 4995061597046359638L;
	
	@Inject
	CreateReportDataController createReportDataController;

	private ReportList<ReportDataTo> reportList;
	
	private ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode;
	
	private String selectedTableType;
	
	private String derivedTableNameForCreation;
	
	private String tableDescription;
	
	private IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo;
	
	private SelectItem selectedDataType;
	
	private SelectItem selectedDerivedTableModule;
	
	private String orgAdminFlag;
	
	private String defaultDerivedTableFlag;
	
	private Integer droppedColumnIndex;
	
	private List<Object[]> reportData;
	
	private List<String> reportHeaders;
	
	private List<String> previewReportHeaders;
	
	private boolean previewFlag;
	
	private boolean disableFilterFlag;
	
	private boolean addCssFlag;
	
	private List<OperatorTo> cssOperators;
	
	private String searchColumnRegex;
		
	@PostConstruct
	public void initialize() {
		createReportDataController.initialize(false);
	}
	
	public ReportList<ReportDataTo> getReportList() {
		return reportList;
	}

	public void setReportList(ReportList<ReportDataTo> reportList) {
		this.reportList = reportList;
	}

	public ReportList.ReportListNode<ReportDataTo> getCurrentPopulatedListNode() {
		return currentPopulatedListNode;
	}

	public void setCurrentPopulatedListNode(
			ReportList.ReportListNode<ReportDataTo> currentPopulatedListNode) {
		this.currentPopulatedListNode = currentPopulatedListNode;
	}

	public String getSelectedTableType() {
		return selectedTableType;
	}

	public void setSelectedTableType(String selectedTableType) {
		this.selectedTableType = selectedTableType;
	}

	public String getDerivedTableNameForCreation() {
		return derivedTableNameForCreation;
	}

	public void setDerivedTableNameForCreation(String derivedTableNameForCreation) {
		this.derivedTableNameForCreation = derivedTableNameForCreation;
	}

	public IntermediateProcessingLogicTemplateTo getIntermediateProcessingLogicTemplateTo() {
		return intermediateProcessingLogicTemplateTo;
	}

	public void setIntermediateProcessingLogicTemplateTo(
			IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo) {
		this.intermediateProcessingLogicTemplateTo = intermediateProcessingLogicTemplateTo;
	}

	public SelectItem getSelectedDataType() {
		return selectedDataType;
	}

	public void setSelectedDataType(SelectItem selectedDataType) {
		this.selectedDataType = selectedDataType;
	}

	public Integer getDroppedColumnIndex() {
		return droppedColumnIndex;
	}

	public void setDroppedColumnIndex(Integer droppedColumnIndex) {
		this.droppedColumnIndex = droppedColumnIndex;
	}

	public List<Object[]> getReportData() {
		return reportData;
	}

	public void setReportData(List<Object[]> reportData) {
		this.reportData = reportData;
	}

	public List<String> getReportHeaders() {
		return reportHeaders;
	}

	public void setReportHeaders(List<String> reportHeaders) {
		this.reportHeaders = reportHeaders;
	}

	public boolean isPreviewFlag() {
		return previewFlag;
	}

	public void setPreviewFlag(boolean previewFlag) {
		this.previewFlag = previewFlag;
	}

	public String getTableDescription() {
		return tableDescription;
	}

	public void setTableDescription(String tableDescription) {
		this.tableDescription = tableDescription;
	}

	public boolean isDisableFilterFlag() {
		return disableFilterFlag;
	}

	public void setDisableFilterFlag(boolean disableFilterFlag) {
		this.disableFilterFlag = disableFilterFlag;
	}

	public String getOrgAdminFlag() {
		return orgAdminFlag;
	}

	public void setOrgAdminFlag(String orgAdminFlag) {
		this.orgAdminFlag = orgAdminFlag;
	}

	public String getDefaultDerivedTableFlag() {
		return defaultDerivedTableFlag;
	}

	public void setDefaultDerivedTableFlag(String defaultDerivedTableFlag) {
		this.defaultDerivedTableFlag = defaultDerivedTableFlag;
	}

	public boolean isAddCssFlag() {
		return addCssFlag;
	}

	public void setAddCssFlag(boolean addCssFlag) {
		this.addCssFlag = addCssFlag;
	}

	public List<OperatorTo> getCssOperators() {
		return cssOperators;
	}

	public void setCssOperators(List<OperatorTo> cssOperators) {
		this.cssOperators = cssOperators;
	}

	public SelectItem getSelectedDerivedTableModule() {
		return selectedDerivedTableModule;
	}

	public void setSelectedDerivedTableModule(SelectItem selectedDerivedTableModule) {
		this.selectedDerivedTableModule = selectedDerivedTableModule;
	}
	
	public String getSearchColumnRegex() {
		return searchColumnRegex;
	}

	public void setSearchColumnRegex(String searchColumnRegex) {
		this.searchColumnRegex = searchColumnRegex;
	}

	public List<String> getPreviewReportHeaders() {
		return previewReportHeaders;
	}

	public void setPreviewReportHeaders(List<String> previewReportHeaders) {
		this.previewReportHeaders = previewReportHeaders;
	}

}
