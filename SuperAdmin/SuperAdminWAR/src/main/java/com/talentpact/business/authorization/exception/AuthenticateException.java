/**
 * 
 */
package com.talentpact.business.authorization.exception;

/**
 * @author radhamadhab.dalai
 *
 */
public class AuthenticateException extends Exception
{

    private String message;

    public AuthenticateException()
    {

    }

    public AuthenticateException(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
