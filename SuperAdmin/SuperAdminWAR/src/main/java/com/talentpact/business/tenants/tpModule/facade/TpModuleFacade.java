/**
 * 
 */
package com.talentpact.business.tenants.tpModule.facade;

import javax.ejb.Stateless;

import com.talentpact.business.common.facade.impl.CommonFacade;

/**
 * @author radhamadhab.dalai
 *
 */
@Stateless
public class TpModuleFacade extends CommonFacade
{

}
