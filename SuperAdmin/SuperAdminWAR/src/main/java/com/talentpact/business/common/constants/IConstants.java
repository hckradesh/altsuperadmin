package com.talentpact.business.common.constants;

/**
 * @author radhamadhab.dalai
 *
 */

public interface IConstants
{

    public static final String TALENTPACT_FORMENGINE_DEFAULT_DATABASE_NAME = "";

    public static final String ORGANIZATION_ID_PARAMETER_NAME = "organizationID";

    public static final String USER_ID_PARAMETER_NAME = "";

    public static final String ROLES_PARAMETER_NAME = "";

    public static final String PARENT_MENU_ID_PARAMETER_NAME = "parentMenuID";

    public static final String FORM_ID_PARAMETER_NAME = "";

    public static final String Login_Page = "LoginPage";

    public static final String Home_Page = "HomePage";

    public static final String Login_Page_Theme = "LoginPageTheme";

    public static final String Default_Theme = "DefaultTheme";

    public static final String DATABASE_METADATA_START_KEY = "DATABASE_";

    public static final String UNDERSCORE = "_";

    public static final String ORGANIZE_CODE = "AORG";

    public static final String PROC_COMMAND = "exec ";

    public static final String BASE_TABLE = "baseTable";

    public static final String TALENTPACT = "TalentPact";

    public static final String Left_Logo = "LeftLogo";

    public static final String Right_Logo = "RightLogo";

    public static final String Footer = "Footer";

    public static final String UsernameTxt = "UsernameTxt";

    public static final String PasswordTxt = "PasswordTxt";

    public static final String BtnTxt = "BtnTxt";

    public static final String Banner = "Banner";

    public static final String SamlHeader = "SamlHeader";

    public static final String SamlBtnTxt = "SamlBtnTxt";

    public static final String HeadingTxt = "HeadingTxt";
    
    public static final String MobileStatus = "MobileStatus";
    
    public static final String BackGroundImage = "BackGroundImage";


}
