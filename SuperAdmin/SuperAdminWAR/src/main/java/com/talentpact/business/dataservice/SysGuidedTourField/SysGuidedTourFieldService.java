/**
 * 
 */
package com.talentpact.business.dataservice.SysGuidedTourField;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysGuidedTourFieldResponseTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.SysConstant.SysConstantService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysGuidedTourFieldService extends CommonService
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantService.class);

    @EJB
    SysGuidedTourFieldDS sysGuidedTourFieldDS;

    public List<SysGuidedTourFieldResponseTO> getSysGuidedTourFieldResponseTOList()
        throws Exception
    {
        List<SysGuidedTourFieldResponseTO> list = null;


        try {
            list = sysGuidedTourFieldDS.getSysGuidedTourFieldTOList();
            return list;
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
    }

    public List<SysGuidedTourFieldResponseTO> getUiFormDropDown()
        throws Exception
    {
        List<SysGuidedTourFieldResponseTO> list = null;


        try {
            list = sysGuidedTourFieldDS.getUiFormDropDown();
            return list;
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
    }

    public void save(SysGuidedTourFieldResponseTO to)
        throws Exception
    {
        try {
            sysGuidedTourFieldDS.save(to);

        } catch (Exception e) {
            LOGGER_.info(e.getMessage());

        }

    }


}
