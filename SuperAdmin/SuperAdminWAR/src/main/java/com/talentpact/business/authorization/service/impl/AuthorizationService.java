/**
 * 
 */
package com.talentpact.business.authorization.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.authorization.converter.AuthorizationConverter;
import com.talentpact.business.authorization.converter.PassWordEncryptionHelper;
import com.talentpact.business.authorization.exception.AuthorizationException;
import com.talentpact.business.authorization.transport.input.AuthRequestTO;
import com.talentpact.business.authorization.transport.output.AuthResponseTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.TpUserDS;
import com.talentpact.ui.login.transport.input.AuthUserTO;

/**
 * @author radhamadhab.dalai
 * 
 */
@Stateless
public class AuthorizationService extends CommonService
{
    @EJB
    TpUserDS tpUserDS;

    @EJB
    AuthorizationConverter authorizationConverter;


    public ServiceResponse authorizeUser(ServiceRequest serviceRequest)
        throws AuthorizationException
    {
        ServiceResponse response = null;
        AuthRequestTO authRequestTO = null;

        AuthResponseTO authResponseTO = null;

        String userName = null;
        String password = null;
        String url = null;
        String appName = null;
        String message = null;
        TpUser tpUser = null;
        String tenantCode = null;
        String orgCode = null;
        Object[] userObj = null;
        List<TpUser> result = null;
        AuthUserTO tpUserTO = null;

        try {
            authRequestTO = (AuthRequestTO) serviceRequest.getRequestTansportObject();

            userName = authRequestTO.getUserName();
            password = PassWordEncryptionHelper.convetToHash(authRequestTO.getPassword());
            url = authRequestTO.getUrlName();
            appName = authRequestTO.getAppName();
            tenantCode = authRequestTO.getTenantCode();
            orgCode = authRequestTO.getOrganizationCode();


            result = tpUserDS.validateUser(userName, password, url, appName, tenantCode, orgCode);

            if (result != null && result.size() > 0) {

                tpUser = (TpUser) result.get(0);
                tpUserTO = authorizationConverter.convertFromTPUser(tpUser);

                message = "Successfull";
            }


            else {
                message = "login unsuccessfull";
            }
            authResponseTO = new AuthResponseTO();
            authResponseTO.setConfirmationMessage(message);

            //convert it to TO
            authResponseTO.setTpUserTO(tpUserTO);

            response = new ServiceResponse();

            response.setResponseTransportObject(authResponseTO);
            return response;
        } catch (Exception e) {
            throw new AuthorizationException(e.getMessage());
        }
    }
}
