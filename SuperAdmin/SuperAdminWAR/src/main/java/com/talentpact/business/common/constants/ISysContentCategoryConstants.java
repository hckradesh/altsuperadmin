package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysContentCategoryConstants
{

    public static String SYSCONTENTCATEGORY_ADD_DIALOG_HIDE = "PF('sysContentCategoryAddModal').hide();";

    public static String SYSCONTENTCATEGORY_EDIT_DIALOG_HIDE = "PF('sysContentCategoryEditModal').hide();";

    public static String SYSCONTENTCATEGORY_SAVE_ERROR_MSG = "Error occured while saving SysContentCategory.";

    public static String SYSCONTENTCATEGORY_UPDATE_ERROR_MSG = "Error occured while updating SysContentCategory.";

    public static String SYSCONTENTCATEGORY_SAVE_SUCCESS_MSG = "SysContentCategory saved successfully.";

    public static String SYSCONTENTCATEGORY_UPDATE_SUCCESS_MSG = "SysContentCategory updated successfully.";

    public static String SYSCONTENTCATEGORY_DATATABLE_RESET = "PF('sysContentCategoryDataTable').clearFilters()";

    public static String SYSCONTENTCATEGORY_PAGINATION_DATATABLE_RESET = "PF('sysContentCategoryDataTable').getPaginator().setPage(0);";

    public static String SYSCONTENTCATEGORY_DUPLICATE_ERROR_MSG = "SysContentCategory already present in system.";
}
