/**
 * 
 */
package com.talentpact.business.common.dataservice;

import javax.persistence.EntityManager;

public interface IDataSerivce
{

    public EntityManager getEntityManager();

    public EntityManager getEntityManager(String unitName);

}
