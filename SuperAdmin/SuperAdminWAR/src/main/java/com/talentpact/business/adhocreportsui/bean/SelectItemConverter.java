package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.common.controller.SelectItem;

@FacesConverter(value = "selectItemConverter", forClass = SelectItem.class)
public class SelectItemConverter implements Converter, Serializable {

	private static final long serialVersionUID = 8934497202003167262L;
	private static final Logger LOGGER = Logger.getLogger(SelectItemConverter.class);
	private static final String LABEL_CONSTANT = "label:";
	private static final String VALUE_CONSTANT = ",value:";
	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && !value.equals("")) {
            SelectItem item = new SelectItem();
            try {
                if (value.contains(LABEL_CONSTANT))
                    item.setLabel(value.substring(value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length(), 
                    		value.indexOf(VALUE_CONSTANT, value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length())));
                if (value.contains(VALUE_CONSTANT))
                    item.setValue(value.substring(value.indexOf(VALUE_CONSTANT) + VALUE_CONSTANT.length(), value.length()));
                return item;
            } catch (Exception e) {
            	LOGGER.error("Select Item Converter for value : " + value + " error.",e);
                return null;
            }

        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof SelectItem) {
            return LABEL_CONSTANT + ((SelectItem) value).getLabel() + VALUE_CONSTANT + ((SelectItem) value).getValue();
        } else
            return "";
    }

}
