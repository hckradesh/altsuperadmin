/**
 * 
 */
package com.talentpact.business.common.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.talentpact.business.application.transport.output.SysRoleTypeTO;

import com.talentpact.business.application.transport.output.SysMenuTO;

/**
 * @author vikas.singh
 * 
 */
public class CacheUtil {
	private static final Logger _LOGGER = Logger.getLogger(CacheUtil.class);
	private  static Map<String, String> databaseMetadata;
	private static Map<String , SysRoleTypeTO> sysRoleTypeToMap;
	private static List<SysMenuTO> sysMenuToList;



	public static void setDatabaseMetadata(Map<String, String> databaseMetadata) {
		CacheUtil.databaseMetadata = databaseMetadata;
	}


	public static void setSysRoleTypeToMap( Map<String, SysRoleTypeTO> sysRoleTypeToMap) {
		CacheUtil.sysRoleTypeToMap = sysRoleTypeToMap;
	}


	public static String getDatabaseByPersistanceKey(String key) throws Exception {
		try {
			return databaseMetadata.get(key);

		} catch (Exception e) {
			throw new Exception(e);
		}
	}


	public static Map<String, SysRoleTypeTO> getSysRoleTypeToMap() {
		return sysRoleTypeToMap;
	}



	public static List<SysMenuTO> getSysMenuToList() {
		return sysMenuToList;
	}

    public static List<String> getDatabaseMetadataInList()
    {
        List<String> tmpDbMetadata = new ArrayList<String>(databaseMetadata.values());
        Set<String> tmp = new HashSet<String>();
        tmp.addAll(tmpDbMetadata);
        tmpDbMetadata.clear();
        tmpDbMetadata.addAll(tmp);
        return tmpDbMetadata;
    }
     	



	public static void setSysMenuToList(List<SysMenuTO> sysMenuToList) {
		CacheUtil.sysMenuToList = sysMenuToList;
	}

	
}


