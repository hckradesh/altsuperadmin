/**
 * 
 */
package com.talentpact.business.dataservice.sysPolicy;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.group.SysPolicy;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysPolicyDS extends AbstractDS<SysPolicy>
{
    public SysPolicyDS()
    {
        super(SysPolicy.class);
    }

    public List<Object[]> getSysPolicyList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ");
            hqlQuery.append(" sp.sysSubModule.moduleId, sp.sysSubModule.moduleName, ");
            hqlQuery.append(" sp.sysEntity.entityId,sp.sysEntity.name, sp.policyCode, ");
            hqlQuery.append(" sp.policyName, sp.policyLabel, sp.sysPolicyID,sp.policyTypeID,sysContentType.sysType ");
            hqlQuery.append("  from SysPolicy sp  ");
            hqlQuery.append(" left join sp.sysContentType sysContentType");
            hqlQuery.append(" order by  ");
            hqlQuery.append(" sp.sysSubModule.moduleName, sp.sysEntity.name ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getNewSysModuleList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct sm.moduleId, sm.moduleName from SysSubModule sm where sm.parentModuleId is null order by sm.moduleName ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public List<Object[]> getNewSysEntityList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct se.entityId, se.name from SysEntity se order by se.name ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean saveSysPolicy(SysPolicyTO sysPolicyTO)
    {
        SysPolicy sysPolicy = new SysPolicy();
        try {
            sysPolicy.setModuleID(sysPolicyTO.getModuleID());
            sysPolicy.setEntityID(sysPolicyTO.getEntityID());
            sysPolicy.setPolicyCode(sysPolicyTO.getPolicyCode());
            sysPolicy.setPolicyLabel(sysPolicyTO.getPolicyLabel());
            sysPolicy.setPolicyName(sysPolicyTO.getPolicyName());
            sysPolicy.setTenantID(1);
            sysPolicy.setCreatedBy(1);
            sysPolicy.setModifiedBy(1);
            sysPolicy.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysPolicy.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            sysPolicy.setPolicyTypeID(sysPolicyTO.getPolicyTypeID());
            getEntityManager("TalentPact").persist(sysPolicy);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean updateSysPolicy(SysPolicyTO sysPolicyTO)
    {
        SysPolicy sysPolicy = null;
        try {
            sysPolicy = new SysPolicy();
            sysPolicy = findSysPolicy(sysPolicyTO);
            sysPolicy.setModuleID(sysPolicyTO.getModuleID());
            sysPolicy.setEntityID(sysPolicyTO.getEntityID());
            sysPolicy.setPolicyCode(sysPolicyTO.getPolicyCode());
            sysPolicy.setPolicyName(sysPolicyTO.getPolicyName());
            sysPolicy.setPolicyLabel(sysPolicyTO.getPolicyLabel());
            sysPolicy.setModifiedBy(1);
            sysPolicy.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            sysPolicy.setPolicyTypeID(sysPolicyTO.getPolicyTypeID());
            getEntityManager("TalentPact").merge(sysPolicy);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysPolicy findSysPolicy(SysPolicyTO sysPolicyTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPolicy> sysPolicyList = null;
        SysPolicy result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sp from SysPolicy sp where sp.sysPolicyID=:sysPolicyID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("sysPolicyID", sysPolicyTO.getSysPolicyID());
            sysPolicyList = query.getResultList();
            if (sysPolicyList != null && sysPolicyList.size() > 0) {
                result = sysPolicyList.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
}