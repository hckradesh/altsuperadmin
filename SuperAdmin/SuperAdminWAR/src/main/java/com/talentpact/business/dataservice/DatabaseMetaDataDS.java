/**
 * 
 */
package com.talentpact.business.dataservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.auth.entity.TpDatabaseMetadata;
import com.talentpact.business.tenants.exception.TenantException;

/**
 * @author vikas.singh
 * 
 */
@Stateless
public class DatabaseMetaDataDS extends AbstractDS<TpDatabaseMetadata> {

	public DatabaseMetaDataDS() {
		  super(TpDatabaseMetadata.class);
	}

	public List<Object[]> getAllDatabaseByName()   throws TenantException{
		    StringBuilder builder =null ;
		    Query query = null;
		    List<Object[]> resultList =null;
		    try{
		    	builder = new StringBuilder() ;
		    	builder.append("select metadata.persistenceUnitName , metadata.tpApp.code , metadata.tenantID.tenantID from  TpDatabaseMetadata metadata ") ;
		    	query = getEntityManager("TalentPactAuth").createQuery(builder.toString()) ;
		    	resultList= query.getResultList();
		    	return resultList ;
		    }
		    catch(Exception  e){
		    	throw new TenantException("Exception occured while retrieving database metadata");
		    }
		    finally{
			    builder =null ;
			    query = null;
		    }
		 
	 }

}
