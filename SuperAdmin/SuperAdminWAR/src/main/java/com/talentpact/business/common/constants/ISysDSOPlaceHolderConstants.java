package com.talentpact.business.common.constants;

public class ISysDSOPlaceHolderConstants {
	public static String DSOPLACEHOLDER_ADD_DIALOG_HIDE = "PF('dsoPlaceHolderAddModal').hide();";

    public static String DSOPLACEHOLDER_EDIT_DIALOG_HIDE = "PF('dsoPlaceHolderEditModal').hide();";

    public static String DSOPLACEHOLDER_SAVE_ERROR_MSG = "Error occured while saving DSOPlaceHolder.";

    public static String DSOPLACEHOLDER_UPDATE_ERROR_MSG = "Error occured while updating DSOPlaceHolder.";

    public static String DSOPLACEHOLDER_SAVE_SUCCESS_MSG = "DSOPlaceHolder saved successfully.";

    public static String DSOPLACEHOLDER_UPDATE_SUCCESS_MSG = "DSOPlaceHolder updated successfully.";

    public static String DSOPLACEHOLDER_DATATABLE_RESET = "PF('dsoPlaceHolderDataTable').clearFilters();";

    public static String DSOPLACEHOLDER_PAGINATION_DATATABLE_RESET = "PF('dsoPlaceHolderDataTable').getPaginator().setPage(0);";

    public static String DSOPLACEHOLDER_ERROR_MSG = "DSOPlaceHolder already present in system.";

}
