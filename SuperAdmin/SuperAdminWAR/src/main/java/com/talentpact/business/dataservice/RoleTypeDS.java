package com.talentpact.business.dataservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.model.SysRole;
import com.talentpact.model.SysRoleType;

@Stateless
public class RoleTypeDS extends AbstractDS<SysRole> {

    public RoleTypeDS() {
        super(SysRole.class);
    }

    public List<Object[]> getAllSysRoleTypeMap()
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;

        try {
            queryStr = new StringBuilder();
            queryStr.append(" select sysroletype.roleTypeId,sysroletype.roleType,sysroletype.ruleID,sysroletype.description,sysroletype.defaultName from SysRoleType sysroletype ");
            queryStr.append(" where sysroletype.isActive=1 ");
            query = getEntityManager("AltCommon").createNativeQuery(queryStr.toString());
            return query.getResultList();

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            queryStr = null;
            query = null;

        }
    }

    public Map<String, Integer> getSysRoleTypeFromTalentPact_AdminPortal()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysRoleType> sysRoleType = null;
        List<Object[]> objectList = null;
        Map<String, Integer> sysRoleTypeMap = null;
        try {

            //db query --start here
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysRoleType.roleType , sysRoleType.roleTypeId from SysRoleType sysRoleType");
            query = getEntityManager("TalentPact").createNativeQuery(hqlQuery.toString());
            objectList = query.getResultList();
            //db-query end here

            //constructin map for later use --start  here
            sysRoleTypeMap = new HashMap<String, Integer>();
            for (Object[] object : objectList) {
                sysRoleTypeMap.put((String) object[0], (Integer) object[1]);
            }
            //construction map --end here 
            return sysRoleTypeMap;

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            query = null;
            hqlQuery = null;
            sysRoleType = null;
            sysRoleTypeMap = null;

        }
    }


}
