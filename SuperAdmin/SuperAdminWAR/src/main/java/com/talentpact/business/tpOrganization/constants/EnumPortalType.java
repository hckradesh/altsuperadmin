package com.talentpact.business.tpOrganization.constants;

public enum EnumPortalType {

    Employee_Portal("Employee Portal", 1l), Onboarding_Portal("Onboarding Portal", 2l), Vendor_Portal("Vendor Portal", 3l), Candidate_Portal("Candidate Portal", 4l), Recruiter_Portal(
            "Recruiter Portal", 5l), Insights_Portal("Insights Portal", 6l), Admin_Portal("Admin Portal", 7l);

    private Long portalId;

    private String portalName;

    EnumPortalType(String portalName, Long portalId)
    {
        this.portalId = portalId;
        this.portalName = portalName;
    }


    public static EnumPortalType getPortalId(String portalname)
    {
        EnumPortalType portalEnum = null;
        if (portalname == null) {
            throw new NullPointerException("Null Portal Name Passed in Enum ");
        }
        for (EnumPortalType enumObj : EnumPortalType.values()) {
            if (enumObj.getPortalName().equalsIgnoreCase(portalname.trim())) {
                portalEnum = enumObj;
            }
        }
        return portalEnum;
    }


    public Long getPortalId()
    {
        return portalId;
    }

    public void setPortalId(Long portalId)
    {
        this.portalId = portalId;
    }

    public String getPortalName()
    {
        return portalName;
    }

    public void setPortalName(String portalName)
    {
        this.portalName = portalName;
    }


}
