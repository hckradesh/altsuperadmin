package com.talentpact.business.dataservice.SysWorkflowType;

import javax.ejb.Stateless;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysWorkflowStageTypeGroup;

@Stateless
public class SysWorkflowStageTypeGroupDS extends AbstractDS<SysWorkflowStageTypeGroup> {

    public SysWorkflowStageTypeGroupDS() {
        super(SysWorkflowStageTypeGroup.class);
    }

}
