package com.talentpact.business.common.constants;

public class ISysChatBotValidationTypeConstants {
	
	public static String SYSCHATBOTVALIDATIONTYPE_ADD_DIALOG_HIDE = "PF('sysChatBotValidationTypeAddModal').hide();";

    public static String SYSCHATBOTVALIDATIONTYPE_EDIT_DIALOG_HIDE = "PF('sysChatBotValidationTypeEditModal').hide();";

    public static String SYSCHATBOTVALIDATIONTYPE_SAVE_ERROR_MSG = "Error occured while saving SysChatBotValidationType.";

    public static String SYSCHATBOTVALIDATIONTYPE_UPDATE_ERROR_MSG = "Error occured while updating SysChatBotValidationType.";

    public static String SYSCHATBOTVALIDATIONTYPE_SAVE_SUCCESS_MSG = "SysChatBotValidationType saved successfully.";

    public static String SYSCHATBOTVALIDATIONTYPE_UPDATE_SUCCESS_MSG = "SysChatBotValidationType updated successfully.";

    public static String SYSCHATBOTVALIDATIONTYPE_DATATABLE_RESET = "PF('sysChatBotValidationTypeDataTable').clearFilters();";

    public static String SYSCHATBOTVALIDATIONTYPE_PAGINATION_DATATABLE_RESET = "PF('sysChatBotValidationTypeDataTable').getPaginator().setPage(0);";

    public static String SYSCHATBOTVALIDATIONTYPE_ERROR_MSG = "SysChatBotValidationType already present in system.";

}
