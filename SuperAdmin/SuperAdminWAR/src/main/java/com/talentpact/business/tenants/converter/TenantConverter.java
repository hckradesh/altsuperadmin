/**
 * 
 */
package com.talentpact.business.tenants.converter;

import java.util.ArrayList;
import java.util.List;

import com.talentpact.auth.entity.TpTenant;
import com.talentpact.business.tenants.transport.newtenant.input.NewTenantRequestTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;

/**
 * @author radhamadhab.dalai
 *
 */
public class TenantConverter
{

    public static List<NewTenantResponseTO> convertTenantEntityToDataTO(List<TpTenant> tpTenantList)
    {
        List<NewTenantResponseTO> resultList = null;
        NewTenantResponseTO newTenantResTO = null;
        try {

            resultList = new ArrayList<NewTenantResponseTO>();

            for (TpTenant tpTenant : tpTenantList) {

                newTenantResTO = new NewTenantResponseTO();
                newTenantResTO.setTenantName(tpTenant.getName());
                newTenantResTO.setTenantCode(tpTenant.getTenantCode());
                newTenantResTO.setTenantID(tpTenant.getTenantID());
                newTenantResTO.setCreatedDate(tpTenant.getCreationDate());
                newTenantResTO.setContractSignedDate(tpTenant.getContractSignedDate());
                newTenantResTO.setEffectiveFrom(tpTenant.getEffectiveFrom());
                newTenantResTO.setEffectiveTo(tpTenant.getEffectiveTo());
                resultList.add(newTenantResTO);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }


    public NewTenantRequestTO convert(TpTenant tpTenant)
    {

        NewTenantRequestTO newTenantTO = null;
        try {
            newTenantTO = new NewTenantRequestTO();
            newTenantTO.setTenantName(tpTenant.getName());
            newTenantTO.setTenantCode(tpTenant.getTenantCode());
            newTenantTO.setTenantID(tpTenant.getTenantID());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return newTenantTO;

    }

}
