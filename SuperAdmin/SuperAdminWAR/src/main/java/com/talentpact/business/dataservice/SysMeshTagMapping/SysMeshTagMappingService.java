/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshTagMapping;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagMappingTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysMeshModule;


@Stateless
public class SysMeshTagMappingService extends CommonService
{

    @EJB
    SysMeshTagMappingDS sysMeshTagMappingDS;

    public List<SysMeshTagMappingTO> getSysMeshTagMappingList()
        throws Exception
    {
        List<SysMeshTagMappingTO> list = null;
        SysMeshTagMappingTO sysMeshTagMappingTO = null;
        List<Object[]> sysMeshTagMappingList = null;

        try {
        	sysMeshTagMappingList = sysMeshTagMappingDS.getSysMeshTagMappingList();
            list = new ArrayList<SysMeshTagMappingTO>();
            for (Object[] sysMeshTagMapping : sysMeshTagMappingList) {
            	sysMeshTagMappingTO = new SysMeshTagMappingTO();
            	sysMeshTagMappingTO.setSysMeshTagMappingID((Integer) sysMeshTagMapping[0]);
                if (sysMeshTagMapping[1] != null)
                	sysMeshTagMappingTO.setSysMeshTagType((String) sysMeshTagMapping[1]);
                if (sysMeshTagMapping[2] != null)
                	sysMeshTagMappingTO.setSysMeshTagModuleName((String) sysMeshTagMapping[2]);
                if (sysMeshTagMapping[3] != null)
                	sysMeshTagMappingTO.setSysMeshModuleID((Integer) sysMeshTagMapping[3]);
                list.add(sysMeshTagMappingTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO)
        throws Exception
    {
        try {
            sysMeshTagMappingDS.addSysMeshTagMapping(sysMeshTagMappingTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO)
        throws Exception
    {
        try {
        	sysMeshTagMappingDS.editSysMeshTagMapping(sysMeshTagMappingTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public List<SysMeshModuleTO> getSysMeshModules() 
    	throws Exception
    {
    	List<Object[]> result = null;
    	List<SysMeshModuleTO> modules = null; 
    	SysMeshModuleTO sysMeshModuleTO = null;
    	try {
    		result = sysMeshTagMappingDS.getSysMeshModules();
    		if(result.size() > 0) {
    			modules = new ArrayList<>();
    		}
    		for(int i = 0 ; i < result.size() ; i++) {
    			sysMeshModuleTO = new SysMeshModuleTO();
    			sysMeshModuleTO.setSysMeshModuleID((Integer)result.get(i)[0]);
    			sysMeshModuleTO.setSysMeshModuleName((String) result.get(i)[1]);
    			modules.add(sysMeshModuleTO);
    		}
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
		return modules;
    }
    
    public List<String> getSysMeshModuleNames()
    	throws Exception
    {
    	List<String> moduleNames = null;
    	try {
    		moduleNames = sysMeshTagMappingDS.getSysMeshModuleNames();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return moduleNames;
    }

	public String checkExist(Integer sysMeshModuleID, String sysMeshTagType) {
		String flag = null;
		try {
    		flag = sysMeshTagMappingDS.checkExist(sysMeshModuleID, sysMeshTagType);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
		return flag;
	}
}