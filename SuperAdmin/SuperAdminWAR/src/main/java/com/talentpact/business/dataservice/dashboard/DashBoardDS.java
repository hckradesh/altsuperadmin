/**
 * 
 */
package com.talentpact.business.dataservice.dashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.application.transport.input.AltBenefitRequestTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.AppModuleDS;
import com.talentpact.business.model.AltCheckList;
import com.talentpact.model.HrModule;
import com.talentpact.model.dashboard.HrDashBoardItem;
import com.talentpact.model.dashboard.SysDashBoard;
import com.talentpact.model.organization.SysSector;
import com.talentpact.ui.dashboard.transport.input.CopyDashBoardReqTO;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class DashBoardDS extends AbstractDS<SysSector>
{
    public DashBoardDS()
    {
        super(SysSector.class);
    }

    @EJB
    AppModuleDS appModuleDS;
    
    @SuppressWarnings("unchecked")
    public List<Object[]> getSysSectorList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> sysSectorObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct s.SectorID,s.SectorLabel,s.SectorCode from SysSector s");
          //  hqlQuery.append("  join talentpactauth..tp_organization o");
          //  hqlQuery.append(" on  o.SectorCode=s.SectorCode ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            sysSectorObjectList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return sysSectorObjectList;
    }


	public List<DashBoardTO> getSysDashBoard()
	throws Exception
	{
		    List<DashBoardTO> dashBoardTOlist = null ;
		    Query query = null;
	        StringBuilder hqlQuery = null;
	        List<SysDashBoard> sdList = null;
	        try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select sd from SysDashBoard sd ");
	            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
	            sdList = query.getResultList();
	            dashBoardTOlist = new ArrayList<DashBoardTO>();
	            if(sdList!=null){
	            	for(SysDashBoard sd:sdList){
	            		 DashBoardTO dbTO = new DashBoardTO();
	            		 dbTO.setColumnPosition(Integer.parseInt(sd.getColumnPosition()));
	            		 dbTO.setDashboardItemName(sd.getDashboardItemName());
	            		 dbTO.setDashboardItemURL(sd.getDashboardItemURL());
	            		 dbTO.setStatus(sd.getIsActive());
	            		 dbTO.setModuleID(sd.getModuleID());
	            		 dbTO.setRowPosition(sd.getRowPosition());
	            		 dbTO.setSysDashboardItemID(sd.getSysDashboardItemID()); 
	            		 dbTO.setDashBoardItemCode(sd.getDashboardItemCode());
	            		 dashBoardTOlist.add(dbTO);
	            	}
	            }
	        } catch (Exception e) {
	            throw e;
	        }
	        return dashBoardTOlist;
	}
	
	public void saveSysDashBoard(DashBoardTO dashBoardTO)
	throws Exception
	{
		
		try{
			  if(dashBoardTO.getSysDashboardItemID()==null){
				  SysDashBoard sdItem = new SysDashBoard();
				  sdItem.setColumnPosition(new Integer(dashBoardTO.getColumnPosition()).toString());
				  sdItem.setDashboardItemName(dashBoardTO.getDashboardItemName());
				  sdItem.setDashboardItemURL(dashBoardTO.getDashboardItemURL());
				  sdItem.setIsActive(dashBoardTO.isStatus());
				  String dashboardCode =dashBoardTO.getDashBoardItemCode();
				  if(dashboardCode==null || dashboardCode.trim().equals("")){
					  sdItem.setDashboardItemCode(null);  
				  }else{
					  sdItem.setDashboardItemCode(dashBoardTO.getDashBoardItemCode());
				  }
				  if(dashBoardTO.getModuleID()==0||dashBoardTO.getModuleID()==null)
				  {
				    sdItem.setModuleID(dashBoardTO.getParentModuleID());
				  }else{
					  sdItem.setModuleID(dashBoardTO.getModuleID());
				  }
				  sdItem.setRowPosition(dashBoardTO.getRowPosition());
				  sdItem.setCreatedBy(1);
				  sdItem.setModifiedBy(1);
				  sdItem.setCreatedDate(new Date());
				  sdItem.setModifiedDate(new Date());
				  getEntityManager("Talentpact").persist(sdItem);
				  
			  }else{
				  SysDashBoard sdItem = getEntityManager("Talentpact").find(SysDashBoard.class, dashBoardTO.getSysDashboardItemID());
				  sdItem.setColumnPosition(new Integer(dashBoardTO.getColumnPosition()).toString());
				  sdItem.setDashboardItemName(dashBoardTO.getDashboardItemName());
				  sdItem.setDashboardItemURL(dashBoardTO.getDashboardItemURL());
				  String dashboardCode =dashBoardTO.getDashBoardItemCode();
				  if(dashboardCode==null || dashboardCode.trim().equals("")){
					  sdItem.setDashboardItemCode(null);  
				  }else{
					  sdItem.setDashboardItemCode(dashBoardTO.getDashBoardItemCode());
				  }
				  sdItem.setIsActive(dashBoardTO.isStatus());
				  if(dashBoardTO.getModuleID()==0||dashBoardTO.getModuleID()==null)
				  {
				    sdItem.setModuleID(dashBoardTO.getParentModuleID());
				  }else{
					  sdItem.setModuleID(dashBoardTO.getModuleID());
				  }
				  sdItem.setModifiedDate(new Date());
				  getEntityManager("Talentpact").merge(sdItem);
			  }
		} catch (Exception e) {
			throw e;
		}
		
	}


	public List<DashBoardTO> getDashBoardsForOrg(ServiceRequest serviceRequest)
	throws Exception
	{
		AltBenefitRequestTO abRequestTO = (AltBenefitRequestTO) serviceRequest.getRequestTansportObject();
		List<Integer> subMuduleIDList = null ;
		List<DashBoardTO> dashBoardTOlist = null ;
	    Query query = null;
        StringBuilder hqlQuery = null;
        List<SysDashBoard> sdList = null;
        
		try 
		{
			 List<HrModule> moduleListForOrg = appModuleDS.getAltModule(abRequestTO.getOrganizationID().intValue(), abRequestTO.getTenantID().intValue());
			 //making list of ids
			 subMuduleIDList = new ArrayList<Integer>();
			 for(HrModule hrm :moduleListForOrg){
				 subMuduleIDList.add(hrm.getParentSysModule().getModuleId());
			 }
			  hqlQuery = new StringBuilder();
	          hqlQuery.append(" select sd from SysDashBoard sd where sd.moduleID in (:moduleList) and sd.isActive=1 ");
	          query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
	          query.setParameter("moduleList", subMuduleIDList);
	          sdList = query.getResultList();
	          dashBoardTOlist = new ArrayList<DashBoardTO>();
	            if(sdList!=null){
	            	for(SysDashBoard sd:sdList){
	            		 HrDashBoardItem hrItem = findHrDashBoard(sd.getSysDashboardItemID(), abRequestTO.getOrganizationID().intValue(), abRequestTO.getTenantID().intValue());
	            		 DashBoardTO dbTO = new DashBoardTO();
	            		 dbTO.setColumnPosition(Integer.parseInt(sd.getColumnPosition()));
	            		 dbTO.setDashboardItemName(sd.getDashboardItemName());
	            		  if (hrItem == null) {
	            			  dbTO.setStatus(false);
	            			  dbTO.setHrDashBoardID(null);
	                      } else {

	                    	  dbTO.setStatus(hrItem.getIsActive());
	                    	  dbTO.setHrDashBoardID(hrItem.getHrDashboardItemID());
	                      }
	            	//	 dbTO.setStatus(false);
	            		 dbTO.setModuleID(sd.getModuleID());
	            		 dbTO.setRowPosition(sd.getRowPosition());
	            		 dbTO.setSysDashboardItemID(sd.getSysDashboardItemID()); 
	            		 dashBoardTOlist.add(dbTO);
	            	}
	            }
			  return dashBoardTOlist;
		} catch (Exception e) {
			throw e;
		}
	}


	private HrDashBoardItem findHrDashBoard(Integer sysDashboardItemID,
			int orgID, int tenantID) throws Exception
	{
		    Query query = null;
	        StringBuilder hqlQuery = null;
	        List<HrDashBoardItem> hrDashBoardItemList = null;
	        HrDashBoardItem result = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select h from HrDashBoardItem h  where h.sysDashboardItemID=:sID and h.organizationID=:OrgID and h.tenantID=:TenantID ");

	            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
	            query.setParameter("sID", sysDashboardItemID);
	            query.setParameter("OrgID", orgID);
	            query.setParameter("TenantID", tenantID);
	            hrDashBoardItemList = query.getResultList();
	            if (hrDashBoardItemList != null && hrDashBoardItemList.size() > 0) {
	                result = hrDashBoardItemList.get(0);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return result;
	}


	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveCopyDashBoard(CopyDashBoardReqTO reqTO)
	throws Exception
	{
		AltCheckList altCheckList = null;
		List<DashBoardTO> dashBoardTOList = null ;
		try 
		{
			
			dashBoardTOList = reqTO.getDashBoardTOList();
			for(DashBoardTO dTO :dashBoardTOList)
			{
				if(dTO.getHrDashBoardID()==null)
				{
					if(dTO.isStatus()==true)
					{
						HrDashBoardItem hrItem = new HrDashBoardItem();
						hrItem.setColumnPosition(dTO.getColumnPosition());
						hrItem.setCreatedBy(1);
						hrItem.setCreatedDate(new Date());
						//hrItem.setHrDashboardItemID(hrDashboardItemID);
						hrItem.setIsActive(true);
						hrItem.setModifiedBy(1);
						hrItem.setModifiedDate(new Date());
						hrItem.setOrganizationID(reqTO.getOrganizationID().intValue());
						hrItem.setRowPosition(dTO.getRowPosition());
						hrItem.setSysDashboardItemID(dTO.getSysDashboardItemID());
						hrItem.setTenantID(reqTO.getTenantID().intValue());
						getEntityManager("TalentPact").persist(hrItem);
					}
				}else{
					HrDashBoardItem hrItem  = getEntityManager("TalentPact").find(HrDashBoardItem.class, dTO.getHrDashBoardID());
					hrItem.setColumnPosition(dTO.getColumnPosition());
					hrItem.setCreatedBy(1);
					//hrItem.setCreatedDate(new Date());
					//hrItem.setHrDashboardItemID(hrDashboardItemID);
					hrItem.setIsActive(dTO.isStatus());
					hrItem.setModifiedBy(1);
					hrItem.setModifiedDate(new Date());
					hrItem.setOrganizationID(reqTO.getOrganizationID().intValue());
					hrItem.setRowPosition(dTO.getRowPosition());
					hrItem.setSysDashboardItemID(dTO.getSysDashboardItemID());
					hrItem.setTenantID(reqTO.getTenantID().intValue());
					getEntityManager("TalentPact").merge(hrItem);
				}
			}
			 //saving check -list in altCheckList tabble 
            altCheckList = findAltCheckList(reqTO.getSysCheckListID().intValue(), reqTO.getOrganizationID(), reqTO.getTenantID());
            if (altCheckList == null) {
                altCheckList = new AltCheckList();
                altCheckList.setSysCheckListID(reqTO.getSysCheckListID().intValue());
                altCheckList.setOrganizationID(reqTO.getOrganizationID());
                altCheckList.setTenantID(reqTO.getTenantID());
                altCheckList.setModifiedDate(new Date());
                altCheckList.setCreatedDate(new Date());
                altCheckList.setIsCompleted(true);
                altCheckList.setCreatedBy(reqTO.getCreatedBy());
                altCheckList.setModifiedBy(reqTO.getModifiedBy());

                getEntityManager("AltCommon").persist(altCheckList);
            }
            getEntityManager("AltCommon").flush();
            getEntityManager("TalentPact").flush();
            //end here 
		} catch (Exception e) {
			throw e;
		}
		
	}
	 public AltCheckList findAltCheckList(int sysCheckListId, Long OrgId, Long TenantId)
	 {

	        Query query = null;
	        StringBuilder hqlQuery = null;
	        List<AltCheckList> altCheckList = null;
	        AltCheckList result = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select altCheckList from AltCheckList altCheckList where altCheckList.sysCheckListID=:SysCheckListID and altCheckList.organizationID=:OrgID and altCheckList.tenantID=:TenantID ");

	            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
	            query.setParameter("SysCheckListID", sysCheckListId);
	            query.setParameter("OrgID", OrgId);
	            query.setParameter("TenantID", TenantId);
	            altCheckList = query.getResultList();
	            if (altCheckList != null && altCheckList.size() > 0) {
	                result = altCheckList.get(0);
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return result;
	    }

}