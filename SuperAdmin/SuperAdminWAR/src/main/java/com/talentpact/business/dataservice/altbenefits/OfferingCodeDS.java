/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.to.OfferingCodeTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.OfferingCode;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class OfferingCodeDS extends AbstractDS<OfferingCode>
{
    public OfferingCodeDS()
    {
        super(OfferingCode.class);
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public Long saveOfferCode(OfferingCodeTO offeringCodeTO)
        throws Exception
    {
        OfferingCode offeringCode = null;
        try {
            if (!offeringCodeTO.isEdit()) {
                offeringCode = new OfferingCode();
                offeringCode.setVendorOfferingID(offeringCodeTO.getVendorOfferingID());
                offeringCode.setActive(offeringCodeTO.isActive());
                offeringCode.setAccessTypeID(offeringCodeTO.getAccessTypeID());
                offeringCode.setLogicBased(offeringCodeTO.isLogicBased());
                offeringCode.setOfferingCode(offeringCodeTO.getOfferingCode().trim());
                offeringCode.setOfferingCodeStartDateID(offeringCodeTO.getOfferingCodeStartDateID());
                offeringCode.setOfferingCodeEndDateID(offeringCodeTO.getOfferingCodeEndDateID());
                offeringCode.setMaximumLimitApplicable(offeringCodeTO.isMaximumLimitApplicable());
                getEntityManager("Talentpact").persist(offeringCode);
            } else {
                offeringCode = getEntityManager("Talentpact").find(OfferingCode.class, offeringCodeTO.getOfferingCodeID());
                offeringCode.setVendorOfferingID(offeringCodeTO.getVendorOfferingID());
                offeringCode.setActive(offeringCodeTO.isActive());
                offeringCode.setAccessTypeID(offeringCodeTO.getAccessTypeID());
                offeringCode.setLogicBased(offeringCodeTO.isLogicBased());
                offeringCode.setOfferingCode(offeringCodeTO.getOfferingCode().trim());
                offeringCode.setOfferingCodeStartDateID(offeringCodeTO.getOfferingCodeStartDateID());
                offeringCode.setOfferingCodeEndDateID(offeringCodeTO.getOfferingCodeEndDateID());
                offeringCode.setMaximumLimitApplicable(offeringCodeTO.isMaximumLimitApplicable());
                getEntityManager("Talentpact").merge(offeringCode);
                getEntityManager("Talentpact").flush();
            }

        } catch (Exception e) {
            throw e;
        }
        return offeringCode.getOfferingCodeID();

    }


    /**
     * 
     * @param vendorOfferingID
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<OfferingCode> getOfferingCodeListByVendorOffering(Long vendorOfferingID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<OfferingCode> OfferingCodeList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  OfferingCode s where s.vendorOfferingID=:vendorOfferingID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorOfferingID", vendorOfferingID);
            OfferingCodeList = query.getResultList();

        } catch (Exception ex) {
            throw ex;
        }
        return OfferingCodeList;
    }

}