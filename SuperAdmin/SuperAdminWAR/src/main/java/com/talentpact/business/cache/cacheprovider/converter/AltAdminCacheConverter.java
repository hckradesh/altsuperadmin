package com.talentpact.business.cache.cacheprovider.converter;

import java.util.ArrayList;
import java.util.List;

import com.alt.loginpage.transport.output.LoginPageResponseTO;





public class AltAdminCacheConverter
{    
    
    public static List<LoginPageResponseTO> traverseDbResultSetInAuthenticationTOList(
            List<Object[]> tpUrlList) throws Exception {

        String leftLogo = null;
        String rightLogo = null;
        String loginPage = null;
        String homePage = null;
       String appName= null;
        String favicon = null;
        String loginPageTheme = null;
        String title = null;
        String meta_Data = null;
        String defaultTheme = null;       
        String appCode = null;
        long urlID = 0;
        String domainName = null;
        Long orgId = 0l;
        Long tenantId = 0l;
        Long portalTypeId = 0l;
        String orgName = null;
        String orgCode = null;
        String portalName = null;
       
        LoginPageResponseTO authenticateTO = null;
        List<LoginPageResponseTO> authenticateTOList = null;        
      
        
        try {
            if (tpUrlList != null) {
                authenticateTOList = new ArrayList<LoginPageResponseTO>();
                for (int i = 0; i < tpUrlList.size(); i++) {
                    Object[] array = tpUrlList.get(i);

                    if (array[0] != null) {
                        leftLogo = (String) array[0];
                    }
                    if (array[1] != null) {
                        rightLogo = (String) array[1];
                    }

                    if (array[2] != null) {
                        loginPage = (String) array[2];
                    }
                    if (array[3] != null) {
                        homePage = (String) array[3];
                    }
                    
                    if (array[4] != null) {
                        favicon = (String) array[4];
                    }
                    if (array[5] != null) {
                        loginPageTheme = (String) array[5];
                    }
                    if (array[6] != null) {
                        title = (String) array[6];
                    }
                    if (array[7] != null) {
                        meta_Data = (String) array[7];
                    }
                    if (array[8] != null) {
                        defaultTheme = (String) array[8];
                    }
                    if (array[9] != null) {
                        domainName = (String) array[9];
                    }

                    if (array[10] != null) {
                        appName = (String) array[10];
                    }

                    if (array[11] != null) {
                        urlID = (Long) array[11];
                    }

                    if (array[12] != null) {
                        portalTypeId = (Long) array[12];
                    }

                    if (array[13] != null) {
                        orgId = (Long) array[13];
                    }

                    if (array[14] != null) {
                        orgName = (String) array[14];
                    }

                    if (array[15] != null) {
                        orgCode = (String) array[15];
                    }

                    if (array[16] != null) {
                        tenantId = (Long) array[16];
                    }

                    if (array[17] != null) {
                        portalName = (String) array[17];
                    }  
                   
                    if (domainName == null || portalName == null) {
                        throw new NullPointerException("Invalid Data in DB Exception ::Url name and Portal name is null  ");
                    }
                   
                    
                    authenticateTO = new LoginPageResponseTO();
                    /* Fill Organization data */
                    authenticateTO.setOrganizationId(orgId);
                    authenticateTO.setOrganizationName(orgName);
                    authenticateTO.setOrganizationCode(orgCode);
                    authenticateTO.setTenantId(tenantId);
                    authenticateTO.setLeftLogo(leftLogo);
                    authenticateTO.setDomainName(domainName);                    
                    authenticateTO.setLoginPage(loginPage);
                   
                
                   
                    authenticateTO.setFavicon(favicon);
                    authenticateTO.setLoginPageTheme(loginPageTheme);
                    authenticateTO.setTitle(title);
                   
                    authenticateTOList.add(authenticateTO);

                }

            }
        } catch (Exception ex) {
            throw new Exception();
        }
        return authenticateTOList;

    }
    
  
    
    
    
    
    
   
    
    
}
