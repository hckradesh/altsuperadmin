package com.talentpact.business.adhocreportsui.controller;

import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_APPLICTIONINPUT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_CONSTANT;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_COLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_DERIVEDGENERICCOLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_DERIVEDGROUPINGCOLUMN;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_NONE;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_PROCESSINGLOGIC;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_EMPTY;
import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_ORGANIZATIONID;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.common.controller.EnumJoinType;
import com.talentpact.adhocreports.business.common.controller.EnumSQLTableOperation;
import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.controller.CreateQuery;
import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.business.adhocreportsui.bean.CommonReportDataBean;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;
import com.talentpact.business.adhocreportsui.common.DummyData;

@SessionScoped
@Named("commonReportDataController")
public class CommonReportDataController implements Serializable {

	private static final long serialVersionUID = 6718019297164890400L;
	private static final Logger LOGGER = Logger.getLogger(CommonReportDataController.class);

	@Inject
	ReportDetailBean reportDetailBean;

	public void initialize(CommonReportDataBean commonReportDataBean) {
		try{
			commonReportDataBean.setDefaultTables(CreateQuery.getDefaultNormalTables());
			commonReportDataBean.setAllTableColumnsMap(CreateQuery.getAllTableColumnsMap());
			List<SelectItem> joinTypeList = CreateQuery.getJoinTypeList();
			List<SelectItem> joinTypeListWithoutNoJoin = new ArrayList<SelectItem>();
			for(SelectItem joinType: joinTypeList){
				if(!joinType.getLabel().equalsIgnoreCase(EnumJoinType.NOJOIN.getName())){
					joinTypeListWithoutNoJoin.add(joinType);
				}
			}
			commonReportDataBean.setJoinTypes(joinTypeListWithoutNoJoin);
			commonReportDataBean.setDataTypes(CreateQuery.getDataTypeList());
			commonReportDataBean.setProcessingLogicTemplatesMap(CreateQuery.getProcessingLogicTemplatesMap());
			commonReportDataBean.setProcessingLogicTemplatesMapById(CreateQuery.getProcessingLogicTemplatesMapById());
			commonReportDataBean.setFilterCombinationOperatorList(CreateQuery.getFilterCombinationOperatorList());
			commonReportDataBean.setOperatorDataTypeMap(CreateQuery.getOperatorDataTypeMap());
			commonReportDataBean.setApplicationInputToList(CreateQuery.getApplicationInputToList());
			commonReportDataBean.setApplicationInputList(CreateQuery.getApplicationInputList());
			commonReportDataBean.setConstantToList(CreateQuery.getConstantToList());
			commonReportDataBean.setConstantList(CreateQuery.getConstantList());
			commonReportDataBean.setApplicationInputProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMap().get(CONSTANT_APPLICTIONINPUT).get(0));
			commonReportDataBean.setConstantInputProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMap().get(CONSTANT_CONSTANT).get(0));
			commonReportDataBean.setColumnProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMap().get(CONSTANT_COLUMN).get(0));
			commonReportDataBean.setGenericColumnProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMap().get(CONSTANT_DERIVEDGENERICCOLUMN).get(0));
			commonReportDataBean.setGroupingColumnProcessingLogicTemplate(commonReportDataBean.getProcessingLogicTemplatesMap().get(CONSTANT_DERIVEDGROUPINGCOLUMN).get(0));
			commonReportDataBean.setProcessingLogicTemplateTypeList(CreateQuery.getProcessingLogicTemplateTypeList());
			commonReportDataBean.setProcessingLogicTemplateTypeListWithNone(new ArrayList<SelectItem>(commonReportDataBean.getProcessingLogicTemplateTypeList()));
			commonReportDataBean.setOrganizationList(CreateQuery.getOrganizationList());
			commonReportDataBean.setNonDefaultReports(CreateQuery.getNonDefaultReportDetails());
			commonReportDataBean.setNonDefaultReportsInOrgMapping(CreateQuery.getNonDefaultReportDetailsInOrgMapping());
			commonReportDataBean.setOrgAdminDerivedTables(CreateQuery.getOrgAdminDerivedTables());
			commonReportDataBean.setOrgAdminDerivedTablesInOrgMapping(CreateQuery.getOrgAdminDerivedTablesInOrgMapping());
			commonReportDataBean.setCssClassList(CreateQuery.getReportCssMasterList());
			commonReportDataBean.setAllReportsDetails(CreateQuery.getAllReportDetails());
			List<SelectItem> moduleList = CreateQuery.getModules();
			moduleList.add(0,new SelectItem("NONE", "0"));
			commonReportDataBean.setCommonModuleList(moduleList);

			List<ReportDetail> reportDetailList = new ArrayList<ReportDetail>();
			List<String> orgList = new ArrayList<String>();
			List<TableTo> tableToList = new ArrayList<TableTo>();
			List<String> tableNameList = new ArrayList<String>();

			for(ReportDetail reportDetail : commonReportDataBean.getNonDefaultReports()){
				for(SelectItem organization : commonReportDataBean.getOrganizationList()){
					reportDetailList.add(new ReportDetail(reportDetail.getReportId(), reportDetail.getReportType(), reportDetail.getModuleId(),
							reportDetail.getModuleName(), reportDetail.getReportName(), organization, reportDetail.getReportDescription(), false));
				}
			}

			for(TableTo table : commonReportDataBean.getOrgAdminDerivedTables()){
				for(SelectItem organization : commonReportDataBean.getOrganizationList()){
					tableToList.add(new TableTo(table.getId(), table.getName(), table.getTableType(), table.getDesc(),
							organization, false));
				}
			}

			for(TableTo table : commonReportDataBean.getOrgAdminDerivedTables()){
				tableNameList.add(table.getName());
			}
			commonReportDataBean.setDerivedTableNameList(tableNameList);

			for(TableTo table : commonReportDataBean.getOrgAdminDerivedTablesInOrgMapping()){
				if(tableToList.contains(table)){
					tableToList.remove(table);
					table.setSelected(true);
					tableToList.add(table);
				}
			}
			commonReportDataBean.setOrgAdminDerivedTablesAllOrg(tableToList);

			for(SelectItem organization : commonReportDataBean.getOrganizationList()){
				orgList.add(organization.getLabel());
			}
			commonReportDataBean.setOrganizationNameList(orgList);

			for(ReportDetail reportDetail : commonReportDataBean.getNonDefaultReportsInOrgMapping()){
				if(reportDetailList.contains(reportDetail)){
					reportDetailList.remove(reportDetail);
					reportDetail.setSelected(true);
					reportDetailList.add(reportDetail);
				}
			}

			commonReportDataBean.setNonDefaultReportsAllOrg(reportDetailList);
			SelectItem selectItem = new SelectItem();
			selectItem.setLabel(CONSTANT_NONE);
			selectItem.setValue(CONSTANT_EMPTY);
			commonReportDataBean.getProcessingLogicTemplateTypeListWithNone().add(0, selectItem);
			List<SelectItem> processingLogicTemplateTypeListWithoutProcessingLogic = new ArrayList<SelectItem>();
			for(SelectItem processingLogicTemplateType : commonReportDataBean.getProcessingLogicTemplateTypeList()){
				if(!processingLogicTemplateType.getLabel().equalsIgnoreCase(CONSTANT_PROCESSINGLOGIC)){
					processingLogicTemplateTypeListWithoutProcessingLogic.add(processingLogicTemplateType);
				}
			}
			commonReportDataBean.setProcessingLogicTemplateTypeListWithoutProcessingLogic(processingLogicTemplateTypeListWithoutProcessingLogic);
			commonReportDataBean.setProcessingLogicTemplateTypeListWithoutProcessingLogicWithNone(new ArrayList<SelectItem>(commonReportDataBean.getProcessingLogicTemplateTypeListWithoutProcessingLogic()));
			commonReportDataBean.getProcessingLogicTemplateTypeListWithoutProcessingLogicWithNone().add(0, selectItem);
			commonReportDataBean.setTableOperation(new ArrayList<String>());
			for(EnumSQLTableOperation enumValue : EnumSQLTableOperation.values()){
				commonReportDataBean.getTableOperation().add(enumValue.getName());
			}

			Integer applicationInputOrganizationId = null;
			for(SelectItem item : commonReportDataBean.getApplicationInputList()){
				if(item.getLabel().equalsIgnoreCase(CONSTANT_ORGANIZATIONID)){
					applicationInputOrganizationId = Integer.valueOf(item.getValue());
					break;
				}
			}
			commonReportDataBean.setApplicationInputOrganizationId(applicationInputOrganizationId);

			List<DummyData> dummyList = new ArrayList<DummyData>();
			DummyData dummyData = new DummyData();
			dummyData.setInt1("1");
			dummyData.setDatetime1("2014-03-15 00:00:51.790");
			dummyData.setDate1("2014-03-15");
			dummyData.setString1("string1");
			dummyData.setFloat1("1.01");
			dummyData.setBit1("false");
			dummyData.setBigint1("11111111111111");
			dummyList.add(dummyData);dummyList.add(dummyData);dummyList.add(dummyData);dummyList.add(dummyData);dummyList.add(dummyData);
			commonReportDataBean.setDummyDataList(dummyList);

		}catch(Exception e){
			LOGGER.error("Error inside initialize(CommonReportDataBean commonReportDataBean) method "
					+ "while initializing CommonReportDataBean", e);
		}
	}

}
