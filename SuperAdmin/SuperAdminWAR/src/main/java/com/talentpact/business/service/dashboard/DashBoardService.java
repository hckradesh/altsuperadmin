package com.talentpact.business.service.dashboard;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.AppModuleDS;
import com.talentpact.business.dataservice.dashboard.DashBoardDS;
import com.talentpact.model.SysSubModule;
import com.talentpact.ui.dashboard.transport.input.CopyDashBoardReqTO;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.dashboard.transport.output.ModuleTO;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class DashBoardService extends CommonService
{
    @EJB
    DashBoardDS dashBoardDS;
    
    @EJB
    AppModuleDS appModuleDS;

	public List<DashBoardTO> getSysDashBoard()
	throws Exception 
	
	{
		List<DashBoardTO> dashBoardTOList = null ;
             
		try {
			
			dashBoardTOList = dashBoardDS.getSysDashBoard();
		} catch (Exception e) {
			throw e ;
		}
		return dashBoardTOList;
            
	}

	public List<ModuleTO> getModileList() 
	throws Exception
	{
		List<ModuleTO> moduleList = null ;
		List<SysSubModule> sysSubModuleList = null;
		try {
			
			 sysSubModuleList = appModuleDS.getAllModules();
			 moduleList  = new ArrayList<ModuleTO>();
			  for (SysSubModule moduleTo : sysSubModuleList) {
				     ModuleTO portaModulelTO = new ModuleTO();
	                //portaModulelTO.setModuleID(moduleTo.getSubModuleID().longValue());
	                portaModulelTO.setModuleID(moduleTo.getModuleId());
	                portaModulelTO.setModuleName(moduleTo.getModuleName());
	               // portaModulelTO.setAppID(moduleTo.getAppId());
	                portaModulelTO.setParentModuleID(moduleTo.getParentModuleId());
	                
	                moduleList.add(portaModulelTO);
			  }
		} catch (Exception e) {
			throw e ;
		}
		return moduleList;
	}

	public void saveDashBoard(DashBoardTO dashBoardTO)
	throws Exception 
	{
		try
		{
			dashBoardDS.saveSysDashBoard(dashBoardTO);
		} catch (Exception e) {
			throw e;
		}
		
	}

	public List<DashBoardTO> getDashBoardsForOrg(ServiceRequest serviceRequest)
	throws Exception
	{
		List<DashBoardTO> dashBoardTOlist  = null ;
		try
		{
			dashBoardTOlist = dashBoardDS.getDashBoardsForOrg(serviceRequest);
			return dashBoardTOlist;
		} catch (Exception e) {
			throw e;
		}
	}

	public ServiceResponse saveCopyDashBoard(ServiceRequest serviceRequest) 
	throws Exception
	{
		CopyDashBoardReqTO reqTO = (CopyDashBoardReqTO) serviceRequest.getRequestTansportObject();
		try {
			dashBoardDS.saveCopyDashBoard(reqTO) ;
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

   
}