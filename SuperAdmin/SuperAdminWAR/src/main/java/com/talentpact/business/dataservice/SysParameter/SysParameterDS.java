/**
 * 
 */
package com.talentpact.business.dataservice.SysParameter;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.business.application.transport.output.SysParameterResponseTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysDataType;
import com.talentpact.model.SysParameter;
import com.talentpact.model.configuration.SysContentCategory;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Stateless
public class SysParameterDS extends AbstractDS<SysContentCategory>
{
    public SysParameterDS()
    {
        super(SysContentCategory.class);
    }

    public List<Object[]> getListingData()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();

            hqlQuery.append(" select distinct sysparameter.paramId, sysparameter.paramLabel, sysparameter.paramDataTypeId.dataTypeName , sysparameter.paramDescription " + " from "
                    + SysParameter.class.getName() + " sysparameter , " + SysDataType.class.getName() + " sysdataType  order by sysparameter.paramId desc ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> dataTypeDropDownList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysdataType.dataTypeName , sysdataType.dataTypeId from  " + SysDataType.class.getName() + " sysdataType");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean saveSysParameter(SysParameterResponseTO sysParameterResponseTO)
    {
        // TODO Auto-generated method stub
        SysDataType sdt=null;
        sdt=getEntityManager("TalentPact").find(SysDataType.class, sysParameterResponseTO.getDataTypeId());
        SysParameter sysParameter = new SysParameter();      
        sysParameter.setParamDescription(sysParameterResponseTO.getParamDescription());
        sysParameter.setParamLabel(sysParameterResponseTO.getParamLabel());
        sysParameter.setTenantID(1);
        sysParameter.setParamDataTypeId(sdt);
        getEntityManager("TalentPact").persist(sysParameter);
        Integer parameterID = sysParameter.getParamId();
        if (parameterID != null && parameterID != 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean editData(SysParameterResponseTO sysParameterResponseTO)
    {
        // TODO Auto-generated method stub
        try {
            SysDataType sdt=null;
            sdt=getEntityManager("TalentPact").find(SysDataType.class, sysParameterResponseTO.getDataTypeId());
            SysParameter sysParameter = new SysParameter();
            sysParameter.setParamId(sysParameterResponseTO.getParamID());
            sysParameter.setParamDescription(sysParameterResponseTO.getParamDescription());
            sysParameter.setParamLabel(sysParameterResponseTO.getParamLabel());
            sysParameter.setTenantID(1);
            sysParameter.setParamDataTypeId(sdt);
            getEntityManager("TalentPact").merge(sysParameter);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public List<Object[]> geteditData(SysParameterResponseTO sysParameterResponseTO)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct sysparameter.paramId, sysparameter.paramLabel, sysparameter.paramDataTypeId.dataTypeName , sysparameter.paramDescription , sysparameter.paramDataTypeId.dataTypeId "
                    + " from " + SysParameter.class.getName() + " sysparameter , " + SysDataType.class.getName() + " sysdataType  where  sysparameter.paramId = :paramid ");

            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("paramid", sysParameterResponseTO.getParamID());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

}