/**
 * 
 */
package com.talentpact.business.application.transport.helper;

import java.util.List;

import com.talentpact.business.application.transport.output.UserResponseTO;


/**
 * @author pankaj.sharma1
 *
 */
public class UserListHelper
{
    public static boolean checkUserNameinList(String userName, List<UserResponseTO> userTOList)
    {

        for (UserResponseTO user : userTOList) {
            if (user != null && user.getUserName() != null && (user.getUserName()).equalsIgnoreCase(userName.trim())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkUserIDinList(Long userID, List<UserResponseTO> userTOList)
    {

        for (UserResponseTO user : userTOList) {
            if (user != null && user.getUserID() != null && (user.getUserID()).equals(userID)) {
                return true;
            }
        }
        return false;
    }
}
