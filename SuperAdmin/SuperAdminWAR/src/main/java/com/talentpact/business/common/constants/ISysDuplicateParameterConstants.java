package com.talentpact.business.common.constants;

/**
 * 
 * @author prachi.bansal
 *
 */

public interface ISysDuplicateParameterConstants{
    public static String SYSDUPLICATEPARAMETER_ADD_DIALOG_HIDE = "PF('sysDuplicateParameterAddModal').hide();";

    public static String SYSDUPLICATEPARAMETER_EDIT_DIALOG_HIDE = "PF('sysDuplicateParameterEditModal').hide();";

    public static String SYSDUPLICATEPARAMETER_SAVE_ERROR_MSG = "Error occured while saving SysDuplicateParameter.";

    public static String SYSDUPLICATEPARAMETER_UPDATE_ERROR_MSG = "Error occured while updating SysDuplicateParameter.";

    public static String SYSDUPLICATEPARAMETER_SAVE_SUCCESS_MSG = "SysDuplicateParameter saved successfully.";

    public static String SYSDUPLICATEPARAMETER_UPDATE_SUCCESS_MSG = "SysDuplicateParameter updated successfully.";

    public static String SYSDUPLICATEPARAMETER_DATATABLE_RESET = "PF('sysDuplicateParameterDataTable').clearFilters();";

    public static String SYSDUPLICATEPARAMETER_PAGINATION_DATATABLE_RESET = "PF('sysDuplicateParameterDataTable').getPaginator().setPage(0);";

    public static String SYSDUPLICATEPARAMETER_DUPLICATE_ERROR_MSG = "SysDuplicateParameter already present in system.";
}