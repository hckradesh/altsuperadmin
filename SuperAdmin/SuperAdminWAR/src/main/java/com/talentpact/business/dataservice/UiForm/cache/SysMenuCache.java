/**
 * 
 */
package com.talentpact.business.dataservice.UiForm.cache;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.talentpact.business.cache.RedisClient;
import com.talentpact.ui.menu.transport.MenuTO;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
//@TransactionManagement(value = TransactionManagementType.CONTAINER)
//@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysMenuCache
{
    // @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public Map<Long, List<MenuTO>> getAltMenuMap(Integer bundleId)
        throws Exception

    {
        Map<Long, List<MenuTO>> menuCacheMap = null;
        Integer bundleID = null;
        try {
            bundleID = bundleId;
            // menuCacheMap = RedisClient.getValueAsMap(AuthCacheConstant.ALT_MENU_KEY_PREFIX + organizationID + "-" + tenantID, MenuTO.class);
            menuCacheMap = RedisClient.getValueAsMapWithGenericKey("SUPERADMIN:MENULIST:" + bundleID, Long.class, MenuTO.class);
            return menuCacheMap;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
        }

    }

}
