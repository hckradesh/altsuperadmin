/**
 * 
 */
package com.talentpact.business.startup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;

import org.apache.log4j.Logger;

import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.common.cache.CacheUtil;
import com.talentpact.business.common.constants.IConstants;

/**
 * @author vikas.singh
 * 
 */
@javax.ejb.Startup
@Singleton
public class Startup {

	@EJB
	StartupService startupService;

	private static final Logger _LOGGER = Logger.getLogger(Logger.class);

	@PostConstruct
	public void doInitialStuff() {

		try {
			populatedatabaseMeta();
			populateSysRoleTypeTO();
			 populateSysMenuTO();
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {

		}
	}

	public void populatedatabaseMeta() {
		Map<String, String> dataList = null;
		try {
			dataList = startupService.setAllDatabase();
			CacheUtil.setDatabaseMetadata(dataList);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}
	public void populateSysRoleTypeTO() {
		Map<String, SysRoleTypeTO> dataList = null;

		try {
			dataList = startupService.setAllSysRoleTypeMap();
			CacheUtil.setSysRoleTypeToMap(dataList);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}
	public void populateSysMenuTO() {
		List<SysMenuTO> sysMenuToList = null;

		try {
			sysMenuToList = startupService.setAllSysMenuToList();
			CacheUtil.setSysMenuToList(sysMenuToList);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}
	
}
