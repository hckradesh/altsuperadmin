/**
 * 
 */
package com.talentpact.business.tpOrganization.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.alt.altbenefits.to.OrganizationTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.common.constants.CoreConstants;
import com.alt.security.transport.output.UiFormTO;
import com.talentpact.ats.to.admin.PortalTO;
import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpPortalType;
import com.talentpact.auth.entity.TpTenant;
import com.talentpact.auth.entity.TpURL;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.application.transport.input.AltBenefitRequestTO;
import com.talentpact.business.application.transport.input.AppRequestTO;
import com.talentpact.business.application.transport.input.CheckListRequestTO;
import com.talentpact.business.application.transport.input.ContentListRequestTO;
import com.talentpact.business.application.transport.input.CopyCheckListRequestTO;
import com.talentpact.business.application.transport.input.DashboardChartRequestTO;
import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.application.transport.input.HrModuleRequestTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.input.SysMenuRequestTO;
import com.talentpact.business.application.transport.input.SysProductAnnouncementRequestTO;
import com.talentpact.business.application.transport.input.SysRoleRequestTO;
import com.talentpact.business.application.transport.input.UIFormRequestTO;
import com.talentpact.business.application.transport.input.UserRequestTO;
import com.talentpact.business.application.transport.output.AltBenefitResponseTO;
import com.talentpact.business.application.transport.output.AppResponseTO;
import com.talentpact.business.application.transport.output.CheckListResponseTO;
import com.talentpact.business.application.transport.output.DashboardChartResponseTO;
import com.talentpact.business.application.transport.output.HrModuleTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysProductAnnouncementTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.application.transport.output.UserResponseTO;
import com.talentpact.business.cache.cacheprovider.AltAdminCacheService;
import com.talentpact.business.cache.cacheprovider.CacheUpdateException;
import com.talentpact.business.cache.cacheprovider.OrganizationCache;
import com.talentpact.business.common.cache.CacheUtil;
import com.talentpact.business.common.constants.EnumSysConstant;
import com.talentpact.business.common.constants.IConstants;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.common.transport.IResponseTransportObject;
import com.talentpact.business.dataservice.AppModuleDS;
import com.talentpact.business.dataservice.FormSecurityDS;
import com.talentpact.business.dataservice.HrOrganisationDS;
import com.talentpact.business.dataservice.SysMenuDS;
import com.talentpact.business.dataservice.TPOrgDS;
import com.talentpact.business.dataservice.TenantDS;
import com.talentpact.business.dataservice.TpUserDS;
import com.talentpact.business.dataservice.altbenefits.HrVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsKeyConstants;
import com.talentpact.business.dataservice.dashboardChart.DashboardChartService;
import com.talentpact.business.dataservice.hrApp.HrAppService;
import com.talentpact.business.dataservice.loginserver.LoginServerTypeDS;
import com.talentpact.business.dataservice.productAnnouncement.ProductAnnouncementDS;
import com.talentpact.business.dataservice.tpApp.TpAppConstants;
import com.talentpact.business.dataservice.tpApp.TpAppService;
import com.talentpact.business.dataservice.tpPortal.TpPortalTypeDS;
import com.talentpact.business.service.altbenefits.HrVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysVendorOfferingTemplateService;
import com.talentpact.business.service.loginserver.LoginServerService;
import com.talentpact.business.startup.StartupService;
import com.talentpact.business.tenants.converter.TenantConverter;
import com.talentpact.business.tenants.transport.newtenant.input.TpURLTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tenants.transport.newtenant.output.OrgMoreInfoResponseTO;
import com.talentpact.business.tenants.transport.newtenant.output.TpURLResponseTO;
import com.talentpact.business.tpModule.transport.input.TpModuleRequestTO;
import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.business.tpOrganization.constants.EnumPortalType;
import com.talentpact.business.tpOrganization.converter.OrganizationConverter;
import com.talentpact.business.tpOrganization.exception.OrganizationException;
import com.talentpact.business.tpOrganization.transport.input.AppListRequestTO;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.business.tpOrganization.transport.input.VideoLinkTO;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlListResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.OrganizationResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.model.HrModule;
import com.talentpact.model.altbenefit.HrVendorOfferingTemplate;
import com.talentpact.model.organization.HrOrganization;
import com.talentpact.model.organization.HrProductAnnouncement;
import com.talentpact.model.organization.SysProductAnnouncement;
import com.talentpact.ui.survey.user.common.transport.output.TpUserCreatedResponseTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

@Stateless
public class OrganizationService extends CommonService {
	private final static Logger UTIL_LOGGER = Logger.getLogger(OrganizationService.class.getName());

	@EJB
	TenantDS tenantDS;

	@EJB
	TPOrgDS tPOrgDS;

	@EJB
	HrOrganisationDS hrOrganisationDS;

	@EJB
	AppModuleDS appModuleDS;

	@EJB
	OrganizationCache organizationCache;

	@EJB
	SysMenuDS sysMenuDS;

	@EJB
	AltAdminCacheService altAdmin;

	@EJB
	TpUserDS tpUserDS;

	@EJB
	StartupService startupService;

	@EJB
	FormSecurityDS formSecurityDS;

	@EJB
	SysVendorOfferingTemplateService sysVendorOfferingTemplateService;

	@EJB
	HrVendorOfferingTemplateDS hrVendorOfferingTemplateServiceDS;

	@EJB
	AltBenefitsCache altBenefitsCache;

	@EJB
	HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

	@EJB
	ProductAnnouncementDS productAnnouncementDS;

	@EJB
	LoginServerService loginServerService;

	@EJB
	HrAppService hrAppService;

	@EJB
	TpAppService tpAppService;

	@EJB
	LoginServerTypeDS ds;

	@EJB
	DashboardChartService dashboardChartService;

	@EJB
	TpPortalTypeDS tpPortalTypeDS;

	/* method to get all URLs */
	public ServiceResponse getAllUrls(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		OrganizationConverter organizationConverter = null;
		List<TpURLTO> result = null;
		TpURLResponseTO tpURLResponseTO = null;
		try {

			tpURLResponseTO = new TpURLResponseTO();
			organizationConverter = new OrganizationConverter();
			result = organizationConverter.convert(tPOrgDS.getAllURLs());
			tpURLResponseTO.setTpURLTOLIST(result);

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(tpURLResponseTO);

		} catch (Exception ex) {
			UTIL_LOGGER.severe(ex.getMessage());
		}
		return serviceResponse;
	}

	/*
	 * The method gives , all Apps , all their portals and respective all
	 * modules. We are using AppId as kep for Mapping
	 */
	public ServiceResponse getCompleteAppTree(ServiceRequest serviceRequest) throws OrganizationException {

		ServiceResponse serviceResponse = null;
		OrgAppPortalTreeResponseTO appTreeTo = null;
		try {
			appTreeTo = getOrgAppPortalTreeResponseTO();
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(appTreeTo);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new OrganizationException(ex.getMessage());
		}
		return serviceResponse;

	}

	/* Get Master Data of Apps , Portals an Modules */
	private OrgAppPortalTreeResponseTO getOrgAppPortalTreeResponseTO() {

		List<AppResponseTO> appList = null;
		OrgAppPortalTreeResponseTO appTreeTo = null;
		try {

			appTreeTo = new OrgAppPortalTreeResponseTO();
			appList = OrganizationConverter.getAppResTo(tPOrgDS.getAllApps());
			Map<Long, List<AppPortalTypeUrlResponseTO>> appPortalMap = OrganizationConverter
					.getMapOfAppAndPortal(tPOrgDS.getAllPortals());
			Map<Long, List<TpModuleResponseTO>> appModuleMap = OrganizationConverter
					.getMapOfAppAndModules(appModuleDS.getAllModules());
			List<NewTenantResponseTO> tenantToList = TenantConverter
					.convertTenantEntityToDataTO(tenantDS.getAllTenants());
			List<Integer> categoryList = new ArrayList<Integer>();
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Login_Page));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Home_Page));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Login_Page_Theme));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Default_Theme));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Left_Logo));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Right_Logo));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Footer));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.UsernameTxt));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.PasswordTxt));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.BtnTxt));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.Banner));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.SamlHeader));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.SamlBtnTxt));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.HeadingTxt));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.MobileStatus));
			categoryList.add(EnumSysConstant.getCatgId(IConstants.BackGroundImage));

			/* .add().add(,,IConstants.Home_Page]; */
			Map<String, List<String>> constantMap = OrganizationConverter
					.getmapOfCategoryAndConstant(appModuleDS.getSysConstant(categoryList));
			appTreeTo = new OrgAppPortalTreeResponseTO();
			appTreeTo.setAppList(appList);
			appTreeTo.setAppModuleMap(appModuleMap);
			appTreeTo.setAppPortalUrlMap(appPortalMap);
			appTreeTo.setSysConstantMap(constantMap);
			appTreeTo.setTenantList(tenantToList);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return appTreeTo;
	}

	public ServiceResponse getAllApps(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;

		List<AppRequestTO> result = null;
		AppResponseTO appResponseTO = null;
		try {

			appResponseTO = new AppResponseTO();

			result = OrganizationConverter.convertApp(tPOrgDS.getAllApps());
			appResponseTO.setAppRequestTOList(result);

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(appResponseTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	/* method to save Organization */
	public ServiceResponse saveOrganization(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;

		OrganizationrequestTO organizationrequestTO = null;

		try {
			organizationrequestTO = (OrganizationrequestTO) serviceRequest.getRequestTansportObject();
			/* Save TpOrganization */
			TpOrganization tpOrganization = tPOrgDS.saveOrganization(organizationrequestTO);
			/* Save HrOrganization */
			hrOrganisationDS.saveHrOrganization(tpOrganization, organizationrequestTO);
			saveTpOrgModule(organizationrequestTO, tpOrganization.getOrganizationID());

			/* delete redis keys */
			altBenefitsCache.deleteHrOfferingTemplateListByOrgIDRedisKey();
			altBenefitsCache.deleteSystemVendorOfferingsTemplatesByOrgRedisKey();
			altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();
			altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
			altBenefitsCache.deleteSystemVendorOfferingsList();
			List<String> orgKeyListCacheList = organizationrequestTO.getOrgKeyListCacheList();
			deleteOldKeysInCache(orgKeyListCacheList, tpOrganization.getOrganizationID());

			OrganizationResponseTO orgTo = new OrganizationResponseTO();
			orgTo.setOrganizationId(tpOrganization.getOrganizationID());

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(orgTo);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	/* Delete Login Keys for Admin portal , Later On It will be configured */
	private void deleteOldKeysInCache(List<String> orgKeyListCacheList, Long orgID) {

		try {
			organizationCache.deleteCommonLoginUrlKey(orgID);
			organizationCache.deleteMobileUrlKey(orgKeyListCacheList, orgID.intValue());
		} catch (CacheUpdateException e1) {
			System.out.println("Exception Occurred while deleteCommonLoginUrlKey  " + e1.getLocalizedMessage());
		}

		if (orgKeyListCacheList != null && orgKeyListCacheList.size() > 0) {
			try {
				organizationCache.deleteAltAdminLoginKeys(orgKeyListCacheList, orgID);

			} catch (CacheUpdateException ex) {
				ex.printStackTrace();
				try {
					organizationCache.deleteAllLoginCacheDataForAllOrg();
				} catch (Exception e) {
					ex.printStackTrace();
					/* TODO Make notify of This */
					System.out.println("Exception Occurred while saving  " + ex.getLocalizedMessage());
				}
			}

		}
	}

	/* Update Alt Admin Login Data */
	private void UpdateForAltAdminCache(Long orgId)

	{

		Long portalID = EnumPortalType.Admin_Portal.getPortalId();
		try {
			altAdmin.populateLoginCache(orgId, portalID);
			/*
			 * Remove this method From here,Make an UI where the user for Admin
			 * Portal is created
			 */
			// altAdmin.loadUserLoginDataInCache(orgId);
		} catch (CacheUpdateException e) {
			// TODO Notify Here
			e.printStackTrace();
		}

	}

	// get all organizations

	/* Get the apps list to get portal Type List */
	public ServiceResponse getPortalsTypebyApp(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		AppListRequestTO appListRequestTO = null;
		List<Object[]> appobjlist = null;
		AppPortalTypeUrlListResponseTO appPortalTypeUrlListResponseTO = null;
		try {

			appListRequestTO = new AppListRequestTO();
			appListRequestTO = (AppListRequestTO) serviceRequest.getRequestTansportObject();
			appobjlist = tPOrgDS.getPortalTypeByAppId(appListRequestTO);
			List<AppPortalTypeUrlResponseTO> list = new OrganizationConverter().ConvertInAppPortal(appobjlist);
			appPortalTypeUrlListResponseTO = new AppPortalTypeUrlListResponseTO();
			appPortalTypeUrlListResponseTO.setAppPortalTypeUrlListResponseTO(list);
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(appPortalTypeUrlListResponseTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getAllOrganizations(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		OrganizationrequestTO organizationrequestTO = null;
		List<OrganizationResponseTO> organizationrequestTOList = null;
		OrganizationResponseTO organizationResponseTO = null;
		try {
			organizationrequestTO = (OrganizationrequestTO) serviceRequest.getRequestTansportObject();
			List<Object[]> orgList = tPOrgDS.getAllOrganizations(organizationrequestTO);
			organizationrequestTOList = OrganizationConverter.convertOrgListToOrgResTO(orgList);
			organizationResponseTO = new OrganizationResponseTO();
			organizationResponseTO.setOrganizationResponseTOList(organizationrequestTOList);
			try {
				organizationResponseTO.setLoginServerTypeTOList(loginServerService.getLoginServerType());
				organizationResponseTO.setPartnerAppList(tpAppService.getPartnerAppList());
				organizationResponseTO.setCustomAppList(tpAppService.getCustomAppList());

			} catch (Exception e) {
				UTIL_LOGGER.severe(e.getMessage());
			}
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(organizationResponseTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	public ServiceResponse getCompleteMergedAppTreeForOrg(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		try {
			Integer sysContentTypeId = null;
			OrganizationResponseTO organizationResTO = null;
			Float attendanceDistanceLimit = null;
			OrganizationrequestTO organizationrequestTO = (OrganizationrequestTO) serviceRequest
					.getRequestTansportObject();
			List<TpOrganization> orgList = tPOrgDS.getOrganizationFromOrgId(organizationrequestTO.getOrganizationId());
			List<HrOrganization> hrOrgList = tPOrgDS.getHrOrganization();
			TpOrganization organization = null;
			for (HrOrganization list : hrOrgList) {
				if (list.getOrganizationId() == organizationrequestTO.getOrganizationId()) {
					sysContentTypeId = list.getSysContentType().getSysTypeId();
					attendanceDistanceLimit = list.getAttendanceDistanceLimit();
				}
			}
			if (orgList != null) {
				organization = orgList.get(0);

				organizationResTO = new OrganizationResponseTO();
				TpTenant tenant = organization.getTenantID();
				organizationResTO.setContractSignedDate(tenant.getContractSignedDate());
				organizationResTO.setStatusID(organization.getStatusId());
				organizationResTO.setDataLocation(organization.getDataLocation());
				organizationResTO.setEffectiveDateFrom(organization.getEffectiveFrom());
				organizationResTO.setContractSignedDate(organization.getContractSignedDate());
				organizationResTO.setEffectiveDateTO(organization.getEffectiveTo());
				organizationResTO.setOrgCode(organization.getOrganizationCode());
				organizationResTO.setOrganizationId(organization.getOrganizationID());
				organizationResTO.setOrgName(organization.getName());
				organizationResTO.setTenantId(organization.getTenantID().getTenantID());
				organizationResTO.setWelcomeMail(
						hrOrganisationDS.getHrOrganizationWelcomeMail(organization.getOrganizationID().intValue()));
				organizationResTO.setEmployeeCount(organization.getEmployeeCount());
				organizationResTO.setLine1(organization.getLine1());
				organizationResTO.setCity(organization.getCity());
				organizationResTO.setCityID(organization.getCityCode());
				organizationResTO.setState(organization.getState());
				organizationResTO.setCountry(organization.getCountry());
				organizationResTO.setZipCode(organization.getZipCode());
				organizationResTO.setEmail(organization.getEmail());
				organizationResTO.setDistrict(organization.getDistrict());
				organizationResTO.setSetupCost(organization.getSetupCost());
				organizationResTO.setRecurringPerMonthAmount(organization.getRecurringPerMonth());
				organizationResTO.setCustomizationAmount(organization.getCustomizationAmount());
				organizationResTO.setSectorId(organization.getSectorID());
				organizationResTO.setWebPayStatus(organization.getWebPayStatus());
				organizationResTO.setSysContentTypeID(sysContentTypeId);
				organizationResTO.setAttendanceDistanceLimit(attendanceDistanceLimit);

				organizationResTO.setLdapServer(organization.getLdapServer());
				organizationResTO.setLdapSearchBase(organization.getLdapSearchBase());
				organizationResTO.setLdapDomain(organization.getLdapDomain());
				organizationResTO.setLdapActive(organization.getLdapActive());
				organizationResTO.setLdapWindowsAuthentication(organization.getLdapWindowsAuthentication());

				organizationResTO.setMessengerEnabled(organization.getMessengerEnabled());
				organizationResTO.setMobileSessionTimeout(organization.getMobileSessionTimeout());

				Map<Long, List<VideoLinkTO>> moduleUrlMap = new HashMap<Long, List<VideoLinkTO>>();
				moduleUrlMap = tPOrgDS.getModuleUrlMap(organization.getOrganizationID().intValue(),
						organization.getTenantID().getTenantID().intValue());
				/* Get TpUrl which are already selected by Organizations */
				List<TpURL> urlListForOrg = tPOrgDS.getActiveTpUrl(organization.getOrganizationID(), null);
				/* Get Modules which are already selected by Organizations */
				List<HrModule> moduleListForOrg = appModuleDS.getAltModule(organization.getOrganizationID().intValue(),
						organization.getTenantID().getTenantID().intValue());

				/* Get Master Data of System */
				OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = getOrgAppPortalTreeResponseTO();

				// if (urlListForOrg != null && urlListForOrg.size() > 0 &&
				// moduleListForOrg != null && moduleListForOrg.size() > 0) {
				/* Merge the Master Data */
				orgAppPortalTreeResponseTO = OrganizationConverter.mergeMasterDataToOrgData(urlListForOrg,
						moduleListForOrg, orgAppPortalTreeResponseTO, moduleUrlMap);
				organizationResTO.setOrgAppPortalTreeResponseTO(orgAppPortalTreeResponseTO);
				// }

				/* Mark Tenant Selected */
				List<NewTenantResponseTO> tenantTOList = orgAppPortalTreeResponseTO.getTenantList();
				if (organizationrequestTO.getTenantID() != null) {
					OrganizationConverter.markTenantTOSelected(tenantTOList, organizationrequestTO.getTenantID());
				}

				organizationResTO.setOrgAppPortalTreeResponseTO(orgAppPortalTreeResponseTO);
				organizationResTO.setTenantTOList(tenantTOList);
				try {
					String type = null;
					organizationResTO.setSocialAppSettingTOMap(
							loginServerService.getLoginServerDetail(organizationrequestTO.getOrganizationId(),
									orgAppPortalTreeResponseTO.getEmployeePortalURLID()));
					for (Entry<String, SocialAppSettingTO> appEntry : loginServerService
							.getLoginServerDetail(organizationrequestTO.getOrganizationId(),
									orgAppPortalTreeResponseTO.getEmployeePortalURLID())
							.entrySet()) {
						String key = appEntry.getKey();
						SocialAppSettingTO appSettingTO = appEntry.getValue();
						String arr[] = key.split(CoreConstants.AT_SPERATOR);
						type = arr[0];
						if (type.equalsIgnoreCase(CoreConstants.LOGIN_SERVER_SP)) {
							organizationResTO.setSpServer(appSettingTO.getServerName());
							organizationResTO.setSpLogoutUrl(appSettingTO.getLogoutServerUrl());
							organizationResTO.setSpActive(appSettingTO.isActive());
						}
						if (type.equalsIgnoreCase(CoreConstants.LOGIN_SERVER_LDAP)) {
							organizationResTO.setLdapLogoutUrl(appSettingTO.getLogoutServerUrl());

						}

					}

				} catch (Exception e) {
					UTIL_LOGGER.severe(e.getMessage());
				}

				serviceResponse = new ServiceResponse();

				serviceResponse.setResponseTransportObject(organizationResTO);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception();
		}
		return serviceResponse;
	}

	/* code for retrieving modules from orgnizationCode */

	public ServiceResponse getAllModulesFromAppID(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		TpModuleRequestTO tpModuleRequestTO = null;
		List<TpModuleRequestTO> tpModuleRequestTOList = null;
		TpModuleResponseTO tpModuleResponseTO = null;
		AppListRequestTO appListRequestTO = null;

		try {

			appListRequestTO = (AppListRequestTO) serviceRequest.getRequestTansportObject();

			List<Object[]> objectlist = appModuleDS.getAltModulesFromAppId(appListRequestTO.getApprequestList());

			if (objectlist != null && objectlist.size() > 0) {
				tpModuleRequestTOList = OrganizationConverter.convertModuleList(objectlist);
			} else {

				tpModuleRequestTOList = OrganizationConverter
						.convertModuleList(appModuleDS.getModulesFromAppId(appListRequestTO.getApprequestList()));
			}
			tpModuleResponseTO = new TpModuleResponseTO();
			tpModuleResponseTO.setTpModuleRequestTOList(tpModuleRequestTOList);

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(tpModuleResponseTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	/* method to save Organization module */

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void saveTpOrgModule(OrganizationrequestTO organizationrequestTO, Long OrganizationId) throws Exception {
		OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = organizationrequestTO.getOrgAppPortalTreeResponseTO();
		List<AppResponseTO> appList = orgAppPortalTreeResponseTO.getAppList();
		Map<Long, List<TpModuleResponseTO>> appModuleMap = orgAppPortalTreeResponseTO.getAppModuleMap();
		Map<Long, List<AppPortalTypeUrlResponseTO>> appPortalUrlMap = orgAppPortalTreeResponseTO.getAppPortalUrlMap();
		HrModule tpOrgModule = null;
		// SysModule tpModule = null;

		for (AppResponseTO arTO : appList) {
			String database = arTO.getDatabase();
			if (!database.isEmpty()) {
				List<AppPortalTypeUrlResponseTO> portalList = appPortalUrlMap.get(arTO.getAppId());
				boolean isAnyPortalIselected = findIsAnyPortalSelected(portalList);

				if (isAnyPortalIselected) {
					List<TpModuleResponseTO> moduleList = appModuleMap.get(arTO.getAppId());
					if (!(moduleList == null || moduleList.isEmpty())) {

						for (TpModuleResponseTO to : moduleList) {
							tpOrgModule = appModuleDS.findAltModule(to.getModuleID(), OrganizationId.intValue());
							if (to.isModuleChecked()) {

								if (tpOrgModule != null) {
									tpOrgModule.setModifiedDate(new Date());
									tpOrgModule.setModifiedBy(organizationrequestTO.getModifiedBy().intValue());
									tpOrgModule.setActive(true);
									appModuleDS.mergeTpOrgModule(tpOrgModule);
								} else {
									tpOrgModule = new HrModule();
									// tpModule =
									// appModuleDS.findAltModule(to.getModuleID());
									tpOrgModule.setModifiedBy(organizationrequestTO.getCreatedBy().intValue());
									tpOrgModule.setCreatedBy(organizationrequestTO.getCreatedBy().intValue());
									// tpOrgModule.setModuleID(tpModule);
									tpOrgModule.setTenantId(organizationrequestTO.getTenantID().intValue());
									tpOrgModule.setOrganizationId(OrganizationId.intValue());
									// tpOrgModule.setEffectiveDateFrom(to.getEffectiveStartDate());
									// tpOrgModule.setEffectiveDateTO(to.getEffectiveEndDate());
									tpOrgModule.setCreatedDate(new Date());
									tpOrgModule.setModifiedDate(new Date());
									tpOrgModule.setActive(true);
									tpOrgModule.setModuleLabel(to.getModuleName());
									tpOrgModule.setModuleName(to.getModuleName());
									tpOrgModule.setSysModuleId(to.getModuleID().intValue());
									appModuleDS.persistTpOrgModule(tpOrgModule);
									// getEntityManager("TalentPact").persist(tpOrgModule);
								}
							} else {
								if (tpOrgModule != null) {
									tpOrgModule.setModifiedDate(new Date());
									tpOrgModule.setModifiedBy(organizationrequestTO.getModifiedBy().intValue());
									tpOrgModule.setActive(false);
									appModuleDS.mergeTpOrgModule(tpOrgModule);
								}
							}
						}
					}
				}
			}
		}

	}

	private boolean findIsAnyPortalSelected(List<AppPortalTypeUrlResponseTO> portalList) {
		boolean isAnySelected = false;
		if (portalList == null || portalList.isEmpty()) {
			isAnySelected = false;
		}

		for (AppPortalTypeUrlResponseTO to : portalList) {
			if (to.isPortalChecked()) {
				isAnySelected = isAnySelected || true;
			}
		}
		return isAnySelected;
	}

	public ServiceResponse getAllCheckListByOrganizationId(ServiceRequest serviceRequest) throws Exception {
		// TODO Auto-generated method stub
		ServiceResponse serviceResponse = null;
		OrganizationConverter organizationConverter = null;
		try {
			organizationConverter = new OrganizationConverter();
			CheckListResponseTO checkListResTO = null;
			CheckListRequestTO checkListRequestTO = (CheckListRequestTO) serviceRequest.getRequestTansportObject();
			List<Object[]> objlist = tPOrgDS.getCheckListByOrgId(checkListRequestTO.getOrganizationID());
			List<IResponseTransportObject> checkListResponseTOlist = (List) OrganizationConverter
					.convertInCheckListResponseToList(objlist);

			// checkListResTO = new CheckListResponseTO();
			// checkListResTO.setCheckListResponseTOList(checkListResponseTOlist);

			serviceResponse = new ServiceResponse();
			serviceResponse.setListResponseTransportObject(checkListResponseTOlist);
			return serviceResponse;

		} catch (Exception ex) {
			throw new Exception(ex);
		}

	}

	public ServiceResponse getAllSysRoleType(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<Object[]> list = null;
		List<SysRoleTypeTO> sysRoleTypeToList = null;
		SysRoleRequestTO sysRoleRequestTo = null;
		try {
			serviceResponse = new ServiceResponse();

			sysRoleRequestTo = (SysRoleRequestTO) serviceRequest.getRequestTansportObject();
			sysRoleTypeToList = new ArrayList<SysRoleTypeTO>(CacheUtil.getSysRoleTypeToMap().values());
			sysRoleRequestTo.setSysRoleTypeTolist(sysRoleTypeToList);
			sysRoleTypeToList = tPOrgDS.syncSysRoleTypeList(sysRoleRequestTo, serviceRequest.getPersistenceUnitKey());
			serviceResponse.setExtendedResponseTransportObjectList(sysRoleTypeToList);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public ServiceResponse getAllSysProductAnnouncement(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<SysProductAnnouncement> list = null;
		List<SysProductAnnouncementTO> sysProductAnnouncementTOList = null;
		List<HrProductAnnouncement> hrProductAnnouncementTOList = null;
		sysProductAnnouncementTOList = new ArrayList<SysProductAnnouncementTO>();
		try {
			serviceResponse = new ServiceResponse();
			list = productAnnouncementDS.getAllSysProductAnnouncement();

			if (list != null && !list.isEmpty()) {
				for (SysProductAnnouncement sysProductAnnouncement : list) {
					SysProductAnnouncementTO sysProductAnnouncementTO = new SysProductAnnouncementTO();
					sysProductAnnouncementTO
							.setSysProductAnnouncementID(sysProductAnnouncement.getSysProductAnnouncementID());
					sysProductAnnouncementTO
							.setSysProductAnnouncement(sysProductAnnouncement.getSysProductAnnouncement());
					sysProductAnnouncementTO
							.setSysProductAnnouncementLabel(sysProductAnnouncement.getSysProductAnnouncementLabel());
					sysProductAnnouncementTO.setValidFrom(sysProductAnnouncement.getValidFromid().getTheDate());
					sysProductAnnouncementTO.setValidTo(sysProductAnnouncement.getValidtoid().getTheDate());
					sysProductAnnouncementTOList.add(sysProductAnnouncementTO);
				}
			}

			hrProductAnnouncementTOList = getAllHrProductAnnouncement(serviceRequest);

			if (hrProductAnnouncementTOList != null && !hrProductAnnouncementTOList.isEmpty()) {
				if (sysProductAnnouncementTOList != null && !sysProductAnnouncementTOList.isEmpty()) {
					for (SysProductAnnouncementTO sysProductAnnouncementTO : sysProductAnnouncementTOList) {
						for (HrProductAnnouncement hrProductAnnouncement : hrProductAnnouncementTOList) {
							if (hrProductAnnouncement.getSysProductAnnouncement()
									.equals(sysProductAnnouncementTO.getSysProductAnnouncementID())) {
								sysProductAnnouncementTO.setSelected(true);
								sysProductAnnouncementTO
										.setValidFrom(hrProductAnnouncement.getValidFromID().getTheDate());
								sysProductAnnouncementTO.setValidTo(hrProductAnnouncement.getValidToID().getTheDate());
								break;
							}
						}
					}
				}
			}
			serviceResponse.setExtendedResponseTransportObjectList(sysProductAnnouncementTOList);
			return serviceResponse;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public List<HrProductAnnouncement> getAllHrProductAnnouncement(ServiceRequest serviceRequest) throws Exception {
		try {
			SysProductAnnouncementRequestTO sysProductAnnouncementRequestTO = (SysProductAnnouncementRequestTO) serviceRequest
					.getRequestTansportObject();
			return productAnnouncementDS
					.getAllHrProductAnnouncement(sysProductAnnouncementRequestTO.getOrganizationID().intValue());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception(ex);
		} finally {
		}
	}

	public ServiceResponse copyHrProductAnnouncement(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		SysProductAnnouncementRequestTO sysProductAnnouncementRequestTO = null;
		try {
			sysProductAnnouncementRequestTO = (SysProductAnnouncementRequestTO) serviceRequest
					.getRequestTansportObject();
			productAnnouncementDS.copyHrProductAnnouncement(sysProductAnnouncementRequestTO);
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public ServiceResponse getOrganizationModuleData(ServiceRequest serviceRequest) throws Exception {
		List<Object[]> moduleList = null;
		List<HrModuleTO> list = null;
		HrModuleTO hrModuleTO = null;
		ServiceResponse response = null;
		try {
			response = new ServiceResponse();
			HrModuleRequestTO organizationrequestTO = (HrModuleRequestTO) serviceRequest.getRequestTansportObject();
			moduleList = formSecurityDS.getOrganizationModuleData(organizationrequestTO.getOrganizationID());
			list = new ArrayList<HrModuleTO>();
			for (Object[] moduleData : moduleList) {
				hrModuleTO = new HrModuleTO();
				hrModuleTO.setModuleID((int) moduleData[0]);
				hrModuleTO.setModuleName((String) moduleData[1]);
				list.add(hrModuleTO);
			}
			response.setResponseTransferObjectObjectList((List) list);
			return response;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public ServiceResponse getAllSysMenu(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<SysMenuTO> sysMenuToList = null;
		SysMenuRequestTO sysMenuRequestTo = null;
		try {
			serviceResponse = new ServiceResponse();
			sysMenuRequestTo = (SysMenuRequestTO) serviceRequest.getRequestTansportObject();
			sysMenuToList = new ArrayList<SysMenuTO>(startupService.setAllSysMenuToList());
			sysMenuRequestTo.setSysMenuTOList(sysMenuToList);
			sysMenuToList = sysMenuDS.syncMenuList(sysMenuRequestTo);
			serviceResponse.setExtendedResponseTransportObjectList(sysMenuToList);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {

			serviceResponse = null;
		}

	}

	public ServiceResponse updateAltCheckList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<Object[]> list = null;
		try {
			CopyCheckListRequestTO reqTO = null;
			reqTO = (CopyCheckListRequestTO) serviceRequest.getRequestTansportObject();
			tPOrgDS.updateAltCheckList(reqTO);

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public ServiceResponse getAllSysContentType(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<Object[]> list = null;
		try {
			ContentListRequestTO contentListRequestTO = (ContentListRequestTO) serviceRequest
					.getRequestTansportObject();
			List<Object[]> objlist = tPOrgDS.getAllSysContentType(contentListRequestTO.getOrganizationID());

			List<IResponseTransportObject> contentListResponseTOlist = (List) OrganizationConverter
					.convertInSysContentTypeResponseToList(objlist);

			serviceResponse = new ServiceResponse();
			serviceResponse.setExtendedResponseTransportObjectList(contentListResponseTOlist);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}

	}

	public ServiceResponse copySysRoleList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		SysRoleRequestTO reqTO = null;
		try {
			reqTO = (SysRoleRequestTO) serviceRequest.getRequestTansportObject();
			tPOrgDS.copySysRoleList(reqTO, serviceRequest.getPersistenceUnitKey());
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public ServiceResponse copySysMenuList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		SysMenuRequestTO reqTO = null;
		try {
			reqTO = (SysMenuRequestTO) serviceRequest.getRequestTansportObject();
			sysMenuDS.copySysMenuList(reqTO);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}

	}

	public List<UserResponseTO> getUserByTenant(ServiceRequest serviceRequest) throws Exception {
		UserRequestTO usrReqTO = null;
		List<UserResponseTO> userList = null;
		try {
			usrReqTO = (UserRequestTO) serviceRequest.getRequestTansportObject();
			userList = tpUserDS.getUserByTenant(usrReqTO);
			return userList;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			userList = null;
		}

	}

	public ServiceResponse getAllUIForm(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		List<Object[]> list = null;
		try {
			UIFormRequestTO uIListRequestTO = (UIFormRequestTO) serviceRequest.getRequestTansportObject();
			List<Object[]> objlist = tPOrgDS.getAllUIForm(uIListRequestTO.getOrganizationID(),
					uIListRequestTO.getTenantID(), uIListRequestTO.getModuleIDList());

			List<IResponseTransportObject> uIFormListResponseTOlist = (List) OrganizationConverter
					.convertInSysUiFormResponseToList(objlist);

			serviceResponse = new ServiceResponse();
			serviceResponse.setExtendedResponseTransportObjectList(uIFormListResponseTOlist);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}

	}

	public ServiceResponse copyUIFormList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		UIFormRequestTO reqTO = null;
		try {
			reqTO = (UIFormRequestTO) serviceRequest.getRequestTansportObject();
			tPOrgDS.copyUIFormList(reqTO, serviceRequest.getPersistenceUnitKey());
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public ServiceResponse setUserListByOrganization(ServiceRequest serviceRequest) throws Exception {
		TpUserCreatedResponseTO tpUserCreatedResponseTO = null;
		ServiceResponse serviceResponse = null;
		UserRequestTO usrReqTO = null;
		try {
			usrReqTO = (UserRequestTO) serviceRequest.getRequestTansportObject();
			tpUserCreatedResponseTO = tpUserDS.setUserListByOrganization(usrReqTO);
			if (tpUserCreatedResponseTO != null) {
				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObject(tpUserCreatedResponseTO);
			}
		} catch (Exception ex) {
			serviceResponse = null;
		}
		return serviceResponse;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public ServiceResponse getAllOfferingTemplateList(ServiceRequest serviceRequest) throws Exception {
		AltBenefitRequestTO altBenefitRequestTO = null;
		ServiceResponse serviceResponse = null;
		AltBenefitResponseTO benefitResponseTO = null;
		try {
			altBenefitRequestTO = (AltBenefitRequestTO) serviceRequest.getRequestTansportObject();

			try {
				benefitResponseTO = altBenefitsCache.getVendorOfferingsTemplateCheckList();
				if (benefitResponseTO == null) {
					throw new Exception(AltBenefitsKeyConstants.NO_DATA_IN_REDIS);
				}
			} catch (Exception ex) {
				try {
					benefitResponseTO = getAllOfferingTemplateListFromDB(altBenefitRequestTO);
					if (benefitResponseTO != null) {
						try {
							altBenefitsCache.putVendorOfferingsTemplateCheckList(benefitResponseTO);
						} catch (Exception ex1) {
							throw ex1;
						}
					}
				} catch (Exception e) {
					throw e;
				}
			}
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(benefitResponseTO);

		} catch (Exception e) {
			throw e;
		}
		return serviceResponse;

	}

	/**
	 * 
	 * @param serviceRequest
	 * @return
	 */
	public AltBenefitResponseTO getAllOfferingTemplateListFromDB(AltBenefitRequestTO altBenefitRequestTO) {
		AltBenefitResponseTO benefitResponseTO = null;
		List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
		List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
		try {
			vendorOfferingTemplateTOList = sysVendorOfferingTemplateService.getOfferingTemplateList();
			hrVendorOfferingTemplateList = hrVendorOfferingTemplateServiceDS.getHrOfferingTemplateList(
					altBenefitRequestTO.getOrganizationID(), altBenefitRequestTO.getTenantID());
			if (hrVendorOfferingTemplateList != null && !hrVendorOfferingTemplateList.isEmpty()) {
				if (vendorOfferingTemplateTOList != null && !vendorOfferingTemplateTOList.isEmpty()) {
					for (VendorOfferingTemplateTO offeringTemplateTO : vendorOfferingTemplateTOList) {
						for (HrVendorOfferingTemplate hrVendorOfferingTemplate : hrVendorOfferingTemplateList) {
							if (hrVendorOfferingTemplate.getVendorOfferingID()
									.equals(offeringTemplateTO.getVendorOfferingID())) {
								offeringTemplateTO.setSelected(true);
								offeringTemplateTO
										.setHrVendorOfferingID(hrVendorOfferingTemplate.getHrVendorOfferingID());
								break;
							}
						}
					}
				}
			}
			benefitResponseTO = new AltBenefitResponseTO();
			benefitResponseTO.setVendorOfferingTemplateTOList(vendorOfferingTemplateTOList);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return benefitResponseTO;
	}

	public ServiceResponse getOrgMoreInfoListByAppIdOrgIdTenantID(ServiceRequest serviceRequest) {
		ServiceResponse serviceResponse = null;
		OrganizationrequestTO organizationrequestTO = null;
		List<OrgMoreInfoResponseTO> orgMoreInforequestTOList = null;
		OrgMoreInfoResponseTO orgMoreInfoResponseTO = null;
		try {
			organizationrequestTO = (OrganizationrequestTO) serviceRequest.getRequestTansportObject();
			List<Object[]> orgList = tPOrgDS.getOrgMoreInfoListByAppIdOrgIdTenantID(organizationrequestTO);
			orgMoreInforequestTOList = OrganizationConverter.getMoreInfoList(orgList);
			orgMoreInfoResponseTO = new OrgMoreInfoResponseTO();
			orgMoreInfoResponseTO.setOrganizationMOreInfoResponseTOList(orgMoreInforequestTOList);

			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(orgMoreInfoResponseTO);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return serviceResponse;
	}

	public List<SysBundleResponseTO> getSysBundle() throws Exception {
		List<SysBundleResponseTO> retlist = null;
		try {
			List<Object[]> bundleList = tPOrgDS.getSysBundle();
			if (bundleList != null) {
				retlist = new ArrayList<SysBundleResponseTO>();
				for (Object[] obj : bundleList) {
					Integer bundleId = obj[0] != null ? (Integer) obj[0] : null;
					String bundleName = obj[1] != null ? (String) obj[1] : null;
					if (bundleId == null || bundleName == null) {
						throw new Exception("Inconsistent Data : either bundleId or bundleName ");
					}
					SysBundleResponseTO o = new SysBundleResponseTO();
					o.setBundleId(bundleId);
					o.setName(bundleName);
					retlist.add(o);
				}
			}
			return retlist;
		} catch (Exception e) {
			throw e;
		}
	}

	public ServiceResponse copyAllOfferingTemplateList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		AltBenefitRequestTO altBenefitRequestTO = null;
		try {
			altBenefitRequestTO = (AltBenefitRequestTO) serviceRequest.getRequestTansportObject();
			hrVendorOfferingTemplateServiceDS.copyAllOfferingTemplateList(altBenefitRequestTO,
					serviceRequest.getPersistenceUnitKey());
			altBenefitsCache.deleteHrOfferingTemplateListByOrgIDRedisKey();
			altBenefitsCache.deleteSystemVendorOfferingsTemplatesByOrgRedisKey();
			altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();
			altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
			altBenefitsCache.deleteSystemVendorOfferingsList();
			altBenefitsCache.deleteHrVendorOfferingsTemplatesByOrgRedisKey(altBenefitRequestTO.getOrganizationID());
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public List<Integer> getOrgBundle(ServiceRequest serviceRequest) throws Exception {
		int orgId = ((OrganizationrequestTO) serviceRequest.getRequestTansportObject()).getOrganizationId().intValue();
		return tPOrgDS.getOrgBundle(orgId);
	}

	public List<AddressResponseTO> getCountryDropDown() {

		return tPOrgDS.getCountryDropDown();
	}

	public List<AddressResponseTO> getStateDropDown(String country) {

		return tPOrgDS.getStateDropDown(country);
	}

	public List<AddressResponseTO> getDistrictDropDown(String state) {

		return tPOrgDS.getDistrictDropDown(state);
	}

	public List<AddressResponseTO> getCityDropDown(String district) {

		return tPOrgDS.getCityDropDown(district);
	}

	public List<AddressResponseTO> getZipCodeDropDown(Integer cityID) {

		return tPOrgDS.getZipCodeDropDown(cityID);
	}

	public List<SectorCostResponseTO> getSectorDropDown() {

		return tPOrgDS.getSectorDropDown();
	}

	public List<FinancialYearResponseTO> getFinancialYearList() {

		return tPOrgDS.getFinancialYearList();
	}

	public List<FinancialYearResponseTO> getGovtFinancialYearList() {

		return tPOrgDS.getGovtFinancialYearList();
	}

	public List<SysSubModuleResponseTO> getModuleListDropDown() {

		return tPOrgDS.getModuleListDropDown();
	}

	public void saveOrgWebPayStatus(String orgCode, boolean status) {

		tPOrgDS.saveOrgWebPayStatus(orgCode, status);
	}

	/**
	 * 
	 * @param sectorCode
	 * @return
	 * @throws Exception
	 */
	public List<OrganizationTO> getTpOrganizationList(Integer sectorId) throws Exception {
		List<Object[]> tpOrganizationObjectList = null;
		List<OrganizationTO> tpOrganizationTOList = null;
		OrganizationTO organizationTO = null;
		try {
			tpOrganizationTOList = new ArrayList<OrganizationTO>();
			tpOrganizationObjectList = tPOrgDS.getAllOrganizationListBySector(sectorId);
			if (tpOrganizationObjectList != null && !tpOrganizationObjectList.isEmpty()) {
				for (Object[] ob : tpOrganizationObjectList) {
					organizationTO = new OrganizationTO();
					organizationTO.setOrgID(((Integer) ob[0]).longValue());
					organizationTO.setOrgName((String) ob[1]);
					organizationTO.setOrgCode((String) ob[2]);
					organizationTO.setSectorLabel((String) ob[3]);
					tpOrganizationTOList.add(organizationTO);
				}
			}
			return tpOrganizationTOList;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	public ServiceResponse getAllDashboardChartList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		DashboardChartResponseTO dashboardChartResponseTO = null;
		DashboardChartRequestTO chartRequestTO = null;
		try {
			chartRequestTO = (DashboardChartRequestTO) serviceRequest.getRequestTansportObject();
			dashboardChartResponseTO = dashboardChartService.getSysDashboardList(chartRequestTO.getOrganizationID(),
					chartRequestTO.getSelectedPortalTypeID());
			if (!(dashboardChartResponseTO.getSelectedPortalID() > 0))
				serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(dashboardChartResponseTO);

		} catch (Exception e) {
			throw e;
		}
		return serviceResponse;

	}

	/**
	 * 
	 * @param persistenceUnitName
	 * @return
	 * @throws Exception
	 */

	public DashboardChartResponseTO getPortalDetails() throws Exception {
		List<PortalTO> portalList = null;
		List<TpPortalType> resultList = null;
		PortalTO portalTO = null;
		DashboardChartResponseTO dashboardChartResponseTO = null;
		try {
			resultList = tpPortalTypeDS.getPortalDetails();
			portalList = new ArrayList<PortalTO>();
			dashboardChartResponseTO = new DashboardChartResponseTO();
			for (TpPortalType portal : resultList) {
				portalTO = new PortalTO();
				portalTO.setPortalID(portal.getPortalTypeID());
				portalTO.setPortalType(portal.getPortalType());
				portalList.add(portalTO);
			}
			dashboardChartResponseTO.setPortalList(portalList);
		} catch (Exception e) {
			throw new Exception();
		}
		return dashboardChartResponseTO;
	}

	public ServiceResponse copyAllDashboardChartList(ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		DashboardChartRequestTO dashboardChartRequestTO = null;
		try {
			dashboardChartRequestTO = (DashboardChartRequestTO) serviceRequest.getRequestTansportObject();
			dashboardChartService.copyAllDashboardChartList(dashboardChartRequestTO);
			serviceResponse = new ServiceResponse();
			serviceResponse.setResponseTransportObject(null);
			return serviceResponse;
		} catch (Exception ex) {
			throw new Exception(ex);
		} finally {
			serviceResponse = null;
		}
	}

	public void saveselectedPartnerApp(List<String> selectedPartnerApps, Long orgId, Long tenantId) throws Exception {
		try {
			List<HrAppTO> hrApps = new ArrayList<>();
			for (String selectedPartnerApp : selectedPartnerApps) {
				HrAppTO hrApp = new HrAppTO(Long.parseLong(selectedPartnerApp), orgId, tenantId, true);
				hrApps.add(hrApp);
			}
			hrAppService.saveHrApp(hrApps);
		} catch (Exception ex) {
			throw new Exception(ex);
		}
	}

	public void saveselectedCustomApp(List<String> selectedCustomAppList, Long orgId, Long tenantId) throws Exception {
		try {
			List<HrAppTO> hrApps = new ArrayList<>();
			for (String selectedCustomApp : selectedCustomAppList) {
				HrAppTO hrApp = new HrAppTO(Long.parseLong(selectedCustomApp), orgId, tenantId, true);
				hrApps.add(hrApp);
			}
			hrAppService.saveHrApp(hrApps);
		} catch (Exception ex) {
			throw new Exception(ex);
		}
	}
	
	public void saveSelectedApps(List<HrAppTO> createFinalAppList) throws Exception {
		try{
			hrAppService.createOrUpdateApps(createFinalAppList);
		}catch (Exception ex) {
			throw new Exception(ex);
		}
		
	}

	public List<SysContentTypeTO> getSysContentTypeDropDown() {

		return tPOrgDS.getSysContentTypeDropDown();
	}

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown() {

		return tPOrgDS.getSysConstantCategoryDropDown();
	}

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown() {

		return tPOrgDS.getSysConstantDropDown();
	}

	public List<UiFormTO> getLandingPageList(Long orgID) {
		// TODO Auto-generated method stub
		return tPOrgDS.getLandingPageList(orgID);
	}

	public List<HrAppTO> getSelectedPartnerApps(Long orgId, Long tenantID) throws Exception {
		List<HrAppTO> list = new ArrayList<>();
		try {
			list =  hrAppService.findAllAppForOrg(orgId, tenantID, TpAppConstants.TP_APP_TYPE_PARTNER);
		} catch (Exception ex) {
			UTIL_LOGGER.severe(ex.getMessage());
		}
		return list;
	}

	public List<HrAppTO> getSelectedCustomApps(Long orgId, Long tenantID) throws Exception {
		List<HrAppTO> list = new ArrayList<>();
		try {
			list =  hrAppService.findAllAppForOrg(orgId, tenantID, TpAppConstants.TP_APP_TYPE_CUSTOM);
		} catch (Exception ex) {
			UTIL_LOGGER.severe(ex.getMessage());
		}
		return list;
	}

}
