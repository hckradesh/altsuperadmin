/**
 * 
 */
package com.talentpact.business.dataservice.SysContentCategory;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.configuration.SysContentCategory;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysContentCategoryDS extends AbstractDS<SysContentCategory>
{
    public SysContentCategoryDS()
    {
        super(SysContentCategory.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysContentCategoryList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select categoryId, categoryName from SysContentCategory order by categoryName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysContentCategory(SysContentCategoryTO sysContentCategoryTO)
    {
        SysContentCategory sysContentCategory = new SysContentCategory();
        try {
            sysContentCategory.setCategoryName(sysContentCategoryTO.getCategoryName());
            sysContentCategory.setSysTenantId(1);
            getEntityManager("TalentPact").persist(sysContentCategory);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean editSysContentCategory(SysContentCategoryTO sysContentCategoryTO)
    {
        SysContentCategory sysContentCategory = new SysContentCategory();
        try {
            sysContentCategory = findSysContentCategory(sysContentCategoryTO);
            sysContentCategory.setCategoryName(sysContentCategoryTO.getCategoryName());
            sysContentCategory.setSysTenantId(1);
            getEntityManager("TalentPact").merge(sysContentCategory);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysContentCategory findSysContentCategory(SysContentCategoryTO sysContentCategoryTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysContentCategory> sysContentCategoryList = null;
        SysContentCategory result = null;
        int tenantID = 1;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select scc from SysContentCategory scc  where scc.categoryId=:categoryId and scc.sysTenantId = :sysTenant ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("categoryId", sysContentCategoryTO.getContentCategoryID());
            query.setParameter("sysTenant", tenantID);
            sysContentCategoryList = query.getResultList();
            if (sysContentCategoryList != null && sysContentCategoryList.size() > 0) {
                result = sysContentCategoryList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
}