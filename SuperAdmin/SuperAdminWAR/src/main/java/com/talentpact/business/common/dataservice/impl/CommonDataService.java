package com.talentpact.business.common.dataservice.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.talentpact.business.common.dataservice.IDataSerivce;

public class CommonDataService implements IDataSerivce
{
    @PersistenceContext(unitName = "TalentPactAdminPortal")
    private EntityManager entityManager;

    public EntityManager getEntityManager()
    {
        return entityManager;
    }

    public EntityManager getEntityManager(String unitName)
    {
        return entityManager;
    }


}
