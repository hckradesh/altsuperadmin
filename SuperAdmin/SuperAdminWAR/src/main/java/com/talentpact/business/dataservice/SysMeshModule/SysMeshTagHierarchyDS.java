/**
 * 
 */
package com.talentpact.business.dataservice.SysMeshModule;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagHierarchyTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysMeshModule;
import com.talentpact.model.SysMeshTagHeirarchy;
import com.talentpact.model.altbenefit.SysOfferingCategory;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysMeshTagHierarchyDS extends AbstractDS<SysOfferingCategory>
{
    public SysMeshTagHierarchyDS()
    {
        super(SysOfferingCategory.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysMeshTagHierarchyList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select SysTagID, SysMeshModuleID, ParentID, TagName, DependentSysTagID, IsMandatory, IsUniqueSourceCodeNode, isDependentUponItself, IsInboundSupported, IsOutboundSupported, TagHierarchy, ResourceTypeId   from AltDataManager..SysMeshTagHeirarchy order by TagHierarchy");
            query = getEntityManager("AltDataManager").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    public List<Object[]> getSysMeshModuleList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysMeshModule.SysMeshModuleID, sysMeshModule.SysMeshModuleName from altdatamanager..SysMeshModule sysMeshModule  order by sysMeshModule.SysMeshModuleName");
            query = getEntityManager("AltDataManager").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean saveSysMeshTagHierarchy(SysMeshTagHierarchyTO sysMeshTagHierarchyTO)
    {
        SysMeshTagHeirarchy sysMeshTagHierarchy = new SysMeshTagHeirarchy();
        try {
            sysMeshTagHierarchy.setTagName(sysMeshTagHierarchyTO.getTagName());
            sysMeshTagHierarchy.setTagHierarchy(sysMeshTagHierarchyTO.getTagHierarchy());
            sysMeshTagHierarchy.setDataType(sysMeshTagHierarchyTO.getDataType());
            sysMeshTagHierarchy.setMandatory(sysMeshTagHierarchyTO.isMandatory());
            sysMeshTagHierarchy.setUniqueSourceCodeNode(sysMeshTagHierarchyTO.isUniqueSourceCodeNode());
            sysMeshTagHierarchy.setDependentUponItself(sysMeshTagHierarchyTO.isDependentUponItself());
            sysMeshTagHierarchy.setInboundSupported(sysMeshTagHierarchyTO.isInboundSupported());
            sysMeshTagHierarchy.setOutboundSupported(sysMeshTagHierarchyTO.isOutboundSupported());
            sysMeshTagHierarchy.setSysMeshModule(findSysMeshModule(sysMeshTagHierarchyTO.getModuleID()));
            sysMeshTagHierarchy.setParentID(sysMeshTagHierarchyTO.getParentID());
            sysMeshTagHierarchy.setResourceTypeId(sysMeshTagHierarchyTO.getResourceTypeID());
            sysMeshTagHierarchy.setSysMeshModuleID(sysMeshTagHierarchyTO.getModuleID());
            if(sysMeshTagHierarchyTO.getDependentTagID() !=0)
            {
           	 sysMeshTagHierarchy.setDependentSysTagID(sysMeshTagHierarchyTO.getDependentTagID());
            }
            sysMeshTagHierarchy.setCreatedBy(1);
            sysMeshTagHierarchy.setModifiedBy(1);
            sysMeshTagHierarchy.setOrganizationId(1);
            sysMeshTagHierarchy.setTenandId(1);
            sysMeshTagHierarchy.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysMeshTagHierarchy.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("AltDataManager").persist(sysMeshTagHierarchy);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void updateSysMeshTagHierarchy(SysMeshTagHierarchyTO sysMeshTagHierarchyTO) 
	    {
    	SysMeshTagHeirarchy sysMeshTagHierarchy = null;
    	List<SysMeshTagHeirarchy> sysMeshTagHeirarchyList = null;
	        try {
	        	sysMeshTagHierarchy = findSysMeshTagHierarchy(sysMeshTagHierarchyTO.getSysTagID());
	        	 sysMeshTagHierarchy.setTagName(sysMeshTagHierarchyTO.getTagName());
	        	 String oldHierarchy = sysMeshTagHierarchy.getTagHierarchy();
	        	 sysMeshTagHeirarchyList = getChildHierarchyList(sysMeshTagHierarchy.getTagHierarchy());
	        	 if(sysMeshTagHeirarchyList != null)
	        	 {
	        		 String hierarchy = null;
	        		for (SysMeshTagHeirarchy sysMeshTagHeirarchy1 : sysMeshTagHeirarchyList) {
	        			hierarchy = sysMeshTagHeirarchy1.getTagHierarchy();
	        			if(hierarchy != null)
	        			{
	        				hierarchy = hierarchy.replaceFirst(oldHierarchy, sysMeshTagHierarchyTO.getTagHierarchy());
	        				sysMeshTagHeirarchy1.setTagHierarchy(hierarchy);
	        				sysMeshTagHeirarchy1.setModifiedDate(new Date());
	        				getEntityManager("AltDataManager").merge(sysMeshTagHeirarchy1);
	        			}
					}
	        	 }
	             sysMeshTagHierarchy.setTagHierarchy(sysMeshTagHierarchyTO.getTagHierarchy());
	             sysMeshTagHierarchy.setDataType(sysMeshTagHierarchyTO.getDataType());
	             sysMeshTagHierarchy.setMandatory(sysMeshTagHierarchyTO.isMandatory());
	             sysMeshTagHierarchy.setUniqueSourceCodeNode(sysMeshTagHierarchyTO.isUniqueSourceCodeNode());
	             sysMeshTagHierarchy.setDependentUponItself(sysMeshTagHierarchyTO.isDependentUponItself());
	             sysMeshTagHierarchy.setInboundSupported(sysMeshTagHierarchyTO.isInboundSupported());
	             sysMeshTagHierarchy.setOutboundSupported(sysMeshTagHierarchyTO.isOutboundSupported());
	             sysMeshTagHierarchy.setSysMeshModuleID(sysMeshTagHierarchyTO.getModuleID());
	             sysMeshTagHierarchy.setParentID(sysMeshTagHierarchyTO.getParentID());
	             //sysMeshTagHierarchy.setResourceTypeId(sysMeshTagHierarchyTO.getResourceTypeID());
	             if(sysMeshTagHierarchyTO.getDependentTagID() !=0)
	             {
	            	 sysMeshTagHierarchy.setDependentSysTagID(sysMeshTagHierarchyTO.getDependentTagID());
	             }
	             sysMeshTagHierarchy.setModifiedBy(1);
	             sysMeshTagHierarchy.setModifiedDate(new Date());
	            getEntityManager("AltDataManager").merge(sysMeshTagHierarchy);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	    }
	
	    
	    private List<SysMeshTagHeirarchy> getChildHierarchyList(String tagHierarchy) {
	    	Query query = null;
	        StringBuilder hqlQuery = null;
	        List<SysMeshTagHeirarchy> sysMeshTagHeirarchyList = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select smm from SysMeshTagHeirarchy smm  where smm.tagHierarchy  like :tagHierarchy ");
	            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
	            query.setParameter("tagHierarchy", tagHierarchy + "%");
	            sysMeshTagHeirarchyList = query.getResultList();
	            return sysMeshTagHeirarchyList;
	        } catch (Exception ex) {
	            throw ex;
	        } finally {
	            hqlQuery = null;
	            query = null;
	        }
	}

		@SuppressWarnings("unchecked")
	    private SysMeshModule findSysMeshModule(Integer sysMeshModuleID)
	        throws Exception
	    {
	        Query query = null;
	        StringBuilder hqlQuery = null;
	        List<SysMeshModule> sysMeshTagHeirarchyTO = null;
	        SysMeshModule result = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select smm from SysMeshModule smm  where smm.sysMeshModuleID=:sysMeshModuleID ");
	            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
	            query.setParameter("sysMeshModuleID", sysMeshModuleID);
	            sysMeshTagHeirarchyTO = query.getResultList();
	            if (sysMeshTagHeirarchyTO != null && sysMeshTagHeirarchyTO.size() > 0) {
	                result = sysMeshTagHeirarchyTO.get(0);
	            }
	            return result;
	        } catch (Exception ex) {
	            throw ex;
	        } finally {
	            hqlQuery = null;
	            query = null;
	        }
	    }
	    
	    @SuppressWarnings("unchecked")
	    private SysMeshTagHeirarchy findSysMeshTagHierarchy(Integer sysmeshTagID)
	        throws Exception
	    {
	        Query query = null;
	        StringBuilder hqlQuery = null;
	        List<SysMeshTagHeirarchy> sysMeshTagHeirarchyTO = null;
	        SysMeshTagHeirarchy result = null;
	        try {

	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select smm from SysMeshTagHeirarchy smm  where smm.sysTagID=:sysmeshTagID ");
	            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
	            query.setParameter("sysmeshTagID", sysmeshTagID);
	            sysMeshTagHeirarchyTO = query.getResultList();
	            if (sysMeshTagHeirarchyTO != null && sysMeshTagHeirarchyTO.size() > 0) {
	                result = sysMeshTagHeirarchyTO.get(0);
	            }
	            return result;
	        } catch (Exception ex) {
	            throw ex;
	        } finally {
	            hqlQuery = null;
	            query = null;
	        }
	    }

		public List<Object[]> getHierarchyList(int moduleID, int sysTagID) {
			Query query = null;
	        StringBuilder hqlQuery = null;
	        List<Object[]> list = null;
	        try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select sysMeshTagHeirarchy.sysTagID, sysMeshTagHeirarchy.tagHierarchy from SysMeshTagHeirarchy sysMeshTagHeirarchy where sysMeshTagHeirarchy.sysTagID != :sysTagID ");
	            if(moduleID != 0)
	            {
	            	hqlQuery.append(" and sysMeshTagHeirarchy.sysMeshModule.sysMeshModuleID = :moduleID");
	            }
	            hqlQuery.append(" order by sysMeshTagHeirarchy.tagHierarchy");
	            query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
	            if(moduleID != 0)
	            {
	            	query.setParameter("moduleID", moduleID);
	            }
	            query.setParameter("sysTagID", sysTagID);
	            list = query.getResultList();
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return list;
		}

		public boolean validateSysMeshTagHierarchy(SysMeshTagHierarchyTO sysMeshTagHierarchyTO) {
	        Query query = null;
	        StringBuilder hqlQuery = null;
	        List<Object[]> list = null;
	        try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select * from altdatamanager..SysMeshTagHeirarchy sysMeshTagHierarchy ");
	            if(sysMeshTagHierarchyTO.getSysTagID() != 0)
	            {
	            	hqlQuery.append(" where  sysMeshTagHierarchy.SysTagID != :SysTagID and sysMeshTagHierarchy.TagHierarchy = :TagHierarchy");
	            }
	            else
	            {
	            	hqlQuery.append(" where  sysMeshTagHierarchy.TagHierarchy = :TagHierarchy ");
	            }
	            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
	            if(sysMeshTagHierarchyTO.getSysTagID() != 0)
	            {
	            	query.setParameter("SysTagID", sysMeshTagHierarchyTO.getSysTagID());
	            	query.setParameter("TagHierarchy", sysMeshTagHierarchyTO.getTagHierarchy());
	            }
	            else
	            {
	            	query.setParameter("TagHierarchy", sysMeshTagHierarchyTO.getTagHierarchy());
	            }
	            list = query.getResultList();
	            if(list != null && list.size() > 0)
	            {
	            	return true;
	            }
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return false;
	    }

		public List<Object[]> getSysEntityList() {
			Query query = null;
	        StringBuilder hqlQuery = null;
	        List<Object[]> list = null;
	        try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select sysEntity.entityId, sysEntity.name from SysEntity sysEntity ");
	            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
	            list = query.getResultList();
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return list;
		}
		
		public List<Object[]> getSysEntityColumnList(int entityID) {
			Query query = null;
	        StringBuilder hqlQuery = null;
	        List<Object[]> list = null;
	        try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append("select sysEntityColumn.entityColumnID, sysEntityColumn.entityColumnName from SysEntityColumn sysEntityColumn where sysEntityColumn.sysEntityID=:entityID");
	            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
	            query.setParameter("entityID", entityID);
	            list = query.getResultList();
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return list;
		}
}