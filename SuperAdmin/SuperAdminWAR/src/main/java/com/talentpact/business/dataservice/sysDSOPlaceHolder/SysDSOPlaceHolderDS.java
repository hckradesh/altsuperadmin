package com.talentpact.business.dataservice.sysDSOPlaceHolder;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysDSOPlaceholders;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;

@Stateless
public class SysDSOPlaceHolderDS extends AbstractDS<SysDSOPlaceholders>{

	@Inject
    UserSessionBean userSessionBean;
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysDSOPlaceHolderDS.class);
	
	public SysDSOPlaceHolderDS() {
		super(SysDSOPlaceholders.class);
		// TODO Auto-generated constructor stub
	}

	public List<SysDSOPlaceholders> getDSOPlaceHolderList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDSOPlaceholders dph");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }
	
	public void saveDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO) {
		SysDSOPlaceholders sysDSOPlaceholders=null;
        try{
        	sysDSOPlaceholders=new SysDSOPlaceholders();
        	sysDSOPlaceholders.setLabel(dsoPlaceHolderTO.getPlaceholderLabel());
        	sysDSOPlaceholders.setPlaceholderType(dsoPlaceHolderTO.getPlaceholderType());
        	sysDSOPlaceholders.setPlaceholderValue(dsoPlaceHolderTO.getPlaceholderValue());
        	sysDSOPlaceholders.setIsActive(dsoPlaceHolderTO.getIsActive());
        	sysDSOPlaceholders.setCreatedBy(userSessionBean.getUserID().intValue());
        	sysDSOPlaceholders.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
        	sysDSOPlaceholders.setModifiedBy(userSessionBean.getUserID().intValue());
        	sysDSOPlaceholders.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
        	sysDSOPlaceholders.setTenantID(1);
        	getEntityManager("TalentPact").persist(sysDSOPlaceholders);
        	
            
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
	
	public void updateDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO){
        try{
        	SysDSOPlaceholders sysDSOPlaceholders=findDSOPlaceHolder(dsoPlaceHolderTO.getdSOPlaceholdersID());
        	sysDSOPlaceholders.setLabel(dsoPlaceHolderTO.getPlaceholderLabel());
        	sysDSOPlaceholders.setPlaceholderType(dsoPlaceHolderTO.getPlaceholderType());
        	sysDSOPlaceholders.setPlaceholderValue(dsoPlaceHolderTO.getPlaceholderValue());
        	sysDSOPlaceholders.setIsActive(dsoPlaceHolderTO.getIsActive());
        	sysDSOPlaceholders.setModifiedBy(userSessionBean.getUserID().intValue());
        	sysDSOPlaceholders.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
        	getEntityManager("TalentPact").merge(sysDSOPlaceholders);
            getEntityManager("TalentPact").flush();
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
	
    private SysDSOPlaceholders findDSOPlaceHolder(Integer dsoPlaceHolderID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysDSOPlaceholders> list=null;
        SysDSOPlaceholders sysDSOPlaceholders=null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDSOPlaceholders sysDSOPlaceholders where sysDSOPlaceholders.dSOPlaceholdersID=:dsoPlaceHolderID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("dsoPlaceHolderID", dsoPlaceHolderID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
            	sysDSOPlaceholders = list.get(0);
            }
            return sysDSOPlaceholders;
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
        
    }
    
	@SuppressWarnings("unchecked")
	public List<SysDSOPlaceholders> getUpdateSysFieldType(Integer dsoPlaceHolderID){
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDSOPlaceholders sdph where sdph.dSOPlaceholdersID!=:dsoPlaceHolderID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("dsoPlaceHolderID", dsoPlaceHolderID);
            return query.getResultList();
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
    }
	
	
	public List<SysDSOPlaceholders> getEmailTypeDSOPlaceHolderList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDSOPlaceholders dph where dph.placeholderType = 'EMAIL'");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }
	
	public List<SysDSOPlaceholders> getTextTypeDSOPlaceHolderList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDSOPlaceholders dph where dph.placeholderType = 'TEXT'");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }
}
