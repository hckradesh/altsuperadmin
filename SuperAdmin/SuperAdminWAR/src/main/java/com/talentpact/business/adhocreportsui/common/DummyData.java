package com.talentpact.business.adhocreportsui.common;

public class DummyData {

	String int1;
	
	String string1;
	
	String date1;
	
	String datetime1;
	
	String bigint1;
	
	String bit1;
	
	String float1;

	public String getInt1() {
		return int1;
	}

	public void setInt1(String int1) {
		this.int1 = int1;
	}

	public String getString1() {
		return string1;
	}

	public void setString1(String string1) {
		this.string1 = string1;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDatetime1() {
		return datetime1;
	}

	public void setDatetime1(String datetime1) {
		this.datetime1 = datetime1;
	}

	public String getBigint1() {
		return bigint1;
	}

	public void setBigint1(String bigint1) {
		this.bigint1 = bigint1;
	}

	public String getBit1() {
		return bit1;
	}

	public void setBit1(String bit1) {
		this.bit1 = bit1;
	}

	public String getFloat1() {
		return float1;
	}

	public void setFloat1(String float1) {
		this.float1 = float1;
	}
	
	
}
