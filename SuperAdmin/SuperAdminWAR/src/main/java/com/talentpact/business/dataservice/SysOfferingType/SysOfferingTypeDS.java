/**
 * 
 */
package com.talentpact.business.dataservice.SysOfferingType;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysOfferingType;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysOfferingTypeDS extends AbstractDS<SysOfferingType>
{
    public SysOfferingTypeDS()
    {
        super(SysOfferingType.class);
    }

    public List<Object[]> getSysOfferingTypeList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysOfferingType.offeringTypeID, sysOfferingType.offeringType, sysOfferingType.description, sysOfferingType.offeringCategoryID.offeringCategoryID, "
                    + " sysOfferingType.offeringCategoryID.offeringCategory from SysOfferingType sysOfferingType "
                    + " order by sysOfferingType.offeringCategoryID.offeringCategory, sysOfferingType.offeringType ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getNewSysOfferingCategoryList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct soc.offeringCategoryID, soc.offeringCategory from SysOfferingCategory soc order by soc.offeringCategory");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            return resultList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
    {
        SysOfferingType sysOfferingType = new SysOfferingType();
        try {
            sysOfferingType.setOfferingCategory(sysOfferingTypeTO.getOfferingCategoryID());
            sysOfferingType.setOfferingType(sysOfferingTypeTO.getOfferingType());
            sysOfferingType.setDescription(sysOfferingTypeTO.getDescription());
            sysOfferingType.setCreatedBy(1);
            sysOfferingType.setModifiedBy(1);
            sysOfferingType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysOfferingType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());

            getEntityManager("TalentPact").persist(sysOfferingType);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean saveSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
    {
        SysOfferingType sysOfferingType = null;
        try {
            sysOfferingType = new SysOfferingType();
            sysOfferingType = findSysOfferingType(sysOfferingTypeTO);

            sysOfferingType.setOfferingCategory(sysOfferingTypeTO.getOfferingCategoryID());
            sysOfferingType.setDescription(sysOfferingTypeTO.getDescription());
            sysOfferingType.setOfferingType(sysOfferingTypeTO.getOfferingType());
            sysOfferingType.setModifiedBy(1);
            sysOfferingType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").merge(sysOfferingType);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysOfferingType findSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysOfferingType> sysOfferingTypeList = null;
        SysOfferingType result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sot from SysOfferingType sot  where sot.offeringTypeID=:offeringTypeID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("offeringTypeID", sysOfferingTypeTO.getOfferingTypeID());
            sysOfferingTypeList = query.getResultList();
            if (sysOfferingTypeList != null && sysOfferingTypeList.size() > 0) {
                result = sysOfferingTypeList.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
}