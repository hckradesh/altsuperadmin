package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

 
public interface IMeshRequestTypeConstants  
{   
    public static String MESHREQUESTTYPE_ADD_DIALOG_HIDE = "PF('meshRequestTypeAddModal').hide();";

    public static String MESHREQUESTTYPE_EDIT_DIALOG_HIDE = "PF('meshRequestTypeEditModal').hide();";

    public static String MESHREQUESTTYPE_SAVE_ERROR_MSG = "Error occured while saving MeshRequestType.";

    public static String MESHREQUESTTYPE_UPDATE_ERROR_MSG = "Error occured while updating MeshRequestType.";

    public static String MESHREQUESTTYPE_SAVE_SUCCESS_MSG = "MeshRequestType saved successfully.";

    public static String MESHREQUESTTYPE_UPDATE_SUCCESS_MSG = "MeshRequestType updated successfully.";

    public static String MESHREQUESTTYPE_DATATABLE_RESET = "PF('meshRequestTypeDataTable').clearFilters()";

    public static String MESHREQUESTTYPE_PAGINATION_DATATABLE_RESET = "PF('meshRequestTypeDataTable').getPaginator().setPage(0);";

    public static String MESHREQUESTTYPE_DUPLICATE_ERROR_MSG = "MeshRequestType already present in system.";
}
