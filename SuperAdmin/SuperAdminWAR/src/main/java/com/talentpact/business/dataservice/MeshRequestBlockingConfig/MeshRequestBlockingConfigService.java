/**
 *
 */
package com.talentpact.business.dataservice.MeshRequestBlockingConfig;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.talentpact.business.application.transport.output.MeshRequestBlockingConfigTO;
import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.altbenefit.MeshRequestType;


@Stateless
public class MeshRequestBlockingConfigService extends CommonService
{

    @EJB
    MeshRequestBlockingConfigDS meshRequestBlockingConfigDS;

    public List<MeshRequestBlockingConfigTO> getMeshRequestBlockingConfigList()
            throws Exception
    {
        List<MeshRequestBlockingConfigTO> list = null;
        MeshRequestBlockingConfigTO meshRequestBlockingConfigTO = null;
        List<Object[]> meshRequestBlockingConfigList = null;

        try {
            meshRequestBlockingConfigList = meshRequestBlockingConfigDS.getMeshRequestBlockingConfigList();
            list = new ArrayList<MeshRequestBlockingConfigTO>();
            for (Object[] meshRequestBlockingConfig: meshRequestBlockingConfigList) {
                meshRequestBlockingConfigTO = new MeshRequestBlockingConfigTO();
                meshRequestBlockingConfigTO.setBlockingConfigID((Integer) meshRequestBlockingConfig[0]);
                if (meshRequestBlockingConfig[1] != null)
                    meshRequestBlockingConfigTO.setRequestTypeID((Integer) meshRequestBlockingConfig[1]);
                if (meshRequestBlockingConfig[2] != null)
                    meshRequestBlockingConfigTO.setBlockingRequestTypeID((Integer) meshRequestBlockingConfig[2]);
                if (meshRequestBlockingConfig[3] != null)
                    meshRequestBlockingConfigTO.setRequestTypeName((String) meshRequestBlockingConfig[3]);
                if (meshRequestBlockingConfig[4] != null)
                    meshRequestBlockingConfigTO.setBlockingRequestTypeName((String) meshRequestBlockingConfig[4]);
                list.add(meshRequestBlockingConfigTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void addMeshRequestBlockingConfig(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO)
            throws Exception
    {
        try {
            meshRequestBlockingConfigDS.addMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editMeshRequestBlockingConfig(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO)
            throws Exception
    {
        try {
            meshRequestBlockingConfigDS.editMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<MeshRequestTypeTO> getMeshRequestTypeTOS()
            throws Exception
    {
        List<MeshRequestTypeTO> result = null;
        MeshRequestTypeTO meshRequestTypeTO = null;
        try{
            result = new ArrayList<>();
            for (MeshRequestType meshRequestType : meshRequestBlockingConfigDS.getMeshRequestTypes()) {
                meshRequestTypeTO = new MeshRequestTypeTO();
                meshRequestTypeTO.setRequestTypeId(meshRequestType.getRequestTypeId());
                meshRequestTypeTO.setRequestTypeName(meshRequestType.getRequestTypeName());
                result.add(meshRequestTypeTO);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result.size() > 0 ? result : null;
    }

    public MeshRequestBlockingConfigTO checkExists(Integer requestTypeId, Integer blockingRequestTypeId)
            throws Exception
    {
        MeshRequestBlockingConfigTO meshRequestBlockingConfigTO = null;
        try{
            Object[] meshRequestBlockingConfig = meshRequestBlockingConfigDS.checkExists(requestTypeId,blockingRequestTypeId);
            if(meshRequestBlockingConfig != null){
                meshRequestBlockingConfigTO = new MeshRequestBlockingConfigTO();
                meshRequestBlockingConfigTO.setBlockingConfigID((Integer) meshRequestBlockingConfig[0]);
                if (meshRequestBlockingConfig[1] != null)
                    meshRequestBlockingConfigTO.setRequestTypeID((Integer) meshRequestBlockingConfig[1]);
                if (meshRequestBlockingConfig[2] != null)
                    meshRequestBlockingConfigTO.setBlockingRequestTypeID((Integer) meshRequestBlockingConfig[2]);
                if (meshRequestBlockingConfig[3] != null)
                    meshRequestBlockingConfigTO.setRequestTypeName((String) meshRequestBlockingConfig[3]);
                if (meshRequestBlockingConfig[4] != null)
                    meshRequestBlockingConfigTO.setBlockingRequestTypeName((String) meshRequestBlockingConfig[4]);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return meshRequestBlockingConfigTO;
    }
}