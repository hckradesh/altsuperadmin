package com.talentpact.business.dataservice.sysDuplicateParameter;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.duplicate.SysDuplicateParameter;
import com.talentpact.ui.sysDuplicateParameter.to.SysDuplicateParameterTO;

/**
 * 
 * @author prachi.bansal
 *
 */
@Stateless
public class SysDuplicateParameterService extends CommonService
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysDuplicateParameterService.class);
    
    @EJB
    SysDuplicateParameterDS sysDuplicateParameterDS;
    
    public List<SysDuplicateParameterTO> getSysDuplicateParameterList() throws Exception{
        List<SysDuplicateParameter> sysDuplicateParameterList=null;
        List<SysDuplicateParameterTO> sysDuplicateParameterTOList=null;
        sysDuplicateParameterTOList=new ArrayList<SysDuplicateParameterTO>();
        try{
            sysDuplicateParameterList=sysDuplicateParameterDS.getSysDuplicateParameterList();
            if(sysDuplicateParameterList!=null && !sysDuplicateParameterList.isEmpty()){
                for(SysDuplicateParameter sDP : sysDuplicateParameterList){
                    SysDuplicateParameterTO sysDuplicateParameterTO=new SysDuplicateParameterTO();
                    sysDuplicateParameterTO.setSysDuplicateParameterID(sDP.getSysDuplicateParameterID());
                    sysDuplicateParameterTO.setSysDuplicateParameterName(sDP.getSysDuplicateParameterName());
                    sysDuplicateParameterTOList.add(sysDuplicateParameterTO);
                }
            }
            return sysDuplicateParameterTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }
    
    public List<SysDuplicateParameterTO> getUpdateSysDuplicateParameters(Integer sysDuplicateParameterID) throws Exception{
        List<SysDuplicateParameter> sysDuplicateParameterList=null;
        List<SysDuplicateParameterTO> sysDuplicateParameterTOList=null;
        sysDuplicateParameterTOList=new ArrayList<SysDuplicateParameterTO>();
        try{
            sysDuplicateParameterList=sysDuplicateParameterDS.getUpdateSysDuplicateParameters(sysDuplicateParameterID);
            if(sysDuplicateParameterList!=null && !sysDuplicateParameterList.isEmpty()){
                for(SysDuplicateParameter sDP : sysDuplicateParameterList){
                    SysDuplicateParameterTO sysDuplicateParameterTO=new SysDuplicateParameterTO();
                    sysDuplicateParameterTO.setSysDuplicateParameterID(sDP.getSysDuplicateParameterID());
                    sysDuplicateParameterTO.setSysDuplicateParameterName(sDP.getSysDuplicateParameterName());
                    sysDuplicateParameterTOList.add(sysDuplicateParameterTO);
                }
            }
            return sysDuplicateParameterTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }

    public void saveSysDuplicateParameter(SysDuplicateParameterTO sysDuplicateParameterTO) {
        try{
            sysDuplicateParameterDS.saveSysDuplicateParameter(sysDuplicateParameterTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        
    }
    
    public void updateSysDuplicateParameter(SysDuplicateParameterTO sysDuplicateParameterTO){
        try{
            sysDuplicateParameterDS.updateSysDuplicateParameter(sysDuplicateParameterTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
}