/**
 * 
 */
package com.talentpact.business.dataservice.dashboardChart;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.insights.dashboardchart.to.DashboardChartTO;
import com.talentpact.model.altone.SysDashBoardChart;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysDashboardChartDS extends AbstractDS<SysDashBoardChart>
{

    public SysDashboardChartDS()
    {
        super(SysDashBoardChart.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysDashboardList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sd.sysDashboardID,sd.sysDashboardName, sd.styleClass  from SysDashBoardChart sd ");
            query = getEntityManager("TalentPactInsightsAgg").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysDashboard(DashboardChartTO dashboardChartTO)
        throws Exception
    {
        SysDashBoardChart sysDashBoard = new SysDashBoardChart();
        try {

            sysDashBoard.setSysDashboardName(dashboardChartTO.getDashboardName());
            sysDashBoard.setStyleClass(dashboardChartTO.getStyleClass());
            getEntityManager("TalentPactInsightsAgg").persist(sysDashBoard);
        } catch (Exception ex) {
            throw ex;
        }
    }


}