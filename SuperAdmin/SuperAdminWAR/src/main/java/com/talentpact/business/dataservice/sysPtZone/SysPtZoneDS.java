package com.talentpact.business.dataservice.sysPtZone;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysState;
import com.talentpact.model.organization.SysPTZone;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysPtZone.to.SysPtZoneTO;

/**
 * 
 * @author prachi.bansal
 *
 */
@Stateless
public class SysPtZoneDS extends AbstractDS<SysPTZone>
{

    @Inject
    UserSessionBean userSessionBean;
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPtZoneDS.class);
    
    public SysPtZoneDS(){
        super(SysPTZone.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysPTZone> getSysPtZoneTOList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        //List<Object[]> list = null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append(" from SysPTZone pz ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<SysState> getSysStateList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append(" from SysState ss order by name");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public void saveSysPtZone(SysPtZoneTO sysPtZoneTO) {
        SysPTZone sysPtZone=null;
        try{
           sysPtZone=new SysPTZone();
           sysPtZone.setPtZoneCode(sysPtZoneTO.getPtZoneCode());
           sysPtZone.setPtZoneName(sysPtZoneTO.getPtZoneName());
           sysPtZone.setStateID(sysPtZoneTO.getStateID());
           sysPtZone.setCreatedBy(userSessionBean.getUserID().intValue());
           sysPtZone.setModifiedBy(userSessionBean.getUserID().intValue());
           sysPtZone.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
           sysPtZone.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
           getEntityManager("Talentpact").persist(sysPtZone);
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
      
        
    }

    public void updateSysPtZone(SysPtZoneTO sysPtZoneTO) {
      SysPTZone sysPtZone=null;
      try{
          sysPtZone=new SysPTZone();
          sysPtZone=findSysPtZone(sysPtZoneTO.getPtZoneID());
          if(sysPtZone.getStateIDObj().getStateID()==sysPtZoneTO.getStateID()
                  &&sysPtZone.getPtZoneName().trim().equalsIgnoreCase(sysPtZoneTO.getPtZoneName().trim())){
              //DO nothing.
          }
          else
          {
              sysPtZone.setPtZoneName(sysPtZoneTO.getPtZoneName());
              sysPtZone.setPtZoneCode(sysPtZoneTO.getPtZoneCode());
              sysPtZone.setStateID(sysPtZoneTO.getStateID());
              sysPtZone.setModifiedBy(userSessionBean.getUserID().intValue());
              sysPtZone.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
              getEntityManager("Talentpact").merge(sysPtZone);
              getEntityManager("TalentPact").flush();
          }
      }catch (Exception e) {
          LOGGER_.error("", e);
      }
        
    }

    @SuppressWarnings("unchecked")
    private SysPTZone findSysPtZone(Integer sysPtZoneID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPTZone> list=null;
        SysPTZone sysPTZone=null;
        try{
            sysPTZone=new SysPTZone();
            hqlQuery=new StringBuilder();
            hqlQuery.append("from SysPTZone sz where sz.ptZoneID=:sysPtZoneID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysPtZoneID", sysPtZoneID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
                sysPTZone = list.get(0);
            }
            return sysPTZone;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<SysPTZone> getUpdatedSysPtZoneTOList(Integer sysPtZoneID) {
        Query query = null;
        StringBuilder hqlQuery = null;        
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append("from SysPTZone sz where sz.ptZoneID!=:sysPtZoneID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysPtZoneID", sysPtZoneID);
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }
    
}
