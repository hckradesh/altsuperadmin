/**
 * 
 */
package com.talentpact.business.dataservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.admin.model.dataupload.SysUploadTable;
import com.admin.model.dataupload.SysUploadTableColumn;
import com.admin.model.dataupload.SysUploadTemplateType;
import com.alt.common.helper.ValidatorUtil;
import com.talentpact.auth.entity.TpTenant;
import com.talentpact.model.SysUploadAttribute;
import com.talentpact.ui.dataupload.to.SysUploadTemplateTypeTO;
import com.talentpact.ui.dataupload.to.TableColumnTO;


/**
 * @author radhamadhab.dalai
 *
 */
@Stateless
public class DataUploadDS extends AbstractDS<SysUploadTemplateType>
{
    public DataUploadDS()
    {
        super(SysUploadTemplateType.class);
    }


    public List<SysUploadTemplateType> getSysUploadTemplateTypeList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysUploadTemplateType> list = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select STY from SysUploadTemplateType STY");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<SysUploadTable> getSysUploadTableList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysUploadTable> list = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select STY from SysUploadTable STY");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<SysUploadTableColumn> getSysUploadTableColumnList(Integer tableID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysUploadTableColumn> list = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select STY from SysUploadTableColumn STY where STY.uploadTableID=:uploadTableID  ");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("uploadTableID", tableID);
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public void saveTemplateType(SysUploadTemplateTypeTO sysUploadTemplateTypeTO, List<TableColumnTO> tableColumnTOList)
    {
        SysUploadTemplateType sysUploadTemplateType = null;
        SysUploadAttribute sysUploadAttribute = null;
        Integer templateTypeID = null;
        try {
            sysUploadTemplateType = new SysUploadTemplateType();
            sysUploadTemplateType.setTemplateTypeName(sysUploadTemplateTypeTO.getTemplateTypeName());
            sysUploadTemplateType.setUploadTableID(sysUploadTemplateTypeTO.getUploadTableID());
            sysUploadTemplateType.setCreatedBy(1);
            sysUploadTemplateType.setModifiedBy(1);
            sysUploadTemplateType.setCreatedDate(new Date());
            sysUploadTemplateType.setModifiedDate(new Date());
            getEntityManager("AltCommon").persist(sysUploadTemplateType);
            templateTypeID = sysUploadTemplateType.getTemplateTypeId();
            for (TableColumnTO tableColumnTO : tableColumnTOList) {
                sysUploadAttribute = new SysUploadAttribute();
                sysUploadAttribute.setAttributeName(tableColumnTO.getAttributeName());
                sysUploadAttribute.setTemplateTypeID(templateTypeID);
                sysUploadAttribute.setUploadTableColumnID(tableColumnTO.getUploadTableColumnID());
                getEntityManager("AltCommon").persist(sysUploadAttribute);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 
     * @param persistenceUnitName
     * @return
     * @throws Exception
     */
    public List<String> getTableNames(String persistenceUnitName)
        throws Exception
    {
        List<String> list = null;
        try {
            list = getEntityManager(persistenceUnitName).createNativeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES order by 1").getResultList();
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public String insertTableAndColumn(String persistenceUnitName, Integer tableId, String tableName, List<TableColumnTO> column)
        throws Exception
    {
        SysUploadTable sysUploadTable = null;
        SysUploadTableColumn sysUploadTableColumn = null;
        try {

            if (column == null || column.isEmpty()) {
                return "No column is there in " + tableName + " table.";
            }

            if (tableId != null && tableId > 0) {
                sysUploadTable = getEntityManager("AltCommon").find(SysUploadTable.class, tableId);
            } else {
                sysUploadTable = new SysUploadTable();
                sysUploadTable.setCreatedBy(1);
                sysUploadTable.setCreatedDate(new Date());
            }
            sysUploadTable.setPersistenceUnitName(persistenceUnitName);
            sysUploadTable.setTableName(tableName);
            sysUploadTable.setModifiedBy(1);
            sysUploadTable.setModifiedDate(new Date());
            getEntityManager("AltCommon").persist(sysUploadTable);
            Integer uploadTableID = sysUploadTable.getUploadTableID();
            for (TableColumnTO to : column) {
                if (to.getUploadTableColumnID() != null && to.getUploadTableColumnID() > 0) {
                    sysUploadTableColumn = getEntityManager("AltCommon").find(SysUploadTableColumn.class, to.getUploadTableColumnID());
                } else {
                    sysUploadTableColumn = new SysUploadTableColumn();
                    sysUploadTableColumn.setCreatedBy(1);
                    sysUploadTableColumn.setCreatedDate(new Date());
                }
                sysUploadTableColumn.setUploadTableID(uploadTableID);
                sysUploadTableColumn.setColumnName(to.getColumnName());
                if (to.getValidationType() != null && !to.getValidationType().isEmpty()) {
                    sysUploadTableColumn.setValidationType(to.getValidationType());
                }
                if (to.getDataType() != null && !to.getDataType().isEmpty()) {
                    sysUploadTableColumn.setDataType(to.getDataType());
                }
                if (to.getDataUploadColumnName() != null && !to.getDataUploadColumnName().isEmpty()) {
                    sysUploadTableColumn.setDataUploadColumnName(to.getDataUploadColumnName());
                }
                sysUploadTableColumn.setModifiedBy(1);
                sysUploadTableColumn.setModifiedDate(new Date());
                getEntityManager("AltCommon").persist(sysUploadTableColumn);
            }
            return null;
        } catch (NoResultException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<SysUploadAttribute> getSysUploadTemplateAttributes(Integer templateTypeID)
        throws Exception
    {
        return getEntityManager("AltCommon").createQuery(" Select SS from SysUploadAttribute SS where SS.templateTypeID=:templateTypeID ")
                .setParameter("templateTypeID", templateTypeID).getResultList();
    }

    public List<TableColumnTO> getTableColumnsNames(String persistenceUnitName, String tableName)
        throws Exception
    {
        List<Object[]> columnList = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TableColumnTO> columnToList = null;
        try {

            hqlQuery = new StringBuilder("SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=:tableName order by 1 asc");
            query = getEntityManager(persistenceUnitName).createNativeQuery(hqlQuery.toString());
            query.setParameter("tableName", tableName);
            columnList = query.getResultList();

            query = null;
            hqlQuery = new StringBuilder(" SELECT top 1 COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME=:tableName and CONSTRAINT_NAME like 'PK%' ");
            query = getEntityManager(persistenceUnitName).createNativeQuery(hqlQuery.toString());
            query.setParameter("tableName", tableName);
            List<String> pk = query.getResultList();

            //            if (pk != null && !pk.isEmpty()) {
            //                columnList.remove(pk.get(0));
            //            }

            columnToList = new ArrayList<TableColumnTO>();
            for (Object[] obj : columnList) {
                String colName = ValidatorUtil.checkAndReturnDataFromSession(obj[0]);
                String dataType = ValidatorUtil.checkAndReturnDataFromSession(obj[1]);
                if (colName.equalsIgnoreCase(ValidatorUtil.checkAndReturnData(pk.get(0))) || colName.equalsIgnoreCase("OrganizationID") || colName.equalsIgnoreCase("TenantID")
                        || colName.equalsIgnoreCase("CreatedDate") || colName.equalsIgnoreCase("ModifiedDate")
                        || colName.equalsIgnoreCase("CreatedBy") || colName.equalsIgnoreCase("ModifiedBy") || colName.equalsIgnoreCase("Isprocessed") || colName.equalsIgnoreCase("IsValid")
                        || colName.equalsIgnoreCase("IsDeleted") || colName.equalsIgnoreCase("TransactionID") || colName.equalsIgnoreCase("IsIntegration")) {
                    continue;
                }
                TableColumnTO to = new TableColumnTO();
                to.setColumnName(colName);
                to.setDataType(dataType);
                columnToList.add(to);
            }
        } catch (NoResultException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return columnToList;
    }

    public boolean saveTableColumn(String persistenceUnitName, String tableName, List<TableColumnTO> columnLst)
    {
        try {
            SysUploadTable sysUploadTable = new SysUploadTable();
            sysUploadTable.setPersistenceUnitName(persistenceUnitName);
            sysUploadTable.setTableName(tableName);
            sysUploadTable.setCreatedBy(1);
            sysUploadTable.setModifiedBy(1);
            sysUploadTable.setCreatedDate(new Date());
            sysUploadTable.setModifiedDate(new Date());
            getEntityManager(persistenceUnitName).persist(sysUploadTable);
            Integer uploadTableID = sysUploadTable.getUploadTableID();
            for (TableColumnTO column : columnLst) {
                SysUploadTableColumn sysUploadTableColumn = new SysUploadTableColumn();
                sysUploadTableColumn.setUploadTableID(uploadTableID);
                sysUploadTableColumn.setColumnName(column.getColumnName());
                if (column.getValidationType() != null && !column.getValidationType().isEmpty()) {
                    sysUploadTableColumn.setValidationType(column.getValidationType());
                }
                if (column.getDataType() != null && !column.getDataType().isEmpty()) {
                    sysUploadTableColumn.setDataType(column.getDataType());
                }
                if (column.getDataUploadColumnName() != null && !column.getDataUploadColumnName().isEmpty()) {
                    sysUploadTableColumn.setDataUploadColumnName(column.getDataUploadColumnName());
                }
                sysUploadTableColumn.setCreatedBy(1);
                sysUploadTableColumn.setModifiedBy(1);
                sysUploadTableColumn.setCreatedDate(new Date());
                sysUploadTableColumn.setModifiedDate(new Date());
                getEntityManager(persistenceUnitName).persist(sysUploadTableColumn);
            }
            return true;
        } catch (Exception ex) {
            throw ex;
        }
    }

}
