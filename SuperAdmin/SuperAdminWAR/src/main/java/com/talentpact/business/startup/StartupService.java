/**
 * 
 */
package com.talentpact.business.startup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.common.constants.IConstants;
import com.talentpact.business.dataservice.DatabaseMetaDataDS;
import com.talentpact.business.dataservice.RoleTypeDS;
import com.talentpact.business.dataservice.SysMenuDS;


/**
 * @author vikas.singh
 *
 */
@Stateless
public class StartupService
{

    @EJB
    DatabaseMetaDataDS databaseMetaDataDS;

    @EJB
    RoleTypeDS roleTypeDS;

    @EJB
    SysMenuDS sysMenuDS;


    public Map<String, String> setAllDatabase()
        throws Exception

    {
        List<Object[]> objectList = null;
        Map<String, String> dbMetadata = null;
        StringBuilder key = null;
        try {
            objectList = databaseMetaDataDS.getAllDatabaseByName();
            dbMetadata = new HashMap<String, String>();
            for (Object[] object : objectList) {
                key = new StringBuilder();
                key.append(IConstants.DATABASE_METADATA_START_KEY + object[1] + IConstants.UNDERSCORE + object[2]);
                dbMetadata.put(key.toString(), (String) object[0]);
            }

            return dbMetadata;
        } catch (Exception e) {
            throw new Exception(e);

        } finally {
            objectList = null;
            key = null;
        }

    }

    public Map<String, SysRoleTypeTO> setAllSysRoleTypeMap()
        throws Exception
    {
        SysRoleTypeTO sysRoleTypeTO = null;
        Map<String, SysRoleTypeTO> resultMap = null;
        Map<String, Integer> sysRoleTypeMap = null;
        List<Object[]> objList = null;
        try {
            //making map
            resultMap = new HashMap<String, SysRoleTypeTO>();
            objList = roleTypeDS.getAllSysRoleTypeMap();
            sysRoleTypeMap = roleTypeDS.getSysRoleTypeFromTalentPact_AdminPortal();
            for (Object object[] : objList) {
                sysRoleTypeTO = new SysRoleTypeTO();
                sysRoleTypeTO.setRoleTypeID((sysRoleTypeMap.get((String) object[1])).longValue());
                sysRoleTypeTO.setRoleType((String) object[1]);
                sysRoleTypeTO.setDescription((String) object[3]);
                sysRoleTypeTO.setDefaultName((String) object[4]);
                resultMap.put((String) object[1], sysRoleTypeTO);

            }
            //end of mnaking map 

            return resultMap;
        } catch (Exception e) {
            throw new Exception(e);

        } finally {
            sysRoleTypeTO = null;
            resultMap = null;
            objList = null;
        }

    }

    public List<SysMenuTO> setAllSysMenuToList()
        throws Exception
    {
        List<SysMenuTO> listSysMenuResponseTO = null;
        SysMenuTO sysMenuResponseTO = null;
        List<Object[]> list;
        try {

            list = sysMenuDS.getAllSysMenu();
            listSysMenuResponseTO = new ArrayList<SysMenuTO>();
            for (Object object[] : list) {
                sysMenuResponseTO = new SysMenuTO();
                if (object[0] != null)
                    sysMenuResponseTO.setMenuID(((Integer) object[0]).longValue());

                if (object[1] != null)
                    sysMenuResponseTO.setMenuName((String) object[1]);


                if (object[2] != null)
                    sysMenuResponseTO.setSequence((Integer) object[2]);

                if (object[3] == null) {
                    sysMenuResponseTO.setParentMenuID(null);
                } else {

                    sysMenuResponseTO.setParentMenuID(((Integer) object[3]).longValue());
                }

                if (object[4] != null)
                    sysMenuResponseTO.setUrl((String) object[4]);

                if (object[5] != null)
                    sysMenuResponseTO.setMenuCategory((Integer) object[5]);

                if (object[6] != null)
                    sysMenuResponseTO.setDefaultClass((String) object[6]);

                if (object[7] == null) {
                    sysMenuResponseTO.setModuleID(null);
                } else {
                    sysMenuResponseTO.setModuleID(((Integer) object[7]).longValue());
                }
                if (object[8] != null)
                    sysMenuResponseTO.setMobSequence((Integer) object[8]);
                sysMenuResponseTO.setSelected(false);
                listSysMenuResponseTO.add(sysMenuResponseTO);
            }
            return listSysMenuResponseTO;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            listSysMenuResponseTO = null;
            sysMenuResponseTO = null;
            list = null;
        }
    }


}
