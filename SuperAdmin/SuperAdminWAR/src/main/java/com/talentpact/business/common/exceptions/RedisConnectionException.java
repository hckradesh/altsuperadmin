package com.talentpact.business.common.exceptions;

public class RedisConnectionException extends Exception
{
    private String message = null;

    public RedisConnectionException()
    {
        super();
    }

    public RedisConnectionException(String message)
    {
        super(message);
        this.message = message;
    }

    public RedisConnectionException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

}
