/**
 * 
 */
package com.talentpact.business.tpOrganization.converter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alt.security.transport.output.UiFormTO;
import com.talentpact.auth.entity.TpApp;
import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpPortalType;
import com.talentpact.auth.entity.TpURL;
import com.talentpact.business.application.transport.input.AppRequestTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.output.AppResponseTO;
import com.talentpact.business.application.transport.output.CheckListResponseTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.common.transport.IResponseTransportObject;
import com.talentpact.business.tenants.transport.newtenant.input.TpURLTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tenants.transport.newtenant.output.OrgMoreInfoResponseTO;
import com.talentpact.business.tpModule.transport.input.TpModuleRequestTO;
import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.business.tpOrganization.transport.input.AppPortalTypeUrlRequestTO;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.business.tpOrganization.transport.input.VideoLinkTO;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.OrganizationResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.model.HrModule;
import com.talentpact.model.SysSubModule;


/**
 * @author radhamadhab.dalai
 *
 */
public class OrganizationConverter
{
    private static final Object NULL = null;

    public static Map<Long, List<AppPortalTypeUrlResponseTO>> getMapOfAppAndPortal(List<TpPortalType> tpURlList)
        throws Exception
    {
        Map<Long, List<AppPortalTypeUrlResponseTO>> mapOfAppAndPortalUrl = new HashMap<Long, List<AppPortalTypeUrlResponseTO>>();
        if (tpURlList != null) {
            AppPortalTypeUrlResponseTO portalTO = null;
            for (TpPortalType tpPortal : tpURlList) {
                portalTO = new AppPortalTypeUrlResponseTO();
                portalTO.setPortalTypeId(tpPortal.getPortalTypeID());
                portalTO.setPortalTypeName(tpPortal.getPortalType());
                Long appKey = tpPortal.getTpAppId().getAppID();
                List<AppPortalTypeUrlResponseTO> portalList = null;
                if (mapOfAppAndPortalUrl.containsKey(appKey)) {
                    portalList = mapOfAppAndPortalUrl.get(appKey);
                    portalList.add(portalTO);
                } else {
                    portalList = new ArrayList<AppPortalTypeUrlResponseTO>();
                    portalList.add(portalTO);

                }
                mapOfAppAndPortalUrl.put(appKey, portalList);


            }

        }

        return mapOfAppAndPortalUrl;
    }


    /*Map<AppIdAsKey  , List<ModulesOfAPP> */
    public static Map<Long, List<TpModuleResponseTO>> getMapOfAppAndModules(List<SysSubModule> moduleList)
        throws Exception
    {
        Map<Long, List<TpModuleResponseTO>> mapOfAppAndPortal = new HashMap<Long, List<TpModuleResponseTO>>();
        if (moduleList != null) {
            TpModuleResponseTO portaModulelTO = null;
            for (SysSubModule moduleTo : moduleList) {
                portaModulelTO = new TpModuleResponseTO();
                //portaModulelTO.setModuleID(moduleTo.getSubModuleID().longValue());
                portaModulelTO.setModuleID(moduleTo.getModuleId().longValue());
                portaModulelTO.setModuleName(moduleTo.getModuleName());
                portaModulelTO.setAppID(moduleTo.getAppId());
                portaModulelTO.setParentSysSubModuleID(moduleTo.getParentModuleId());

                List<TpModuleResponseTO> moduleTOList = null;
                Long appKey = moduleTo.getAppId().longValue();
                if (mapOfAppAndPortal.containsKey(appKey)) {
                    moduleTOList = mapOfAppAndPortal.get(appKey);
                } else {
                    moduleTOList = new ArrayList<TpModuleResponseTO>();
                }
                moduleTOList.add(portaModulelTO);
                mapOfAppAndPortal.put(appKey, moduleTOList);

            }

        }

        return mapOfAppAndPortal;
    }


    public static List<TpURLTO> convert(List<TpURL> tpURLTOList)
        throws Exception
    {
        List<TpURLTO> resultList = null;
        TpURLTO tpURLTO = null;
        try {

            resultList = new ArrayList<TpURLTO>();

            for (TpURL tpURL : tpURLTOList) {

                tpURLTO = new TpURLTO();

                tpURLTO.setUrl(tpURL.getUrl());
                tpURLTO.setUrlID(tpURL.getUrlID());

                resultList.add(tpURLTO);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }

    public static List<AppRequestTO> convertApp(List<TpApp> tpAppTOList)
        throws Exception
    {
        List<AppRequestTO> resultList = null;
        AppRequestTO tpAppTO = null;
        try {

            resultList = new ArrayList<AppRequestTO>();

            for (TpApp tpApp : tpAppTOList) {

                tpAppTO = new AppRequestTO();

                tpAppTO.setAppId(tpApp.getAppID());
                tpAppTO.setAppName(tpApp.getName());

                resultList.add(tpAppTO);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }


    public static List<AppResponseTO> getAppResTo(List<TpApp> tpAppTOList)
        throws Exception
    {
        List<AppResponseTO> resultList = null;
        AppResponseTO tpAppTO = null;
        try {

            resultList = new ArrayList<AppResponseTO>();

            for (TpApp tpApp : tpAppTOList) {

                tpAppTO = new AppResponseTO();

                tpAppTO.setAppId(tpApp.getAppID());
                tpAppTO.setAppName(tpApp.getName());
                tpAppTO.setAppCode(tpApp.getCode());

                resultList.add(tpAppTO);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }


    public static OrgAppPortalTreeResponseTO mergeMasterDataToOrgData(List<TpURL> orgSelectedUrlList, List<HrModule> moduleListForOrg,
            OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO, Map<Long, List<VideoLinkTO>> moduleUrlMap)
        throws Exception
    {
  try {

            /* Master Data Of System*/
            Map<Long, List<TpModuleResponseTO>> moduleToMap = orgAppPortalTreeResponseTO.getAppModuleMap();
            Map<Long, List<AppPortalTypeUrlResponseTO>> portalTOMap = orgAppPortalTreeResponseTO.getAppPortalUrlMap();
            // List<AppResponseTO> appResToList = orgAppPortalTreeResponseTO.getAppList();


            Map<Long, Map<String, String>> selectedPortalValues = new HashMap<Long, Map<String, String>>();
            Map<Long, Map<String, String>> selectedAppValues = new HashMap<Long, Map<String, String>>();
            Set<Long> activeModuleIdSet = new HashSet<Long>();

            if (orgSelectedUrlList != null && !orgSelectedUrlList.isEmpty()) {
                Map<String, String> dBvalue = null;
                Long appId = null;
                for (TpURL tpurl : orgSelectedUrlList) {
                    /**
                     * for login server details only
                     */
                    if(tpurl.getTpPortalType().getPortalType().equalsIgnoreCase("Employee Portal")){
                        orgAppPortalTreeResponseTO.setEmployeePortalURLID(tpurl.getUrlID());
                    }
                    dBvalue = new HashMap<String, String>();
                    dBvalue.put("url", tpurl.getUrl());
                    dBvalue.put("homePage", tpurl.getHomePage());
                    dBvalue.put("loginPage", tpurl.getLoginPage());
                    dBvalue.put("defualtTheme", tpurl.getDefaultTheme());
                    dBvalue.put("loginPageTheme", tpurl.getLoginPageTheme());

                    dBvalue.put("leftLogo", tpurl.getLeftLogo());
                    dBvalue.put("rightLogo", tpurl.getRightLogo());
                    dBvalue.put("favicon", tpurl.getFavicon());
                    dBvalue.put("footer", tpurl.getFooter());
                    dBvalue.put("title", tpurl.getTitle());
                    dBvalue.put("headingTxt", tpurl.getHeadingTxt());
                    dBvalue.put("userTxt", tpurl.getUsernameTxt());
                    dBvalue.put("btnTxt", tpurl.getBtnTxt());
                    dBvalue.put("banner", tpurl.getBanner());
                    dBvalue.put("samlBtnTxt", tpurl.getSamlBtnTxt());
                    dBvalue.put("samlHeader", tpurl.getSamlHeader());
                    dBvalue.put("passwordTxt", tpurl.getPasswordTxt());
                    if (tpurl.getMobileEnabled() != null) {
                        dBvalue.put("mobileStatus", tpurl.getMobileEnabled().toString());
                    }
                    dBvalue.put("backGroundImage", tpurl.getBackgroundImage());
                    dBvalue.put("emailAddress", tpurl.getEmailAddress());
                    dBvalue.put("contactNumber", tpurl.getContactNumber());
                    dBvalue.put("externalUrl", tpurl.getExternalURL());
                    if(tpurl != null  && tpurl.getLandingPageID() != null && tpurl.getLandingPageID() != 0)
                    dBvalue.put("landingPage", tpurl.getLandingPageID().toString());
                    appId = tpurl.getTpPortalType().getTpAppId().getAppID();


                    if (!(selectedAppValues.containsKey(appId))) {

                        Map<String, String> appValueMap = new HashMap<String, String>();
                        appValueMap.put("database", tpurl.getMeta_Data());
                        appValueMap.put("favicon", tpurl.getFavicon());
                        selectedAppValues.put(appId, appValueMap);
                    }


                    selectedPortalValues.put(tpurl.getTpPortalType().getPortalTypeID(), dBvalue);

                }
                /*Mark Portal selected By Organization */
                for (Long appId1 : portalTOMap.keySet()) {
                    List<AppPortalTypeUrlResponseTO> portalList = portalTOMap.get(appId1);
                    for (AppPortalTypeUrlResponseTO urlTo : portalList) {
                        Long portalId = urlTo.getPortalTypeId();
                        Map<String, String> portalValuesMap = selectedPortalValues.get(portalId);

                        if (selectedPortalValues.containsKey(portalId)) {

                            for (Map.Entry<Long, List<VideoLinkTO>> entry : moduleUrlMap.entrySet()) {
                                if (entry.getKey().equals(portalId)) {
                                    urlTo.setOnBoardingVideoLinkList(entry.getValue());
                                }
                            }

                            urlTo.setPortalChecked(true);
                            urlTo.setUrl(portalValuesMap.get("url"));
                            urlTo.setHomePage(portalValuesMap.get("homePage"));
                            urlTo.setLoginPage(portalValuesMap.get("loginPage"));
                            urlTo.setDefualtTheme(portalValuesMap.get("defualtTheme"));
                            urlTo.setLoginPageTheme(portalValuesMap.get("loginPageTheme"));
                            if(portalValuesMap.get("landingPage") != null)
                            urlTo.setLandingPageID(Integer.parseInt(portalValuesMap.get("landingPage")));
                            urlTo.setLeftLogo(portalValuesMap.get("leftLogo"));
                            urlTo.setRightLogo(portalValuesMap.get("rightLogo"));
                            urlTo.setFooter(portalValuesMap.get("footer"));
                            urlTo.setFavicon(portalValuesMap.get("favicon"));
                            urlTo.setTitle(portalValuesMap.get("title"));
                            urlTo.setHeadingTxt(portalValuesMap.get("headingTxt"));
                            urlTo.setUsernameTxt(portalValuesMap.get("userTxt"));
                            urlTo.setBtnTxt(portalValuesMap.get("btnTxt"));
                            urlTo.setBanner(portalValuesMap.get("banner"));
                            urlTo.setBackGroundImage(portalValuesMap.get("backGroundImage"));
                            urlTo.setSamlBtnTxt(portalValuesMap.get("samlBtnTxt"));
                            urlTo.setSamlHeader(portalValuesMap.get("samlHeader"));
                            urlTo.setPasswordTxt(portalValuesMap.get("passwordTxt"));
                            urlTo.seteMail(portalValuesMap.get("emailAddress"));
                            urlTo.setContactNumber(portalValuesMap.get("contactNumber"));
                            urlTo.setExternalVideoPath(portalValuesMap.get("externalUrl"));
                            if (portalValuesMap.get("mobileStatus") != null) {
                                if (portalValuesMap.get("mobileStatus").equalsIgnoreCase("true")) {
                                    urlTo.setMobileStatus(true);
                                } else {
                                    urlTo.setMobileStatus(false);
                                }
                            } else {
                                urlTo.setMobileStatus(false);
                            }

                        } else {
                            urlTo.setPortalChecked(false);
                        }

                    }
                }
            }

            if ((moduleListForOrg != null) && (!moduleListForOrg.isEmpty())) {
                for (HrModule orgMod : moduleListForOrg) {
                    activeModuleIdSet.add(new Long(orgMod.getSysModuleId()));
                }
                for (Long appId : moduleToMap.keySet()) {
                    List<TpModuleResponseTO> modulelList = moduleToMap.get(appId);
                    for (TpModuleResponseTO modulelTo : modulelList) {
                        Long moduleId = modulelTo.getModuleID();
                        if (activeModuleIdSet.contains(moduleId)) {
                            modulelTo.setModuleChecked(true);

                        } else {
                            modulelTo.setModuleChecked(false);
                        }

                    }
                }

            }


            /*Mark App value Selected By Organization*/

            //            for (AppResponseTO appResponseTO : appResToList) {
            //                Map<String, String> valueMap = selectedAppValues.get(appResponseTO.getAppId());
            //                if (valueMap != null) {
            //                    appResponseTO.setDatabase(valueMap.get("database"));
            //                    appResponseTO.setFavicon(valueMap.get("favicon"));
            //                }
            //
            //            }


            /*Mark Portal selected By Organization */
            //            for (Long appId : portalTOMap.keySet()) {
            //                List<AppPortalTypeUrlResponseTO> portalList = portalTOMap.get(appId);
            //                for (AppPortalTypeUrlResponseTO urlTo : portalList) {
            //                    Long portalId = urlTo.getPortalTypeId();
            //                    Map<String, String> portalValuesMap = selectedPortalValues.get(portalId);
            //
            //                    if (selectedPortalValues.containsKey(portalId)) {
            //                        urlTo.setPortalChecked(true);
            //                        urlTo.setUrl(portalValuesMap.get("url"));
            //                        urlTo.setHomePage(portalValuesMap.get("homePage"));
            //                        urlTo.setLoginPage(portalValuesMap.get("loginPage"));
            //                        urlTo.setDefualtTheme(portalValuesMap.get("defualtTheme"));
            //                        urlTo.setLoginPageTheme(portalValuesMap.get("loginPageTheme"));
            //
            //                    } else {
            //                        urlTo.setPortalChecked(false);
            //                    }
            //
            //                }
            //            }

            /*Mark Module selected By Organization */
            //            for (Long appId : moduleToMap.keySet()) {
            //                List<TpModuleResponseTO> modulelList = moduleToMap.get(appId);
            //                for (TpModuleResponseTO modulelTo : modulelList) {
            //                    Long moduleId = modulelTo.getModuleID();
            //                    if (activeModuleIdSet.contains(moduleId)) {
            //                        modulelTo.setModuleChecked(true);
            //
            //                    } else {
            //                        modulelTo.setModuleChecked(false);
            //                    }
            //
            //                }
            //            }


        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception();
        }
        return orgAppPortalTreeResponseTO;
    }

    public static List<OrganizationResponseTO> convertOrgListToOrgResTO(List<Object[]> objectList)
        throws Exception
    {
        List<OrganizationResponseTO> organizationresTOList = null;
        OrganizationResponseTO organizationRestTO = null;
        try {
            if (objectList != null) {
                organizationresTOList = new ArrayList<OrganizationResponseTO>();
                for (Object[] objArr : objectList) {
                    organizationRestTO = new OrganizationResponseTO();
                    organizationRestTO.setOrgName((String) objArr[0]);
                    organizationRestTO.setOrgCode((String) objArr[1]);
                    organizationRestTO.setOrganizationId((Long) objArr[2]);
                    organizationRestTO.setTenantName((String) objArr[3]);
                    organizationresTOList.add(organizationRestTO);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return organizationresTOList;
    }

    public static OrganizationrequestTO convertOrgReqTO(TpOrganization organization)
        throws Exception
    {

        OrganizationrequestTO organizationrequestTO = null;
        try {
            organizationrequestTO = new OrganizationrequestTO();


            organizationrequestTO.setOrgName(organization.getName());
            organizationrequestTO.setOrgCode(organization.getOrganizationCode());
            organizationrequestTO.setOrganizationId(organization.getOrganizationID());


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return organizationrequestTO;
    }


    public static List<AppPortalTypeUrlResponseTO> ConvertInAppPortal(List<Object[]> objlist)
        throws Exception
    {
        List<AppPortalTypeUrlResponseTO> apturtlist = null;
        AppPortalTypeUrlResponseTO appPortalTypeUrlResponseTO = null;
        try {
            apturtlist = new ArrayList<AppPortalTypeUrlResponseTO>();

            for (Object[] object : objlist) {
                appPortalTypeUrlResponseTO = new AppPortalTypeUrlResponseTO();
                appPortalTypeUrlResponseTO.setPortalTypeId((Long) object[0]);
                appPortalTypeUrlResponseTO.setPortalTypeName((String) object[1]);
                appPortalTypeUrlResponseTO.setAppId((Long) object[2]);
                appPortalTypeUrlResponseTO.setAppName((String) object[3]);
                apturtlist.add(appPortalTypeUrlResponseTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return apturtlist;
    }

    public static List<AppPortalTypeUrlRequestTO> ConvertInAppPortalRequestTO(List<AppPortalTypeUrlResponseTO> objlist)
        throws Exception
    {
        List<AppPortalTypeUrlRequestTO> apturtlist = null;
        AppPortalTypeUrlRequestTO appPortalTypeUrlRequestTO = null;
        try {
            apturtlist = new ArrayList<AppPortalTypeUrlRequestTO>();

            for (AppPortalTypeUrlResponseTO appPortalsTypeUrlResponseTO : objlist) {
                appPortalTypeUrlRequestTO = new AppPortalTypeUrlRequestTO();
                appPortalTypeUrlRequestTO.setAppId(appPortalsTypeUrlResponseTO.getAppId());
                appPortalTypeUrlRequestTO.setAppName(appPortalsTypeUrlResponseTO.getAppName());
                appPortalTypeUrlRequestTO.setPortalTypeId(appPortalsTypeUrlResponseTO.getPortalTypeId());
                appPortalTypeUrlRequestTO.setPortalTypeName(appPortalsTypeUrlResponseTO.getPortalTypeName());
                appPortalTypeUrlRequestTO.setUrl(appPortalsTypeUrlResponseTO.getUrl());
                appPortalTypeUrlRequestTO.setUrlId(appPortalsTypeUrlResponseTO.getUrlId());
                apturtlist.add(appPortalTypeUrlRequestTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return apturtlist;
    }

    // convert TpModule to TPModuleTO

    public static List<TpModuleRequestTO> convertModuleList(List<Object[]> tpModuleList)
        throws Exception
    {
        List<TpModuleRequestTO> moduleTOList = null;
        TpModuleRequestTO tpModuleRequestTO = null;
        try {
            moduleTOList = new ArrayList<TpModuleRequestTO>();

            if (tpModuleList != null && tpModuleList.size() != 0) {

                for (Object[] tpModule : tpModuleList) {
                    tpModuleRequestTO = new TpModuleRequestTO();
                    if (tpModule[0] != null) {
                        tpModuleRequestTO.setModuleID((Long) tpModule[0]);
                    }

                    if (tpModule[1] != null) {
                        tpModuleRequestTO.setModuleName((String) tpModule[1]);
                    }

                    if (tpModule[2] != null) {
                        tpModuleRequestTO.setModuleDesc((String) tpModule[2]);
                    }

                    if (tpModule[3] != null) {
                        tpModuleRequestTO.setAppID((Long) tpModule[3]);
                    }

                    if (tpModule.length > 4) {

                        if (tpModule[4] != null) {
                            if ((Boolean) tpModule[4] == true) {
                                tpModuleRequestTO.setActive(true);
                            } else if ((Boolean) tpModule[4] == false) {
                                tpModuleRequestTO.setActive(false);
                            }

                            //                        tpModuleRequestTO.setActive((Boolean) tpModule[4]);
                        }
                    }


                    moduleTOList.add(tpModuleRequestTO);
                }
            }//end of if

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return moduleTOList;
    }


    public static List<NewTenantResponseTO> markTenantTOSelected(List<NewTenantResponseTO> tenantToList, Long tenantId)
        throws Exception
    {
        if (tenantToList != null && tenantId != null) {
            for (NewTenantResponseTO tenantTO : tenantToList) {
                if (tenantTO.getTenantID().equals(tenantId)) {
                    tenantTO.setIsSelected(true);
                }
            }

        }

        return tenantToList;
    }

    public static List<CheckListResponseTO> convertInCheckListResponseToList(List<Object[]> objlist)
        throws Exception
    {
        List<CheckListResponseTO> tempCheckListList = null;
        CheckListResponseTO checkListResponseTO = null;
        List<CheckListResponseTO> checkListList = null;
        try {
            checkListList = new ArrayList<CheckListResponseTO>();
            tempCheckListList = new ArrayList<CheckListResponseTO>();
            for (Object[] object : objlist) {
                checkListResponseTO = new CheckListResponseTO();
                checkListResponseTO.setItemName((String) object[0]);
                if (object[1] != NULL) {
                    checkListResponseTO.setSelected(true);
                } else {
                    checkListResponseTO.setSelected(false);
                }
                checkListResponseTO.setSysCheckListID((Integer) object[2]);
                tempCheckListList.add(checkListResponseTO);
            }
            for (CheckListResponseTO l : tempCheckListList) {
                if (!l.getItemName().equalsIgnoreCase("SELECT_THEME")) {
                    checkListList.add(l);
                }
            }
            return checkListList;

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            checkListList = null;
            checkListResponseTO = null;
        }

    }


    public static List<? extends IResponseTransportObject> convertInSysRoleTypeResponseToList(List<Object[]> list)
        throws Exception
    {
        List<SysRoleTypeTO> listSysRoleTypeResponseTO = null;
        SysRoleTypeTO sysRoleTypeResponseTO = null;
        try {
            listSysRoleTypeResponseTO = new ArrayList<SysRoleTypeTO>();
            for (Object object[] : list) {
                sysRoleTypeResponseTO = new SysRoleTypeTO();
                sysRoleTypeResponseTO.setRoleTypeID(((Integer) object[0]).longValue());
                sysRoleTypeResponseTO.setRoleType((String) object[1]);
                sysRoleTypeResponseTO.setDescription((String) object[3]);
                sysRoleTypeResponseTO.setDefaultName((String) object[4]);
                listSysRoleTypeResponseTO.add(sysRoleTypeResponseTO);
            }
            return listSysRoleTypeResponseTO;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            sysRoleTypeResponseTO = null;
        }
    }


    public static List<? extends IResponseTransportObject> convertInSysMenuResponseToList(List<Object[]> list)
        throws Exception
    {
        List<SysMenuTO> listSysMenuResponseTO = null;
        SysMenuTO sysMenuResponseTO = null;
        try {
            listSysMenuResponseTO = new ArrayList<SysMenuTO>();
            for (Object object[] : list) {
                sysMenuResponseTO = new SysMenuTO();
                sysMenuResponseTO.setMenuID((Long) object[0]);
                sysMenuResponseTO.setMenuName((String) object[1]);
                sysMenuResponseTO.setMenuCode((String) object[2]);
                sysMenuResponseTO.setSelected(false);
                listSysMenuResponseTO.add(sysMenuResponseTO);
            }
            return listSysMenuResponseTO;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            listSysMenuResponseTO = null;
            sysMenuResponseTO = null;
        }
    }


    public static List<? extends IResponseTransportObject> convertInSysContentTypeResponseToList(List<Object[]> list)
        throws Exception
    {
        List<SysContentTypeTO> listSysContentTypeResponseTO = null;
        SysContentTypeTO sysContentTypeResponseTO = null;
        try {
            listSysContentTypeResponseTO = new ArrayList<SysContentTypeTO>();
            for (Object object[] : list) {
                sysContentTypeResponseTO = new SysContentTypeTO();
                sysContentTypeResponseTO.setCategoryName((String) object[0]);
                sysContentTypeResponseTO.setContentName((String) object[1]);
                BigInteger Idr = (BigInteger) object[2];
                long sysConentId = Idr.longValue();
                //sysContentTypeResponseTO.setContentCategoryID(sysConentId);
                sysContentTypeResponseTO.setContentCategoryID(Integer.parseInt(String.valueOf(sysConentId)));

                listSysContentTypeResponseTO.add(sysContentTypeResponseTO);
            }
            return listSysContentTypeResponseTO;
        } catch (Exception ex) {
            throw new Exception(ex.getCause());
        } finally {
            sysContentTypeResponseTO = null;
        }
    }


    public static Map<String, List<String>> getmapOfCategoryAndConstant(List<Object[]> categoryValueList)
    {
        Map<String, List<String>> catgValues = null;
        if (categoryValueList != null) {
            catgValues = new HashMap<String, List<String>>();
            List<String> valueList = null;
            for (Object object[] : categoryValueList) {
                String catgName = (String) object[0];

                if (catgValues.containsKey(catgName)) {
                    valueList = catgValues.get(catgName);

                } else {
                    valueList = new ArrayList<String>();
                }
                String value = (String) object[1];
                valueList.add(value);
                catgValues.put(catgName, valueList);
            }


        }


        return catgValues;

    }

    public static List<? extends IResponseTransportObject> convertInSysUiFormResponseToList(List<Object[]> list)
        throws Exception
    {
        List<UiFormResponseTO> listSysUiFormResponseTO = null;
        UiFormResponseTO sysUiFormResponseTO = null;
        try {
            listSysUiFormResponseTO = new ArrayList<UiFormResponseTO>();
            for (Object object[] : list) {
                sysUiFormResponseTO = new UiFormResponseTO();
                sysUiFormResponseTO.setFormID((Integer) object[0]);
                sysUiFormResponseTO.setFormName((String) object[1]);
                if (object[2] != null) {
                    sysUiFormResponseTO.setHrFormID((BigInteger) object[2]);
                }
                if (object[3] != null) {
                    sysUiFormResponseTO.setModuleID((Integer) object[3]);
                }
                if (object[4] != null) {
                    sysUiFormResponseTO.setModuleName((String) object[4]);
                }


                listSysUiFormResponseTO.add(sysUiFormResponseTO);
            }
            return listSysUiFormResponseTO;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            sysUiFormResponseTO = null;
        }
    }


    public static List<OrgMoreInfoResponseTO> getMoreInfoList(List<Object[]> resultList)
    {
        List<OrgMoreInfoResponseTO> moreInfoList = null;
        OrgMoreInfoResponseTO responseTO = null;
        String leftLogo = null;
        String rightLogo = null;
        String favicon = null;
        String title = null;
        String metaData = null;
        String resetPassword = null;
        String headingTxt = null;
        String usernameTxt = null;
        String passwordTxt = null;
        String btnTxt = null;
        String footer = null;
        String banner = null;
        String samlHeader = null;
        String samlBtnTxt = null;
        try {
            moreInfoList = new ArrayList<OrgMoreInfoResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new OrgMoreInfoResponseTO();
                if (object[0] != null) {
                    leftLogo = (String) object[0];
                    responseTO.setLeftLogo(leftLogo);
                }
                if (object[1] != null) {
                    rightLogo = (String) object[1];
                    responseTO.setRightLogo(rightLogo);
                }
                if (object[2] != null) {
                    favicon = (String) object[2];
                    responseTO.setFavicon(favicon);

                }
                if (object[3] != null) {
                    title = (String) object[3];
                    responseTO.setTitle(title);
                }
                if (object[4] != null) {
                    metaData = (String) object[4];
                    responseTO.setMetaData(metaData);
                }

                if (object[5] != null) {
                    resetPassword = (String) object[5];
                    responseTO.setResetPassword(resetPassword);
                }
                if (object[6] != null) {
                    footer = (String) object[6];
                    responseTO.setFooter(footer);
                }
                if (object[7] != null) {
                    btnTxt = (String) object[7];
                    responseTO.setBtnTxt(btnTxt);
                }
                if (object[8] != null) {
                    passwordTxt = (String) object[8];
                    responseTO.setPasswordTxt(passwordTxt);
                }
                if (object[9] != null) {
                    usernameTxt = (String) object[9];
                    responseTO.setUsernameTxt(usernameTxt);
                }
                if (object[10] != null) {
                    headingTxt = (String) object[10];
                    responseTO.setHeadingTxt(headingTxt);
                }
                if (object[11] != null) {
                    banner = (String) object[11];
                    responseTO.setBanner(banner);
                }
                if (object[12] != null) {
                    samlHeader = (String) object[12];
                    responseTO.setSamlHeader(samlHeader);
                }
                if (object[12] != null) {
                    samlBtnTxt = (String) object[12];
                    responseTO.setSamlBtnTxt(samlBtnTxt);
                }
                moreInfoList.add(responseTO);
            }
        } catch (Exception ex) {

        } finally {
            responseTO = null;

        }
        return moreInfoList;
    }

    public static List<AddressResponseTO> getCountryDropDown(List<Object[]> resultList)
    {
        List<AddressResponseTO> countryList = null;
        AddressResponseTO responseTO = null;
        String country = null;
        Long countryID = null;
        try {
            countryList = new ArrayList<AddressResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new AddressResponseTO();

                if (object[0] != null) {
                    countryID = (Long) object[0];
                    responseTO.setCountryID(countryID);
                }
                if (object[1] != null) {
                    country = (String) object[1];
                    responseTO.setCountry(country);
                }
                countryList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return countryList;
    }


    public static List<AddressResponseTO> getStateDropDown(List<Object[]> resultList)
    {
        List<AddressResponseTO> stateList = null;
        AddressResponseTO responseTO = null;
        String state = null;
        Long stateID = null;
        try {
            stateList = new ArrayList<AddressResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new AddressResponseTO();

                if (object[0] != null) {
                    stateID = (Long) object[0];
                    responseTO.setStateID(stateID);
                }
                if (object[1] != null) {
                    state = (String) object[1];
                    responseTO.setState(state);
                }
                stateList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return stateList;
    }

    public static List<AddressResponseTO> getDistrictDropDown(List<Object[]> resultList)
    {
        List<AddressResponseTO> districtList = null;
        AddressResponseTO responseTO = null;
        String district = null;
        Long districtID = null;
        try {
            districtList = new ArrayList<AddressResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new AddressResponseTO();

                if (object[0] != null) {
                    districtID = (Long) object[0];
                    responseTO.setDistrictID(districtID);
                }
                if (object[1] != null) {
                    district = (String) object[1];
                    responseTO.setDistrict(district);
                }
                districtList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return districtList;
    }


    public static List<AddressResponseTO> getCityDropDown(List<Object[]> resultList)
    {
        List<AddressResponseTO> cityList = null;
        AddressResponseTO responseTO = null;
        String city = null;
        Long cityID = null;
        try {
            cityList = new ArrayList<AddressResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new AddressResponseTO();

                if (object[0] != null) {
                    cityID = (Long) object[0];
                    responseTO.setCityID(cityID);
                }
                if (object[1] != null) {
                    city = (String) object[1];
                    responseTO.setCity(city);
                }
                cityList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return cityList;
    }


    public static List<AddressResponseTO> getZipDropDown(List<Object[]> resultList)
    {
        List<AddressResponseTO> zipList = null;
        AddressResponseTO responseTO = null;
        String zipCode = null;
        Long zipCodeID = null;
        try {
            zipList = new ArrayList<AddressResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new AddressResponseTO();

                if (object[0] != null) {
                    zipCodeID = (Long) object[0];
                    responseTO.setZipCodeID(zipCodeID);
                }
                if (object[1] != null) {
                    zipCode = (String) object[1];
                    responseTO.setZipCode(zipCode);
                }
                zipList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return zipList;
    }

    public static List<SectorCostResponseTO> getSectorDropDown(List<Object[]> resultList)
    {
        List<SectorCostResponseTO> sectorList = null;
        SectorCostResponseTO responseTO = null;
        Integer sectorId = null;
        String sectorLabel = null;
        try {
            sectorList = new ArrayList<SectorCostResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new SectorCostResponseTO();

                if (object[0] != null) {
                    sectorId = (Integer) object[0];
                    responseTO.setSectorID(sectorId);
                }
                if (object[1] != null) {
                    sectorLabel = (String) object[1];
                    responseTO.setSector(sectorLabel);
                }
                sectorList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return sectorList;
    }

    public static List<FinancialYearResponseTO> getFinancialYearList(List<Object[]> resultList)
    {
        List<FinancialYearResponseTO> FYList = null;
        FinancialYearResponseTO responseTO = null;
        Integer financialYearID = null;
        Date startDate = null;
        Date endDate = null;
        Integer organizationId = null;

        try {
            FYList = new ArrayList<FinancialYearResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new FinancialYearResponseTO();

                if (object[0] != null) {
                    financialYearID = (Integer) object[0];
                    responseTO.setFinancialYearID(financialYearID);
                }
                if (object[1] != null) {
                    startDate = (Date) object[1];
                    responseTO.setStartDate(startDate);
                }
                if (object[2] != null) {
                    endDate = (Date) object[2];
                    responseTO.setEndDate(endDate);
                }
                if (object[3] != null) {
                    organizationId = (Integer) object[3];
                    responseTO.setOrganizationId(organizationId);
                }
                FYList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return FYList;
    }

    public static List<FinancialYearResponseTO> getGovtFinancialYearList(List<Object[]> resultList)
    {
        List<FinancialYearResponseTO> FYList = null;
        FinancialYearResponseTO responseTO = null;
        Integer financialYearID = null;
        Date startDate = null;
        Date endDate = null;
        Integer organizationId = null;

        try {
            FYList = new ArrayList<FinancialYearResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new FinancialYearResponseTO();

                if (object[0] != null) {
                    financialYearID = (Integer) object[0];
                    responseTO.setFinancialYearID(financialYearID);
                }
                if (object[1] != null) {
                    startDate = (Date) object[1];
                    responseTO.setStartDate(startDate);
                }
                if (object[2] != null) {
                    endDate = (Date) object[2];
                    responseTO.setEndDate(endDate);
                }
                if (object[3] != null) {
                    organizationId = (Integer) object[3];
                    responseTO.setOrganizationId(organizationId);
                }
                FYList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return FYList;
    }

    public static List<SysContentTypeTO> getSysContentTypeDropDown(List<Object[]> resultList)
    {
        List<SysContentTypeTO> sysContentTypeList = null;
        SysContentTypeTO responseTO = null;
        String sysType = null;
        Integer sysTypeID = null;
        try {
            sysContentTypeList = new ArrayList<SysContentTypeTO>();
            for (Object[] object : resultList) {
                responseTO = new SysContentTypeTO();

                if (object[0] != null) {
                    sysTypeID = (Integer) object[0];
                    responseTO.setSysTypeID(sysTypeID);
                }
                if (object[1] != null) {
                    sysType = (String) object[1];
                    responseTO.setSysType(sysType);
                }
                sysContentTypeList.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return sysContentTypeList;
    }


    public static List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown(List<Object[]> resultList)
    {
        List<SysConstantAndSysConstantCategoryTO> list = null;
        SysConstantAndSysConstantCategoryTO responseTO = null;
        try {
            list = new ArrayList<SysConstantAndSysConstantCategoryTO>();
            for (Object[] object : resultList) {
                responseTO = new SysConstantAndSysConstantCategoryTO();

                if (object[0] != null) {
                    responseTO.setSysConstantCategoryID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysConstantCategoryName((String) object[1]);
                }
                list.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return list;
    }

    public static List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown(List<Object[]> resultList)
    {
        List<SysConstantAndSysConstantCategoryTO> list = null;
        SysConstantAndSysConstantCategoryTO responseTO = null;
        try {
            list = new ArrayList<SysConstantAndSysConstantCategoryTO>();
            for (Object[] object : resultList) {
                responseTO = new SysConstantAndSysConstantCategoryTO();
                if (object[0] != null) {
                    responseTO.setSysConstantID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysConstantName((String) object[1]);
                }
                if (object[2] != null) {
                    responseTO.setSysConstantCategoryID((Integer) object[2]);
                }
                if (object[3] != null) {
                    responseTO.setSysConstantCategoryName((String) object[3]);
                }
                list.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return list;
    }


	public static List<UiFormTO> getLandingPageList(List<Object[]> resultList) {
		// TODO Auto-generated method stub

        List<UiFormTO> list = null;
        UiFormTO responseTO = null;
        try {
            list = new ArrayList<UiFormTO>();
            for (Object[] object : resultList) { 
            	
                responseTO = new UiFormTO();
                if (object[0] != null) {
                    responseTO.setFormName((String) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setFormId((Integer) object[1]);
                }
              
                list.add(responseTO);
            }

        } catch (Exception ex) {
        } finally {
            responseTO = null;
        }
        return list;
    
	}


}
