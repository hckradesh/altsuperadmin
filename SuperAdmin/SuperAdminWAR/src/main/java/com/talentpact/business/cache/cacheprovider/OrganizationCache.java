package com.talentpact.business.cache.cacheprovider;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.loginpage.transport.output.LoginPageResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;
import com.talentpact.business.dataservice.TPOrgDS;
import com.talentpact.business.util.JsonConversionUtil;


@Stateless
public class OrganizationCache
{


    @EJB
    TPOrgDS tporgds;

    public Long deleteAltAdminLoginKeys(List<String> orgKeyListCacheList, Long organisationID)
        throws CacheUpdateException
    { 	
    	Long keysDeleted = null; 
        if(orgKeyListCacheList != null){            
        StringBuilder keybuilder = null;
        List<String> portalUrlKeysForDelete = new ArrayList<String>();
        for (String orgKey : orgKeyListCacheList) {
            String[] portalurl = orgKey.split("_");
            if (portalurl.length > 1) {
                keybuilder = new StringBuilder();
                keybuilder.append(RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY).append(portalurl[0].trim()).append("-");
                keybuilder.append(portalurl[1].trim());
                portalUrlKeysForDelete.add(keybuilder.toString().toUpperCase());
            }
        }

        if (portalUrlKeysForDelete.size() > 0) {
            try {
                keysDeleted = RedisClient.delete(portalUrlKeysForDelete);
            } catch (RedisConnectionException ex) {
                ex.printStackTrace();
                throw new CacheUpdateException("Cache Update Exception  for OrgId " + organisationID + "  StackTrace : " + ex.getMessage());
            }
        }

      

    }
        return keysDeleted;
    }


    public Long deleteAndSaveAllLoginOrgCache()
        throws CacheUpdateException
    {
        Long recordUpdated = 0l;
        Long keysDeleted = 0l;

        try {
            String orgKey = "ALTADMIN:LOGINPAGE:*";
            keysDeleted = RedisClient.deleteByPattern(orgKey);
        } catch (RedisConnectionException ex) {
            ex.printStackTrace();
            // TODO Notify It
            throw new CacheUpdateException("Redis Exception In deleting Context  " + RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY);
        }
        System.out.println("Keys Deleted " + keysDeleted);
        try {
            recordUpdated = updateCacheForTpUrlPortalLoginTO();
        } catch (Exception ex) {
            // TODO Notify It
            throw new CacheUpdateException("Redis Exception In Updating cache " + ex.getCause());
        }

        return recordUpdated;

    }

    
    
    public Long deleteAllLoginCacheDataForAllOrg()
            throws CacheUpdateException
        {
            Long recordUpdated = 0l;
            Long keysDeleted = 0l;

            try {
                String orgKey = "ALTADMIN:LOGINPAGE:*";
                keysDeleted = RedisClient.deleteByPattern(orgKey);
            } catch (RedisConnectionException ex) {
                ex.printStackTrace();
                // TODO Notify It
                throw new CacheUpdateException("Redis Exception In deleting Context  " + RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY);
            }
            System.out.println("Keys Deleted " + keysDeleted);        
            return recordUpdated;

        }


    public Long updateCacheForTpUrlPortalLoginTO()
        throws Exception
    {
        Long recordUpdated = 0l;
        try {

            List<Object[]> orgDatList = tporgds.getOrganizationData();
            List<LoginPageResponseTO> tpUrlList = traverseDbResultSetInAuthenticationTOList(orgDatList);
            recordUpdated = loadOrganizationLoginDataInCache(tpUrlList);
        } catch (Exception ex) {
            throw new Exception(ex.getCause());
        }

        return recordUpdated;
    }


    public Long updateCacheForLoginData(Long organisationID)
        throws Exception
    {
        Long recordUpdated = 0l;
        try {
            System.out.println("Save data In redis for OrgId " + organisationID);
            List<Object[]> orgDatList = tporgds.getOrganizationData(organisationID, null);
            List<LoginPageResponseTO> tpUrlList = traverseDbResultSetInAuthenticationTOList(orgDatList);
            recordUpdated = loadOrganizationLoginDataInCache(tpUrlList);

        } catch (RedisConnectionException ex) {
            throw new Exception(ex.getCause());
        } catch (Exception ex) {
            throw new Exception(ex.getCause());
        }
        return recordUpdated;

    }


    private Long loadOrganizationLoginDataInCache(List<LoginPageResponseTO> authenticateTOList)

    {

        long count = 0;
        try {
            /* key = Orgdata:PortalName:url */
            if (authenticateTOList != null) {
                String key = null;
                for (int i = 0; i < authenticateTOList.size(); i++) {
                    StringBuilder keybuilder = null;
                    LoginPageResponseTO authenticationTO = null;
                    try {
                        authenticationTO = authenticateTOList.get(i);
                        keybuilder = new StringBuilder();
                        keybuilder.append(RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY).append(authenticationTO.getDomainName()).append("-");
                        keybuilder.append(authenticationTO.getDomainName());
                        key = keybuilder.toString().toUpperCase();
                        RedisClient.putValueAsTO(key, authenticationTO);
                    } catch (Exception ex) {
                        System.out.println("Exception Occurred for  " + ex.getMessage());

                    }
                    keybuilder = null;
                    authenticationTO = null;

                }

                String ValueFromRedis = null;

              /*  for (LoginPageResponseTO authTO : authenticateTOList) {

                    StringBuilder keybuilder = new StringBuilder();
                    keybuilder.append(RedisCacheKeyConstant.ALT_ADMIN_LOGIN_KEY).append(authTO.getDomainName()).append("-");
                    keybuilder.append(authTO.getDomainName());
                    ValueFromRedis = RedisClient.getValue(keybuilder.toString().toUpperCase());
                    LoginPageResponseTO authTOFromCache = null;
                    try {
                        authTOFromCache = (LoginPageResponseTO) JsonConversionUtil.convertJSONStringToObject(ValueFromRedis, LoginPageResponseTO.class);
                        if (authTOFromCache != null) {
                            count++;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    keybuilder = null;
                    authTOFromCache = null;
                }
                authenticateTOList.clear();
                System.out.println("Organization  count updated " + count);*/

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return count;
    }


    public List<LoginPageResponseTO> traverseDbResultSetInAuthenticationTOList(List<Object[]> tpUrlList)
        throws Exception
    {

        String leftLogo = null;
        String rightLogo = null;
        String loginPage = null;
        String homePage = null;

        String favicon = null;
        String loginPageTheme = null;
        String title = null;
        String meta_Data = null;
        String defaultTheme = null;
        String appName = null;
        long urlID = 0;
        String urlName = null;
        Long orgId = 0l;
        Long tenantId = 0l;
        Long portalTypeId = 0l;
        String orgName = null;
        String orgCode = null;
        String portalName = null;
        LoginPageResponseTO authenticateTO = null;
        List<LoginPageResponseTO> authenticateTOList = null;

        try {
            if (tpUrlList != null) {
                authenticateTOList = new ArrayList<LoginPageResponseTO>();
                for (int i = 0; i < tpUrlList.size(); i++) {
                    Object[] array = tpUrlList.get(i);

                    if (array[0] != null) {
                        leftLogo = (String) array[0];
                    }
                    if (array[1] != null) {
                        rightLogo = (String) array[1];
                    }

                    if (array[2] != null) {
                        loginPage = (String) array[2];
                    }
                    if (array[3] != null) {
                        homePage = (String) array[3];
                    }

                    if (array[4] != null) {
                        favicon = (String) array[4];
                    }
                    if (array[5] != null) {
                        loginPageTheme = (String) array[5];
                    }
                    if (array[6] != null) {
                        title = (String) array[6];
                    }
                    if (array[7] != null) {
                        meta_Data = (String) array[7];
                    }
                    if (array[8] != null) {
                        defaultTheme = (String) array[8];
                    }
                    if (array[9] != null) {
                        urlName = (String) array[9];
                    }

                    if (array[10] != null) {
                        appName = (String) array[10];
                    }

                    if (array[11] != null) {
                        urlID = (Long) array[11];
                    }

                    if (array[12] != null) {
                        portalTypeId = (Long) array[12];
                    }

                    if (array[13] != null) {
                        orgId = (Long) array[13];
                    }

                    if (array[14] != null) {
                        orgName = (String) array[14];
                    }

                    if (array[15] != null) {
                        orgCode = (String) array[15];
                    }

                    if (array[16] != null) {
                        tenantId = (Long) array[16];
                    }

                    if (array[17] != null) {
                        portalName = (String) array[17];
                    }
                    if (urlName == null || portalName == null) {
                        throw new NullPointerException();
                    }
                    authenticateTO = new LoginPageResponseTO();
                    /* Fill Organization data */
                    authenticateTO.setOrganizationId(orgId);
                    authenticateTO.setOrganizationName(orgName);
                    authenticateTO.setOrganizationCode(orgCode);
                    authenticateTO.setTenantId(tenantId);
                    authenticateTO.setLeftLogo(leftLogo);
                    authenticateTO.setDomainName(urlName);
                    authenticateTO.setLoginPage(loginPage);
                    authenticateTO.setFavicon(favicon);
                    authenticateTO.setLoginPageTheme(loginPageTheme);
                    authenticateTO.setTitle(title);
                    authenticateTOList.add(authenticateTO);

                }

            }
        } catch (Exception ex) {
            throw new Exception();
        }
        return authenticateTOList;
        
    }

	public void deleteCommonLoginUrlKey(Long orgID) throws CacheUpdateException {
		String keyPattern = RedisCacheKeyConstant.REDIS_ORGANIZATION_URL_KEY_PREFIX + "*:" + orgID;		
		try {
			RedisClient.deleteByPattern(keyPattern);
			RedisClient.delete(RedisCacheKeyConstant.REDIS_ALL_URL_MAP_KEY);
		} catch (RedisConnectionException e) {
			e.printStackTrace();
            throw new CacheUpdateException("Cache Update Exception  for OrgId " + orgID);
		}
	}
	
	public void deleteMobileUrlKey(List<String> orgKeyListCacheList,int orgID) throws CacheUpdateException {
		
		try {
			if(orgKeyListCacheList!=null && !orgKeyListCacheList.isEmpty())
				for(String url : orgKeyListCacheList){
					String result = url.replaceFirst("_", ":");
					String key = RedisCacheKeyConstant.REDIS__MOBILEURL_KEY_PREFIX + result.toUpperCase();
					RedisClient.delete(key);
				}
				
		} catch (RedisConnectionException e) {
			e.printStackTrace();
            throw new CacheUpdateException("Cache Update Exception  for OrgId " + orgID);
		}
	}
	
}
