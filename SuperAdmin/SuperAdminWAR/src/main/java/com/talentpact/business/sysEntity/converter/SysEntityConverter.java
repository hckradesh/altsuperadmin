package com.talentpact.business.sysEntity.converter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.SysEntityRequestTO;
import com.talentpact.business.application.transport.output.SysEntityResponseTO;
import com.talentpact.business.common.converter.IConverter;
import com.talentpact.model.SysEntity;

/*
 * @author raghvendra.mishra
 */

public class SysEntityConverter implements IConverter {

	public List<SysEntityResponseTO> convertSysEntityToDataTO(List<SysEntity> sysEntityList){
        List<SysEntityResponseTO> resultList = null;
        SysEntityResponseTO sysEntityResponseTO = null;
        Timestamp createdDate = null;
        Timestamp modifiedDate = null;
        try {
            resultList = new ArrayList<SysEntityResponseTO>();
            for (SysEntity sysEntity : sysEntityList) {
            	sysEntityResponseTO = new SysEntityResponseTO();
            	sysEntityResponseTO.setEntityId(sysEntity.getEntityId());
            	sysEntityResponseTO.setName(sysEntity.getName());      
            	sysEntityResponseTO.setDescription(sysEntity.getDescription());
            	sysEntityResponseTO.setTableName(sysEntity.getTableName());
            	sysEntityResponseTO.setTenantID(sysEntity.getTenantID());         	
            	if(sysEntity.getCreatedDate() != null){
            		createdDate = SQLDateHelper.getSqlTimeStampFromDate(new Date(sysEntity.getCreatedDate().getTime()));
            	}
            	sysEntityResponseTO.setCreatedDate(createdDate);
            	if(sysEntity.getModifiedDate() != null){
            		modifiedDate = SQLDateHelper.getSqlTimeStampFromDate(new Date(sysEntity.getModifiedDate().getTime()));
            	}
            	sysEntityResponseTO.setModifiedDate(modifiedDate);
            	sysEntityResponseTO.setCreatedBy(sysEntity.getCreatedBy());
            	sysEntityResponseTO.setModifiedBy(sysEntity.getModifiedBy());
            	sysEntityResponseTO.setPrimaryKey(sysEntity.getPrimaryKey());
            	sysEntityResponseTO.setLableColumn(sysEntity.getLableColumn());
            	sysEntityResponseTO.setModuleId(sysEntity.getModuleId());
            	sysEntityResponseTO.setPrimaryKeyColumnID(sysEntity.getPrimaryKeyColumnID());
                resultList.add(sysEntityResponseTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }

    public SysEntityRequestTO convert(SysEntity sysEntity){
    	SysEntityRequestTO sysEntityRequestTO = null;
        try {
        	sysEntityRequestTO = new SysEntityRequestTO();
        	sysEntityRequestTO.setEntityId(sysEntity.getEntityId());
        	sysEntityRequestTO.setName(sysEntity.getName());      
        	sysEntityRequestTO.setDescription(sysEntity.getDescription());
        	sysEntityRequestTO.setTableName(sysEntity.getTableName());
        	sysEntityRequestTO.setTenantID(sysEntity.getTenantID());
        	sysEntityRequestTO.setCreatedDate(sysEntity.getCreatedDate());
        	sysEntityRequestTO.setModifiedDate(sysEntity.getModifiedDate());
        	sysEntityRequestTO.setCreatedBy(sysEntity.getCreatedBy());       	
        	sysEntityRequestTO.setModifiedBy(sysEntity.getModifiedBy());
        	sysEntityRequestTO.setPrimaryKey(sysEntity.getPrimaryKey());
        	sysEntityRequestTO.setLableColumn(sysEntity.getLableColumn());      
        	sysEntityRequestTO.setPrimaryKeyColumnID(sysEntity.getPrimaryKeyColumnID());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sysEntityRequestTO;

    }
}
