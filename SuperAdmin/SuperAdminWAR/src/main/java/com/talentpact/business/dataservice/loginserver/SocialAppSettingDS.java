/**
 * 
 */
package com.talentpact.business.dataservice.loginserver;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.ats.core.entity.TpSocialAppSettings;
import com.talentpact.auth.entity.TpURL;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.dataservice.AbstractDS;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SocialAppSettingDS extends AbstractDS<TpSocialAppSettings>
{
    public SocialAppSettingDS()
    {
        super(TpSocialAppSettings.class);
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void insertIntoSocialAppSettings(SocialAppSettingTO socialAppSettingTO) throws Exception
    {
        TpSocialAppSettings tpSocialAppSettings = null;
        TpURL tpUrl=null;
        try {
            tpUrl=findUrlById(socialAppSettingTO.getUrlID());
            tpSocialAppSettings= findSocialAppSettings(socialAppSettingTO.getUrlID(), socialAppSettingTO.getSocialAppType());
            if(tpSocialAppSettings==null){
            tpSocialAppSettings = new TpSocialAppSettings();
            tpSocialAppSettings.setSocialAppID(getMaxID());
            tpSocialAppSettings.setAccessLevel(socialAppSettingTO.getAccessLevel());
            tpSocialAppSettings.setApiKey(socialAppSettingTO.getAppKey());
            tpSocialAppSettings.setSocialAppName(socialAppSettingTO.getAppName());
            tpSocialAppSettings.setSecretKey(socialAppSettingTO.getAppSecretKey());
            tpSocialAppSettings.setClientID(socialAppSettingTO.getClientID());
            tpSocialAppSettings.setForwardUri(socialAppSettingTO.getForwardUrl());
            tpSocialAppSettings.setSocialAppType(socialAppSettingTO.getSocialAppType());
            tpSocialAppSettings.setRedirectUri(socialAppSettingTO.getRedirectUrl());
            tpSocialAppSettings.setDefaultCSS(socialAppSettingTO.getDefaultCSS());
            tpSocialAppSettings.setUrl(tpUrl);
            getEntityManager("TalentpactAuth").persist(tpSocialAppSettings);
            }else{
                tpSocialAppSettings.setAccessLevel(socialAppSettingTO.getAccessLevel());
                tpSocialAppSettings.setApiKey(socialAppSettingTO.getAppKey());
                tpSocialAppSettings.setSocialAppName(socialAppSettingTO.getAppName());
                tpSocialAppSettings.setSecretKey(socialAppSettingTO.getAppSecretKey());
                tpSocialAppSettings.setClientID(socialAppSettingTO.getClientID());
                tpSocialAppSettings.setForwardUri(socialAppSettingTO.getForwardUrl());
                tpSocialAppSettings.setSocialAppType(socialAppSettingTO.getSocialAppType());
                tpSocialAppSettings.setRedirectUri(socialAppSettingTO.getRedirectUrl());
                tpSocialAppSettings.setDefaultCSS(socialAppSettingTO.getDefaultCSS());
                getEntityManager("TalentpactAuth").merge(tpSocialAppSettings);
            }
        } catch (Exception e) {
            throw e;
        }

    }

    private TpURL findUrlById(Long urlID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        TpURL tpUrl=null;
        try{
            hqlQuery=new StringBuilder();
            hqlQuery.append(" select tu from TpURL tu where tu.urlID=:urlID");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            query.setParameter("urlID", urlID);
            tpUrl=(TpURL) query.getSingleResult();
            
        }catch(Exception e){
            throw e;
        }
        return tpUrl;
    }


    @SuppressWarnings("unchecked")
    public List<TpSocialAppSettings> getSocialAppSettingsByLoginServerType(String appType, Long urlID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpSocialAppSettings> tpSocialAppSettingsList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from TpSocialAppSettings s where s.socialAppType=:appType and s.url.urlID=:urlID");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            query.setParameter("appType", appType);
            query.setParameter("urlID", urlID);
            tpSocialAppSettingsList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return tpSocialAppSettingsList;
    }


    @SuppressWarnings("unchecked")
    public Integer getMaxID()
        throws Exception
    {
        List<Long> typeResultList = null;
        StringBuilder hqlquery = null;
        Query query = null;
        Integer maxSequence = 0;
        try {
            hqlquery = new StringBuilder();
            hqlquery.append("select MAX(ds.socialAppID) from TpSocialAppSettings ds ");
            query = getEntityManager().createQuery(hqlquery.toString());
            typeResultList = query.getResultList();
            if (typeResultList!=null && !typeResultList.isEmpty()) {
                if (typeResultList.get(0) != null)
                    maxSequence = Integer.valueOf(typeResultList.get(0).intValue());
            }
            return maxSequence + 1;
        } catch (Exception e) {
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    public TpSocialAppSettings findSocialAppSettings(Long urlID,String typeName)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpSocialAppSettings> appSettingsList = null;
        TpSocialAppSettings tpSocialAppSettings=null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from TpSocialAppSettings s where s.url.urlID=:urlID and s.socialAppType=:typeName ");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            query.setParameter("urlID", urlID);
            query.setParameter("typeName", typeName.toLowerCase());
            appSettingsList = query.getResultList();
            if(!appSettingsList.isEmpty()){
                tpSocialAppSettings  =appSettingsList.get(0); 
            }
        } catch (Exception e) {
            throw e;
        }
        return tpSocialAppSettings;
    }



}