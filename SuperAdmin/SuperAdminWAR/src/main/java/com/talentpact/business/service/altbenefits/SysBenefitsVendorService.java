/**
 * 
 */
package com.talentpact.business.service.altbenefits;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.CommonUtilHelper;
import com.alt.role.constants.IRoleConstants;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.SysBenefitsVendorDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingPeakDetailDS;
import com.talentpact.business.dataservice.altbenefits.SysVendorOfferingTemplateDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;
import com.talentpact.business.dataservice.timeDimension.TimeDimensionDS;
import com.talentpact.model.altbenefit.SysBenefitsVendor;
import com.talentpact.ui.altbenefits.transport.SysBenefitVendorTO;

/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class SysBenefitsVendorService extends CommonService
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysBenefitsVendorService.class);

    @EJB
    SysBenefitsVendorDS sysBenefitsVendorDS;

    @EJB
    TimeDimensionDS timeDimensionDS;

    @EJB
    SysVendorOfferingPeakDetailDS sysVendorOfferingPeakDetailDS;

    @EJB
    SysVendorOfferingTemplateDS offeringTemplateDS;

    @EJB
    HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

    @EJB
    SysSectorVendorOfferingSequenceService sysSectorVendorOfferingSequenceService;

    @EJB
    AltBenefitsCache altBenefitsCache;

    /**
     * 
     * @return
     * @throws Exception
     */

    public List<SysBenefitVendorTO> getSysBenefitsVendorList()
        throws Exception
    {
        List<SysBenefitVendorTO> benefitVendorTOList = null;
        SysBenefitVendorTO sysBenefitVendorTO = null;
        List<SysBenefitsVendor> benefitVendorResultList = null;

        try {
            benefitVendorResultList = sysBenefitsVendorDS.getSysBenefitsVendorList();
            benefitVendorTOList = new ArrayList<SysBenefitVendorTO>();
            for (SysBenefitsVendor vendor : benefitVendorResultList) {
                sysBenefitVendorTO = new SysBenefitVendorTO();
                sysBenefitVendorTO.setVendorID(vendor.getVendorID());
                sysBenefitVendorTO.setVendorName(vendor.getVendorName());
                sysBenefitVendorTO.setVendorCode(vendor.getVendorCode());
                sysBenefitVendorTO.setContractSignedDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractSignedDate()));
                sysBenefitVendorTO.setContractStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractStartDate()));
                if (vendor.getContractEndDate() != null) {
                    sysBenefitVendorTO.setContractEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getContractEndDate()));
                }
                sysBenefitVendorTO.setActive(vendor.getActive());
                if (vendor.getEffectiveStartDate() != null) {
                    sysBenefitVendorTO.setEffectiveStartDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveStartDate()));
                }
                if (vendor.getEffectiveEndDate() != null) {
                    sysBenefitVendorTO.setEffectiveEndDate(CommonUtilHelper.getDateFromTimeDimensionId(vendor.getEffectiveEndDate()));
                }
                if (vendor.getActive() != null && vendor.getActive()) {
                    sysBenefitVendorTO.setStatus(IRoleConstants.ACTIVE_STRING);
                } else {
                    sysBenefitVendorTO.setStatus(IRoleConstants.INACTIVE_STRING);
                }
                sysBenefitVendorTO.setContractSignedDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(sysBenefitVendorTO.getContractSignedDate()));
                sysBenefitVendorTO.setContractStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(sysBenefitVendorTO.getContractStartDate()));
                sysBenefitVendorTO.setContractEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(sysBenefitVendorTO.getContractEndDate()));
                sysBenefitVendorTO.setEffectiveStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(sysBenefitVendorTO.getEffectiveStartDate()));
                sysBenefitVendorTO.setEffectiveEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(sysBenefitVendorTO.getEffectiveEndDate()));
                sysBenefitVendorTO.setTermAndConditions(vendor.getTermAndConditions());
                sysBenefitVendorTO.setVendorLogo(vendor.getVendorLogo());
                benefitVendorTOList.add(sysBenefitVendorTO);
            }
            return benefitVendorTOList;
        } catch (Exception e) {
            LOGGER_.error(e.getMessage(), e);
            throw e;
        }
    }


    /**
     * 
     * @param benefitVendorTO
     * @return
     * @throws Exception
     */

    public boolean saveSysBenefitsVendorDetail(SysBenefitVendorTO benefitVendorTO)
        throws Exception
    {
        boolean success = false;
        try {
            benefitVendorTO.setContractSignedDateId(timeDimensionDS.getTimeDimensionFromDate(benefitVendorTO.getContractSignedDate()).getTimeDimensionId());
            benefitVendorTO.setContractStartDateId(timeDimensionDS.getTimeDimensionFromDate(benefitVendorTO.getContractStartDate()).getTimeDimensionId());
            if (benefitVendorTO.getContractEndDate() != null) {
                benefitVendorTO.setContractEndDateId(timeDimensionDS.getTimeDimensionFromDate(benefitVendorTO.getContractEndDate()).getTimeDimensionId());
            }
            if (benefitVendorTO.getEffectiveStartDate() != null) {
                benefitVendorTO.setEffectiveStartDateId(timeDimensionDS.getTimeDimensionFromDate(benefitVendorTO.getEffectiveStartDate()).getTimeDimensionId());
            }
            if (benefitVendorTO.getEffectiveEndDate() != null) {
                benefitVendorTO.setEffectiveEndDateId(timeDimensionDS.getTimeDimensionFromDate(benefitVendorTO.getEffectiveEndDate()).getTimeDimensionId());
            }
            sysBenefitsVendorDS.saveSysBenefitsVendorDetail(benefitVendorTO);
            success = true;

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return success;
    }

    /**
     * 
     * @param vendorId
     * @param status
     * @return
     * @throws Exception
     */
    public boolean changeTheStatusForBenefitsVendor(Long vendorId, boolean status)
        throws Exception
    {
        boolean success = false;
        try {
            sysBenefitsVendorDS.changeTheStatusForBenefitsVendor(vendorId, status);
            success = true;
            hrVendorOfferingSequenceService.changeTheStatusHrOfferingTemplate();
            altBenefitsCache.deleteSystemVendorOfferingsTemplatesRedisKey();
            altBenefitsCache.deleteOrganizationWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSectorWiseVendorOfferingsRedisKey();
            altBenefitsCache.deleteSystemVendorOfferingsList();
            altBenefitsCache.deleteVendorOfferingsTemplateCheckListRedisKey();
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return success;
    }


}