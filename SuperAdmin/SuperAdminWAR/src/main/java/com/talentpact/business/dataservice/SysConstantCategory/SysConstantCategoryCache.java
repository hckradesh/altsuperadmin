/**
 * 
 */
package com.talentpact.business.dataservice.SysConstantCategory;

import java.util.List;

import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysConstantCategoryResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
public class SysConstantCategoryCache
{
    public List<SysConstantCategoryResponseTO> getSysConstantCategoy()
        throws Exception
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORY";
        return RedisClient.getValueAsList(key, SysConstantCategoryResponseTO.class);
    }

    public void persistSysConstantCategoy(List<SysConstantCategoryResponseTO> catList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORY";
        RedisClient.putValueAsTO(key, catList);
    }

    public void removeSysConstantCategoy()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORY";
        RedisClient.delete(key);
    }


}
