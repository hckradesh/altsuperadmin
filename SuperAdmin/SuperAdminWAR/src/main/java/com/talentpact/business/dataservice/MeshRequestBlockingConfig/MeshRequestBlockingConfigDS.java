/**
 * 
 */
package com.talentpact.business.dataservice.MeshRequestBlockingConfig;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import com.talentpact.business.application.transport.output.MeshRequestBlockingConfigTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.MeshRequestBlockingConfig;
import com.talentpact.model.altbenefit.MeshRequestType;


@Stateless
public class MeshRequestBlockingConfigDS extends AbstractDS<MeshRequestBlockingConfigDS> {
	public MeshRequestBlockingConfigDS() {
		super(MeshRequestBlockingConfigDS.class);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getMeshRequestBlockingConfigList() {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> list = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(
					"select mrbc.blockingConfigID, mrbc.requestTypeId, mrbc.blockingRequestTypeId, mrbc.requestType.requestTypeName ,mrbc.blockingRequestType.requestTypeName from MeshRequestBlockingConfig mrbc order by mrbc.blockingConfigID");
			query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
			list = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public boolean addMeshRequestBlockingConfig(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO) {
		MeshRequestBlockingConfig meshRequestBlockingConfig = new MeshRequestBlockingConfig();
		try {
			meshRequestBlockingConfig
					.setRequestType(findRequestTypeByID(Long.valueOf(meshRequestBlockingConfigTO.getRequestTypeID())));
			meshRequestBlockingConfig.setBlockingRequestType(
					findRequestTypeByID(Long.valueOf(meshRequestBlockingConfigTO.getBlockingRequestTypeID())));
			meshRequestBlockingConfig.setRequestTypeId(meshRequestBlockingConfigTO.getRequestTypeID());
			meshRequestBlockingConfig.setBlockingRequestTypeId(meshRequestBlockingConfigTO.getBlockingRequestTypeID());
			getEntityManager("AltDataManager").persist(meshRequestBlockingConfig);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	private MeshRequestType findRequestTypeByID(Long requestTypeId) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<MeshRequestType> meshMeshRequestTypeList = null;
		MeshRequestType result = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append("select mrt from MeshRequestType mrt where mrt.requestTypeId=:requestTypeId ");
			query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
			query.setParameter("requestTypeId", requestTypeId);
			meshMeshRequestTypeList = query.getResultList();
			if (meshMeshRequestTypeList != null && meshMeshRequestTypeList.size() > 0) {
				result = meshMeshRequestTypeList.get(0);
			}
			return result;
		} catch (Exception ex) {
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public boolean editMeshRequestBlockingConfig(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO) {
		MeshRequestBlockingConfig meshRequestBlockingConfig = new MeshRequestBlockingConfig();
		try {
			meshRequestBlockingConfig = findMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
			meshRequestBlockingConfig.setBlockingRequestType(
					findRequestTypeByID(Long.valueOf(meshRequestBlockingConfigTO.getBlockingRequestTypeID())));
			meshRequestBlockingConfig
					.setRequestType(findRequestTypeByID(Long.valueOf(meshRequestBlockingConfigTO.getRequestTypeID())));
			meshRequestBlockingConfig.setRequestTypeId(meshRequestBlockingConfigTO.getRequestTypeID());
			meshRequestBlockingConfig.setBlockingRequestTypeId(meshRequestBlockingConfigTO.getBlockingRequestTypeID());
			getEntityManager("AltDataManager").merge(meshRequestBlockingConfig);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private MeshRequestBlockingConfig findMeshRequestBlockingConfig(
			MeshRequestBlockingConfigTO meshRequestBlockingConfigTO) throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<MeshRequestBlockingConfig> meshRequestBlockingConfigList = null;
		MeshRequestBlockingConfig result = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append(
					"select mrbc from MeshRequestBlockingConfig mrbc where mrbc.blockingConfigID=:blockingConfigID ");
			query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
			query.setParameter("blockingConfigID", meshRequestBlockingConfigTO.getBlockingConfigID());
			meshRequestBlockingConfigList = query.getResultList();
			if (meshRequestBlockingConfigList != null && meshRequestBlockingConfigList.size() > 0) {
				result = meshRequestBlockingConfigList.get(0);
			}
			return result;
		} catch (Exception ex) {
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	public List<MeshRequestType> getMeshRequestTypes() throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<MeshRequestType> meshRequestTypes = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append("select mrt from MeshRequestType mrt");
			query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
			meshRequestTypes = query.getResultList();
			if (meshRequestTypes != null && meshRequestTypes.size() > 0) {
				return meshRequestTypes;
			}
			return null;
		} catch (Exception ex) {
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
		}
	}

	public Object[] checkExists(Integer requestTypeId, Integer blockingRequestTypeId) throws Exception {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> meshRequestBlockingConfigList = null;
		try {

			hqlQuery = new StringBuilder();
			hqlQuery.append(
					"select mrbc.blockingConfigID, mrbc.requestTypeId, mrbc.blockingRequestTypeId, mrbc.requestType.requestTypeName ,mrbc.blockingRequestType.requestTypeName from MeshRequestBlockingConfig mrbc where mrbc.requestTypeId=:requestTypeId and mrbc.blockingRequestTypeId=:blockingRequestTypeId ");
			query = getEntityManager("AltDataManager").createQuery(hqlQuery.toString());
			query.setParameter("requestTypeId", requestTypeId);
			query.setParameter("blockingRequestTypeId", blockingRequestTypeId);
			meshRequestBlockingConfigList = query.getResultList();
			if (meshRequestBlockingConfigList != null && meshRequestBlockingConfigList.size() > 0) {
				return meshRequestBlockingConfigList.get(0);
			}
			return null;
		} catch (Exception ex) {
			throw ex;
		} finally {
			hqlQuery = null;
			query = null;
		}
	}
}