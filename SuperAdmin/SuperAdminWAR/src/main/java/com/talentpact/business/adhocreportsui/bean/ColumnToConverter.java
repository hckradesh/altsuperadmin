package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.ui.business.to.ColumnTo;
import com.talentpact.adhocreports.ui.business.to.TableTo;

@FacesConverter(value = "columnToConverter", forClass = ColumnTo.class)
public class ColumnToConverter implements Converter, Serializable {

	private static final long serialVersionUID = 4555475661811449063L;
	private static final Logger LOGGER = Logger.getLogger(ColumnToConverter.class);
	private static final String SEPARATOR_CONSTANT = "__";
	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
	   	TableToConverter tableToConverter = new TableToConverter();
    	SelectItemConverter selectItemConverter = new SelectItemConverter();
        if (value != null && !value.equals("")) {
        	ColumnTo item = new ColumnTo();
            try {
            	String[] splits = value.split(SEPARATOR_CONSTANT);
        		TableTo table = (TableTo) tableToConverter.getAsObject(context, component, splits[0]);
        		SelectItem selectItem = (SelectItem) selectItemConverter.getAsObject(context, component, splits[1]);
        		item.setColumn(selectItem);
        		item.setColumnTableTo(table);
                return item;
            } catch (Exception e) {
            	LOGGER.error("Column TO Converter for value : " + value + " error.",e);
            }
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	TableToConverter tableToConverter = new TableToConverter();
    	SelectItemConverter selectItemConverter = new SelectItemConverter();
        if (value != null && value instanceof ColumnTo) {
            return tableToConverter.getAsString(context, component, ((ColumnTo)value).getColumnTableTo())
            		+ SEPARATOR_CONSTANT + selectItemConverter.getAsString(context, component, ((ColumnTo)value).getColumn());
        } else
            return "";
    }
}
