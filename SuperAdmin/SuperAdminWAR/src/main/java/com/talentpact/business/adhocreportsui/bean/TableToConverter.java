package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.adhocreports.business.common.controller.EnumTableType;
import com.talentpact.adhocreports.business.common.controller.SelectItem;

@FacesConverter(value = "tableToConverter", forClass = TableTo.class)
public class TableToConverter implements Converter, Serializable {

	private static final long serialVersionUID = 4551384433724833123L;
	private static final Logger LOGGER = Logger.getLogger(TableToConverter.class);
	private static final String LABEL_CONSTANT = "label:";
	private static final String VALUE_CONSTANT = ",value:";
	private static final String ENUMTABLETYPE_CONSTANT=",enumTableType:";
	private static final String ORGANIZATION_CONSTANT=",organization:";

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && !value.equals("")) {
        	TableTo item = new TableTo();
        	SelectItemConverter selectItemConverter = new SelectItemConverter();
            try {
                if (value.contains(LABEL_CONSTANT)){
                    item.setName(value.substring(value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length(), 
                    		value.indexOf(VALUE_CONSTANT, value.indexOf(LABEL_CONSTANT) + LABEL_CONSTANT.length())));
                }
                if (value.contains(VALUE_CONSTANT)){
                    item.setId(new Integer(value.substring(value.indexOf(VALUE_CONSTANT) + VALUE_CONSTANT.length(), 
                    		value.indexOf(ENUMTABLETYPE_CONSTANT, value.indexOf(VALUE_CONSTANT) + VALUE_CONSTANT.length()))));
                }
                if (!value.contains(ORGANIZATION_CONSTANT)){
                	if (value.contains(ENUMTABLETYPE_CONSTANT)){
                		item.setTableType(EnumTableType.valueOf(value.substring(value.indexOf(ENUMTABLETYPE_CONSTANT) + ENUMTABLETYPE_CONSTANT.length(), 
                    		value.length()).toUpperCase()));
                	}
                } else {
                	if (value.contains(ENUMTABLETYPE_CONSTANT)){
                		item.setTableType(EnumTableType.valueOf(value.substring(value.indexOf(ENUMTABLETYPE_CONSTANT) + ENUMTABLETYPE_CONSTANT.length(), 
                			value.indexOf(ORGANIZATION_CONSTANT, value.indexOf(ENUMTABLETYPE_CONSTANT) + ENUMTABLETYPE_CONSTANT.length()))));
                	}
                	if (value.contains(ORGANIZATION_CONSTANT)){
                		String selectItemString = value.substring(value.indexOf(ORGANIZATION_CONSTANT) + ORGANIZATION_CONSTANT.length(), 
                        		value.length()).toUpperCase();
                		SelectItem selectItem = (SelectItem) selectItemConverter.getAsObject(context, component, selectItemString);
                		item.setOrganization(selectItem);
                	}
                }
                
                return item;
            } catch (Exception e) {
            	LOGGER.error("Table TO Converter for value : " + value + " error.",e);
            }
        }
        return null;
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	SelectItemConverter selectItemConverter = new SelectItemConverter();
    	String result = "";
        if (value != null && value instanceof TableTo) {
            result += LABEL_CONSTANT + ((TableTo) value).getName() + VALUE_CONSTANT + ((TableTo) value).getId() +
            		ENUMTABLETYPE_CONSTANT + ((TableTo) value).getTableType().getName();
        	
            if(((TableTo) value).getOrganization() != null) {
            	result += ORGANIZATION_CONSTANT + selectItemConverter.getAsString(context, component,((TableTo) value).getOrganization());
            }
            return result;
        } else
            return "";
    }
    
}
