package com.talentpact.business.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.type.JavaType;

import com.talentpact.business.common.exceptions.JSONConversionException;

public class JsonConversionUtil
{

    public static <T> String convertObjectToJSONString(T obj)
        throws JSONConversionException
    {
        return convertObjectToJSONString(obj, false);
    }


    public static <T> String convertObjectToJSONString(T obj, boolean disableFailOnEmptyBean)
        throws JSONConversionException
    {
        ObjectMapper om = new ObjectMapper();
        if (disableFailOnEmptyBean) {
            om.disable(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS);
        }
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        String json = null;
        try {
            json = ow.writeValueAsString(obj);
        } catch (JsonGenerationException e) {
            throw new JSONConversionException(e.getCause());
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw new JSONConversionException(e.getCause());
        } catch (IOException e) {
            throw new JSONConversionException(e.getCause());
        } finally {
            ow = null;
        }
        return json;

    }


    public static <T> T convertJSONStringToObject(String jsonString, Class<T> objType)
        throws JSONConversionException
    {
        ObjectMapper objectReader = null;
        T object = null;
        objectReader = new ObjectMapper();
        try {
            object = objectReader.readValue(jsonString, objType);
        } catch (JsonParseException e) {
            throw new JSONConversionException(e.getCause());
        } catch (JsonMappingException e) {
            throw new JSONConversionException(e.getCause());
        } catch (IOException e) {
            throw new JSONConversionException(e.getCause());
        }

        return object;

    }


    public static <T> List<T> convertJSONStringToList(String jsonString, Class<T> objType)
        throws JSONConversionException
    {
        ObjectMapper objectReader = null;
        List<T> resultList = null;
        objectReader = new ObjectMapper();
        try {
            CollectionType listOfTOType = objectReader.getTypeFactory().constructCollectionType(List.class, objType);
            resultList = objectReader.readValue(jsonString, listOfTOType);

        } catch (JsonParseException e) {
            throw new JSONConversionException(e.getCause());
        } catch (JsonMappingException e) {
            throw new JSONConversionException(e.getCause());
        } catch (IOException e) {
            throw new JSONConversionException(e.getCause());
        }

        return resultList;

    }


    public static <V> HashMap<String, List<V>> convertJSONStringToObjectMap(String jsonString, Class<V> valueClass)
        throws JSONConversionException
    {

        ObjectMapper objectReader = null;
        HashMap<String, List<V>> objectMap = null;
        objectReader = new ObjectMapper();
        try {
            CollectionType listOfLongType = objectReader.getTypeFactory().constructCollectionType(ArrayList.class, valueClass);
            JavaType stringType = objectReader.getTypeFactory().uncheckedSimpleType(String.class);
            MapType mapType = objectReader.getTypeFactory().constructMapType(HashMap.class, stringType, listOfLongType);

            objectMap = objectReader.readValue(jsonString, mapType);
        } catch (JsonParseException e) {
            throw new JSONConversionException(e.getCause());
        } catch (JsonMappingException e) {
            throw new JSONConversionException(e.getCause());
        } catch (IOException e) {
            throw new JSONConversionException(e.getCause());
        }

        return objectMap;

    }

    public static <T, V> HashMap<T, List<V>> convertJSONStringToObjectMapWithGenericKey(String jsonString, Class<T> keyType, Class<V> valueClass)
        throws JSONConversionException
    {
        ObjectMapper objectReader = null;
        HashMap<T, List<V>> objectMap = null;
        objectReader = new ObjectMapper();
        try {
            CollectionType listOfLongType = objectReader.getTypeFactory().constructCollectionType(ArrayList.class, valueClass);
            JavaType genericType = objectReader.getTypeFactory().uncheckedSimpleType(keyType);
            MapType mapType = objectReader.getTypeFactory().constructMapType(HashMap.class, genericType, listOfLongType);

            objectMap = objectReader.readValue(jsonString, mapType);
        } catch (JsonParseException e) {
            throw new JSONConversionException(e.getCause());
        } catch (JsonMappingException e) {
            throw new JSONConversionException(e.getCause());
        } catch (IOException e) {
            throw new JSONConversionException(e.getCause());
        } catch (Exception ex) {
            throw new JSONConversionException(ex.getCause());
        }
        return objectMap;
    }


    public static <V> HashMap<String, V> convertJSONStringToObjectTO(String jsonString, Class<V> valueClass)
            throws JSONConversionException
        {

            ObjectMapper objectReader = null;
            HashMap<String, V> objectMap = null;
            objectReader = new ObjectMapper();
            try {
                CollectionType listOfLongType = objectReader.getTypeFactory().constructCollectionType(ArrayList.class, valueClass);
                JavaType stringType = objectReader.getTypeFactory().uncheckedSimpleType(String.class);
                MapType mapType = objectReader.getTypeFactory().constructMapType(HashMap.class, stringType, listOfLongType);

                objectMap = objectReader.readValue(jsonString, mapType);
            } catch (JsonParseException e) {
                throw new JSONConversionException(e.getCause());
            } catch (JsonMappingException e) {
                throw new JSONConversionException(e.getCause());
            } catch (IOException e) {
                throw new JSONConversionException(e.getCause());
            }

            return objectMap;

        }

    
}
