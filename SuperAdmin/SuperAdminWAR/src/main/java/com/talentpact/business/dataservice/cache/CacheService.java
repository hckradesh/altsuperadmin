/**
 * 
 */
package com.talentpact.business.dataservice.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import com.talentpact.business.dataservice.TPOrgDS;
import com.talentpact.business.dataservice.altbenefits.cache.AltBenefitsCache;

@Stateless
public class CacheService
{
    private static final Logger LOGGER_ = Logger.getLogger(CacheService.class.toString());

    @EJB
    TPOrgDS tpOrgDS;

    @EJB
    AltBenefitsCache altBenefitsCache;

    public void loadOrgnanizationSectorMap()
    {
        List<Object[]> orgObjectList = null;
        Map<String, String> orgSectorMap = null;
        try {
            orgObjectList = tpOrgDS.getAllOrganizationList();
            orgSectorMap = new HashMap<String, String>();
            if (orgObjectList != null && !orgObjectList.isEmpty()) {
                for (Object[] ob : orgObjectList) {
                    orgSectorMap.put(ob[0].toString(),ob[2].toString());

                }
            }

            altBenefitsCache.putOrganizationSectorMap(orgSectorMap);
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

}
