/**
 * 
 */
package com.talentpact.business.dataservice.MeshRequestType;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.MeshRequestType;
import com.talentpact.model.configuration.SysContentCategory;

/**
 * 
 * @author vivek.goyal
 * 
 */

@Stateless
public class MeshRequestTypeDS extends AbstractDS<MeshRequestType> {
	public MeshRequestTypeDS() {
		super(MeshRequestType.class);
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getMeshRequestTypeList() {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<Object[]> list = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select meshRequestType.requestTypeId, meshRequestType.requestTypeName "
					+ "from MeshRequestType meshRequestType order by meshRequestType.requestTypeName");
			query = getEntityManager("AltDataManager").createQuery(
					hqlQuery.toString());
			list = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public boolean addMeshRequestType(MeshRequestTypeTO meshRequestTypeTO) {
		MeshRequestType meshRequestType = new MeshRequestType();
		try {
			meshRequestType.setRequestTypeName(meshRequestTypeTO
					.getRequestTypeName());
			meshRequestType.setCreatedBy(1);
			meshRequestType.setModifiedBy(1);
			meshRequestType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
			meshRequestType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			getEntityManager("AltDataManager").persist(meshRequestType);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public boolean editMeshRequestType(MeshRequestTypeTO meshRequestTypeTO) {
		try {
			MeshRequestType meshRequestType = getEntityManager("AltDataManager")
					.find(MeshRequestType.class,
							meshRequestTypeTO.getRequestTypeId());
			meshRequestType.setRequestTypeName(meshRequestTypeTO
					.getRequestTypeName());
			meshRequestType.setModifiedBy(1);
			meshRequestType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			getEntityManager("AltDataManager").merge(meshRequestType);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/*
	 * @SuppressWarnings("unchecked") private MeshRequestType
	 * findMeshRequestType( MeshRequestTypeTO meshRequestTypeTO) throws
	 * Exception { Query query = null; StringBuilder hqlQuery = null;
	 * List<MeshRequestType> meshRequestTypeList = null; MeshRequestType result
	 * = null; try {
	 * 
	 * hqlQuery = new StringBuilder(); hqlQuery.append(
	 * "select mrt from MeshRequestType mrt  where soc.requestTypeId=:requestTypeId "
	 * ); query = getEntityManager("AltDataManager").createQuery(
	 * hqlQuery.toString()); query.setParameter("requestTypeId",
	 * meshRequestTypeTO.getRequestTypeId()); meshRequestTypeList =
	 * query.getResultList(); if (meshRequestTypeList != null &&
	 * meshRequestTypeList.size() > 0) { result = meshRequestTypeList.get(0); }
	 * return result; } catch (Exception ex) { throw ex; } finally { hqlQuery =
	 * null; query = null; } }
	 */
}