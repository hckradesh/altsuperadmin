/**
 * 
 */
package com.talentpact.business.dataservice.SysParameter;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysParameterResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.SysParameter.cache.SysParameterCache;
import com.talentpact.ui.common.bean.UserSessionBean;


/**
 * 
 * @author Rahul.Chabba
 *
 */


@Stateless
public class SysParameterService extends CommonService
{
    private final Logger LOGGER_ = LoggerFactory.getLogger(SysParameterService.class);

    @EJB
    SysParameterDS sysParameterDS;

    @Inject
    UserSessionBean userSessionBean;

    @EJB
    SysParameterCache sysParameterCache;

    public List<SysParameterResponseTO> getListingData()
    {
        List<SysParameterResponseTO> list = null;
        SysParameterResponseTO sysParameterResponseTO = null;
        List<Object[]> sysparamList = null;
        try {
            sysparamList = sysParameterDS.getListingData();
            list = new ArrayList<SysParameterResponseTO>();
            for (Object[] sysParamResult : sysparamList) {
                sysParameterResponseTO = new SysParameterResponseTO();
                if (sysParamResult[0] != null) {
                    sysParameterResponseTO.setParamID((Integer) sysParamResult[0]);
                }
                if (sysParamResult[1] != null) {
                    sysParameterResponseTO.setParamLabel((String) sysParamResult[1]);
                }
                if (sysParamResult[2] != null) {
                    sysParameterResponseTO.setDataTypeName((String) sysParamResult[2]);
                }
                if (sysParamResult[3] != null) {
                    sysParameterResponseTO.setParamDescription((String) sysParamResult[3]);
                }
                list.add(sysParameterResponseTO);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public List<SysParameterResponseTO> dataTypeDropDownList()
    {
        List<SysParameterResponseTO> list = null;
        SysParameterResponseTO sysParameterResponseTO = null;
        List<Object[]> sysparamList = null;
        try {
            sysparamList = sysParameterDS.dataTypeDropDownList();
            list = new ArrayList<SysParameterResponseTO>();
            for (Object[] sysParamResult : sysparamList) {
                sysParameterResponseTO = new SysParameterResponseTO();
                if (sysParamResult[0] != null) {
                    sysParameterResponseTO.setDataTypeName((String) sysParamResult[0]);
                }
                if (sysParamResult[1] != null) {
                    sysParameterResponseTO.setDataTypeId((Integer) sysParamResult[1]);
                }
                list.add(sysParameterResponseTO);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public boolean saveSysParameter(SysParameterResponseTO sysParameterResponseTO)
        throws Exception
    {
        boolean status = false;
        try {

            status = sysParameterDS.saveSysParameter(sysParameterResponseTO);
            RedisClient.deleteByPattern("ALTADMIN:HRPAYROLLPARAMETERPOLICYSYSPAYCODEPARAMETERMAPPINGDROPDROWN:" + "*");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public boolean editData(SysParameterResponseTO sysParameterResponseTO)
        throws Exception
    {
        boolean status = false;
        try {
            status = sysParameterDS.editData(sysParameterResponseTO);
            RedisClient.deleteByPattern("ALTADMIN:HRPAYROLLPARAMETERPOLICYSYSPAYCODEPARAMETERMAPPINGDROPDROWN:" + "*");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public List<SysParameterResponseTO> geteditData(SysParameterResponseTO sysParameterResponseTOo)
    {
        List<SysParameterResponseTO> list = null;
        SysParameterResponseTO sysParameterResponseTO = null;
        List<Object[]> sysparamList = null;
        try {
            sysparamList = sysParameterDS.geteditData(sysParameterResponseTOo);
            list = new ArrayList<SysParameterResponseTO>();
            for (Object[] sysParamResult : sysparamList) {
                sysParameterResponseTO = new SysParameterResponseTO();
                if (sysParamResult[0] != null) {
                    sysParameterResponseTO.setParamID((Integer) sysParamResult[0]);
                }
                if (sysParamResult[1] != null) {
                    sysParameterResponseTO.setParamLabel((String) sysParamResult[1]);
                }
                if (sysParamResult[2] != null) {
                    sysParameterResponseTO.setDataTypeName((String) sysParamResult[2]);
                }
                if (sysParamResult[3] != null) {
                    sysParameterResponseTO.setParamDescription((String) sysParamResult[3]);
                }
                if (sysParamResult[4] != null) {
                    sysParameterResponseTO.setDataTypeId((Integer) sysParamResult[4]);
                }
                list.add(sysParameterResponseTO);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }


}
