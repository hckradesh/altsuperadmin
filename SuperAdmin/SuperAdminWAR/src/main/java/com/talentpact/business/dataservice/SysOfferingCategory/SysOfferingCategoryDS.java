/**
 * 
 */
package com.talentpact.business.dataservice.SysOfferingCategory;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysOfferingCategory;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysOfferingCategoryDS extends AbstractDS<SysOfferingCategory>
{
    public SysOfferingCategoryDS()
    {
        super(SysOfferingCategory.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysOfferingCategoryList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select offeringCategoryID, offeringCategory, Description from SysOfferingCategory order by offeringCategory");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO)
    {
        SysOfferingCategory sysOfferingCategory = new SysOfferingCategory();
        try {
            sysOfferingCategory.setOfferingCategory(sysOfferingCategoryTO.getOfferingCategory());
            sysOfferingCategory.setDescription(sysOfferingCategoryTO.getDescription());
            sysOfferingCategory.setCreatedBy(1);
            sysOfferingCategory.setModifiedBy(1);
            sysOfferingCategory.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysOfferingCategory.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").persist(sysOfferingCategory);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean editSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO)
    {
        SysOfferingCategory sysOfferingCategory = new SysOfferingCategory();
        try {
            sysOfferingCategory = findSysOfferingCategory(sysOfferingCategoryTO);
            sysOfferingCategory.setOfferingCategory(sysOfferingCategoryTO.getOfferingCategory());
            sysOfferingCategory.setDescription(sysOfferingCategoryTO.getDescription());
            sysOfferingCategory.setModifiedBy(1);
            sysOfferingCategory.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").merge(sysOfferingCategory);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysOfferingCategory findSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysOfferingCategory> sysOfferingCategoryList = null;
        SysOfferingCategory result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select soc from SysOfferingCategory soc  where soc.offeringCategoryID=:offeringCategoryId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("offeringCategoryId", sysOfferingCategoryTO.getOfferingCategoryID());
            sysOfferingCategoryList = query.getResultList();
            if (sysOfferingCategoryList != null && sysOfferingCategoryList.size() > 0) {
                result = sysOfferingCategoryList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
}