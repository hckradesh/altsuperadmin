/**
 * 
 */
package com.talentpact.business.dataservice.sysRule;

import java.util.List;

import javax.ejb.Stateless;

import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
public class SysRuleCache
{
    public List<RuleResponseTO> getSysRuleListByTenant()
        throws Exception
    {
        String key = "SUPERADMIN:SYSRULE";
        return RedisClient.getValueAsList(key, RuleResponseTO.class);
    }

    public void persistSysRuleListByTenant(List<RuleResponseTO> ruleList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSRULE";
        RedisClient.putValueAsTO(key, ruleList);

    }

    public void removeSysRuleListByTenant()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSRULE";
        RedisClient.delete(key);
    }

}
