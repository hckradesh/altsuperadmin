package com.talentpact.business.dataservice.sysEntity;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.SysEntityRequestTO;
import com.talentpact.business.common.constants.IConstants;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysEntity;
import com.talentpact.ui.sysEntity.Exception.SysEntityException;
import com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO;
import com.talentpact.ui.sysEntity.constants.ISysEntityConstants;
import com.talentpact.ui.sysEntity.converter.SysEntityConverter;

@Stateless
public class SysEntityDS extends AbstractDS<SysEntity> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SysEntityDS.class);

    public SysEntityDS()
    {
        super(SysEntity.class);
    }
    
    /**
     * fetching SysEntity List
     * @return List<Object[]>
     * @throws SysEntityException
     * @author raghvendra.mishra
     */
    
    @SuppressWarnings("unchecked")
    public List<SysEntityResponseTO> getAllEntities()
        throws Exception
    {
    	SysEntityResponseTO sysEntityResponseTO = null;
    	List<SysEntityResponseTO> sysEntityResponseTOList = null;
    	List<SysEntity> sysEntityList = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" from SysEntity sysEntity ");
            sql.append(" order by sysEntity.name");
            Query query = getEntityManager("TalentPact").createQuery(sql.toString());
            sysEntityList = query.getResultList();
            if(sysEntityList!=null && 
            		!sysEntityList.isEmpty()){
            	sysEntityResponseTOList = new LinkedList<SysEntityResponseTO>();
            	for(SysEntity entityObj : sysEntityList){
            		sysEntityResponseTO = 
            				SysEntityConverter.getSysEntityResponseTOByEntity(entityObj);
            		if(sysEntityResponseTO!=null){
            			sysEntityResponseTOList.add(sysEntityResponseTO);
            		}
            	}
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw e;
        }
        
        return sysEntityResponseTOList;
    }
    
	public List<SysEntity> getSysEntityDetails() throws SysEntityException{
		List<SysEntity> sysEntityList = null;
		StringBuilder hqlQuery = null;
		Query query = null;
		try {
			hqlQuery = new StringBuilder("select sysEntity from SysEntity sysEntity ");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			sysEntityList = query.getResultList();
			return sysEntityList;
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityException("Error occured while retrieving SysEntity List.");
		}
	}

	/**
	 * method to save SysEntity
	 * @param sysEntityRequestTO
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public SysEntity saveOrUpdateSysEntity(SysEntityRequestTO sysEntityRequestTO) throws SysEntityException{
		int entityID = 0;
		SysEntity sysEntity = null;
		boolean persistenceFlag = false;
		try{
			if(sysEntityRequestTO.getEntityId() != null){
			entityID = sysEntityRequestTO.getEntityId();
			sysEntity = findSysEntity(entityID);
			}
			if(sysEntity != null){
				sysEntity.setName(sysEntityRequestTO.getName());
				sysEntity.setDescription(sysEntityRequestTO.getDescription());
				sysEntity.setTableName(sysEntityRequestTO.getTableName());
				sysEntity.setPrimaryKey(sysEntityRequestTO.getPrimaryKey());
				sysEntity.setLableColumn(sysEntityRequestTO.getLableColumn());
				sysEntity.setModuleId(sysEntityRequestTO.getModuleId());
				sysEntity.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
				sysEntity.setModifiedBy(sysEntityRequestTO.getModifiedBy());
				sysEntity.setPrimaryKeyColumnID(sysEntityRequestTO.getPrimaryKeyColumnID());;
				getEntityManager("TalentPact").merge(sysEntity);
				persistenceFlag = false;
			}else{
				sysEntity = new SysEntity();
				sysEntityRequestTO.setTenantID(1);
				sysEntity.setName(sysEntityRequestTO.getName());
				sysEntity.setDescription(sysEntityRequestTO.getDescription());
				sysEntity.setTableName(sysEntityRequestTO.getTableName());
				sysEntity.setTenantID(sysEntityRequestTO.getTenantID());
				sysEntity.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
				sysEntity.setModifiedDate(sysEntityRequestTO.getModifiedDate());
				sysEntity.setCreatedBy(sysEntityRequestTO.getCreatedBy());
				sysEntity.setModifiedBy(sysEntityRequestTO.getModifiedBy());
				sysEntity.setPrimaryKey(sysEntityRequestTO.getPrimaryKey());
				sysEntity.setLableColumn(sysEntityRequestTO.getLableColumn());
				sysEntity.setModuleId(sysEntityRequestTO.getModuleId());
				sysEntity.setPrimaryKeyColumnID(sysEntityRequestTO.getPrimaryKeyColumnID());
				// syscolumnId has to be set			
				getEntityManager("TalentPact").persist(sysEntity);
				persistenceFlag = true;
			}
			return sysEntity;
		}catch(Exception e){
			LOGGER.error("",e);
			if(persistenceFlag){
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_SAVE_ERROR_MSG, e);
			}else{
				throw new SysEntityException(ISysEntityConstants.SYSENTITY_UPDATE_ERROR_MSG, e);
			}
		}
	}
	
	/**
	 * method to get primaryColumnNames on the basis of tableName( Table may also contain composite key( i.e. combination of more than two columns as primary key
	 * , in that case this method will return more than two column as primary key column name))
	 * @param tableName
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public List<String> getTablesPrimaryKeyColumnNames(String tableName) throws SysEntityException{
		String primaryKeyColumnName = null;
        Query query = null;
        List<String> result = null;
        StringBuilder stringBuilder = null;
        String primaryKey = "PRIMARY KEY";
        String tableSchema = "dbo";
        try {
            stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE ");
            stringBuilder.append("WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + CONSTRAINT_NAME), 'IsPrimaryKey') = 1 ");
            stringBuilder.append("AND TABLE_NAME = :tableName AND TABLE_SCHEMA = :tableSchema ");
            query = getEntityManager("TalentPact").createNativeQuery(stringBuilder.toString());
            query.setParameter(ISysEntityConstants.SYSENTITY_TABLE_NAME, tableName);
            query.setParameter(ISysEntityConstants.SYSENTITY_TABLE_SCHEMA, tableSchema);            
            result = query.getResultList();
		}catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_PRIMARY_KEY_COLUMN_NAME_ERROR_MSG, e);
		}
		return result;
	}
	
	/**
	 * method get names of all tables in talentpact which are not in SysEntity
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public List<String> getTableNameList() throws SysEntityException {
        Query query = null;
        List<String> result = null;
        StringBuilder queryBuilder = null;
        String baseTable = "BASE TABLE";
        try {
        	queryBuilder = new StringBuilder();
        	queryBuilder.append("select TABLE_NAME from INFORMATION_SCHEMA.TABLES ");
        	queryBuilder.append("where TABLE_TYPE=:baseTable  and TABLE_NAME ");
        	queryBuilder.append("not in (select distinct \"TableName\" from \"SysEntity\") order by TABLE_NAME");
            query = getEntityManager("TalentPact").createNativeQuery(queryBuilder.toString());
            query.setParameter(IConstants.BASE_TABLE, baseTable);
            result = query.getResultList();
        } catch (Exception e) {
            LOGGER.error("",e);
            throw new SysEntityException(ISysEntityConstants.SYSENTITY_TABLE_NAME_LIST_ERROR_MSG, e);
        }
        return result;
    }
	
	/**
	 * method to obtain COLUMN_NAME and DATA_TYPE of all columns in a table
	 * @return
	 * @throws SysEntityException
	 * @author raghvendra.mishra
	 */
	public List<Object[]> getColumnNamesAndDataTypeOfAllColumnsOfATable(String tableName) throws SysEntityException {
        Query query = null;
        List<Object[]> result = null;
        StringBuilder queryBuilder = null;
        String tableSchema = "dbo";
        try {
        	queryBuilder = new StringBuilder();
        	queryBuilder.append("SELECT COLUMN_NAME, DATA_TYPE ");
        	queryBuilder.append("FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = :tableName AND TABLE_SCHEMA = :tableSchema ");
        	queryBuilder.append("ORDER BY ordinal_position");
            query = getEntityManager("TalentPact").createNativeQuery(queryBuilder.toString());
            query.setParameter(ISysEntityConstants.SYSENTITY_TABLE_NAME, tableName);
            query.setParameter(ISysEntityConstants.SYSENTITY_TABLE_SCHEMA, tableSchema);
            result = query.getResultList();
        } catch (Exception e) {
            LOGGER.error("",e);
            throw new SysEntityException(ISysEntityConstants.SYSENTITY_COLUMN_NAME_DATA_TYPE_LIST_ERROR_MSG, e);
        }
        return result;
    }
		
	
	/**
	 * 
	 * @param sysEntityId
	 * @return
	 * @throws SysEntityException
	 */
	public SysEntity findSysEntity(int sysEntityId) throws SysEntityException{
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysEntity> sysEntityList = null;
		SysEntity result = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select sysEntity from SysEntity sysEntity where sysEntity.entityId=:entityID");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			query.setParameter("entityID", sysEntityId);
			sysEntityList = query.getResultList();
			if (sysEntityList != null && sysEntityList.size() > 0) {
				result = sysEntityList.get(0);
			}
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_RETRIEVE_ERROR_MSG, e);
		}
		return result;
	}
	
	public SysEntity updatePrimaryKeyColumnDetailsInSysEntity(SysEntityRequestTO sysEntityRequestTO) throws SysEntityException{
		int entityID = 0;
		SysEntity sysEntity = null;
		try {
			if(sysEntityRequestTO.getEntityId() != null){
				entityID = sysEntityRequestTO.getEntityId();
				sysEntity = findSysEntity(entityID);
			}
			sysEntity.setPrimaryKeyColumnID(sysEntityRequestTO.getPrimaryKeyColumnID());
			getEntityManager("TalentPact").merge(sysEntity);
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_PRIMARY_COLUMN_UPDATE_ERROR_MSG, e);
		}
		return sysEntity;
	}
	
	

}
