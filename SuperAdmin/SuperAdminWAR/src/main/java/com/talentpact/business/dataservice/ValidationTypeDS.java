package com.talentpact.business.dataservice;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.model.SysValidation;
import com.talentpact.model.SysValidationType;

@Stateless
public class ValidationTypeDS extends AbstractDS<SysValidationType>
{

    @EJB
    ValidationDS validationDS;

    public ValidationTypeDS()
    {
        super(SysValidationType.class);
    }

    public List<SysValidationType> getSysValidationTypeList(boolean groupValidation)
        throws Exception
    {
        Set<String> validationType = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            List<SysValidation> lst = validationDS.getSysValidationList(groupValidation);
            if (lst != null && !lst.isEmpty()) {
                validationType = new HashSet<String>();
                for (SysValidation sv : lst) {
                    validationType.add(sv.getValidationType());
                }
                hqlQuery = new StringBuilder("select STY from SysValidationType STY where STY.validationType in (:validationType) ");
                query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
                query.setParameter("validationType", validationType);
                return query.getResultList();
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            validationType = null;
        }
        return null;
    }


}
