package com.talentpact.business.cache;

import static com.alt.cache.constant.RedisExceptionConstant.REDIS_DOWN_FLAG_SET_MESSAGE;

import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

import com.alt.cache.constant.RedisExceptionConstant;
import com.talentpact.business.cache.scheduler.RedisSchedulerHelper;
import com.talentpact.business.common.exceptions.JSONConversionException;
import com.talentpact.business.common.exceptions.RedisConnectionException;
import com.talentpact.business.util.JsonConversionUtil;


/**
 * @author seema.sharma
 * 
 */
public class RedisClient
{


    /* Save Values*/

    public static <T> String putValueAsTO(final String key, T valueObj)
        throws RedisConnectionException
    {
        return putValueAsTO(key, valueObj, false);
    }

    public static <T> String putValueAsTO(final String key, T valueObj, boolean disableFailOnEmptyBean)
        throws RedisConnectionException
    {

        JedisPool pool = null;
        Jedis jedis = null;
        String value = null;
        String valueMem = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            if (disableFailOnEmptyBean) {
                value = JsonConversionUtil.convertObjectToJSONString(valueObj, true);
            } else {
                value = JsonConversionUtil.convertObjectToJSONString(valueObj, false);
            }
            valueMem = jedis.set(key, value);
        } catch (JSONConversionException ex) {
            throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
        } catch (Exception ex) {
            throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }

        return valueMem;

    }


    public static void putValue(final String key, final String value)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            jedis.set(key, value);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
    }


    /* Redis with dataStructure Hashes. Value is used as Map */
    public static void putValue(final String key, final Map<String, String> hash)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            jedis.hmset(key, hash);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
    }


    /*Retrieve Values*/

    public static String getValue(final String key)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        String value = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            value = jedis.get(key);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return value;

    }

    public static <T> T getValue(final String key, Class<T> valueClass)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        T result = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            String jsonString = jedis.get(key);
            if (jsonString != null) {
                try {
                    result = JsonConversionUtil.convertJSONStringToObject(jsonString, valueClass);
                } catch (JSONConversionException e) {
                    throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + e.getCause());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return result;

    }

    public static <T> List<T> getValueAsList(final String key, Class<T> classType)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        List<T> result = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            String jsonString = jedis.get(key);
            if (jsonString != null) {
                try {
                    result = JsonConversionUtil.convertJSONStringToList(jsonString, classType);
                } catch (Exception ex) {
                    throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
                }
            }
        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return result;
    }

    public static <V> Map<String, List<V>> getValueAsMap(final String key, Class<V> classType)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        Map<String, List<V>> result = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            String jsonString = jedis.get(key);

            if (jsonString != null) {
                try {
                    result = JsonConversionUtil.convertJSONStringToObjectMap(jsonString, classType);
                } catch (JSONConversionException ex) {
                    throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
                }
            }
        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return result;
    }


    /*Delete Values*/
    public static Long delete(final String key)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        if (key == null) {
            throw new NullPointerException(RedisExceptionConstant.Null_Delete);
        }
        Long noOfKeysRemoved = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            noOfKeysRemoved = jedis.del(key);

        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return noOfKeysRemoved;

    }


    public static Long delete(List<String> keyList)
        throws RedisConnectionException
    {
        if (keyList == null || keyList.size() == 0) {
            throw new NullPointerException("Key Passed to delete is null");
        }
        JedisPool pool = null;
        Jedis jedis = null;
        Long noOfKeysRemoved = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            String[] arr = keyList.toArray(new String[keyList.size()]);
            noOfKeysRemoved = jedis.del(arr);
        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return noOfKeysRemoved;

    }


    public static Long deleteByPattern(String pattern)
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        Long noOfKeysRemoved = 0l;
        if (pattern == null) {
            throw new NullPointerException(RedisExceptionConstant.Null_Delete);
        }
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            Set<String> keys = jedis.keys(pattern);
            if (keys != null && keys.size() > 0) {
                String[] arr = keys.toArray(new String[keys.size()]);
                noOfKeysRemoved = jedis.del(arr);
            }
        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return noOfKeysRemoved;

    }


    public Pipeline getPiplelineInstance()
        throws RedisConnectionException
    {
    	JedisPool pool = null;
        Jedis jedis = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
        } catch (Exception ex) {
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return jedis.pipelined();

    }

    public void runPipleline()
    {

    }

    public static <T, V> Map<T, List<V>> getValueAsMapWithGenericKey(final String key, Class<T> keyType, Class<V> objListType)
        throws RedisConnectionException
    {
        if (!RedisSchedulerHelper.isRedisUpFlag()) {
            throw new RedisConnectionException(REDIS_DOWN_FLAG_SET_MESSAGE);
        }
        JedisPool pool = null;
        Jedis jedis = null;
        Map<T, List<V>> result = null;
        try {
            pool = RedisPoolFactory.getJedisPoolInstance();
            jedis = pool.getResource();
            String jsonString = jedis.get(key);

            if (jsonString != null) {
                try {
                    result = JsonConversionUtil.convertJSONStringToObjectMapWithGenericKey(jsonString, keyType, objListType);
                } catch (JSONConversionException ex) {
                    throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RedisConnectionException(ex.getCause());
        }finally {
            pool.returnResource(jedis);
        }
        return result;
    }


    public static <V> Map<String, V> getValueAsMapTO(final String key, Class<V> classType)
            throws RedisConnectionException
        {
    		JedisPool pool = null;
    		Jedis jedis = null;
            Map<String, V> result = null;
            try {
                pool = RedisPoolFactory.getJedisPoolInstance();
                jedis = pool.getResource();
                String jsonString = jedis.get(key);

                if (jsonString != null) {
                    try {
                        result = JsonConversionUtil.convertJSONStringToObjectTO(jsonString, classType);
                    } catch (JSONConversionException ex) {
                        throw new RedisConnectionException(RedisExceptionConstant.Json_Conversion_EXception + ex.getCause());
                    }
                }
            } catch (Exception ex) {
                throw new RedisConnectionException(ex.getCause());
            }finally {
                pool.returnResource(jedis);
            }
            return result;
        }

    
}
