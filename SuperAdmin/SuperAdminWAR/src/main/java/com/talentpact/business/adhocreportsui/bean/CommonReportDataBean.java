package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.to.OperatorTo;
import com.talentpact.adhocreports.ui.business.to.ApplicationInputTo;
import com.talentpact.adhocreports.ui.business.to.ColumnTo;
import com.talentpact.adhocreports.ui.business.to.ConstantTo;
import com.talentpact.adhocreports.ui.business.to.ProcessingLogicTemplateTo;
import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.business.adhocreportsui.common.DummyData;
import com.talentpact.business.adhocreportsui.controller.CommonReportDataController;

@SessionScoped
@Named("commonReportDataBean")
public class CommonReportDataBean implements Serializable {

	private static final long serialVersionUID = -5577238244876073934L;

	@Inject
	CommonReportDataController commonReportDataController;

	private DualListModel<TableTo> normalTables;

	private List<TableTo> defaultTables;

	private DualListModel<TableTo> derivedTables;

	private Map<TableTo,List<ColumnTo>> allTableColumnsMap;

	private List<SelectItem> joinTypes;

	private List<SelectItem> dataTypes;

	private Map<Integer,List<OperatorTo>> operatorDataTypeMap;

	private Map<String, List<ProcessingLogicTemplateTo>> processingLogicTemplatesMap;

	private Map<Integer,ProcessingLogicTemplateTo> processingLogicTemplatesMapById;

	private ProcessingLogicTemplateTo applicationInputProcessingLogicTemplate;

	private ProcessingLogicTemplateTo constantInputProcessingLogicTemplate;

	private ProcessingLogicTemplateTo columnProcessingLogicTemplate;

	private ProcessingLogicTemplateTo genericColumnProcessingLogicTemplate;

	private ProcessingLogicTemplateTo groupingColumnProcessingLogicTemplate;

	private List<SelectItem> filterCombinationOperatorList;

	private List<SelectItem> constantList;

	private List<ConstantTo> constantToList;

	private List<SelectItem> applicationInputList;

	private List<ApplicationInputTo> applicationInputToList;

	private List<SelectItem> processingLogicTemplateTypeList;

	private List<SelectItem> processingLogicTemplateTypeListWithNone;

	private List<SelectItem> processingLogicTemplateTypeListWithoutProcessingLogic;

	private List<SelectItem> processingLogicTemplateTypeListWithoutProcessingLogicWithNone;

	private List<String> tableOperation;

	private List<DummyData> dummyDataList;

	private Integer applicationInputOrganizationId;

	private List<SelectItem> organizationList;

	private List<String> organizationNameList;

	private List<TableTo> orgAdminDerivedTables;

	private List<String> derivedTableNameList;

	private List<TableTo> orgAdminDerivedTablesAllOrg;

	private List<TableTo> orgAdminFilteredDerivedTablesAllOrg;

	private List<ReportDetail> nonDefaultReports;

	private List<ReportDetail> nonDefaultReportsAllOrg;

	private List<ReportDetail> nonDefaultFilteredReportsAllOrg;

	private List<ReportDetail> nonDefaultReportsInOrgMapping;

	private List<TableTo> orgAdminDerivedTablesInOrgMapping;

	private List<SelectItem> cssClassList;

	private List<SelectItem> commonModuleList;

	private List<ReportDetail> allReportsDetails;

	@PostConstruct
	public void initialize() {
		commonReportDataController.initialize(this);
	}

	public List<TableTo> getDefaultTables() {
		return defaultTables;
	}

	public void setDefaultTables(List<TableTo> defaultTables) {
		this.defaultTables = defaultTables;
	}

	public DualListModel<TableTo> getNormalTables() {
		return normalTables;
	}

	public void setNormalTables(DualListModel<TableTo> normalTables) {
		this.normalTables = normalTables;
	}

	public DualListModel<TableTo> getDerivedTables() {
		return derivedTables;
	}

	public void setDerivedTables(DualListModel<TableTo> derivedTables) {
		this.derivedTables = derivedTables;
	}

	public List<SelectItem> getJoinTypes() {
		return joinTypes;
	}

	public void setJoinTypes(List<SelectItem> joinTypes) {
		this.joinTypes = joinTypes;
	}

	public Map<TableTo, List<ColumnTo>> getAllTableColumnsMap() {
		return allTableColumnsMap;
	}

	public void setAllTableColumnsMap(
			Map<TableTo, List<ColumnTo>> allTableColumnsMap) {
		this.allTableColumnsMap = allTableColumnsMap;
	}

	public List<SelectItem> getDataTypes() {
		return dataTypes;
	}

	public void setDataTypes(List<SelectItem> dataTypes) {
		this.dataTypes = dataTypes;
	}

	public Map<String, List<ProcessingLogicTemplateTo>> getProcessingLogicTemplatesMap() {
		return processingLogicTemplatesMap;
	}

	public void setProcessingLogicTemplatesMap(
			Map<String, List<ProcessingLogicTemplateTo>> processingLogicTemplatesMap) {
		this.processingLogicTemplatesMap = processingLogicTemplatesMap;
	}

	public List<SelectItem> getFilterCombinationOperatorList() {
		return filterCombinationOperatorList;
	}

	public void setFilterCombinationOperatorList(
			List<SelectItem> filterCombinationOperatorList) {
		this.filterCombinationOperatorList = filterCombinationOperatorList;
	}

	public Map<Integer, List<OperatorTo>> getOperatorDataTypeMap() {
		return operatorDataTypeMap;
	}

	public void setOperatorDataTypeMap(
			Map<Integer, List<OperatorTo>> operatorDataTypeMap) {
		this.operatorDataTypeMap = operatorDataTypeMap;
	}

	public List<SelectItem> getConstantList() {
		return constantList;
	}

	public void setConstantList(List<SelectItem> constantList) {
		this.constantList = constantList;
	}

	public List<SelectItem> getApplicationInputList() {
		return applicationInputList;
	}

	public void setApplicationInputList(List<SelectItem> applicationInputList) {
		this.applicationInputList = applicationInputList;
	}

	public ProcessingLogicTemplateTo getApplicationInputProcessingLogicTemplate() {
		return applicationInputProcessingLogicTemplate;
	}

	public void setApplicationInputProcessingLogicTemplate(
			ProcessingLogicTemplateTo applicationInputProcessingLogicTemplate) {
		this.applicationInputProcessingLogicTemplate = applicationInputProcessingLogicTemplate;
	}

	public ProcessingLogicTemplateTo getConstantInputProcessingLogicTemplate() {
		return constantInputProcessingLogicTemplate;
	}

	public void setConstantInputProcessingLogicTemplate(
			ProcessingLogicTemplateTo constantInputProcessingLogicTemplate) {
		this.constantInputProcessingLogicTemplate = constantInputProcessingLogicTemplate;
	}

	public List<SelectItem> getProcessingLogicTemplateTypeList() {
		return processingLogicTemplateTypeList;
	}

	public void setProcessingLogicTemplateTypeList(
			List<SelectItem> processingLogicTemplateTypeList) {
		this.processingLogicTemplateTypeList = processingLogicTemplateTypeList;
	}

	public List<SelectItem> getProcessingLogicTemplateTypeListWithoutProcessingLogic() {
		return processingLogicTemplateTypeListWithoutProcessingLogic;
	}

	public void setProcessingLogicTemplateTypeListWithoutProcessingLogic(
			List<SelectItem> processingLogicTemplateTypeListWithoutProcessingLogic) {
		this.processingLogicTemplateTypeListWithoutProcessingLogic = processingLogicTemplateTypeListWithoutProcessingLogic;
	}

	public Map<Integer,ProcessingLogicTemplateTo> getProcessingLogicTemplatesMapById() {
		return processingLogicTemplatesMapById;
	}

	public void setProcessingLogicTemplatesMapById(
			Map<Integer, ProcessingLogicTemplateTo> processingLogicTemplatesMapById) {
		this.processingLogicTemplatesMapById = processingLogicTemplatesMapById;
	}

	public List<String> getTableOperation() {
		return tableOperation;
	}

	public void setTableOperation(List<String> tableOperation) {
		this.tableOperation = tableOperation;
	}

	public List<SelectItem> getProcessingLogicTemplateTypeListWithNone() {
		return processingLogicTemplateTypeListWithNone;
	}

	public void setProcessingLogicTemplateTypeListWithNone(
			List<SelectItem> processingLogicTemplateTypeListWithNone) {
		this.processingLogicTemplateTypeListWithNone = processingLogicTemplateTypeListWithNone;
	}

	public ProcessingLogicTemplateTo getColumnProcessingLogicTemplate() {
		return columnProcessingLogicTemplate;
	}

	public void setColumnProcessingLogicTemplate(
			ProcessingLogicTemplateTo columnProcessingLogicTemplate) {
		this.columnProcessingLogicTemplate = columnProcessingLogicTemplate;
	}

	public ProcessingLogicTemplateTo getGenericColumnProcessingLogicTemplate() {
		return genericColumnProcessingLogicTemplate;
	}

	public void setGenericColumnProcessingLogicTemplate(
			ProcessingLogicTemplateTo genericColumnProcessingLogicTemplate) {
		this.genericColumnProcessingLogicTemplate = genericColumnProcessingLogicTemplate;
	}

	public ProcessingLogicTemplateTo getGroupingColumnProcessingLogicTemplate() {
		return groupingColumnProcessingLogicTemplate;
	}

	public void setGroupingColumnProcessingLogicTemplate(
			ProcessingLogicTemplateTo groupingColumnProcessingLogicTemplate) {
		this.groupingColumnProcessingLogicTemplate = groupingColumnProcessingLogicTemplate;
	}

	public List<DummyData> getDummyDataList() {
		return dummyDataList;
	}

	public void setDummyDataList(List<DummyData> dummyDataList) {
		this.dummyDataList = dummyDataList;
	}

	public List<SelectItem> getProcessingLogicTemplateTypeListWithoutProcessingLogicWithNone() {
		return processingLogicTemplateTypeListWithoutProcessingLogicWithNone;
	}

	public void setProcessingLogicTemplateTypeListWithoutProcessingLogicWithNone(
			List<SelectItem> processingLogicTemplateTypeListWithoutProcessingLogicWithNone) {
		this.processingLogicTemplateTypeListWithoutProcessingLogicWithNone = processingLogicTemplateTypeListWithoutProcessingLogicWithNone;
	}

	public Integer getApplicationInputOrganizationId() {
		return applicationInputOrganizationId;
	}

	public void setApplicationInputOrganizationId(
			Integer applicationInputOrganizationId) {
		this.applicationInputOrganizationId = applicationInputOrganizationId;
	}

	public List<ConstantTo> getConstantToList() {
		return constantToList;
	}

	public void setConstantToList(List<ConstantTo> constantToList) {
		this.constantToList = constantToList;
	}

	public List<ApplicationInputTo> getApplicationInputToList() {
		return applicationInputToList;
	}

	public void setApplicationInputToList(
			List<ApplicationInputTo> applicationInputToList) {
		this.applicationInputToList = applicationInputToList;
	}

	public List<SelectItem> getOrganizationList() {
		return organizationList;
	}

	public void setOrganizationList(List<SelectItem> organizationList) {
		this.organizationList = organizationList;
	}

	public List<TableTo> getOrgAdminDerivedTables() {
		return orgAdminDerivedTables;
	}

	public void setOrgAdminDerivedTables(List<TableTo> orgAdminDerivedTables) {
		this.orgAdminDerivedTables = orgAdminDerivedTables;
	}

	public List<ReportDetail> getNonDefaultReports() {
		return nonDefaultReports;
	}

	public void setNonDefaultReports(List<ReportDetail> nonDefaultReports) {
		this.nonDefaultReports = nonDefaultReports;
	}

	public List<ReportDetail> getNonDefaultReportsAllOrg() {
		return nonDefaultReportsAllOrg;
	}

	public void setNonDefaultReportsAllOrg(List<ReportDetail> nonDefaultReportsAllOrg) {
		this.nonDefaultReportsAllOrg = nonDefaultReportsAllOrg;
	}

	public List<ReportDetail> getNonDefaultReportsInOrgMapping() {
		return nonDefaultReportsInOrgMapping;
	}

	public void setNonDefaultReportsInOrgMapping(
			List<ReportDetail> nonDefaultReportsInOrgMapping) {
		this.nonDefaultReportsInOrgMapping = nonDefaultReportsInOrgMapping;
	}

	public List<ReportDetail> getNonDefaultFilteredReportsAllOrg() {
		return nonDefaultFilteredReportsAllOrg;
	}

	public void setNonDefaultFilteredReportsAllOrg(
			List<ReportDetail> nonDefaultFilteredReportsAllOrg) {
		this.nonDefaultFilteredReportsAllOrg = nonDefaultFilteredReportsAllOrg;
	}

	public List<String> getOrganizationNameList() {
		return organizationNameList;
	}

	public void setOrganizationNameList(List<String> organizationNameList) {
		this.organizationNameList = organizationNameList;
	}

	public List<TableTo> getOrgAdminDerivedTablesAllOrg() {
		return orgAdminDerivedTablesAllOrg;
	}

	public void setOrgAdminDerivedTablesAllOrg(
			List<TableTo> orgAdminDerivedTablesAllOrg) {
		this.orgAdminDerivedTablesAllOrg = orgAdminDerivedTablesAllOrg;
	}

	public List<TableTo> getOrgAdminDerivedTablesInOrgMapping() {
		return orgAdminDerivedTablesInOrgMapping;
	}

	public void setOrgAdminDerivedTablesInOrgMapping(
			List<TableTo> orgAdminDerivedTablesInOrgMapping) {
		this.orgAdminDerivedTablesInOrgMapping = orgAdminDerivedTablesInOrgMapping;
	}

	public List<String> getDerivedTableNameList() {
		return derivedTableNameList;
	}

	public void setDerivedTableNameList(List<String> derivedTableNameList) {
		this.derivedTableNameList = derivedTableNameList;
	}

	public List<TableTo> getOrgAdminFilteredDerivedTablesAllOrg() {
		return orgAdminFilteredDerivedTablesAllOrg;
	}

	public void setOrgAdminFilteredDerivedTablesAllOrg(
			List<TableTo> orgAdminFilteredDerivedTablesAllOrg) {
		this.orgAdminFilteredDerivedTablesAllOrg = orgAdminFilteredDerivedTablesAllOrg;
	}

	public List<SelectItem> getCssClassList() {
		return cssClassList;
	}

	public void setCssClassList(List<SelectItem> cssClassList) {
		this.cssClassList = cssClassList;
	}

	public List<SelectItem> getCommonModuleList() {
		return commonModuleList;
	}

	public void setCommonModuleList(List<SelectItem> commonModuleList) {
		this.commonModuleList = commonModuleList;
	}

	public List<ReportDetail> getAllReportsDetails() {
		return allReportsDetails;
	}

	public void setAllReportsDetails(List<ReportDetail> allReportsDetails) {
		this.allReportsDetails = allReportsDetails;
	}

}
