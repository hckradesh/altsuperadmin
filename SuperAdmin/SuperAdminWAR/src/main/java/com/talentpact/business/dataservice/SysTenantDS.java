package com.talentpact.business.dataservice;

import java.util.Date;

import javax.ejb.Stateless;

import com.talentpact.auth.entity.TpTenant;
import com.talentpact.model.organization.SysTenant;


@Stateless
public class SysTenantDS extends AbstractDS<SysTenant>
{
    public SysTenantDS()
    {
        super(SysTenant.class);
        // TODO Auto-generated constructor stub
    }

    public void saveSysTenant(TpTenant tpTenant)
        throws Exception
    {

        SysTenant sysTenant = null;
        try {
            boolean alreadyExist = false;
            sysTenant = getEntityManager("TalentPact").find(SysTenant.class, tpTenant.getTenantID().intValue());
            if (sysTenant != null) {
                if (tpTenant.getCreatedBy() != null && tpTenant.getCreatedBy().getUserID() != null) {
                    sysTenant.setModifiedBy(tpTenant.getCreatedBy().getUserID().intValue());
                }
                sysTenant.setModifiedDate(new Date());
                alreadyExist = true;
            } else {
                sysTenant = new SysTenant();
                sysTenant.setTenantId(tpTenant.getTenantID().intValue());
                sysTenant.setCreatedDate(new Date());
                sysTenant.setModifiedDate(new Date());
                if (tpTenant.getCreatedBy() != null && tpTenant.getCreatedBy().getUserID() != null) {
                    sysTenant.setCreatedBy(tpTenant.getCreatedBy().getUserID().intValue());
                }
            }
            sysTenant.setTenantName(tpTenant.getName());
            sysTenant.setTenantInterface(true);

            if (alreadyExist) {
                getEntityManager("TalentPact").merge(sysTenant);
            } else {
                getEntityManager("TalentPact").persist(sysTenant);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

}
