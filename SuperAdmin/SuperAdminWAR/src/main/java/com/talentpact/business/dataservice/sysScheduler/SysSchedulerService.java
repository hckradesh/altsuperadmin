package com.talentpact.business.dataservice.sysScheduler;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysSchedulerType;
import com.talentpact.ui.sysScheduler.to.SysSchedulerTO;

/**
 * 
 * @author prachi.bansal
 *
 */
@Stateless
public class SysSchedulerService extends CommonService
{
    @EJB
    SysSchedulerDS sysSchedulerDS;
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysSchedulerService.class);
    
    public List<SysSchedulerTO> getSysScheduler() throws Exception {
        
        List<SysSchedulerType> sysSchedulerTypeList=null;
        List<SysSchedulerTO> sysSchedulerTOList = null;
        sysSchedulerTOList = new ArrayList<SysSchedulerTO>();
        try {
            sysSchedulerTypeList = sysSchedulerDS.getSysSchedulerList();
            if (sysSchedulerTypeList != null && !sysSchedulerTypeList.isEmpty()) {
                for (SysSchedulerType sysSchedulerType : sysSchedulerTypeList) {
                    SysSchedulerTO sysSchedulerTO = new SysSchedulerTO();
                    sysSchedulerTO.setSysSchedulerTypeID(sysSchedulerType.getSchedulerTypeID());
                    sysSchedulerTO.setSysSchedulerName(sysSchedulerType.getSchedulerName());
                    sysSchedulerTO.setSysSchedulerDescription(sysSchedulerType.getSchedulerDescription());
                    
                    sysSchedulerTOList.add(sysSchedulerTO);
                    }
            }
            return sysSchedulerTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } finally {
        }
    }

    public void saveSysScheduler(SysSchedulerTO sysSchedulerTO) {
        
        try{
            sysSchedulerDS.saveSysScheduler(sysSchedulerTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
           
    }

    public void updateSysScheduler(SysSchedulerTO sysSchedulerTO) {
        try{
            sysSchedulerDS.updateSysScheduler(sysSchedulerTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public List<SysSchedulerTO> getUpdateSysSchedulerList(Integer sysSchedulerTypeID) throws Exception {
        List<SysSchedulerType> sysSchedulerTypeList=null;
        List<SysSchedulerTO> sysSchedulerTOList = null;
        sysSchedulerTOList = new ArrayList<SysSchedulerTO>();
        try {
            sysSchedulerTypeList = sysSchedulerDS.getUpdateSysSchedulerList(sysSchedulerTypeID);
            if (sysSchedulerTypeList != null && !sysSchedulerTypeList.isEmpty()) {
                for (SysSchedulerType sysSchedulerType : sysSchedulerTypeList) {
                    SysSchedulerTO sysSchedulerTO = new SysSchedulerTO();
                    sysSchedulerTO.setSysSchedulerTypeID(sysSchedulerType.getSchedulerTypeID());
                    sysSchedulerTO.setSysSchedulerName(sysSchedulerType.getSchedulerName());
                    sysSchedulerTO.setSysSchedulerDescription(sysSchedulerType.getSchedulerDescription());
                    
                    sysSchedulerTOList.add(sysSchedulerTO);
                    }
            }
            return sysSchedulerTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } finally {
        }
    }
}