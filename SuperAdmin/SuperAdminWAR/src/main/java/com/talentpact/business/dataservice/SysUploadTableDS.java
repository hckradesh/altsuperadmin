package com.talentpact.business.dataservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.admin.model.dataupload.SysUploadTable;

@Stateless
public class SysUploadTableDS extends AbstractDS<SysUploadTable>
{
    public SysUploadTableDS()
    {
        super(SysUploadTable.class);
    }

    public List<SysUploadTable> getExistingTableColumnsNames(String tableName)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder("select SUT from SysUploadTable SUT where SUT.tableName=:tableName  ");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("tableName", tableName);
            return query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
    }


}
