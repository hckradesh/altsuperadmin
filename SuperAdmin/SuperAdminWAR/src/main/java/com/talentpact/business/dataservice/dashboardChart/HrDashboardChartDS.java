/**
 * 
 */
package com.talentpact.business.dataservice.dashboardChart;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altone.HrDashBoard;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class HrDashboardChartDS extends AbstractDS<HrDashBoard>
{

    public HrDashboardChartDS()
    {
        super(HrDashBoard.class);
    }

    @SuppressWarnings("unchecked")
    public List<HrDashBoard> getHrDashboardList(Integer organizationID ,int portalTypeID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrDashBoard> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select hd  from HrDashBoard hd where hd.organizationID=:organizationID and hd.portalTypeID=:portalTypeID order by hd.sequence ");
            query = getEntityManager("TalentPactInsightsAgg").createQuery(hqlQuery.toString());
            query.setParameter("organizationID", organizationID);
            query.setParameter("portalTypeID", portalTypeID);
            list = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }

    /**
     * 
     * @param sysDashboardID
     * @param organizationID
     * @return
     */

    @SuppressWarnings("unchecked")
    public HrDashBoard findHrDashboard(Integer sysDashboardID, long organizationID,int portalTypeID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrDashBoard> hrDashBoardList = null;
        HrDashBoard hrDashBoard = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select hd  from HrDashBoard hd where hd.organizationID=:organizationID and hd.sysDashboardID=:sysDashboardID and hd.portalTypeID=:portalTypeID ");
            query = getEntityManager("TalentPactInsightsAgg").createQuery(hqlQuery.toString());
            query.setParameter("organizationID", (int) organizationID);
            query.setParameter("sysDashboardID", sysDashboardID);
            query.setParameter("portalTypeID", portalTypeID);
            hrDashBoardList = query.getResultList();
            if (!hrDashBoardList.isEmpty()) {
                hrDashBoard = hrDashBoardList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return hrDashBoard;
    }

}