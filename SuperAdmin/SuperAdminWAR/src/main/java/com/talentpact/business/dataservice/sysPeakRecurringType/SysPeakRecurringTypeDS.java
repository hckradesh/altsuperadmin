/**
 * 
 */
package com.talentpact.business.dataservice.sysPeakRecurringType;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.to.SysPeakRecurringTypeTO;
import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysPeakRecurringType;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysPeakRecurringTypeDS extends AbstractDS<SysPeakRecurringType>
{
    public SysPeakRecurringTypeDS()
    {
        super(SysPeakRecurringType.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysPeakRecurringTypeList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select recurringTypeID, recurringTypeLabel from SysPeakRecurringType order by recurringTypeLabel");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addSysPeakRecurringType(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
    {
        SysPeakRecurringType sysPeakRecurringType = new SysPeakRecurringType();
        try {
            sysPeakRecurringType.setRecurringTypeLabel(sysPeakRecurringTypeTO.getRecurringTypeLabel());
            sysPeakRecurringType.setCreatedBy(1);
            sysPeakRecurringType.setModifiedBy(1);
            sysPeakRecurringType.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
            sysPeakRecurringType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").persist(sysPeakRecurringType);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean editSysPeakRecurringType(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
    {
        SysPeakRecurringType sysPeakRecurringType = new SysPeakRecurringType();
        try {
            sysPeakRecurringType = findSysPeakRecurringType(sysPeakRecurringTypeTO);
            sysPeakRecurringType.setRecurringTypeLabel(sysPeakRecurringTypeTO.getRecurringTypeLabel());
            sysPeakRecurringType.setModifiedBy(1);
            sysPeakRecurringType.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            getEntityManager("TalentPact").merge(sysPeakRecurringType);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private SysPeakRecurringType findSysPeakRecurringType(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPeakRecurringType> sysPeakRecurringTypeList = null;
        SysPeakRecurringType result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select soc from SysPeakRecurringType sprt  where sprt.recurringTypeID=:recurringTypeLabelId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("recurringTypeLabelId", sysPeakRecurringTypeTO.getRecurringTypeID());
            sysPeakRecurringTypeList = query.getResultList();
            if (sysPeakRecurringTypeList != null && sysPeakRecurringTypeList.size() > 0) {
                result = sysPeakRecurringTypeList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }
}