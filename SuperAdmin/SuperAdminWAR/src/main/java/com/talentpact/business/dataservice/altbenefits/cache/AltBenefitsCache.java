/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits.cache;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import com.alt.altbenefits.to.BannerOfferingSequenceTO;
import com.alt.altbenefits.to.HrOfferingTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.output.BenefitResponseTO;
import com.talentpact.business.application.transport.output.AltBenefitResponseTO;
import com.talentpact.business.cache.RedisClient;

/**
 * @author javed.ali
 *
 */
@Stateless
public class AltBenefitsCache
{
    public BenefitResponseTO getSectorWiseVendorOfferings()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SECTOR_OFFERINGS_TEMPLATE;
        return RedisClient.getValue(key, BenefitResponseTO.class);

    }

    public void putSectorWiseVendorOfferings(BenefitResponseTO benefitResponseTO)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SECTOR_OFFERINGS_TEMPLATE;
        RedisClient.putValueAsTO(key, benefitResponseTO);

    }

    public void deleteSectorWiseVendorOfferingsRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SECTOR_OFFERINGS_TEMPLATE;
        RedisClient.delete(key);

    }


    public List<VendorOfferingTemplateTO> getSystemVendorOfferingsTemplates()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_TEMPLATE;
        return RedisClient.getValueAsList(key, VendorOfferingTemplateTO.class);

    }

    public void putSystemVendorOfferingsTemplates(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_TEMPLATE;
        RedisClient.putValueAsTO(key, vendorOfferingTemplateTOList);

    }


    public List<VendorOfferingTemplateTO> getHrVendorOfferingsTemplatesByOrg(Long orgID)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_VENDOR_OFFERING_TEMPLATE + AltBenefitsKeyConstants.COLLON + orgID;
        return RedisClient.getValueAsList(key, VendorOfferingTemplateTO.class);

    }

    public void putSystemVendorOfferingsTemplatesByOrg(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList, Long orgID)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_VENDOR_OFFERING_TEMPLATE + AltBenefitsKeyConstants.COLLON + orgID;
        RedisClient.putValueAsTO(key, vendorOfferingTemplateTOList);

    }

    public void deleteSystemVendorOfferingsTemplatesByOrgRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_VENDOR_OFFERING_TEMPLATE;
        RedisClient.delete(key);

    }

    public void deleteSystemVendorOfferingsTemplatesRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_TEMPLATE;
        RedisClient.delete(key);

    }


    public BenefitResponseTO getOrganizationWiseVendorOfferings()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_ORG_OFFERING_TEMPLATE;
        return RedisClient.getValue(key, BenefitResponseTO.class);

    }

    public void putOrganizationWiseVendorOfferings(BenefitResponseTO benefitResponseTO)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_ORG_OFFERING_TEMPLATE;
        RedisClient.putValueAsTO(key, benefitResponseTO);

    }


    public AltBenefitResponseTO getVendorOfferingsTemplateCheckList()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_OFFERING_TEMPLATE_CHECKLIST;
        return RedisClient.getValue(key, AltBenefitResponseTO.class);

    }

    public void putVendorOfferingsTemplateCheckList(AltBenefitResponseTO benefitResponseTO)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_OFFERING_TEMPLATE_CHECKLIST;
        RedisClient.putValueAsTO(key, benefitResponseTO);

    }


    public void deleteOrganizationWiseVendorOfferingsRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_ORG_OFFERING_TEMPLATE;
        RedisClient.delete(key);

    }

    public void deleteVendorOfferingsTemplateCheckListRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_OFFERING_TEMPLATE_CHECKLIST;
        RedisClient.delete(key);

    }

    public List<VendorOfferingTemplateTO> getHrOfferingTemplateListByOrgID()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_OFFERING_TEMPALTE;
        return RedisClient.getValueAsList(key, VendorOfferingTemplateTO.class);

    }

    public void putHrOfferingTemplateListByOrgID(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_OFFERING_TEMPALTE;
        RedisClient.putValueAsTO(key, vendorOfferingTemplateTOList);

    }

    public void deleteHrOfferingTemplateListByOrgIDRedisKey()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_OFFERING_TEMPALTE;
        RedisClient.delete(key);

    }

    public void putHrVendorOfferingsMap(Map<Long, List<HrOfferingTO>> hrOfferSequenceMap)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_OFFERING;
        RedisClient.putValueAsTO(key, hrOfferSequenceMap);
    }

    public void deleteHrVendorOfferingsMap()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_OFFERING;
        RedisClient.delete(key);
    }

    public void putEmpGroupVendorOfferingsMap(Map<String, List<HrOfferingTO>> hrOfferSequenceMap)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_EMPGROUP_OFFERING;
        RedisClient.putValueAsTO(key, hrOfferSequenceMap);
    }

    public void deleteEmpGroupVendorOfferingsMap()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_EMPGROUP_OFFERING;
        RedisClient.delete(key);
    }

    public void putSectorVendorOfferingsMap(Map<Integer, List<BannerOfferingSequenceTO>> sectorOfferingTOMap)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SECTOR_OFFERING;
        RedisClient.putValueAsTO(key, sectorOfferingTOMap);
    }

    public void deleteSectorVendorOfferingsMap()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SECTOR_OFFERING;
        RedisClient.delete(key);
    }

    public void putSystemVendorOfferingsList(List<BannerOfferingSequenceTO> systemOfferingTOList)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_LIST;
        RedisClient.putValueAsTO(key, systemOfferingTOList);
    }

    public void deleteSystemVendorOfferingsList()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_LIST;
        RedisClient.delete(key);
    }

    public List<BannerOfferingSequenceTO> getSystemVendorOfferingsList()
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEM_OFFERING_LIST;
        return RedisClient.getValueAsList(key, BannerOfferingSequenceTO.class);
    }

    public void putOrganizationSectorMap(Map<String, String> orgSectorMap)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_ORG_SECTOR_MAP;
        RedisClient.putValue(key, orgSectorMap);
    }

    public void putVendorOfferingCode(List<OfferingCodeTO> offeringCodeTOList,Long VendorOfferinID)
            throws Exception
        {
            String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEN_VENDOR_OFFERING_CODE+AltBenefitsKeyConstants.COLLON+VendorOfferinID;
            RedisClient.putValueAsTO(key, offeringCodeTOList);

        }

    public List<OfferingCodeTO> getVendorOfferingCode(Long VendorOfferinID)
            throws Exception
        {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEN_VENDOR_OFFERING_CODE+AltBenefitsKeyConstants.COLLON+VendorOfferinID;
            return RedisClient.getValueAsList(key, OfferingCodeTO.class);

        }


    public void deleteVendorOfferingCodeRedisKey(Long VendorOfferinID)
        throws Exception
    {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_SYSTEN_VENDOR_OFFERING_CODE+AltBenefitsKeyConstants.COLLON+VendorOfferinID;
        RedisClient.delete(key);

    }

    public void deleteHrVendorOfferingsTemplatesByOrgRedisKey(Long orgID)
            throws Exception
        {
        String key = AltBenefitsKeyConstants.ALT_BENEFIT_HR_VENDOR_OFFERING_TEMPLATE + AltBenefitsKeyConstants.COLLON + orgID;
            RedisClient.delete(key);

        }
}
