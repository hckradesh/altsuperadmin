/**
 * 
 */
package com.talentpact.business.cache.scheduler;


/**
 * @author pankaj.sharma1
 *
 */
public class RedisSchedulerHelper
{
    private static RedisSchedulerHelper singleInstance;

    private static boolean redisUpFlag;

    static {
        redisUpFlag = true;
    }

    private RedisSchedulerHelper()
    {
        redisUpFlag = true;
    }

    public static RedisSchedulerHelper getSingleInstance()
    {
        if (singleInstance == null) {
            synchronized (RedisSchedulerHelper.class) {
                if (singleInstance == null) {
                    singleInstance = new RedisSchedulerHelper();
                }
            }
        }
        return singleInstance;
    }

    public static boolean isRedisUpFlag()
    {
        return redisUpFlag;
    }

    public static void setRedisUpFlag(boolean redisUpFlagValue)
    {
        redisUpFlag = redisUpFlagValue;
    }


}
