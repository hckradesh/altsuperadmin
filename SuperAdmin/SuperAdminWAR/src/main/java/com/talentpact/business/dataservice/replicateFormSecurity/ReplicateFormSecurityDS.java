package com.talentpact.business.dataservice.replicateFormSecurity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.HrReplicateFormSecurityHistory;
import com.talentpact.model.organise.HrFormFieldConfiguration;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class ReplicateFormSecurityDS extends AbstractDS<HrFormFieldConfiguration>
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(ReplicateFormSecurityDS.class);

    public ReplicateFormSecurityDS()
    {
        super(HrFormFieldConfiguration.class);
    }

    public List<Object[]> getSourceOrganizationList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select distinct hm.hrOrganization.organizationId, hm.hrOrganization.organizationName from HrModule hm ");
            hqlQuery.append(" where hm.hrOrganization.tenantOrg = 0 and hm.active = 1 order by hm.hrOrganization.organizationName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return list;
    }

    public List<Object[]> getDestinationOrganizationList(Integer orgID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ho.organizationId, ho.organizationName from HrOrganization ho ");
            hqlQuery.append("where ho.tenantOrg = 0 and ho.organizationId !=:orgID order by ho.organizationName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            list = query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return list;
    }

    public List<Object[]> getSourceModuleList(Integer sourceOrgID, Integer destOrgID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select distinct shm.moduleId, shm.moduleName, shm.sysModuleId from HrModule shm, HrModule dhm ");
            hqlQuery.append(" where shm.sysModuleId = dhm.sysModuleId and shm.organizationId=:sourceOrgID and ");
            hqlQuery.append(" dhm.organizationId=:destOrgID and shm.active = 1 and dhm.active = 1 order by shm.moduleName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sourceOrgID", sourceOrgID);
            query.setParameter("destOrgID", destOrgID);
            list = query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return list;
    }

    public boolean validateExistingFormSecurity(Integer orgID, Integer sysModuleID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct 1 from HrForm hf where hf.organizationId=:organizationID and hf.sysForm.subModuleId=:sysModuleID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("organizationID", orgID);
            query.setParameter("sysModuleID", sysModuleID);
            List results = query.getResultList();
            if (!results.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return false;
    }

    public boolean deactivateExistingFormSecurity(Integer orgID, Integer moduleID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("exec proc_deactivateExistingFormSecurity " + orgID + "," + moduleID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            result = query.getResultList();
            if (!result.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return false;
        }
    }

    public boolean replicateFormSecurity(Integer sourceOrgID, Integer destOrgID, Integer sysModuleID, Long userID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("exec proc_replicateFormSecurity  " + sourceOrgID + "," + destOrgID + "," + sysModuleID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            result = query.getResultList();
            addReplicateFormSecurityHistory(sourceOrgID, destOrgID, sysModuleID, userID);
            if (!result.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return false;
        }
    }

    public boolean isWorkFlowEnabled(Integer sourceOrgID, Integer sysModuleID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct 1 from HrFormFieldConfiguration hffc where hffc.organizationID=:organizationID ");
            hqlQuery.append(" and hffc.formField.hrFormFieldGroup.hrForm.sysForm.subModuleId=:sysModuleID ");
            hqlQuery.append(" and hffc.stageID is not null ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("organizationID", sourceOrgID);
            query.setParameter("sysModuleID", sysModuleID);
            List results = query.getResultList();
            if (!results.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return false;
        }
    }

    public List<Object[]> getReplicateFormSecurityHistory()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        Date currentDate = null;
        Calendar c = Calendar.getInstance();
        try {

            c.add(Calendar.DATE, -1);
            currentDate = c.getTime();
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct hrfsh.sourceOrganizationID.organizationName, hrfsh.destinationOrganizationID.organizationName,  hrfsh.sysSubModuleID.moduleName, ");
            hqlQuery.append(" hrfsh.createdByUser.userName, hrfsh.createdDate from HrReplicateFormSecurityHistory hrfsh ");
            hqlQuery.append(" where hrfsh.createdDate >:currentDate order by hrfsh.createdDate desc ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("currentDate", currentDate);
            list = query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public boolean addReplicateFormSecurityHistory(Integer sourceOrgID, Integer destOrgID, Integer sysModuleID, Long userID)
        throws Exception
    {
        HrReplicateFormSecurityHistory hrReplicateFormSecurityHistory = new HrReplicateFormSecurityHistory();
        try {

            hrReplicateFormSecurityHistory.setSourceOrganization(sourceOrgID);
            hrReplicateFormSecurityHistory.setDestinationOrganization(destOrgID);
            hrReplicateFormSecurityHistory.setSysSubModule(sysModuleID);
            hrReplicateFormSecurityHistory.setCreatedBy(userID.intValue());
            hrReplicateFormSecurityHistory.setCreatedDate(SQLDateHelper.getSqlTimeStamp());

            getEntityManager("TalentPact").persist(hrReplicateFormSecurityHistory);
            return true;

        } catch (Exception e) {
            LOGGER_.error("", e);
            return false;
        }
    }
}