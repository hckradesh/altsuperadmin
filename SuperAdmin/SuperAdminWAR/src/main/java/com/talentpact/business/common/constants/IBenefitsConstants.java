package com.talentpact.business.common.constants;

public interface IBenefitsConstants {
    public static final String SYS_BENEFITS_VENDOR_ADD_DIALOG_HIDE = "PF('sysVendorModal').hide();";
    public static final String SYS_BENEFITS_VENDOR_SAVE_ERROR_MSG = "Error Occured While Saving SysBenefits Vendor.";
    public static final String SYS_BENEFITS_VENDOR_UPDATE_ERROR_MSG = "Error Occured While Updating SysBenefits Vendor.";
    public static final String SYS_BENEFITS_VENDOR_SAVE_SUCCESS_MSG = "SysBenefits Vendor Saved successfully.";
    public static final String SYS_BENEFITS_VENDOR_BANNER_EMPTY = "Banner is required. Please upload first.";
    public static final String SYS_BENEFITS_VENDOR_LOGO_EMPTY = "Vendor Logo is required. Please upload first.";
    public static final String SYS_BENEFITS_VENDOR_OFFERING_TEMPLATE_SAVE_SUCCESS_MSG = "SysBenefits Vendor Offering Template Saved successfully.";
    public static final String SYS_BENEFITS_VENDOR_OFFERING_TEMPLATE_SAVE_ERROR_MSG = "Unexpected Error Occurred While saving vendor Offering.";
    public static final String SYS_BENEFITS_VENDOR_CHANGE_STATUS_SUCCESS_MSG = "SysBenefits Vendor Status Changed Successfully.";
    public static final String SYS_BENEFITS_VENDOR_CHANGE_STATUS_ERROR_MSG = "Error Occured While Changing The Vendor Status.";
    public static final String SYS_BENEFITS_VENDOR_UPDATE_SUCCESS_MSG = "SysBenefits Vendor Updated Successfully.";
    public static final String SYS_BENEFITS_VENDOR_DATATABLE_RESET = "PF('sysVendorModalDataTable').clearFilters();";
    public static final String SYS_BENEFITS_VENDOR_PAGINATION_DATATABLE_RESET = "PF('sysVendorModalDataTable').getPaginator().setPage(0);";
    public static final String SYS_BENEFITS_VENDOR_DUPLICATE_ERROR_MSG = "SysBenefits Vendor already present in system.";
    public static final String OFFER_TEMPLATE_EDIT_DIALOG_HIDE = "PF('OfferTemplateEditModal').hide();";
    public static final String SYS_SECTOR_VENDOR_DATATABLE_RESET = "PF('sectorOfferSequenceDataTable').clearFilters();";
    public static final String SYS_SECTOR_VENDOR_PAGINATION_DATATABLE_RESET = "PF('sectorOfferSequenceDataTable').getPaginator().setPage(0);";
    public static final String HR_VENDOR_DATATABLE_RESET = "PF('hrOfferSequenceDataTable').clearFilters();";
    public static final String HR_VENDOR_PAGINATION_DATATABLE_RESET = "PF('hrOfferSequenceDataTable').getPaginator().setPage(0);";
    
    public static final String OFFER_CODE_DIALOG_HIDE = "PF('offerCodeModal').hide();";
    public static final String OFFER_CODE_DATATABLE_RESET = "PF('offerCodeDataTable').clearFilters();";
    public static final String OFFER_CODE_PAGINATION_DATATABLE_RESET = "PF('offerCodeDataTable').getPaginator().setPage(0);";
    public static final String SYS_BENEFITS_OFFERING_CODE_SAVE_SUCCESS_MSG = "Vendor Offering Code Saved successfully.";
    public static final String SYS_BENEFITS_OFFERING_CODE_SAVE_ERROR_MSG = "Unexpected Error Occurred While saving vendor Offering Code.";
    public static final String SYS_BENEFITS_MAXIMUM_COUNT_ERROR_MSG = "Invalid maximum count. Only numbers allowed.";
    public static final String SYS_BENEFITS_DUPLICATE_OFFER_CODE_ERROR_MSG = "Duplicate offer code. This offer code is already exist.";
    
}