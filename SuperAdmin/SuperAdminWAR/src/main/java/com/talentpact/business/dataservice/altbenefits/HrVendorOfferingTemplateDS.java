/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.AltBenefitRequestTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.HrVendorOfferingTemplate;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class HrVendorOfferingTemplateDS extends AbstractDS<HrVendorOfferingTemplate>
{
    public HrVendorOfferingTemplateDS()
    {
        super(HrVendorOfferingTemplate.class);
    }


    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingTemplate> getHrOfferingTemplateList(Long orgId, Long tenantId)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where s.organizationID=:orgId and s.tenantID=:tenantId and s.active=true order by s.sysVendorOfferingTemplate.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgId", orgId);
            query.setParameter("tenantId", tenantId.intValue());
            hrVendorOfferingTemplateList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveHrVendorOfferSequence(List<Integer> sequenceList, BenefitRequestTO benefitRequestTO)
    {
        Long vendorOfferingId = null;
        HrVendorOfferingTemplate offeringSequence = null;
        Long orgId = null;
        Map<Integer, Long> hrVendorOfferSequenceMap = null;
        try {
            orgId = benefitRequestTO.getSelectedOrganization();
            hrVendorOfferSequenceMap = benefitRequestTO.getHrVendorOfferSequenceMap();
            if (!benefitRequestTO.isEdit()) {
                for (int i = 0; i < hrVendorOfferSequenceMap.size(); i++) {
                    vendorOfferingId = hrVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence = new HrVendorOfferingTemplate();
                    offeringSequence.setOrganizationID(orgId);
                    offeringSequence.setVendorOfferingID(vendorOfferingId);
                    offeringSequence.setOfferingSequence(i + 1);
                    getEntityManager("Talentpact").persist(offeringSequence);
                }

            } else {
                for (int i = 0; i < hrVendorOfferSequenceMap.size(); i++) {
                    vendorOfferingId = hrVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence = findBenefitsHrBasedByVendorOfferingId(vendorOfferingId, benefitRequestTO.getOrgID());
                    if (offeringSequence != null) {
                        offeringSequence.setOfferingSequence(i + 1);
                        getEntityManager("Talentpact").merge(offeringSequence);
                    } else {
                        offeringSequence = new HrVendorOfferingTemplate();
                        offeringSequence.setOrganizationID(benefitRequestTO.getOrgID());
                        offeringSequence.setVendorOfferingID(vendorOfferingId);
                        offeringSequence.setOfferingSequence(sequenceList.get(i));
                        getEntityManager("Talentpact").persist(offeringSequence);
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrVendorOfferingTemplate findBenefitsHrBasedByOfferingId(Long hrVendorOfferingId, Long orgId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingSequenceList = null;
        HrVendorOfferingTemplate hrVendorOfferingSequence = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where s.hrVendorOfferingID=:hrVendorOfferingId and s.organizationID=:orgId  ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("hrVendorOfferingId", hrVendorOfferingId);
            query.setParameter("orgId", orgId);
            hrVendorOfferingSequenceList = query.getResultList();
            if (hrVendorOfferingSequenceList != null && !hrVendorOfferingSequenceList.isEmpty()) {
                hrVendorOfferingSequence = hrVendorOfferingSequenceList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequence;
    }

    
    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrVendorOfferingTemplate findBenefitsHrBasedByVendorOfferingId(Long vendorOfferingId, Long orgId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingSequenceList = null;
        HrVendorOfferingTemplate hrVendorOfferingSequence = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where s.vendorOfferingID=:vendorOfferingId and s.organizationID=:orgId ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorOfferingId", vendorOfferingId);
            query.setParameter("orgId", orgId);
            hrVendorOfferingSequenceList = query.getResultList();
            if (hrVendorOfferingSequenceList != null && !hrVendorOfferingSequenceList.isEmpty()) {
                hrVendorOfferingSequence = hrVendorOfferingSequenceList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingSequence;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void copyAllOfferingTemplateList(AltBenefitRequestTO benefitRequestTO, String persitenceKey)
        throws Exception
    {
        HrVendorOfferingTemplate hrVendorOfferingTemplate = null;
        List<VendorOfferingTemplateTO> offeringTemplateTOList = null;
        try {
            offeringTemplateTOList = benefitRequestTO.getVendorOfferingTemplateTOList();
            if (offeringTemplateTOList != null && !offeringTemplateTOList.isEmpty()) {
                for (VendorOfferingTemplateTO offeringTemplateTO : offeringTemplateTOList) {
                    if (offeringTemplateTO.getHrVendorOfferingID() != null && !offeringTemplateTO.isSelected()) {
                        hrVendorOfferingTemplate = find("Talentpact", offeringTemplateTO.getHrVendorOfferingID());
                        if (hrVendorOfferingTemplate != null) {
                            hrVendorOfferingTemplate.setActive(false);
                            getEntityManager("Talentpact").merge(hrVendorOfferingTemplate);
                        }
                    } else {
                        if (offeringTemplateTO.isSelected() && offeringTemplateTO.getHrVendorOfferingID() == null) {
                            hrVendorOfferingTemplate = findBenefitsHrBasedByVendorOfferingId(offeringTemplateTO.getVendorOfferingID(), benefitRequestTO.getOrganizationID());
                            if (hrVendorOfferingTemplate == null) {
                                hrVendorOfferingTemplate = new HrVendorOfferingTemplate();
                                hrVendorOfferingTemplate.setVendorOfferingID(offeringTemplateTO.getVendorOfferingID());
                                hrVendorOfferingTemplate.setOrganizationID(benefitRequestTO.getOrganizationID());
                                hrVendorOfferingTemplate.setTenantID(benefitRequestTO.getTenantID().intValue());
                                hrVendorOfferingTemplate.setOfferingSequence(offeringTemplateTO.getOfferingSequence());
                                hrVendorOfferingTemplate.setActive(true);
                                hrVendorOfferingTemplate.setCreatedBy((int) benefitRequestTO.getCreatedBy());
                                hrVendorOfferingTemplate.setModifiedBY((int) benefitRequestTO.getModifiedBy());
                                hrVendorOfferingTemplate.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                                hrVendorOfferingTemplate.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                                getEntityManager("Talentpact").persist(hrVendorOfferingTemplate);
                            } else {
                                hrVendorOfferingTemplate.setActive(true);
                                getEntityManager("Talentpact").merge(hrVendorOfferingTemplate);

                            }
                        }
                    }
                }

            }


        } catch (Exception ex) {
            throw ex;
        }

    }

    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingTemplate> getHrOfferingTemplateListByOrgID(Long orgID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where s.organizationID =:orgID and s.active=true order by s.sysVendorOfferingTemplate.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);

            hrVendorOfferingTemplateList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }

    /**
     * 
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<HrVendorOfferingTemplate> getHrOfferingTemplateList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrVendorOfferingTemplate s  where  s.active=true order by s.offeringSequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            hrVendorOfferingTemplateList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public List<HrVendorOfferingTemplate> changeTheStatusHrOfferingTemplate(Long vendorOfferingID, boolean status)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" update  HrVendorOfferingTemplate s  set s.active=:status  where  s.sysVendorOfferingTemplate.vendorOfferingID=:vendorOfferingID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("status", status);
            query.setParameter("vendorOfferingID", vendorOfferingID);
            query.executeUpdate();
       //     hrVendorOfferingTemplateList = getHrOfferingTemplateList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }


    /**
     * 
     * @return
     * @throws Exception
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public List<HrVendorOfferingTemplate> changeTheStatusHrOfferingTemplateByVendor()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrVendorOfferingTemplate> hrVendorOfferingTemplateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" update  HrVendorOfferingTemplate s  set s.active=false  where  s.sysVendorOfferingTemplate.sysBenefitsVendor.active=false ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
          //  query.executeUpdate();
         //   hrVendorOfferingTemplateList = getHrOfferingTemplateList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrVendorOfferingTemplateList;
    }

}