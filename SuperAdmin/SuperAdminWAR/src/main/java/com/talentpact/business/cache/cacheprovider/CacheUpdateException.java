package com.talentpact.business.cache.cacheprovider;

@SuppressWarnings("serial")
public class CacheUpdateException extends Exception
{
    
    private String message = null;


    public CacheUpdateException(Throwable cause)
    {
        super(cause);
    }


    public CacheUpdateException(String message)
    {
        super(message);
    }


    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }


    
    public void setMessage(String message)
    {
        this.message = message;
    }


}
