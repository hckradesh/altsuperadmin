/**
 * 
 */
package com.talentpact.business.dataservice.SysConstant;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.SysConstantRequestTO;
import com.talentpact.business.application.transport.output.CheckDuplicateValueTO;
import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysConstantService extends CommonService
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantService.class);

    @EJB
    SysConstantCache sysConstantCache;

    @EJB
    SysConstantDS sysConstantDS;

    public List<SysConstantResponseTO> getSysConstantTypeList()
        throws Exception

    {
        List<SysConstantResponseTO> sysConstantList = null;
        try {
            sysConstantList = sysConstantCache.getSysConstantTypeList();
            if (sysConstantList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return sysConstantList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                sysConstantList = sysConstantDS.getSysConstantTypeList();
                if (sysConstantList != null && !sysConstantList.isEmpty()) {
                    try {
                        sysConstantCache.persistSysConstantListByTenant(sysConstantList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistSysConstantList");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return sysConstantList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }


    public List<SysConstantResponseTO> getSysConstantCategoryListDropDown()
        throws Exception

    {
        List<SysConstantResponseTO> sysConstantDropDownList = null;
        try {
            sysConstantDropDownList = sysConstantCache.getSysConstantCategoryListDropDown();
            if (sysConstantDropDownList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return sysConstantDropDownList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                sysConstantDropDownList = sysConstantDS.getSysConstantCategoryListDropDown();
                if (sysConstantDropDownList != null && !sysConstantDropDownList.isEmpty()) {
                    try {
                        sysConstantCache.persistSysConstantCategoryListDropDown(sysConstantDropDownList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistSysConstantCategoryListDropDown");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return sysConstantDropDownList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }


    public void saveSysConstant(SysConstantRequestTO sysConstantRequestTO)
        throws Exception
    {
        try {
            sysConstantDS.saveSysConstant(sysConstantRequestTO);
            sysConstantCache.removeSysConstantListByTenant();
            sysConstantCache.removeSysConstantCategoryListDropDown();

        } catch (Exception e) {
            LOGGER_.info(e.getMessage());

        }

    }

    public List<String> getThemeListFromTpURL()
        throws Exception

    {
        List<String> list = null;

        try {
            list = sysConstantDS.getThemeListFromTpURL();


            return list;
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage());
            throw ex;
        }
    }

}
