/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.SysBenefitsVendor;
import com.talentpact.ui.altbenefits.transport.SysBenefitVendorTO;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysBenefitsVendorDS extends AbstractDS<SysBenefitsVendor>
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysBenefitsVendorDS.class);

    public SysBenefitsVendorDS()
    {
        super(SysBenefitsVendor.class);
    }


    @SuppressWarnings("unchecked")
    public List<SysBenefitsVendor> getSysBenefitsVendorList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysBenefitsVendor> sysBenefitsVendorsList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysBenefitsVendor from  SysBenefitsVendor sysBenefitsVendor ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            sysBenefitsVendorsList = query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage(), ex);
            throw ex;
        }
        return sysBenefitsVendorsList;
    }

/**
 * 
 * @param sysBenefitVendorTO
 * @throws Exception
 */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysBenefitsVendorDetail(SysBenefitVendorTO sysBenefitVendorTO)
        throws Exception
    {
        SysBenefitsVendor benefitsVendor = null;
        try {
            if (sysBenefitVendorTO.getVendorID() != null) {
                benefitsVendor = findBenefitVendor(sysBenefitVendorTO.getVendorID());
            }
            if (benefitsVendor == null) {
                benefitsVendor = new SysBenefitsVendor();
                benefitsVendor.setVendorCode(sysBenefitVendorTO.getVendorCode().trim());
                benefitsVendor.setVendorName(sysBenefitVendorTO.getVendorName().trim());
                benefitsVendor.setActive(sysBenefitVendorTO.getActive());
                benefitsVendor.setContractSignedDate(sysBenefitVendorTO.getContractSignedDateId());
                benefitsVendor.setContractStartDate(sysBenefitVendorTO.getContractStartDateId());
                benefitsVendor.setContractEndDate(sysBenefitVendorTO.getContractEndDateId());
                benefitsVendor.setEffectiveStartDate(sysBenefitVendorTO.getEffectiveStartDateId());
                benefitsVendor.setEffectiveEndDate(sysBenefitVendorTO.getEffectiveEndDateId());
                benefitsVendor.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                benefitsVendor.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                benefitsVendor.setTermAndConditions(sysBenefitVendorTO.getTermAndConditions());
                benefitsVendor.setVendorLogo(sysBenefitVendorTO.getVendorLogo());
                benefitsVendor.setCreatedBy(1);
                benefitsVendor.setModifiedBy(1);
                getEntityManager("Talentpact").persist(benefitsVendor);
            } else {
                benefitsVendor.setVendorCode(sysBenefitVendorTO.getVendorCode().trim());
                benefitsVendor.setVendorName(sysBenefitVendorTO.getVendorName().trim());
                benefitsVendor.setActive(sysBenefitVendorTO.getActive());
                benefitsVendor.setContractSignedDate(sysBenefitVendorTO.getContractSignedDateId());
                benefitsVendor.setContractStartDate(sysBenefitVendorTO.getContractStartDateId());
                benefitsVendor.setContractEndDate(sysBenefitVendorTO.getContractEndDateId());
                benefitsVendor.setEffectiveStartDate(sysBenefitVendorTO.getEffectiveStartDateId());
                benefitsVendor.setEffectiveEndDate(sysBenefitVendorTO.getEffectiveEndDateId());
                benefitsVendor.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                benefitsVendor.setModifiedBy(1);
                benefitsVendor.setTermAndConditions(sysBenefitVendorTO.getTermAndConditions());
                benefitsVendor.setVendorLogo(sysBenefitVendorTO.getVendorLogo());
                getEntityManager("Talentpact").merge(benefitsVendor);
            }

            getEntityManager("Talentpact").flush();
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage(), ex);
            throw ex;
        }
    }
    /**
     * 
     * @param vendorID
     * @return
     * @throws Exception
     */

    @SuppressWarnings("unchecked")
    private SysBenefitsVendor findBenefitVendor(Long vendorID)
        throws Exception
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysBenefitsVendor> SysBenefitsVendorList = null;
        SysBenefitsVendor vendor = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select t from SysBenefitsVendor  t  where t.vendorID=:vendorID  ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorID", vendorID);
            SysBenefitsVendorList = query.getResultList();
            if (SysBenefitsVendorList != null && !SysBenefitsVendorList.isEmpty()) {
                vendor = SysBenefitsVendorList.get(0);
            }
        } catch (Exception ex) {
            LOGGER_.error(ex.getMessage(), ex);
            throw ex;
        }
        return vendor;
    }
/**
 * 
 * @param vendorId
 * @param status
 * @throws Exception
 */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void changeTheStatusForBenefitsVendor(Long vendorId, boolean status)
        throws Exception
    {
        SysBenefitsVendor benefitsVendor = null;
        try {
            benefitsVendor = findBenefitVendor(vendorId);
            if(benefitsVendor.getActive()){
            benefitsVendor.setActive(false);
            }else{
                benefitsVendor.setActive(true);
            }
            benefitsVendor.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
            benefitsVendor.setModifiedBy(1);
            getEntityManager("Talentpact").merge(benefitsVendor);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
}