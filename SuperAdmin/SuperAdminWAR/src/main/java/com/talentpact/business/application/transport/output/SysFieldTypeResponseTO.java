package com.talentpact.business.application.transport.output;

import com.alt.common.transport.impl.ResponseTransferObject;

public class SysFieldTypeResponseTO extends ResponseTransferObject{



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer sysFieldTypeID;
	
    private String fieldType;

	public int getSysFieldTypeID() {
		return sysFieldTypeID;
	}

	public void setSysFieldTypeID(int sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
    
    
}
