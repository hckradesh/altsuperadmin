package com.talentpact.business.dataservice.sysModule;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.constants.IConstants;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysModule;
import com.talentpact.ui.sysModule.constants.ISysModuleConstants;
import com.talentpact.ui.sysModule.exception.SysModuleException;

/**
 * 
 * @author raghvendra.mishra
 *
 */
@Stateless
public class SysModuleDS extends AbstractDS<SysModule> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysModuleDS.class);
	
	public SysModuleDS() {
		super(SysModule.class);
	}

	/**
	 * to find SysModule List
	 * @return
	 * @throws SysModuleException
	 * @author raghvendra.mishra
	 */
	public List<SysModule> getAllModules() throws SysModuleException{
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysModule> sysModuleList = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" from SysModule ");
			query = getEntityManager(IConstants.TALENTPACT).createQuery(hqlQuery.toString());
			sysModuleList = query.getResultList();
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysModuleException(ISysModuleConstants.SYSMODULE_RETRIEVE_ERROR_MSG);
		}
		return sysModuleList;
	}
}
