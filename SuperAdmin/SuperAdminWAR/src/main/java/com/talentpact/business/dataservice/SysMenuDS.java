package com.talentpact.business.dataservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.configuration.menu.constants.IMenuPermissionConstants;
import com.talentpact.business.application.transport.input.SysMenuRequestTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.cache.cacheprovider.RedisCacheKeyConstant;
import com.talentpact.business.model.AltCheckList;
import com.talentpact.model.SysMenu;
import com.talentpact.model.organise.HrForm;
import com.talentpact.model.organise.HrFormField;
import com.talentpact.model.organise.HrFormFieldGroup;
import com.talentpact.model.worflow.UiFormField;
import com.talentpact.model.worflow.UiFormFieldGroup;

@Stateless
public class SysMenuDS extends AbstractDS<SysMenu>
{

    public SysMenuDS()
    {
        super(SysMenu.class);
    }

    public List<Object[]> getAllSysMenu()
        throws Exception
    {
        StringBuilder queryStr = null;
        Query query = null;
        try {
            queryStr = new StringBuilder();
            //  queryStr.append(" select sysmenu.menuID,sysmenu.menuName,sysmenu.menuCode ,sysMenu.sequence ,sysMenu.parentMenuID ,sysMenu.url, sysMenu.menuCategory ,sysMenu.defaultClass , sysMenu.moduleID from SysMenu sysmenu order by sysmenu.sequence");
            queryStr.append("select  uiForm.formId , uiForm.formName , uiForm.sequence , uiForm.parentFormId ,"
                    + "uiForm.url, uiForm.menuCategory ,uiForm.defaultClassCall , uiForm.subModuleId, uiForm.mobSequence from  UiForm "
                    + " uiForm where uiForm.menuType=1 and uiForm.formName!='Home'  order by uiForm.sequence  ");
            query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            //  query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            List<Object[]> resultList = query.getResultList();

            return query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            queryStr = null;
            query = null;
        }

    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void copySysMenuList(SysMenuRequestTO reqTO)
        throws Exception
    {
        HrForm hrForm = null;
        List<SysMenuTO> sysMenuTOlist = null;
        List<SysMenuTO> sysMenuTOCachelist = null;
        AltCheckList altCheckList = null;
        Map<Long, List<SysMenuTO>> menuCacheMap = null;
        List<UiFormFieldGroup> uiFormFieldGroupList = null;
        List<UiFormField> uiFormFList = null;
        try {

            sysMenuTOlist = reqTO.getSysMenuTOList();
            sysMenuTOCachelist = new ArrayList<SysMenuTO>();
            //get sysRoleType entity from TalentPact -admin Portal ---start here
            //sysRoleTypeMap = getSysRoleTypeFromTalentPact_AdminPortal();

            //end here 

            for (SysMenuTO reqTO1 : sysMenuTOlist) {

                if (reqTO1.isSelected()) {
                    hrForm = findHrForm(reqTO1.getMenuID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue());
                    if (hrForm == null) {
                        hrForm = new HrForm();
                        // hrForm.setOrganizationID(reqTO.getOrganizationID().intValue());
                        hrForm.setOrganizationId(reqTO.getOrganizationID().intValue());
                        hrForm.setTenantId(reqTO.getTenantID().intValue());
                        hrForm.setCreatedBy(reqTO.getCreatedBy().intValue());
                        hrForm.setModifiedBy(reqTO.getModifiedBy().intValue());
                        hrForm.setCreatedDate(new Date());
                        hrForm.setModifiedDate(new Date());
                        hrForm.setSysFormId((new Long(reqTO1.getMenuID())).intValue());
                        hrForm.setActive(true);
                        // hrForm.setP(reqTO1.getParentMenuID());
                        hrForm.setFormName(null);
                        hrForm.setSequence(reqTO1.getSequence());
                        hrForm.setMobileSequence(reqTO1.getMobSequence() != 0?reqTO1.getMobSequence():null);
                        hrForm.setCustomComponentAllowed(false);
                        hrForm.setPermissionId(12);
                        sysMenuTOCachelist.add(reqTO1);
                        getEntityManager("TalentPact").persist(hrForm);

                        if (reqTO1.isChild()) {
                            uiFormFieldGroupList = getUiFormFildGroupForUiForm(reqTO1.getMenuID());
                            for (UiFormFieldGroup ui : uiFormFieldGroupList) {

                                HrFormFieldGroup hrFormFieldGroup = new HrFormFieldGroup();
                                hrFormFieldGroup.setHrFieldGroupName(null);
                                hrFormFieldGroup.setSysFieldGroupId(ui.getFieldGroupId());
                                hrFormFieldGroup.setHrFormId(hrForm.getHrFormId());
                                hrFormFieldGroup.setOrganizationID(reqTO.getOrganizationID().intValue());
                                hrFormFieldGroup.setTenantId(reqTO.getTenantID().intValue());
                                hrFormFieldGroup.setCreatedBy(reqTO.getCreatedBy().intValue());
                                hrFormFieldGroup.setModifiedBy(reqTO.getModifiedBy().intValue());
                                hrFormFieldGroup.setCreatedDate(new Date());
                                hrFormFieldGroup.setModifiedDate(new Date());
                                getEntityManager("TalentPact").persist(hrFormFieldGroup);


                                uiFormFList = getAllFormFieldsForUiFormFieldGroup(ui.getFieldGroupId());
                                for (UiFormField uf : uiFormFList) {
                                    HrFormField hrFormField = new HrFormField();
                                    hrFormField.setActionType(false);
                                    hrFormField.setActive(true);
                                    hrFormField.setApprovalRequired(uf.getApprovalRequired());
                                    hrFormField.setAttachmentEnabled(uf.isAttachmentEnabled());
                                    hrFormField.setAttachmentRequired(uf.getAttachmentRequired());
                                    hrFormField.setClientId(null);
                                    hrFormField.setEffectiveDateRequired(uf.getEffectiveDateRequired());
                                    hrFormField.setHrFormFieldGroupId(hrFormFieldGroup.getHrFormFieldGroupId());
                                    hrFormField.setOrganizationID(reqTO.getOrganizationID().intValue());
                                    hrFormField.setTenantId(reqTO.getTenantID().intValue());
                                    hrFormField.setCreatedBy(reqTO.getCreatedBy().intValue());
                                    hrFormField.setModifiedBy(reqTO.getModifiedBy().intValue());
                                    hrFormField.setCreatedDate(new Date());
                                    hrFormField.setModifiedDate(new Date());
                                    hrFormField.setSysFormFieldID(uf.getFormFieldId());
                                    hrFormField.setConfigurationValueWeb(uf.getConfigurationValueWeb());
                                    hrFormField.setConfigurationValueMobile(uf.getConfigurationValueMobile());
                                    hrFormField.setConfigurationValueChatBot(uf.getConfigurationValueChatBot());
                                    hrFormField.setGroupParameter(uf.getIsGroupParameter());
                                    getEntityManager("TalentPact").persist(hrFormField);

                                }

                            }

                        }


                    } else {

                        hrForm.setOrganizationId(reqTO.getOrganizationID().intValue());
                        hrForm.setTenantId(reqTO.getTenantID().intValue());
                        hrForm.setCreatedBy(reqTO.getCreatedBy().intValue());
                        hrForm.setModifiedBy(reqTO.getModifiedBy().intValue());
                        hrForm.setCreatedDate(new Date());
                        hrForm.setModifiedDate(new Date());
                        hrForm.setSysFormId(new Long(reqTO1.getMenuID()).intValue());
                        // hrForm.setParentMenuID(reqTO1.getParentMenuID());
                        hrForm.setFormName(null);
                        hrForm.setMobileSequence(reqTO1.getMobSequence() != 0?reqTO1.getMobSequence():null);
                        hrForm.setSequence(reqTO1.getSequence());
                        hrForm.setCustomComponentAllowed(false);
                        hrForm.setActive(true);
                        sysMenuTOCachelist.add(reqTO1);
                        getEntityManager("TalentPact").merge(hrForm);

                    }

                } else {
                    hrForm = findHrForm(reqTO1.getMenuID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue());
                    if (hrForm != null) {
                        hrForm.setActive(false);
                        getEntityManager("TalentPact").merge(hrForm);
                    }
                }


            }

            //saving check -list in altCheckList tabble 
            altCheckList = findAltCheckList(reqTO.getSysCheckListID().intValue(), reqTO.getOrganizationID(), reqTO.getTenantID());
            if (altCheckList == null) {
                altCheckList = new AltCheckList();
                altCheckList.setSysCheckListID(reqTO.getSysCheckListID().intValue());
                altCheckList.setOrganizationID(reqTO.getOrganizationID());
                altCheckList.setTenantID(reqTO.getTenantID());
                altCheckList.setModifiedDate(new Date());
                altCheckList.setCreatedDate(new Date());
                altCheckList.setIsCompleted(true);
                altCheckList.setCreatedBy(reqTO.getCreatedBy());
                altCheckList.setModifiedBy(reqTO.getModifiedBy());

                getEntityManager("AltCommon").persist(altCheckList);
            }
            //end here 


            getEntityManager("AltCommon").flush();
            getEntityManager("TalentPact").flush();

            // persisting to cache  ---code start here 
            //  sysMenuTOCachelist = findAltMenuListByMenuCategory(sysMenuTOCachelist, 3);
            //  menuCacheMap = prepareMenuListForCache(sysMenuTOCachelist);
            //putting value in Redis ...Key is ORGANIZE_ALLROLE__organizationID_tenantID

            RedisClient.delete(RedisCacheKeyConstant.ALTADMIN_MENULIST + reqTO.getTenantID() + ":" + reqTO.getOrganizationID());
            RedisClient.deleteByPattern(IMenuPermissionConstants.ALTORGNIZE_MENU_TOP_PATTERN + ":" + "*");
        } catch (Exception Ex) {
            Ex.printStackTrace();
            throw Ex;
        } finally {
            hrForm = null;
            sysMenuTOlist = null;
            altCheckList = null;
            menuCacheMap = null;
        }


    }

    private List<UiFormField> getAllFormFieldsForUiFormFieldGroup(Integer fieldGroupID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<UiFormField> uiFormFieldList = null;
        UiFormField result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiFormField from UiFormField uiFormField where uiFormField.fieldGroupId=:FieldGroupID ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("FieldGroupID", (new Long(fieldGroupID)).intValue());

            uiFormFieldList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return uiFormFieldList;
    }

    private List<UiFormFieldGroup> getUiFormFildGroupForUiForm(long menuID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<UiFormFieldGroup> uiFormFieldGroupList = null;
        UiFormFieldGroup result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiFormFieldGroup from UiFormFieldGroup uiFormFieldGroup where uiFormFieldGroup.formId=:FormID ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("FormID", (new Long(menuID)).intValue());

            uiFormFieldGroupList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return uiFormFieldGroupList;
    }

    private HrForm findHrForm(long menuID, Integer OrgId, Integer TenantId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrForm> hrFormList = null;
        HrForm result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select hrForm from HrForm hrForm  where hrForm.sysFormId=:MenuID and hrForm.organizationId=:OrgID and hrForm.tenantId=:TenantID ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("MenuID", (new Long(menuID)).intValue());
            query.setParameter("OrgID", OrgId);
            query.setParameter("TenantID", TenantId);
            hrFormList = query.getResultList();
            if (hrFormList != null && hrFormList.size() > 0) {
                result = hrFormList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public AltCheckList findAltCheckList(int sysCheckListId, Long OrgId, Long TenantId)
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<AltCheckList> altCheckList = null;
        AltCheckList result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select altCheckList from AltCheckList altCheckList where altCheckList.sysCheckListID=:SysCheckListID and altCheckList.organizationID=:OrgID and altCheckList.tenantID=:TenantID ");

            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("SysCheckListID", sysCheckListId);
            query.setParameter("OrgID", OrgId);
            query.setParameter("TenantID", TenantId);
            altCheckList = query.getResultList();
            if (altCheckList != null && altCheckList.size() > 0) {
                result = altCheckList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public List<SysMenuTO> syncMenuList(SysMenuRequestTO reqTO)
        throws Exception
    {
        HrForm hrForm = null;
        List<SysMenuTO> sysMenuTOlist = null;

        try {

            sysMenuTOlist = reqTO.getSysMenuTOList();

            for (SysMenuTO reqTO1 : sysMenuTOlist) {
                // altMenu = findAltMenu(reqTO1.getMenuID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue());
                hrForm = findHrForm(reqTO1.getMenuID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue());

                if (hrForm == null) {
                    reqTO1.setSelected(false);
                } else {

                    reqTO1.setSelected(hrForm.getActive());

                }
            }
            return sysMenuTOlist;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

            hrForm = null;
            sysMenuTOlist = null;
        }
    }

    private Map<Long, List<SysMenuTO>> prepareMenuListForCache(List<SysMenuTO> sysMenuTOlist)
        throws Exception
    {
        Map<Long, List<SysMenuTO>> menuCacheMap = null;
        List<SysMenuTO> nullList = null;
        try {
            nullList = new ArrayList<SysMenuTO>();
            menuCacheMap = new HashMap<Long, List<SysMenuTO>>();
            if (sysMenuTOlist == null || sysMenuTOlist.isEmpty()) {
                return menuCacheMap;
            }
            for (SysMenuTO menu : sysMenuTOlist) {
                if (menu.getParentMenuID() == null) {
                    nullList.add(menu);
                }
            }

            menuCacheMap.put((long) 0, nullList);
            for (SysMenuTO menu : nullList) {
                prepareMenuMap(sysMenuTOlist, menu, menuCacheMap);
            }


            return menuCacheMap;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            menuCacheMap = null;
            nullList = null;
        }
    }

    private void prepareMenuMap(List<SysMenuTO> sysMenuTOlist, SysMenuTO menu, Map<Long, List<SysMenuTO>> menuCacheMap)
        throws Exception
    {

        List<SysMenuTO> childList = null;

        try {
            childList = findChildMenuList(menu, sysMenuTOlist);
            if (childList == null | childList.isEmpty()) {
                return;
            } else {
                menuCacheMap.put(menu.getMenuID(), childList);
                for (SysMenuTO menu1 : childList) {
                    prepareMenuMap(sysMenuTOlist, menu1, menuCacheMap);
                }
            }

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }
    }

    private List<SysMenuTO> findChildMenuList(SysMenuTO menu, List<SysMenuTO> sysMenuTOlist)
        throws Exception
    {
        List<SysMenuTO> childList = null;
        try {
            childList = new ArrayList<SysMenuTO>();
            for (SysMenuTO menu1 : sysMenuTOlist) {
                if (menu1.getParentMenuID() != null) {
                    if (menu.getMenuID() == menu1.getParentMenuID()) {
                        childList.add(menu1);
                    }
                }
            }
            return childList;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }
    }

    private List<SysMenuTO> findAltMenuListByMenuCategory(List<SysMenuTO> sysMenuTOCachelist, Integer i)
        throws Exception
    {
        List<SysMenuTO> returnList = null;
        try {
            returnList = new ArrayList<SysMenuTO>();
            for (SysMenuTO menu : sysMenuTOCachelist) {
                if (menu.getMenuCategory() == null | menu.getMenuCategory() == i) {
                    returnList.add(menu);
                }
            }
            return returnList;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            returnList = null;
        }
    }


}
