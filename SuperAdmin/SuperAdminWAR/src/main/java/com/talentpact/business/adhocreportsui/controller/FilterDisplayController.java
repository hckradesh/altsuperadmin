package com.talentpact.business.adhocreportsui.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.controller.Filter;
import com.talentpact.adhocreports.business.to.FilterGroupTo;
import com.talentpact.adhocreports.business.to.GenericInputTo;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;

@SessionScoped
@Named("filterDisplayController")
public class FilterDisplayController implements Serializable{

	private static final long serialVersionUID = -5739319804242791024L;
	private static final Logger LOGGER = Logger.getLogger(FilterDisplayController.class);
	
	@Inject
	ReportDetailBean reportDetailBean;
	
	public void initialize(){
	}
	
	public void getFilters(){
		GenericInputTo genericInputTo = new GenericInputTo();
		genericInputTo.setId(reportDetailBean.getSelectedReport().getReportId());
		genericInputTo.setReportType(reportDetailBean.getSelectedReport().getReportType());
		genericInputTo.setApplicationInputMap(reportDetailBean.getApplicationInputMap());
		try {
			List<FilterGroupTo> filterGroupToList = Filter.process(genericInputTo);
			reportDetailBean.setSelectReportFilters(filterGroupToList);
		} catch (Exception e) {
			LOGGER.error("Error inside getFilters() method while fetching filter for report :"+
					reportDetailBean.getSelectedReport().getReportId() + " with type : " +
					reportDetailBean.getSelectedReport().getReportType(), e);
		}
	}
	
	public void getFilters(ReportDetail report){
		GenericInputTo genericInputTo = new GenericInputTo();
		genericInputTo.setId(report.getReportId());
		genericInputTo.setReportType(report.getReportType());
		genericInputTo.setApplicationInputMap(reportDetailBean.getApplicationInputMap());
		try {
			List<FilterGroupTo> filterGroupToList = Filter.process(genericInputTo);
			reportDetailBean.setSelectReportFilters(filterGroupToList);
			reportDetailBean.setSelectedReport(report);
		} catch (Exception e) {
			LOGGER.error("Error inside getFilters() method while fetching filter for report :"+
					reportDetailBean.getSelectedReport().getReportId() + " with type : " +
					reportDetailBean.getSelectedReport().getReportType(), e);
		}
	}
	
}
