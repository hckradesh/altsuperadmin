/*
 * 
 * 
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.talentpact.business.dataservice;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractDS<T> {

	private Class<T> entityClass;

	@PersistenceContext(unitName = "TalentPactAuth")
	private EntityManager em;

	@PersistenceContext(unitName = "TalentPactATS")
	private EntityManager em1;

	@PersistenceContext(unitName = "AltCommon")
	private EntityManager altCommon;

	@PersistenceContext(unitName = "TalentPact")
	private EntityManager talentpact;

	@PersistenceContext(unitName = "AltDataManager")
	private EntityManager emAltDataManager;

	@PersistenceContext(unitName = "TalentPactFormEngine_New")
	private EntityManager emFormEngine;
	
	@PersistenceContext(unitName = "TalentPactInsightsAgg")
    private EntityManager emInferAgg;

	// @Resource private SessionContext ctx;
	public AbstractDS(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected EntityManager getEntityManager() {
		return em;
	}

	protected EntityManager getEntityManager(String unit) {
		EntityManager resultEntity;
		resultEntity = em;
		if (unit != null) {
			if (unit.equalsIgnoreCase("TalentPactATS")) {
				resultEntity = em1;
			} else if (unit.equalsIgnoreCase("TalentPactAuth")) {
				resultEntity = em;
			} else if (unit.equalsIgnoreCase("AltCommon")) {
				resultEntity = altCommon;
			} else if (unit.equalsIgnoreCase("TalentPact")) {
				resultEntity = talentpact;
			} else if (unit.equalsIgnoreCase("AltDataManager")) {
				resultEntity = emAltDataManager;
			} else if (unit.equalsIgnoreCase("TalentPactFormEngine_New")) {
				resultEntity = emFormEngine;
			}
			else if (unit.equalsIgnoreCase("TalentPactInsightsAgg")) {
                resultEntity = emInferAgg;
            }
		}
		return resultEntity;
	}

	public void create(T entity) {
		getEntityManager().persist(entity);
	}

	public void create(String unit, T entity) {
		getEntityManager(unit).persist(entity);
	}

	public void edit(T entity) {
		getEntityManager().merge(entity);
	}

	public void edit(String unit, T entity) {
		getEntityManager(unit).merge(entity);
	}

	public void remove(T entity) {
		getEntityManager().remove(getEntityManager().merge(entity));
	}

	public void remove(String unit, T entity) {
		getEntityManager(unit).remove(getEntityManager(unit).merge(entity));
	}

	public T find(Object id) {
		return getEntityManager().find(entityClass, id);
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public T find(String unit, Object id) {
		return getEntityManager(unit).find(entityClass, id);
	}

	public List<T> findAll() {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}

	public List<T> findAll(String unit) {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(unit)
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager(unit).createQuery(cq).getResultList();
	}

	public List<T> findRange(int[] range) {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	public List<T> findRange(String unit, int[] range) {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(unit)
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		javax.persistence.Query q = getEntityManager(unit).createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	public int count() {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
		cq.select(getEntityManager().getCriteriaBuilder().count(rt));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	public int count(String unit) {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(unit)
				.getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
		cq.select(getEntityManager(unit).getCriteriaBuilder().count(rt));
		javax.persistence.Query q = getEntityManager(unit).createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	public void flush() {
		getEntityManager().flush();
	}

	public void flush(String persistanceUnit) {
		getEntityManager(persistanceUnit).flush();
	}

	public void clear() {
		getEntityManager().clear();
	}

	public void clear(String persistanceUnit) {
		getEntityManager(persistanceUnit).clear();
	}
}
