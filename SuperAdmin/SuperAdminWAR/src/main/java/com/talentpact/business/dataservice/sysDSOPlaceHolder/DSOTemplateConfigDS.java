package com.talentpact.business.dataservice.sysDSOPlaceHolder;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysSubModule;
import com.talentpact.model.SysTemplateConfig;
import com.talentpact.ui.DSPTemplateConfig.to.DSOTemplateConfigTO;
import com.talentpact.ui.common.bean.UserSessionBean;

@Stateless
public class DSOTemplateConfigDS extends AbstractDS<SysTemplateConfig>{

	private static final Logger LOGGER_ = LoggerFactory.getLogger(DSOTemplateConfigDS.class);

	@Inject
    UserSessionBean userSessionBean;
	
	public DSOTemplateConfigDS() {
		super(SysTemplateConfig.class);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("unchecked")
	public List<SysTemplateConfig> getDSOTemplateConfigList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        SysSubModule subModule = null;
        Integer moduleId = null;
		try {
			subModule = getModuleIDbyName("DSO");
			moduleId = (Integer) subModule.getModuleId();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			subModule = null;
			moduleId = null;
		}
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysTemplateConfig stc where stc.hrOrganizationId = 1 and stc.sysTenantId = 1 and stc.isAdmin = 1 and stc.subModule.moduleId = moduleId");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }
	
	public Integer setDSOTemplateConfig(DSOTemplateConfigTO dsoTemplateConfigTO) throws Exception{
			SysTemplateConfig sysTemplateConfig=null;
			SysTemplateConfig sysTemplateConfig1=null;
			SysTemplateConfig sysTemplateConfig2=null;
			SysSubModule subModule = null;
			try {
				subModule = getModuleIDbyName(dsoTemplateConfigTO.getModuleName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				subModule = null;
			}
	        int templateId = 0;
			try {
				sysTemplateConfig1 = findDSOTemplate(dsoTemplateConfigTO);
            Date date = new Date();
            if (sysTemplateConfig1 == null) {
            	sysTemplateConfig2 = checkDSOTemplate(dsoTemplateConfigTO);
            	if(sysTemplateConfig2 == null){
            			sysTemplateConfig = new SysTemplateConfig();
                        sysTemplateConfig.setTemplateName(dsoTemplateConfigTO.getTemplateName());
                        sysTemplateConfig.setDescription(dsoTemplateConfigTO.getDescription());
                        sysTemplateConfig.setTo(dsoTemplateConfigTO.getTo());
                        sysTemplateConfig.setCc(dsoTemplateConfigTO.getCc());
                        sysTemplateConfig.setBcc(dsoTemplateConfigTO.getBcc());
                        sysTemplateConfig.setSubject(dsoTemplateConfigTO.getSubject());
                        sysTemplateConfig.setContent(dsoTemplateConfigTO.getContent());
                        sysTemplateConfig.setVisible(true);
                        sysTemplateConfig.setSysTenantId(1);
                        sysTemplateConfig.setHrOrganizationId(1);
                        sysTemplateConfig.setIsAdmin(true);
                        sysTemplateConfig.setSubModule(subModule);
                        getEntityManager("TalentPact").persist(sysTemplateConfig);
                        getEntityManager("TalentPact").flush();
                        templateId = sysTemplateConfig.getTemplateId();
                        	
            		
            	}
            	
                
            } else {
            	sysTemplateConfig2 = checkDSOTemplate(dsoTemplateConfigTO);
                if(sysTemplateConfig2 == null){
                	sysTemplateConfig1.setTemplateName(dsoTemplateConfigTO.getTemplateName());
                	sysTemplateConfig1.setDescription(dsoTemplateConfigTO.getDescription());
                    sysTemplateConfig1.setTo(dsoTemplateConfigTO.getTo());
                    sysTemplateConfig1.setCc(dsoTemplateConfigTO.getCc());
                    sysTemplateConfig1.setBcc(dsoTemplateConfigTO.getBcc());
                    sysTemplateConfig1.setSubject(dsoTemplateConfigTO.getSubject());
                    sysTemplateConfig1.setContent(dsoTemplateConfigTO.getContent());
                    sysTemplateConfig1.setVisible(true);
                    sysTemplateConfig1.setSysTenantId(1);
                    sysTemplateConfig1.setHrOrganizationId(1);
                    sysTemplateConfig1.setIsAdmin(true);
                    sysTemplateConfig1.setSubModule(subModule);
                    getEntityManager("TalentPact").merge(sysTemplateConfig1);
                    getEntityManager("TalentPact").flush();
                    templateId = sysTemplateConfig1.getTemplateId();
                }
                
            }
	            
	        } catch (Exception e) {
	            LOGGER_.error("", e.getMessage(), e);
	        }
			return templateId;
	    }
	 
		
		private SysSubModule getModuleIDbyName (String moduleName) throws Exception {
	    	StringBuilder hqlQuery = null;
	        Query query = null;
	        
	        if (moduleName == null)
	            return null;
	        SysSubModule sysSubModule = null;
	    	try {
	            hqlQuery = new StringBuilder();
	            hqlQuery.append(" select ssm from SysSubModule ssm where ssm.moduleName = :moduleName");
	            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
	            query.setParameter("moduleName", moduleName);
	            sysSubModule = (SysSubModule) query.getResultList().get(0);
	        } catch (Exception ex) {
	        	sysSubModule = null;
	            throw ex;
	        }
	    	
			return sysSubModule;
	    	
	    }
		
				
		@SuppressWarnings("unchecked")
		private SysTemplateConfig findDSOTemplate(DSOTemplateConfigTO dsoTemplateConfigTO)
	            throws Exception {
	            Query query = null;
	            StringBuilder hqlQuery = null;
	            List<SysTemplateConfig> sysTemplateConfigList = null;
	            SysTemplateConfig result = null;
	            try {

	                hqlQuery = new StringBuilder();
	                hqlQuery.append("select stc from SysTemplateConfig  stc  where stc.templateId = :templateId  and stc.hrOrganizationId=1 and stc.sysTenantId = 1 and stc.isAdmin = 1");
	                query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
	                query.setParameter("templateId", dsoTemplateConfigTO.getTemplateId());
	                sysTemplateConfigList = query.getResultList();
	                if (sysTemplateConfigList != null && sysTemplateConfigList.size() > 0) {
	                    result = sysTemplateConfigList.get(0);
	                }
	                return result;
	            } catch (Exception ex) {
	                /*LOGGER_.warn("Error : " + ex.toString());*/
	                throw ex;
	            } finally {
	                hqlQuery = null;
	                query = null;
	            }
	        }
		
		@SuppressWarnings({ "unused", "unchecked" })
		private SysTemplateConfig checkDSOTemplate(DSOTemplateConfigTO dsoTemplateConfigTO)
	            throws Exception {
	            Query query = null;
	            StringBuilder hqlQuery = null;
	            List<SysTemplateConfig> sysTemplateConfigList = null;
	            SysTemplateConfig result = null;
	            try {

	                hqlQuery = new StringBuilder();
	                hqlQuery.append("select stc from SysTemplateConfig  stc  where stc.templateName=:templateName and stc.templateId!=:templateId and stc.hrOrganizationId=1 and stc.sysTenantId = 1 and stc.isAdmin = 1");
	                query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
	                query.setParameter("templateName", dsoTemplateConfigTO.getTemplateName());
	                query.setParameter("templateId", dsoTemplateConfigTO.getTemplateId());
	                sysTemplateConfigList = query.getResultList();
	                if (sysTemplateConfigList != null && sysTemplateConfigList.size() > 0) {
	                    result = sysTemplateConfigList.get(0);
	                }
	                return result;
	            } catch (Exception ex) {
	                /*LOGGER_.warn("Error : " + ex.toString());*/
	                throw ex;
	            } finally {
	                hqlQuery = null;
	                query = null;
	            }
	        }
}
