/**
 * 
 */
package com.talentpact.business.dataservice.loginserver;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.ats.core.entity.LoginServerType;
import com.talentpact.business.dataservice.AbstractDS;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class LoginServerTypeDS extends AbstractDS<LoginServerType>
{
    public LoginServerTypeDS()
    {
        super(LoginServerType.class);
    }


    @SuppressWarnings("unchecked")
    public List<LoginServerType> getLoginServerType()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<LoginServerType> loginServerTypeList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from LoginServerType s");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            loginServerTypeList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return loginServerTypeList;
    }

}