/**
 * 
 */
package com.talentpact.business.dataservice.SysConstantCategory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.talentpact.business.application.transport.input.SysConstantCategoryRequestTO;
import com.talentpact.business.application.transport.output.SysConstantCategoryResponseTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysConstantCategory;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysConstantCategoryDS extends AbstractDS<SysConstantCategory>
{
    public SysConstantCategoryDS()
    {
        super(SysConstantCategory.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysConstantCategoryResponseTO> getSysConstantCategoryList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysConstantCategoryResponseTO> list = null;
        List<SysConstantCategory> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("from SysConstantCategory");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getCategoryList(resultList);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;

        }
        return list;
    }

    @SuppressWarnings("null")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysConstantCategory(SysConstantCategoryRequestTO sysConstantCategoryRequestTO)
    {
        SysConstantCategory sysConstantCategory = null;
        Date date = new Date();
        try {
            sysConstantCategory = findSysConstantCategory(sysConstantCategoryRequestTO);
            if (sysConstantCategory == null) {
                sysConstantCategory = new SysConstantCategory();
                sysConstantCategory.setSysConstantCategoryName(sysConstantCategoryRequestTO.getSysConstantCategoryName());
                sysConstantCategory.setCreatedDate(date);
                sysConstantCategory.setModifiedDate(date);
                sysConstantCategory.setModifiedBy(sysConstantCategoryRequestTO.getLoggedInUserId());
                sysConstantCategory.setCreatedBy(sysConstantCategoryRequestTO.getLoggedInUserId());
                getEntityManager("AltCommon").persist(sysConstantCategory);
            } else {
                sysConstantCategory.setSysConstantCategoryName(sysConstantCategoryRequestTO.getSysConstantCategoryName());
                sysConstantCategory.setSysConstantCategoryID(sysConstantCategoryRequestTO.getSysConstantCategoryID());
                sysConstantCategory.setCreatedDate(date);
                sysConstantCategory.setModifiedDate(date);
                sysConstantCategory.setModifiedBy(sysConstantCategoryRequestTO.getLoggedInUserId());
                getEntityManager("AltCommon").merge(sysConstantCategory);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @SuppressWarnings("unchecked")
    private SysConstantCategory findSysConstantCategory(SysConstantCategoryRequestTO sysConstantCategoryTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysConstantCategory> sysConstantCategoryList = null;
        SysConstantCategory result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select scc from SysConstantCategory scc  where scc.sysConstantCategoryID=:categoryId  ");

            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("categoryId", sysConstantCategoryTO.getSysConstantCategoryID());
            sysConstantCategoryList = query.getResultList();
            if (sysConstantCategoryList != null && sysConstantCategoryList.size() > 0) {
                result = sysConstantCategoryList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }


    public static List<SysConstantCategoryResponseTO> getCategoryList(List<SysConstantCategory> resultList)
        throws Exception
    {
        List<SysConstantCategoryResponseTO> list = null;
        SysConstantCategoryResponseTO responseTO = null;
        try {
            list = new ArrayList<SysConstantCategoryResponseTO>();
            for (SysConstantCategory object : resultList) {
                responseTO = new SysConstantCategoryResponseTO();

                if (object.getSysConstantCategoryID() != null) {

                    responseTO.setSysConstantCategoryID(object.getSysConstantCategoryID());
                }
                if (object.getSysConstantCategoryName() != null) {

                    responseTO.setSysConstantCategoryName(object.getSysConstantCategoryName());
                }


                list.add(responseTO);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            responseTO = null;
        }
        return list;

    }


}
