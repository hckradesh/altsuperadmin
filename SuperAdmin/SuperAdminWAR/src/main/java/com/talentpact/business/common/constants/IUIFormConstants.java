package com.talentpact.business.common.constants;

public class IUIFormConstants
{
public static String FORM_SAVE_DIALOG_HIDE = "PF('addFormListModel').hide();";
public static String FORM_ADD_SUCCESS_MSG = "FORM created successfully.";
public static String FORM_ADD_ERROR_MSG = "Unexpected error occured while creating new grade.";
public static String FORM_UPDATE_DIALOG_HIDE = "PF('gradeListEditModel').hide();";
public static String FORM_UPDATE_SUCCESS_MSG = "Form details updated successfully.";
public static String FORM_UPDATE_ERROR_MSG = "Unexpected error occured while updating grade details.";
public static String ACTIVE_STRING = "Active";
public static String INACTIVE_STRING = "Inactive";
public static String FORM_ACTIVE_SUCCESS_MSG = "Form activated successfully";
public static String FORM_INACTIVE_SUCCESS_MSG = "Form deactivated successfully";
public static final String FORM_UPLOAD_PAGE_INVALID_COLUMN_TYPE = "Invalid information for ";
public static String UPLOAD_STATUS = "Upload Status Summary";
public static String FORM_DUPLICATE_COLUMN = " Form already exists";
public static final String FORM_UPLOAD_PAGE_EMPTY_COLUMN = " is missing, ";
public static final String FORM_UPLOAD_PAGE_COLUMNCOUNT = "12";
public static final String FORM_UPLOAD_PAGE_INVALID_HEADER = "Invalid Header";
public static final String FORM_UPLOAD_PAGE_VALID_RECORD = "Valid Record";
public static final String FORM_UPLOAD_PAGE_INVALID_RECORD = "Invalid Record";
public static final String MASTER_FORM_UPLOAD_EMPTY_SHEET = "Empty sheet uploaded. No data found in excel sheet.";
public static final String MASTER_FORM_UPLOAD_LESS_COLUMN_COUNT_MSG = "File format is not correct. Some columns are missing";
public static final String MASTER_FORM_UPLOAD_CONSTRAINTS_VOILATION_MSG = "Constraints voilation occured. Masterdata upload rolled back.";
public static final String MASTER_FORM_UPLOAD_SUCCESS_MSG = "Form data uploaded successfully.";
public static final String MASTER_FORM_UPLOAD_EXCESS_COLUMN_COUNT_MSG = "File format is not correct. Column count exceeded the maximun column allowed";


}
