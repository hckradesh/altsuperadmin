/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.organization.SysSector;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysSectorDS extends AbstractDS<SysSector>
{
    public SysSectorDS()
    {
        super(SysSector.class);
    }


    @SuppressWarnings("unchecked")
    public List<Object[]> getSysSectorList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> sysSectorObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct s.SectorID,s.SectorLabel,s.SectorCode from SysSector s");
          //  hqlQuery.append("  join talentpactauth..tp_organization o");
          //  hqlQuery.append(" on  o.SectorCode=s.SectorCode ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            sysSectorObjectList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return sysSectorObjectList;
    }

}