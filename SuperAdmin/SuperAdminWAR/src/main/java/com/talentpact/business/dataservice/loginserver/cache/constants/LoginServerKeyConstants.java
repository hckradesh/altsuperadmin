package com.talentpact.business.dataservice.loginserver.cache.constants;

/**
 * 
 * @author javed.ali
 *
 */
public interface LoginServerKeyConstants
{
    public static final String LOGIINSERVER_ACCESS_ROOT = "SUPERADMIN:LOGINSERVER";
    public static final String COLLON = ":";
    public static final String NO_DATA_IN_REDIS ="Data not found in Redis. Moving to DB";
  
}
