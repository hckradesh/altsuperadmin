package com.talentpact.business.common.constants;


public interface IMeshRequestBlockingConfig
{
    public static String MESHREQUESTBLOCKINGCONFIG_ADD_DIALOG_HIDE = "PF('meshRequestBlockingConfigAddModal').hide();";

    public static String MESHREQUESTBLOCKINGCONFIG_EDIT_DIALOG_HIDE = "PF('meshRequestBlockingConfigEditModal').hide();";

    public static String MESHREQUESTBLOCKINGCONFIG_SAVE_ERROR_MSG = "Error occured while saving MeshRequestBlockingConfig.";

    public static String MESHREQUESTBLOCKINGCONFIG_UPDATE_ERROR_MSG = "Error occured while updating MeshRequestBlockingConfig.";

    public static String MESHREQUESTBLOCKINGCONFIG_SAVE_SUCCESS_MSG = "MeshRequestBlockingConfig saved successfully.";

    public static String MESHREQUESTBLOCKINGCONFIG_UPDATE_SUCCESS_MSG = "MeshRequestBlockingConfig updated successfully.";

    public static String MESHREQUESTBLOCKINGCONFIG_DATATABLE_RESET = "PF('meshRequestBlockingConfigDataTable').clearFilters()";

    public static String MESHREQUESTBLOCKINGCONFIG_PAGINATION_DATATABLE_RESET = "PF('meshRequestBlockingConfigDataTable').getPaginator().setPage(0);";

    public static String MESHREQUESTBLOCKINGCONFIG_DUPLICATE_ERROR_MSG = "MeshRequestBlockingConfig already present in system.";
}
