package com.talentpact.business.cache;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

import com.alt.cache.exception.RedisConnectionException;

/**
 * @author seema.sharma
 *
 */
public class RedisPoolFactory
{
    private static JedisPool pool;

    public static JedisPool getJedisPoolInstance()
        throws RedisConnectionException
    {
        if (pool == null) {
            try {
                synchronized (RedisPoolFactory.class) {
                    if (pool == null) {
                        final String ipAddress = System.getProperty("Redis.IP");
                        final int port = Integer.parseInt(System.getProperty("Redis.Port"));
                        int poolThreadSize = Integer.parseInt(System.getProperty("Redis.Client.PoolSize"));
                        String password = System.getProperty("Redis.Password");
                        String databaseIdString = System.getProperty("Redis.Database.Id");
                        int databaseId = Protocol.DEFAULT_DATABASE;
                        if(databaseIdString != null){
                        	databaseId = Integer.parseInt(databaseIdString);
                        }
                        JedisPoolConfig config = new JedisPoolConfig();
                        config.setMaxTotal(poolThreadSize);
                        pool = new JedisPool(config, ipAddress, port, Protocol.DEFAULT_TIMEOUT, password, databaseId, null);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new RedisConnectionException("Properties are not found in standalone " + ex.getLocalizedMessage());
            }
        }

        return pool;
    }

    public void release()
    {
        pool.destroy();
    }

    public static Jedis getJedis()
    {
        return pool.getResource();
    }

    public static void returnJedis(Jedis jedis)
    {
        try {
            pool.returnResource(jedis);
        } catch (Exception ex) {
            ex.printStackTrace();
            pool.returnBrokenResource(jedis);
        }
    }

}
