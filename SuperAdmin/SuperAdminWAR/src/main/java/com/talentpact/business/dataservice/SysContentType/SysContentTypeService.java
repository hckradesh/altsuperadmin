/**
 * 
 */
package com.talentpact.business.dataservice.SysContentType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.contentType.transport.input.ContentTypeTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.configuration.SysContentType;

/**
 * 
 * @author vivek.goyal
 *
 */
@Stateless
public class SysContentTypeService extends CommonService
{

    @EJB
    SysContentTypeDS sysContentTypeDS;

    public List<SysContentTypeTO> getSysContentTypeList()
        throws Exception
    {
        List<SysContentTypeTO> list = null;
        SysContentTypeTO sysContentTypeTO = null;
        List<Object[]> sysContentTypeList = null;

        try {
            sysContentTypeList = sysContentTypeDS.getSysContentTypeList();
            list = new ArrayList<SysContentTypeTO>();
            for (Object[] sysContentType : sysContentTypeList) {
                sysContentTypeTO = new SysContentTypeTO();
                sysContentTypeTO.setSysTypeID((Integer) sysContentType[0]);
                sysContentTypeTO.setSysType(sysContentType[1].toString());
                sysContentTypeTO.setContentCategoryID((Integer) sysContentType[2]);
                sysContentTypeTO.setCategoryName(sysContentType[3].toString());
                list.add(sysContentTypeTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SysContentTypeTO> getNewSysContentCategoryList()
        throws Exception
    {
        List<SysContentTypeTO> list = null;
        SysContentTypeTO sysContentTypeTO = null;
        List<Object[]> sysContentTypeList = null;
        try {
            sysContentTypeList = sysContentTypeDS.getNewSysContentCategoryList();
            list = new ArrayList<SysContentTypeTO>();
            for (Object[] sysContentType : sysContentTypeList) {
                sysContentTypeTO = new SysContentTypeTO();
                sysContentTypeTO.setContentCategoryID((Integer) sysContentType[0]);
                sysContentTypeTO.setCategoryName(sysContentType[1].toString());
                list.add(sysContentTypeTO);
            }
            return list;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addSysContentType(SysContentTypeTO sysContentTypeTO)
        throws Exception
    {
        try {
            sysContentTypeDS.addSysContentType(sysContentTypeTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveSysContentType(SysContentTypeTO sysContentTypeTO)
    {
        sysContentTypeDS.saveSysContentType(sysContentTypeTO);
    }

    /**
     * 
     * @param contentCategoryName
     * @return
     * @throws Exception
     */
    public List<ContentTypeTO> getSysContentTypeList(String contentCategoryName)
        throws Exception
    {
        List<ContentTypeTO> contentTypeTOList = null;
        ContentTypeTO contentTypeTO = null;
        List<Object[]> sysContentTypeList = null;

        try {
            sysContentTypeList = sysContentTypeDS.getSysTypeListByCategoryName(contentCategoryName);
            contentTypeTOList = new ArrayList<ContentTypeTO>();
            for (Object[] sysContentType : sysContentTypeList) {
                contentTypeTO = new ContentTypeTO();
                contentTypeTO.setSysTypeID((Integer) sysContentType[0]);
                contentTypeTO.setSysType(sysContentType[1].toString());
                contentTypeTOList.add(contentTypeTO);
            }
            return contentTypeTOList;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public ContentTypeTO findSysContentType(SysContentTypeTO sysContentTypeTO)
            throws Exception
        {
            
            SysContentType sysContentType = null;
            ContentTypeTO contentTypeTO = null;
            try {
            	sysContentType = sysContentTypeDS.findSysContentType(sysContentTypeTO);
                contentTypeTO = new ContentTypeTO();
                if (sysContentType != null) {
                	contentTypeTO.setSysTypeID(sysContentType.getSysTypeId());
                	contentTypeTO.setSysType(sysContentType.getSysType());
                }
                return contentTypeTO;
            } catch (Exception e) {
                throw e;
            }
        }
    
}