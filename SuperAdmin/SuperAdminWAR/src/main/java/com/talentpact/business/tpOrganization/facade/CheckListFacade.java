package com.talentpact.business.tpOrganization.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.UserResponseTO;
import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.tpOrganization.service.impl.OrganizationService;
import com.talentpact.remote.checkList.ICheckListFacadeRemote;

@Stateless
public class CheckListFacade extends CommonFacade implements ICheckListFacadeRemote
{
    @EJB
    OrganizationService organizationService;


    @Override
    public ServiceResponse getAllCheckListByOrganizationId(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {

            serviceResponse = organizationService.getAllCheckListByOrganizationId(serviceRequest);


        } catch (Exception ex) {
            throw new Exception();
        }

        return serviceResponse;
    }

    @Override
    public ServiceResponse getAllSysRoleType(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.getAllSysRoleType(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }


    @Override
    public ServiceResponse getAllSysProductAnnouncement(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.getAllSysProductAnnouncement(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }

    @Override
    public ServiceResponse getOrganizationModuleData(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.getOrganizationModuleData(serviceRequest);
        } catch (Exception e) {
            throw e;
        }
        return serviceResponse;
    }


    @Override
    public ServiceResponse getAllSysMenu(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;

        try {
            serviceResponse = organizationService.getAllSysMenu(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;


    }

    @Override
    public ServiceResponse updateAltCheckList(ServiceRequest serviceRequest)
        throws Exception
    {


        ServiceResponse serviceResponse = null;

        try {
            serviceResponse = organizationService.updateAltCheckList(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }

    @Override
    public ServiceResponse copySysRoleList(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.copySysRoleList(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;

    }


    @Override
    public ServiceResponse getAllSysContentType(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.getAllSysContentType(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }

    @Override
    public ServiceResponse copySysMenuList(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.copySysMenuList(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }

    @Override
    public ServiceResponse getUserByTenant(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        List<UserResponseTO> userList = null;
        try {
            userList = organizationService.getUserByTenant(serviceRequest);
            serviceResponse = new ServiceResponse();
            serviceResponse.setListResponseTransportObject((List) userList);
            return serviceResponse;
        } catch (Exception e) {
            throw e;
        }
    }


    @Override
    public ServiceResponse getAllUIForm(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.getAllUIForm(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;
    }


    @Override
    public ServiceResponse copyUIFormList(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.copyUIFormList(serviceRequest);
        } catch (Exception e) {
            throw new Exception();
        }
        return serviceResponse;

    }


    @Override
    public ServiceResponse setUserListByOrganization(ServiceRequest serviceRequest)
        throws Exception
    {
        return organizationService.setUserListByOrganization(serviceRequest);

    }

    @Override
    public ServiceResponse getAllOfferingTemplateList(ServiceRequest serviceRequest)
        throws Exception
    {
        return organizationService.getAllOfferingTemplateList(serviceRequest);
    }

    @Override
    public ServiceResponse copyAllOfferingTemplateList(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.copyAllOfferingTemplateList(serviceRequest);
        } catch (Exception e) {
            throw e;
        }
        return serviceResponse;
    }

    @Override
    public ServiceResponse copyHrProductAnnouncement(ServiceRequest serviceRequest)
        throws Exception
    {
        ServiceResponse serviceResponse = null;
        try {
            serviceResponse = organizationService.copyHrProductAnnouncement(serviceRequest);
        } catch (Exception e) {
            throw e;
        }
        return serviceResponse;
    }

    
    @Override
    public ServiceResponse copyAllDashboardChartList(ServiceRequest serviceRequest)
        throws Exception
    {
        return organizationService.copyAllDashboardChartList(serviceRequest);
    }
    
    @Override
    public ServiceResponse getAllDashboardChartList(ServiceRequest serviceRequest)
        throws Exception
    {
        return organizationService.getAllDashboardChartList(serviceRequest);
    }
    

}
