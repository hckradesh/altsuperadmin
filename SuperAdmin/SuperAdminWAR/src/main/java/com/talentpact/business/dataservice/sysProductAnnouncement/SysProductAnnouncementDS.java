package com.talentpact.business.dataservice.sysProductAnnouncement;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.alt.utility.CommonUtil;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.productAnnouncement.ProductAnnouncementDS;
import com.talentpact.business.model.AltCheckList;
import com.talentpact.model.organization.HrProductAnnouncement;
import com.talentpact.model.organization.SysProductAnnouncement;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysProductAnnouncement.to.SysProductAnnouncementTO;


/**
 * 
 * @author vivek.goyal
 *
 */

@Stateless
public class SysProductAnnouncementDS extends AbstractDS<SysProductAnnouncement>
{
    @Inject
    UserSessionBean userSessionBean;

    @Inject
    ProductAnnouncementDS productAnnouncementDS;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysProductAnnouncementDS.class);

    public SysProductAnnouncementDS()
    {
        super(SysProductAnnouncement.class);
    }

    public List<SysProductAnnouncement> getSysProductAnnouncementList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysProductAnnouncement sysProductAnnouncement ");

            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public List<Object[]> getOrganizationList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct case when hrProductAnnouncement.hrProductAnnouncementID is not null then 1 else 0 end, organizationID.organizationId, ");
            hqlQuery.append(" organizationID.organizationName from HrProductAnnouncement hrProductAnnouncement ");
            hqlQuery.append(" right outer join hrProductAnnouncement.organizationID as organizationID where organizationID.tenantOrg=0 order by organizationID.organizationName");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO, Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        SysProductAnnouncement sysProductAnnouncement = null;
        try {
            sysProductAnnouncement = new SysProductAnnouncement();

            sysProductAnnouncement.setSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncement());
            sysProductAnnouncement.setSysProductAnnouncementLabel(sysProductAnnouncementTO.getSysProductAnnouncementLabel());
            sysProductAnnouncement.setValidFrom(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidFrom())));
            sysProductAnnouncement.setValidTo(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidTo())));
            sysProductAnnouncement.setActive(sysProductAnnouncementTO.getActive());
            sysProductAnnouncement.setCreatedBy(userSessionBean.getUserID().intValue());
            sysProductAnnouncement.setCreatedDate(new Date());
            sysProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
            sysProductAnnouncement.setModifiedDate(new Date());
            sysProductAnnouncement.setSysProductAnnouncementBanner(sysProductAnnouncementTO.getAnnouncementBanner());
            sysProductAnnouncement.setSysProductAnnouncementBannerUrl(sysProductAnnouncementTO.getBannerUrl());
            getEntityManager("TalentPact").persist(sysProductAnnouncement);

            if (sysProductAnnouncementTO.getSelectedOrganizations() != null) {
                saveHrProductAnnouncement(sysProductAnnouncementTO, sysProductAnnouncement.getSysProductAnnouncementID(), organizationTOMap);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveHrProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO, int sysProductAnnouncementID, Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        HrProductAnnouncement hrProductAnnouncement = null;
        AltCheckList altCheckList = null;
        Date date = new Date();
        try {
            int[] orgCount = new int[sysProductAnnouncementTO.getSelectedOrganizations().length];
            int orgID = 0;
            for (int i = 0; i < orgCount.length; i++) {
                orgID = sysProductAnnouncementTO.getSelectedOrganizations()[i];
                hrProductAnnouncement = new HrProductAnnouncement();

                hrProductAnnouncement.setSysProductAnnouncement(sysProductAnnouncementID);
                hrProductAnnouncement.setValidFrom(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidFrom())));
                hrProductAnnouncement.setValidTo(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidTo())));
                hrProductAnnouncement.setHrOrganization(orgID);
                hrProductAnnouncement.setSysTenant(organizationTOMap.get(orgID).getTenantID());
                if (sysProductAnnouncementTO.getValidTo().compareTo(date) < 0) {
                    hrProductAnnouncement.setActive(false);
                } else {
                    hrProductAnnouncement.setActive(sysProductAnnouncementTO.getActive());
                }
                hrProductAnnouncement.setCreatedBy(userSessionBean.getUserID().intValue());
                hrProductAnnouncement.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                getEntityManager("TalentPact").persist(hrProductAnnouncement);

                copyAltCheckList((long) orgID, (long) organizationTOMap.get(orgID).getTenantID());
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void updateSysProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO)
    {
        SysProductAnnouncement sysProductAnnouncement = null;
        try {
            sysProductAnnouncement = new SysProductAnnouncement();
            sysProductAnnouncement = findSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID());

            if (sysProductAnnouncement.getSysProductAnnouncement().trim().equalsIgnoreCase(sysProductAnnouncementTO.getSysProductAnnouncement().trim())
                    && sysProductAnnouncement.getSysProductAnnouncementLabel().trim().equalsIgnoreCase(sysProductAnnouncementTO.getSysProductAnnouncementLabel().trim())
                    && sysProductAnnouncement.getValidFrom().equals(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidFrom())))
                    && sysProductAnnouncement.getValidTo().equals(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidTo())))
                    && sysProductAnnouncement.isActive() == sysProductAnnouncementTO.getActive()&& sysProductAnnouncement.getSysProductAnnouncementBannerUrl() == sysProductAnnouncementTO.getBannerUrl() && sysProductAnnouncement.getSysProductAnnouncementBanner() == sysProductAnnouncementTO.getAnnouncementBanner()) {
                sysProductAnnouncement.setSysProductAnnouncementBanner(sysProductAnnouncementTO.getAnnouncementBanner());
                sysProductAnnouncement.setSysProductAnnouncementBannerUrl(sysProductAnnouncementTO.getBannerUrl());
                getEntityManager("TalentPact").merge(sysProductAnnouncement);
                getEntityManager("TalentPact").flush();
            } else {

                sysProductAnnouncement.setSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncement());
                sysProductAnnouncement.setSysProductAnnouncementLabel(sysProductAnnouncementTO.getSysProductAnnouncementLabel());
                sysProductAnnouncement.setValidFrom(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidFrom())));
                sysProductAnnouncement.setValidTo(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidTo())));
                sysProductAnnouncement.setActive(sysProductAnnouncementTO.getActive());
                sysProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                sysProductAnnouncement.setModifiedDate(new Date());
                sysProductAnnouncement.setSysProductAnnouncementBanner(sysProductAnnouncementTO.getAnnouncementBanner());
                sysProductAnnouncement.setSysProductAnnouncementBannerUrl(sysProductAnnouncementTO.getBannerUrl());
                getEntityManager("TalentPact").merge(sysProductAnnouncement);
                getEntityManager("TalentPact").flush();
            }
            if (sysProductAnnouncementTO.getSelectedOrganizations() != null) {
                updateHrProductAnnouncement(sysProductAnnouncementTO);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void updateHrProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO)
    {
        HrProductAnnouncement hrProductAnnouncement = null;
        Map<Integer, SysProductAnnouncementTO> organizationTOMap = new HashMap<Integer, SysProductAnnouncementTO>();
        int[] orgCount = new int[sysProductAnnouncementTO.getSelectedOrganizations().length];
        try {
            organizationTOMap = sysProductAnnouncementTO.getOrganizationTOMap();
            int orgID = 0;
            for (int i = 0; i < orgCount.length; i++) {
                orgID = sysProductAnnouncementTO.getSelectedOrganizations()[i];
                for (Entry<Integer, SysProductAnnouncementTO> mapVal : organizationTOMap.entrySet()) {
                    SysProductAnnouncementTO sysProductAnnouncementTO1 = mapVal.getValue();

                    //update values for existing organizations
                    if (orgID == sysProductAnnouncementTO1.getOrganizationID().intValue() && sysProductAnnouncementTO1.getOrganizationAnnouncementActive()) {
                        hrProductAnnouncement = new HrProductAnnouncement();
                        hrProductAnnouncement = productAnnouncementDS.findHrProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID(), orgID);

                        if (hrProductAnnouncement != null) {
                            if (hrProductAnnouncement.getSysProductAnnouncement().equals(sysProductAnnouncementTO.getSysProductAnnouncementID())
                                    && hrProductAnnouncement.getActive() == sysProductAnnouncementTO.getActive()) {
                                //do nothing, no change is made in HrProductAnnouncement
                            } else {
                                hrProductAnnouncement.setActive(sysProductAnnouncementTO.getActive());
                                hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                                hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                                getEntityManager("TalentPact").merge(hrProductAnnouncement);
                                getEntityManager("TalentPact").flush();
                            }
                        }
                    }

                    //add values for new organizations
                    else if (orgID == sysProductAnnouncementTO1.getOrganizationID().intValue() && !sysProductAnnouncementTO1.getOrganizationAnnouncementActive()) {
                        hrProductAnnouncement = new HrProductAnnouncement();

                        hrProductAnnouncement.setSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID());
                        hrProductAnnouncement.setValidFrom(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidFrom())));
                        hrProductAnnouncement.setValidTo(CommonUtil.getTimeDimensionId((sysProductAnnouncementTO.getValidTo())));
                        hrProductAnnouncement.setHrOrganization(orgID);
                        hrProductAnnouncement.setSysTenant(organizationTOMap.get(orgID).getTenantID());
                        hrProductAnnouncement.setActive(sysProductAnnouncementTO.getActive());
                        hrProductAnnouncement.setCreatedBy(userSessionBean.getUserID().intValue());
                        hrProductAnnouncement.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                        hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                        hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                        getEntityManager("TalentPact").persist(hrProductAnnouncement);

                        copyAltCheckList((long) orgID, (long) organizationTOMap.get(orgID).getTenantID());
                    }
                }
            }

            //deactivate values for removed organizations
            if (sysProductAnnouncementTO.getDeletedOrganizations() != null) {
                int[] deletedOrganizations = new int[] { sysProductAnnouncementTO.getDeletedOrganizations().length };
                deletedOrganizations = sysProductAnnouncementTO.getDeletedOrganizations();

                for (int i = 0; i < deletedOrganizations.length; i++) {
                    hrProductAnnouncement = new HrProductAnnouncement();
                    hrProductAnnouncement = productAnnouncementDS.findHrProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncementID(), deletedOrganizations[i]);
                    if (hrProductAnnouncement != null) {
                        hrProductAnnouncement.setActive(false);
                        hrProductAnnouncement.setModifiedBy(userSessionBean.getUserID().intValue());
                        hrProductAnnouncement.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                        getEntityManager("TalentPact").merge(hrProductAnnouncement);
                        getEntityManager("TalentPact").flush();
                    }
                }
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    private SysProductAnnouncement findSysProductAnnouncement(int sysProductAnnouncementID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysProductAnnouncement> list = null;
        SysProductAnnouncement sysProductAnnouncement = null;
        try {
            sysProductAnnouncement = new SysProductAnnouncement();
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysProductAnnouncement sysProductAnnouncement where sysProductAnnouncement.sysProductAnnouncementID=:sysProductAnnouncementID");

            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysProductAnnouncementID", sysProductAnnouncementID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
                sysProductAnnouncement = list.get(0);
            }
            return sysProductAnnouncement;
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
    }

    public List<HrProductAnnouncement> getAllHrProductAnnouncement(Integer sysProductAnnouncementID)
        throws Exception
    {
        StringBuilder queryStr = null;
        Query query = null;
        try {
            queryStr = new StringBuilder();
            queryStr.append(" from HrProductAnnouncement hrProductAnnouncement where hrProductAnnouncement.active=1");
            queryStr.append(" and CONVERT(date, getdate()) between  hrProductAnnouncement.validFromID.theDate and hrProductAnnouncement.validToID.theDate ");
            queryStr.append(" and hrProductAnnouncement.sysProductAnnouncement =:sysProductAnnouncementID");
            query = getEntityManager("TalentPact").createQuery(queryStr.toString());
            query.setParameter("sysProductAnnouncementID", sysProductAnnouncementID);
            return query.getResultList();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw ex;
        } finally {
            queryStr = null;
            query = null;
        }
    }

    //saving checklist in altCheckList table    
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    private void copyAltCheckList(long orgID, long tenantID)
        throws Exception
    {
        AltCheckList altCheckList = null;

        try {
            altCheckList = productAnnouncementDS.findAltCheckList(10, orgID);

            if (altCheckList == null) {
                altCheckList = new AltCheckList();
                altCheckList.setSysCheckListID(10);
                altCheckList.setOrganizationID(orgID);
                altCheckList.setTenantID(tenantID);
                altCheckList.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                altCheckList.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                altCheckList.setIsCompleted(true);
                altCheckList.setCreatedBy(userSessionBean.getUserID());
                altCheckList.setModifiedBy(userSessionBean.getUserID());
                getEntityManager("AltCommon").persist(altCheckList);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

}