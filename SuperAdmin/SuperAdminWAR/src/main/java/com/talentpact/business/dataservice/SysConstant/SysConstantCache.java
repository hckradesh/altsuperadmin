/**
 * 
 */
package com.talentpact.business.dataservice.SysConstant;

import java.util.List;

import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
public class SysConstantCache
{
    public List<SysConstantResponseTO> getSysConstantTypeList()
        throws Exception
    {
        String key = "SUPERADMIN:SYSCONSTANT";
        return RedisClient.getValueAsList(key, SysConstantResponseTO.class);
    }

    public void persistSysConstantListByTenant(List<SysConstantResponseTO> constantList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANT";
        RedisClient.putValueAsTO(key, constantList);

    }

    public void removeSysConstantListByTenant()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANT";
        RedisClient.delete(key);
    }


    public List<SysConstantResponseTO> getSysConstantCategoryListDropDown()
        throws Exception
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORYDROPDOWN";
        return RedisClient.getValueAsList(key, SysConstantResponseTO.class);
    }

    public void persistSysConstantCategoryListDropDown(List<SysConstantResponseTO> constantList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORYDROPDOWN";
        RedisClient.putValueAsTO(key, constantList);

    }

    public void removeSysConstantCategoryListDropDown()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSCONSTANTCATEGORYDROPDOWN";
        RedisClient.delete(key);
    }


}
