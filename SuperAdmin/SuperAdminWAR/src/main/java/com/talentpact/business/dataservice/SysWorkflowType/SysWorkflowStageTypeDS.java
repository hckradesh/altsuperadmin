package com.talentpact.business.dataservice.SysWorkflowType;

import javax.ejb.Stateless;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysWorkflowStageType_Revamp;

@Stateless
public class SysWorkflowStageTypeDS extends AbstractDS<SysWorkflowStageType_Revamp> {

    public SysWorkflowStageTypeDS() {
        super(SysWorkflowStageType_Revamp.class);
    }

}
