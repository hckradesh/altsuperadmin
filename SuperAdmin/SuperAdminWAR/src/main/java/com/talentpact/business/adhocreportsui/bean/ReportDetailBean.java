package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.adhocreports.business.common.controller.ReportColumnCss;
import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.to.FilterGroupTo;
import com.talentpact.business.adhocreportsui.controller.ReportDisplayController;

@SessionScoped
@Named("reportDetailBean")
public class ReportDetailBean implements Serializable {

	private static final long serialVersionUID = -3508341932473255767L;

	@Inject
	private ReportDisplayController reportDisplayController;

	private Map<String,List<ReportDetail>> reports;

	private List<ReportDetail> reportDetailList;
	
	private List<ReportDetail> filteredReportDetailList;

	private List<ReportDetail> activeInactiveReportDetailList;

	private List<String> reportNameList;

	private ReportDetail selectedReport;

	private ReportDetail selectedReportForDeletion;

	private List<FilterGroupTo> selectReportFilters;

	private Map<String, String> applicationInputMap;

	private String selectedReportQuery;

	private List<Object[]> reportData;

	private List<String> reportHeaders;

	private List<ReportColumnCss> cssDetails;

	private List<SelectItem> modules;

	private List<String> moduleNameList;

	private SelectItem selectedModule;

	private String reportNameForCreation;

	private String selectedReportType;

	private String reportDescriptionForCreation;

	private String defaultFlag;

	private Integer orgID;

	@PostConstruct
    public void initialize() {
		reportDisplayController.initialize(this);
    }

	public Map<String, List<ReportDetail>> getReports() {
		return reports;
	}

	public void setReports(Map<String, List<ReportDetail>> reports) {
		this.reports = reports;
	}

	public ReportDetail getSelectedReport() {
		return selectedReport;
	}

	public void setSelectedReport(ReportDetail selectedReport) {
		this.selectedReport = selectedReport;
	}

	public List<FilterGroupTo> getSelectReportFilters() {
		return selectReportFilters;
	}

	public void setSelectReportFilters(List<FilterGroupTo> selectReportFilters) {
		this.selectReportFilters = selectReportFilters;
	}

	public Map<String, String> getApplicationInputMap() {
		return applicationInputMap;
	}

	public void setApplicationInputMap(Map<String, String> applicationInputMap) {
		this.applicationInputMap = applicationInputMap;
	}

	public String getSelectedReportQuery() {
		return selectedReportQuery;
	}

	public void setSelectedReportQuery(String selectedReportQuery) {
		this.selectedReportQuery = selectedReportQuery;
	}

	public List<Object[]> getReportData() {
		return reportData;
	}

	public void setReportData(List<Object[]> reportData) {
		this.reportData = reportData;
	}

	public List<String> getReportHeaders() {
		return reportHeaders;
	}

	public void setReportHeaders(List<String> reportHeaders) {
		this.reportHeaders = reportHeaders;
	}

	public List<SelectItem> getModules() {
		return modules;
	}

	public void setModules(List<SelectItem> modules) {
		this.modules = modules;
	}

	public SelectItem getSelectedModule() {
		return selectedModule;
	}

	public void setSelectedModule(SelectItem selectedModule) {
		this.selectedModule = selectedModule;
	}

	public String getReportNameForCreation() {
		return reportNameForCreation;
	}

	public void setReportNameForCreation(String reportNameForCreation) {
		this.reportNameForCreation = reportNameForCreation;
	}

	public String getSelectedReportType() {
		return selectedReportType;
	}

	public void setSelectedReportType(String selectedReportType) {
		this.selectedReportType = selectedReportType;
	}

	public String getReportDescriptionForCreation() {
		return reportDescriptionForCreation;
	}

	public void setReportDescriptionForCreation(String reportDescriptionForCreation) {
		this.reportDescriptionForCreation = reportDescriptionForCreation;
	}

	public List<ReportDetail> getReportDetailList() {
		return reportDetailList;
	}

	public void setReportDetailList(List<ReportDetail> reportDetailList) {
		this.reportDetailList = reportDetailList;
	}

	public String getDefaultFlag() {
		return defaultFlag;
	}

	public void setDefaultFlag(String defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	public Integer getOrgID() {
		return orgID;
	}

	public void setOrgID(Integer orgID) {
		this.orgID = orgID;
	}

	public List<String> getReportNameList() {
		return reportNameList;
	}

	public void setReportNameList(List<String> reportNameList) {
		this.reportNameList = reportNameList;
	}

	public List<String> getModuleNameList() {
		return moduleNameList;
	}

	public void setModuleNameList(List<String> moduleNameList) {
		this.moduleNameList = moduleNameList;
	}

	public List<ReportDetail> getActiveInactiveReportDetailList() {
		return activeInactiveReportDetailList;
	}

	public void setActiveInactiveReportDetailList(
			List<ReportDetail> activeInactiveReportDetailList) {
		this.activeInactiveReportDetailList = activeInactiveReportDetailList;
	}

	public List<ReportColumnCss> getCssDetails() {
		return cssDetails;
	}

	public void setCssDetails(List<ReportColumnCss> cssDetails) {
		this.cssDetails = cssDetails;
	}

	public ReportDetail getSelectedReportForDeletion() {
		return selectedReportForDeletion;
	}

	public void setSelectedReportForDeletion(ReportDetail selectedReportForDeletion) {
		this.selectedReportForDeletion = selectedReportForDeletion;
	}

	public List<ReportDetail> getFilteredReportDetailList() {
		return filteredReportDetailList;
	}

	public void setFilteredReportDetailList(List<ReportDetail> filteredReportDetailList) {
		this.filteredReportDetailList = filteredReportDetailList;
	}

}
