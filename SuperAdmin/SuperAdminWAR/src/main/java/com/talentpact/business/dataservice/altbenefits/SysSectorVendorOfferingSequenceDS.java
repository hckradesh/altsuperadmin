/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.HrSectorVendorOfferingSequence;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysSectorVendorOfferingSequenceDS extends AbstractDS<HrSectorVendorOfferingSequence>
{
    public SysSectorVendorOfferingSequenceDS()
    {
        super(HrSectorVendorOfferingSequence.class);
    }



    @SuppressWarnings("unchecked")
    public List<HrSectorVendorOfferingSequence> getBenefitsSectorBasedList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrSectorVendorOfferingSequence> hrSectorVendorOfferingSequencesList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrSectorVendorOfferingSequence s  where s.sysVendorOfferingTemplate.active=1 order by s.sequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            hrSectorVendorOfferingSequencesList = query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
        return hrSectorVendorOfferingSequencesList;
    }

    
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSectorVendorOfferSequence(List<Integer> sequenceList,BenefitRequestTO benefitRequestTO)
    {
        Long vendorOfferingId = null;
        HrSectorVendorOfferingSequence offeringSequence = null;
        Integer[] selectedSectors = null;
        Map<Integer, Long> sectorVendorOfferSequenceMap = null;
        try {
            selectedSectors = benefitRequestTO.getSelectedSectors();
            sectorVendorOfferSequenceMap = benefitRequestTO.getSectorVendorOfferSequenceMap();
           if(!benefitRequestTO.isEdit()){
            for (Integer sectorId : selectedSectors) {
                for (int i = 0; i < sectorVendorOfferSequenceMap.size(); i++) {
                    vendorOfferingId = sectorVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence = new HrSectorVendorOfferingSequence();
                    offeringSequence.setSectorID(sectorId);
                    offeringSequence.setVendorOfferingID(vendorOfferingId);
                    offeringSequence.setSequence(i+1);
                    getEntityManager("Talentpact").persist(offeringSequence);
                }
            }
            }else{
                for (int i = 0; i < sectorVendorOfferSequenceMap.size(); i++) {
                    vendorOfferingId = sectorVendorOfferSequenceMap.get(sequenceList.get(i));
                    offeringSequence=findBenefitsSectorBasedByOfferingId(vendorOfferingId, benefitRequestTO.getSectorId());
                    if(offeringSequence!=null){
                        offeringSequence.setSequence(i+1);
                        getEntityManager("Talentpact").merge(offeringSequence);     
                    }else{
                        offeringSequence = new HrSectorVendorOfferingSequence();
                        offeringSequence.setSectorID(benefitRequestTO.getSectorId());
                        offeringSequence.setVendorOfferingID(vendorOfferingId);
                        offeringSequence.setSequence(sequenceList.get(i));
                        getEntityManager("Talentpact").persist(offeringSequence);     
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public HrSectorVendorOfferingSequence findBenefitsSectorBasedByOfferingId(Long vendorOfferingId,Integer sectorId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrSectorVendorOfferingSequence> hrSectorVendorOfferingSequencesList = null;
        HrSectorVendorOfferingSequence sectorVendorOfferingSequence=null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  HrSectorVendorOfferingSequence s  where s.vendorOfferingID=:vendorOfferingId and s.sectorID=:sectorId order by s.sequence ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorOfferingId", vendorOfferingId);
            query.setParameter("sectorId", sectorId);
            hrSectorVendorOfferingSequencesList = query.getResultList();
            if(hrSectorVendorOfferingSequencesList!=null&&!hrSectorVendorOfferingSequencesList.isEmpty()){
                sectorVendorOfferingSequence= hrSectorVendorOfferingSequencesList.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
        return sectorVendorOfferingSequence;
    }

}