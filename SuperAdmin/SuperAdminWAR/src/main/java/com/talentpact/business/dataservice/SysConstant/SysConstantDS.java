/**
 * 
 */
package com.talentpact.business.dataservice.SysConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.SysConstantRequestTO;
import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysConstant;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysConstantDS extends AbstractDS<SysConstant>
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantDS.class);

    public SysConstantDS()
    {
        super(SysConstant.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysConstantResponseTO> getSysConstantTypeList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysConstantResponseTO> sysConstantTOList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sct.sysConstantId, sct.sysConstantName, sysConstantCategory.sysConstantCategoryID, sysConstantCategory.sysConstantCategoryName,sct.status from SysConstant sct "
                    + "order by  sct.createdDate desc");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                sysConstantTOList = getSysConstantListRevised(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sysConstantTOList;
    }

    @SuppressWarnings("unchecked")
    public List<SysConstantResponseTO> getSysConstantCategoryListDropDown()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysConstantResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct scc.sysConstantCategoryID, scc.sysConstantCategoryName from SysConstantCategory scc order by scc.sysConstantCategoryName");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getCategoryListDropDown(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysConstant(SysConstantRequestTO sysConstantRequestTO)
    {
        SysConstant sysConstant = null;
        try {
            sysConstant = findSysConstant(sysConstantRequestTO);

            if (sysConstant == null) {
                sysConstant = new SysConstant();

                sysConstant.setSysConstantName(sysConstantRequestTO.getSysConstantName());
                sysConstant.setSysConstantCategoryId(sysConstantRequestTO.getSysConstantCategoryID());
                sysConstant.setCreatedBy(1);
                sysConstant.setModifiedBy(1);
                sysConstant.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                sysConstant.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                sysConstant.setStatus(sysConstantRequestTO.getStatus());

                getEntityManager("AltCommon").persist(sysConstant);
            } else {
                sysConstant.setSysConstantName(sysConstantRequestTO.getSysConstantName());
                sysConstant.setSysConstantCategoryId(sysConstantRequestTO.getSysConstantCategoryID());
                sysConstant.setSysConstantId(sysConstantRequestTO.getSysConstantID());
                sysConstant.setModifiedBy(1);
                sysConstant.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                sysConstant.setStatus(sysConstantRequestTO.getStatus());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private SysConstant findSysConstant(SysConstantRequestTO sysConstantRequestTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysConstant> sysConstantList = null;
        SysConstant result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sct from SysConstant sct  where sct.sysConstantId=:sysConstantId ");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("sysConstantId", sysConstantRequestTO.getSysConstantID());
            sysConstantList = query.getResultList();
            if (sysConstantList != null && sysConstantList.size() > 0) {
                result = sysConstantList.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }


    public static List<SysConstantResponseTO> getSysConstantListRevised(List<Object[]> resultList)
        throws Exception
    {
        List<SysConstantResponseTO> constantList = null;
        SysConstantResponseTO responseTO = null;
        Boolean status = null;
        try {
            constantList = new ArrayList<SysConstantResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new SysConstantResponseTO();

                if (object[0] != null) {

                    responseTO.setSysConstantID((Integer) object[0]);
                }
                if (object[1] != null) {

                    responseTO.setSysConstantName((String) object[1]);
                }


                if (object[2] != null) {

                    responseTO.setSysConstantCategoryID((Integer) object[2]);
                }

                if (object[3] != null) {

                    responseTO.setSysConstantCategoryName((String) object[3]);
                }
                if (object[4] != null) {
                    status = (Boolean) object[4];
                    if (status == true) {
                        responseTO.setStatusType("Active");
                    } else {
                        responseTO.setStatusType("InActive");
                    }
                }
                responseTO.setStatus(status);


                constantList.add(responseTO);
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            responseTO = null;
        }
        return constantList;

    }

    public static List<SysConstantResponseTO> getCategoryListDropDown(List<Object[]> resultList)
    {
        List<SysConstantResponseTO> list = null;
        SysConstantResponseTO responseTO = null;
        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysConstantResponseTO();
                if (object[0] != null) {
                    responseTO.setSysConstantCategoryID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysConstantCategoryName((String) object[1]);
                }


                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

    public List<String> getThemeListFromTpURL()
        throws Exception
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<String> list = null;
        Set<String> set = new HashSet<>();
        String th = "";
        try {
            list = new ArrayList<String>();
            hqlQuery = new StringBuilder();
            hqlQuery.append("select t.loginPage,t.homePage,t.loginPageTheme,t.defaultTheme  from  TpURL t  ");
            query = getEntityManager().createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                list = getThemeConverter(resultList);
                set.addAll(list);
                resultList.clear();
                list.addAll(set);
                list.removeAll(Collections.singleton(null));
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
        return list;

    }

    public static List<String> getThemeConverter(List<Object[]> resultList)
    {
        List<String> list = null;
        SysConstantResponseTO responseTO = null;
        try {
            list = new ArrayList<String>();
            for (Object[] object : resultList) {
                list.add((String) object[0]);
                list.add((String) object[1]);
                list.add((String) object[2]);
                list.add((String) object[3]);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

}
