package com.talentpact.business.authorization.converter;

import javax.ejb.Stateless;

import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.common.converter.impl.CommonConerter;
import com.talentpact.ui.login.transport.input.AuthUserTO;



@Stateless
public class AuthorizationConverter extends CommonConerter
{

    public AuthUserTO convertFromTPUser(TpUser tpUSer)
    {
        AuthUserTO tpUserTO = null;
        try {

            tpUserTO = new AuthUserTO();
            tpUserTO.setUserID(tpUSer.getUserID());
            tpUserTO.setUsername(tpUSer.getUsername());
            tpUserTO.setPassword(tpUSer.getPassword());
            tpUserTO.setOrganizationName(tpUSer.getOrganizationID().getName());
            tpUserTO.setOrganizationId(tpUSer.getOrganizationID().getOrganizationID());
            tpUserTO.setCreatedDate(tpUSer.getCreatedDate());
            tpUserTO.setModifiedDate(tpUSer.getModifiedDate());
            //            tpUserTO.setCreatedBy(tpUSer.getCreatedBy().get);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpUserTO;
    }


}
