package com.talentpact.business.dataservice.tpApp;

public interface TpAppConstants {

	public static final Integer TP_APP_TYPE_PARTNER = 3;
	
	public static final Integer TP_APP_TYPE_CUSTOM = 2;
}
