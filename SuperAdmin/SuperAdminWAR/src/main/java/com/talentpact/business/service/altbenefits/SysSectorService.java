/**
 * 
 */
package com.talentpact.business.service.altbenefits;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.alt.altbenefits.to.SysSectorTO;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.dataservice.altbenefits.SysSectorDS;


/**
 * 
 * @author javed.ali
 *
 */
@Stateless
public class SysSectorService extends CommonService
{
    @EJB
    SysSectorDS sysSectorDS;

    public List<SysSectorTO> getSysSectorList()
        throws Exception
    {
        List<Object[]> sysSectorObjectList = null;
        List<SysSectorTO> sysSectorTOList = null;
        SysSectorTO sectorTO = null;
        try {
            sysSectorTOList = new ArrayList<SysSectorTO>();
            sysSectorObjectList = sysSectorDS.getSysSectorList();
            if (sysSectorObjectList != null && !sysSectorObjectList.isEmpty()) {
                for (Object[] ob : sysSectorObjectList) {
                    sectorTO = new SysSectorTO();
                    sectorTO.setSectorID((Integer) ob[0]);
                    sectorTO.setSectorLabel((String) ob[1]);
                    sectorTO.setSectorCode((String) ob[2]);
                    sysSectorTOList.add(sectorTO);
                }
            }
            return sysSectorTOList;
        } catch (Exception e) {
            throw e;
        }
    }

}
