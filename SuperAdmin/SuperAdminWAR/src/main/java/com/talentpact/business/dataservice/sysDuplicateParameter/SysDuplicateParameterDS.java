package com.talentpact.business.dataservice.sysDuplicateParameter;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.duplicate.SysDuplicateParameter;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysDuplicateParameter.to.SysDuplicateParameterTO;

/**
 * @author prachi.bansal
 * 
 */

@Stateless
public class SysDuplicateParameterDS extends AbstractDS<SysDuplicateParameter>{
    
    @Inject
    UserSessionBean userSessionBean;
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysDuplicateParameterDS.class);

    public SysDuplicateParameterDS() {
        super(SysDuplicateParameter.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysDuplicateParameter> getSysDuplicateParameterList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDuplicateParameter sdp");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            return query.getResultList();
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            return null;
        }
        
    }

    public void saveSysDuplicateParameter(SysDuplicateParameterTO sysDuplicateParameterTO) {
        SysDuplicateParameter sysDuplicateParameter=null;
        try{
            sysDuplicateParameter=new SysDuplicateParameter();
            sysDuplicateParameter.setSysDuplicateParameter(sysDuplicateParameterTO.getSysDuplicateParameterName());
            sysDuplicateParameter.setCreatedBy(userSessionBean.getUserID().intValue());
            sysDuplicateParameter.setCreatedDate(new Date());
            sysDuplicateParameter.setModifiedBy(userSessionBean.getUserID().intValue());
            sysDuplicateParameter.setModifiedDate(new Date());
            getEntityManager("TalentPact").persist(sysDuplicateParameter);
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<SysDuplicateParameter> getUpdateSysDuplicateParameters(Integer sysDuplicateParameterID){
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDuplicateParameter sdp where sdp.sysDuplicateParameterID!=:sysDuplicateParameterID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysDuplicateParameterID", sysDuplicateParameterID);
            return query.getResultList();
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
    }
    
    public void updateSysDuplicateParameter(SysDuplicateParameterTO sysDuplicateParameterTO){
        try{
            SysDuplicateParameter sysDuplicateParameter=findSysDuplicateParameter(sysDuplicateParameterTO.getSysDuplicateParameterID());
            sysDuplicateParameter.setSysDuplicateParameter(sysDuplicateParameterTO.getSysDuplicateParameterName());
            sysDuplicateParameter.setModifiedBy(userSessionBean.getUserID().intValue());
            sysDuplicateParameter.setModifiedDate(new Date());
            getEntityManager("TalentPact").merge(sysDuplicateParameter);
            getEntityManager("TalentPact").flush();
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    private SysDuplicateParameter findSysDuplicateParameter(Integer sysDuplicateParameterID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysDuplicateParameter> list=null;
        SysDuplicateParameter sysDuplicateParameter=null;
        try{
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysDuplicateParameter sysDuplicateParameter where sysDuplicateParameter.sysDuplicateParameterID=:sysDuplicateParameterID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("sysDuplicateParameterID", sysDuplicateParameterID);
            list = query.getResultList();
            if (list != null && !list.isEmpty()) {
                sysDuplicateParameter = list.get(0);
            }
            return sysDuplicateParameter;
        }catch (Exception e) {
            LOGGER_.error("", e);
            return null;
        }
        
    }
    
    
    
}