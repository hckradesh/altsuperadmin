package com.talentpact.business.sysEntity.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.sysEntity.service.impl.SysEntityService;
import com.talentpact.remote.sysEntity.ISysEntityRemote;
import com.talentpact.ui.sysEntity.Exception.SysEntityException;
import com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO;
import com.talentpact.ui.sysEntity.constants.ISysEntityConstants;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.dataservice.SysEntityColumnDataService;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
@Local(ISysEntityRemote.class)
public class SysEntityFacade extends CommonFacade implements ISysEntityRemote {

	private static final Logger LOGGER = Logger.getLogger(SysEntityFacade.class.toString());
	
	@EJB
	SysEntityService sysEntityService;
	
	@EJB
	SysEntityColumnDataService sysEntityColumnService;
	
	
	
	@Override
	public ServiceResponse getAllEntities() throws Exception {
		
		ServiceResponse serviceResponse = null;
		List<SysEntityResponseTO> sysEntityResponseTOList = null;
		List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
		List<Integer> sysEntityIDList = null;
		try{
			sysEntityResponseTOList = sysEntityService.getAllEntities();
			if(sysEntityResponseTOList!=null && 
					!sysEntityResponseTOList.isEmpty()){
				sysEntityIDList = new ArrayList<Integer>();
				/**
				 * prepare entity ID list
				 * */
				for(SysEntityResponseTO entityResponseTO : sysEntityResponseTOList){
					sysEntityIDList.add(entityResponseTO.getEntityId());
				}
				
				/**
				 * based on entity fetched, fetch entity column
				 * */
				sysEntityColumnResponseTOList = sysEntityColumnService.getSysEntityColumnsByEntityID(
						sysEntityIDList);
				if(sysEntityColumnResponseTOList!=null && 
						!sysEntityColumnResponseTOList.isEmpty()){
					serviceResponse = new ServiceResponse();
					serviceResponse.setResponseTransferObjectObjectList(sysEntityColumnResponseTOList);
				}
			}
			
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	
	}
	
	@Override
	public ServiceResponse updateModules(ServiceRequest serviceRequest) throws SysEntityException
	    {
	        ServiceResponse response = null;
	        try {

	            return response;
	        } catch (Exception e) {
	            throw new SysEntityException();
	        }
	    }

	@Override
	public ServiceResponse saveOrUpdateSysEntity(ServiceRequest serviceRequest) throws SysEntityException {
		ServiceResponse response = null;
		response = sysEntityService.saveOrUpdateSysEntity(serviceRequest);
		return response;
	}

	@Override
	public ServiceResponse getAllSysEntityList(ServiceRequest serviceRequest)
			throws SysEntityException {
		ServiceResponse response = null;
		response = sysEntityService.getSysEntityList(serviceRequest);
		return response;
	}
	
	
	public ServiceResponse getAllSysEntityColumnListBySysEntityIDList(ServiceRequest serviceRequest)
			throws Exception {
		ServiceResponse response = null;
		List<Integer> sysEntityIDList = null;
		List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
		response = sysEntityService.getSysEntityList(serviceRequest);
		/**
		 * based on entity fetched, fetch entity column
		 * */
		sysEntityColumnResponseTOList = sysEntityColumnService.getSysEntityColumnsByEntityID(
				sysEntityIDList);
		return response;
	}

	@Override
	public List<String> getAllTablesFromInformationSchema() {
		List<String> tableNamesNotPresentInSysEntity = null;
		tableNamesNotPresentInSysEntity = sysEntityService.getAllTablesFromInformationSchema();
		return tableNamesNotPresentInSysEntity;
	}
	
	@Override
	public List<Object[]> getColumnNamesAndDataTypeOfAllColumnsOfATable(String tableName) throws SysEntityException{
		List<Object[]> columnNamesAndDataTypes = null;
		try{
			columnNamesAndDataTypes = sysEntityService.getColumnNamesAndDataTypeOfAllColumnsOfATable(tableName);
		}catch (Exception ex) {
			LOGGER.error("", ex);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_COLUMN_NAME_DATA_TYPE_LIST_ERROR_MSG, ex);
		}
		return columnNamesAndDataTypes;
	}

	@Override
	public List<String> getTablesPrimaryKeyColumnNames(String tableName) throws SysEntityException {
		List<String> primaryKeyColumnNameList = null;
		try{
		primaryKeyColumnNameList = sysEntityService.getTablesPrimaryKeyColumnNames(tableName);
		}catch (Exception ex) {
			LOGGER.error("", ex);
			throw new SysEntityException(ISysEntityConstants.SYSENTITY_TABLES_PRIMARY_COLUMN_NAMES_RETRIEVE_ERROR_MSG, ex);
		}
		return primaryKeyColumnNameList;
	}
	
	@Override
	public ServiceResponse updatePrimaryKeyColumnDetailsInSysEntity(ServiceRequest serviceRequest) throws SysEntityException {
		ServiceResponse response = null;
		response = sysEntityService.updatePrimaryKeyColumnDetailsInSysEntity(serviceRequest);
		return response;
	}

}
