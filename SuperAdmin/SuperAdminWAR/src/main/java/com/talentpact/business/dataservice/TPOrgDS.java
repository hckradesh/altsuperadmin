/**
 * 
 */
package com.talentpact.business.dataservice;


import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;

import com.alt.security.transport.output.UiFormTO;
import com.peoplestrong.admin.common.constants.CoreConstants;
import com.talentpact.auth.entity.TpApp;
import com.talentpact.auth.entity.TpDatabaseMetadata;
import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpPortalType;
import com.talentpact.auth.entity.TpTenant;
import com.talentpact.auth.entity.TpURL;
import com.talentpact.business.application.loginserver.LoginServerDetailTO;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.application.transport.input.AppRequestTO;
import com.talentpact.business.application.transport.input.CopyCheckListRequestTO;
import com.talentpact.business.application.transport.input.HrContentRequestTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.input.SysRoleRequestTO;
import com.talentpact.business.application.transport.input.UIFormRequestTO;
import com.talentpact.business.application.transport.output.AppResponseTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.application.transport.output.UIFormDetailsTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.cache.cacheprovider.RedisCacheKeyConstant;
import com.talentpact.business.common.cache.CacheUtil;
import com.talentpact.business.dataservice.loginserver.LoginServerDetailDS;
import com.talentpact.business.dataservice.loginserver.SocialAppSettingDS;
import com.talentpact.business.dataservice.loginserver.cache.LoginServerRedisCache;
import com.talentpact.business.model.AltCheckList;
import com.talentpact.business.model.HrContent;
import com.talentpact.business.model.HrModuleUrlLink;
import com.talentpact.business.startup.StartupService;
import com.talentpact.business.tpOrganization.converter.OrganizationConverter;
import com.talentpact.business.tpOrganization.transport.input.AppListRequestTO;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.business.tpOrganization.transport.input.VideoLinkTO;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.model.GovtHrFinancialYear;
import com.talentpact.model.SysBundle;
import com.talentpact.model.SysRole;
import com.talentpact.model.SysRoleType;
import com.talentpact.model.TimeDimension;
import com.talentpact.model.admin.HrBundle;
import com.talentpact.model.employee.HrFinancialYear;
import com.talentpact.model.organise.HrForm;
import com.talentpact.model.organise.HrFormField;
import com.talentpact.model.organise.HrFormFieldGroup;
import com.talentpact.model.organization.HrOrganization;
import com.talentpact.model.organization.SysTenant;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.constants.Iconstants;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

/**
 * @author radhamadhab.dalai
 *
 */
@Stateless
public class TPOrgDS extends AbstractDS<TpOrganization> {
    @EJB
    StartupService startupService;

    @Inject
    UserSessionBean userSessionBean;

    @EJB
    LoginServerDetailDS loginServerDetailDS;

    @EJB
    SocialAppSettingDS socialAppSettingDS;

    @Inject
    SysTenantDS sysTenantDS;

    @EJB
    LoginServerRedisCache loginServerRedisCache;

    public TPOrgDS() {
        super(TpOrganization.class);
    }


    @SuppressWarnings("unused")
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public TpOrganization saveOrganization(OrganizationrequestTO organizationrequestTO)
        throws Exception {
        String OrgName = null;
        String OrgCode = null;
        Long orgId = null;

        TpOrganization tpOrganization = null;
        TpTenant tptenant = null;
        TpURL tpURL = null;
        TpPortalType tpPortalType = null;
        Integer orgFinancialYearId = null;
        Integer govtFinancialYearId = null;
        String initialDbPath = null;
        ResourceBundle resourceBundle = null;
        String initialPathForRecruit = null;
        String initialPathForInfer = null;
        String initialPathForAdmin = null;
        HrModuleUrlLink hrModuleUrlLink = null;
        LoginServerDetailTO loginServerDetailTO = null;
        SocialAppSettingTO socialAppSettingTO = null;
        try {
            loginServerDetailTO = new LoginServerDetailTO();
            socialAppSettingTO = new SocialAppSettingTO();
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPathForRecruit = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_RECRUIT_LOGO_ACCESS);
            initialPathForInfer = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_INFER_LOGO_ACCESS);
            initialPathForAdmin = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_ADMIN_LOGO_ACCESS);
            // setGovtFinancialYearID(organizationrequestTO);
            // setOrgFinancialYearID(organizationrequestTO);
            OrgName = organizationrequestTO.getOrgName();
            OrgCode = organizationrequestTO.getOrgCode();
            orgId = organizationrequestTO.getOrganizationId();
            tpOrganization = findTpOrganization(orgId);
            if (tpOrganization != null) {
                int tenantId = tpOrganization.getTpTenantID().intValue();
                tpOrganization.setName(organizationrequestTO.getOrgName());
                tpOrganization.setOrganizationCode(organizationrequestTO.getOrgCode());
                tpOrganization.setEffectiveFrom(organizationrequestTO.getEffectiveDateFrom());
                tpOrganization.setEffectiveTo(organizationrequestTO.getEffectiveDateTO());
                tpOrganization.setContractSignedDate(organizationrequestTO.getContractSignedDate());

                tptenant = findTenant(organizationrequestTO.getTenantID());
                if (tptenant != null) {
                    tpOrganization.setTpTenantID(tptenant.getTenantID());
                    tptenant.setContractSignedDate(organizationrequestTO.getContractSignedDate());
                }
                tpOrganization.setDataLocation(organizationrequestTO.getDataLocation());
                tpOrganization.setEmployeeCount(organizationrequestTO.getEmployeeCount());
                tpOrganization.setLeftLogo(organizationrequestTO.getLeftLogo());
                tpOrganization.setRightLogo(organizationrequestTO.getRightLogo());
                tpOrganization.setEffectiveTo(organizationrequestTO.getEffectiveDateTO());
                tpOrganization.setFooter(organizationrequestTO.getFooter());
                tpOrganization.setLine1(organizationrequestTO.getLine1());
                tpOrganization.setCity(organizationrequestTO.getCity());
                tpOrganization.setCityCode(organizationrequestTO.getCityID());
                tpOrganization.setState(organizationrequestTO.getState());
                tpOrganization.setCountry(organizationrequestTO.getCountry());
                tpOrganization.setZipCode(organizationrequestTO.getZipCode());
                tpOrganization.setEmail(organizationrequestTO.getEmail());
                tpOrganization.setDistrict(organizationrequestTO.getDistrict());
                tpOrganization.setSectorCode(organizationrequestTO.getSectorCode());
                tpOrganization.setCustomizationAmount(organizationrequestTO.getCustomizationAmount());
                tpOrganization.setRecurringPerMonth(organizationrequestTO.getRecurringPerMonthAmount());
                tpOrganization.setSetupCost(organizationrequestTO.getSetupCost());
                tpOrganization.setSectorID(organizationrequestTO.getSectorID());
                tpOrganization.setStatusId(organizationrequestTO.getStatusID());
                tpOrganization.setMessengerEnabled(organizationrequestTO.getMessengerEnabled());
                tpOrganization.setMobileSessionTimeout(organizationrequestTO.getMobileSessionTimeout());

                if (organizationrequestTO.getLdapServer() != null && !organizationrequestTO.getLdapServer().trim().equalsIgnoreCase("")) {
                    tpOrganization.setLdapServer(organizationrequestTO.getLdapServer());
                } else {
                    tpOrganization.setLdapServer(null);
                }

                if (organizationrequestTO.getLdapSearchBase() != null && !organizationrequestTO.getLdapSearchBase().trim().equalsIgnoreCase("")) {
                    tpOrganization.setLdapSearchBase(organizationrequestTO.getLdapSearchBase());
                } else {
                    tpOrganization.setLdapSearchBase(null);
                }


                if (organizationrequestTO.getLdapDomain() != null && !organizationrequestTO.getLdapDomain().trim().equalsIgnoreCase("")) {
                    tpOrganization.setLdapDomain(organizationrequestTO.getLdapDomain());
                } else {
                    tpOrganization.setLdapDomain(null);
                }
                tpOrganization.setLdapActive(organizationrequestTO.isLdapActive());
                tpOrganization.setLdapWindowsAuthentication(organizationrequestTO.isLdapWindowsAuthentication());

                getEntityManager().merge(tpOrganization);
                loginServerDetailTO.setOrganizationID(orgId.intValue());
                RedisClient.deleteByPattern("ALTADMIN:TPUSER:" + tenantId + ":" + orgId + "*");
                RedisClient.delete("ALTCOMMON:LOGIN:ALLURLMAP");
            } else {

                tpOrganization = new TpOrganization();
                tpOrganization.setName(organizationrequestTO.getOrgName());
                tpOrganization.setOrganizationCode(organizationrequestTO.getOrgCode());
                if (organizationrequestTO.getEffectiveDateFrom() == null) {
                    tpOrganization.setEffectiveFrom(new Date(0));
                } else {

                    tpOrganization.setEffectiveFrom(organizationrequestTO.getEffectiveDateFrom());
                }

                if (organizationrequestTO.getContractSignedDate() == null) {
                    tpOrganization.setContractSignedDate(new Date(0));
                } else {

                    tpOrganization.setContractSignedDate(organizationrequestTO.getContractSignedDate());
                }

                if (organizationrequestTO.getEffectiveDateTO() == null) {
                    tpOrganization.setEffectiveTo(new Date(0));
                } else {

                    tpOrganization.setEffectiveTo(organizationrequestTO.getEffectiveDateFrom());
                }

                tpOrganization.setCreationDate(new Date());
                tpOrganization.setDataLocation(organizationrequestTO.getDataLocation());
                tpOrganization.setEmployeeCount(organizationrequestTO.getEmployeeCount());
                tptenant = findTenant(organizationrequestTO.getTenantID());
                if (tptenant != null) {
                    tpOrganization.setTpTenantID(tptenant.getTenantID());
                }

                tpOrganization.setLeftLogo(organizationrequestTO.getLeftLogo());
                tpOrganization.setRightLogo(organizationrequestTO.getRightLogo());
                tpOrganization.setLdapActive(false);

                tpOrganization.setFooter(organizationrequestTO.getFooter());

                KeyGenerator keyGen = null;
                try {
                    keyGen = KeyGenerator.getInstance("AES");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                keyGen.init(256);
                SecretKey secretKey = keyGen.generateKey();
                String stringKey = (new Base64()).encodeToString(secretKey.getEncoded());

                //Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                //calendar.clear();
                // Set SecretKey for Organization
                //long secondsSinceEpoch = calendar.getTimeInMillis();
                //String secretKeySeeder = (Math.random() * 7493749) + secondsSinceEpoch + UUID.randomUUID().toString();
                //MessageDigest md = MessageDigest.getInstance("SHA-256");
                //byte[] hash = md.digest(secretKeySeeder.getBytes());
                //tpOrganization.setSecretKey(secretKeySeeder);
                tpOrganization.setSecretKey(stringKey);
                tpOrganization.setLine1(organizationrequestTO.getLine1());
                tpOrganization.setCity(organizationrequestTO.getCity());
                tpOrganization.setCityCode(organizationrequestTO.getCityID());
                tpOrganization.setState(organizationrequestTO.getState());
                tpOrganization.setCountry(organizationrequestTO.getCountry());
                tpOrganization.setZipCode(organizationrequestTO.getZipCode());
                tpOrganization.setEmail(organizationrequestTO.getEmail());
                tpOrganization.setSectorCode(organizationrequestTO.getSectorCode());
                tpOrganization.setDistrict(organizationrequestTO.getDistrict());
                tpOrganization.setSectorID(organizationrequestTO.getSectorID());
                tpOrganization.setStatusId(organizationrequestTO.getStatusID());
                tpOrganization.setCustomizationAmount(organizationrequestTO.getCustomizationAmount());
                tpOrganization.setRecurringPerMonth(organizationrequestTO.getRecurringPerMonthAmount());
                tpOrganization.setSetupCost(organizationrequestTO.getSetupCost());
                tpOrganization.setLdapServer(organizationrequestTO.getLdapServer());
                tpOrganization.setLdapSearchBase(organizationrequestTO.getLdapSearchBase());
                tpOrganization.setLdapDomain(organizationrequestTO.getLdapDomain());
                // tpOrganization.setSecretKey(organizationrequestTO.getSecretKey());
                tpOrganization.setLdapActive(organizationrequestTO.isLdapActive());
                tpOrganization.setLdapWindowsAuthentication(organizationrequestTO.isLdapWindowsAuthentication());
                tpOrganization.setMessengerEnabled(organizationrequestTO.getMessengerEnabled());
                tpOrganization.setMobileSessionTimeout(organizationrequestTO.getMobileSessionTimeout());

                getEntityManager().persist(tpOrganization);
            }

            setGovtFinancialYearID(organizationrequestTO, tpOrganization.getOrganizationID().intValue());
            
            orgId = tpOrganization.getOrganizationID();
            loginServerDetailTO.setOrganizationID(orgId.intValue());
            int tenantId = tpOrganization.getTpTenantID().intValue();

            // deleting old bundle list;
            EntityManager emTalentPact = getEntityManager("TalentPact");
            String hql = " delete from " + HrBundle.class.getSimpleName() + " hb " + " where hb.organizationId = :orgId " + " and hb.tenantId = :tenantId";
            Query q = emTalentPact.createQuery(hql.toString());
            q.setParameter("orgId", orgId.intValue());
            q.setParameter("tenantId", tenantId);
            q.executeUpdate();

            // Adding new bundle list
            List<Integer> bundleIds = organizationrequestTO.getBundleIds();
            if (bundleIds != null) {
                int i = 0;
                while (i < bundleIds.size()) {
                    HrBundle hb = new HrBundle();
                    hb.setCreatedBy(organizationrequestTO.getLogedInUserId());
                    hb.setCreatedDate(new Date());
                    hb.setModifiedBy(organizationrequestTO.getLogedInUserId());
                    hb.setModifiedDate(new Date());
                    hb.setOrganizationId(orgId.intValue());
                    hb.setTenantId(tenantId);
                    Object bundleO = bundleIds.get(i);
                    if (bundleO instanceof String) {
                        hb.setSysBundleId(Integer.parseInt(bundleO.toString()));
                    } else {
                        hb.setSysBundleId(bundleIds.get(i));
                    }
                    emTalentPact.persist(hb);
                    i++;
                }
            }

            //added code by vikas start here 
            Boolean isMetaDataUpdated = false;
            OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = organizationrequestTO.getOrgAppPortalTreeResponseTO();
            List<AppResponseTO> appList = orgAppPortalTreeResponseTO.getAppList();
            Map<Long, List<AppPortalTypeUrlResponseTO>> appPortalUrlMap = orgAppPortalTreeResponseTO.getAppPortalUrlMap();
            for (AppResponseTO arTO : appList) {
                String database = arTO.getDatabase();

                if (!database.isEmpty()) {
                    List<AppPortalTypeUrlResponseTO> portalList = appPortalUrlMap.get(arTO.getAppId());
                    for (AppPortalTypeUrlResponseTO portal : portalList) {
                        tpURL = findTpUrl(orgId, portal.getPortalTypeId());
                        if (portal.isPortalChecked()) {
                            if (arTO.getAppName().equalsIgnoreCase("ALTRECRUIT")) {
                                initialDbPath = null;
                                initialDbPath = initialPathForRecruit;
                            } else if (arTO.getAppName().equalsIgnoreCase("ALTINFER")) {
                                initialDbPath = null;
                                initialDbPath = initialPathForInfer;
                            } else if (arTO.getAppName().equalsIgnoreCase("ALTADMIN")) {
                                initialDbPath = null;
                                initialDbPath = initialPathForAdmin;
                            } else {
                                initialDbPath = "";
                            }
                            if (tpURL == null) {
                                tpURL = new TpURL();
                                hrModuleUrlLink = new HrModuleUrlLink();
                                tpURL.setUrl(portal.getUrl().trim());
                                tpPortalType = findtpPortalType(portal.getPortalTypeId());
                                tpURL.setTpPortalType(tpPortalType);
                                tpURL.setActive(true);
                                tpURL.setMeta_Data(database);
                                tpURL.setFavicon(arTO.getFavicon());
                                tpURL.setLandingPageID(portal.getLandingPageID());
                                tpURL.setTpOrganization(tpOrganization);
                                tpURL.setHomePage(portal.getHomePage());
                                tpURL.setLoginPage(portal.getLoginPage());
                                tpURL.setDefaultTheme(portal.getDefualtTheme());
                                tpURL.setLoginPageTheme(portal.getLoginPageTheme());
                                if (portal.getLeftLogo() != null && !portal.getLeftLogo().isEmpty()) {
                                    tpURL.setLeftLogo(initialDbPath + "/" + portal.getLeftLogo());
                                }
                                if (portal.getRightLogo() != null && !portal.getRightLogo().isEmpty()) {
                                    tpURL.setRightLogo(initialDbPath + "/" + portal.getRightLogo());
                                }
                                tpURL.setTitle(portal.getTitle());
                                tpURL.setFooter(portal.getFooter());
                                if (portal.getBanner() != null && !portal.getBanner().isEmpty()) {
                                    tpURL.setBanner(initialDbPath + "/" + portal.getBanner());
                                }
                                tpURL.setBtnTxt(portal.getBtnTxt());
                                tpURL.setHeadingTxt(portal.getHeadingTxt());
                                tpURL.setUsernameTxt(portal.getUsernameTxt());
                                tpURL.setSamlBtnTxt(portal.getSamlBtnTxt());
                                tpURL.setSamlHeader(portal.getSamlHeader());
                                tpURL.setResetPassword(portal.getResetPassword());
                                tpURL.setPasswordTxt(portal.getPasswordTxt());
                                tpURL.setMobileEnabled(portal.getMobileStatus());
                                if (portal.getBackGroundImage() != null && !portal.getBackGroundImage().isEmpty()) {
                                    tpURL.setBackgroundImage(initialDbPath + "/" + portal.getBackGroundImage());
                                }
                                tpURL.setContactNumber(portal.getContactNumber());
                                tpURL.setEmailAddress(portal.geteMail());
                               // tpURL.setExternalURL(portal.getExternalVideoPath());

                                getEntityManager().persist(tpURL);
                                if (tpPortalType.getPortalType().equalsIgnoreCase("Employee Portal")) {
                                    loginServerDetailTO.setUrlID(tpURL.getUrlID());
                                }
                                RedisClient.deleteByPattern("ALTADMIN:LOGINPAGE:" + portal.getUrl().toUpperCase() + "*");
                                RedisClient.delete("ALTCOMMON:LOGIN:URL:" + portal.getUrlId() + ":" + orgId);
                               /* if (portal.getOnBoardingVideoLinkList() != null && !portal.getOnBoardingVideoLinkList().isEmpty()) {
                                    for (VideoLinkTO vp : portal.getOnBoardingVideoLinkList()) {
                                        if (vp.getModuleIdList() != null && !vp.getModuleIdList().isEmpty()) {
                                            //  for (Object moduleID : vp.getModuleIdList()) {
                                            hrModuleUrlLink = new HrModuleUrlLink();
                                            hrModuleUrlLink.setCreatedBy(1);
                                            hrModuleUrlLink.setModifiedBy(1);
                                            hrModuleUrlLink.setCreatedDate(new Date());
                                            hrModuleUrlLink.setModifiedDate(new Date());
                                            hrModuleUrlLink.setUrl(vp.getUrl());
                                            hrModuleUrlLink.setTenantID(tenantId);
                                            hrModuleUrlLink.setOrganizationID(orgId.intValue());
                                            hrModuleUrlLink.setModuleID(vp.getModuleId());
                                            tpPortalType = findtpPortalType(portal.getPortalTypeId());
                                            hrModuleUrlLink.setPortalTypeID(tpPortalType.getPortalTypeID());
                                            EntityManager emTalentPact2 = getEntityManager("TalentPact");
                                            emTalentPact2.persist(hrModuleUrlLink);
                                        }
                                        //}
                                    }
                                }*/
                            } else {

                                tpURL.setUrl(portal.getUrl().trim());
                                tpPortalType = findtpPortalType(portal.getPortalTypeId());
                                if (tpPortalType.getPortalType().equalsIgnoreCase("Employee Portal")) {
                                    loginServerDetailTO.setUrlID(tpURL.getUrlID());
                                }
                                tpURL.setTpPortalType(tpPortalType);
                                tpURL.setActive(true);
                                tpURL.setMeta_Data(database);
                                tpURL.setFavicon(arTO.getFavicon());
                                tpURL.setTpOrganization(tpOrganization);
                                tpURL.setHomePage(portal.getHomePage());
                                tpURL.setLoginPage(portal.getLoginPage());
                                tpURL.setDefaultTheme(portal.getDefualtTheme());
                                tpURL.setLoginPageTheme(portal.getLoginPageTheme());
                                tpURL.setLandingPageID(portal.getLandingPageID());
                                String leftLogo = null;
                                if (portal.getLeftLogo() != null && tpURL.getLeftLogo() == null) {
                                    leftLogo = initialDbPath + "/" + portal.getLeftLogo();
                                    tpURL.setLeftLogo(leftLogo);
                                } else if (portal.getLeftLogo() == null && tpURL.getLeftLogo() != null) {
                                    // Do Nothing
                                } else if (portal.getLeftLogo() != null && tpURL.getLeftLogo() != null) {
                                    if (portal.getLeftLogo().equals(tpURL.getLeftLogo())) {
                                        // Do Nothing
                                    } else {
                                        if (portal.getLeftLogo().indexOf(initialDbPath) == -1) {
                                            leftLogo = initialDbPath + "/" + portal.getLeftLogo();
                                            tpURL.setLeftLogo(leftLogo);
                                        } else {
                                            leftLogo = portal.getLeftLogo();
                                            tpURL.setLeftLogo(leftLogo);
                                        }
                                    }
                                } else {
                                    // DO Nothing
                                }


                                String rightLogo = null;
                                if (portal.getRightLogo() != null && tpURL.getRightLogo() == null) {
                                    rightLogo = initialDbPath + "/" + portal.getRightLogo();
                                    tpURL.setRightLogo(rightLogo);
                                } else if (portal.getRightLogo() == null && tpURL.getRightLogo() != null) {
                                    // Do Nothing
                                } else if (portal.getRightLogo() != null && tpURL.getRightLogo() != null) {
                                    if (portal.getRightLogo().equals(tpURL.getRightLogo())) {
                                        // Do Nothing
                                    } else {
                                        if (portal.getRightLogo().indexOf(initialDbPath) == -1) {
                                            rightLogo = initialDbPath + "/" + portal.getRightLogo();
                                            tpURL.setRightLogo(rightLogo);
                                        } else {
                                            rightLogo = portal.getRightLogo();
                                            tpURL.setRightLogo(rightLogo);
                                        }
                                    }
                                } else {
                                    // DO Nothing
                                }


                                tpURL.setTitle(portal.getTitle());
                                tpURL.setFooter(portal.getFooter());

                                String banner = null;
                                if (portal.getBanner() != null && tpURL.getBanner() == null) {
                                    banner = initialDbPath + "/" + portal.getBanner();
                                    tpURL.setBanner(banner);
                                } else if (portal.getBanner() == null && tpURL.getBanner() != null) {
                                    // Do Nothing
                                } else if (portal.getBanner() != null && tpURL.getBanner() != null) {
                                    if (portal.getBanner().equals(tpURL.getBanner())) {
                                        // Do Nothing
                                    } else {
                                        if (portal.getBanner().indexOf(initialDbPath) == -1) {
                                            banner = initialDbPath + "/" + portal.getBanner();
                                            tpURL.setBanner(banner);
                                        } else {
                                            banner = portal.getBanner();
                                            tpURL.setBanner(banner);
                                        }
                                    }
                                } else {
                                    // DO Nothing
                                }


                                tpURL.setBtnTxt(portal.getBtnTxt());
                                tpURL.setHeadingTxt(portal.getHeadingTxt());
                                tpURL.setUsernameTxt(portal.getUsernameTxt());
                                tpURL.setSamlBtnTxt(portal.getSamlBtnTxt());
                                tpURL.setSamlHeader(portal.getSamlHeader());
                                tpURL.setResetPassword(portal.getResetPassword());
                                tpURL.setPasswordTxt(portal.getPasswordTxt());
                                tpURL.setMobileEnabled(portal.getMobileStatus());
                                tpURL.setExternalURL(organizationrequestTO.getExternalUrl());
                                String backGroundImage = null;
                                if (portal.getBackGroundImage() != null && tpURL.getBackgroundImage() == null) {
                                    backGroundImage = initialDbPath + "/" + portal.getBackGroundImage();
                                    tpURL.setBackgroundImage(backGroundImage);
                                } else if (portal.getBackGroundImage() == null && tpURL.getBackgroundImage() != null) {
                                    // Do Nothing
                                } else if (portal.getBackGroundImage() != null && tpURL.getBackgroundImage() != null) {
                                    if (portal.getBackGroundImage().equals(tpURL.getBackgroundImage())) {
                                        // Do Nothing
                                    } else {
                                        if (portal.getBackGroundImage().indexOf(initialDbPath) == -1) {
                                            backGroundImage = initialDbPath + "/" + portal.getBackGroundImage();
                                            tpURL.setBackgroundImage(backGroundImage);
                                        } else {
                                            backGroundImage = portal.getBackGroundImage();
                                            tpURL.setBackgroundImage(backGroundImage);
                                        }
                                    }
                                } else {
                                    // DO Nothing
                                }


                                tpURL.setContactNumber(portal.getContactNumber());
                                tpURL.setEmailAddress(portal.geteMail());
                            //    tpURL.setExternalURL(portal.getExternalVideoPath());
                                getEntityManager().merge(tpURL);
                                RedisClient.deleteByPattern("ALTADMIN:LOGINPAGE:" + portal.getUrl().toUpperCase() + "*");
                                RedisClient.delete("ALTCOMMON:LOGIN:URL:" + portal.getUrlId() + ":" + orgId);
                                if (portal.getOnBoardingVideoLinkList() != null && !portal.getOnBoardingVideoLinkList().isEmpty()) {
                                    for (VideoLinkTO vp : portal.getOnBoardingVideoLinkList()) {
                                        // if (vp.getModuleIdList() != null && !vp.getModuleIdList().isEmpty()) {
                                        // for (Object moduleID : vp.getModuleIdList()) {
                                        hrModuleUrlLink = findHrModuleUrlLink(orgId.intValue(), tenantId, vp.getModuleUrlLinkID());
                                        if (hrModuleUrlLink == null) {
                                            hrModuleUrlLink = new HrModuleUrlLink();
                                            hrModuleUrlLink.setCreatedBy(1);
                                            hrModuleUrlLink.setModifiedBy(1);
                                            hrModuleUrlLink.setCreatedDate(new Date());
                                            hrModuleUrlLink.setModifiedDate(new Date());
                                            hrModuleUrlLink.setUrl(vp.getUrl());
                                            hrModuleUrlLink.setTenantID(tenantId);
                                            hrModuleUrlLink.setOrganizationID(orgId.intValue());

                                            hrModuleUrlLink.setModuleID((vp.getModuleId()));
                                            tpPortalType = findtpPortalType(portal.getPortalTypeId());
                                            hrModuleUrlLink.setPortalTypeID(tpPortalType.getPortalTypeID());
                                            EntityManager emTalentPact2 = getEntityManager("TalentPact");
                                            emTalentPact2.persist(hrModuleUrlLink);
                                        } else {
                                            hrModuleUrlLink.setModifiedBy(1);
                                            hrModuleUrlLink.setModifiedDate(new Date());
                                            hrModuleUrlLink.setUrl(vp.getUrl());
                                            hrModuleUrlLink.setTenantID(tenantId);
                                            hrModuleUrlLink.setOrganizationID(orgId.intValue());
                                            hrModuleUrlLink.setModuleUrlLinkID(vp.getModuleUrlLinkID());
                                            hrModuleUrlLink.setModuleID((vp.getModuleId()));
                                            tpPortalType = findtpPortalType(portal.getPortalTypeId());
                                            hrModuleUrlLink.setPortalTypeID(tpPortalType.getPortalTypeID());
                                            EntityManager emTalentPact3 = getEntityManager("TalentPact");
                                            emTalentPact3.merge(hrModuleUrlLink);
                                        }
                                    }
                                    // }
                                    //   }
                                }
                            }


                        }
                    }
                    //insert into tp_database_metadata
                    TpDatabaseMetadata tpDataMeta = null;
                    tpDataMeta = findTpDatabaseMetadata(arTO.getAppId(), organizationrequestTO.getTenantID());
                    if (tpDataMeta == null) {
                        tpDataMeta = new TpDatabaseMetadata();
                        tpDataMeta.setPersistenceUnitName(database);
                        TpApp tpApp = getEntityManager().find(TpApp.class, arTO.getAppId());
                        tpDataMeta.setTpApp(tpApp);
                        TpTenant tpTenant2 = getEntityManager().find(TpTenant.class, organizationrequestTO.getTenantID());
                        tpDataMeta.setTenantID(tpTenant2);
                        isMetaDataUpdated = true;
                        getEntityManager().persist(tpDataMeta);
                    } else {
                        isMetaDataUpdated = true;
                        tpDataMeta.setPersistenceUnitName(database);
                        getEntityManager().merge(tpDataMeta);
                    }

                }
            }

            if (isMetaDataUpdated == true) {
                // startupService.setAllDatabase();
                CacheUtil.setDatabaseMetadata(startupService.setAllDatabase());
            }
            //end here 
            /**Login Server Settings start
             * 
             */
            String type = null;
            loginServerDetailDS.inActiveAllLoginServerDetailByOrgID(loginServerDetailTO.getOrganizationID());
            Integer loginServerTypeID = null;
            if (organizationrequestTO.getSocialAppSettingTOMap() != null && !organizationrequestTO.getSocialAppSettingTOMap().isEmpty()) {
                loginServerDetailTO.setDefaultServer(organizationrequestTO.getDefaultServer());
                for (Entry<String, SocialAppSettingTO> appEntry : organizationrequestTO.getSocialAppSettingTOMap().entrySet()) {
                    String key = appEntry.getKey();
                    SocialAppSettingTO appSettingTO = appEntry.getValue();
                    String arr[] = key.split(CoreConstants.AT_SPERATOR);
                    loginServerTypeID = Integer.parseInt(arr[1]);
                    type = arr[0];
                    loginServerDetailTO.setLoginServerTypeID(loginServerTypeID);
                    if (organizationrequestTO.getDefaultServer() != null && organizationrequestTO.getDefaultServer().equalsIgnoreCase(appSettingTO.getSocialAppType())) {
                        loginServerDetailTO.setIsDefault(true);
                    } else {
                        loginServerDetailTO.setIsDefault(false);
                    }
                    loginServerDetailTO.setActive(appSettingTO.isActive());
                    socialAppSettingTO.setUrlID(loginServerDetailTO.getUrlID());
                    appSettingTO.setUrlID(loginServerDetailTO.getUrlID());
                    if (!type.equalsIgnoreCase(CoreConstants.LOGIN_SERVER_SP) && !type.equalsIgnoreCase(CoreConstants.LOGIN_SERVER_LDAP)){
                    	loginServerDetailDS.insertIntoLoginServerDetail(loginServerDetailTO);
                    	socialAppSettingDS.insertIntoSocialAppSettings(appSettingTO);
                    }
                }
                if (organizationrequestTO.isLdapActive() || organizationrequestTO.getLdapTypeID() != null) {
                    loginServerDetailTO.setLoginServerTypeID(organizationrequestTO.getLdapTypeID());
                    if (organizationrequestTO.getDefaultServer() != null && organizationrequestTO.getDefaultServer().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_LDAP)) {
                        loginServerDetailTO.setIsDefault(true);
                    } else {
                        loginServerDetailTO.setIsDefault(false);
                    }
                    loginServerDetailTO.setActive(organizationrequestTO.isLdapActive());
                    loginServerDetailTO.setLogoutServerUrl(organizationrequestTO.getLdapLogoutUrl());
                    loginServerDetailDS.insertIntoLoginServerDetail(loginServerDetailTO);

                }
                if (organizationrequestTO.isSpActive()){
                	loginServerDetailTO.setActive(organizationrequestTO.isSpActive());
                	loginServerDetailTO.setLoginServerTypeID(loginServerTypeID);
                	if (organizationrequestTO.getDefaultServer() != null && organizationrequestTO.getDefaultServer().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_SP)) {
                        loginServerDetailTO.setIsDefault(true);
                    } else {
                        loginServerDetailTO.setIsDefault(false);
                    }
                	loginServerDetailTO.setLoginServerTypeName(organizationrequestTO.getSpServer());
                	loginServerDetailTO.setLogoutServerUrl(organizationrequestTO.getSpLogoutUrl());
                	loginServerDetailDS.insertIntoLoginServerDetail(loginServerDetailTO);
                }

            }
            loginServerRedisCache.deleteLoginServerAccessTO(loginServerDetailTO.getUrlID(), loginServerDetailTO.getOrganizationID().longValue());
            /**
             * Login Server Settings end
             */


        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return tpOrganization;
    }

    private TpDatabaseMetadata findTpDatabaseMetadata(Long appId, Long tenantID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        TpDatabaseMetadata result = null;
        List<TpDatabaseMetadata> TpDatabaseMetadataList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select t from TpDatabaseMetadata t where ");
            if (tenantID != null) {
                hqlQuery.append(" t.tenantID.tenantID =:tenantID");
            }
            if (appId != null) {
                hqlQuery.append(" and t.tpApp.appID=:appID ");
            }
            query = getEntityManager().createQuery(hqlQuery.toString());

            if (tenantID != null) {
                query.setParameter("tenantID", tenantID);
            }

            if (appId != null) {
                query.setParameter("appID", appId);
            }

            TpDatabaseMetadataList = query.getResultList();
            if (TpDatabaseMetadataList != null && TpDatabaseMetadataList.size() != 0) {
                result = TpDatabaseMetadataList.get(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }


    private TpURL findTpUrl(Long orgId, Long portalTypeId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        TpURL tpurl = null, result = null;
        List<TpURL> tpURLList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpURL from TpURL tpURL where ");
            if (orgId != null) {
                hqlQuery.append(" tpURL.tpOrganization.organizationID =:orgID");
            }
            if (portalTypeId != null) {
                hqlQuery.append(" and tpURL.tpPortalType.portalTypeID=:portalTypeId ");
            }
            query = getEntityManager().createQuery(hqlQuery.toString());

            if (orgId != null) {
                query.setParameter("orgID", orgId);
            }

            if (portalTypeId != null) {
                query.setParameter("portalTypeId", portalTypeId);
            }

            tpURLList = query.getResultList();
            if (tpURLList != null && tpURLList.size() != 0) {
                result = tpURLList.get(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }


    public List<TpURL> getActiveTpUrl(Long orgID, Long portalTypeId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        TpURL tpurl = null;
        List<TpURL> tpURLList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpURL from TpURL tpURL   where ");
            if (orgID != null) {
                hqlQuery.append("    tpURL.tpOrganization.organizationID =:orgID");
            }
            if (portalTypeId != null) {
                hqlQuery.append(" and tpURL.tpPortalType.portalTypeID=:portalTypeId ");
            }
            hqlQuery.append(" and tpURL.active=:active");
            query = getEntityManager().createQuery(hqlQuery.toString());
            if (orgID != null) {
                query.setParameter("orgID", orgID);
            }

            if (portalTypeId != null) {
                query.setParameter("portalTypeId", portalTypeId);
            }
            query.setParameter("active", true);
            tpURLList = query.getResultList();


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpURLList;
    }


    public List<TpURL> getTpUrl(Long orgID, Long portalTypeId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        TpURL tpurl = null;
        List<TpURL> tpURLList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpURL from TpURL tpURL where tpURL.urlID is not null ");
            if (orgID != null) {
                hqlQuery.append(" and  tpURL.tpOrganization.organizationID =:orgID");
            }
            if (portalTypeId != null) {
                hqlQuery.append(" and tpURL.tpPortalType.portalTypeID=:portalTypeId ");
            }
            query = getEntityManager().createQuery(hqlQuery.toString());

            if (orgID != null) {
                query.setParameter("orgID", orgID);
            }

            if (portalTypeId != null) {
                query.setParameter("portalTypeId", portalTypeId);
            }

            tpURLList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpURLList;
    }

    public List<Object[]> getPortalTypeByAppId(AppListRequestTO appListRequestTO) {
        StringBuilder hqlQuery = null;
        List<TpApp> appList = null;
        Query query = null;
        List<Object[]> resultlist = null;
        TpApp tpApp = null;
        try {
            resultlist = new ArrayList<Object[]>();
            List<AppRequestTO> appRequestTOList = appListRequestTO.getApprequestList();
            appList = new ArrayList<TpApp>();
            for (AppRequestTO appRequestTO : appRequestTOList) {
                tpApp = new TpApp();
                tpApp.setAppID(appRequestTO.getAppId());
                tpApp.setName(appRequestTO.getAppName());
                appList.add(tpApp);
            }

            if (appList != null && appList.size() != 0) {
                hqlQuery = new StringBuilder();
                hqlQuery.append("select tpPortalType.portalTypeID,tpPortalType.portalType,tpPortalType.tpAppId.appID,tpPortalType.tpAppId.name from  TpPortalType tpPortalType where tpPortalType.tpAppId in (:appList)");
                query = getEntityManager().createQuery(hqlQuery.toString());
                query.setParameter("appList", appList);
                resultlist = query.getResultList();

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultlist;

    }

    public List<TpURL> getAllURLs() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpURL> tpURLLIst = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpURL from TpURL tpURL");
            query = getEntityManager().createQuery(hqlQuery.toString());
            tpURLLIst = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpURLLIst;
    }

    public List<TpApp> getAllApps() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpApp> tpAppList = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpApp from TpApp tpApp");
            query = getEntityManager().createQuery(hqlQuery.toString());
            tpAppList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpAppList;
    }

    public List<TpPortalType> getAllPortals() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpPortalType> tpPortalTypeList = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from TpPortalType ");
            query = getEntityManager().createQuery(hqlQuery.toString());
            tpPortalTypeList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpPortalTypeList;
    }


    public TpTenant findTenant(long tenantID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpTenant> tpTenantList = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpTenant from TpTenant tpTenant where tpTenant.tenantID=:tenantID");
            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("tenantID", tenantID);
            tpTenantList = query.getResultList();
            if (tpTenantList != null && tpTenantList.size() != 0) {
                result = tpTenantList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }


    public List<Object[]> getAllOrganizations(OrganizationrequestTO organizationrequestTO) {


        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> tpOrganizationList = null;
        Long tenantID = null;
        if (organizationrequestTO != null) {
            tenantID = organizationrequestTO.getTenantID();
        }

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tp.name , tp.organizationCode , tp.organizationID , tp.tenantID.name  from TpOrganization tp  ");
            if (tenantID != null) {
                hqlQuery.append(" where tpOrganization.tenantID.tenantID=:tenantID ");
            }

            query = getEntityManager().createQuery(hqlQuery.toString());
            if (tenantID != null) {
                query.setParameter("tenantID", tenantID);
            }
            tpOrganizationList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpOrganizationList;
    }

    public List<TpOrganization> getOrganizationFromOrgId(Long orgId) {

        Query query = null;
        StringBuilder hqlQuery = null;
        TpOrganization tpOrganization = null;
        List<TpOrganization> tpOrganizationList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpOrganization from TpOrganization tpOrganization where tpOrganization.organizationID =:orgID ");
            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgId);
            tpOrganizationList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpOrganizationList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public TpOrganization findTpOrganization(Long orgId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpOrganization> TpOrganizationList = null;
        TpOrganization result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpOrganization from TpOrganization tpOrganization where tpOrganization.organizationID=:OrgCode");

            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("OrgCode", orgId);
            TpOrganizationList = query.getResultList();
            if (TpOrganizationList != null && TpOrganizationList.size() > 0) {
                result = TpOrganizationList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public TpPortalType findtpPortalType(Long Id) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpPortalType> tpPortalTypeList = null;
        TpPortalType tpportalType = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpPortalType from TpPortalType  tpPortalType where tpPortalType.portalTypeID=:portalId");
            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("portalId", Id);
            tpPortalTypeList = query.getResultList();
            if (tpPortalTypeList != null && tpPortalTypeList.size() > 0) {
                tpportalType = tpPortalTypeList.get(0);
            } else {
                System.out.println("Portal Type Id not found");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpportalType;

    }


    public List<Object[]> getCheckListByOrgId(Long OrgId) {
        StringBuilder queryStr = null;
        Query query = null;
        List<Object[]> resultlist = null;
        try {
            resultlist = new ArrayList<Object[]>();
            queryStr = new StringBuilder();
            queryStr.append("SELECT DISTINCT s.ItemName , a.SysCheckListID , s.SysCheckListID as sSysCheckListID,s.Sequence FROM dbo.SysCheckList s LEFT JOIN dbo.AltCheckList a ON s.SysCheckListID = a.SysCheckListID "
                    + " AND a.OrganizationID = " + OrgId + "order by s.Sequence");
            query = getEntityManager("AltCommon").createNativeQuery(queryStr.toString());
            resultlist = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultlist;

    }

    public List<Object[]> getAllSysRoleType()
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;
        try {
            queryStr = new StringBuilder();
            queryStr.append(" select sysroletype.roleTypeId,sysroletype.roleType,sysroletype.ruleID,sysroletype.description,sysroletype.defaultName from SysRoleType sysroletype ");
            queryStr.append(" where sysroletype.isActive=1 ");
            query = getEntityManager("AltCommon").createQuery(queryStr.toString());
            return query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            queryStr = null;
            query = null;
        }
    }

    public List<Object[]> getAllSysMenu()
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;
        try {
            queryStr = new StringBuilder();
            queryStr.append(" select sysmenu.menuID,sysmenu.menuName,sysmenu.menuCode  from SysMenu sysmenu  ");
            query = getEntityManager("AltCommon").createQuery(queryStr.toString());
            List<Object[]> resultList = query.getResultList();

            return query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            queryStr = null;
            query = null;
        }

    }

    public void updateAltCheckList(CopyCheckListRequestTO reqTO)
        throws Exception {

        AltCheckList altCheckList = null;
        try {
            altCheckList = new AltCheckList();
            altCheckList.setSysCheckListID((int) reqTO.getSysCheckListID());
            altCheckList.setOrganizationID((long) reqTO.getOrgID());
            altCheckList.setTenantID(reqTO.getTenantID());
            altCheckList.setModifiedDate(new Date());
            altCheckList.setCreatedDate(new Date());
            altCheckList.setIsCompleted(true);
            altCheckList.setCreatedBy((long) reqTO.getOrgID());
            altCheckList.setModifiedBy((long) reqTO.getOrgID());

            getEntityManager("AltCommon").persist(altCheckList);

        } catch (Exception Ex) {
            throw new Exception(Ex);
        } finally {

        }


    }

    public List<Object[]> getOrganizationData(Long orgId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> OrgDataList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpUrl.leftLogo,  tpUrl.rightLogo ,  tpUrl.loginPage,  tpUrl.homePage,   tpUrl.favicon ,  "
                    + " tpUrl.loginPageTheme,   tpUrl.title,   tpUrl.meta_Data,   tpUrl.defaultTheme,  tpUrl.url,  tpUrl.tpPortalType.tpAppId.name , "
                    + " tpUrl.urlID  , tpUrl.tpPortalType.portalTypeID,  tpUrl.tpOrganization.organizationID,  tpUrl.tpOrganization.name,   "
                    + " tpUrl.tpOrganization.organizationCode  ,tpUrl.tpOrganization.tenantID.tenantID   ,tpUrl.tpPortalType.portalType ,tpUrl.landingPageID   from TpURL tpUrl  "
                    + "    where  tpUrl.active = :active and  tpUrl.tpOrganization.organizationID = :orgId   ");
            query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
            query.setParameter("orgId", orgId);
            query.setParameter("active", true);
            //.setFirstResult(0).setMaxResults(5)          
            OrgDataList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return OrgDataList;
    }


    public List<Object[]> getOrganizationData() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> OrgDataList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpUrl.leftLogo,  tpUrl.rightLogo ,  tpUrl.loginPage,  tpUrl.homePage,  tpUrl.favicon , "
                    + "tpUrl.loginPageTheme,   tpUrl.title,   tpUrl.meta_Data,   tpUrl.defaultTheme,  tpUrl.url,  tpUrl.tpPortalType.tpAppId.name ,"
                    + "tpUrl.urlID  , tpUrl.tpPortalType.portalTypeID,  tpUrl.tpOrganization.organizationID,  tpUrl.tpOrganization.name,  "
                    + "tpUrl.tpOrganization.organizationCode  ,tpUrl.tpOrganization.tenantID.tenantID   ,tpUrl.tpPortalType.portalType   from TpURL tpUrl "
                    + " where  tpUrl.active = :active    ");
            query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
            query.setParameter("active", true);
            //.setFirstResult(0).setMaxResults(5)          
            OrgDataList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return OrgDataList;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getAllSysContentType(Long OrgId)
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;
        List<Object[]> resultlist = null;
        try {
            resultlist = new ArrayList<Object[]>();
            queryStr = new StringBuilder();
            queryStr.append("select scc.categoryName ,sct.contentName ,sct.contentCategoryID   from  SysContentType sct inner join SysContentCategory scc on   sct.contentCategoryID = scc.sysContentCategoryID "
                    + " inner join AltModule am on scc.moduleID = am.moduleID where organizationID = " + OrgId);
            query = getEntityManager("AltCommon").createNativeQuery((queryStr.toString()));
            return query.getResultList();


        } catch (Exception ex) {
            throw new Exception(ex);

        } finally {
            queryStr = null;
            query = null;
        }
    }


    @SuppressWarnings("unchecked")
    public List<Object[]> getAllUIForm(Long OrgId, Integer tenantID, List<Long> moduleIDList)
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;
        List<Object[]> resultlist = null;
        List<Integer> modID1 = null;

        try {
            modID1 = new ArrayList<Integer>();
            for (Long mdID : moduleIDList) {
                modID1.add(mdID.intValue());
            }
            Object[] mo = modID1.toArray();
            resultlist = new ArrayList<Object[]>();
            queryStr = new StringBuilder();
            queryStr.append("select uf.FormID, uf.FormName, T.a, uf.SubModuleID, S.n from UiForm uf left join " + "(select hf.HrFormID as a, hf.SysFormID as b "
                    + "from  HrForm hf where hf.OrganizationID =" + OrgId + " and hf.TenantID = " + tenantID + "and hf.IsActive= 1" + ")" + " T on uf.FormID = T.b" + " left join "
                    + "( select ssm.ModuleID as m, ssm.ModuleName as n from SysSubModule ssm )" + " S on uf.SubModuleID = S.m "
                    + " where uf.TenantID = 1 and uf.IsMenu != 1 and uf.subModuleID in (:modID1)");

            query = getEntityManager("TalentPact").createNativeQuery((queryStr.toString()));
            query.setParameter("modID1", modID1);
            return query.getResultList();


        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;

        } finally {
            queryStr = null;
            query = null;
        }
    }

    public void copySysRoleList(SysRoleRequestTO reqTO, String persistenceKey)
        throws Exception {
        SysRole sysRole = null;
        List<SysRoleTypeTO> sysRoleTypeTOlist = null;
        AltCheckList altCheckList = null;
        // Map<String, Integer> sysRoleTypeMap = null ;
        String databaseEntity = null;


        try {

            databaseEntity = CacheUtil.getDatabaseByPersistanceKey(persistenceKey);
            //saving sys-roles to sys role table 
            sysRoleTypeTOlist = reqTO.getSysRoleTypeTolist();
            //get sysRoleType entity from TalentPact -admin Portal ---start here
            //sysRoleTypeMap = getSysRoleTypeFromTalentPact_AdminPortal();

            //end here 

            for (SysRoleTypeTO reqTO1 : sysRoleTypeTOlist) {

                if (reqTO1.isSelected()) {
                    sysRole = findSysRole(reqTO1.getRoleTypeID().intValue(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue(), databaseEntity);
                    if (sysRole == null) {
                        SysRoleType sysRoleType=getEntityManager(databaseEntity).find(SysRoleType.class , reqTO1.getRoleTypeID().intValue());
                        sysRole = new SysRole();
                        sysRole.setOrganizationId(reqTO.getOrganizationID().intValue());
                        sysRole.setSysTenantId(reqTO.getTenantID().intValue());
                        sysRole.setCreatedBy(reqTO.getCreatedBy().intValue());
                        sysRole.setModifiedBy(reqTO.getModifiedBy().intValue());
                        sysRole.setCreatedDate(new Date());
                        sysRole.setModifiedDate(new Date());
                        sysRole.setActive(true);
                        sysRole.setRoleDescription(reqTO1.getDescription());
                        sysRole.setRoleName(reqTO1.getDefaultName());
                        sysRole.setSysRoleType(sysRoleType);
                        sysRole.setDisableEmployeeRole(false);
                        getEntityManager(databaseEntity).persist(sysRole);
                    } else {
                        SysRoleType sysRoleType=getEntityManager(databaseEntity).find(SysRoleType.class , reqTO1.getRoleTypeID().intValue());
                        sysRole.setOrganizationId(reqTO.getOrganizationID().intValue());
                        sysRole.setSysTenantId(reqTO.getTenantID().intValue());
                        sysRole.setCreatedBy(reqTO.getCreatedBy().intValue());
                        sysRole.setModifiedBy(reqTO.getModifiedBy().intValue());
                        sysRole.setCreatedDate(new Date());
                        sysRole.setModifiedDate(new Date());
                        sysRole.setActive(true);
                        sysRole.setRoleDescription(reqTO1.getDescription());
                        sysRole.setRoleName(reqTO1.getDefaultName());
                        sysRole.setSysRoleType(sysRoleType);
                        sysRole.setDisableEmployeeRole(false);
                        getEntityManager(databaseEntity).merge(sysRole);

                    }

                } else {
                    sysRole = findSysRole(reqTO1.getRoleTypeID().intValue(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue(), databaseEntity);
                    if (sysRole != null) {
                        sysRole.setActive(false);
                        getEntityManager(databaseEntity).merge(sysRole);
                    }
                }


            }

            //saving check -list in altCheckList tabble 
            altCheckList = findAltCheckList(reqTO.getSysCheckListID().intValue(), reqTO.getOrganizationID(), reqTO.getTenantID());
            if (altCheckList == null) {
                altCheckList = new AltCheckList();
                altCheckList.setSysCheckListID(reqTO.getSysCheckListID().intValue());
                altCheckList.setOrganizationID(reqTO.getOrganizationID());
                altCheckList.setTenantID(reqTO.getTenantID());
                altCheckList.setModifiedDate(new Date());
                altCheckList.setCreatedDate(new Date());
                altCheckList.setIsCompleted(true);
                altCheckList.setCreatedBy(reqTO.getCreatedBy());
                altCheckList.setModifiedBy(reqTO.getModifiedBy());

                getEntityManager("AltCommon").persist(altCheckList);
            }
            //end here 


            getEntityManager("AltCommon").flush();
            //putting value in Redis ...Key is ORGANIZE_ALLROLE__organizationID_tenantID
            RedisClient.putValueAsTO(RedisCacheKeyConstant.ORGANIZE_ALLROLE + reqTO.getOrganizationID() + RedisCacheKeyConstant.HYPHEN + reqTO.getTenantID(), sysRoleTypeTOlist);


        } catch (Exception Ex) {
            throw new Exception(Ex);
        } finally {
            sysRole = null;
            sysRoleTypeTOlist = null;
        }

    }

    public void copyHrContentList(HrContentRequestTO reqTO)
        throws Exception {
        HrContent hrContent = null;
        List<SysContentTypeTO> sysContentTypeTOlist = null;
        AltCheckList altCheckList = null;

        try {
            //saving content type to hr content table 
            sysContentTypeTOlist = reqTO.getSysContentTypeTOlist();
            for (SysContentTypeTO reqTO1 : sysContentTypeTOlist) {

                if (reqTO1.isSelected()) {
                    hrContent = new HrContent();
                    // hrContent.setCreatedBy(reqTO.getCreatedBy());
                    //hrContent.setModifiedBy(reqTO.getModifiedBy());
                    //hrContent.setCreatedDate(new Date());
                    //hrContent.setModifiedDate(new Date());
                    //hrContent.setIsActive(true)
                    hrContent.setContentCategory(reqTO1.getCategoryName());
                    //hrContent.setSysTypeID(reqTO1.getContentCategoryID());

                    hrContent.setSysTypeID((long) Integer.parseInt(String.valueOf(reqTO1.getContentCategoryID())));

                    hrContent.setType(reqTO1.getContentName());
                    getEntityManager("TalentPact").persist(hrContent);
                    break;


                }


            }

            //saving check -list in altCheckList tabble 
            altCheckList = new AltCheckList();
            altCheckList.setSysCheckListID(reqTO.getSysCheckListID().intValue());
            altCheckList.setOrganizationID(reqTO.getOrganizationID());
            altCheckList.setTenantID(reqTO.getTenantID());
            altCheckList.setModifiedDate(new Date());
            altCheckList.setCreatedDate(new Date());
            altCheckList.setIsCompleted(true);
            altCheckList.setCreatedBy(reqTO.getCreatedBy());
            altCheckList.setModifiedBy(reqTO.getModifiedBy());

            getEntityManager("AltCommon").persist(altCheckList);
            //end here 


            getEntityManager("AltCommon").flush();

        } catch (Exception Ex) {
            throw new Exception(Ex);
        } finally {
            hrContent = null;
            sysContentTypeTOlist = null;
        }

    }

    public AltCheckList findAltCheckList(int sysCheckListId, Long OrgId, Long TenantId) {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<AltCheckList> altCheckList = null;
        AltCheckList result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select altCheckList from AltCheckList altCheckList where altCheckList.sysCheckListID=:SysCheckListID and altCheckList.organizationID=:OrgID and altCheckList.tenantID=:TenantID ");

            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("SysCheckListID", sysCheckListId);
            query.setParameter("OrgID", OrgId);
            query.setParameter("TenantID", TenantId);
            altCheckList = query.getResultList();
            if (altCheckList != null && altCheckList.size() > 0) {
                result = altCheckList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public SysRole findSysRole(int RoleTypeId, Integer OrgId, Integer TenantId, String databaseEntity) {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysRole> sysRoleList = null;
        SysRole result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysRole from SysRole sysRole where sysRole.roleTypeID=:RoleTypeID and sysRole.organizationId=:OrgID and sysRole.sysTenantId=:TenantID ");

            query = getEntityManager(databaseEntity).createQuery(hqlQuery.toString());
            query.setParameter("RoleTypeID", RoleTypeId);
            query.setParameter("OrgID", OrgId);
            query.setParameter("TenantID", TenantId);
            sysRoleList = query.getResultList();
            if (sysRoleList != null && sysRoleList.size() > 0) {
                result = sysRoleList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public Map<String, Integer> getSysRoleTypeFromTalentPact_AdminPortal()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysRoleType> sysRoleType = null;
        List<Object[]> objectList = null;
        Map<String, Integer> sysRoleTypeMap = null;
        try {

            //db query --start here
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysRoleType.roleType , sysRoleType.roleTypeId from SysRoleType sysRoleType");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            objectList = query.getResultList();
            //db-query end here

            //constructin map for later use --start  here
            sysRoleTypeMap = new HashMap<String, Integer>();
            for (Object[] object : objectList) {
                sysRoleTypeMap.put((String) object[0], (Integer) object[1]);
            }
            //construction map --end here 
            return sysRoleTypeMap;

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            query = null;
            hqlQuery = null;
            sysRoleType = null;
            sysRoleTypeMap = null;

        }
    }


    public List<SysRoleTypeTO> syncSysRoleTypeList(SysRoleRequestTO reqTO, String persistanceKey)
        throws Exception {
        SysRole sysRole = null;
        List<SysRoleTypeTO> sysRoleTypeTOlist = null;
        String databaseEntity = null;

        try {
            databaseEntity = CacheUtil.getDatabaseByPersistanceKey(persistanceKey);
            sysRoleTypeTOlist = reqTO.getSysRoleTypeTolist();
            for (SysRoleTypeTO sysRoleTypeTO : sysRoleTypeTOlist) {
                sysRole = findSysRole(sysRoleTypeTO.getRoleTypeID().intValue(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue(), databaseEntity);
                if (sysRole == null) {
                    sysRoleTypeTO.setSelected(false);
                } else {
                    if (sysRole.getActive()) {
                        sysRoleTypeTO.setSelected(true);
                    } else {
                        sysRoleTypeTO.setSelected(false);
                    }
                }
            }
            return sysRoleTypeTOlist;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

            sysRole = null;
            sysRoleTypeTOlist = null;
            databaseEntity = null;
        }
    }


    public List<Object[]> getOrganizationData(Long orgId, Long portalTypeId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> OrgDataList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpUrl.leftLogo,  tpUrl.rightLogo ,  tpUrl.loginPage,  tpUrl.homePage,   tpUrl.favicon ,  "
                    + " tpUrl.loginPageTheme,   tpUrl.title,   tpUrl.meta_Data,   tpUrl.defaultTheme,  tpUrl.url,  tpUrl.tpPortalType.tpAppId.name , "
                    + " tpUrl.urlID  , tpUrl.tpPortalType.portalTypeID,  tpUrl.tpOrganization.organizationID,  tpUrl.tpOrganization.name,   "
                    + " tpUrl.tpOrganization.organizationCode  ,tpUrl.tpOrganization.tenantID.tenantID   ,tpUrl.tpPortalType.portalType   from TpURL tpUrl  "
                    + "    where  tpUrl.active = :active and  tpUrl.tpOrganization.organizationID = :orgId   ");

            if (portalTypeId != null) {
                hqlQuery.append("  and tpUrl.tpPortalType.portalTypeID = :portalId  ");
            }
            query = getEntityManager("TalentPactAuth").createQuery(hqlQuery.toString());
            query.setParameter("orgId", orgId);
            query.setParameter("active", true);
            if (portalTypeId != null) {
                query.setParameter("portalId", portalTypeId);
            }

            OrgDataList = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return OrgDataList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void copyUIFormList(UIFormRequestTO reqTO, String persistenceKey)
        throws Exception {
        HrForm hrForm = null;
        HrFormField hrFormField = null;
        HrFormFieldGroup hrFormFieldGroup = null;
        List<UiFormResponseTO> uIFormTypeTOlist = null;

        Map<String, Integer> uIFormTypeMap = null;
        String databaseEntity = null;
        List<UIFormDetailsTO> formDetails = null;
        Long hrFormID = null;
        Long hrFormFieldGroupID = null;
        Long hrFormFieldID = null;


        try {

            databaseEntity = CacheUtil.getDatabaseByPersistanceKey(persistenceKey);
            uIFormTypeTOlist = reqTO.getUiFormTOlist();
            for (UiFormResponseTO reqTO1 : uIFormTypeTOlist) {
                hrForm = findHrForm(reqTO1.getFormID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID().intValue());
                if (hrForm == null) {
                    if (reqTO1.isSelected() && reqTO1.getHrFormID() == null) {
                        hrForm = new HrForm();
                        hrForm.setOrganizationId(reqTO.getOrganizationID().intValue());
                        hrForm.setTenantId(reqTO.getTenantID().intValue());
                        hrForm.setCreatedBy(userSessionBean.getUserID().intValue());
                        hrForm.setModifiedBy(userSessionBean.getUserID().intValue());
                        hrForm.setCreatedDate(new Date());
                        hrForm.setModifiedDate(new Date());
                        hrForm.setSysForm(null);
                        hrForm.setSysFormId(reqTO1.getFormID());
                        hrForm.setPermissionId(12);
                        hrForm.setActive(true);
                        formDetails = getAllUIFormDetails(reqTO1.getFormID());
                        hrForm.setCustomComponentAllowed(false);
                        getEntityManager(databaseEntity).persist(hrForm);
                        hrFormID = hrForm.getHrFormId();
                        if (formDetails != null) {
                            for (UIFormDetailsTO ui : formDetails) {
                                hrFormFieldGroup = findHrFormFieldGroup(ui.getFieldGroupID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID(), hrFormID);
                                if (hrFormFieldGroup == null) {
                                    hrFormFieldGroup = new HrFormFieldGroup();
                                    hrFormFieldGroup.setCreatedBy(userSessionBean.getUserID().intValue());
                                    hrFormFieldGroup.setModifiedBy(userSessionBean.getUserID().intValue());
                                    hrFormFieldGroup.setCreatedDate(new Date());
                                    hrFormFieldGroup.setModifiedDate(new Date());
                                    hrFormFieldGroup.setOrganizationID(reqTO.getOrganizationID().intValue());
                                    hrFormFieldGroup.setTenantId(reqTO.getTenantID().intValue());
                                    hrFormFieldGroup.setHrFieldGroupName(null);
                                    hrFormFieldGroup.setHrFormId(hrFormID.longValue());
                                    hrFormFieldGroup.setSysFieldGroupId(ui.getFieldGroupID());
                                    getEntityManager(databaseEntity).persist(hrFormFieldGroup);
                                    hrFormFieldGroupID = hrFormFieldGroup.getHrFormFieldGroupId();
                                } else {
                                    hrFormFieldGroupID = hrFormFieldGroup.getHrFormFieldGroupId();
                                }

                                if (!isExistHrFormField(ui.getFormFieldID(), reqTO.getOrganizationID().intValue(), reqTO.getTenantID(), hrFormFieldGroupID)) {
                                    hrFormField = new HrFormField();
                                    hrFormField.setCreatedBy(userSessionBean.getUserID().intValue());
                                    hrFormField.setModifiedBy(userSessionBean.getUserID().intValue());
                                    hrFormField.setCreatedDate(new Date());
                                    hrFormField.setModifiedDate(new Date());
                                    hrFormField.setOrganizationID(reqTO.getOrganizationID().intValue());
                                    hrFormField.setTenantId(reqTO.getTenantID().intValue());
                                    hrFormField.setApprovalRequired(ui.getIsApprovalRequired());
                                    hrFormField.setAttachmentRequired(ui.getIsAttachmentRequired());
                                    hrFormField.setAttachmentEnabled(ui.getIsAttachmentEnabled());
                                    hrFormField.setSysFormFieldID(ui.getFormFieldID());
                                    hrFormField.setEffectiveDateRequired(ui.getIsEffectiveDateRequired());
                                    hrFormField.setActive(true);
                                    hrFormField.setActionType(false);
                                    hrFormField.setHrFormFieldGroupId(hrFormFieldGroupID);
                                    hrFormField.setConfigurationValueWeb(ui.getConfigurationValueWeb());
                                    hrFormField.setConfigurationValueMobile(ui.getConfigurationValueMobile());
                                    hrFormField.setConfigurationValueChatBot(ui.getConfigurationValueChatBot());
                                    hrFormField.setGroupParameter(ui.getIsGroupParameter());
                                    getEntityManager(databaseEntity).persist(hrFormField);

                                }
                            }
                        }
                    }
                } else if (reqTO1.isSelected() == false) {
                    hrForm.setActive(false);
                    getEntityManager(databaseEntity).merge(hrForm);
                } else if (reqTO1.isSelected() == true) {
                    hrForm.setActive(true);
                    getEntityManager(databaseEntity).merge(hrForm);
                }
            }


        } catch (Exception Ex) {
            Ex.printStackTrace();
            throw Ex;
        } finally {
            hrForm = null;

        }

    }


    @SuppressWarnings("unchecked")
    public List<UIFormDetailsTO> getAllUIFormDetails(Integer FormID)
        throws Exception {
        StringBuilder queryStr = null;
        Query query = null;
        List<Object[]> resultList = null;
        List<UIFormDetailsTO> formDetailslist = null;
        try {
            resultList = new ArrayList<Object[]>();
            queryStr = new StringBuilder();
            queryStr.append("SELECT uffg.FieldGroupID, uff.FormFieldID, uff.IsAttachmentEnabled, uff.IsAttachmentRequired, uff.IsApprovalRequired,uff.IsEffectiveDateRequired, uff.IsMandatory, uff.configurationValueWeb, uff.configurationValueMobile, uff.isGroupParameter, uff.configurationValueBot "
                    + " FROM UiForm uf INNER JOIN UiFormFieldGroup uffg on uf.FormID = uffg.FormID INNER JOIN UiFormField uff  on uff.FieldGroupID = uffg.FieldGroupID "
                    + "  where uf.FormID =" + FormID + " and uf.TenantID = 1 ");
            query = getEntityManager("TalentPact").createNativeQuery((queryStr.toString()));
            resultList = query.getResultList();

            if ((resultList != null) && !resultList.isEmpty()) {
                formDetailslist = getUIFromDetails(resultList);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;

        } finally {
            queryStr = null;
            query = null;
        }
        return formDetailslist;
    }


    public static List<UIFormDetailsTO> getUIFromDetails(List<Object[]> resultList) {
        List<UIFormDetailsTO> uiFormDatailsList = null;
        UIFormDetailsTO responseTO = null;
        Integer FieldGroupID = null;
        Integer FormFieldID = null;
        Boolean IsAttachmentEnabled = null;
        Boolean IsAttachmentRequired = null;
        Boolean IsApprovalRequired = null;
        Boolean IsEffectiveDateRequired = null;
        Boolean IsMandatory = null;
        String configurationValueWeb = null;
        String configurationValueMobile = null;
        String configurationValueChatBot = null;
        Boolean IsGroupParameter = null;

        try {
            uiFormDatailsList = new ArrayList<UIFormDetailsTO>();

            for (Object[] object : resultList) {
                responseTO = new UIFormDetailsTO();
                if (object[0] != null) {
                    FieldGroupID = (Integer) object[0];
                    responseTO.setFieldGroupID(FieldGroupID);
                }
                if (object[1] != null) {
                    FormFieldID = (Integer) object[1];
                    responseTO.setFormFieldID(FormFieldID);
                }
                if (object[2] != null) {
                    IsAttachmentEnabled = (Boolean) object[2];
                    responseTO.setIsAttachmentEnabled(IsAttachmentEnabled);
                }

                if (object[3] != null) {
                    IsAttachmentRequired = (Boolean) object[3];
                    responseTO.setIsAttachmentRequired(IsAttachmentRequired);
                }

                if (object[4] != null) {
                    IsApprovalRequired = (Boolean) object[4];
                    responseTO.setIsApprovalRequired(IsApprovalRequired);


                }

                if (object[5] != null) {
                    IsEffectiveDateRequired = (Boolean) object[5];
                    responseTO.setIsEffectiveDateRequired(IsEffectiveDateRequired);
                }
                if (object[6] != null) {
                    IsMandatory = (Boolean) object[6];
                    responseTO.setIsMandatory(IsMandatory);
                }
                if (object[7] != null) {
                    configurationValueWeb = (String) object[7];
                    responseTO.setConfigurationValueWeb(configurationValueWeb);
                }
                if (object[8] != null) {
                    configurationValueMobile = (String) object[8];
                    responseTO.setConfigurationValueMobile(configurationValueMobile);
                }
                if (object[9] != null) {
                    IsMandatory = (Boolean) object[9];
                    responseTO.setIsGroupParameter(IsGroupParameter);
                }
                if (object[10] != null) {
                    configurationValueChatBot = (String) object[10];
                    responseTO.setConfigurationValueChatBot(configurationValueChatBot);
                }
                uiFormDatailsList.add(responseTO);
            }

        } catch (Exception e) {
        } finally {
            responseTO = null;

        }
        return uiFormDatailsList;
    }

    private HrForm findHrForm(Integer hrFormID, Integer organizationID, Integer tenantID)
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrForm> hrFormList = null;
        HrForm result = null;

        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select hrf from HrForm  hrf  where hrf.sysFormId=:hrFormID  and hrf.organizationId=:organizationID and hrf.tenantId=:tenantID");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("hrFormID", hrFormID);
            query.setParameter("organizationID", organizationID);
            query.setParameter("tenantID", tenantID);
            // query = getEntityManager("TalentPact").createNativeQuery((queryStr.toString()));
            hrFormList = query.getResultList();

            if (hrFormList != null && hrFormList.size() > 0) {
                result = hrFormList.get(0);
            }

            //            if (hrFormList != null && hrFormList.size() > 0) {
            //                result = new HrForm();
            //                result.setHrFormId(((hrFormList.get(0)).longValue()));
            //            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
        return result;
    }


    private HrFormFieldGroup findHrFormFieldGroup(Integer sysFormFieldGroupID, Integer organizationID, Integer tenantID, Long hrFormId)
        throws Exception {
        Query query = null;
        StringBuilder queryStr = null;
        List<BigInteger> hrFormGroupList = null;
        HrFormFieldGroup result = null;
        try {

            queryStr = new StringBuilder();
            queryStr.append("select hrfg.HrFormFieldGroupID from HrFormFieldGroup  hrfg  where hrfg.SysFieldGroupID= " + sysFormFieldGroupID + " and hrfg.OrganizationID= "
                    + organizationID + " and hrfg.TenantID = " + tenantID + " and hrfg.hrFormId = " + hrFormId);
            query = getEntityManager("TalentPact").createNativeQuery((queryStr.toString()));
            hrFormGroupList = query.getResultList();
            if (hrFormGroupList != null && hrFormGroupList.size() > 0) {
                result = new HrFormFieldGroup();
                result.setHrFormFieldGroupId(((BigInteger) (hrFormGroupList.get(0))).longValue());
            }
        } catch (Exception ex) {

            throw ex;
        } finally {
            queryStr = null;
            query = null;

        }
        return result;

    }


    private boolean isExistHrFormField(Integer sysFormFieldID, Integer organizationID, Integer tenantID, Long hrFormFieldGroupID)
        throws Exception {
        Query query = null;
        StringBuilder queryStr = null;
        List<HrFormField> hrFormFieldList = null;
        try {

            queryStr = new StringBuilder();
            queryStr.append("select * from HrFormField  hrf  where hrf.HrFormFieldGroupID= " + hrFormFieldGroupID + " and hrf.OrganizationID= " + organizationID
                    + " and hrf.TenantID = " + tenantID + " and hrf.sysFormFieldId = " + sysFormFieldID);
            query = getEntityManager("TalentPact").createNativeQuery((queryStr.toString()));
            hrFormFieldList = query.getResultList();
            if (hrFormFieldList != null && hrFormFieldList.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception ex) {

            throw ex;
        } finally {
            queryStr = null;
            query = null;

        }
    }


    @SuppressWarnings("unchecked")
    public List<Object[]> getOrgMoreInfoListByAppIdOrgIdTenantID(OrganizationrequestTO organizationrequestTO) {


        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> organizationMoreInfoList = null;
        Long orgID = null;
        String url = null;
        Long portalID = null;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpUrl.leftLogo,  tpUrl.rightLogo ,tpUrl.favicon ,  "
                    + "    tpUrl.title,   tpUrl.meta_Data,tpUrl.resetPassword,tpUrl.footer,tpUrl.btnTxt,tpUrl.passwordTxt,tpUrl.usernameTxt,  "
                    + " tpUrl.headingTxt  , tpUrl.banner,  tpUrl.samlHeader,  tpUrl.samlBtnTxt from TpURL tpUrl  "
                    + "    where   tpUrl.tpOrganization.organizationID = :orgId and tpUrl.url=:url and tpUrl.tpPortalType.portalTypeID=:portalID");


            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("url", organizationrequestTO.getUrl());
            query.setParameter("portalID", organizationrequestTO.getPortalTypeID());
            query.setParameter("orgId", organizationrequestTO.getOrganizationId());

            organizationMoreInfoList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return organizationMoreInfoList;
    }


    @SuppressWarnings("unchecked")
    public List<Object[]> getSysBundle()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" sb.bundleId, sb.name ");
            hqlQuery.append(" from ");
            hqlQuery.append(" " + SysBundle.class.getSimpleName() + " sb ");
            hqlQuery.append(" order by sb.name ");
            query = getEntityManager("TalentPact").createQuery((hqlQuery.toString()));
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }


    @SuppressWarnings("unchecked")
    public List<Integer> getOrgBundle(int orgId)
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" hb.sysBundleId ");
            hqlQuery.append(" from ");
            hqlQuery.append(" " + HrBundle.class.getSimpleName() + " hb ");
            hqlQuery.append(" where hb.organizationId = :orgId ");
            query = getEntityManager("TalentPact").createQuery((hqlQuery.toString()));
            query.setParameter("orgId", orgId);
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }


    public List<AddressResponseTO> getCountryDropDown() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<AddressResponseTO> countryList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sc.countryID , sc.name from SysCountry sc  ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                countryList = OrganizationConverter.getCountryDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return countryList;
    }


    public List<AddressResponseTO> getStateDropDown(String country) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<AddressResponseTO> stateList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ss.stateID , ss.name from SysState ss where ss.country.name=:country");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("country", country);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                stateList = OrganizationConverter.getStateDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stateList;
    }

    public List<AddressResponseTO> getDistrictDropDown(String state) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<AddressResponseTO> districtList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sd.districtID , sd.name from SysDistrict sd where sd.state.name=:state");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("state", state);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                districtList = OrganizationConverter.getDistrictDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return districtList;
    }

    public List<AddressResponseTO> getCityDropDown(String district) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<AddressResponseTO> cityList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sc.cityID , sc.name from SysCity sc where sc.sysDistrict.name=:district");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("district", district);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                cityList = OrganizationConverter.getCityDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cityList;
    }


    public List<AddressResponseTO> getZipCodeDropDown(Integer cityID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<AddressResponseTO> zipList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sp.pinCodeID, sp.pinCode from SysPinCode sp where sp.sysCity.cityID=:cityID");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("cityID", (long) cityID);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                zipList = OrganizationConverter.getZipDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return zipList;
    }

    public List<SectorCostResponseTO> getSectorDropDown() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SectorCostResponseTO> sectorList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ss.sectorId, ss.sectorLabel from SysSector ss ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                sectorList = OrganizationConverter.getSectorDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sectorList;
    }


    public List<FinancialYearResponseTO> getFinancialYearList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<FinancialYearResponseTO> finYearList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select hfy.financialYearId, hfy.startDateId.theDate,hfy.endDateId.theDate,hfy.hrOrganization.organizationId from HrFinancialYear hfy ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                finYearList = OrganizationConverter.getFinancialYearList(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return finYearList;
    }

    public List<FinancialYearResponseTO> getGovtFinancialYearList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<FinancialYearResponseTO> finYearList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select gfy.financialYearId, gfy.startDateId.theDate,gfy.endDateId.theDate,gfy.hrOrganization.organizationId from GovtHrFinancialYear gfy ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                finYearList = OrganizationConverter.getGovtFinancialYearList(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return finYearList;
    }

    public Integer setOrgFinancialYearID(OrganizationrequestTO organizationrequestTO, int orgID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        int financialYearId = 0;
        Date startDate = null;
        Date date = null;
        HrFinancialYear hrFinancialYear = null;
        Integer orgFYStartDateID = null;
        Integer orgFYEndDateID = null;
        SysTenant sysTenant = null;
        HrOrganization hrOrg = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select st from SysTenant st where st.tenantId=:tenantId ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("tenantId", organizationrequestTO.getTenantID().intValue());
            sysTenant = (SysTenant) query.getSingleResult();

            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ho from HrOrganization ho where ho.organizationId=:orgID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            hrOrg = (HrOrganization) query.getSingleResult();

            hrFinancialYear = findOrgFY(organizationrequestTO, orgID);
            if (hrFinancialYear == null) {
                orgFYStartDateID = getOrgFYStartDateID(organizationrequestTO.getOrgFinancialStartDate());
                orgFYEndDateID = getOrgFYEndDateID(organizationrequestTO.getOrgFinancialStartDate());
                date = new Date();
                hrFinancialYear = new HrFinancialYear();
                hrFinancialYear.setHrOrganization(hrOrg);
                hrFinancialYear.setCreatedBy(1);
                hrFinancialYear.setModifiedBy(null);
                hrFinancialYear.setCreatedDate(date);
                hrFinancialYear.setModifiedDate(date);
                hrFinancialYear.setSysTenant(sysTenant);
                hrFinancialYear.setStartDate(orgFYStartDateID);
                hrFinancialYear.setEndDate(orgFYEndDateID);
                hrFinancialYear.setIsCurrentYear(false);
                getEntityManager("TalentPact").persist(hrFinancialYear);
            } else {
                hrFinancialYear.setHrOrganization(hrOrg);
                hrFinancialYear.setModifiedBy(1);
                hrFinancialYear.setModifiedDate(date);
                hrFinancialYear.setSysTenant(sysTenant);
                hrFinancialYear.setStartDate(orgFYStartDateID);
                getEntityManager("TalentPact").merge(hrFinancialYear);

            }
        } catch (Exception ex) {

        } finally {

        }
        return financialYearId;
    }

    public Integer getOrgFYStartDateID(Date fYstartDate) {
        Integer startFinancialYearId = null;
        Date startDate = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object> resultList = null;
        Date orgFinStartDate = null;
        try {
            orgFinStartDate = fYstartDate;
            hqlQuery = new StringBuilder();
            hqlQuery.append("select timeDimensionId  from  TimeDimension where theDate=:orgFinStartDate");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("orgFinStartDate", orgFinStartDate);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                startFinancialYearId = (Integer) (resultList.get(0));
            }

        } catch (Exception ex) {

        } finally {

        }
        return startFinancialYearId;
    }

    public Integer getOrgFYEndDateID(Date fYstartDate) {
        Integer endFinancialYearId = null;
        Date startDate = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object> resultList = null;
        Date orgFinEndDate = null;

        try {
            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(fYstartDate);

            cal.add(cal.DATE, -1);
            Date dd = cal.getTime();
            String preEndDate = formater.format(dd);
            Date finalEndDate = formater.parse(preEndDate.toString());
            orgFinEndDate = finalEndDate;
            hqlQuery = new StringBuilder();
            hqlQuery.append("select timeDimensionId  from  TimeDimension where theDate=:orgFinEndDate");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("orgFinEndDate", orgFinEndDate);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                endFinancialYearId = (Integer) (resultList.get(0));
            }

        } catch (Exception ex) {

        } finally {

        }
        return endFinancialYearId;
    }


    public Integer setGovtFinancialYearID(OrganizationrequestTO organizationrequestTO, int orgID)
        throws Exception {

        int financialYearId = 0;
        Date startDate = null;
        Date date = null;
        GovtHrFinancialYear govtHrFinancialYear = null;
        TimeDimension govtFYStartDateID = null;
        TimeDimension govtFYEndDateID = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        SysTenant sysTenant = null;
        HrOrganization hrOrg = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select st from SysTenant st where st.tenantId=:tenantId ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("tenantId", organizationrequestTO.getTenantID().intValue());
            sysTenant = (SysTenant) query.getSingleResult();
            govtHrFinancialYear = findGovtFY(organizationrequestTO, orgID);
            govtFYStartDateID = getGovtFYStartDateID(organizationrequestTO.getGovtFinancialStartDate());
            govtFYEndDateID = getGovtFYEndDateID(organizationrequestTO.getGovtFinancialStartDate());
            date = new Date();
            if (govtHrFinancialYear == null) {
                govtHrFinancialYear = new GovtHrFinancialYear();
                govtHrFinancialYear.setOrganizationId(orgID);
                govtHrFinancialYear.setSysTenant(sysTenant);
                govtHrFinancialYear.setCreatedBy(1);
                govtHrFinancialYear.setCreatedDate(date);
                govtHrFinancialYear.setModifiedBy(null);
                govtHrFinancialYear.setModifiedDate(date);
                govtHrFinancialYear.setStartDateId(govtFYStartDateID);
                govtHrFinancialYear.setEndDateId(govtFYEndDateID);
                govtHrFinancialYear.setIsCurrentYear(false);
                getEntityManager("TalentPact").persist(govtHrFinancialYear);
            } else {
                govtHrFinancialYear.setModifiedBy(1);
                govtHrFinancialYear.setModifiedDate(date);
                govtHrFinancialYear.setStartDateId(govtFYStartDateID);
                govtHrFinancialYear.setEndDateId(govtFYEndDateID);
                getEntityManager("TalentPact").merge(govtHrFinancialYear);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {

        }
        return financialYearId;
    }

    public TimeDimension getGovtFYStartDateID(Date fYstartDate)
        throws Exception {
        Integer startFinancialYearId = null;
        Date startDate = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object> resultList = null;
        Date govtFinStartDate = null;
        TimeDimension startDateId = null;
        try {
            govtFinStartDate = fYstartDate;
            hqlQuery = new StringBuilder();
            hqlQuery.append("select Td  from  TimeDimension td where td.theDate=:govtFinStartDate");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("govtFinStartDate", govtFinStartDate);
            startDateId = (TimeDimension) query.getSingleResult();
            /*if ((resultList != null) && !resultList.isEmpty()) {
                startFinancialYearId = (Integer) (resultList.get(0));
            }*/

        } catch (Exception ex) {

        } finally {

        }
        return startDateId;
    }

    public TimeDimension getGovtFYEndDateID(Date fYstartDate)
        throws Exception {
        Integer endFinancialYearId = null;
        Date startDate = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object> resultList = null;
        Date govtFinEndDate = null;
        TimeDimension endDateId = null;

        try {
            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(fYstartDate);
            cal.add(Calendar.YEAR, 1);
            cal.add(Calendar.DATE, -1);
            Date dd = cal.getTime();
            String preEndDate = formater.format(dd);
            Date finalEndDate = formater.parse(preEndDate.toString());
            govtFinEndDate = finalEndDate;
            hqlQuery = new StringBuilder();
            hqlQuery.append("select td  from  TimeDimension td where td.theDate=:govtFinEndDate");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("govtFinEndDate", govtFinEndDate);
            endDateId = (TimeDimension) query.getSingleResult();
            /* if ((resultList != null) && !resultList.isEmpty()) {
                 endFinancialYearId = (Integer) (resultList.get(0));
             }*/

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {

        }
        return endDateId;
    }

    public GovtHrFinancialYear findGovtFY(OrganizationrequestTO organizationrequestTO, int orgID) {
        GovtHrFinancialYear govtHrFinancialYear = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<GovtHrFinancialYear> govtFyList = null;
        GovtHrFinancialYear result = null;
        Date startDate = null;
        List<Object> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ghy  from  GovtHrFinancialYear ghy where  ghy.sysTenant.tenantId=:tenantId and ghy.hrOrganization.organizationId=:orgID");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("orgID", organizationrequestTO.getOrganizationId().intValue());
            query.setParameter("tenantId", organizationrequestTO.getTenantID().intValue());
            govtFyList = query.getResultList();
            if (govtFyList != null && govtFyList.size() > 0) {
                result = govtFyList.get(govtFyList.size()-1);
            }
            return result;

        } catch (Exception ex) {

        } finally {

        }
        return result;
    }


    @SuppressWarnings("unchecked")
    public HrFinancialYear findOrgFY(OrganizationrequestTO organizationrequestTO, int orgID)

    {
        HrFinancialYear hrFinancialYear = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrFinancialYear> orgFyList = null;
        HrFinancialYear result = null;
        Date startDate = null;
        List<Object> resultList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ghy  from  HrFinancialYear ghy where ghy.sysTenant.tenantId=:tenantId and ghy.hrOrganization.organizationId=:orgID");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            //    query.setParameter("organizationID", organizationrequestTO.getOrganizationId().intValue());
            query.setParameter("tenantId", organizationrequestTO.getTenantID().intValue());
            orgFyList = query.getResultList();
            if (orgFyList != null && orgFyList.size() > 0) {
                result = orgFyList.get(0);
            }
            return result;

        } catch (Exception ex) {

        } finally {

        }
        return result;
    }


    public void saveOrgWebPayStatus(String orgCode, boolean status)

    {
        Query query = null;
        StringBuilder hqlQuery = null;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("update  TpOrganization set webPayStatus =:status  where organizationCode=:orgCode");
            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("orgCode", orgCode);
            query.setParameter("status", status);
            query.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getAllOrganizationListBySector(Integer sectorId)
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> sysSectorObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct s.organizationId,s.organizationName,s.organizationCode,s.sysSector.sectorLabel from HrOrganization s where s.sectorId=:sectorId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("sectorId", sectorId);
            sysSectorObjectList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return sysSectorObjectList;
    }


    @SuppressWarnings("unchecked")
    public List<Object[]> getAllOrganizationList()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> orgObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select o.OrganizationID,o.OrganizationName,s.SectorID,s.SectorCode from sysSector s");
            hqlQuery.append("  join HrOrganization o");
            hqlQuery.append(" on s.SectorID=o.SectorID ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            orgObjectList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return orgObjectList;
    }


    public List<SysSubModuleResponseTO> getModuleListDropDown()

    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysSubModuleResponseTO> modulelist = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" a.moduleId, a.moduleName ");
            hqlQuery.append(" from ");
            hqlQuery.append(" SysSubModule a ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                modulelist = converterModuleList(resultList);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return modulelist;
    }

    public static List<SysSubModuleResponseTO> converterModuleList(List<Object[]> resultList) {
        List<SysSubModuleResponseTO> list = null;
        SysSubModuleResponseTO responseTO = null;
        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysSubModuleResponseTO();
                if (object[0] != null) {
                    responseTO.setModuleId((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setModuleName((String) object[1]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            responseTO = null;
        }
        return list;
    }

    public Map<Long, List<VideoLinkTO>> getModuleUrlMap(Integer orgID, Integer tenantID)
        throws Exception {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<VideoLinkTO> list = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" moduleUrlLinkID, moduleID, portalTypeID, url ");
            hqlQuery.append(" from HrModuleUrlLink ");
            hqlQuery.append(" where organizationID=:organizationID and tenantID=:tenantID ");

            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("organizationID", orgID);
            query.setParameter("tenantID", tenantID);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                list = moduleUrlConverter(resultList);
            }
            Map<Long, List<VideoLinkTO>> map = new HashMap<Long, List<VideoLinkTO>>();
            if (list != null) {
                for (VideoLinkTO link : list) {
                    List<VideoLinkTO> links = map.get(link.getPortalTypeID());
                    if (links != null) {
                        links.add(link);
                    } else {
                        links = new LinkedList<VideoLinkTO>();
                        links.add(link);
                    }
                    map.put(link.getPortalTypeID(), links);

                }
            }
            return map;

        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }

    }

    public static List<VideoLinkTO> moduleUrlConverter(List<Object[]> resultList) {
        List<VideoLinkTO> list = null;
        VideoLinkTO responseTO = null;
        List<Integer> mID = null;
        try {

            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new VideoLinkTO();
                if (object[0] != null) {
                    responseTO.setModuleUrlLinkID((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setModuleId((Integer) object[1]);
                    mID = new ArrayList<>();
                    mID.add((Integer) object[1]);
                    responseTO.setModuleIdList(mID);
                }

                if (object[2] != null) {
                    responseTO.setPortalTypeID((Long) object[2]);
                }
                if (object[3] != null) {
                    responseTO.setUrl((String) object[3]);
                }

                list.add(responseTO);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            responseTO = null;
        }
        return list;
    }

    private HrModuleUrlLink findHrModuleUrlLink(Integer organizationID, Integer tenantID, Integer hrModuleUrlLinkID)
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrModuleUrlLink> hrModuleUrlLinkList = null;
        HrModuleUrlLink result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select m from HrModuleUrlLink  m  where m.moduleUrlLinkID=:hrModuleUrlLinkID and m.organizationID=:organizationID and m.tenantID=:tenantID ");
            EntityManager emTalentPact2 = getEntityManager("TalentPact");
            query = emTalentPact2.createQuery(hqlQuery.toString());
            query.setParameter("hrModuleUrlLinkID", hrModuleUrlLinkID);
            query.setParameter("organizationID", organizationID);
            query.setParameter("tenantID", tenantID);
            hrModuleUrlLinkList = query.getResultList();
            if (hrModuleUrlLinkList != null && hrModuleUrlLinkList.size() > 0) {
                result = hrModuleUrlLinkList.get(0);
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

    public List<SysContentTypeTO> getSysContentTypeDropDown() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysContentTypeTO> cList = null;
        Integer categoryId = 62;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sc.sysTypeId , sc.sysType from SysContentType sc  where sc.contentCategory.categoryId=:categoryId");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("categoryId", categoryId);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                cList = OrganizationConverter.getSysContentTypeDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cList;
    }

    public List<HrOrganization> getHrOrganization() {

        Query query = null;
        StringBuilder hqlQuery = null;
        HrOrganization hrOrganization = null;
        List<HrOrganization> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select hrOrganization from HrOrganization hrOrganization  ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            list = query.getResultList();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list;
    }

    public List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysConstantAndSysConstantCategoryTO> cList = null;
        Boolean status = null;
        try {
            status = true;
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sc.sysConstantId , sc.sysConstantName, sc.sysConstantCategory.sysConstantCategoryID,sc.sysConstantCategory.sysConstantCategoryName from SysConstant sc  where sc.status=:status");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            query.setParameter("status", status);
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                cList = OrganizationConverter.getSysConstantDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cList;
    }

    public List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysConstantAndSysConstantCategoryTO> cList = null;
        Boolean status = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sc.sysConstantCategoryID,sc.sysConstantCategoryName from SysConstantCategory sc ");
            query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                cList = OrganizationConverter.getSysConstantCategoryDropDown(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cList;
    }


    public List<UiFormTO> getLandingPageList(Long orgID) {
        // TODO Auto-generated method stub

        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<UiFormTO> cList = null;
        Boolean status = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select u.FormName +'('+s.ModuleName+')' ,u.FormID  from uiform u ,"
                    + " SysSubModule s  where URL is not null and IsMenu = '1' and u.SubModuleID = s.ModuleID and"
                    + " FormID in (select SysFormID from hrform where OrganizationID = :orgID )");
            query = getEntityManager("TalentPact").createNativeQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            resultList = query.getResultList();

            if ((resultList != null) && !resultList.isEmpty()) {
                cList = OrganizationConverter.getLandingPageList(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cList;

    }


}
