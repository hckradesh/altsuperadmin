/**
 * 
 */
package com.talentpact.business.dataservice.passwordpolicy;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.passwordPolicy.SysPasswordPolicy;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysPasswordPolicyDS extends AbstractDS<SysPasswordPolicy>
{
    public SysPasswordPolicyDS()
    {
        super(SysPasswordPolicy.class);
    }


    @SuppressWarnings("unchecked")
    public List<SysPasswordPolicy> getPasswordPolicySetting()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPasswordPolicy> passwordPoliciesList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from SysPasswordPolicy s");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            passwordPoliciesList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return passwordPoliciesList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void savePasswordPolicySetting(List<SysPasswordPolicy> passwordPoliciesList, boolean edit, int userID)
    {
        try {
            for (SysPasswordPolicy passwordPolicy : passwordPoliciesList) {
                if (!edit) {
                    passwordPolicy.setCreatedBy(userID);
                    getEntityManager("Talentpact").persist(passwordPolicy);
                } else {
                    passwordPolicy.setCreatedBy(userID);
                    passwordPolicy.setModifiedBy(userID);
                    passwordPolicy.setModifiedDate(new Date());
                    getEntityManager("Talentpact").persist(passwordPolicy);
                }
            }
        } catch (Exception e) {
            throw e;
        }

    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void deletePasswordPolicySetting()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("truncate table SysPasswordPolicy ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            query.executeUpdate();
            getEntityManager("Talentpact").flush();
        } catch (Exception e) {
            throw e;
        }

    }
}