/**
 * 
 */
package com.talentpact.business.dataservice.altbenefits;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.altbenefit.OfferingCode;
import com.talentpact.model.altbenefit.SysOfferingCodeMaximumLimit;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class SysOfferingCodeMaximumLimitDS extends AbstractDS<SysOfferingCodeMaximumLimit>
{
    public SysOfferingCodeMaximumLimitDS()
    {
        super(SysOfferingCodeMaximumLimit.class);
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveOfferingCodeMaximumLimit(OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO, boolean edit)
        throws Exception
    {
        SysOfferingCodeMaximumLimit offeringCodeMaximumLimit=null;
        try {
            if (!edit) {
                 offeringCodeMaximumLimit = new SysOfferingCodeMaximumLimit();
                offeringCodeMaximumLimit.setMaximumUsageCount(offeringCodeMaximumLimitTO.getMaximumUsageCount());
                offeringCodeMaximumLimit.setMaximumUsageDurationID(offeringCodeMaximumLimitTO.getMaximumUsageDurationID());
                offeringCodeMaximumLimit.setMaximumUsageTypeID(offeringCodeMaximumLimitTO.getMaximumUsageTypeID());
                offeringCodeMaximumLimit.setOfferingCodeID(offeringCodeMaximumLimitTO.getOfferingCodeID());
                getEntityManager("Talentpact").persist(offeringCodeMaximumLimit);
            } else {
                if(offeringCodeMaximumLimitTO.getOfferingCodeMaximumLimitID()!=null){
                offeringCodeMaximumLimit=getEntityManager("Talentpact").find(SysOfferingCodeMaximumLimit.class, offeringCodeMaximumLimitTO.getOfferingCodeMaximumLimitID());
                offeringCodeMaximumLimit.setMaximumUsageCount(offeringCodeMaximumLimitTO.getMaximumUsageCount());
                offeringCodeMaximumLimit.setMaximumUsageDurationID(offeringCodeMaximumLimitTO.getMaximumUsageDurationID());
                offeringCodeMaximumLimit.setMaximumUsageTypeID(offeringCodeMaximumLimitTO.getMaximumUsageTypeID());
                offeringCodeMaximumLimit.setOfferingCodeID(offeringCodeMaximumLimitTO.getOfferingCodeID());
                getEntityManager("Talentpact").merge(offeringCodeMaximumLimit);
                getEntityManager("Talentpact").flush();
                }else{
                    offeringCodeMaximumLimit = new SysOfferingCodeMaximumLimit(); offeringCodeMaximumLimit.setMaximumUsageCount(offeringCodeMaximumLimitTO.getMaximumUsageCount());
                    offeringCodeMaximumLimit.setMaximumUsageDurationID(offeringCodeMaximumLimitTO.getMaximumUsageDurationID());
                    offeringCodeMaximumLimit.setMaximumUsageTypeID(offeringCodeMaximumLimitTO.getMaximumUsageTypeID());
                    offeringCodeMaximumLimit.setOfferingCodeID(offeringCodeMaximumLimitTO.getOfferingCodeID());
                    getEntityManager("Talentpact").persist(offeringCodeMaximumLimit);
                }
                
            }
        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * 
     * @param vendorOfferingID
     * @return
     */

    @SuppressWarnings("unchecked")
    public List<OfferingCode> getOfferingCodeListByVendorOffering(Long vendorOfferingID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<OfferingCode> OfferingCodeList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from  OfferingCode s where s.vendorOfferingID=:vendorOfferingID ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("vendorOfferingID", vendorOfferingID);
            OfferingCodeList = query.getResultList();

        } catch (Exception ex) {
            throw ex;
        }
        return OfferingCodeList;
    }

}