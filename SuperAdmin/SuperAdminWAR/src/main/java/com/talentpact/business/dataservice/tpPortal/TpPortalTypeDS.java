/**
 * 
 */
package com.talentpact.business.dataservice.tpPortal;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.auth.entity.TpPortalType;
import com.talentpact.business.dataservice.AbstractDS;

/**
 * @author javed.ali
 *
 */

@Stateless
public class TpPortalTypeDS extends AbstractDS<TpPortalType> {

    public TpPortalTypeDS() {
        super(TpPortalType.class);

    }

    public List<TpPortalType> getPortalDetails()
        throws Exception {

        Query query = null;
        List<TpPortalType> resultList = null;

        try {

            query = getEntityManager().createNamedQuery("TpPortalType.findAll");
            resultList = (List<TpPortalType>) query.getResultList();
            return resultList;

        } catch (Exception e) {

            throw new Exception("Exception occured in getPortalDetails");

        } finally {
            query = null;


        }
    }

    public String findPortalByID(String persistenceUnitName, Long portalID)
        throws Exception {
        Query query = null;
        String result = "";
        TpPortalType tpPortalType = null;
        try {

            query = getEntityManager().createNamedQuery("TpPortalType.findByPortalTypeID");
            query.setParameter("portalTypeID", portalID);
            if (!query.getResultList().isEmpty()) {
                tpPortalType = (TpPortalType) query.getSingleResult();
            }

            result = tpPortalType.getPortalType();


        } catch (Exception e) {

            throw new Exception("Exception occured in getPortalDetails");

        } finally {
            query = null;

        }
        return result;
    }

    public long getPortalID(String persistenceUnitName, String portalName)
        throws Exception {
        long portalId = 0;

        Query query = null;
        TpPortalType tpPortal = null;

        try {

            query = getEntityManager().createNamedQuery("TpPortalType.findByPortalType");

            query.setParameter("portalType", portalName);
            if (!query.getResultList().isEmpty()) {
                tpPortal = (TpPortalType) query.getSingleResult();
            }

            portalId = tpPortal.getPortalTypeID();

        } catch (Exception e) {

            throw new Exception("Exception occured in getPortalID");

        } finally {
            query = null;

        }
        return portalId;
    }

}
