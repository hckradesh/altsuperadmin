package com.talentpact.business.dataservice.hrApp;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.dataservice.tpApp.TpAppService;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class HrAppService {

	private static final Logger LOGGER_ = LoggerFactory.getLogger(TpAppService.class);

	@EJB
	HrAppDS hrAppDS;

	public void saveHrApp(List<HrAppTO> selectedPartnerApps) {
		try {
			for (HrAppTO selectedPartnerApp : selectedPartnerApps) {
				hrAppDS.saveHrApp(selectedPartnerApp);
			}
		} catch (Exception ex) {
			LOGGER_.error(ex.getMessage());
			throw ex;
		}
	}

	public List<HrAppTO> findAllAppForOrg(Long orgId, Long tenantId, Integer appTypeId) {
		List<HrAppTO> list = null;

		try {
			list = hrAppDS.findAllAppForOrg(orgId, tenantId, appTypeId);
		} catch (Exception ex) {
			LOGGER_.error(ex.getMessage());
			throw ex;
		}
		return list;
	}

	public void inactivateHrApp(List<HrAppTO> inactiveApps) {
		try {
			for (HrAppTO inactiveApp : inactiveApps) {
				hrAppDS.updateHrAppStatus(inactiveApp);
			}
		} catch (Exception ex) {
			LOGGER_.error(ex.getMessage());
			throw ex;
		}

	}

	public void createOrUpdateApps(List<HrAppTO> createFinalAppList) {

		try {
			hrAppDS.createOrUpdateApps(createFinalAppList);
		} catch (Exception ex) {
			LOGGER_.error(ex.getMessage());
			throw ex;
		}

	}
}
