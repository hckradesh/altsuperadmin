package com.talentpact.business.dataservice.sysPtZone;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.model.SysState;
import com.talentpact.model.organization.SysPTZone;
import com.talentpact.ui.sysPtZone.to.SysPtZoneTO;


/**
 * 
 * @author prachi.bansal
 *
 */
@Stateless
public class SysPtZoneService extends CommonService{
    
    @EJB
    SysPtZoneDS sysPtZoneDS;
    
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPtZoneService.class);

    public List<SysPtZoneTO> getSysPtZoneTOList() 
            throws Exception{
       
        List<SysPTZone> sysPtZoneList=null;
        List<SysPtZoneTO> sysPtZoneTOList=null;
        sysPtZoneTOList = new ArrayList<SysPtZoneTO>();
        
        try{
            sysPtZoneList= sysPtZoneDS.getSysPtZoneTOList();
            if(sysPtZoneList!=null && !sysPtZoneList.isEmpty())
            {
                for(SysPTZone sysPtZone : sysPtZoneList)
                {
                    SysPtZoneTO sysPtZoneTO=new SysPtZoneTO();
                    sysPtZoneTO.setPtZoneID(sysPtZone.getPtZoneID());
                    sysPtZoneTO.setPtZoneName(sysPtZone.getPtZoneName());
                    sysPtZoneTO.setPtZoneCode(sysPtZone.getPtZoneCode());
                    sysPtZoneTO.setStateID(sysPtZone.getStateIDObj().getStateID());
                    sysPtZoneTO.setStateName(sysPtZone.getStateIDObj().getName());
                    
                    sysPtZoneTOList.add(sysPtZoneTO);
                }
            }
            return sysPtZoneTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        finally{   
        }
    }

    public List<SysPtZoneTO> getSysStateList() 
    throws Exception{
        List<SysState> sysPtZoneList=null;
        List<SysPtZoneTO> sysPtZoneTOList=null;
        sysPtZoneTOList = new ArrayList<SysPtZoneTO>();
        try{
            sysPtZoneList= sysPtZoneDS.getSysStateList();
            if(sysPtZoneList!=null && !sysPtZoneList.isEmpty())
            {
                for(SysState sysPtZone : sysPtZoneList)
                {
                    SysPtZoneTO sysPtZoneTO=new SysPtZoneTO();
                    sysPtZoneTO.setStateID(sysPtZone.getStateID());
                    sysPtZoneTO.setStateName(sysPtZone.getName());
                    sysPtZoneTOList.add(sysPtZoneTO);
                }
            }
            return sysPtZoneTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
        finally{   
        }
    }

    public void saveSysPtZone(SysPtZoneTO sysPtZoneTO) {
       try{
           sysPtZoneDS.saveSysPtZone(sysPtZoneTO);
       }catch (Exception e) {
           LOGGER_.error("", e);
       }
        
    }

    public void updateSysPtZone(SysPtZoneTO sysPtZoneTO) {
       try{
           sysPtZoneDS.updateSysPtZone(sysPtZoneTO);
       }catch (Exception e) {
           LOGGER_.error("", e);
       }
        
    }

    public List<SysPtZoneTO> getUpdatedSysPtZoneTOList(Integer ptZoneID) throws Exception {
        List<SysPTZone> sysPtZoneList=null;
        List<SysPtZoneTO> sysPtZoneTOList=null;
        sysPtZoneTOList = new ArrayList<SysPtZoneTO>();
        
        try{
            sysPtZoneList= sysPtZoneDS.getUpdatedSysPtZoneTOList(ptZoneID);
            if(sysPtZoneList!=null && !sysPtZoneList.isEmpty())
            {
                for(SysPTZone sysPtZone : sysPtZoneList)
                {
                    SysPtZoneTO sysPtZoneTO=new SysPtZoneTO();
                    sysPtZoneTO.setPtZoneID(sysPtZone.getPtZoneID());
                    sysPtZoneTO.setPtZoneName(sysPtZone.getPtZoneName());
                    sysPtZoneTO.setPtZoneCode(sysPtZone.getPtZoneCode());
                    sysPtZoneTO.setStateID(sysPtZone.getStateIDObj().getStateID());
                    sysPtZoneTO.setStateName(sysPtZone.getStateIDObj().getName());
                    
                    sysPtZoneTOList.add(sysPtZoneTO);
                }
            }
            return sysPtZoneTOList;
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        } 
    }
    
}