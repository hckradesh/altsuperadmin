/**
 * 
 */
package com.talentpact.business.tenants.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.talentpact.auth.entity.TpTenant;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.SysTenantDS;
import com.talentpact.business.dataservice.TenantDS;
import com.talentpact.business.tenants.converter.TenantConverter;
import com.talentpact.business.tenants.transport.newtenant.input.NewTenantRequestTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;

/**
 * @author radhamadhab.dalai
 *
 */

@Stateless
public class CreateTenantService
{

    @EJB
    TenantDS tenantDS;
    @EJB
    SysTenantDS sysTenantDS;

    /*method to save a new tenant  */
    public ServiceResponse saveOrUpdateTenant(ServiceRequest serviceRequest)
    {      


        NewTenantRequestTO newRequestTO = null;
        ServiceResponse serviceResponse = null;
        try {
            newRequestTO = (NewTenantRequestTO) serviceRequest.getRequestTansportObject();
            /* Save TpTenant*/
            TpTenant tpTenant =  tenantDS.saveTenant(newRequestTO);
            /* Save SysTenant*/
            sysTenantDS.saveSysTenant(tpTenant);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return serviceResponse;
    }


    /* method to get all tenants  */
    public ServiceResponse getAllTenants(ServiceRequest serviceRequest)
    {
        ServiceResponse serviceResponse = null;
        List<NewTenantResponseTO> result = null;
        NewTenantResponseTO newTenantResponseTO = null;
        try {
            List<TpTenant> tenantList = tenantDS.getAllTenants();
            result = TenantConverter.convertTenantEntityToDataTO(tenantList);
            newTenantResponseTO = new NewTenantResponseTO();
            newTenantResponseTO.setTenantListTo(result);
            serviceResponse = new ServiceResponse();
            serviceResponse.setResponseTransportObject(newTenantResponseTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return serviceResponse;
    }

    public ServiceResponse getTenant(ServiceRequest serviceRequest)
    {

        long tenantID = 0;

        NewTenantRequestTO newRequestTO = null;
        ServiceResponse serviceResponse = null;
        TenantConverter tenantConverter = null;
        NewTenantResponseTO newTenantResponseTO = null;
        try {
            newRequestTO = (NewTenantRequestTO) serviceRequest.getRequestTansportObject();

            tenantID = newRequestTO.getTenantID();
            tenantConverter = new TenantConverter();
            newRequestTO = tenantConverter.convert(tenantDS.getTenant(tenantID));
            serviceResponse = new ServiceResponse();
            newTenantResponseTO = new NewTenantResponseTO();
            newTenantResponseTO.setNewTenantRequestTO(newRequestTO);
            serviceResponse.setResponseTransportObject(newTenantResponseTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return serviceResponse;
    }


}
