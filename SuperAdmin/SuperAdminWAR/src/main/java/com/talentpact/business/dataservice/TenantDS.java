/**
 * 
 */
package com.talentpact.business.dataservice;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.talentpact.auth.entity.TpTenant;
import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.tenants.exception.TenantException;
import com.talentpact.business.tenants.transport.newtenant.input.NewTenantRequestTO;


/**
 * @author radhamadhab.dalai
 *
 */
@Stateless
public class TenantDS extends AbstractDS<TpTenant>
{
    public TenantDS()
    {
        super(TpTenant.class);
    }

    public TpTenant saveTenant(NewTenantRequestTO newTenantRequestTO)
        throws TenantException
    {

        String tenantName = null;
        String tenantCode = null;
        TpUser modifiedBy = null;
        Query query = null;
        StringBuilder hqlQuery = null;
        TpTenant tpTenant = null;
        Long tenantID = null;
        try {
            tenantName = newTenantRequestTO.getTenantName();
            tenantCode = newTenantRequestTO.getTenantCode();
            tenantID = newTenantRequestTO.getTenantID();

            tpTenant = findTpTenant(tenantID);
            if (tpTenant != null) {
                tpTenant.setName(tenantName);
                tpTenant.setTenantCode(tenantCode);
                modifiedBy = getEntityManager().find(TpUser.class, newTenantRequestTO.getModifiedBy());
                tpTenant.setModifiedBy(modifiedBy);
                tpTenant.setEffectiveFrom(newTenantRequestTO.getEffectiveFrom());
                tpTenant.setEffectiveTo(newTenantRequestTO.getEffectiveTo());
                tpTenant.setModifiedDate(newTenantRequestTO.getModifiedDate());
                tpTenant.setContractSignedDate(newTenantRequestTO.getContractSignedDate());
                getEntityManager().merge(tpTenant);
            }

            else {
                tpTenant = new TpTenant();
                tpTenant.setName(tenantName);
                tpTenant.setTenantCode(tenantCode);
                tpTenant.setEffectiveFrom(newTenantRequestTO.getEffectiveFrom());
                tpTenant.setEffectiveTo(newTenantRequestTO.getEffectiveTo());
                modifiedBy = getEntityManager().find(TpUser.class, newTenantRequestTO.getModifiedBy());
                tpTenant.setModifiedBy(modifiedBy);
                tpTenant.setModifiedDate(newTenantRequestTO.getModifiedDate());
                tpTenant.setContractSignedDate(newTenantRequestTO.getContractSignedDate());
                tpTenant.setCreationDate(newTenantRequestTO.getCreatedDate());
                tpTenant.setCreatedBy(modifiedBy);
                getEntityManager().persist(tpTenant);

            }
            getEntityManager().flush();
            return tpTenant;

        } catch (Exception ex) {
            throw new TenantException(ex.getMessage());
        }
    }

    public TpTenant findTpTenant(Long tenantId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpTenant> tpTenantList = null;
        TpTenant result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpTenant from TpTenant tpTenant where tpTenant.tenantID=:tenantID");
            query = getEntityManager().createQuery(hqlQuery.toString());
            query.setParameter("tenantID", tenantId);
            tpTenantList = query.getResultList();
            if (tpTenantList != null && tpTenantList.size() > 0) {
                result = tpTenantList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }


    public List<TpTenant> getAllTenants()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpTenant> tpTenantList = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpTenant from TpTenant tpTenant");
            query = getEntityManager().createQuery(hqlQuery.toString());
            tpTenantList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpTenantList;
    }


    public TpTenant getTenant(long tenantID)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<TpTenant> tpTenantList = null;
        TpTenant result = null;
        try {
            result = getEntityManager().find(TpTenant.class, tenantID);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }


}
