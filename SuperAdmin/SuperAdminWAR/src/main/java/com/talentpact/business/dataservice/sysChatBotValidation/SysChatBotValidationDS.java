package com.talentpact.business.dataservice.sysChatBotValidation;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysChatBotValidation;
import com.talentpact.model.worflow.UiFormField;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.sysChatBotValidation.to.SysChatBotValidationTO;


@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysChatBotValidationDS extends
AbstractDS<SysChatBotValidation> {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationDS.class);

	@Inject
	UserSessionBean userSessionBean;

	public SysChatBotValidationDS() {
		// TODO Auto-generated constructor stub
		super(SysChatBotValidation.class);
	}


	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveSysChatBotValidation(
			SysChatBotValidationTO sysChatBotValidationTO) {
			SysChatBotValidation sysChatBotValidation = null;
		try {
			sysChatBotValidation = new SysChatBotValidation();
			sysChatBotValidation.setSysValidationTypeID(sysChatBotValidationTO.getSysValidationTypeID());
			sysChatBotValidation.setParamValues(sysChatBotValidationTO.getParamValues());
			sysChatBotValidation.setUiFormFieldID(sysChatBotValidationTO.getUiFormFieldID());
			sysChatBotValidation.setCreatedBy(userSessionBean.getUserID().intValue());
			sysChatBotValidation.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
			sysChatBotValidation.setModifiedBy(userSessionBean.getUserID().intValue());
			sysChatBotValidation.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
			sysChatBotValidation.setTenantID(1);
			getEntityManager("TalentPact").persist(sysChatBotValidation);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SysChatBotValidation> getSysChatBotValidationByUiFormFieldID(Integer uiFormFieldID) {
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysChatBotValidation> list = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append(" from SysChatBotValidation sysChatBotValidation where sysChatBotValidation.uiFormFieldID=:uiFormFieldID");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			query.setParameter("uiFormFieldID", uiFormFieldID);
			list = query.getResultList();
			
			return list;
		} catch (Exception e) {
			LOGGER_.error("", e);
			return null;
		}

	}
	
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void deleteSysBotValidation(
			Integer uiformFieldId) {
			Query query = null;
			StringBuilder hqlQuery = null;
		try {
			
			hqlQuery = new StringBuilder();
			hqlQuery.append(" delete from SysChatBotValidation sysChatBotValidation where sysChatBotValidation.uiFormFieldID=:uiFormFieldID");
			query = getEntityManager("Talentpact").createQuery(
					hqlQuery.toString());
			query.setParameter("uiFormFieldID", uiformFieldId);
			query.executeUpdate();
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
	}


} // end of class
