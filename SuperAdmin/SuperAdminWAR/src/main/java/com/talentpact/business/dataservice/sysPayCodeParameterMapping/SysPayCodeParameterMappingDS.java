/**
 * 
 */
package com.talentpact.business.dataservice.sysPayCodeParameterMapping;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.SysPayCodeParameterMappingRequestTO;
import com.talentpact.business.application.transport.output.SysPayCodeParameterMappingResponseTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysPayCodeParameterMappingSimple;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPayCodeParameterMappingDS extends AbstractDS<SysPayCodeParameterMappingSimple>
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPayCodeParameterMappingDS.class);

    public SysPayCodeParameterMappingDS()
    {
        super(SysPayCodeParameterMappingSimple.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMappingList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysPayCodeParameterMappingResponseTO> sysPayCodeParameterMappingList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" spcm.sysPayCodeParameterMappingID, ");
            hqlQuery.append(" spcm.sysParameter.paramId, spcm.sysParameter.paramLabel, ");
            hqlQuery.append(" spcm.sysPayCode.sysPayCodeID, spcm.sysPayCode.sysPayCode ");
            hqlQuery.append(" from SysPayCodeParameterMappingSimple spcm ");
            hqlQuery.append(" order by  spcm.sysPayCodeParameterMappingID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                sysPayCodeParameterMappingList = getSysPayCodeParameterMapping(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sysPayCodeParameterMappingList;
    }


    @SuppressWarnings("unchecked")
    public List<SysPayCodeParameterMappingResponseTO> getSysParameterDropDownList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysPayCodeParameterMappingResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" distinct sp.paramId, sp.paramLabel ");
            hqlQuery.append(" from ");
            hqlQuery.append(" SysParameter sp ");
            hqlQuery.append(" order by sp.paramLabel");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getSysParameter(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }


    @SuppressWarnings("unchecked")
    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeDropDownList()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysPayCodeParameterMappingResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" distinct spc.sysPayCodeID, spc.sysPayCode ");
            hqlQuery.append(" from ");
            hqlQuery.append(" SysPayCode spc ");
            hqlQuery.append(" order by spc.sysPayCode");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getSysPayCode(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMapping(List<Object[]> resultList)
    {
        List<SysPayCodeParameterMappingResponseTO> list = null;
        SysPayCodeParameterMappingResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysPayCodeParameterMappingResponseTO();
                if (object[0] != null) {
                    responseTO.setSysPayCodeParameterMappingID((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setSysParameterID((Integer) object[1]);
                }
                if (object[2] != null) {
                    responseTO.setSysParameterName((String) object[2]);
                }

                if (object[3] != null) {
                    responseTO.setSysPayCodeID((Integer) object[3]);
                }
                if (object[4] != null) {
                    responseTO.setSysPayCodeName((String) object[4]);
                }


                responseTO.setSysPayCodeParameterMappingName((String) object[2] + "-" + (String) object[4]);


                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }


    public static List<SysPayCodeParameterMappingResponseTO> getSysPayCode(List<Object[]> resultList)
    {
        List<SysPayCodeParameterMappingResponseTO> list = null;
        SysPayCodeParameterMappingResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysPayCodeParameterMappingResponseTO();
                if (object[0] != null) {
                    responseTO.setSysPayCodeID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysPayCodeName((String) object[1]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }


    public static List<SysPayCodeParameterMappingResponseTO> getSysParameter(List<Object[]> resultList)
    {
        List<SysPayCodeParameterMappingResponseTO> list = null;
        SysPayCodeParameterMappingResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysPayCodeParameterMappingResponseTO();
                if (object[0] != null) {
                    responseTO.setSysParameterID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setSysParameterName((String) object[1]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void saveSysPayCodeParameterMapping(SysPayCodeParameterMappingRequestTO sysPayCodeParameterMappingRequestTO)
    {
        SysPayCodeParameterMappingSimple sysPayCodeParameterMapping = null;
        try {
            sysPayCodeParameterMapping = findSysPayCodeParameterMapping(sysPayCodeParameterMappingRequestTO);

            if (sysPayCodeParameterMapping == null) {
                sysPayCodeParameterMapping = new SysPayCodeParameterMappingSimple();

                sysPayCodeParameterMapping.setSysPayCodeID(sysPayCodeParameterMappingRequestTO.getSysPayCodeID());
                sysPayCodeParameterMapping.setSysParameterID(sysPayCodeParameterMappingRequestTO.getSysParameterID());
                sysPayCodeParameterMapping.setCreatedBy(1);
                sysPayCodeParameterMapping.setModifiedBy(1);
                sysPayCodeParameterMapping.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
                sysPayCodeParameterMapping.setModifiedDate(SQLDateHelper.getSqlTimeStamp());

                getEntityManager("TalentPact").persist(sysPayCodeParameterMapping);
            } else {
                sysPayCodeParameterMapping.setSysPayCodeID(sysPayCodeParameterMappingRequestTO.getSysPayCodeID());
                sysPayCodeParameterMapping.setSysParameterID(sysPayCodeParameterMappingRequestTO.getSysParameterID());
                sysPayCodeParameterMapping.setSysPayCodeParameterMappingID(sysPayCodeParameterMappingRequestTO.getSysPayCodeParameterMappingID());
                sysPayCodeParameterMapping.setModifiedBy(1);
                sysPayCodeParameterMapping.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
                getEntityManager("TalentPact").merge(sysPayCodeParameterMapping);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @SuppressWarnings("unchecked")
    private SysPayCodeParameterMappingSimple findSysPayCodeParameterMapping(SysPayCodeParameterMappingRequestTO sysPayCodeParameterMappingRequestTO)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysPayCodeParameterMappingSimple> list = null;
        SysPayCodeParameterMappingSimple result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select s from SysPayCodeParameterMappingSimple s  where s.sysPayCodeParameterMappingID=:sysPayCodeParameterMappingID ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("sysPayCodeParameterMappingID", sysPayCodeParameterMappingRequestTO.getSysPayCodeParameterMappingID());
            list = query.getResultList();
            if (list != null && list.size() > 0) {
                result = list.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

  public List<Integer> getSysPayCodeParameterMappingIdsFromHrPayrollParameterPolicy()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Integer> list = null;
        SysPayCodeParameterMappingSimple result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select SysPayCodeParameterMappingID from HrPayrollParameterPolicy ");
            query = getEntityManager("TalentPact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();

            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

}
