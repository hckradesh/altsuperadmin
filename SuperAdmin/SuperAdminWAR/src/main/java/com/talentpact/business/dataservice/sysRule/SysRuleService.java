/**
 * 
 */

package com.talentpact.business.dataservice.sysRule;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.ui.rule.to.RuleListRequestTO;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 * 
 */
@Stateless
public class SysRuleService extends CommonService
{

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysRuleService.class);

    @EJB
    SysRuleCache sysRuleCache;

    @EJB
    SysRuleDS sysRuleDS;

    public List<RuleResponseTO> getSysRuleListByTenant()
        throws Exception
    {
        List<RuleResponseTO> ruleList = null;
        try {
            ruleList = sysRuleCache.getSysRuleListByTenant();
            if (ruleList == null) {
                LOGGER_.info("Data not found in Redis. Moving to DB");
                throw new Exception("Data not found in Redis. Moving to DB");
            }

            return ruleList;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                ruleList = sysRuleDS.getSysRuleListByTenant();
                if (ruleList != null && !ruleList.isEmpty()) {
                    try {
                        sysRuleCache.persistSysRuleListByTenant(ruleList);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistSysRuleListByTenantId");
                        LOGGER_.error(ex.getMessage());
                    }
                }

                return ruleList;
            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }


    public List<RuleResponseTO> getRuleTypeIDList()
        throws Exception
    {
        return sysRuleDS.getRuleTypeIDList();

    }

    public void setSysRuleListByTenant(RuleListRequestTO ruleTO)
        throws Exception
    {
        try {
            sysRuleDS.setSysRuleListByTenant(ruleTO);
            sysRuleCache.removeSysRuleListByTenant();
        } catch (Exception e) {

        }
    }
}
