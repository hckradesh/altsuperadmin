package com.talentpact.business.dataservice.SysWorkflowType;

import java.util.List;

import javax.ejb.Stateless;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.model.SysWorkflowType_Revamp;

@Stateless
public class SysWorkflowTypeDS extends AbstractDS<SysWorkflowType_Revamp> {

    public SysWorkflowTypeDS() {
        super(SysWorkflowType_Revamp.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysWorkflowTypeList() {
        StringBuilder qlString = new StringBuilder();
        qlString.append("select sysWorkflowType.workflowTypeID, sysWorkflowType.workflowType");
        qlString.append(" , sysSubModule.moduleId, sysSubModule.moduleName");
        qlString.append(" from SysWorkflowType_Revamp sysWorkflowType join sysWorkflowType.sysSubModule sysSubModule");
        qlString.append(" order by sysSubModule.moduleName, sysWorkflowType.workflowType asc");
        return getEntityManager("TalentPact").createQuery(qlString.toString()).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysWorkflowStageTypeList() {
        StringBuilder qlString = new StringBuilder();
        qlString.append("select sysWorkflowStageType.workflowStageTypeID , sysWorkflowStageType.workflowStageType");
        qlString.append(" , uiForm.formId , uiForm.formName , sysWorkflowType.workflowTypeID ");
        qlString.append(" , sysWorkflowStageTypeGroup.workflowStageTypeGroupId , sysWorkflowStageTypeGroup.workflowStageTypeGroupName");
        qlString.append(" from SysWorkflowStageType_Revamp sysWorkflowStageType join sysWorkflowStageType.uiForm uiForm");
        qlString.append(" join sysWorkflowStageType.sysWorkflowType sysWorkflowType");
        qlString.append(" left join sysWorkflowStageType.sysWorkflowStageTypeGroup sysWorkflowStageTypeGroup");
        qlString.append(" order by uiForm.formName , sysWorkflowStageType.workflowStageType asc");
        return getEntityManager("TalentPact").createQuery(qlString.toString()).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getSysWorkflowStageTypeGroupList() {
        StringBuilder qlString = new StringBuilder();
        qlString.append(" select sysWorkflowStageTypeGroup.workflowStageTypeGroupId , sysWorkflowStageTypeGroup.workflowStageTypeGroupName , sysWorkflowStageTypeGroup.workflowTypeId");
        qlString.append(" from SysWorkflowStageTypeGroup sysWorkflowStageTypeGroup");
        return getEntityManager("TalentPact").createQuery(qlString.toString()).getResultList();
    }
}
