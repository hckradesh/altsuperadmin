package com.talentpact.business.dataservice.UiForm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.alt.user.transport.input.UserRequestTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.business.application.transport.output.SysResourceBundleNewResponseTO;
import com.talentpact.business.application.transport.output.SysResourceBundleResponseTO;
import com.talentpact.business.application.transport.output.SysResourceTO;
import com.talentpact.business.application.transport.output.SysRuleResponseTO;
import com.talentpact.business.application.transport.output.UIFormTO;
import com.talentpact.business.application.transport.output.UiFormFieldGroupResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.cache.scheduler.RedisSchedulerHelper;
import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.common.transport.IResponseTransportObject;
import com.talentpact.business.dataservice.UiForm.cache.SysMenuCache;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.menu.transport.MenuResponseTO;
import com.talentpact.ui.menu.transport.MenuTO;

/**
 * 
 * @author Rahul.Chabba
 *
 */

@Stateless
public class UiFormService extends CommonService {

    @EJB
    UiFormDS uiFormDS;

    @Inject
    UserSessionBean userSessionBean;

    @EJB
    SysMenuCache sysMenuCache;

    public List<UiFormResponseTO> getDataList()
        throws Exception {
        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;

        try {
            uiFormList = uiFormDS.getDataList();
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();

                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setFormID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setFormName(uiFormDataResult[1].toString());
                }
                if (uiFormDataResult[2] != null) {
                    uiFormResponseTO.setPrimaryEntityID((Integer) uiFormDataResult[2]);
                }
                if (uiFormDataResult[3] != null) {
                    uiFormResponseTO.setPrimaryEntityName(uiFormDataResult[3].toString());
                }
                if (uiFormDataResult[4] != null) {
                    uiFormResponseTO.setUrl(uiFormDataResult[4].toString());
                }
                if (uiFormDataResult[5] != null) {
                    uiFormResponseTO.setSubModuleID((Integer) uiFormDataResult[5]);
                }
                if (uiFormDataResult[6] != null) {
                    uiFormResponseTO.setParentFormID((Integer) uiFormDataResult[6]);
                }
                if (uiFormDataResult[7] != null) {
                    uiFormResponseTO.setParentFormName(uiFormDataResult[7].toString());
                }
                if (uiFormDataResult[8] != null) {
                    uiFormResponseTO.setCustomComponentAllowed((Boolean) uiFormDataResult[8]);
                }
                if (uiFormDataResult[9] != null) {
                    uiFormResponseTO.setMenu((Boolean) uiFormDataResult[9]);
                }
                if (uiFormDataResult[10] != null) {
                    uiFormResponseTO.setResourceId((Integer) uiFormDataResult[10]);
                }
                if (uiFormDataResult[11] != null) {
                    uiFormResponseTO.setSequence((Integer) uiFormDataResult[11]);
                }
                if (uiFormDataResult[12] != null) {
                    uiFormResponseTO.setIconPath(uiFormDataResult[12].toString());
                }
                if (uiFormDataResult[13] != null) {
                    uiFormResponseTO.setMenuCategory((Integer) uiFormDataResult[13]);
                }
                if (uiFormDataResult[14] != null) {
                    uiFormResponseTO.setDefaultClassCall(uiFormDataResult[14].toString());
                }

                if (uiFormDataResult[15] != null) {
                    uiFormResponseTO.setFloatingMenuIconPath(uiFormDataResult[15].toString());
                }

                if (uiFormDataResult[16] != null) {
                    uiFormResponseTO.setInactiveIconPath(uiFormDataResult[16].toString());
                }

                if (uiFormDataResult[17] != null) {
                    uiFormResponseTO.setSubModuleName(uiFormDataResult[17].toString());
                }
                if (uiFormDataResult[18] != null) {
                    uiFormResponseTO.setModuleName(uiFormDataResult[18].toString());
                }
                if (uiFormDataResult[19] != null) {
                    uiFormResponseTO.setResourceName(uiFormDataResult[19].toString());
                }
                if (uiFormDataResult[20] != null) {
                    uiFormResponseTO.setResourceDiscription(uiFormDataResult[20].toString());
                }
                if (uiFormDataResult[21] != null) {
                    uiFormResponseTO.setFragmentClass(uiFormDataResult[21].toString());
                }
                if (uiFormDataResult[22] != null) {
                    uiFormResponseTO.setIosSideMenu(uiFormDataResult[22].toString());
                }
                if (uiFormDataResult[23] != null) {
                    uiFormResponseTO.setDisableUrl(uiFormDataResult[23].toString());
                }
                if (uiFormDataResult[24] != null) {
                    uiFormResponseTO.setDisableText(uiFormDataResult[24].toString());
                }
                if (uiFormDataResult[25] != null) {
                    uiFormResponseTO.setDisableBean(uiFormDataResult[25].toString());
                }

                if (uiFormResponseTO.getModuleName() == null) {
                    uiFormResponseTO.setModuleName(uiFormResponseTO.getSubModuleName());
                    uiFormResponseTO.setSubModuleName(null);
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<UiFormResponseTO> getSubModuleList(int ModuleId)
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getSubModuleList(ModuleId);
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setSubModuleID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setSubModuleName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }


    public List<UiFormResponseTO> getModuleList()
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getModuleList();
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setSubModuleID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setSubModuleName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public List<UiFormResponseTO> getSysEntityList()
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getSysEntityList();
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setPrimaryEntityID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setPrimaryEntityName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormResponseTO> getParentFormList()
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getParentFormList();
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setParentFormID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setParentFormName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public boolean saveFormDatanew(UiFormResponseTO uiFormResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormResponseTO.setCreatedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormResponseTO.setCreatedDate(new Date());
            uiFormResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormResponseTO.setModifiedDate(new Date());
            result = uiFormDS.saveFormDataNew(uiFormResponseTO);

            String patter = "ALTORGNIZE:MENU";
            String floatingLink = "ALTORGANIZE:HRIMPORTANT_LINK";
            RedisClient.deleteByPattern(patter + ":" + "*");
            RedisClient.deleteByPattern(floatingLink + ":" + "*");
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormFieldGroupResponseTO> getFieldGroupData(Integer formID)
        throws Exception {
        List<UiFormFieldGroupResponseTO> list = null;
        UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getFieldGroupData(formID);
            list = new ArrayList<UiFormFieldGroupResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormFieldGroupResponseTO = new UiFormFieldGroupResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormFieldGroupResponseTO.setFieldGroupID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormFieldGroupResponseTO.setFieldGroupName(uiFormDataResult[1].toString());
                }
                if (uiFormDataResult[2] != null) {
                    uiFormFieldGroupResponseTO.setFormID((Integer) uiFormDataResult[2]);
                }
                list.add(uiFormFieldGroupResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean updateFormDatanew(UiFormResponseTO uiFormResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormResponseTO.setModifiedDate(new Date());
            result = uiFormDS.updateFormDataNew(uiFormResponseTO);
            String patter = "ALTORGNIZE:MENU";
            String floatingLink = "ALTORGANIZE:HRIMPORTANT_LINK";
            RedisClient.deleteByPattern(patter + ":" + "*");
            RedisClient.deleteByPattern(floatingLink + ":" + "*");
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean saveFormGroupData(UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormFieldGroupResponseTO.setCreatedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldGroupResponseTO.setCreatedDate(new Date());
            result = uiFormDS.saveFormDataNew(uiFormFieldGroupResponseTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public List<UiFormFieldResponseTO> getFormGroupFieldData(Integer fieldGroupId)
        throws Exception {
        List<UiFormFieldResponseTO> list = null;
        UiFormFieldResponseTO uiFormFieldResponseTO = null;
        List<Object[]> uiFormFieldList = null;
        try {
            uiFormFieldList = uiFormDS.getFormGroupFieldData(fieldGroupId);
            list = new ArrayList<UiFormFieldResponseTO>();
            for (Object[] uiFormfieldDataResult : uiFormFieldList) {
                uiFormFieldResponseTO = new UiFormFieldResponseTO();
                if (uiFormfieldDataResult[0] != null) {
                    uiFormFieldResponseTO.setFormFieldID((Integer) uiFormfieldDataResult[0]);
                }

                if (uiFormfieldDataResult[1] != null) {
                    uiFormFieldResponseTO.setFieldGroupID((Integer) uiFormfieldDataResult[1]);
                }
                if (uiFormfieldDataResult[2] != null) {
                    //uiFormFieldResponseTO.setEntityFieldID((Integer) uiFormfieldDataResult[2]);
                    uiFormFieldResponseTO.setEntityFieldID(Integer.parseInt(String.valueOf(uiFormfieldDataResult[2])));

                }
                if (uiFormfieldDataResult[3] != null) {
                    uiFormFieldResponseTO.setOrganizationID((Integer) uiFormfieldDataResult[3]);
                }
                if (uiFormfieldDataResult[4] != null) {
                    uiFormFieldResponseTO.setClientID(uiFormfieldDataResult[4].toString());
                }
                if (uiFormfieldDataResult[5] != null) {
                    uiFormFieldResponseTO.setTenantID((Integer) uiFormfieldDataResult[5]);
                }
                if (uiFormfieldDataResult[6] != null) {
                    if ((Boolean) uiFormfieldDataResult[6])
                        uiFormFieldResponseTO.setAction(true);
                    else
                        uiFormFieldResponseTO.setAction(false);
                }
                /*if(uiFormfieldDataResult[7]!= null){
                    if((Boolean)uiFormfieldDataResult[7])
                    uiFormFieldResponseTO.setCutomfieldRequired(true);
                    else
                        uiFormFieldResponseTO.setCutomfieldRequired(false);
                }*/
                if (uiFormfieldDataResult[7] != null) {
                    if ((Boolean) uiFormfieldDataResult[7])
                        uiFormFieldResponseTO.setApprovalRequired(true);
                    else
                        uiFormFieldResponseTO.setApprovalRequired(false);
                }
                if (uiFormfieldDataResult[8] != null) {
                    if ((Boolean) uiFormfieldDataResult[8])
                        uiFormFieldResponseTO.setEffectiveDateRequired(true);
                    else
                        uiFormFieldResponseTO.setEffectiveDateRequired(false);
                }
                if (uiFormfieldDataResult[9] != null) {
                    uiFormFieldResponseTO.setResourceID((Integer) uiFormfieldDataResult[9]);
                }
                if (uiFormfieldDataResult[10] != null) {
                    if ((Boolean) uiFormfieldDataResult[10])
                        uiFormFieldResponseTO.setMandatory(true);
                    else
                        uiFormFieldResponseTO.setMandatory(false);
                }
                if (uiFormfieldDataResult[11] != null) {
                    uiFormFieldResponseTO.setParentFormFieldID((Integer) uiFormfieldDataResult[11]);
                }
                if (uiFormfieldDataResult[12] != null) {
                    if ((Boolean) uiFormfieldDataResult[12])
                        uiFormFieldResponseTO.setAttachmentRequired(true);
                    else
                        uiFormFieldResponseTO.setAttachmentRequired(false);
                }
                if (uiFormfieldDataResult[13] != null) {
                    uiFormFieldResponseTO.setRegex(uiFormfieldDataResult[13].toString());
                }
                if (uiFormfieldDataResult[14] != null) {
                    uiFormFieldResponseTO.setSysEntityField((String) uiFormfieldDataResult[14]);
                }
                if (uiFormfieldDataResult[15] != null) {
                    uiFormFieldResponseTO.setSysResourceValue(uiFormfieldDataResult[15].toString());
                }
                if (uiFormfieldDataResult[16] != null) {
                    if ((Boolean) uiFormfieldDataResult[16])
                        uiFormFieldResponseTO.setMaster(true);
                    else
                        uiFormFieldResponseTO.setMaster(false);
                }
                if (uiFormfieldDataResult[17] != null) {
                    if ((Boolean) uiFormfieldDataResult[17])
                        uiFormFieldResponseTO.setAttachmentEnabled(true);
                    else
                        uiFormFieldResponseTO.setAttachmentEnabled(false);
                }
                if (uiFormfieldDataResult[21] != null) {
                    uiFormFieldResponseTO.setParentClientId(uiFormfieldDataResult[21].toString());
                }
                if (uiFormfieldDataResult[19] != null) {
                    uiFormFieldResponseTO.setConfigurationValueMobile(uiFormfieldDataResult[19].toString());
                }
                if (uiFormfieldDataResult[18] != null) {
                    uiFormFieldResponseTO.setConfigurationValueWeb(uiFormfieldDataResult[18].toString());
                }
                if (uiFormfieldDataResult[20] != null) {
                    if ((Boolean) uiFormfieldDataResult[20])
                        uiFormFieldResponseTO.setGroupParameter(true);
                    else
                        uiFormFieldResponseTO.setGroupParameter(false);
                }

                if (uiFormfieldDataResult[22] != null) {
                    uiFormFieldResponseTO.setSysFieldTypeID((Integer) uiFormfieldDataResult[22]);
                }

                if (uiFormfieldDataResult[23] != null) {
                    uiFormFieldResponseTO.setRuleID((Integer) uiFormfieldDataResult[23]);
                }

                if (uiFormfieldDataResult[24] != null) {
                    uiFormFieldResponseTO.setFieldType((String) uiFormfieldDataResult[24]);
                }

                if (uiFormfieldDataResult[25] != null) {
                    uiFormFieldResponseTO.setRuleMethod((String) uiFormfieldDataResult[25]);
                }
                if (uiFormfieldDataResult[26] != null) {
                    uiFormFieldResponseTO.setChatBotResourceID((Integer) uiFormfieldDataResult[26]);
                }

                if (uiFormfieldDataResult[27] != null) {
                    uiFormFieldResponseTO.setConfigurationValueChatBot((String) uiFormfieldDataResult[27]);
                }

                if (uiFormfieldDataResult[28] != null) {
                    uiFormFieldResponseTO.setChatBotSequence((Integer) uiFormfieldDataResult[28]);
                }

                list.add(uiFormFieldResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public int saveFormFieldData(UiFormFieldResponseTO uiFormFieldResponseTO)
        throws Exception {

        try {
            uiFormFieldResponseTO.setCreatedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldResponseTO.setCreatedDate(new Date());
            uiFormFieldResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldResponseTO.setModifiedDate(new Date());
            return uiFormDS.saveFormFieldData(uiFormFieldResponseTO);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean updateFieldData(UiFormFieldResponseTO uiFormFieldResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormFieldResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldResponseTO.setModifiedDate(new Date());
            result = uiFormDS.updateFieldData(uiFormFieldResponseTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<SysBundleResponseTO> getSysBundle()
        throws Exception {
        List<SysBundleResponseTO> list = null;
        SysBundleResponseTO sysBundleResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getSysBundle();
            list = new ArrayList<SysBundleResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                sysBundleResponseTO = new SysBundleResponseTO();
                if (uiFormDataResult[0] != null) {
                    sysBundleResponseTO.setBundleId((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    sysBundleResponseTO.setCountry(uiFormDataResult[1].toString());
                }
                if (uiFormDataResult[2] != null) {
                    sysBundleResponseTO.setLanguage(uiFormDataResult[2].toString());
                }
                if (uiFormDataResult[3] != null) {
                    sysBundleResponseTO.setName(uiFormDataResult[3].toString());
                }
                if (uiFormDataResult[4] != null) {
                    sysBundleResponseTO.setSysTenant((Integer) uiFormDataResult[4]);
                }
                if (uiFormDataResult[5] != null) {
                    sysBundleResponseTO.setVariant(uiFormDataResult[5].toString());
                }

                list.add(sysBundleResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean insertToSysResourceBundleNew(String nameOFLang, Integer bundleId, String textBoxData, Integer resourceID, String textBoxDataTitle)
        throws Exception {

        try {
            SysResourceBundleResponseTO sysResourceBundleResponseTO = new SysResourceBundleResponseTO();
            boolean result = false;

            sysResourceBundleResponseTO.setResourceID(resourceID);
            sysResourceBundleResponseTO.setBundleID(bundleId);
            sysResourceBundleResponseTO.setValue(textBoxData != "" ? textBoxData : null);
            sysResourceBundleResponseTO.setTenantID(1);
            sysResourceBundleResponseTO.setCreatedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            sysResourceBundleResponseTO.setCreatedDate(new Date());
            sysResourceBundleResponseTO.setTitle(textBoxDataTitle != "" ? textBoxDataTitle : null);
            sysResourceBundleResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            sysResourceBundleResponseTO.setModifiedDate(new Date());
            result = uiFormDS.insertToSysResourceBundleNew(sysResourceBundleResponseTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean insertToSysResource(Integer bundleId, String clientIdtoSet)
        throws Exception {
        // TODO Auto-generated method stub


        try {
            SysResourceTO sysResourceTO = new SysResourceTO();
            boolean result = false;

            sysResourceTO.setDescription(clientIdtoSet);
            sysResourceTO.setBundleID(bundleId);
            sysResourceTO.setTenant(1);
            sysResourceTO.setValue(clientIdtoSet);

            result = uiFormDS.insertToSysResource(sysResourceTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public int insertToSysResourceForm(Integer bundleId, String clientIdtoSet, String resourceDiscription)
        throws Exception {
        // TODO Auto-generated method stub


        try {
            SysResourceTO sysResourceTO = new SysResourceTO();
            int result = 0;

            sysResourceTO.setDescription(resourceDiscription);
            sysResourceTO.setBundleID(bundleId);
            sysResourceTO.setTenant(1);
            sysResourceTO.setValue(clientIdtoSet);

            result = uiFormDS.insertToSysResourceForm(sysResourceTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public Integer getresourceIdToPutInSysResourceBundle(Integer bundleId, String clientIdtoSet)
        throws Exception {
        // TODO Auto-generated method stub


        try {
            SysResourceTO sysResourceTO = new SysResourceTO();
            Integer result = null;

            sysResourceTO.setDescription(clientIdtoSet);
            sysResourceTO.setBundleID(bundleId);
            sysResourceTO.setTenant(1);
            sysResourceTO.setValue(clientIdtoSet);

            result = uiFormDS.getresourceIdToPutInSysResourceBundle(sysResourceTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public boolean updateFormGroupData(UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormFieldGroupResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldGroupResponseTO.setModifiedDate(new Date());
            result = uiFormDS.updateFormDataNew(uiFormFieldGroupResponseTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormResponseTO> getParentFormListByComType(int subModuleID, int moduleID)
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getParentFormListByComType(subModuleID, moduleID);
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setParentFormID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setParentFormName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public Integer checkCombinationofBundleIdandResource(Integer bundleId, Integer resourceID)
        throws Exception {

        try {
            Integer result = 0;

            result = uiFormDS.checkCombinationofBundleIdandResource(bundleId, resourceID);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;

        }

    }

    public boolean mergeToSysResourceBundleNew(String nameOFLang, Integer bundleId, String textboxdata, Integer resourceID, Integer checkCombinationResult, String textBoxDataTitle)
        throws Exception {

        try {
            boolean result = false;
            Integer CreatedBy = Integer.parseInt(String.valueOf(userSessionBean.getUserID()));
            Date CreatedDate = new Date();
            Date ModifiedDate = new Date();
            Integer ModifiedBy = Integer.parseInt(String.valueOf(userSessionBean.getUserID()));


            result = uiFormDS.mergeToSysResourceBundleNew(nameOFLang, bundleId, textboxdata, resourceID, checkCombinationResult, CreatedBy, CreatedDate, ModifiedDate, ModifiedBy,
                    textBoxDataTitle);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public List<SysResourceBundleNewResponseTO> getSysBundleDataForTextBox(Integer resourceID)
        throws Exception {

        List<SysResourceBundleNewResponseTO> list = null;
        SysResourceBundleNewResponseTO resourceBundleResponseTO = null;
        List<Object[]> resourceBundleResponseTOList = null;
        try {
            resourceBundleResponseTOList = uiFormDS.getSysBundleDataForTextBox(resourceID);
            list = new ArrayList<SysResourceBundleNewResponseTO>();
            for (Object[] resourceFormDataResult : resourceBundleResponseTOList) {
                resourceBundleResponseTO = new SysResourceBundleNewResponseTO();
                if (resourceFormDataResult[0] != null) {
                    resourceBundleResponseTO.setSysResourceBundleId(((BigInteger) resourceFormDataResult[0]).intValue());
                }
                if (resourceFormDataResult[1] != null) {
                    resourceBundleResponseTO.setResourceId((Integer) resourceFormDataResult[1]);
                }
                if (resourceFormDataResult[2] != null) {
                    resourceBundleResponseTO.setBundleId((Integer) resourceFormDataResult[2]);
                }

                if (resourceFormDataResult[3] != null) {
                    resourceBundleResponseTO.setValue(resourceFormDataResult[3].toString());
                }

                if (resourceFormDataResult[4] != null) {
                    resourceBundleResponseTO.setTitle(resourceFormDataResult[4].toString());
                }

                list.add(resourceBundleResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean UpdateFormFieldData(UiFormFieldResponseTO uiFormFieldResponseTO)
        throws Exception {

        try {
            boolean result = false;
            uiFormFieldResponseTO.setModifiedBy(Integer.parseInt(String.valueOf(userSessionBean.getUserID())));
            uiFormFieldResponseTO.setModifiedDate(new Date());
            result = uiFormDS.UpdateFormFieldData(uiFormFieldResponseTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormFieldResponseTO> getParentFormFieldData(Integer fieldGroupID)
        throws Exception {

        List<UiFormFieldResponseTO> list = null;
        UiFormFieldResponseTO uiFormFieldResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getParentFormFieldData(fieldGroupID);
            list = new ArrayList<UiFormFieldResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormFieldResponseTO = new UiFormFieldResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormFieldResponseTO.setFormFieldID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormFieldResponseTO.setClientID(uiFormDataResult[1].toString());
                }
                list.add(uiFormFieldResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormFieldResponseTO> getSysResourceData()
        throws Exception {

        List<UiFormFieldResponseTO> list = null;
        UiFormFieldResponseTO uiFormFieldResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getSysResourceData();
            list = new ArrayList<UiFormFieldResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormFieldResponseTO = new UiFormFieldResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormFieldResponseTO.setResourceID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormFieldResponseTO.setSysResourceValue(uiFormDataResult[1].toString());
                }
                list.add(uiFormFieldResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public List<UiFormFieldResponseTO> getEntityFieldData(Integer formId)
        throws Exception {

        List<UiFormFieldResponseTO> list = null;
        UiFormFieldResponseTO uiFormFieldResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getEntityFieldData(formId);
            list = new ArrayList<UiFormFieldResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormFieldResponseTO = new UiFormFieldResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormFieldResponseTO.setEntityFieldID(((BigInteger) uiFormDataResult[0]).intValue());
                }
                if (uiFormDataResult[1] != null) {
                    uiFormFieldResponseTO.setSysEntityField(uiFormDataResult[1].toString());
                }
                list.add(uiFormFieldResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean groupnameExists(String fieldGroupName, Integer formId, Integer groupId)
        throws Exception {

        try {
            boolean result = false;
            result = uiFormDS.groupnameExists(fieldGroupName, formId, groupId);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean clientExists(String clientID, Integer formFieldID)
        throws Exception {

        try {
            boolean result = false;
            result = uiFormDS.clientExists(clientID, formFieldID);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public boolean clientExistsExcel(String clientID, Integer groupId)
        throws Exception {

        try {
            boolean result = false;
            result = uiFormDS.clientExistsExcel(clientID, groupId);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public boolean formNameExists(String formName, Integer formID, int moduleSubmoduleID)
        throws Exception {

        try {
            boolean result = false;
            result = uiFormDS.formNameExists(formName, formID, moduleSubmoduleID);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public String earlierformName(Integer formID)
        throws Exception {

        try {
            String result;
            result = uiFormDS.earlierformName(formID);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }


    public int getresourceIDSysResource(String formName)
        throws Exception {

        try {
            Integer result = 0;
            result = uiFormDS.getresourceIDSysResource(formName);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public Map<String, Integer> getGroupNameForExcel(Integer formId)
        throws Exception {
        Map<String, Integer> map = null;
        List<Object[]> uiFormList = null;

        try {
            uiFormList = uiFormDS.getGroupNameForExcel(formId);
            map = new HashMap<String, Integer>();
            for (Object[] uiFormDataResult : uiFormList) {
                String dbname = null;
                Integer entityId = null;

                if (uiFormDataResult[0] != null) {
                    dbname = (uiFormDataResult[0].toString());
                }
                if (uiFormDataResult[1] != null) {
                    entityId = ((Integer) uiFormDataResult[1]);
                }
                map.put(dbname, entityId);
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }


    public Map<String, Integer> getEntityNameForExcel(Integer formId)
        throws Exception {

        Map<String, Integer> map = null;
        List<Object[]> uiFormList = null;

        try {
            uiFormList = uiFormDS.getEntityNameForExcel(formId);
            map = new HashMap<String, Integer>();
            for (Object[] uiFormDataResult : uiFormList) {
                String dbname = null;
                Integer entityId = null;

                if (uiFormDataResult[0] != null) {
                    dbname = (uiFormDataResult[0].toString());
                }
                if (uiFormDataResult[1] != null) {
                    entityId = ((BigInteger) uiFormDataResult[1]).intValue();
                }
                map.put(dbname, entityId);
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public int getParentidforExcel(String stringCellValue, Integer groupIdOfCurrentIteration)
        throws Exception {

        try {
            Integer result = 0;
            result = uiFormDS.getParentidforExcel(stringCellValue, groupIdOfCurrentIteration);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public String getFormNameForExcel(Integer formId)
        throws Exception {

        String formName = null;
        List<String[]> uiFormList = null;

        try {
            formName = uiFormDS.getFormNameForExcel(formId);

            return formName;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public int UpdateToSysResourceForm(int i, String resourceName, String resourceDiscription, int resourceIDSysResource)
        throws Exception {
        // TODO Auto-generated method stub


        try {
            SysResourceTO sysResourceTO = new SysResourceTO();
            int result = 0;

            sysResourceTO.setDescription(resourceDiscription);
            sysResourceTO.setBundleID(i);
            sysResourceTO.setTenant(1);
            sysResourceTO.setValue(resourceName);
            sysResourceTO.setResourceId(resourceIDSysResource);

            result = uiFormDS.UpdateToSysResourceForm(sysResourceTO);

            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public List<UiFormResponseTO> getSubModuleListUpdate(int subModuleID)
        throws Exception {

        List<UiFormResponseTO> list = null;
        UiFormResponseTO uiFormResponseTO = null;
        List<Object[]> uiFormList = null;
        try {
            uiFormList = uiFormDS.getSubModuleListUpdate(subModuleID);
            list = new ArrayList<UiFormResponseTO>();
            for (Object[] uiFormDataResult : uiFormList) {
                uiFormResponseTO = new UiFormResponseTO();
                if (uiFormDataResult[0] != null) {
                    uiFormResponseTO.setSubModuleID((Integer) uiFormDataResult[0]);
                }
                if (uiFormDataResult[1] != null) {
                    uiFormResponseTO.setSubModuleName(uiFormDataResult[1].toString());
                }
                list.add(uiFormResponseTO);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public Integer getMaxSequence(Integer moduleID, Integer formId)
        throws Exception {

        List<UiFormResponseTO> list = null;
        Integer Sequence = null;
        try {
            Sequence = uiFormDS.getMaxSequence(moduleID, formId);

            return Sequence;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean checkSequenceExists(Integer parentformID, Integer sequence, Integer formid) {
        boolean exists = false;
        try {
            List<UiFormResponseTO> list = null;
            exists = uiFormDS.checkSequenceExists(parentformID, sequence, formid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return exists;
    }


    //Code for Menu Sequence


    public IResponseTransportObject doLogin(Integer bundleId)
        throws Exception {

        UserRequestTO userRequestTO = null;
        Map<Long, List<MenuTO>> menuMap = null;
        MenuResponseTO menuResponseTO = new MenuResponseTO();
        Integer bundleID = null;
        try {
            bundleID = bundleId;
            menuMap = getMenu(bundleID);
            menuResponseTO.setMenuMap(menuMap);

            return menuResponseTO;
        } catch (Exception e) {
            throw e;
        }
    }


    public Map<Long, List<MenuTO>> getMenu(Integer bundleId)
        throws Exception {
        Map<Long, List<MenuTO>> menuMap = null;
        Integer bundleID = null;

        try {
            bundleID = bundleId;
            try {
                menuMap = sysMenuCache.getAltMenuMap(bundleID);
                if (menuMap == null) {
                    throw new Exception("Data not found in Redis. Moving to DB");
                }
            } catch (Exception ex) {
                try {
                    menuMap = uiFormDS.getAllMenuMap(bundleID);
                    if (menuMap != null) {
                        try {
                            if (RedisSchedulerHelper.isRedisUpFlag()) {
                                RedisClient.putValueAsTO("SUPERADMIN:MENULIST", menuMap);
                            }
                        } catch (Exception ex1) {
                            ex1.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return menuMap;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SysBundleResponseTO> bundleDropDown()
        throws Exception {
        List<SysBundleResponseTO> response = null;
        try {
            response = uiFormDS.bundleDropDown();
        } catch (Exception ex) {

        }
        return response;
    }

    public boolean callProcedure(Integer formID)
        throws Exception {
        boolean result = false;
        result = uiFormDS.callProcedure(formID);
        return result;

    }

    public Integer getFormID(UiFormFieldResponseTO uiFormFieldResponseTO)
        throws Exception {
        // TODO Auto-generated method stub
        Integer result = null;
        try {
            Integer grpId = uiFormFieldResponseTO.getFieldGroupID();
            result = uiFormDS.getFormID(grpId);
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }


    }

    public void deleteAllRedisKey() {
        // TODO Auto-generated method stub
        uiFormDS.deleteAllRedisKey();
    }

    public List<UIFormTO> getUIFormTOsForModule(Integer moduleId) {
        UIFormTO uiFormTO = null;
        List<UIFormTO> formTOs = new ArrayList<UIFormTO>();
        for (Object[] row : uiFormDS.getUiFormsForModule(moduleId)) {
            uiFormTO = new UIFormTO();
            uiFormTO.setFormID((Integer) row[0]);
            uiFormTO.setFormName((String) row[1]);
            formTOs.add(uiFormTO);
        }
        return formTOs;
    }

    public List<SysFieldTypeResponseTO> fieldTypeDropDown()
        throws Exception {
        List<SysFieldTypeResponseTO> response = null;
        try {
            response = uiFormDS.fieldTypeDropDown();
        } catch (Exception ex) {

        }
        return response;
    }

    public List<SysRuleResponseTO> methodDropDown()
        throws Exception {
        List<SysRuleResponseTO> response = null;
        try {
            response = uiFormDS.methodDropDown();
        } catch (Exception ex) {

        }
        return response;
    }

    public boolean checkBotSequenceExists(Integer fieldGroupID, Integer sequence)
        throws Exception {
        boolean exists = false;
        try {
            exists = uiFormDS.checkBotSequenceExists(fieldGroupID, sequence);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return exists;
    }

    public Integer getBotSequenceByFormFieldID(Integer formFieldID)
        throws Exception {
        Integer botSequence = null;
        try {
            botSequence = uiFormDS.getBotSequenceByFormFieldID(formFieldID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return botSequence;

    }

}
