/**
 * 
 */
package com.talentpact.business.dataservice.loginserver;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.ats.core.entity.LoginServerDetail;
import com.talentpact.business.application.loginserver.LoginServerDetailTO;
import com.talentpact.business.dataservice.AbstractDS;


/**
 * 
 * @author javed.ali
 *
 */

@Stateless
public class LoginServerDetailDS extends AbstractDS<LoginServerDetail>
{
    public LoginServerDetailDS()
    {
        super(LoginServerDetail.class);
    }


    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void insertIntoLoginServerDetail(LoginServerDetailTO loginServerDetailTO) throws  Exception
    {
        LoginServerDetail loginServerDetail = null;
        try {
            loginServerDetail=findLoginServerDetail(loginServerDetailTO.getOrganizationID(), loginServerDetailTO.getLoginServerTypeID());
            if(loginServerDetail==null){
            loginServerDetail = new LoginServerDetail();
            loginServerDetail.setLoginServerTypeID(loginServerDetailTO.getLoginServerTypeID());
            loginServerDetail.setActive(loginServerDetailTO.getActive());
            loginServerDetail.setIsDefault(loginServerDetailTO.getIsDefault());
            loginServerDetail.setOrganizationID(loginServerDetailTO.getOrganizationID());
            loginServerDetail.setUrlID(loginServerDetailTO.getUrlID());
            loginServerDetail.setLogoutServerUrl(loginServerDetailTO.getLogoutServerUrl());
            loginServerDetail.setServerName(loginServerDetailTO.getLoginServerTypeName());
            getEntityManager("TalentpactAuth").persist(loginServerDetail);
            }else{
                loginServerDetail.setActive(loginServerDetailTO.getActive());
                loginServerDetail.setIsDefault(loginServerDetailTO.getIsDefault());
                loginServerDetail.setLogoutServerUrl(loginServerDetailTO.getLogoutServerUrl());
                loginServerDetail.setServerName(loginServerDetailTO.getLoginServerTypeName());
                getEntityManager("TalentpactAuth").merge(loginServerDetail);
                  
            }
        } catch (Exception e) {
            throw e;
        }

    }


    @SuppressWarnings("unchecked")
    public List<LoginServerDetail> getLoginServerDetailByOrgID(Integer orgID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<LoginServerDetail> loginServerDetailList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from LoginServerDetail s where s.organizationID=:orgID  ");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            loginServerDetailList = query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return loginServerDetailList;
    }

    @SuppressWarnings("unchecked")
    public LoginServerDetail findLoginServerDetail(Integer orgID,Integer typeID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<LoginServerDetail> loginServerDetailList = null;
        LoginServerDetail loginServerDetail=null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select s from LoginServerDetail s where s.organizationID=:orgID and s.loginServerTypeID=:loginServerTypeID ");
            query = getEntityManager("TalentpactAuth").createQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            query.setParameter("loginServerTypeID", typeID);
            loginServerDetailList = query.getResultList();
            if(!loginServerDetailList.isEmpty()){
                loginServerDetail  =loginServerDetailList.get(0); 
            }
        } catch (Exception e) {
            throw e;
        }
        return loginServerDetail;
    }

    public void inActiveAllLoginServerDetailByOrgID(Integer orgID)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("update tp_LoginServerdetail set IsActive=0 where organizationID=:orgID ");
            query = getEntityManager("TalentpactAuth").createNativeQuery(hqlQuery.toString());
            query.setParameter("orgID", orgID);
            query.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

}