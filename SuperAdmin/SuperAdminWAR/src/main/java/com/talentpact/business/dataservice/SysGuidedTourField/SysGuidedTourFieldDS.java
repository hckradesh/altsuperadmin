/**
 * 
 */
package com.talentpact.business.dataservice.SysGuidedTourField;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysGuidedTourFieldResponseTO;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.business.dataservice.sysCongratulationAnnouncement.SysCongratulationAnnouncementDS;
import com.talentpact.model.SysGuidedTourField;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysGuidedTourFieldDS extends AbstractDS<SysGuidedTourField>
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysCongratulationAnnouncementDS.class);

    public SysGuidedTourFieldDS()
    {
        super(SysGuidedTourField.class);
    }

    @SuppressWarnings("unchecked")
    public List<SysGuidedTourFieldResponseTO> getSysGuidedTourFieldTOList()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysGuidedTourFieldResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" s.guidedTourFieldId, s.sysForm.formId,s.sysForm.formName,");
            hqlQuery.append(" s.selector, s.clickPath, s.combine ");
            hqlQuery.append(" from SysGuidedTourField s ");
            hqlQuery.append(" order by  s.guidedTourFieldId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = converter(resultList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public static List<SysGuidedTourFieldResponseTO> converter(List<Object[]> resultList)
    {
        List<SysGuidedTourFieldResponseTO> list = null;
        SysGuidedTourFieldResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysGuidedTourFieldResponseTO();
                if (object[0] != null) {
                    responseTO.setGuidedTourFieldId((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setSysFormId((Integer) object[1]);
                }
                if (object[2] != null) {
                    responseTO.setSysForm((String) object[2]);
                }

                if (object[3] != null) {
                    responseTO.setSelector((String) object[3]);
                }
                if (object[4] != null) {
                    responseTO.setClickPath((String) object[4]);
                }
                if (object[5] != null) {
                    responseTO.setCombine((Boolean) object[5]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void save(SysGuidedTourFieldResponseTO to)
    {
        SysGuidedTourField sysGuidedTourField = null;
        Date date = new Date();
        try {
            sysGuidedTourField = findSysGuidedTourField(to);

            if (sysGuidedTourField == null) {
                sysGuidedTourField = new SysGuidedTourField();

                sysGuidedTourField.setSysFormId(to.getSysFormId());
                sysGuidedTourField.setSelector(to.getSelector());
                sysGuidedTourField.setCombine(to.getCombine());
                sysGuidedTourField.setClickPath(to.getClickPath());
                sysGuidedTourField.setCreatedDate(date);
                sysGuidedTourField.setModifiedDate(date);
                sysGuidedTourField.setModifiedBy(1);
                sysGuidedTourField.setCreatedBy(1);

                getEntityManager("TalentPact").persist(sysGuidedTourField);
            } else {
                sysGuidedTourField.setSysFormId(to.getSysFormId());
                sysGuidedTourField.setSelector(to.getSelector());
                sysGuidedTourField.setCombine(to.getCombine());
                sysGuidedTourField.setClickPath(to.getClickPath());
                sysGuidedTourField.setModifiedDate(date);
                sysGuidedTourField.setModifiedBy(1);
                getEntityManager("TalentPact").merge(sysGuidedTourField);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private SysGuidedTourField findSysGuidedTourField(SysGuidedTourFieldResponseTO to)
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysGuidedTourField> list = null;
        SysGuidedTourField result = null;
        try {

            hqlQuery = new StringBuilder();
            hqlQuery.append("select s from SysGuidedTourField s  where s.guidedTourFieldId=:guidedTourFieldId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("guidedTourFieldId", to.getGuidedTourFieldId());
            list = query.getResultList();
            if (list != null && list.size() > 0) {
                result = list.get(0);
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
        }
    }

    public List<SysGuidedTourFieldResponseTO> getUiFormDropDown()
        throws Exception
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysGuidedTourFieldResponseTO> list = null;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append("a.formId, a.formName ");
            hqlQuery.append("from ");
            hqlQuery.append("UiForm a ");
            query = getEntityManager("TalentPact").createQuery((hqlQuery.toString()));
            resultList = query.getResultList();
            if ((resultList != null) && !resultList.isEmpty()) {
                list = uiFormConverter(resultList);
            }
            return list;
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());
            throw ex;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysGuidedTourFieldResponseTO> uiFormConverter(List<Object[]> resultList)
    {
        List<SysGuidedTourFieldResponseTO> list = null;
        SysGuidedTourFieldResponseTO responseTO = null;

        try {
            list = new ArrayList<>();
            for (Object[] object : resultList) {
                responseTO = new SysGuidedTourFieldResponseTO();
                if (object[0] != null) {
                    responseTO.setSysFormId((Integer) object[0]);
                }

                if (object[1] != null) {
                    responseTO.setSysForm((String) object[1]);
                }


                list.add(responseTO);
            }
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());

        } finally {
            responseTO = null;
        }
        return list;
    }


}
