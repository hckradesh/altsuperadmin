/**
 * 
 */
package com.talentpact.business.dataservice.passwordpolicy.cache;

import javax.ejb.Stateless;

import com.alt.passwordPolicy.to.PasswordPolicyTO;
import com.talentpact.business.cache.RedisClient;

/**
 * @author javed.ali
 *
 */
@Stateless
public class PasswordPolicyCache
{
    
    public PasswordPolicyTO getPasswordPolicySettings()
            throws Exception
        {
            String key = PasswordPolicyKeyConstants.SYS_PASSWORD_POLICY_KEY;
            return RedisClient.getValue(key, PasswordPolicyTO.class);

        }

        public void putPasswordPolicySettings(PasswordPolicyTO passwordPolicyTO)
            throws Exception
        {
            String key = PasswordPolicyKeyConstants.SYS_PASSWORD_POLICY_KEY;
            RedisClient.putValueAsTO(key, passwordPolicyTO);

        }

        public void deletePasswordPolicySettingsRedisKey()
            throws Exception
        {
            String key = PasswordPolicyKeyConstants.SYS_PASSWORD_POLICY_KEY;
            RedisClient.delete(key);

        }


}
