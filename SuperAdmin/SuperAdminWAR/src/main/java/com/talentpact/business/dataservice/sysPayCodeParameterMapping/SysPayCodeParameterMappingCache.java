/**
 * 
 */
package com.talentpact.business.dataservice.sysPayCodeParameterMapping;

import java.util.List;

import javax.ejb.Stateless;

import com.talentpact.business.application.transport.output.SysPayCodeParameterMappingResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
public class SysPayCodeParameterMappingCache
{
    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMappingList()
        throws Exception
    {
        String key = "SUPERADMIN:SYSPAYCODEPARAMETERMAPPING";
        return RedisClient.getValueAsList(key, SysPayCodeParameterMappingResponseTO.class);
    }

    public void persistSysPayCodeParameterMappingListByTenant(List<SysPayCodeParameterMappingResponseTO> constantList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPAYCODEPARAMETERMAPPING";
        RedisClient.putValueAsTO(key, constantList);

    }

    public void removeSysPayCodeParameterMappingListByTenant()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPAYCODEPARAMETERMAPPING";
        RedisClient.delete(key);
    }


    public List<SysPayCodeParameterMappingResponseTO> getSysParameterDropDownList()
        throws Exception
    {
        String key = "SUPERADMIN:SYSPARAMETERDROPDOWN";
        return RedisClient.getValueAsList(key, SysPayCodeParameterMappingResponseTO.class);
    }

    public void persistgetSysParameterDropDownList(List<SysPayCodeParameterMappingResponseTO> constantList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPARAMETERDROPDOWN";
        RedisClient.putValueAsTO(key, constantList);

    }

    public void removeSysParameterListDropDown()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPARAMETERDROPDOWN";
        RedisClient.delete(key);
    }


    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeDropDownList()
        throws Exception
    {
        String key = "SUPERADMIN:SYSPAYCODEDROPDOWN";
        return RedisClient.getValueAsList(key, SysPayCodeParameterMappingResponseTO.class);
    }

    public void persistgetSysPayCodeDropDownList(List<SysPayCodeParameterMappingResponseTO> constantList)
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPAYCODEDROPDOWN";
        RedisClient.putValueAsTO(key, constantList);

    }

    public void removeSysPayCodeListDropDown()
        throws RedisConnectionException
    {
        String key = "SUPERADMIN:SYSPAYCODEDROPDOWN";
        RedisClient.delete(key);
    }


}
