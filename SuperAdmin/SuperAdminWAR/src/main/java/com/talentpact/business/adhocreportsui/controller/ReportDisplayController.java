package com.talentpact.business.adhocreportsui.controller;

import static com.talentpact.adhocreports.business.common.controller.Constants.CONSTANT_UNDERSCORE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.talentpact.adhocreports.business.controller.CreateQuery;
import com.talentpact.adhocreports.business.controller.ExecuteQuery;
import com.talentpact.adhocreports.business.controller.MakeQuery;
import com.talentpact.adhocreports.business.common.controller.EnumReportType;
import com.talentpact.adhocreports.business.common.controller.EnumTableType;
import com.talentpact.adhocreports.business.common.controller.ReportColumnCss;
import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.common.exception.CreateQueryException;
import com.talentpact.adhocreports.business.common.exception.ExecuteQueryException;
import com.talentpact.adhocreports.business.common.exception.InputMissingException;
import com.talentpact.adhocreports.business.common.exception.MakeQueryException;
import com.talentpact.adhocreports.business.to.GenericInputTo;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.talentpact.business.adhocreportsui.bean.CommonReportDataBean;
import com.talentpact.business.adhocreportsui.bean.CreateConfigurtionDataBean;
import com.talentpact.business.adhocreportsui.bean.CreateReportDataBean;
import com.talentpact.business.adhocreportsui.bean.ReportDetailBean;
import com.talentpact.ui.common.bean.UserSessionBean;

@SessionScoped
@Named("reportDisplayController")
public class ReportDisplayController implements Serializable{

	private static final long serialVersionUID = -8500227902753162589L;
	private static final Logger LOGGER = Logger.getLogger(ReportDisplayController.class);
	
	@Inject
	CreateReportDataBean createReportDataBean;
	
	@Inject
	CommonReportDataBean commonReportDataBean;
	
    @Inject
    UserSessionBean userSessionBean;
	
	public void initialize(ReportDetailBean reportDetailBean){
		Map<String,String> applicationInputMap = new HashMap<String,String>();
		Map<String,String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if(requestParameterMap != null && requestParameterMap.size() != 0 && requestParameterMap.get("orgid") != null && reportDetailBean.getOrgID() == null){
			String orgid = requestParameterMap.get("orgid");
			reportDetailBean.setOrgID(Integer.valueOf(orgid));
		}
		
		List<ReportDetail> reportDetailList = null;
		List<SelectItem> modulesList = null;
		Map<String,List<ReportDetail>> reports = new HashMap<String,List<ReportDetail>>();
		List<String> reportNameList = new ArrayList<String>();
		List<String> moduleNameList = new ArrayList<String>();
		
		try{
			List<SelectItem> applicationInputList = CreateQuery.getApplicationInputList();
	    	for(SelectItem applicationInput : applicationInputList){
	    		if(applicationInput.getLabel().contains("OrganizationID")){
	    			applicationInputMap.put(applicationInput.getValue() + CONSTANT_UNDERSCORE + applicationInput.getLabel(), "3");
	    		}else if(applicationInput.getLabel().contains("UserID")){
	    			applicationInputMap.put(applicationInput.getValue() + CONSTANT_UNDERSCORE + applicationInput.getLabel(), userSessionBean.getUserID().toString());
	    		}
	    	}
			reportDetailBean.setApplicationInputMap(applicationInputMap);

			if(reportDetailBean.getOrgID() == null){
				reportDetailList = CreateQuery.getAllReportDetails();
				reportDetailBean.setReportDetailList(reportDetailList);
			}else{
				reportDetailList = CreateQuery.getAllReportDetails(reportDetailBean.getOrgID().intValue());
				reportDetailBean.setReportDetailList(reportDetailList);
			}
			modulesList = CreateQuery.getReportModules();
			reportDetailBean.setActiveInactiveReportDetailList(CreateQuery.getAllActiveInactiveReportDetails());
		}catch(Exception e){
			LOGGER.error("Error getting reportDetails inside initialize of ReportDisplayController.", e);
		}
		
		for(SelectItem module : modulesList){
			moduleNameList.add(module.getLabel());
		}
		
		if(reportDetailList!=null){
			for(ReportDetail reportDetail: reportDetailList){
				if(reports.containsKey(reportDetail.getModuleName())){
					List<ReportDetail> reportDetailListTemp = reports.get(reportDetail.getModuleName());
					reportDetailListTemp.add(reportDetail);
					reports.put(reportDetail.getModuleName(), reportDetailListTemp);
				}else{
					List<ReportDetail> reportDetailListTemp = new ArrayList<ReportDetail>();
					reportDetailListTemp.add(reportDetail);
					reports.put(reportDetail.getModuleName(), reportDetailListTemp);
				}
			}
		}
		
		for(ReportDetail reportDetail: commonReportDataBean.getNonDefaultReports()){
			reportNameList.add(reportDetail.getReportName());
		}
		
		reportDetailBean.setReportNameList(reportNameList);
		reportDetailBean.setReports(reports);
		reportDetailBean.setModules(modulesList);
		reportDetailBean.setModuleNameList(moduleNameList);
	}

	public void getSelectedReportQuery(ReportDetailBean reportDetailBean){
		GenericInputTo genericInputTo = new GenericInputTo();
		genericInputTo.setId(reportDetailBean.getSelectedReport().getReportId());
		genericInputTo.setReportType(reportDetailBean.getSelectedReport().getReportType());
		if(reportDetailBean.getSelectedReport().getReportType()==EnumReportType.REPORTGENERIC){
			genericInputTo.setTableType(EnumTableType.REPORTGENERIC);
		}else if(reportDetailBean.getSelectedReport().getReportType()==EnumReportType.REPORTGROUPING){
			genericInputTo.setTableType(EnumTableType.REPORTGROUPING);
		}
		genericInputTo.setApplicationInputMap(reportDetailBean.getApplicationInputMap());
		
		try {
			String query = MakeQuery.process(genericInputTo,reportDetailBean.getSelectReportFilters());
			reportDetailBean.setSelectedReportQuery(query);
		} catch (InputMissingException e) {
			LOGGER.error("Error inside getSelectedReportQuery(ReportDetailBean reportDetailBean)", e);
		} catch (MakeQueryException e) {
			LOGGER.error("Error inside getSelectedReportQuery(ReportDetailBean reportDetailBean)", e);
		}
		executeSelectedReportQuery(reportDetailBean);
	}
	
	public void executeSelectedReportQuery(ReportDetailBean reportDetailBean){
		try {
			List<String> reportHeaders = ExecuteQuery.getReportHeaders(reportDetailBean.getSelectedReport().getReportId(),3);//For SuperAdmin portal passing organizationid=3 
			List<Object[]> reportData = ExecuteQuery.executeNativeQuery(reportDetailBean.getSelectedReportQuery(),reportDetailBean.getSelectedReport().getReportId(),3);//For SuperAdmin portal passing organizationid=3
			List<ReportColumnCss> reportColumnCssList = ExecuteQuery.getReportColumnCss(reportDetailBean.getSelectedReport().getReportId(),3);//For SuperAdmin portal passing organizationid=3
			reportDetailBean.setReportHeaders(reportHeaders);
			reportDetailBean.setReportData(reportData);
			reportDetailBean.setCssDetails(reportColumnCssList);
			reportDetailBean.setReportNameForCreation(reportDetailBean.getSelectedReport().getReportName());
		} catch (ExecuteQueryException e) {
			LOGGER.error("Error inside executeSelectedReportQuery(ReportDetailBean reportDetailBean)", e);
		}
	}
	
	public void checkDuplicateReport(ReportDetailBean reportDetailBean){
		String reportName = reportDetailBean.getReportNameForCreation();
		for(ReportDetail reportDetail: reportDetailBean.getReportDetailList()){
			if(reportDetail.getReportName().equalsIgnoreCase(reportName)){
				FacesContext.getCurrentInstance().addMessage("displayReportsForm:reportExistsError",new FacesMessage(FacesMessage.SEVERITY_ERROR,"Duplicate Report", "Report with same name already exists"));
				FacesContext.getCurrentInstance().validationFailed();
				return;
			}
		}
		createReportDataBean.initialize();
	}
	
	public void preRenderView(ReportDetailBean reportDetailBean){
		if (!FacesContext.getCurrentInstance().isPostback()) {
			try {
				reportDetailBean.setActiveInactiveReportDetailList(CreateQuery.getAllActiveInactiveReportDetails());
			} catch (CreateQueryException e) {
				LOGGER.error("Error inside preRenderView(ReportDetailBean reportDetailBean) : ", e);
			}
		}
	}
	
	public void setReportDetailBeanAttributesToNull(ReportDetailBean reportDetailBean) {
			reportDetailBean.setReportNameForCreation(null);
					reportDetailBean.setReportDescriptionForCreation(null);
						reportDetailBean.setSelectedModule(null);
						reportDetailBean.setSelectedReportType(null);
						reportDetailBean.setDefaultFlag(null);
	}
	
	public void preRenderView(CreateConfigurtionDataBean configurationDataBean){
		if (!FacesContext.getCurrentInstance().isPostback()) {
			configurationDataBean.setConstantNameForCreation(null);
			configurationDataBean.setConstantValue(null);
			configurationDataBean.setOrganizationId(null);
			configurationDataBean.setSelectedDataType(null);
		}
	}
	
	public void invalidateSession(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

}
