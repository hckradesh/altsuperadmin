package com.talentpact.business.common.exceptions;

public class JSONConversionException extends Exception
{
   
    private static final long serialVersionUID = 5342578512789887430L;
    
    private String message = null;

    public JSONConversionException()
    {
        super();
    }

    public JSONConversionException(String message)
    {
        super(message);
        this.message = message;
    }

    public JSONConversionException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

}
