package com.talentpact.business.adhocreportsui.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.adhocreports.business.common.controller.ReportDetail;
import com.talentpact.adhocreports.business.common.controller.SelectItem;
import com.talentpact.adhocreports.business.to.OperatorTo;
import com.talentpact.adhocreports.ui.business.common.ReportList;
import com.talentpact.adhocreports.ui.business.to.IntermediateProcessingLogicTemplateTo;
import com.talentpact.adhocreports.ui.business.to.ReportDataTo;
import com.talentpact.adhocreports.ui.business.to.TableTo;
import com.talentpact.business.adhocreportsui.controller.EditReportDataController;

@SessionScoped
@Named("editReportDataBean")
public class EditReportDataBean implements Serializable {

	private static final long serialVersionUID = -3671326222867344675L;

	@Inject
	EditReportDataController editReportDataController;

	private ReportDetail selectedReportForEditing;

	private ReportList.ReportListNode<ReportDataTo> editCurrentPopulatedListNode;

	private ReportList.ReportListNode<ReportDataTo> updatedReportListNode; // not used as of now

	private String selectedReportType;

	private SelectItem selectedReportModule;

	private String searchColumnRegex;

	private boolean previewFlag;

	private boolean disableFilterFlag;

	private boolean addCssFlag;

	private List<OperatorTo> cssOperators;

	private String tableDescription;

	private String derivedTableNameForCreation;

	private String selectedTableType;

	private String orgAdminFlag;

	private String defaultDerivedTableFlag;

	private Integer droppedColumnIndex;

	private SelectItem selectedDerivedTableModule;

	private List<TableTo> previousSelectedSysTables;

	private List<TableTo> previousSelectedDerivedTables;

	private boolean previousSelecteDefaultFlag;

	private IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo;

	private List<Object[]> reportData;

	private List<String> reportHeaders;

	private List<String> previewReportHeaders;

	private List<TableTo> newlyAddedTables;

	private List<TableTo> removedTables;

	private List<TableTo> previousTablesUsedInJoin;

	private TableTo organizationIdFilterTable;
	
	public void initialize(ReportDetail report) {
		editReportDataController.initialize(report);
	}

	public ReportDetail getSelectedReportForEditing() {
		return selectedReportForEditing;
	}

	public void setSelectedReportForEditing(ReportDetail selectedReportForEditing) {
		this.selectedReportForEditing = selectedReportForEditing;
	}

	public ReportList.ReportListNode<ReportDataTo> getEditCurrentPopulatedListNode() {
		return editCurrentPopulatedListNode;
	}

	public void setEditCurrentPopulatedListNode(
			ReportList.ReportListNode<ReportDataTo> editCurrentPopulatedListNode) {
		this.editCurrentPopulatedListNode = editCurrentPopulatedListNode;
	}

	public String getSelectedReportType() {
		return selectedReportType;
	}

	public void setSelectedReportType(String selectedReportType) {
		this.selectedReportType = selectedReportType;
	}

	public SelectItem getSelectedReportModule() {
		return selectedReportModule;
	}

	public void setSelectedReportModule(SelectItem selectedReportModule) {
		this.selectedReportModule = selectedReportModule;
	}

	public String getSearchColumnRegex() {
		return searchColumnRegex;
	}

	public void setSearchColumnRegex(String searchColumnRegex) {
		this.searchColumnRegex = searchColumnRegex;
	}

	public boolean isPreviewFlag() {
		return previewFlag;
	}

	public void setPreviewFlag(boolean previewFlag) {
		this.previewFlag = previewFlag;
	}

	public boolean isDisableFilterFlag() {
		return disableFilterFlag;
	}

	public void setDisableFilterFlag(boolean disableFilterFlag) {
		this.disableFilterFlag = disableFilterFlag;
	}

	public boolean isAddCssFlag() {
		return addCssFlag;
	}

	public void setAddCssFlag(boolean addCssFlag) {
		this.addCssFlag = addCssFlag;
	}

	public List<OperatorTo> getCssOperators() {
		return cssOperators;
	}

	public void setCssOperators(List<OperatorTo> cssOperators) {
		this.cssOperators = cssOperators;
	}

	public String getTableDescription() {
		return tableDescription;
	}

	public void setTableDescription(String tableDescription) {
		this.tableDescription = tableDescription;
	}

	public String getDerivedTableNameForCreation() {
		return derivedTableNameForCreation;
	}

	public void setDerivedTableNameForCreation(String derivedTableNameForCreation) {
		this.derivedTableNameForCreation = derivedTableNameForCreation;
	}

	public String getSelectedTableType() {
		return selectedTableType;
	}

	public void setSelectedTableType(String selectedTableType) {
		this.selectedTableType = selectedTableType;
	}

	public String getOrgAdminFlag() {
		return orgAdminFlag;
	}

	public void setOrgAdminFlag(String orgAdminFlag) {
		this.orgAdminFlag = orgAdminFlag;
	}

	public String getDefaultDerivedTableFlag() {
		return defaultDerivedTableFlag;
	}

	public void setDefaultDerivedTableFlag(String defaultDerivedTableFlag) {
		this.defaultDerivedTableFlag = defaultDerivedTableFlag;
	}

	public Integer getDroppedColumnIndex() {
		return droppedColumnIndex;
	}

	public void setDroppedColumnIndex(Integer droppedColumnIndex) {
		this.droppedColumnIndex = droppedColumnIndex;
	}

	public SelectItem getSelectedDerivedTableModule() {
		return selectedDerivedTableModule;
	}

	public void setSelectedDerivedTableModule(SelectItem selectedDerivedTableModule) {
		this.selectedDerivedTableModule = selectedDerivedTableModule;
	}

	public ReportList.ReportListNode<ReportDataTo> getUpdatedReportListNode() {
		return updatedReportListNode;
	}

	public void setUpdatedReportListNode(
			ReportList.ReportListNode<ReportDataTo> updatedReportListNode) {
		this.updatedReportListNode = updatedReportListNode;
	}

	public List<TableTo> getPreviousSelectedSysTables() {
		return previousSelectedSysTables;
	}

	public void setPreviousSelectedSysTables(List<TableTo> previousSelectedSysTables) {
		this.previousSelectedSysTables = previousSelectedSysTables;
	}

	public List<TableTo> getPreviousSelectedDerivedTables() {
		return previousSelectedDerivedTables;
	}

	public void setPreviousSelectedDerivedTables(
			List<TableTo> previousSelectedDerivedTables) {
		this.previousSelectedDerivedTables = previousSelectedDerivedTables;
	}

	public boolean isPreviousSelecteDefaultFlag() {
		return previousSelecteDefaultFlag;
	}

	public void setPreviousSelecteDefaultFlag(boolean previousSelecteDefaultFlag) {
		this.previousSelecteDefaultFlag = previousSelecteDefaultFlag;
	}

	public IntermediateProcessingLogicTemplateTo getIntermediateProcessingLogicTemplateTo() {
		return intermediateProcessingLogicTemplateTo;
	}

	public void setIntermediateProcessingLogicTemplateTo(
			IntermediateProcessingLogicTemplateTo intermediateProcessingLogicTemplateTo) {
		this.intermediateProcessingLogicTemplateTo = intermediateProcessingLogicTemplateTo;
	}

	public List<Object[]> getReportData() {
		return reportData;
	}

	public void setReportData(List<Object[]> reportData) {
		this.reportData = reportData;
	}

	public List<String> getReportHeaders() {
		return reportHeaders;
	}

	public void setReportHeaders(List<String> reportHeaders) {
		this.reportHeaders = reportHeaders;
	}

	public List<String> getPreviewReportHeaders() {
		return previewReportHeaders;
	}

	public void setPreviewReportHeaders(List<String> previewReportHeaders) {
		this.previewReportHeaders = previewReportHeaders;
	}

	public List<TableTo> getNewlyAddedTables() {
		return newlyAddedTables;
	}

	public void setNewlyAddedTables(List<TableTo> newlyAddedTables) {
		this.newlyAddedTables = newlyAddedTables;
	}

	public List<TableTo> getRemovedTables() {
		return removedTables;
	}

	public void setRemovedTables(List<TableTo> removedTables) {
		this.removedTables = removedTables;
	}

	public List<TableTo> getPreviousTablesUsedInJoin() {
		return previousTablesUsedInJoin;
	}

	public void setPreviousTablesUsedInJoin(List<TableTo> previousTablesUsedInJoin) {
		this.previousTablesUsedInJoin = previousTablesUsedInJoin;
	}

	public TableTo getOrganizationIdFilterTable() {
		return organizationIdFilterTable;
	}

	public void setOrganizationIdFilterTable(TableTo organizationIdFilterTable) {
		this.organizationIdFilterTable = organizationIdFilterTable;
	}

}
