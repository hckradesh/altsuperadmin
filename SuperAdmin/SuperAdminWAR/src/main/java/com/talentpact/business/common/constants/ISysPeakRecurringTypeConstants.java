package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysPeakRecurringTypeConstants
{

    public static String SYSPEAKRECURRINGTYPE_ADD_DIALOG_HIDE = "PF('sysPeakRecurringTypeAddModal').hide();";

    public static String SYSPEAKRECURRINGTYPE_EDIT_DIALOG_HIDE = "PF('sysPeakRecurringTypeEditModal').hide();";

    public static String SYSPEAKRECURRINGTYPE_SAVE_ERROR_MSG = "Error occured while saving SysPeakRecurringType.";

    public static String SYSPEAKRECURRINGTYPE_UPDATE_ERROR_MSG = "Error occured while updating SysPeakRecurringType.";

    public static String SYSPEAKRECURRINGTYPE_SAVE_SUCCESS_MSG = "SysPeakRecurringType saved successfully.";

    public static String SYSPEAKRECURRINGTYPE_UPDATE_SUCCESS_MSG = "SysPeakRecurringType updated successfully.";

    public static String SYSPEAKRECURRINGTYPE_DATATABLE_RESET = "PF('sysPeakRecurringTypeDataTable').clearFilters()";

    public static String SYSPEAKRECURRINGTYPE_PAGINATION_DATATABLE_RESET = "PF('sysPeakRecurringTypeDataTable').getPaginator().setPage(0);";

    public static String SYSPEAKRECURRINGTYPE_DUPLICATE_ERROR_MSG = "SysPeakRecurringType already present in system.";
}
