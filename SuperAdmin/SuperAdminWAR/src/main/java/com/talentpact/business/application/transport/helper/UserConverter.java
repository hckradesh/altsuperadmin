/**
 * 
 */
package com.talentpact.business.application.transport.helper;

import java.util.ArrayList;
import java.util.List;

import com.talentpact.business.application.transport.output.UserResponseTO;


/**
 * @author pankaj.sharma1
 *
 */
public class UserConverter
{
    public static List<UserResponseTO> getUserList(List<Object[]> resultList)
        throws Exception
    {
        List<UserResponseTO> userList = null;
        UserResponseTO responseTO = null;
        Boolean statusID = null;
        String userName = null;
        String password = null;
        Long userID = null;
        String status = null;
        try {
            userList = new ArrayList<UserResponseTO>();

            for (Object[] object : resultList) {
                responseTO = new UserResponseTO();
                if (object[0] != null) {
                    userID = (Long) object[0];
                    responseTO.setUserID(userID);
                }
                if (object[1] != null) {
                    userName = (String) object[1];
                    responseTO.setUserName(userName);
                }
                if (object[2] != null) {
                    statusID = (Boolean) object[2];
                    responseTO.setStatusID(statusID);
                    if (statusID == true) {
                        responseTO.setStatus("Active");
                    } else {
                        responseTO.setStatus("Inactive");
                    }
                }
                if (object[3] != null) {
                    password = (String) object[3];
                    responseTO.setPassword(password);
                }
                userList.add(responseTO);
            }


        } catch (Exception e) {
            throw e;
        } finally {
            responseTO = null;
            userID = null;
        }
        return userList;
    }


}
