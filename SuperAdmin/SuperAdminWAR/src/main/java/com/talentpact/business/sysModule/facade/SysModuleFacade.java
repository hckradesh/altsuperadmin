package com.talentpact.business.sysModule.facade;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import org.apache.log4j.Logger;
import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.sysModule.service.impl.SysModuleService;
import com.talentpact.remote.sysModule.ISysModuleRemote;
import com.talentpact.ui.sysModule.constants.ISysModuleConstants;
import com.talentpact.ui.sysModule.exception.SysModuleException;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
@Local(ISysModuleRemote.class)
public class SysModuleFacade extends CommonFacade implements ISysModuleRemote {

	private static final Logger LOGGER = Logger.getLogger(SysModuleFacade.class.toString());
	
	@EJB
	SysModuleService sysModuleService;
	
	@Override
	public ServiceResponse getAllSysModuleList(ServiceRequest serviceRequest) throws SysModuleException {
		ServiceResponse response = null;
		try{
			response = sysModuleService.getAllSysModuleList(serviceRequest);
		}catch (Exception ex) {
			LOGGER.error("", ex);
			throw new SysModuleException(ISysModuleConstants.SYSMODULE_RETRIEVE_ERROR_MSG);
		}
		return response;
	}

}
