/**
 * 
 */
package com.talentpact.business.dataservice.UiForm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.business.application.transport.output.SysResourceBundleResponseTO;
import com.talentpact.business.application.transport.output.SysResourceTO;
import com.talentpact.business.application.transport.output.SysRuleResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldGroupResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.cache.RedisClient;
import com.talentpact.business.common.exceptions.RedisConnectionException;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysBundle;
import com.talentpact.model.SysResource;
import com.talentpact.model.SysResourceBundle;
import com.talentpact.model.worflow.UiForm;
import com.talentpact.model.worflow.UiFormField;
import com.talentpact.model.worflow.UiFormFieldGroup;
import com.talentpact.ui.menu.transport.MenuTO;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Stateless
public class UiFormDS extends AbstractDS<UiForm> {
    public UiFormDS() {
        super(UiForm.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getDataList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiform.FormID, uiform.FormName  ,uiform.PrimaryEntityID,  sysEntity.Name , uiform.URL ,uiform.SubModuleID, uiform.ParentFormID,"
                    + "(select uiform1.FormName from uiform uiform1 where uiform.ParentFormID=uiform1.FormID ) as parentFormName ,uiform.IsCustomComponentAllowed ,"
                    + "uiform.ismenu ,uiform.ResourceID ,uiform.Sequence , uiform.IconPath ,uiform.MenuCategory, uiform.DefaultClassCall,uiform.floatingMenuIconPath,uiform.InactiveIconPath, "
                    + "(select sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ModuleID = uiform.subModuleID) as sysSubModuleName,"
                    + " (select sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ModuleID = (select sysSubModule1.ParentModuleID from SysSubModule sysSubModule1 where sysSubModule1.ModuleID= uiform.subModuleID)) as moduleName,"
                    + "  sysresource.Value ,sysresource.description,uiform.fragmentClass,uiform.IOSSideMenu , uiform.DisableURL , uiform.DisableText , uiform.DisableBean from uiform uiform left join SysEntity sysEntity on "
                    + "uiform.PrimaryEntityID=sysEntity.EntityID and sysEntity.TenantID = uiform.TenantID left join Sysresource sysresource on uiform.ResourceID=sysresource.ResourceID"
                    + " and uiform.TenantID=sysresource.TenantID order by uiform.FormID desc");

            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getSubModuleList(int moduleId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            // hqlQuery.append("select sysSubModule.ModuleID ,sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ParentModuleID is not null ");
            hqlQuery.append("select sysSubModule.ModuleID ,sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ParentModuleID is not null and sysSubModule.ParentModuleID = "
                    + moduleId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getModuleList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysSubModule.ModuleID ,sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ParentModuleID is  null ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getSysEntityList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select SysEntity.EntityID ,SysEntity.Name from SysEntity SysEntity ");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getParentFormList() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiform.FormID ,uiform.FormName from UiForm uiform");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean saveFormDataNew(UiFormResponseTO uiFormResponseTO) {
        // TODO Auto-generated method stub

        try {
            UiForm uiForm = new UiForm();
            uiForm.setFormName(uiFormResponseTO.getFormName());
            uiForm.setSysEntityId(uiFormResponseTO.getPrimaryEntityID());
            uiForm.setCreatedBy(uiFormResponseTO.getCreatedBy());
            uiForm.setCreatedDate(new Date());
            uiForm.setModifiedBy(uiFormResponseTO.getModifiedBy());
            uiForm.setModifiedDate(uiFormResponseTO.getModifiedDate());
            uiForm.setUrl(uiFormResponseTO.getUrl());
            uiForm.setSubModuleId(uiFormResponseTO.getSubModuleID());
            uiForm.setParentFormId(uiFormResponseTO.getParentFormID());
            uiForm.setTenantId(uiFormResponseTO.getTenantID());
            uiForm.setCustomComponentAllowed(uiFormResponseTO.isCustomComponentAllowed());
            uiForm.setMenuType(uiFormResponseTO.isMenu());
            uiForm.setIconPath(uiFormResponseTO.getIconPath());
            uiForm.setFloatingMenuIconPath(uiFormResponseTO.getFloatingMenuIconPath());
            uiForm.setInactiveIconPath(uiFormResponseTO.getInactiveIconPath());
            uiForm.setDefaultClassCall(uiFormResponseTO.getDefaultClassCall());
            uiForm.setSequence(uiFormResponseTO.getSequence());
            uiForm.setMenuCategory(uiFormResponseTO.getMenuCategory());
            uiForm.setDisableBean(uiFormResponseTO.getDisableBean());
            uiForm.setDisableText(uiFormResponseTO.getDisableText());
            uiForm.setDisableUrl(uiFormResponseTO.getDisableUrl());
            if (uiFormResponseTO.getResourceId() != 0) {
                uiForm.setResourceId(uiFormResponseTO.getResourceId());
            } else {
                uiForm.setResourceId(null);
            }
            uiForm.setFragmentClass(uiFormResponseTO.getFragmentClass());
            uiForm.setIosSideMenu(uiFormResponseTO.getIosSideMenu());
            getEntityManager("TalentPact").persist(uiForm);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Object[]> getFieldGroupData(Integer formID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select FieldGroupID,FieldGroupName,FormID from UiFormFieldGroup where FormID=" + formID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getFormFieldData(Integer formID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select FieldGroupID,FieldGroupName,FormID from UiFormFieldGroup where FormID=" + formID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getFormGroupFieldData(Integer formGroupID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uff.FormFieldID, uff.FieldGroupID, uff.EntityColumnID, uff.OrganizationID, uff.ClientID, uff.tenantID, uff.isAction, "
                    + "	 uff.isApprovalRequired, uff.isEffectiveDateRequired, uff.resourceID, uff.isMandatory, uff.parentFormFieldID, "
                    + "  uff.IsAttachmentRequired, uff.regex , sysentitycolumn.DBColumnName , sysResource.Value ,uff.IsMaster, uff.IsAttachmentEnabled,uff.configurationValueWeb,uff.configurationValueMobile,uff.isGroupParameter, "
                    + "	 (select uff1.clientid  from   UiFormField uff1  where uff1.formfieldid = uff.parentformfieldid ) as ParentclientId, uff.sysFieldTypeID, uff.sysRuleID, sft.FieldType, sr.RuleMethod, uff.BotResourceID, uff.ConfigurationValueBot, uff.BotSequence  from  UiFormField uff "
                    + "	 left join sysentitycolumn sysentitycolumn on sysentitycolumn.EntityColumnID = uff.EntityColumnID and sysentitycolumn.TenantID = uff.TenantID		 left join sysResource sysResource on uff.ResourceID = sysResource.ResourceID and uff.TenantID = sysResource.TenantID"
                    + "	 left join SysFieldType sft on sft.SysFieldTypeID=uff.SysFieldTypeID " + "	 left join SysRule sr on sr.RuleID=uff.SysRuleID "
                    + "	 left join UiFormField uff1 on uff.ParentFormFieldID = uff1.FormFieldID  where uff.FieldGroupID=" + formGroupID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean updateFormDataNew(UiFormResponseTO uiFormResponseTO) {
        // TODO Auto-generated method stub

        try {
            UiForm uiForm = getEntityManager("TalentPact").find(UiForm.class, uiFormResponseTO.getFormID());
            uiForm.setFormName(uiFormResponseTO.getFormName());
            uiForm.setSysEntityId(uiFormResponseTO.getPrimaryEntityID());
            uiForm.setModifiedBy(uiFormResponseTO.getModifiedBy());
            uiForm.setModifiedDate(new Date());
            uiForm.setUrl(uiFormResponseTO.getUrl());
            uiForm.setSubModuleId(uiFormResponseTO.getSubModuleID());
            uiForm.setParentFormId(uiFormResponseTO.getParentFormID());
            uiForm.setTenantId(uiFormResponseTO.getTenantID());
            uiForm.setCustomComponentAllowed(uiFormResponseTO.isCustomComponentAllowed());
            uiForm.setMenuType(uiFormResponseTO.isMenu());
            uiForm.setIconPath(uiFormResponseTO.getIconPath());
            uiForm.setFloatingMenuIconPath(uiFormResponseTO.getFloatingMenuIconPath());
            uiForm.setInactiveIconPath(uiFormResponseTO.getInactiveIconPath());
            uiForm.setDefaultClassCall(uiFormResponseTO.getDefaultClassCall());
            uiForm.setSequence(uiFormResponseTO.getSequence());
            uiForm.setMenuCategory(uiFormResponseTO.getMenuCategory());
            uiForm.setDisableBean(uiFormResponseTO.getDisableBean());
            uiForm.setDisableText(uiFormResponseTO.getDisableText());
            uiForm.setDisableUrl(uiFormResponseTO.getDisableUrl());

            if (uiFormResponseTO.getResourceId() != 0) {
                uiForm.setResourceId(uiFormResponseTO.getResourceId());
            } else {
                uiForm.setResourceId(null);
            }
            uiForm.setFragmentClass(uiFormResponseTO.getFragmentClass());
            uiForm.setIosSideMenu(uiFormResponseTO.getIosSideMenu());
            getEntityManager("TalentPact").merge(uiForm);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean saveFormDataNew(UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO) {
        // TODO Auto-generated method stub

        try {
            UiFormFieldGroup uiFormGroup = new UiFormFieldGroup();
            uiFormGroup.setFormId(uiFormFieldGroupResponseTO.getFormID());
            uiFormGroup.setFieldGroupName(uiFormFieldGroupResponseTO.getFieldGroupName());
            uiFormGroup.setCreatedBy(uiFormFieldGroupResponseTO.getCreatedBy());
            uiFormGroup.setCreatedDate(new Date());
            uiFormGroup.setTenantId(uiFormFieldGroupResponseTO.getTenantID());
            getEntityManager("TalentPact").persist(uiFormGroup);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public int saveFormFieldData(UiFormFieldResponseTO uiFormFieldResponseTO) {
        // TODO Auto-generated method stub

        try {
            UiFormField uiFormField = new UiFormField();

            uiFormField.setFieldGroupId(uiFormFieldResponseTO.getFieldGroupID());
            uiFormField.setEntityColumnId(uiFormFieldResponseTO.getEntityFieldID());
            uiFormField.setOrganizationID(uiFormFieldResponseTO.getOrganizationID());
            uiFormField.setClientId(uiFormFieldResponseTO.getClientID());
            uiFormField.setTenantId(1);
            uiFormField.setCreatedBy(uiFormFieldResponseTO.getCreatedBy());
            uiFormField.setCreatedDate(new Date());
            uiFormField.setModifiedBy(uiFormFieldResponseTO.getModifiedBy());
            uiFormField.setModifiedDate(new Date());
            uiFormField.setActionType(uiFormFieldResponseTO.isAction());
            uiFormField.setApprovalRequired(uiFormFieldResponseTO.isApprovalRequired());
            uiFormField.setAttachmentEnabled(uiFormFieldResponseTO.isAttachmentEnabled());
            uiFormField.setMaster(uiFormFieldResponseTO.isMaster());
            uiFormField.setAttachmentRequired(uiFormFieldResponseTO.isAttachmentRequired());
            uiFormField.setEffectiveDateRequired(uiFormFieldResponseTO.isEffectiveDateRequired());
            uiFormField.setResourceId(uiFormFieldResponseTO.getResourceID());
            uiFormField.setMandatory(uiFormFieldResponseTO.isMandatory());
            uiFormField.setParentFormFieldId(uiFormFieldResponseTO.getParentFormFieldID());
            uiFormField.setRegex(uiFormFieldResponseTO.getRegex());
            uiFormField.setConfigurationValueMobile(uiFormFieldResponseTO.getConfigurationValueMobile());
            uiFormField.setConfigurationValueWeb(uiFormFieldResponseTO.getConfigurationValueWeb());
            uiFormField.setIsGroupParameter(uiFormFieldResponseTO.isGroupParameter());
            uiFormField.setSysFieldTypeID((uiFormFieldResponseTO.getSysFieldTypeID() != null && uiFormFieldResponseTO.getSysFieldTypeID() != 0) ? uiFormFieldResponseTO
                    .getSysFieldTypeID() : null);
            uiFormField.setSysRuleID((uiFormFieldResponseTO.getRuleID() != null && uiFormFieldResponseTO.getRuleID() != 0) ? uiFormFieldResponseTO.getRuleID() : null);
            uiFormField.setChatBotRresourceID(uiFormFieldResponseTO.getChatBotResourceID() != null ? uiFormFieldResponseTO.getChatBotResourceID() : null);
            uiFormField.setConfigurationValueChatBot(uiFormFieldResponseTO.getConfigurationValueChatBot() != null ? uiFormFieldResponseTO.getConfigurationValueChatBot() : null);
            uiFormField.setChatBotSequence(uiFormFieldResponseTO.getChatBotSequence() != null ? uiFormFieldResponseTO.getChatBotSequence() : null);
            getEntityManager("TalentPact").persist(uiFormField);

            return uiFormField.getFormFieldId();
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public boolean updateFieldData(UiFormFieldResponseTO uiFormFieldResponseTO) {
        // TODO Auto-generated method stub
        try {
            UiFormField uiFormField = getEntityManager("TalentPact").find(UiFormField.class, uiFormFieldResponseTO.getFormFieldID());
            uiFormField.setFieldGroupId(uiFormFieldResponseTO.getFieldGroupID());
            uiFormField.setSysEntityFieldId(uiFormFieldResponseTO.getEntityFieldID());
            uiFormField.setOrganizationID(uiFormFieldResponseTO.getOrganizationID());
            uiFormField.setClientId(uiFormFieldResponseTO.getClientID());
            uiFormField.setTenantId(1);
            uiFormField.setModifiedBy(uiFormFieldResponseTO.getModifiedBy());
            uiFormField.setModifiedDate(new Date());
            uiFormField.setActionType(uiFormFieldResponseTO.isAction());
            uiFormField.setApprovalRequired(uiFormFieldResponseTO.isApprovalRequired());
            uiFormField.setAttachmentEnabled(uiFormFieldResponseTO.isAttachmentEnabled());
            uiFormField.setMaster(uiFormFieldResponseTO.isMaster());
            uiFormField.setAttachmentRequired(uiFormFieldResponseTO.isAttachmentRequired());
            uiFormField.setEffectiveDateRequired(uiFormFieldResponseTO.isEffectiveDateRequired());
            uiFormField.setResourceId(uiFormFieldResponseTO.getResourceID());
            uiFormField.setMandatory(uiFormFieldResponseTO.isMandatory());
            uiFormField.setParentFormFieldId(uiFormFieldResponseTO.getParentFormFieldID());
            uiFormField.setRegex(uiFormFieldResponseTO.getRegex());
            uiFormField.setConfigurationValueMobile(uiFormFieldResponseTO.getConfigurationValueMobile());
            uiFormField.setConfigurationValueWeb(uiFormFieldResponseTO.getConfigurationValueWeb());
            uiFormField.setIsGroupParameter(uiFormFieldResponseTO.isGroupParameter());
            getEntityManager("TalentPact").merge(uiFormField);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Object[]> getSysBundle() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysBundle.BundleID ,sysBundle.Country,sysBundle.Language,sysBundle.Name,sysBundle.TenantID," + "sysBundle.Variant from SysBundle sysBundle");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean insertToSysResourceBundleNew(SysResourceBundleResponseTO sysResourceBundleResponseTO) {
        // TODO Auto-generated method stub

        try {
            SysResourceBundle sysResourceBundle = new SysResourceBundle();
            sysResourceBundle.setSysResource(getEntityManager("TalentPact").find(SysResource.class, sysResourceBundleResponseTO.getResourceID()));
            sysResourceBundle.setSysBundle(getEntityManager("TalentPact").find(SysBundle.class, sysResourceBundleResponseTO.getBundleID()));
            sysResourceBundle.setValueStr(sysResourceBundleResponseTO.getValue());
            sysResourceBundle.setTenantID(sysResourceBundleResponseTO.getTenantID());
            sysResourceBundle.setCreatedBy(sysResourceBundleResponseTO.getCreatedBy());
            sysResourceBundle.setModifiedBy(sysResourceBundleResponseTO.getModifiedBy());
            sysResourceBundle.setCreatedDate(new Date());
            sysResourceBundle.setModifiedDate(new Date());
            sysResourceBundle.setTitleStr(sysResourceBundleResponseTO.getTitle());
            getEntityManager("TalentPact").persist(sysResourceBundle);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean insertToSysResource(SysResourceTO sysResourceTO) {
        // TODO Auto-generated method stub

        try {
            SysResource sysResource = new SysResource();
            sysResource.setDescription(sysResourceTO.getDescription());
            sysResource.setSysBundle(getEntityManager("TalentPact").find(SysBundle.class, sysResourceTO.getBundleID()));
            sysResource.setSysTenantId(sysResourceTO.getTenant());
            sysResource.setValue(sysResourceTO.getValue());
            getEntityManager("TalentPact").persist(sysResource);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public int insertToSysResourceForm(SysResourceTO sysResourceTO) {
        // TODO Auto-generated method stub
        int resourceId = 0;
        try {
            SysResource sysResource = new SysResource();
            sysResource.setDescription(sysResourceTO.getDescription());
            sysResource.setSysBundle(getEntityManager("TalentPact").find(SysBundle.class, sysResourceTO.getBundleID()));
            sysResource.setSysTenantId(sysResourceTO.getTenant());
            sysResource.setValue(sysResourceTO.getValue());
            getEntityManager("TalentPact").persist(sysResource);

            resourceId = sysResource.getResourceId();

            return resourceId;
        } catch (Exception ex) {
            ex.printStackTrace();
            return resourceId;
        }
    }

    public Integer getresourceIdToPutInSysResourceBundle(SysResourceTO sysResourceTO) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Integer> result = null;
        Integer resourceId = 0;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select max(sysResource.resourceid) from  SysResource sysResource where  sysResource.Value='" + sysResourceTO.getValue() + "'");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            result = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (result != null && !result.isEmpty()) {
            resourceId = result.get(0);
        }
        return resourceId;
    }

    public boolean updateFormDataNew(UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO) {
        // TODO Auto-generated method stub

        try {
            UiFormFieldGroup uiFormGroup = getEntityManager("TalentPact").find(UiFormFieldGroup.class, uiFormFieldGroupResponseTO.getFieldGroupID());
            // uiFormGroup.setFormID(uiFormFieldGroupResponseTO.getFormID());
            uiFormGroup.setFieldGroupName(uiFormFieldGroupResponseTO.getFieldGroupName());
            uiFormGroup.setModifiedBy(uiFormFieldGroupResponseTO.getModifiedBy());
            uiFormGroup.setModifiedDate(new Date());
            uiFormGroup.setTenantId(uiFormFieldGroupResponseTO.getTenantID());
            getEntityManager("TalentPact").merge(uiFormGroup);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Object[]> getParentFormListByComType(int subModuleID, int moduleID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            if (moduleID == 0)
                hqlQuery.append("select uiform.FormID ,uiform.FormName from UiForm uiform where uiform.url is null and uiform.subModuleID = " + moduleID);
            else {
                hqlQuery.append("select uiform.FormID ,uiform.FormName from UiForm uiform where uiform.subModuleID = " + subModuleID + " or uiform.subModuleID =" + moduleID
                        + " and uiform.url is null ");
            }
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public Integer checkCombinationofBundleIdandResource(Integer bundleId, Integer resourceID) {
        // TODO Auto-generated method stub
        Query query = null;
        StringBuilder hqlQuery = null;
        Integer result = 0;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysResourceBundleNew.SysResourceBundleID from SysResourceBundleNew sysResourceBundleNew where bundleId = " + bundleId + " and resourceID ="
                    + resourceID);

            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());

            List<BigInteger> list = query.getResultList();
            if (list.size() != 0)
                result = ((BigInteger) list.get(0)).intValue();
            return result;

        } catch (Exception ex) {
            ex.printStackTrace();
            return result;
        }
    }

    public boolean mergeToSysResourceBundleNew(String nameOFLang, Integer bundleId, String textboxdata, Integer resourceID, Integer checkCombinationResult, Integer createdBy,
            Date createdDate, Date modifiedDate, Integer modifiedBy, String textBoxDataTitle) {
        // TODO Auto-generated method stub

        try {
            SysResourceBundle sysResourceBundle = getEntityManager("TalentPact").find(SysResourceBundle.class, new Long(checkCombinationResult));
            sysResourceBundle.setSysResource(getEntityManager("TalentPact").find(SysResource.class, resourceID));
            sysResourceBundle.setSysBundle(getEntityManager("TalentPact").find(SysBundle.class, bundleId));
            sysResourceBundle.setValueStr(textboxdata);
            sysResourceBundle.setTenantID(1);
            sysResourceBundle.setCreatedBy(createdBy);
            sysResourceBundle.setCreatedDate(createdDate);
            sysResourceBundle.setModifiedBy(modifiedBy);
            sysResourceBundle.setModifiedDate(modifiedDate);
            sysResourceBundle.setTitleStr(textBoxDataTitle);

            getEntityManager("TalentPact").merge(sysResourceBundle);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Object[]> getSysBundleDataForTextBox(Integer resourceID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysResourceBundleNew.SysResourceBundleID , sysResourceBundleNew.ResourceID , sysResourceBundleNew.BundleID ,sysResourceBundleNew.Value ,sysResourceBundleNew.Title from SysResourceBundleNew sysResourceBundleNew , sysBundle sysBundle "
                    + "where sysResourceBundleNew.BundleID = sysBundle.BundleID and sysResourceBundleNew.ResourceID =" + resourceID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean UpdateFormFieldData(UiFormFieldResponseTO uiFormFieldResponseTO) {
        // TODO Auto-generated method stub
        try {
            UiFormField uiFormField = getEntityManager("TalentPact").find(UiFormField.class, uiFormFieldResponseTO.getFormFieldID());

            uiFormField.setFieldGroupId(uiFormFieldResponseTO.getFieldGroupID());
            uiFormField.setEntityColumnId(uiFormFieldResponseTO.getEntityFieldID());
            uiFormField.setOrganizationID(uiFormFieldResponseTO.getOrganizationID());
            uiFormField.setClientId(uiFormFieldResponseTO.getClientID());
            uiFormField.setTenantId(1);
            uiFormField.setModifiedBy(uiFormFieldResponseTO.getModifiedBy());
            uiFormField.setModifiedDate(new Date());
            uiFormField.setActionType(uiFormFieldResponseTO.isAction());
            uiFormField.setApprovalRequired(uiFormFieldResponseTO.isApprovalRequired());
            uiFormField.setAttachmentEnabled(uiFormFieldResponseTO.isAttachmentEnabled());
            uiFormField.setMaster(uiFormFieldResponseTO.isMaster());
            uiFormField.setAttachmentRequired(uiFormFieldResponseTO.isAttachmentRequired());
            uiFormField.setEffectiveDateRequired(uiFormFieldResponseTO.isEffectiveDateRequired());
            uiFormField.setResourceId(uiFormFieldResponseTO.getResourceID());
            uiFormField.setMandatory(uiFormFieldResponseTO.isMandatory());
            uiFormField.setParentFormFieldId(uiFormFieldResponseTO.getParentFormFieldID());
            uiFormField.setRegex(uiFormFieldResponseTO.getRegex());
            uiFormField.setConfigurationValueMobile(uiFormFieldResponseTO.getConfigurationValueMobile());
            uiFormField.setConfigurationValueWeb(uiFormFieldResponseTO.getConfigurationValueWeb());
            uiFormField.setIsGroupParameter(uiFormFieldResponseTO.isGroupParameter());
            uiFormField.setSysFieldTypeID(uiFormFieldResponseTO.getSysFieldTypeID() != null && uiFormFieldResponseTO.getSysFieldTypeID() != 0 ? uiFormFieldResponseTO
                    .getSysFieldTypeID() : null);
            uiFormField.setSysRuleID(uiFormFieldResponseTO.getRuleID() != null && uiFormFieldResponseTO.getRuleID() != 0 ? uiFormFieldResponseTO.getRuleID() : null);
            uiFormField.setChatBotRresourceID(uiFormFieldResponseTO.getChatBotResourceID() != null ? uiFormFieldResponseTO.getChatBotResourceID() : null);
            uiFormField.setConfigurationValueChatBot(uiFormFieldResponseTO.getConfigurationValueChatBot() != null ? uiFormFieldResponseTO.getConfigurationValueChatBot() : null);
            uiFormField.setChatBotSequence(uiFormFieldResponseTO.getChatBotSequence() != null ? uiFormFieldResponseTO.getChatBotSequence() : null);
            getEntityManager("TalentPact").merge(uiFormField);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public List<Object[]> getEntityFieldData(Integer groupId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            // hqlQuery.append(" select sysentityfield.EntityFieldID,
            // sysentityfield.Field from SysEntityField sysentityfield where
            // tenantId=1 );
            hqlQuery.append("select sysentitycolumn.EntityColumnID, sysentitycolumn.DBColumnName from sysentitycolumn sysentitycolumn where "
                    + " tenantId=1 and sysentitycolumn.sysentityID = ( select uf.primaryEntityID from uiform uf where uf.formid in "
                    + " (select distinct ufg.FormID from UiFormFieldGroup ufg where ufg.fieldGroupID =" + groupId + "  ))");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getParentFormFieldData(Integer formgroupid) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiformfield.FormFieldID,uiformfield.ClientId from UiFormField uiformfield where uiformfield.FieldGroupID=" + formgroupid);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getSysResourceData() {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select s.ResourceID,s.Value from sysresource s where s.BundleID =1");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public boolean groupnameExists(String fieldGroupName, Integer formID, Integer groupId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ug.fieldGroupName from UiFormFieldGroup ug where ug.FieldGroupName = '" + fieldGroupName + "' and formID =" + formID
                    + "and ug.fieldGroupID <> " + groupId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                return true;
            } else
                return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean clientExists(String clientID, Integer formFieldID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uf.ClientId from UiFormField uf where uf.ClientId='" + clientID + "' and uf.FormFieldID <> " + formFieldID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                return true;
            } else
                return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean clientExistsExcel(String clientID, Integer groupId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uf.ClientId from UiFormField uf where uf.ClientId='" + clientID + "' and uf.fieldgroupId = " + groupId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                return true;
            } else
                return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean formNameExists(String formName, Integer formID, int moduleSubmoduleID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select * from uiform uf where uf.FormName = '" + formName + "' and uf.formid <> " + formID + " and uf.subModuleID = " + moduleSubmoduleID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                return true;
            } else
                return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public String earlierformName(Integer formID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<String> list = null;
        String result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uf.FormName from uiform uf where uf.formid = '" + formID + "'");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                result = (list.get(0)).toString();
            }
            if (list == null) {
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return result;
        }
        return result;
    }

    public Integer getresourceIDSysResource(String formName) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Integer> list = null;
        String result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sr.ResourceID from SysResource sr where Value =  '" + formName + "'");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                Integer value = Integer.parseInt(list.get(0).toString());
                if (value != null)
                    return value;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            return 0;
        }
        return null;
    }

    public List<Object[]> getGroupNameForExcel(Integer formId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select ufg.FieldGroupName , ufg.FieldGroupID  from UiFormFieldGroup ufg where formID =" + formId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public List<Object[]> getEntityNameForExcel(Integer formId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select  sysentitycolumn.DBColumnName ,sysentitycolumn.EntityColumnID from sysentitycolumn sysentitycolumn where "
                    + " tenantId=1 and sysentitycolumn.sysentityID = ( select uf.primaryEntityID from uiform uf where uf.formid = " + formId + ")");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public Integer getParentidforExcel(String stringCellValue, Integer groupIdOfCurrentIteration) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Integer> list = null;
        String result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uff.formfieldID from uiformfield uff where uff.fieldgroupId = " + groupIdOfCurrentIteration + " and uff.clientid = '" + stringCellValue + "'");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
            if (list != null && list.size() != 0) {
                Integer value = Integer.parseInt(list.get(0).toString());
                if (value != null)
                    return value;
            } else {
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    public String getFormNameForExcel(Integer formId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<String[]> list = null;
        String formName = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uff.formName from uiform uff where uff.formID = " + formId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            formName = query.getResultList().get(0).toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return formName;
    }

    public int UpdateToSysResourceForm(SysResourceTO sysResourceTO) {
        // TODO Auto-generated method stub
        int resourceId = 0;
        try {
            SysResource sysResource = getEntityManager("TalentPact").find(SysResource.class, sysResourceTO.getResourceId().intValue());

            if (sysResource != null) {
                sysResource.setDescription(sysResourceTO.getDescription());
                sysResource.setSysBundle(getEntityManager("TalentPact").find(SysBundle.class, sysResourceTO.getBundleID()));
                sysResource.setSysTenantId(sysResourceTO.getTenant());
                sysResource.setValue(sysResourceTO.getValue());
                getEntityManager("TalentPact").merge(sysResource);

                resourceId = sysResource.getResourceId();
            }
            return resourceId;
        } catch (Exception ex) {
            ex.printStackTrace();
            return resourceId;
        }
    }

    public List<Object[]> getSubModuleListUpdate(int subModuleID) {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> list = null;
        try {
            hqlQuery = new StringBuilder();
            // hqlQuery.append("select sysSubModule.ModuleID ,sysSubModule.ModuleName from SysSubModule sysSubModule where  sysSubModule.ParentModuleID is not null ");
            hqlQuery.append("select sysSubModule.ModuleID ,sysSubModule.ModuleName from SysSubModule sysSubModule where sysSubModule.ModuleID in (select sysSubModule1.ParentModuleID from SysSubModule sysSubModule1 where sysSubModule1.ModuleID  = "
                    + subModuleID + ")");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            list = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public Integer getMaxSequence(Integer moduleID, Integer formId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        int sequence = 0;
        try {
            hqlQuery = new StringBuilder();
            if (moduleID == 0)
                return 0;

            hqlQuery.append("select uiform.ParentFormID from UiForm uiform where uiForm.FormID = " + formId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            List list1 = query.getResultList();
            if (list1 != null && list1.size() > 0 && list1.get(0) != null && (Integer) list1.get(0) == moduleID) {
                hqlQuery.setLength(0);
                hqlQuery.append("select sequence from UiForm uiform where uiform.FormId=" + formId);
                query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
                List list = query.getResultList();
                if (list != null && list.size() > 0) {
                    if (list.get(0) != null)
                        sequence = (Integer) list.get(0);
                }
            } else {
                hqlQuery.setLength(0);
                hqlQuery.append("select ISNULL(max(uiform.sequence),0)+1 from UiForm uiform where uiform.ParentFormID = " + moduleID);
                query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
                List list = query.getResultList();
                if (list != null && list.size() > 0) {
                    if (list.get(0) != null)
                        sequence = (Integer) list.get(0);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sequence;
    }

    public boolean checkSequenceExists(Integer parentFormID, Integer sequence, Integer formid) {

        Query query = null;
        StringBuilder hqlQuery = null;
        boolean result = false;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select COUNT(*)  from UiForm uiform where  uiform.ParentFormID = " + parentFormID + " and uiform.Sequence = " + sequence + " and  uiform.FormID <> "
                    + formid);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            List list = query.getResultList();
            if (list != null && list.size() > 0) {
                if (list.get(0) != null && list.get(0).equals(0)) {
                    result = true;
                } else {
                    result = false;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }
        return result;
    }

    // code for Menu

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public Map<Long, List<MenuTO>> getAllMenuMap(Integer bundleId)
        throws Exception {
        StringBuilder hqlQuery = null;
        Query query = null;
        Map<Long, List<MenuTO>> menuCacheMap = null;
        List<MenuTO> menuTOList = null;
        List<MenuTO> tempWorkingMenuTOList = null;
        Integer bundleID = null;

        try {
            bundleID = bundleId;
            hqlQuery = new StringBuilder();
            // hqlQuery.append("select uf.formID , uf.sysResource.sysResourceBundleNew.value,uf.sequence,uf.parentFormID ,uf.URL, uf.menuCategory ,uf.defaultClassCall, uf.subModuleID  "
            // +
            // " from UiForm uf where uf.menu=1 and uf.sysResource.sysResourceBundleNew.bundleID=:bundleID order by uf.sequence");

            hqlQuery.append(" select ");
            hqlQuery.append(" uf.formId, srbn.value,uf.sequence, uf.parentFormId ,uf.URL, uf.menuCategory, ");
            hqlQuery.append(" uf.defaultClassCall, uf.subModuleId ");
            hqlQuery.append(" from ");
            hqlQuery.append(" UiForm uf ");
            hqlQuery.append(" inner join uf.sysResource.resourceBundle srbn ");
            hqlQuery.append(" where ");
            hqlQuery.append(" uf.menuType=1 ");
            hqlQuery.append(" and srbn.bundleID=:bundleID ");
            hqlQuery.append(" order by uf.sequence ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("bundleID", bundleID);
            List<Object[]> resultList = query.getResultList();

            menuTOList = ConvertIntoMenuTOlist(resultList);
            tempWorkingMenuTOList = findMenuTOForAdminPortal(menuTOList);
            Collections.sort(tempWorkingMenuTOList);
            menuCacheMap = prepareMenuListForCache(tempWorkingMenuTOList);

            return menuCacheMap;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);

        }

    }

    private List<MenuTO> findMenuTOForAdminPortal(List<MenuTO> menuTOList)
        throws Exception {
        Set<MenuTO> adminPortalMenuTOlist = null;
        List<MenuTO> leafNodes = null;
        List<MenuTO> allMenuTOList = null;
        try {
            adminPortalMenuTOlist = new HashSet<MenuTO>();
            allMenuTOList = new ArrayList<MenuTO>();

            leafNodes = findAltMenuListByMenuCategory(menuTOList, 3);

            adminPortalMenuTOlist.addAll(leafNodes);

            getAllNodesRelatedTOAdminPoeratl(adminPortalMenuTOlist, leafNodes, menuTOList);

            allMenuTOList.addAll(adminPortalMenuTOlist);

            return allMenuTOList;
        } catch (Exception e) {
            throw e;
        }
    }

    private List<MenuTO> findAltMenuListByMenuCategory(List<MenuTO> sysMenuTOCachelist, Integer i)
        throws Exception {
        List<MenuTO> returnList = null;
        try {
            returnList = new ArrayList<MenuTO>();
            for (MenuTO menu : sysMenuTOCachelist) {
                // if (menu.getMenuCategory() == i) {
                returnList.add(menu);
                // }
            }
            return returnList;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            returnList = null;
        }
    }

    private void getAllNodesRelatedTOAdminPoeratl(Set<MenuTO> adminPortalMenuTOlist, List<MenuTO> leafNodes, List<MenuTO> menuTOList)
        throws Exception {
        List<MenuTO> parentList = null;
        try {

            parentList = findParentListForMenuTOList(leafNodes, menuTOList);
            if (!parentList.isEmpty()) {
                adminPortalMenuTOlist.addAll(parentList);
                getAllNodesRelatedTOAdminPoeratl(adminPortalMenuTOlist, parentList, menuTOList);
            }

        } catch (Exception e) {
            throw e;
        }

    }

    private List<MenuTO> findParentListForMenuTOList(List<MenuTO> leafNodes, List<MenuTO> menuTOList) {
        List<MenuTO> parentList = null;
        parentList = new ArrayList<MenuTO>();
        for (MenuTO lTO : leafNodes) {
            if (lTO.getParentMenuID() != null) {
                long lc = lTO.getParentMenuID();
                for (MenuTO mTO : menuTOList) {
                    long mc = mTO.getMenuID();
                    if (mc == lc) {
                        parentList.add(mTO);
                        break;
                    }
                }
            }
        }

        return parentList;
    }

    private List<MenuTO> ConvertIntoMenuTOlist(List<Object[]> resultList)
        throws Exception {
        List<MenuTO> listSysMenuResponseTO = null;
        MenuTO sysMenuResponseTO = null;

        try {

            // list = sysMenuDS.getAllSysMenu();
            listSysMenuResponseTO = new ArrayList<MenuTO>();
            for (Object object[] : resultList) {
                sysMenuResponseTO = new MenuTO();

                if (object[0] != null)
                    sysMenuResponseTO.setMenuID(Long.parseLong((object[0]).toString()));

                if (object[1] != null)
                    sysMenuResponseTO.setMenuName((String) object[1]);

                if (object[2] != null) {
                    sysMenuResponseTO.setSequence(Integer.parseInt((object[2]).toString()));
                }
                if (object[3] != null) {
                    sysMenuResponseTO.setParentMenuID(Long.parseLong((object[3]).toString()));
                }

                if (object[4] != null)
                    sysMenuResponseTO.setUrl((String) object[4]);

                if (object[5] != null) {
                    sysMenuResponseTO.setMenuCategory(Integer.parseInt((object[5]).toString()));
                }

                if (object[6] != null)
                    sysMenuResponseTO.setDefaultClass((String) object[6]);

                if (object[7] != null) {
                    sysMenuResponseTO.setModuleID(Long.parseLong((object[7]).toString()));
                }

                sysMenuResponseTO.setSelected(true);
                listSysMenuResponseTO.add(sysMenuResponseTO);
            }
            return listSysMenuResponseTO;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    private Map<Long, List<MenuTO>> prepareMenuListForCache(List<MenuTO> MenuTOlist)
        throws Exception {
        Map<Long, List<MenuTO>> menuCacheMap = null;
        List<MenuTO> nullList = null;
        try {
            nullList = new ArrayList<MenuTO>();
            menuCacheMap = new HashMap<Long, List<MenuTO>>();
            if (MenuTOlist == null || MenuTOlist.isEmpty()) {
                return menuCacheMap;
            }
            for (MenuTO menu : MenuTOlist) {
                if (menu.getParentMenuID() == null) {
                    nullList.add(menu);
                }
            }

            menuCacheMap.put((long) 0, nullList);
            for (MenuTO menu : nullList) {
                prepareMenuMap(MenuTOlist, menu, menuCacheMap);
            }

            return menuCacheMap;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            menuCacheMap = null;
            nullList = null;
        }
    }

    private void prepareMenuMap(List<MenuTO> MenuTOlist, MenuTO menu, Map<Long, List<MenuTO>> menuCacheMap)
        throws Exception {

        List<MenuTO> childList = null;

        try {
            childList = findChildMenuList(menu, MenuTOlist);
            if (childList == null | childList.isEmpty()) {
                return;
            } else {
                menuCacheMap.put(menu.getMenuID(), childList);
                for (MenuTO menu1 : childList) {
                    prepareMenuMap(MenuTOlist, menu1, menuCacheMap);
                }
            }

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }
    }

    private List<MenuTO> findChildMenuList(MenuTO menu, List<MenuTO> MenuTOlist)
        throws Exception {
        List<MenuTO> childList = null;
        try {
            childList = new ArrayList<MenuTO>();
            for (MenuTO menu1 : MenuTOlist) {
                if (menu1.getParentMenuID() != null) {
                    if (menu.getMenuID() == menu1.getParentMenuID()) {
                        childList.add(menu1);
                    }
                }
            }
            return childList;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }
    }

    @SuppressWarnings("unchecked")
    public List<SysBundleResponseTO> bundleDropDown()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysBundleResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select distinct sb.bundleId, sb.name,sb.language,sb.country,sb.sysTenantId from SysBundle sb order by sb.language");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getBundleDropDown(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysBundleResponseTO> getBundleDropDown(List<Object[]> resultList) {
        List<SysBundleResponseTO> list = null;
        SysBundleResponseTO responseTO = null;

        try {
            list = new ArrayList<SysBundleResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new SysBundleResponseTO();
                if (object[0] != null) {
                    responseTO.setBundleId((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setName((String) object[1]);
                }
                if (object[2] != null) {
                    responseTO.setLanguage((String) object[2]);
                }
                if (object[3] != null) {
                    responseTO.setCountry((String) object[3]);
                }
                if (object[4] != null) {
                    responseTO.setSysTenant((Integer) object[4]);
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {

        } finally {
            responseTO = null;
        }
        return list;
    }

    public boolean callProcedure(Integer formID) {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<String> resultList = new LinkedList<String>();
        String resultreturn = null;
        boolean value = false;
        try {
            System.out.println("Calling Proc.........");
            hqlQuery = new StringBuilder();
            hqlQuery.append("exec proc_form_group_field_data_merge " + formID);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null) {
                resultreturn = resultList.get(0).toString();
                if (resultreturn.equalsIgnoreCase("True")) {
                    value = true;
                }


            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
        return value;

    }

    public Integer getFormID(Integer grpId) {
        Query query = null;
        StringBuilder hqlQuery = null;
        Integer formid = 0;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select FormID from UiFormFieldGroup where FieldGroupID =  " + grpId);
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            List list1 = query.getResultList();
            if (list1 != null && list1.size() > 0) {
                if (list1.get(0) != null)
                    formid = (Integer) list1.get(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return formid;
    }

    public void deleteAllRedisKey() {
        // TODO Auto-generated method stub
        try {
            String patter = "ALTORGNIZE:MENU";
            RedisClient.deleteByPattern(patter + ":" + "*");
            String patter1 = "ALTORGANIZE:MENU";
            RedisClient.deleteByPattern(patter1 + ":" + "*");
            String patter2 = "ALTADMIN";
            RedisClient.deleteByPattern(patter2 + ":" + "*");
        } catch (RedisConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getUiFormsForModule(Integer moduleId) {
        StringBuilder qlString = new StringBuilder();
        qlString.append("select uiForm.formId , uiForm.formName");
        qlString.append(" from UiForm uiForm where uiForm.subModuleId=:moduleId");
        qlString.append(" order by uiForm.formName asc");
        return getEntityManager("TalentPact").createQuery(qlString.toString()).setParameter("moduleId", moduleId).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<SysFieldTypeResponseTO> fieldTypeDropDown()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysFieldTypeResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select sft.sysFieldTypeID, sft.fieldType from SysFieldType sft ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getFieldTypeDropDown(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysFieldTypeResponseTO> getFieldTypeDropDown(List<Object[]> resultList) {
        List<SysFieldTypeResponseTO> list = null;
        SysFieldTypeResponseTO responseTO = null;

        try {
            list = new ArrayList<SysFieldTypeResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new SysFieldTypeResponseTO();
                if (object[0] != null) {
                    responseTO.setSysFieldTypeID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setFieldType((String) object[1]);
                }
                list.add(responseTO);
            }
        } catch (Exception ex) {

        } finally {
            responseTO = null;
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<SysRuleResponseTO> methodDropDown()
        throws Exception {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> resultList = null;
        List<SysRuleResponseTO> list = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select sr.ruleID, sr.ruleClassName, sr.ruleMethod from SysRule sr where sr.sysContentType.sysTypeId = 1447 ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            resultList = query.getResultList();
            if (resultList != null && !resultList.isEmpty()) {
                list = getMethodDropDown(resultList);
            }
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            hqlQuery = null;
            query = null;
            resultList = null;
        }
    }

    public static List<SysRuleResponseTO> getMethodDropDown(List<Object[]> resultList) {
        List<SysRuleResponseTO> list = null;
        SysRuleResponseTO responseTO = null;

        try {
            list = new ArrayList<SysRuleResponseTO>();
            for (Object[] object : resultList) {
                responseTO = new SysRuleResponseTO();
                if (object[0] != null) {
                    responseTO.setRuleID((Integer) object[0]);
                }
                if (object[1] != null) {
                    responseTO.setRuleClassMethodName(((String) object[1]) + "." + (((String) object[2])));
                }

                list.add(responseTO);
            }
        } catch (Exception ex) {

        } finally {
            responseTO = null;
        }
        return list;
    }

    public boolean checkBotSequenceExists(Integer fieldGroupID, Integer sequence) {

        Query query = null;
        StringBuilder hqlQuery = null;
        boolean result = false;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select uiFormField.BotSequence from UiFormField uiFormField ");
            hqlQuery.append("where uiFormField.FieldGroupID in (select uiFormFieldGroup.FieldGroupID from UiFormFieldGroup uiFormFieldGroup ");
            hqlQuery.append("where uiFormFieldGroup.FormID in (select uiFieldGroup.FormID from UiFormFieldGroup uiFieldGroup where uiFieldGroup.FieldGroupID = :fieldGroupID ))");
            query = getEntityManager("Talentpact").createNativeQuery(hqlQuery.toString());
            query.setParameter("fieldGroupID", fieldGroupID);
            List list = query.getResultList();
            result = list.contains(sequence);
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Integer getBotSequenceByFormFieldID(Integer formFieldID) {
        StringBuilder qlString = new StringBuilder();
        qlString.append("select uiFormField.chatBotSequence");
        qlString.append(" from UiFormField uiFormField where uiFormField.formFieldId=:formFieldId");
        return (Integer) getEntityManager("TalentPact").createQuery(qlString.toString()).setParameter("formFieldId", formFieldID).getSingleResult();
    }
}