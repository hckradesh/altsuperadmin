package com.talentpact.business.dataservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.talentpact.auth.entity.TpTenant;
import com.talentpact.business.application.transport.input.AppRequestTO;
import com.talentpact.model.HrModule;
import com.talentpact.model.SysModule;
import com.talentpact.model.SysSubModule;


@Stateless
public class AppModuleDS extends AbstractDS<AppModuleDS>
{


    public AppModuleDS()
    {
        super(AppModuleDS.class);

    }


    /* code for retrieving modules from appID */
    public List<Object[]> getModulesFromAppId(List<AppRequestTO> apprequestList)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> tpModuleList = null;
        List<Long> appIDs = null;
        try {

            if (apprequestList != null && apprequestList.size() != 0) {
                appIDs = new ArrayList<Long>();

                for (AppRequestTO appRequestTO : apprequestList) {
                    appIDs.add(appRequestTO.getAppId());
                }

                hqlQuery = new StringBuilder();
                hqlQuery.append("select tpModule.moduleID,tpModule.moduleName,tpModule.moduleDesc,tpModule.appID.appID from SysModule tpModule where tpModule.appID.appID in (:appIDs)");
                query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
                query.setParameter("appIDs", appIDs);
                tpModuleList = query.getResultList();

            }// end of if
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpModuleList;
    }


    public List<SysSubModule> getAllModules()
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysSubModule> tpPortalTypeList = null;
        TpTenant result = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" from SysSubModule ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            tpPortalTypeList = query.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpPortalTypeList;
    }


    public List<Object[]> getSysConstant(List<Integer> constantCategoryId)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> sysConstantList = null;
        if (constantCategoryId != null) {
            StringBuilder build = new StringBuilder();
            for (int i = 0; i < constantCategoryId.size(); i++) {

                if (i == (constantCategoryId.size() - 1)) {
                    build.append(constantCategoryId.get(i));
                } else {
                    build.append(constantCategoryId.get(i)).append(",");
                }

            }
            String inList = build.toString();

            try {
                hqlQuery = new StringBuilder();
                Boolean status = true;
                hqlQuery.append(" select  syscat.SysConstantCategoryName , syscon.SysConstantname from SysConstant syscon inner join "
                        + "SysConstantCategory syscat on syscon.SysConstantCategoryID = syscat.SysConstantCategoryId" + "  where syscat.sysconstantcategoryid  in (" + inList
                        + "  ) and syscon.IsActive =:status");


                query = getEntityManager("AltCommon").createNativeQuery(hqlQuery.toString());
                query.setParameter("status", status);
                sysConstantList = query.getResultList();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return sysConstantList;
    }


    /* code for getting org module  from organization id and modileid */

    public HrModule findAltModule(long moduleID, int orgID)
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrModule> tpOrgModuleList = null;
        HrModule tpOrgModule = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpOrgModule from HrModule tpOrgModule where tpOrgModule.sysModuleId=:moduleID and tpOrgModule.organizationId=:orgID");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("moduleID", new Long(moduleID).intValue());
            query.setParameter("orgID", orgID);
            tpOrgModuleList = query.getResultList();
            if (tpOrgModuleList != null && tpOrgModuleList.size() != 0) {
                tpOrgModule = tpOrgModuleList.get(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpOrgModule;
    }

    /* code for perrsisting  org modules */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persistTpOrgModule(HrModule tpOrgModule)
    {
        try {

            getEntityManager("TalentPact").persist(tpOrgModule);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /* code for merging  org modules */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void mergeTpOrgModule(HrModule tpOrgModule)
    {
        try {

            getEntityManager("TalentPact").merge(tpOrgModule);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    /* code for retrieving modules from appID */
    public List<Object[]> getAltModulesFromAppId(List<AppRequestTO> apprequestList)
    {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<Object[]> tpModuleList = null;
        List<Long> appIDs = null;
        try {

            if (apprequestList != null && apprequestList.size() != 0) {
                appIDs = new ArrayList<Long>();

                for (AppRequestTO appRequestTO : apprequestList) {
                    appIDs.add(appRequestTO.getAppId());
                }

                hqlQuery = new StringBuilder();
                hqlQuery.append("select altOrgModule.moduleID.moduleID,altOrgModule.moduleID.moduleName,altOrgModule.moduleID.moduleDesc,altOrgModule.moduleID.appID,altOrgModule.active from AltModule altOrgModule where tpOrgModule.moduleID.appID in (:appIDs)");
                query = getEntityManager("AltCommon").createQuery(hqlQuery.toString());
                query.setParameter("appIDs", appIDs);
                tpModuleList = query.getResultList();

            }// end of if
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpModuleList;
    }


    public SysModule findAltModule(long moduleID)
    {

        SysModule tpModule = null;

        try {
            tpModule = getEntityManager("AltCommon").find(SysModule.class, moduleID);

        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        return tpModule;
    }


    public List<HrModule> getAltModule(Integer orgID, Integer tenantID)
    {

        Query query = null;
        StringBuilder hqlQuery = null;
        List<HrModule> tpOrgModuleList = null;
        HrModule tpOrgModule = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select tpOrgModule from HrModule tpOrgModule where ");

            if (orgID != null) {
                hqlQuery.append("  tpOrgModule.organizationId=:orgID ");
            }
            if (tenantID != null) {
                hqlQuery.append(" and  tpOrgModule.tenantId=:tenantID ");
            }

            hqlQuery.append(" and  tpOrgModule.active=1 ");

            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());

            if (orgID != null) {
                query.setParameter("orgID", orgID);
            }
            if (tenantID != null) {
                query.setParameter("tenantID", tenantID);
            }

            tpOrgModuleList = query.getResultList();


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpOrgModuleList;
    }


}
