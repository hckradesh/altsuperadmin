package com.talentpact.business.sysModule.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.common.converter.IConverter;
import com.talentpact.model.SysModule;
import com.talentpact.ui.sysModule.common.transport.output.SysModuleResponseTO;

/**
 * 
 * @author raghvendra.mishra
 *
 */
public class SysModuleConverter implements IConverter {
	
	private static final Logger LOGGER = Logger.getLogger(SysModuleConverter.class.toString());

	/**
	 * 
	 * @param sysModuleList
	 * @return
	 * @author raghvendra.mishra
	 */
	public List<SysModuleResponseTO> convertSysEntityToDataTO(List<SysModule> sysModuleList){
		List<SysModuleResponseTO> resultList = null;
		SysModuleResponseTO sysModuleResponseTO = null;
		try {
			resultList = new ArrayList<SysModuleResponseTO>();
			for (SysModule sysModule : sysModuleList) {
				sysModuleResponseTO = new SysModuleResponseTO();
				sysModuleResponseTO.setModuleID(sysModule.getModuleId());
				sysModuleResponseTO.setModuleName(sysModule.getModuleName());
				sysModuleResponseTO.setModuleDesc(sysModule.getModuleDesc());
				sysModuleResponseTO.setSysTenant(sysModule.getSysTenant());
				sysModuleResponseTO.setAppID(sysModule.getAppID());
				sysModuleResponseTO.setCreatedBy(sysModule.getCreatedBy());
				sysModuleResponseTO.setModifiedBy(sysModule.getModifiedBy());
				sysModuleResponseTO.setCreatedDate(SQLDateHelper.getSqlTimeStampFromDate(sysModule.getCreatedDate()));
				sysModuleResponseTO.setModifiedDate(SQLDateHelper.getSqlTimeStampFromDate(sysModule.getModifiedDate()));
				sysModuleResponseTO.setModuleCode(sysModule.getModuleCode());
				sysModuleResponseTO.setAppCode(sysModule.getAppCode());
				resultList.add(sysModuleResponseTO);
			}
		} catch (Exception ex) {
			LOGGER.error("", ex);
		}
		return resultList;
	}
}
