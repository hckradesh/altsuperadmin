package com.talentpact.business.dataservice.sysChatBotValidationType;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.model.SysChatBotValidationType;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysChatBotValidationTypeService extends CommonService {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationTypeService.class);

	@EJB
	SysChatBotValidationTypeDS sysChatBotValidationTypeDS;

	public List<SysChatBotValidationTypeTO> getSysChatBotValidationTypeList()
			throws Exception {
		
		List<Object[]> result = null;
		List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList = null;
		sysChatBotValidationTypeTOList = new ArrayList<SysChatBotValidationTypeTO>();
		try {
			result = sysChatBotValidationTypeDS.getSysChatBotValidationTypeListing();
			for (Object[] o : result) {
				SysChatBotValidationTypeTO sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
				 if(o[0]!=null){
				sysChatBotValidationTypeTO.setSysValidationTypeID((Integer) o[0]);}
				 if(o[1]!=null){
				sysChatBotValidationTypeTO.setSysFieldTypeID((Integer) o[1]);}
				 if(o[2]!=null){
				sysChatBotValidationTypeTO.setFieldType((String) o[2]);}
				 if(o[3]!=null){
				sysChatBotValidationTypeTO.setNoOfParameters((Integer) o[3]);}
				 if(o[4]!=null){
				sysChatBotValidationTypeTO.setLabel((String) o[4]);}
				sysChatBotValidationTypeTOList.add(sysChatBotValidationTypeTO);
			}
			
			return sysChatBotValidationTypeTOList;
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			throw new Exception(ex);
		}
	}

	public List<SysChatBotValidationTypeTO> getUpdateSysChatBotValidationType(
			Integer sysValidationTypeID) throws Exception {
		List<SysChatBotValidationType> sysChatBotValidationTypeList = null;
		List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList = null;
		sysChatBotValidationTypeTOList = new ArrayList<SysChatBotValidationTypeTO>();
		try {
			sysChatBotValidationTypeList = sysChatBotValidationTypeDS.getUpdateSysChatBotValidationType(sysValidationTypeID);
					
			if (sysChatBotValidationTypeList != null
					&& !sysChatBotValidationTypeList.isEmpty()) {
				for (SysChatBotValidationType sFT : sysChatBotValidationTypeList) {
					SysChatBotValidationTypeTO sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
					sysChatBotValidationTypeTO.setLabel(sFT.getLabel());
					sysChatBotValidationTypeTO.setNoOfParameters(sFT.getNoOfParameters());
					sysChatBotValidationTypeTO.setSysFieldTypeID(sFT.getSysFieldTypeID());
					sysChatBotValidationTypeTOList.add(sysChatBotValidationTypeTO);
				}
			}
			return sysChatBotValidationTypeTOList;
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			throw new Exception(ex);
		}
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void saveSysChatBotValidationType(
			SysChatBotValidationTypeTO sysChatBotValidationTypeTO) {
		try {
			sysChatBotValidationTypeDS.saveSysChatBotValidationType(sysChatBotValidationTypeTO);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}

	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void updateSysChatBotValidationType(
			SysChatBotValidationTypeTO sysChatBotValidationTypeTO) {
		try {
			sysChatBotValidationTypeDS
					.updateSysChatBotValidationType(sysChatBotValidationTypeTO);
		} catch (Exception e) {
			LOGGER_.error("", e);
		}
	}
	
	public List<SysChatBotValidationTypeTO> ChatBotValidationTypeDropdown(Integer sysFieldTypeID )
			throws Exception {
		
		List<SysChatBotValidationType> sysChatBotValidationTypeList = null;
		List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList = null;
		sysChatBotValidationTypeTOList = new ArrayList<SysChatBotValidationTypeTO>();
		try {
			sysChatBotValidationTypeList = sysChatBotValidationTypeDS.ChatBotValidationTypeDropdown(sysFieldTypeID);
					
			if (sysChatBotValidationTypeList != null
					&& !sysChatBotValidationTypeList.isEmpty()) {
				for (SysChatBotValidationType sFT : sysChatBotValidationTypeList) {
					SysChatBotValidationTypeTO sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
					sysChatBotValidationTypeTO.setSysValidationTypeID(sFT.getSysValidationTypeID());
					sysChatBotValidationTypeTO.setLabel(sFT.getLabel());
					sysChatBotValidationTypeTO.setNoOfParameters(sFT.getNoOfParameters());
					sysChatBotValidationTypeTO.setSysFieldTypeID(sFT.getSysFieldTypeID());
					sysChatBotValidationTypeTOList.add(sysChatBotValidationTypeTO);
				}
			}
			return sysChatBotValidationTypeTOList;
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			throw new Exception(ex);
		}
	}
	
	@SuppressWarnings("null")
	public SysChatBotValidationTypeTO findSysChatBotValidationType(Integer sysValidationTypeID)
			throws Exception {
		SysChatBotValidationType sysChatBotValidationType = null;
		SysChatBotValidationTypeTO sysChatBotValidationTypeTO = null;
		try {
			sysChatBotValidationType = sysChatBotValidationTypeDS.findSysChatBotValidationType(sysValidationTypeID);
			if(sysChatBotValidationType != null)
			{
				sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
			
				sysChatBotValidationTypeTO.setSysValidationTypeID(sysChatBotValidationType.getSysValidationTypeID() != null ? sysChatBotValidationType.getSysValidationTypeID():null);
				sysChatBotValidationTypeTO.setSysFieldTypeID(sysChatBotValidationType.getSysFieldTypeID() != null ? sysChatBotValidationType.getSysFieldTypeID():null);
				sysChatBotValidationTypeTO.setLabel(sysChatBotValidationType.getLabel() != null ? sysChatBotValidationType.getLabel():null);
				sysChatBotValidationTypeTO.setNoOfParameters(sysChatBotValidationType.getNoOfParameters() != null ? sysChatBotValidationType.getNoOfParameters():null);
			}
			return sysChatBotValidationTypeTO;
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			throw new Exception(ex);
		}
		
	}
}// end of class