/**
 * 
 */
package com.talentpact.business.dataservice.SysConstantCategory;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.SysConstantCategoryRequestTO;
import com.talentpact.business.application.transport.output.SysConstantCategoryResponseTO;
import com.talentpact.business.common.service.impl.CommonService;

/**
 * @author pankaj.sharma1
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysConstantCategoryService extends CommonService
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantCategoryService.class);

    @EJB
    SysConstantCategoryDS sysConstantCategoryDS;

    @EJB
    SysConstantCategoryCache sysConstantCategoryCache;

    public List<SysConstantCategoryResponseTO> getSysConstantCategoryList()
        throws Exception
    {
        List<SysConstantCategoryResponseTO> list = null;
        try {
            list = sysConstantCategoryCache.getSysConstantCategoy();
            if (list == null) {
                throw new Exception("Data not found in Redis. Moving to DB");
            }
            return list;
        } catch (Exception e) {
            LOGGER_.info(e.getMessage());
            try {
                list = sysConstantCategoryDS.getSysConstantCategoryList();
                if (list != null && !list.isEmpty()) {
                    try {
                        sysConstantCategoryCache.persistSysConstantCategoy(list);
                    } catch (Exception ex) {
                        LOGGER_.error("Write failed in Redis : for  persistSysConstantCategoy");
                        LOGGER_.error(ex.getMessage());
                    }
                }
                return list;

            } catch (Exception ex) {
                LOGGER_.error(ex.getMessage());
                throw ex;
            }
        }
    }

    public void saveSysConstantCategory(SysConstantCategoryRequestTO sysConstantCategoryRequestTO)
        throws Exception
    {
        try {
            sysConstantCategoryDS.saveSysConstantCategory(sysConstantCategoryRequestTO);
            sysConstantCategoryCache.removeSysConstantCategoy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
