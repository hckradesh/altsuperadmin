package com.talentpact.business.dataservice.sysDSOPlaceHolder;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.impl.CommonService;
import com.talentpact.business.model.SysDSOPlaceholders;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;

@Stateless
public class SYSDSOPlaceHolderService extends CommonService{
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(SYSDSOPlaceHolderService.class);
	
	@EJB
	SysDSOPlaceHolderDS sysDSOPlaceHolderDS;
	
	public List<DSOPlaceHolderTO> getDSOPlaceHolderList() throws Exception{
        List<SysDSOPlaceholders> sysDSOPlaceholderList=null;
        List<DSOPlaceHolderTO> dsoPlaceHolderTOList=null;
        dsoPlaceHolderTOList=new ArrayList<DSOPlaceHolderTO>();
        try{
        	sysDSOPlaceholderList=sysDSOPlaceHolderDS.getDSOPlaceHolderList();
            if(sysDSOPlaceholderList!=null && !sysDSOPlaceholderList.isEmpty()){
                for(SysDSOPlaceholders dph : sysDSOPlaceholderList){
                	DSOPlaceHolderTO dspPlaceHolderTO=new DSOPlaceHolderTO();
                	dspPlaceHolderTO.setdSOPlaceholdersID(dph.getdSOPlaceholdersID());
                	dspPlaceHolderTO.setPlaceholderLabel(dph.getLabel());
                	dspPlaceHolderTO.setPlaceholderType(dph.getPlaceholderType());
                	dspPlaceHolderTO.setPlaceholderValue(dph.getPlaceholderValue());
                	dspPlaceHolderTO.setIsActive(dph.getIsActive());
                	dspPlaceHolderTO.setSelected(false);
                	dsoPlaceHolderTOList.add(dspPlaceHolderTO);
                }
            }
            return dsoPlaceHolderTOList; 
        }catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }
	
	 public void saveDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO) {
	        try{
	        	sysDSOPlaceHolderDS.saveDSOPlaceHolder(dsoPlaceHolderTO);
	        } catch (Exception e) {
	            LOGGER_.error("", e);
	        }
	        
	    }
	 
	 public void updateDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO){
	        try{
	        	sysDSOPlaceHolderDS.updateDSOPlaceHolder(dsoPlaceHolderTO);
	        } catch (Exception e) {
	            LOGGER_.error("", e);
	        }
	    }
	 
	 public List<DSOPlaceHolderTO> getUpdateDSOPlaceHolder(Integer dSOPlaceholdersID) throws Exception{
	        List<SysDSOPlaceholders> sysDSOPlaceholdersList=null;
	        List<DSOPlaceHolderTO> dsoPlaceHolderTOList=null;
	        dsoPlaceHolderTOList = new ArrayList<DSOPlaceHolderTO>();
	        try{
	        	sysDSOPlaceholdersList=sysDSOPlaceHolderDS.getUpdateSysFieldType(dSOPlaceholdersID);
	            if(sysDSOPlaceholdersList!=null && !sysDSOPlaceholdersList.isEmpty()){
	                for(SysDSOPlaceholders sdph : sysDSOPlaceholdersList){
	                	DSOPlaceHolderTO dsoPlaceHolderTO=new DSOPlaceHolderTO();
	                	dsoPlaceHolderTO.setdSOPlaceholdersID(sdph.getdSOPlaceholdersID());
	                	dsoPlaceHolderTO.setPlaceholderLabel(sdph.getLabel());
	                	dsoPlaceHolderTO.setPlaceholderType(sdph.getPlaceholderType());
	                	dsoPlaceHolderTO.setPlaceholderValue(sdph.getPlaceholderValue());
	                	dsoPlaceHolderTO.setIsActive(sdph.getIsActive());
	                	dsoPlaceHolderTOList.add(dsoPlaceHolderTO);
	                }
	            }
	            return dsoPlaceHolderTOList; 
	        }catch (Exception ex) {
	            LOGGER_.error("", ex);
	            throw new Exception(ex);
	        }
	    }
	 
		public List<DSOPlaceHolderTO> getEmailTypeDSOPlaceHolderList() throws Exception{
	        List<SysDSOPlaceholders> sysDSOPlaceholderList=null;
	        List<DSOPlaceHolderTO> dsoPlaceHolderTOList=null;
	        dsoPlaceHolderTOList=new ArrayList<DSOPlaceHolderTO>();
	        try{
	        	sysDSOPlaceholderList=sysDSOPlaceHolderDS.getEmailTypeDSOPlaceHolderList();
	            if(sysDSOPlaceholderList!=null && !sysDSOPlaceholderList.isEmpty()){
	                for(SysDSOPlaceholders dph : sysDSOPlaceholderList){
	                	DSOPlaceHolderTO dspPlaceHolderTO=new DSOPlaceHolderTO();
	                	dspPlaceHolderTO.setdSOPlaceholdersID(dph.getdSOPlaceholdersID());
	                	dspPlaceHolderTO.setPlaceholderLabel(dph.getLabel());
	                	dspPlaceHolderTO.setPlaceholderType(dph.getPlaceholderType());
	                	dspPlaceHolderTO.setPlaceholderValue(dph.getPlaceholderValue());
	                	dspPlaceHolderTO.setIsActive(dph.getIsActive());
	                	dspPlaceHolderTO.setSelected(false);
	                	dsoPlaceHolderTOList.add(dspPlaceHolderTO);
	                }
	            }
	            return dsoPlaceHolderTOList; 
	        }catch (Exception ex) {
	            LOGGER_.error("", ex);
	            throw new Exception(ex);
	        }
	    }
		
		
		public List<DSOPlaceHolderTO> getTextTypeDSOPlaceHolderList() throws Exception{
	        List<SysDSOPlaceholders> sysDSOPlaceholderList=null;
	        List<DSOPlaceHolderTO> dsoPlaceHolderTOList=null;
	        dsoPlaceHolderTOList=new ArrayList<DSOPlaceHolderTO>();
	        try{
	        	sysDSOPlaceholderList=sysDSOPlaceHolderDS.getTextTypeDSOPlaceHolderList();
	            if(sysDSOPlaceholderList!=null && !sysDSOPlaceholderList.isEmpty()){
	                for(SysDSOPlaceholders dph : sysDSOPlaceholderList){
	                	DSOPlaceHolderTO dspPlaceHolderTO=new DSOPlaceHolderTO();
	                	dspPlaceHolderTO.setdSOPlaceholdersID(dph.getdSOPlaceholdersID());
	                	dspPlaceHolderTO.setPlaceholderLabel(dph.getLabel());
	                	dspPlaceHolderTO.setPlaceholderType(dph.getPlaceholderType());
	                	dspPlaceHolderTO.setPlaceholderValue(dph.getPlaceholderValue());
	                	dspPlaceHolderTO.setIsActive(dph.getIsActive());
	                	dspPlaceHolderTO.setSelected(false);
	                	dsoPlaceHolderTOList.add(dspPlaceHolderTO);
	                }
	            }
	            return dsoPlaceHolderTOList; 
	        }catch (Exception ex) {
	            LOGGER_.error("", ex);
	            throw new Exception(ex);
	        }
	    }

}
