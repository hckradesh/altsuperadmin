package com.talentpact.business.common.constants;

/**
 * 
 * @author vivek.goyal
 *
 */

public interface ISysPolicyConstants
{

    public static String SYSPOLICY_ADD_DIALOG_HIDE = "PF('sysPolicyAddModal').hide();";

    public static String SYSPOLICY_EDIT_DIALOG_HIDE = "PF('sysPolicyEditModal').hide();";

    public static String SYSPOLICY_SAVE_ERROR_MSG = "Error occured while saving SysPolicy.";

    public static String SYSPOLICY_UPDATE_ERROR_MSG = "Error occured while updating SysPolicy.";

    public static String SYSPOLICY_SAVE_SUCCESS_MSG = "SysPolicy saved successfully.";

    public static String SYSPOLICY_UPDATE_SUCCESS_MSG = "SysPolicy updated successfully.";

    public static String SYSPOLICY_DATATABLE_RESET = "PF('sysPolicyDataTable').clearFilters();";

    public static String SYSPOLICY_PAGINATION_DATATABLE_RESET = "PF('sysPolicyDataTable').getPaginator().setPage(0);";

    public static String SYSPOLICY_DUPLICATE_ERROR_MSG = "SysPolicy already present in system.";
}
