package com.talentpact.ui.login.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.talentpact.business.authorization.constants.IAuthMessages;
import com.talentpact.business.authorization.transport.input.AuthRequestTO;
import com.talentpact.business.authorization.transport.output.AuthResponseTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.authorization.IAuthorizationRemote;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.login.bean.LoginBean;
import com.talentpact.ui.login.transport.input.AuthUserTO;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.tenants.newtenant.bean.NewTenantBean;
import com.talentpact.ui.tenants.organization.bean.NewOrganizationBean;

/**
 * @author radhamadhab.dalai
 *
 */
@ConversationScoped
@Named("loginActionController")
public class LoginActionController implements Serializable
{


    @EJB(beanName = "AuthorizationFacade")
    IAuthorizationRemote authorizationRemote;

    @Inject
    LoginBean loginBean;

    @Inject
    MenuBean menuBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    NewOrganizationBean newOrganizationBean;

    @Inject
    NewTenantBean newTenantBean;

    public void login()
    {
        String useName = null;
        String password = null;
        ServiceRequest request = null;
        ServiceResponse response = null;
        AuthRequestTO authRequestTO = null;
        AuthResponseTO authResponseTO = null;
        FacesContext facesContext = null;
        ExternalContext externalContext = null;
        HttpServletRequest httpRequest = null;
        String serverName = null;
        String appName = null;

        AuthUserTO tpUSerTO = null;
        try {
            facesContext = FacesContext.getCurrentInstance();
            externalContext = facesContext.getExternalContext();
            httpRequest = (HttpServletRequest) externalContext.getRequest();
            serverName = httpRequest.getServerName();
            appName = "TalentPactAdminPortal";

            useName = loginBean.getUserName();
            password = loginBean.getPassword();

            request = new ServiceRequest();
            authRequestTO = new AuthRequestTO();
            authRequestTO.setUserName(useName);
            authRequestTO.setPassword(password);
            authRequestTO.setUrlName(serverName);
            authRequestTO.setAppName(appName);
            authRequestTO.setTenantCode("SYSTEM");
            authRequestTO.setOrganizationCode("SYSTEM");

            request.setRequestTansportObject(authRequestTO);

            if ((useName != null && password != null) || !useName.equalsIgnoreCase("")) {

                response = authorizationRemote.authorizeUser(request);
                authResponseTO = (AuthResponseTO) response.getResponseTransportObject();
                if (authResponseTO.getConfirmationMessage().equalsIgnoreCase("Successfull")) {

                    tpUSerTO = authResponseTO.getTpUserTO();
                    userSessionBean.setUserID(tpUSerTO.getUserID());
                    newTenantBean.initialize();
                    loginBean.setMenuName("Tenant");
                    navigateHomePage("template/mainTemplate.jsf");
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IAuthMessages.LOGIN_FAILURE_MESSAGE, ""));
                    return;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        finally {

        }
    }

    public void navigateHomePage(String page)
    {
        try {
            menuBean.setPage("../tenants.xhtml");
            FacesContext.getCurrentInstance().getExternalContext().redirect(("/TalentPactAdminPortal/" + page));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        finally {

        }
    }

    public void logout()
    {
        try {
        	FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            FacesContext.getCurrentInstance().getExternalContext().redirect("/TalentPactAdminPortal/logout.jsf");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        finally {

        }
    }

    public void loginPage()
    {
        try {

            FacesContext.getCurrentInstance().getExternalContext().redirect("/TalentPactAdminPortal/login.jsf");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        finally {

        }
    }


}
