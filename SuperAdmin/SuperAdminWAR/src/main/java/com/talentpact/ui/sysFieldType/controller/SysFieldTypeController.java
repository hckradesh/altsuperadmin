package com.talentpact.ui.sysFieldType.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysFieldTypeConstants;
import com.talentpact.business.dataservice.sysFieldType.SysFieldTypeService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysFieldType.bean.SysFieldTypeBean;
import com.talentpact.ui.sysFieldType.to.SysFieldTypeTO;


@Named("sysFieldTypeController")
public class SysFieldTypeController extends CommonController implements Serializable, IDefaultAction  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysFieldTypeController.class);
	
	@Inject
    MenuBean menuBean;
	
	@Inject
    UserSessionBean userSessionBean;
	
	@Inject
	SysFieldTypeBean sysFieldTypeBean;
	
	@Inject
	SysFieldTypeService sysFieldTypeService;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		ServiceResponse response = null;
        try {
        	sysFieldTypeBean.setSysFieldTypeTOList(sysFieldTypeService.getSysFieldTypeList());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../sysFieldType/sysFieldType.xhtml");
            requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_DATATABLE_RESET);
            requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_PAGINATION_DATATABLE_RESET);

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
	}

	public void resetSysFieldType() {
		sysFieldTypeBean.setSysFieldTypeID(0);
		sysFieldTypeBean.setFieldType(null);
		sysFieldTypeBean.setSysFieldTypeTOList(null);
        
    }
	
	
	public void saveSysFieldType()
	        throws Exception {
			SysFieldTypeTO sysFieldTypeTO = null;
	        RequestContext request = null;
	        try {
	            request = RequestContext.getCurrentInstance();
	            sysFieldTypeTO = new SysFieldTypeTO();
	            sysFieldTypeTO.setFieldType(sysFieldTypeBean.getFieldType());
	            
	            if (validateFieldType(sysFieldTypeTO, false)) {
	            	sysFieldTypeService.saveSysFieldType(sysFieldTypeTO);
	                initialize();
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysFieldType added successfully", ""));
	                request.execute(ISysFieldTypeConstants.SYSFIELDTYPE_ADD_DIALOG_HIDE);
	                request.execute(ISysFieldTypeConstants.SYSFIELDTYPE_DATATABLE_RESET);
	                request.execute(ISysFieldTypeConstants.SYSFIELDTYPE_PAGINATION_DATATABLE_RESET);
	                request.update("sysFieldTypeListForm");
	            } else {
	                FacesContext.getCurrentInstance().addMessage(null,
	                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysFieldTypeConstants.SYSFIELDTYPE_ERROR_MSG, ""));
	            }
	        } catch (Exception e) {
	            FacesContext.getCurrentInstance().addMessage(null,
	                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysFieldTypeConstants.SYSFIELDTYPE_SAVE_ERROR_MSG, ""));
	            LOGGER_.error("", e);
	            throw e;
	        }

	    }
	
	public void editSysFieldType(SysFieldTypeTO sysFieldTypeTO) {
        try {
        	sysFieldTypeBean.setSysFieldTypeTO(sysFieldTypeTO);
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }
	 public void updateSysFieldType()
		        throws Exception {
		 		SysFieldTypeTO sysFieldTypeTO = null;
		        RequestContext requestContext = null;
		        try {
		            requestContext = RequestContext.getCurrentInstance();
		            sysFieldTypeTO = new SysFieldTypeTO();
		            sysFieldTypeTO.setSysFieldTypeID(sysFieldTypeBean.getSysFieldTypeTO().getSysFieldTypeID());
		            sysFieldTypeTO.setFieldType(sysFieldTypeBean.getSysFieldTypeTO().getFieldType().trim());
		            if (validateFieldType(sysFieldTypeTO, true)) {
		            	sysFieldTypeService.updateSysFieldType(sysFieldTypeTO);
		                initialize();
		                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysFieldType updated successfully", ""));
		                requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_EDIT_DIALOG_HIDE);
		                requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_DATATABLE_RESET);
		                requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_PAGINATION_DATATABLE_RESET);
		                requestContext.update("sysFieldTypeListForm");
		            } else {
		                FacesContext.getCurrentInstance().addMessage(null,
		                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysFieldTypeConstants.SYSFIELDTYPE_ERROR_MSG, ""));
		            }
		        } catch (Exception e) {
		            FacesContext.getCurrentInstance().addMessage(null,
		                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysFieldTypeConstants.SYSFIELDTYPE_UPDATE_ERROR_MSG, ""));
		            LOGGER_.error("", e);
		            throw e;
		        }
		    }
	 
	 @SuppressWarnings("unused")
	private boolean validateFieldType(SysFieldTypeTO sysFieldTypeTO, boolean edited)
		        throws Exception {
		        List<SysFieldTypeTO> sysFieldTypeTOList = null;
		        try {
		            if (edited) {
		            	sysFieldTypeTOList = sysFieldTypeService.getUpdateSysFieldType(sysFieldTypeTO.getSysFieldTypeID());
		            } else {
		            	sysFieldTypeTOList = sysFieldTypeService.getSysFieldTypeList();
		            }
		            if (sysFieldTypeTOList != null && !sysFieldTypeTOList.isEmpty()) {
		                for (SysFieldTypeTO sysFT : sysFieldTypeTOList) {
		                    if (sysFT.getFieldType().trim().equalsIgnoreCase(sysFieldTypeTO.getFieldType())) {
		                        return false;
		                    }
		                }
		            }
		            return true;
		        } catch (Exception ex) {
		            LOGGER_.error("", ex);
		            throw new Exception(ex);
		        }
		    }

}
