/**
 * 
 */
package com.talentpact.ui.hrContentType.dataprovider;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;
import com.talentpact.ui.hrContentType.dataservice.HrContentTypeDataService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class HrContentTypeDataProvider {

	@EJB
	private HrContentTypeDataService hrContentTypeDataService;
	/**
	 * @return
	 */
	public ServiceResponse getHrContentTypeByContentCategory() 
			throws Exception{
		ServiceResponse serviceResponse = null;
		List<HrContentTypeResponseTO> hrContentTypeResponseTOList = null;
		try{
			String contentCategory = "PlaceHolderType";
			hrContentTypeResponseTOList = 
					hrContentTypeDataService.getHrContentTypeByContentCategory(contentCategory);
			if(hrContentTypeResponseTOList!=null && 
					!hrContentTypeResponseTOList.isEmpty()){
				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObjectObjectList(hrContentTypeResponseTOList);
			}
		}catch(Exception ex){
			hrContentTypeResponseTOList = null;
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}

}
