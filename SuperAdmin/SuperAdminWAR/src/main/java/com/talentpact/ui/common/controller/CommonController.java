/**
 * 
 */
package com.talentpact.ui.common.controller;

import java.io.Serializable;

import javax.enterprise.context.Conversation;
import javax.inject.Inject;

/**
 * @author radhamadhab.dalai
 *
 */
public class CommonController implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 3476442879703879514L;
	@Inject
    private Conversation conversation;

    public void beginConversation()
    {

        if (conversation.isTransient()) {
            conversation.begin();
        }

    }

    public void endConversation()
    {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }
}
