package com.talentpact.ui.tenants.organization.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.TreeNode;

import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.security.transport.output.UiFormTO;
import com.talentpact.ats.to.admin.PortalTO;
import com.talentpact.business.application.loginserver.LoginServerTypeTO;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.application.transport.input.HrContentRequestTO;
import com.talentpact.business.application.transport.output.HrModuleTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysProductAnnouncementTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.application.transport.output.UserResponseTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tenants.transport.newtenant.output.OrgMoreInfoResponseTO;
import com.talentpact.business.tpModule.transport.input.TpModuleRequestTO;
import com.talentpact.business.tpOrganization.transport.input.AppPortalTypeUrlRequestTO;
import com.talentpact.business.tpOrganization.transport.input.VideoLinkTO;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.business.tpOrganization.transport.output.OrganizationResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.business.tpUrl.transport.input.UrlRequestTO;
import com.talentpact.insights.dashboardchart.to.DashboardChartTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.constants.Iconstants;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.tenants.organization.controller.NewOrganiztionController;

/**
 * @author radhamadhab.dalai
 * 
 */
@Named("newOrganizationBean")
@SessionScoped
public class NewOrganizationBean extends CommonBean implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = 39842934929479274L;

    @Inject
    NewOrganiztionController newOrganiztionController;

    private Long orgId;

    private String orgName;

    private String orgCode;

    private String dataLocation;

    private String url;
 
    private String categoryName;  
    
    private String externalUrl;

    private String contentName;

    private Long contentCategoryID;

    private Boolean saveFlag;

    private Boolean updateFlag;

    private Integer FormID;

    Map<Long, TreeNode> moduleTreeWithAppMap = null;

    Map<Long, TreeNode[]> selectedNodeByApp;

    private List<SysBundleResponseTO> sysBundle;

    private List<Integer> selectedBundleIds;

    private Integer landingPageID;

    public Long getContentCategoryID()
    {
        return contentCategoryID;
    }

    public void setContentCategoryID(Long contentCategoryID)
    {
        this.contentCategoryID = contentCategoryID;
    }

    public String getContentName()
    {
        return contentName;
    }

    public void setContentName(String contentName)
    {
        this.contentName = contentName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    private Integer statusID;

    private List<NewTenantResponseTO> tenantList;

    private List<SelectItem> appList;

    private List<SysRoleTypeTO> sysRoleTypeList;

    private List<HrContentRequestTO> hrContentRequestList;

    private List<SysMenuTO> sysMenuList;

    private List<SelectItem> portalThemeList;

    private List<UserResponseTO> userTOList;

    private List<UiFormResponseTO> sysCopyUIList;

    private List<UiFormResponseTO> sysCopyUIList1;

    private List<HrModuleTO> hrModuleList;

    private List<DashBoardTO> dashBoardTOlist;

    private List<DashboardChartTO> dashBoardChartTOlist;

    public List<DashboardChartTO> getDashBoardChartTOlist()
    {
        return dashBoardChartTOlist;
    }

    public void setDashBoardChartTOlist(List<DashboardChartTO> dashBoardChartTOlist)
    {
        this.dashBoardChartTOlist = dashBoardChartTOlist;
    }

    public List<HrContentRequestTO> getHrContentRequestList()
    {
        return hrContentRequestList;
    }

    public void setHrContentRequestList(List<HrContentRequestTO> hrContentRequestList)
    {
        this.hrContentRequestList = hrContentRequestList;
    }

    private List<SelectItem> selectedAppList;

    private List<UrlRequestTO> tpUrlList;

    private List<AppPortalTypeUrlRequestTO> filteredOrgList;

    private List<AppPortalTypeUrlRequestTO> appPortalUrlList;

    private Long tenantID;

    private Date contractSignedDate;

    private Date effectiveFromDate;

    private Date effectiveTODate;

    private List<TpModuleRequestTO> orgModuleList;

    private List<TpModuleRequestTO> filteredModuleList;

    private List<OrganizationResponseTO> allOrgList;

    private OrgAppPortalTreeResponseTO orgAppPortalTreeTO;

    private List<Long> selectedApp;

    private List<Long> selectedModule;

    private String userName;

    private String password;

    private Boolean status;

    private List<SysContentTypeTO> sysContentTypeList;

    private Integer employeeCount;

    List<SelectItem> selectLoginPageList;

    List<SelectItem> selectHomePageList;

    List<SelectItem> selectloginPageThemeList;

    List<SelectItem> selectdefualtThemeList;

    List<SelectItem> tenantResTOList;

    private List<UiFormTO> uiFormList;


    // List<SelectItem> selectDatabaseList;

    List<SelectItem> selectFeviconList;

    List<SelectItem> metaDatabaseList;

    TreeNode menuTree;

    TreeNode[] selectedMenuNodes;

    private String leftLogo;

    private String rightLogo;

    private String favicon;

    private String title;

    private String metaData;

    private String headingTxt;

    private String usernameTxt;

    private String passwordTxt;

    private String btnTxt;

    private String footer;

    private String banner;

    private String samlHeader;

    private String samlBtnTxt;

    private String resetPassword;

    private Boolean active;

    List<OrgMoreInfoResponseTO> allMOreInfoOrgList;

    private String line1;

    private String city;

    private String state;

    private String zipCode;

    private String country;

    private String district;

    private String email;

    private Long countryID;

    private Long stateID;

    private Long districtID;

    private Integer cityID;

    private Long zipCodeID;

    private boolean welcomeMail;

    public boolean isWelcomeMail()
    {
        return welcomeMail;
    }

    public void setWelcomeMail(boolean welcomeMail)
    {
        this.welcomeMail = welcomeMail;
    }

    List<AddressResponseTO> addressTOList;

    List<AddressResponseTO> countryTOList;

    List<AddressResponseTO> stateTOList;

    List<AddressResponseTO> districtTOList;

    List<AddressResponseTO> cityTOList;

    List<AddressResponseTO> zipCodeTOList;

    private String sectorCode;

    private Integer sectorID;

    private String sector;

    private Double customizationAmount;

    private Double recurringPerMonthAmount;

    private Double setupCost;

    List<SectorCostResponseTO> sectorTOList;

    private Date orgFinancialStartDate;

    private Date govtFinancialStartDate;

    private OrganizationResponseTO orgTO;

    private List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList;

    private Boolean mobileStatus;

    private String backGroundImage;

    private String contactNumber;

    private String eMail;

    private String externalVideoPath;


    private List<SysProductAnnouncementTO> sysProductAnnouncementTOList;

    private Date currentDate = new Date();

    private List<VideoLinkTO> onBoardingVideoLinkList = new ArrayList<VideoLinkTO>();

    private List<SysSubModuleResponseTO> moduleDropDownList;

    private Integer moduleID;

    private List<SysContentTypeTO> sysContentTypeTOList;

    private Integer sysContentTypeID;

    private Float attendanceDistanceLimit;

    private String selectedLanguages;

    private boolean ldapInfo;

    private String ldapServer;

    private String ldapSearchBase;

    private String ldapDomain;

    private boolean ldapActive;

    private String secretKey;

    private List<UiFormTO> selectLandingePagList;

    private boolean ldapWindowsAuthentication;
    
    private String ldapLogoutUrl;
    
    private boolean renderSP;
    
    private String spServer;
    
    private String spLogoutUrl;
    
    private boolean spActive;
    
    private List<TpAppResponseTO> partnerAppList;
    
    private List<String> selectedPartnerAppList;
    
    private List<TpAppResponseTO> customAppList;
    
    private List<String> selectedCustomAppList;

    public Date getCurrentDate()
    {
        return currentDate;
    }

    public List<VendorOfferingTemplateTO> getVendorOfferingTemplateTOList()
    {
        return vendorOfferingTemplateTOList;
    }

    public void setVendorOfferingTemplateTOList(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList)
    {
        this.vendorOfferingTemplateTOList = vendorOfferingTemplateTOList;
    }

    //added  

    AppPortalTypeUrlResponseTO selectedPortal;

    public Integer getSectorID()
    {
        return sectorID;
    }

    public void setSectorID(Integer sectorID)
    {
        this.sectorID = sectorID;
    }

    /*
     * private Long tpURLID;
     * 
     * private List<SelectItem> tpURLList;
     */

    /**
     * @return the tpURLList
     * 
     * 
     */

    public AppPortalTypeUrlResponseTO getSelectedPortal()
    {
        return selectedPortal;
    }

    public void setSelectedPortal(AppPortalTypeUrlResponseTO selectedPortal)
    {
        this.selectedPortal = selectedPortal;
    }

    /**
     * @return the effectiveFromDate
     */
    public Date getEffectiveFromDate()
    {
        return effectiveFromDate;
    }

    /**
     * @param effectiveFromDate
     *            the effectiveFromDate to set
     */
    public void setEffectiveFromDate(Date effectiveFromDate)
    {
        this.effectiveFromDate = effectiveFromDate;
    }

    /**
     * @return the effectiveTODate
     */
    public Date getEffectiveTODate()
    {
        return effectiveTODate;
    }

    /**
     * @param effectiveTODate
     *            the effectiveTODate to set
     */
    public void setEffectiveTODate(Date effectiveTODate)
    {
        this.effectiveTODate = effectiveTODate;
    }

    /**
     * @return the tenantID
     */
    public Long getTenantID()
    {
        return tenantID;
    }

    /**
     * @param tenantID
     *            the tenantID to set
     */
    public void setTenantID(Long tenantID)
    {
        this.tenantID = tenantID;
    }


    /**
     * @return the orgName
     */
    public String getOrgName()
    {
        return orgName;
    }

    /**
     * @param orgName
     *            the orgName to set
     */
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    /**
     * @return the orgCode
     */
    public String getOrgCode()
    {
        return orgCode;
    }

    /**
     * @param orgCode
     *            the orgCode to set
     */
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }

    /**
     * @return the dataLocation
     */
    public String getDataLocation()
    {
        return dataLocation;
    }

    /**
     * @param dataLocation
     *            the dataLocation to set
     */
    public void setDataLocation(String dataLocation)
    {
        this.dataLocation = dataLocation;
    }

    /**
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(String url)
    {
        this.url = url;
    }


    public void initialize()
    {
        endConversation();
        beginConversation();
        newOrganiztionController.initialize();

    }

    public Long getOrgId()
    {
        return orgId;
    }


    public void setOrgId(Long orgId)
    {
        this.orgId = orgId;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public List<UrlRequestTO> getTpUrlList()
    {
        return tpUrlList;
    }

    public void setTpUrlList(List<UrlRequestTO> tpUrlList)
    {
        this.tpUrlList = tpUrlList;
    }


    public Date getContractSignedDate()
    {
        return contractSignedDate;
    }

    public void setContractSignedDate(Date contractSignedDate)
    {
        this.contractSignedDate = contractSignedDate;
    }

    public List<SelectItem> getAppList()
    {
        return appList;
    }

    public void setAppList(List<SelectItem> appList)
    {
        this.appList = appList;
    }

    public List<SelectItem> getSelectedAppList()
    {
        return selectedAppList;
    }

    public void setSelectedAppList(List<SelectItem> selectedAppList)
    {
        this.selectedAppList = selectedAppList;
    }

    public List<AppPortalTypeUrlRequestTO> getAppPortalUrlList()
    {
        return appPortalUrlList;
    }

    public void setAppPortalUrlList(List<AppPortalTypeUrlRequestTO> appPortalUrlList)
    {
        this.appPortalUrlList = appPortalUrlList;
    }

    /**
     * @return the orgModuleList
     */
    public List<TpModuleRequestTO> getOrgModuleList()
    {
        return orgModuleList;
    }

    /**
     * @param orgModuleList
     *            the orgModuleList to set
     */
    public void setOrgModuleList(List<TpModuleRequestTO> orgModuleList)
    {
        this.orgModuleList = orgModuleList;
    }

    /**
     * @return the filteredModuleList
     */
    public List<TpModuleRequestTO> getFilteredModuleList()
    {
        return filteredModuleList;
    }

    /**
     * @param filteredModuleList
     *            the filteredModuleList to set
     */
    public void setFilteredModuleList(List<TpModuleRequestTO> filteredModuleList)
    {
        this.filteredModuleList = filteredModuleList;
    }

    /**
     * @return the allOrgList
     */
    public List<OrganizationResponseTO> getAllOrgList()
    {
        return allOrgList;
    }

    /**
     * @param allOrgList
     *            the allOrgList to set
     */
    public void setAllOrgList(List<OrganizationResponseTO> allOrgList)
    {
        this.allOrgList = allOrgList;
    }

    public void clearData()
    {
        this.orgId = null;
        this.orgName = null;
        this.orgCode = null;
        this.dataLocation = null;
        this.url = null;
        this.statusID = null;
        this.tenantID = null;
        this.contractSignedDate = null;
        this.effectiveFromDate = null;
        this.effectiveTODate = null;

    }

    public NewOrganiztionController getNewOrganiztionController()
    {
        return newOrganiztionController;
    }

    public void setNewOrganiztionController(NewOrganiztionController newOrganiztionController)
    {
        this.newOrganiztionController = newOrganiztionController;
    }

    public OrgAppPortalTreeResponseTO getOrgAppPortalTreeTO()
    {
        return orgAppPortalTreeTO;
    }

    public void setOrgAppPortalTreeTO(OrgAppPortalTreeResponseTO orgAppPortalTreeTO)
    {
        this.orgAppPortalTreeTO = orgAppPortalTreeTO;
    }

    public List<NewTenantResponseTO> getTenantList()
    {
        return tenantList;
    }

    public void setTenantList(List<NewTenantResponseTO> tenantList)
    {
        this.tenantList = tenantList;
    }


    public Integer getStatusID()
    {
        return statusID;
    }

    public void setStatusID(Integer statusID)
    {
        this.statusID = statusID;
    }

    public List<Long> getSelectedApp()
    {
        return selectedApp;
    }

    public void setSelectedApp(List<Long> selectedApp)
    {
        this.selectedApp = selectedApp;
    }

    public List<Long> getSelectedModule()
    {
        return selectedModule;
    }

    public void setSelectedModule(List<Long> selectedModule)
    {
        this.selectedModule = selectedModule;
    }

    public List<AppPortalTypeUrlRequestTO> getFilteredOrgList()
    {
        return filteredOrgList;
    }


    public List<SysRoleTypeTO> getSysRoleTypeList()
    {
        return sysRoleTypeList;
    }

    public void setSysRoleTypeList(List<SysRoleTypeTO> sysRoleTypeList)
    {
        this.sysRoleTypeList = sysRoleTypeList;
    }

    public void setFilteredOrgList(List<AppPortalTypeUrlRequestTO> filteredOrgList)
    {
        this.filteredOrgList = filteredOrgList;
    }

    public List<SysMenuTO> getSysMenuList()
    {
        return sysMenuList;
    }

    public void setSysMenuList(List<SysMenuTO> sysMenuList)
    {
        this.sysMenuList = sysMenuList;
    }


    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public List<SelectItem> getPortalThemeList()
    {
        return portalThemeList;
    }

    public void setPortalThemeList(List<SelectItem> portalThemeList)
    {
        this.portalThemeList = portalThemeList;
    }

    public List<SysContentTypeTO> getSysContentTypeList()
    {
        return sysContentTypeList;
    }

    public void setSysContentTypeList(List<SysContentTypeTO> sysContentTypeList)
    {
        this.sysContentTypeList = sysContentTypeList;
    }


    public Integer getEmployeeCount()
    {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount)
    {
        this.employeeCount = employeeCount;
    }

    public List<SelectItem> getSelectLoginPageList()
    {
        return selectLoginPageList;
    }

    public void setSelectLoginPageList(List<SelectItem> selectLoginPageList)
    {
        this.selectLoginPageList = selectLoginPageList;
    }

    public List<SelectItem> getSelectHomePageList()
    {
        return selectHomePageList;
    }

    public void setSelectHomePageList(List<SelectItem> selectHomePageList)
    {
        this.selectHomePageList = selectHomePageList;
    }

    public List<SelectItem> getSelectloginPageThemeList()
    {
        return selectloginPageThemeList;
    }

    public void setSelectloginPageThemeList(List<SelectItem> selectloginPageThemeList)
    {
        this.selectloginPageThemeList = selectloginPageThemeList;
    }

    public List<SelectItem> getSelectdefualtThemeList()
    {
        return selectdefualtThemeList;
    }

    public void setSelectdefualtThemeList(List<SelectItem> selectdefualtThemeList)
    {
        this.selectdefualtThemeList = selectdefualtThemeList;
    }

    public List<SelectItem> getTenantResTOList()
    {
        return tenantResTOList;
    }

    public void setTenantResTOList(List<SelectItem> tenantResTOList)
    {
        this.tenantResTOList = tenantResTOList;
    }


    public List<SelectItem> getSelectFeviconList()
    {
        return selectFeviconList;
    }

    public void setSelectFeviconList(List<SelectItem> selectFeviconList)
    {
        this.selectFeviconList = selectFeviconList;
    }

    public List<SelectItem> getMetaDatabaseList()
    {
        return metaDatabaseList;
    }

    public void setMetaDatabaseList(List<SelectItem> metaDatabaseList)
    {
        this.metaDatabaseList = metaDatabaseList;
    }

    public List<UserResponseTO> getUserTOList()
    {
        return userTOList;
    }

    public void setUserTOList(List<UserResponseTO> userTOList)
    {
        this.userTOList = userTOList;
    }

    public Boolean getStatus()
    {
        return status;
    }

    public void setStatus(Boolean status)
    {
        this.status = status;
    }

    public Boolean getSaveFlag()
    {
        return saveFlag;
    }

    public Boolean getUpdateFlag()
    {
        return updateFlag;
    }

    public void setSaveFlag(Boolean saveFlag)
    {
        this.saveFlag = saveFlag;
    }

    public void setUpdateFlag(Boolean updateFlag)
    {
        this.updateFlag = updateFlag;
    }

    public TreeNode getMenuTree()
    {
        return menuTree;
    }

    public void setMenuTree(TreeNode menuTree)
    {
        this.menuTree = menuTree;
    }

    public TreeNode[] getSelectedMenuNodes()
    {
        return selectedMenuNodes;
    }

    public void setSelectedMenuNodes(TreeNode[] selectedMenuNodes)
    {
        this.selectedMenuNodes = selectedMenuNodes;
    }

    public List<UiFormResponseTO> getSysCopyUIList()
    {
        return sysCopyUIList;
    }

    public void setSysCopyUIList(List<UiFormResponseTO> sysCopyUIList)
    {
        this.sysCopyUIList = sysCopyUIList;
    }


    public List<UiFormResponseTO> getSysCopyUIList1()
    {
        return sysCopyUIList1;
    }

    public void setSysCopyUIList1(List<UiFormResponseTO> sysCopyUIList1)
    {
        this.sysCopyUIList1 = sysCopyUIList1;
    }

    public Integer getFormID()
    {
        return FormID;
    }

    public void setFormID(Integer formID)
    {
        FormID = formID;
    }

    public List<UiFormTO> getUiFormList()
    {
        return uiFormList;
    }

    public void setUiFormList(List<UiFormTO> uiFormList)
    {
        this.uiFormList = uiFormList;
    }

    public String getLeftLogo()
    {
        return leftLogo;
    }

    public void setLeftLogo(String leftLogo)
    {
        this.leftLogo = leftLogo;
    }

    public String getRightLogo()
    {
        return rightLogo;
    }

    public void setRightLogo(String rightLogo)
    {
        this.rightLogo = rightLogo;
    }

    public String getFavicon()
    {
        return favicon;
    }

    public void setFavicon(String favicon)
    {
        this.favicon = favicon;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getMetaData()
    {
        return metaData;
    }

    public void setMetaData(String metaData)
    {
        this.metaData = metaData;
    }

    public String getHeadingTxt()
    {
        return headingTxt;
    }

    public void setHeadingTxt(String headingTxt)
    {
        this.headingTxt = headingTxt;
    }

    public String getUsernameTxt()
    {
        return usernameTxt;
    }

    public void setUsernameTxt(String usernameTxt)
    {
        this.usernameTxt = usernameTxt;
    }

    public String getPasswordTxt()
    {
        return passwordTxt;
    }

    public void setPasswordTxt(String passwordTxt)
    {
        this.passwordTxt = passwordTxt;
    }

    public String getBtnTxt()
    {
        return btnTxt;
    }

    public void setBtnTxt(String btnTxt)
    {
        this.btnTxt = btnTxt;
    }

    public String getFooter()
    {
        return footer;
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

    public String getBanner()
    {
        return banner;
    }

    public void setBanner(String banner)
    {
        this.banner = banner;
    }

    public String getSamlHeader()
    {
        return samlHeader;
    }

    public void setSamlHeader(String samlHeader)
    {
        this.samlHeader = samlHeader;
    }

    public String getSamlBtnTxt()
    {
        return samlBtnTxt;
    }

    public void setSamlBtnTxt(String samlBtnTxt)
    {
        this.samlBtnTxt = samlBtnTxt;
    }

    public List<OrgMoreInfoResponseTO> getAllMOreInfoOrgList()
    {
        return allMOreInfoOrgList;
    }

    public void setAllMOreInfoOrgList(List<OrgMoreInfoResponseTO> allMOreInfoOrgList)
    {
        this.allMOreInfoOrgList = allMOreInfoOrgList;
    }

    public String getResetPassword()
    {
        return resetPassword;
    }

    public void setResetPassword(String resetPassword)
    {
        this.resetPassword = resetPassword;
    }

    public Boolean getActive()
    {
        return active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public Map<Long, TreeNode> getModuleTreeWithAppMap()
    {
        return moduleTreeWithAppMap;
    }

    public void setModuleTreeWithAppMap(Map<Long, TreeNode> moduleTreeWithAppMap)
    {
        this.moduleTreeWithAppMap = moduleTreeWithAppMap;
    }

    public Map<Long, TreeNode[]> getSelectedNodeByApp()
    {
        return selectedNodeByApp;
    }

    public void setSelectedNodeByApp(Map<Long, TreeNode[]> selectedNodeByApp)
    {
        this.selectedNodeByApp = selectedNodeByApp;
    }

    public List<SysBundleResponseTO> getSysBundle()
    {
        return sysBundle;
    }

    public void setSysBundle(List<SysBundleResponseTO> sysBundle)
    {
        this.sysBundle = sysBundle;
    }

    public List<Integer> getSelectedBundleIds()
    {
        return selectedBundleIds;
    }

    public void setSelectedBundleIds(List<Integer> selectedBundleIds)
    {
        this.selectedBundleIds = selectedBundleIds;
    }

    public String getLine1()
    {
        return line1;
    }

    public void setLine1(String line1)
    {
        this.line1 = line1;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }


    public Long getCountryID()
    {
        return countryID;
    }

    public void setCountryID(Long countryID)
    {
        this.countryID = countryID;
    }

    public Long getStateID()
    {
        return stateID;
    }

    public void setStateID(Long stateID)
    {
        this.stateID = stateID;
    }

    public Long getDistrictID()
    {
        return districtID;
    }

    public void setDistrictID(Long districtID)
    {
        this.districtID = districtID;
    }

    public Integer getCityID()
    {
        return cityID;
    }

    public void setCityID(Integer cityID)
    {
        this.cityID = cityID;
    }

    public Long getZipCodeID()
    {
        return zipCodeID;
    }

    public void setZipCodeID(Long zipCodeID)
    {
        this.zipCodeID = zipCodeID;
    }

    public List<AddressResponseTO> getAddressTOList()
    {
        return addressTOList;
    }

    public void setAddressTOList(List<AddressResponseTO> addressTOList)
    {
        this.addressTOList = addressTOList;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public List<AddressResponseTO> getCountryTOList()
    {
        return countryTOList;
    }

    public void setCountryTOList(List<AddressResponseTO> countryTOList)
    {
        this.countryTOList = countryTOList;
    }

    public List<AddressResponseTO> getStateTOList()
    {
        return stateTOList;
    }

    public void setStateTOList(List<AddressResponseTO> stateTOList)
    {
        this.stateTOList = stateTOList;
    }

    public List<AddressResponseTO> getDistrictTOList()
    {
        return districtTOList;
    }

    public void setDistrictTOList(List<AddressResponseTO> districtTOList)
    {
        this.districtTOList = districtTOList;
    }

    public List<AddressResponseTO> getCityTOList()
    {
        return cityTOList;
    }

    public void setCityTOList(List<AddressResponseTO> cityTOList)
    {
        this.cityTOList = cityTOList;
    }

    public List<AddressResponseTO> getZipCodeTOList()
    {
        return zipCodeTOList;
    }

    public void setZipCodeTOList(List<AddressResponseTO> zipCodeTOList)
    {
        this.zipCodeTOList = zipCodeTOList;
    }

    public String getSectorCode()
    {
        return sectorCode;
    }

    public void setSectorCode(String sectorCode)
    {
        this.sectorCode = sectorCode;
    }

    public String getSector()
    {
        return sector;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public Double getCustomizationAmount()
    {
        return customizationAmount;
    }

    public void setCustomizationAmount(Double customizationAmount)
    {
        this.customizationAmount = customizationAmount;
    }

    public Double getRecurringPerMonthAmount()
    {
        return recurringPerMonthAmount;
    }

    public void setRecurringPerMonthAmount(Double recurringPerMonthAmount)
    {
        this.recurringPerMonthAmount = recurringPerMonthAmount;
    }

    public Double getSetupCost()
    {
        return setupCost;
    }

    public void setSetupCost(Double setupCost)
    {
        this.setupCost = setupCost;
    }

    public List<SectorCostResponseTO> getSectorTOList()
    {
        return sectorTOList;
    }

    public void setSectorTOList(List<SectorCostResponseTO> sectorTOList)
    {
        this.sectorTOList = sectorTOList;
    }

    public Date getOrgFinancialStartDate()
    {
        return orgFinancialStartDate;
    }

    public void setOrgFinancialStartDate(Date orgFinancialStartDate)
    {
        this.orgFinancialStartDate = orgFinancialStartDate;
    }

    public Date getGovtFinancialStartDate()
    {
        return govtFinancialStartDate;
    }

    public void setGovtFinancialStartDate(Date govtFinancialStartDate)
    {
        this.govtFinancialStartDate = govtFinancialStartDate;
    }

    public OrganizationResponseTO getOrgTO()
    {
        return orgTO;
    }

    public void setOrgTO(OrganizationResponseTO orgTO)
    {
        this.orgTO = orgTO;
    }

    public List<HrModuleTO> getHrModuleList()
    {
        return hrModuleList;
    }

    public void setHrModuleList(List<HrModuleTO> hrModuleList)
    {
        this.hrModuleList = hrModuleList;
    }


    public Boolean getMobileStatus()
    {
        return mobileStatus;
    }

    public void setMobileStatus(Boolean mobileStatus)
    {
        this.mobileStatus = mobileStatus;
    }


    public String getBackGroundImage()
    {
        return backGroundImage;
    }

    public void setBackGroundImage(String backGroundImage)
    {
        this.backGroundImage = backGroundImage;
    }


    public String getContactNumber()
    {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber)
    {
        this.contactNumber = contactNumber;
    }

    public String geteMail()
    {
        return eMail;
    }

    public void seteMail(String eMail)
    {
        this.eMail = eMail;
    }

    public String getExternalVideoPath()
    {
        return externalVideoPath;
    }

    public void setExternalVideoPath(String externalVideoPath)
    {
        this.externalVideoPath = externalVideoPath;
    }

    public void handleLeftLogo(FileUploadEvent event)
    {
        FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;
        String[] destinationPathArr = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPath();
            destinationPath = destinationPathArr[1];
            setLeftLogo(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream(), destinationPathArr[0]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleRightLogo(FileUploadEvent event)
    {
        FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;
        String[] destinationPathArr = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPath();
            destinationPath = destinationPathArr[1];
            setRightLogo(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream(), destinationPathArr[0]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleBannerLogo(FileUploadEvent event)
    {
        FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;
        String[] destinationPathArr = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPath();
            destinationPath = destinationPathArr[1];
            setBanner(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream(), destinationPathArr[0]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleBackGroundImage(FileUploadEvent event)
    {
        FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;
        String[] destinationPathArr = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPathForBackGroundImage();
            destinationPath = destinationPathArr[1];
            setBackGroundImage(destinationPath + event.getFile().getFileName());
            copyFileForBackgroundImage(event.getFile().getFileName(), event.getFile().getInputstream(), destinationPathArr[0]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private String[] getDestinationPath()
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String initialPathForRecruit = null;
        String initialPathForInfer = null;
        String initialPathForAdmin = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;
        String[] path = null;
        String destinationPathforDB = null;
        String initialPathPathforDB = null;
        String finalPathforDB = null;

        try {
            path = new String[2];
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_LOGO);

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length())) {
                    initialPath = initialPath +  "/" + "tenant_" + getTenantID() +  "/" + "org_" + getOrgId() +  "/";
                    //  initialPath = initialPath + "/" + "tenant_" + getTenantID() + "/" + "org_" + getOrgId() +"/";
                    initialPathPathforDB = initialPath; //"/" + "tenant_" + getTenantID() + "/" + "org_" + getOrgId() + "/";


                }
            } else {
                throw new Exception("Invalid path");
            }

            finalPath = initialPath + "logo";
            finalPathforDB = initialPathPathforDB + "logo";
            dir = new File(finalPath);
            dir.mkdirs();
            destinationPath = finalPath + File.separator;
            //  destinationPathforDB = finalPathforDB + File.separator;
            destinationPathforDB = finalPathforDB + "/";
            path[0] = destinationPath;
            path[1] = destinationPathforDB;


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return path;
    }

    private void copyFile(String fileName, InputStream in, String uploadPath)
    {
        String[] destinationPathArr = null;
        String destinationPath = null;
        try {
            destinationPathArr = new String[2];
            // destinationPathArr = getDestinationPath();
            destinationPath = uploadPath;
            OutputStream out = new FileOutputStream(new File(destinationPath + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String[] getDestinationPathForBackGroundImage()
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;
        String[] path = null;
        String destinationPathforDB = null;
        String initialPathPathforDB = null;
        String finalPathforDB = null;

        try {
            path = new String[2];
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_LOGO);

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length())) {
                    initialPath = initialPath + File.separator + "tenant_" + getTenantID() + File.separator + "org_" + getOrgId() + File.separator;
                    // initialPathPathforDB = File.separator + "tenant_" + getTenantID() + File.separator + "org_" + getOrgId() + File.separator;
                    initialPathPathforDB = "/" + "tenant_" + getTenantID() + "/" + "org_" + getOrgId() + "/";
                }
            } else {
                throw new Exception("Invalid path");
            }

            finalPath = initialPath + "background";
            finalPathforDB = initialPathPathforDB + "background";
            dir = new File(finalPath);
            dir.mkdirs();
            destinationPath = finalPath + File.separator;
            destinationPathforDB = finalPathforDB + "/";
            path[0] = destinationPath;
            path[1] = destinationPathforDB;


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return path;
    }

    private void copyFileForBackgroundImage(String fileName, InputStream in, String uploadPath)
    {
        String[] destinationPathArr = null;
        String destinationPath = null;
        try {
            destinationPathArr = new String[2];
            //   destinationPathArr = getDestinationPathForBackGroundImage();
            destinationPath = uploadPath;
            //  destinationPath = destinationPathArr[0];
            OutputStream out = new FileOutputStream(new File(destinationPath + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<DashBoardTO> getDashBoardTOlist()
    {
        return dashBoardTOlist;
    }

    public void setDashBoardTOlist(List<DashBoardTO> dashBoardTOlist)
    {
        this.dashBoardTOlist = dashBoardTOlist;
    }

    public List<SysProductAnnouncementTO> getSysProductAnnouncementTOList()
    {
        return sysProductAnnouncementTOList;
    }

    public void setSysProductAnnouncementTOList(List<SysProductAnnouncementTO> sysProductAnnouncementTOList)
    {
        this.sysProductAnnouncementTOList = sysProductAnnouncementTOList;
    }


    public List<VideoLinkTO> getOnBoardingVideoLinkList()
    {
        return onBoardingVideoLinkList;
    }

    public void setOnBoardingVideoLinkList(List<VideoLinkTO> onBoardingVideoLinkList)
    {
        this.onBoardingVideoLinkList = onBoardingVideoLinkList;
    }

    public void addVideoLink()
    {
        VideoLinkTO to = new VideoLinkTO();
        if (onBoardingVideoLinkList == null) {
            onBoardingVideoLinkList = new ArrayList<VideoLinkTO>();
        }
        onBoardingVideoLinkList.add(to);

    }

    public List<SysSubModuleResponseTO> getModuleDropDownList()
    {
        return moduleDropDownList;
    }

    public void setModuleDropDownList(List<SysSubModuleResponseTO> moduleDropDownList)
    {
        this.moduleDropDownList = moduleDropDownList;
    }

    public Integer getModuleID()
    {
        return moduleID;
    }

    public void setModuleID(Integer moduleID)
    {
        this.moduleID = moduleID;
    }

    public List<SysContentTypeTO> getSysContentTypeTOList()
    {
        return sysContentTypeTOList;
    }

    public void setSysContentTypeTOList(List<SysContentTypeTO> sysContentTypeTOList)
    {
        this.sysContentTypeTOList = sysContentTypeTOList;
    }

    public Integer getSysContentTypeID()
    {
        return sysContentTypeID;
    }

    public void setSysContentTypeID(Integer sysContentTypeID)
    {
        this.sysContentTypeID = sysContentTypeID;
    }

    public Float getAttendanceDistanceLimit()
    {
        return attendanceDistanceLimit;
    }

    public void setAttendanceDistanceLimit(Float attendanceDistanceLimit)
    {
        this.attendanceDistanceLimit = attendanceDistanceLimit;
    }

    public String getSelectedLanguages()
    {
        return selectedLanguages;
    }

    public void setSelectedLanguages(String selectedLanguages)
    {
        this.selectedLanguages = selectedLanguages;
    }

    public boolean isLdapInfo()
    {
        return ldapInfo;
    }

    public void setLdapInfo(boolean ldapInfo)
    {
        this.ldapInfo = ldapInfo;
    }

    public String getLdapServer()
    {
        return ldapServer;
    }

    public void setLdapServer(String ldapServer)
    {
        this.ldapServer = ldapServer;
    }

    public String getLdapSearchBase()
    {
        return ldapSearchBase;
    }

    public void setLdapSearchBase(String ldapSearchBase)
    {
        this.ldapSearchBase = ldapSearchBase;
    }

    public String getLdapDomain()
    {
        return ldapDomain;
    }

    public void setLdapDomain(String ldapDomain)
    {
        this.ldapDomain = ldapDomain;
    }

    public boolean isLdapActive()
    {
        return ldapActive;
    }

    public void setLdapActive(boolean ldapActive)
    {
        this.ldapActive = ldapActive;
    }

    public String getSecretKey()
    {
        return secretKey;
    }

    public void setSecretKey(String secretKey)
    {
        this.secretKey = secretKey;
    }

    public Integer getLandingPageID()
    {
        return landingPageID;
    }

    public void setLandingPageID(Integer landingPageID)
    {
        this.landingPageID = landingPageID;
    }

    public List<UiFormTO> getSelectLandingePagList()
    {
        return selectLandingePagList;
    }

    public void setSelectLandingePagList(List<UiFormTO> selectLandingePagList)
    {
        this.selectLandingePagList = selectLandingePagList;
    }

    public boolean isLdapWindowsAuthentication()
    {
        return ldapWindowsAuthentication;
    }

    public void setLdapWindowsAuthentication(boolean ldapWindowsAuthentication)
    {
        this.ldapWindowsAuthentication = ldapWindowsAuthentication;
    }

    private boolean messengerEnabled;

    private Integer mobileSessionTimeout;


    public boolean isMessengerEnabled()
    {
        return messengerEnabled;
    }

    public void setMessengerEnabled(boolean messengerEnabled)
    {
        this.messengerEnabled = messengerEnabled;
    }

    public Integer getMobileSessionTimeout()
    {
        return mobileSessionTimeout;
    }

    public void setMobileSessionTimeout(Integer mobileSessionTimeout)
    {
        this.mobileSessionTimeout = mobileSessionTimeout;
    }

    private List<LoginServerTypeTO> loginServerTypeList;

    private Map<String, SocialAppSettingTO> socialAppSettingTOMap;

    public List<LoginServerTypeTO> getLoginServerTypeList()
    {
        return loginServerTypeList;
    }

    public void setLoginServerTypeList(List<LoginServerTypeTO> loginServerTypeList)
    {
        this.loginServerTypeList = loginServerTypeList;
    }

    public Map<String, SocialAppSettingTO> getSocialAppSettingTOMap()
    {
        return socialAppSettingTOMap;
    }

    public void setSocialAppSettingTOMap(Map<String, SocialAppSettingTO> socialAppSettingTOMap)
    {
        this.socialAppSettingTOMap = socialAppSettingTOMap;
    }

    private boolean renderLoginServerPanel;

    private boolean renderLADP;

    public boolean isRenderLADP()
    {
        return renderLADP;
    }

    public boolean isRenderLoginServerPanel()
    {
        return renderLoginServerPanel;
    }

    public void setRenderLADP(boolean renderLADP)
    {
        this.renderLADP = renderLADP;
    }

    public void setRenderLoginServerPanel(boolean renderLoginServerPanel)
    {
        this.renderLoginServerPanel = renderLoginServerPanel;
    }

    private List<String> selectedLoginServerTypes;

    private List<String> selectedLoginServerList;

    private String defaultLoginServer;

    public String getDefaultLoginServer()
    {
        return defaultLoginServer;
    }

    public void setDefaultLoginServer(String defaultLoginServer)
    {
        this.defaultLoginServer = defaultLoginServer;
    }

    public List<String> getSelectedLoginServerList()
    {
        return selectedLoginServerList;
    }

    public void setSelectedLoginServerList(List<String> selectedLoginServerList)
    {
        this.selectedLoginServerList = selectedLoginServerList;
    }

    public List<String> getSelectedLoginServerTypes()
    {
        return selectedLoginServerTypes;
    }

    public void setSelectedLoginServerTypes(List<String> selectedLoginServerTypes)
    {
        this.selectedLoginServerTypes = selectedLoginServerTypes;
    }

    private List<SocialAppSettingTO> socialAppSettingTOList;

    private boolean editLoginServer;

    public List<SocialAppSettingTO> getSocialAppSettingTOList()
    {
        return socialAppSettingTOList;
    }

    public void setSocialAppSettingTOList(List<SocialAppSettingTO> socialAppSettingTOList)
    {
        this.socialAppSettingTOList = socialAppSettingTOList;
    }

    public boolean isEditLoginServer()
    {
        return editLoginServer;
    }

    public void setEditLoginServer(boolean editLoginServer)
    {
        this.editLoginServer = editLoginServer;
    }

    private List<PortalTO> portalList;

    public List<PortalTO> getPortalList()
    {
        return portalList;
    }

    public void setPortalList(List<PortalTO> portalList)
    {
        this.portalList = portalList;
    }

    private long selectedPortalID;

    public long getSelectedPortalID()
    {
        return selectedPortalID;
    }

    public void setSelectedPortalID(long selectedPortalID)
    {
        this.selectedPortalID = selectedPortalID;
    }

    private boolean showChartDataList;

    public boolean isShowChartDataList()
    {
        return showChartDataList;
    }

    public void setShowChartDataList(boolean showChartDataList)
    {
        this.showChartDataList = showChartDataList;
 
    
    }
    
    public String getExternalUrl() {
		return externalUrl;
	}
    public void setExternalUrl(String externalUrl) {
		this.externalUrl = externalUrl;
	}

	public String getLdapLogoutUrl() {
		return ldapLogoutUrl;
	}

	public void setLdapLogoutUrl(String ldapLogoutUrl) {
		this.ldapLogoutUrl = ldapLogoutUrl;
	}

	public boolean isRenderSP() {
		return renderSP;
	}

	public void setRenderSP(boolean renderSP) {
		this.renderSP = renderSP;
	}

	public String getSpServer() {
		return spServer;
	}

	public void setSpServer(String spServer) {
		this.spServer = spServer;
	}

	public String getSpLogoutUrl() {
		return spLogoutUrl;
	}

	public void setSpLogoutUrl(String spLogoutUrl) {
		this.spLogoutUrl = spLogoutUrl;
	}

	public boolean isSpActive() {
		return spActive;
	}

	public void setSpActive(boolean spActive) {
		this.spActive = spActive;
	}

	public List<TpAppResponseTO> getPartnerAppList() {
		return partnerAppList;
	}

	public void setPartnerAppList(List<TpAppResponseTO> partnerAppList) {
		this.partnerAppList = partnerAppList;
	}

	public List<TpAppResponseTO> getCustomAppList() {
		return customAppList;
	}

	public void setCustomAppList(List<TpAppResponseTO> customAppList) {
		this.customAppList = customAppList;
	}

	public List<String> getSelectedPartnerAppList() {
		return selectedPartnerAppList;
	}

	public void setSelectedPartnerAppList(List<String> selectedPartnerAppList) {
		this.selectedPartnerAppList = selectedPartnerAppList;
	}

	public List<String> getSelectedCustomAppList() {
		return selectedCustomAppList;
	}

	public void setSelectedCustomAppList(List<String> selectedCustomAppList) {
		this.selectedCustomAppList = selectedCustomAppList;
	}
}
