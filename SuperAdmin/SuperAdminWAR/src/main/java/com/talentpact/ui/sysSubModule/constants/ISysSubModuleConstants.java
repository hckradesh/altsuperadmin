package com.talentpact.ui.sysSubModule.constants;

public interface ISysSubModuleConstants
{
    public static String SYS_SUB_MODULE_RETRIEVE_ERROR_MSG = "Error occured while retrieving SysSubModule List";
}
