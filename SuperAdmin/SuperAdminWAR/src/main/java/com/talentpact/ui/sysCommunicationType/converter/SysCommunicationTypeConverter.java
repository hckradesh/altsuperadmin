/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.converter;

import com.talentpact.model.SysCommunicationType;
import com.talentpact.ui.sysCommunicationConfig.common.transport.output.SysCommunicationConfigResponseTO;
import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysModule.common.transport.output.SysModuleResponseTO;
import com.talentpact.ui.sysTenant.common.transport.output.SysTenantResponseTO;

/**
 * @author vaibhav.kashyap
 *
 */
public class SysCommunicationTypeConverter {
	
	public static SysCommunicationTypeResponseTO getCommTypeResponseTOFromEntity(
			SysCommunicationType entityObject) throws Exception{
		SysCommunicationTypeResponseTO sysCommunicationTypeResponseTO = null;
		SysCommunicationConfigResponseTO sysCommunicationConfigResponseTO = null;
		SysModuleResponseTO sysModuleResponseTO = null;
		SysTenantResponseTO sysTenantResponseTO = null;
		try{
			if(entityObject!=null){
				sysCommunicationTypeResponseTO = new SysCommunicationTypeResponseTO();
				
				if(entityObject.getCommType()!=null && 
						!entityObject.getCommType().equals(""))
					sysCommunicationTypeResponseTO.setCommType(
							entityObject.getCommType());
				
				if(entityObject.getCommTypeId()>0)
					sysCommunicationTypeResponseTO.setCommTypeId(
							entityObject.getCommTypeId());
				
				if(entityObject.getPrimarayKey()!=null && 
						!entityObject.getPrimarayKey().equals(""))
					sysCommunicationTypeResponseTO.setPrimarayKey(entityObject.getPrimarayKey());
				
			/*	if(entityObject.getSysCommunicationConfigs()!=null){
					sysCommunicationConfigResponseTO = new SysCommunicationConfigResponseTO();
					sysCommunicationTypeResponseTO.setSysCommunicationConfigResponseTOObj(sysCommunicationConfigResponseTO);
				}*/
				
				if(entityObject.getSysModule()!=null){
					sysModuleResponseTO = new SysModuleResponseTO();
					//sysModuleResponseTO.setAppCode(entityObject.getSysModule().getAppCode());
					//sysModuleResponseTO.setAppID(entityObject.getSysModule().getAppID());
					//sysModuleResponseTO.setModuleCode(entityObject.getSysModule().getModuleCode());
					//sysModuleResponseTO.setModuleDesc(entityObject.getSysModule().getModuleDesc());
					sysModuleResponseTO.setModuleID(entityObject.getSysModule().getModuleId());
					//sysModuleResponseTO.setModuleName(entityObject.getSysModule().getModuleName());
					sysCommunicationTypeResponseTO.setSysModuleResponseTOResponseToObj(sysModuleResponseTO);
				}
				
				if(entityObject.getSysTenant()>0){
					sysTenantResponseTO = new SysTenantResponseTO();
					/**
					 * ideally it should be fetched from db
					 *   **time constraint
					 *   hard coding it to 1
					 * */
					sysTenantResponseTO.setTenantId(new Integer(1));
					sysCommunicationTypeResponseTO.setSysTenantResponseTO(sysTenantResponseTO);
				}
				
				if(entityObject.getTableName()!=null && 
						!entityObject.getTableName().equals(""))
						sysCommunicationTypeResponseTO.setTableName(entityObject.getTableName());
						
			}
		}catch(Exception ex){
			sysCommunicationTypeResponseTO = null;
			throw ex;
		}
		
		return sysCommunicationTypeResponseTO;
	}

}
