/**
 * 
 */
package com.talentpact.ui.sysSubModule.facaderemote;

import javax.ejb.Local;
import javax.ejb.Stateless;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysSubModule.exception.SysSubModuleException;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@Local
public interface SysSubModuleLocalFacade {

	/**
	 * @param serviceRequest
	 * @return
	 */
	ServiceResponse getParentModuleAndSubModules(ServiceRequest serviceRequest)
			throws Exception;

	/**
     * 
     * @param serviceRequest
     * @return
     * @throws SysSubModuleException
     * @author raghvendra.mishra
     */
    public ServiceResponse getAllSysSubModuleList(ServiceRequest serviceRequest) throws SysSubModuleException;
}
