package com.talentpact.ui.system.converter;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.alt.system.transport.UrlTO;


@FacesConverter(value = "urlConverter", forClass = UrlTO.class)
public class UrlConverter implements Converter, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4343484581641902042L;

    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }

        try {
            return null;// warehouseService.find(Long.valueOf(submittedValue));
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(submittedValue + " is not a valid Warehouse ID"), e);
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }

        if (modelValue instanceof UrlTO) {
            return String.valueOf(((UrlTO) modelValue).getUrl());
        } else {
            throw new ConverterException(new FacesMessage(modelValue + " is not a valid Url"));
        }
    }}
