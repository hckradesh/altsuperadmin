/**
 * 
 */
package com.talentpact.ui.sysConstantCategory.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysConstantCategoryResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysConstantCategory.controller.SysConstantCategoryController;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysConstantCategoryBean")
@ConversationScoped
public class SysConstantCategoryBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysConstantCategoryController sysConstantCategoryController;

    private Integer sysConstantCategoryID;

    private String sysConstantCategoryName;

    private List<SysConstantCategoryResponseTO> sysConstantCategoryTOList;

    private boolean renderSysConstantCategoryPopup;

    private boolean renderEditSysConstantCategoryPopup;

    private boolean renderMessageBox;

    private Set<String> sysCategoryName;

    private Boolean renderUpdateButton;


    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysConstantCategoryController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public Integer getSysConstantCategoryID()
    {
        return sysConstantCategoryID;
    }


    public void setSysConstantCategoryID(Integer sysConstantCategoryID)
    {
        this.sysConstantCategoryID = sysConstantCategoryID;
    }


    public String getSysConstantCategoryName()
    {
        return sysConstantCategoryName;
    }


    public void setSysConstantCategoryName(String sysConstantCategoryName)
    {
        this.sysConstantCategoryName = sysConstantCategoryName;
    }


    public List<SysConstantCategoryResponseTO> getSysConstantCategoryTOList()
    {
        return sysConstantCategoryTOList;
    }

    public void setSysConstantCategoryTOList(List<SysConstantCategoryResponseTO> sysConstantCategoryTOList)
    {
        this.sysConstantCategoryTOList = sysConstantCategoryTOList;
    }

    public boolean isRenderSysConstantCategoryPopup()
    {
        return renderSysConstantCategoryPopup;
    }

    public void setRenderSysConstantCategoryPopup(boolean renderSysConstantCategoryPopup)
    {
        this.renderSysConstantCategoryPopup = renderSysConstantCategoryPopup;
    }

    public boolean isRenderEditSysConstantCategoryPopup()
    {
        return renderEditSysConstantCategoryPopup;
    }

    public void setRenderEditSysConstantCategoryPopup(boolean renderEditSysConstantCategoryPopup)
    {
        this.renderEditSysConstantCategoryPopup = renderEditSysConstantCategoryPopup;
    }

    public boolean isRenderMessageBox()
    {
        return renderMessageBox;
    }

    public void setRenderMessageBox(boolean renderMessageBox)
    {
        this.renderMessageBox = renderMessageBox;
    }

    public Set<String> getSysCategoryName()
    {
        return sysCategoryName;
    }

    public void setSysCategoryName(Set<String> sysCategoryName)
    {
        this.sysCategoryName = sysCategoryName;
    }

    public Boolean getRenderUpdateButton()
    {
        return renderUpdateButton;
    }

    public void setRenderUpdateButton(Boolean renderUpdateButton)
    {
        this.renderUpdateButton = renderUpdateButton;
    }


}
