package com.talentpact.ui.sysEntity.Exception;

public class SysEntityException extends Exception {

private static final long serialVersionUID = 1L;
    
    private String message = null;

    public SysEntityException()
    {
        super();
    }

    public SysEntityException(String message)
    {
        super(message);
        this.message = message;
    }
    
    public SysEntityException(String message, Exception e) {
        super(message, e);
    }

    public SysEntityException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
