package com.talentpact.ui.sysDSOPlaceHolder.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysDSOPlaceHolder.controller.SysDSOPlaceHolderController;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;

@SuppressWarnings("serial")
@Named("sysDSOPlaceHolderBean")
@ConversationScoped
public class SysDSOPlaceHolderBean extends CommonBean implements Serializable, IDefaultAction {

	@Inject
	SysDSOPlaceHolderController sysDSOPlaceHolderController;
	
	private Integer dSOPlaceholdersID;
	private String placeHolderLabel;
	private String placeHolderType;
	private String placeHolderValue;
	private Boolean isActive;
	private DSOPlaceHolderTO dsoPlaceHolderTO;
    private List<DSOPlaceHolderTO> DSOPlaceHolderTOList;
    private boolean renderErrorMessage;
    private List<String> placeHolderTypelist;

	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		try {
        	endConversation();
            beginConversation();
            sysDSOPlaceHolderController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public Integer getdSOPlaceholdersID() {
		return dSOPlaceholdersID;
	}

	public void setdSOPlaceholdersID(Integer dSOPlaceholdersID) {
		this.dSOPlaceholdersID = dSOPlaceholdersID;
	}

	public String getPlaceHolderLabel() {
		return placeHolderLabel;
	}

	public void setPlaceHolderLabel(String placeHolderLabel) {
		this.placeHolderLabel = placeHolderLabel;
	}

	public String getPlaceHolderType() {
		return placeHolderType;
	}

	public void setPlaceHolderType(String placeHolderType) {
		this.placeHolderType = placeHolderType;
	}

	public String getPlaceHolderValue() {
		return placeHolderValue;
	}

	public void setPlaceHolderValue(String placeHolderValue) {
		this.placeHolderValue = placeHolderValue;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public DSOPlaceHolderTO getDsoPlaceHolderTO() {
		return dsoPlaceHolderTO;
	}

	public void setDsoPlaceHolderTO(DSOPlaceHolderTO dsoPlaceHolderTO) {
		this.dsoPlaceHolderTO = dsoPlaceHolderTO;
	}

	public List<DSOPlaceHolderTO> getDSOPlaceHolderTOList() {
		return DSOPlaceHolderTOList;
	}

	public void setDSOPlaceHolderTOList(List<DSOPlaceHolderTO> dSOPlaceHolderTOList) {
		DSOPlaceHolderTOList = dSOPlaceHolderTOList;
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	public List<String> getPlaceHolderTypelist() {
		return placeHolderTypelist;
	}

	public void setPlaceHolderTypelist(List<String> placeHolderTypelist) {
		this.placeHolderTypelist = placeHolderTypelist;
	}

	
}
