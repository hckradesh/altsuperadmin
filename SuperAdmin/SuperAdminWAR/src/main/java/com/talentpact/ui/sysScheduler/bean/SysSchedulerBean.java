package com.talentpact.ui.sysScheduler.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysScheduler.controller.SysSchedulerController;
import com.talentpact.ui.sysScheduler.to.SysSchedulerTO;

/**
 * 
 * @author prachi.bansal
 *
 */

@SuppressWarnings("serial")
@Named("sysSchedulerBean")
@ConversationScoped
public class SysSchedulerBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysSchedulerController sysSchedulerController;
    
    private Integer sysSchedulerTypeID;
    
    private String sysSchedulerName;
    
    private String sysSchedulerDescription;
    
    private SysSchedulerTO sysSchedulerTO;
    
    private List<SysSchedulerTO> sysSchedulerTOList;
    
    private boolean renderErrorMessage;

    @Override
    public void initialize() {
        try {
        	endConversation();
            beginConversation();
            sysSchedulerController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public Integer getSysSchedulerTypeID() {
        return sysSchedulerTypeID;
    }

    public void setSysSchedulerTypeID(Integer sysSchedulerTypeID) {
        this.sysSchedulerTypeID = sysSchedulerTypeID;
    }

    public String getSysSchedulerName() {
        return sysSchedulerName;
    }

    public void setSysSchedulerName(String sysSchedulerName) {
        this.sysSchedulerName = sysSchedulerName;
    }

    public String getSysSchedulerDescription() {
        return sysSchedulerDescription;
    }

    public void setSysSchedulerDescription(String sysSchedulerDescription) {
        this.sysSchedulerDescription = sysSchedulerDescription;
    }

    public List<SysSchedulerTO> getSysSchedulerTOList() {
        return sysSchedulerTOList;
    }

    public void setSysSchedulerTOList(List<SysSchedulerTO> sysSchedulerTOList) {
        this.sysSchedulerTOList = sysSchedulerTOList;
    }

    public boolean isRenderErrorMessage() {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage) {
        this.renderErrorMessage = renderErrorMessage;
    }

    public SysSchedulerTO getSysSchedulerTO() {
        return sysSchedulerTO;
    }

    public void setSysSchedulerTO(SysSchedulerTO sysSchedulerTO) {
        this.sysSchedulerTO = sysSchedulerTO;
    }
    
}