package com.talentpact.ui.sysEntity.constants;

public interface ISysEntityConstants {
	
	public static String SYSENTITY_SAVE_ERROR_MSG = "Error occured while saving SysEntity";
	
	public static String SYSENTITYCOLUMN_SAVE_ERROR_MSG = "Error occured while saving SysEntityColumn.";
	
	public static String SYSENTITY_SYSCOLUMN_SAVE_ERROR_MSG = "Error occured while saving SysEntity and SysColumn.";
	
	public static String SYSENTITY_SYSCOLUMN_UPDATE_ERROR_MSG = "Error occured while updating SysEntity and SysColumn.";

    public static String SYSENTITY_UPDATE_ERROR_MSG = "Error occured while updating SysEntity.";
    
    public static String SYSENTITY_SYSCOLUMN_ADD_SUCCESS_MSG = "SysEntity and SysColumn created successfully.";
    
    public static String SYSENTITY_SYSCOLUMN_UPDATE_SUCCESS_MSG = "SysEntity and SysColumn updated successfully.";

    public static String SYSENTITY_UPDATE_SUCCESS_MSG = "SysEntity updated successfully.";
    
    public static String SYSENTITY_EDIT_DIALOG_HIDE = "PF('sysEntityEditModal').hide();";
    
    public static String SYSENTITY_RETRIEVE_ERROR_MSG = "Error occured while retrieving SysEntity on basis of entityID";
    
    public static String SYSENTITY_PRIMARY_KEY_COLUMN_NAME_ERROR_MSG = "Error occured while retrieving Primary Column Names of table on basis of table name";
    
    public static String SYSENTITY_TABLE_SCHEMA = "tableSchema";
    
    public static String SYSENTITY_TABLE_NAME = "tableName";

	public static String SYSENTITY_TABLE_NAME_LIST_ERROR_MSG = "Error occured while retrieving Table Name List from INFORMATION_SCHEMA which are not in SysEntity";
	
	public static String SYSENTITY_COLUMN_NAME_DATA_TYPE_LIST_ERROR_MSG = "Error occured while retrieving Column_Name and Data_Type  List from INFORMATION_SCHEMA.COLUMNS on the basis of table name";
	
	public static String SYSENTITY_TABLES_PRIMARY_COLUMN_NAMES_RETRIEVE_ERROR_MSG = "Error occured while retrieving Table's Primary Columns Names from INFORMATION_SCHEMA.KEY_COLUMN_USAGE with Table Name as input";

	public static String SYSENTITY_PRIMARY_COLUMN_UPDATE_ERROR_MSG = "Error occured while updating Primary Column detais in SysEntity on basis of entityID";
}
