/**
 * 
 */
package com.talentpact.ui.sysRule.dataservice;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.dataservice.sysRule.SysRuleDS;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author vaibhav.kashyap
 *
 */

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysRuleDataService
{

    @EJB
    private SysRuleDS sysRuleDS;

    /**
     * @return
     */
    public List<RuleResponseTO> getAllRules()
        throws Exception
    {
        return sysRuleDS.getAllRules();
    }

}
