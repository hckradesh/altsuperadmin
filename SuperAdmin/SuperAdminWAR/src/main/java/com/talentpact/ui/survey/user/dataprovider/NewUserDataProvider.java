/**
 * 
 */
package com.talentpact.ui.survey.user.dataprovider;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.authorization.transport.input.AuthRequestTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.survey.user.common.transport.input.NewUserRequestTO;
import com.talentpact.ui.survey.user.common.transport.input.SurveyOrgRequestTO;
import com.talentpact.ui.survey.user.common.transport.input.SurveyUserRequestTO;
import com.talentpact.ui.survey.user.common.transport.output.SurveyOrgResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.SurveyUserResponseTO;
import com.talentpact.ui.survey.user.dataservice.NewUserDataService;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class NewUserDataProvider {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(NewUserDataService.class);

	@EJB
	NewUserDataService newUserDataService;

	/**
	 * @param serviceRequest
	 * @return
	 */
//	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
//	public ServiceResponse createSurveyUser(ServiceRequest serviceRequest)
//			throws Exception {
//		ServiceResponse serviceResponse = null;
//		NewUserRequestTO newUserRequestTO = null;
//		SurveyOrgRequestTO surveyOrgRequestTO = null;
//		SurveyOrgResponseTO surveyOrgResponseTO = null;
//		SurveyUserRequestTO surveyUserRequestTO = null;
//		SurveyUserResponseTO surveyUserResponseTO = null;
//		AuthRequestTO authRequestTO = null;
//		TpOrganization tpOrganization = null;
//		TpUser userTO = null;
//		try {
//			if (serviceRequest.getRequestTransferObjectRevised() != null) {
//				newUserRequestTO = (NewUserRequestTO) serviceRequest
//						.getRequestTransferObjectRevised();
//
//				surveyOrgRequestTO = new SurveyOrgRequestTO();
//				if (newUserRequestTO.getOrganizationName() == null
//						|| newUserRequestTO.getOrganizationName()
//								.equalsIgnoreCase("")) {
//					tpOrganization = newUserDataService
//							.findTpOrganization(newUserRequestTO
//									.getOrganizationID());
//					if (tpOrganization != null) {
//						surveyOrgRequestTO.setOrganizationName(tpOrganization
//								.getName());
//						surveyOrgRequestTO.setOrganizationID(newUserRequestTO
//								.getOrganizationID());
//					}
//				} else {
//					surveyOrgRequestTO.setOrganizationName(newUserRequestTO
//							.getOrganizationName());
//					surveyOrgRequestTO.setOrganizationID(newUserRequestTO
//							.getOrganizationID());
//				}
//
//				surveyOrgRequestTO.setTenantID(newUserRequestTO.getTenantID());
//
//				surveyOrgResponseTO = newUserDataService
//						.createNewOrganization(surveyOrgRequestTO);
//				if (surveyOrgResponseTO != null) {
//					serviceResponse = new ServiceResponse();
//					serviceResponse
//							.setResponseTransferObject(surveyOrgResponseTO);
//
//					surveyUserRequestTO = new SurveyUserRequestTO();
//					surveyUserRequestTO.setAdmin(newUserRequestTO
//							.getAdminEnabled());
//					surveyUserRequestTO.setOrganizationID(surveyOrgResponseTO
//							.getOrganizationalID());
//					surveyUserRequestTO.setUserName(newUserRequestTO
//							.getUserName());
//					surveyUserRequestTO.setSurveyUserID(newUserRequestTO
//							.getSurveyUserID());
//
//					if (surveyUserRequestTO.getUserName() == null
//							&& surveyUserRequestTO.getUserName().equals(""))
//						throw new Exception("Username found null");
//
//					authRequestTO = new AuthRequestTO();
//					authRequestTO.setPersistenceUnitName("TalentPactAuth");
//					authRequestTO
//							.setUserName(surveyUserRequestTO.getUserName());
//					userTO = newUserDataService
//							.checkIfUserExists(authRequestTO);
//					/**
//					 * survey user being created must exist in master database
//					 * if it doesn't new survey user creation must be aborted
//					 * */
//					if (userTO == null)
//						throw new Exception("User doesn't exist");
//
//					surveyUserRequestTO.setSurveyUserID(new Long(userTO
//							.getUserID()));
//					surveyUserResponseTO = newUserDataService
//							.createNewUser(surveyUserRequestTO);
//					if (surveyUserResponseTO == null) {
//						LOGGER_.warn("Survey user creation failed after successful "
//								+ "organization creation");
//						serviceResponse = null;
//					} else {
//						if (surveyUserResponseTO.getSurveyUserID() != null
//								&& surveyUserResponseTO.getSurveyUserID() > 0) {
//							LOGGER_.info("Survey user successfully created");
//						}
//					}
//
//				} else {
//					LOGGER_.warn("Survey organization creation failed hence survey user"
//							+ "creation should be aborted");
//					serviceResponse = null;
//				}
//			}
//
//		} catch (Exception ex) {
//			LOGGER_.debug("new user creration failed for survey : " + ex);
//			serviceResponse = null;
//			throw ex;
//		} finally {
//
//		}
//		return serviceResponse;
//
//	}

}
