package com.talentpact.ui.sysPinCode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysPinCodeConstants;
import com.talentpact.business.dataservice.sysPinCode.SysPinCodeService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysPinCode.bean.SysPinCodeBean;
import com.talentpact.ui.sysPinCode.to.PostOfficeTO;
import com.talentpact.ui.sysPinCode.to.SysPinCodeTO;

/**
 * 
 * @author prachi.bansal
 *
 */

@Named("sysPinCodeController")
@ConversationScoped
public class SysPinCodeController extends CommonController implements Serializable, IDefaultAction{
    
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPinCodeController.class);
    
    @Inject
    MenuBean menuBean;
    
    @Inject
    SysPinCodeBean sysPinCodeBean;
    
    @Inject
    UserSessionBean userSessionBean;
    
    @Inject
    SysPinCodeService sysPinCodeService;

    @Override
    public void initialize() {
      ServiceResponse response=null;
      try{
          sysPinCodeBean.setSysPinCodeTOList(sysPinCodeService.getSysPinCodeList());
          RequestContext requestContext = RequestContext.getCurrentInstance();
          menuBean.setPage("../sysPinCode/sysPinCode.xhtml");
          requestContext.execute(ISysPinCodeConstants.SYSPINCODE_DATATABLE_RESET);
          requestContext.execute(ISysPinCodeConstants.SYSPINCODE_PAGINATION_DATATABLE_RESET);
          
      }catch (Exception e) {
          LOGGER_.error("", e);
      }
        
    }
    
    
    
    public void resetSysPinCode() throws Exception{
        PostOfficeTO to= new PostOfficeTO();
        List<PostOfficeTO> po = new ArrayList<PostOfficeTO>();
        po.add(to);
        sysPinCodeBean.setCityID(null);
        sysPinCodeBean.setCityName(null);
        sysPinCodeBean.setPinCode(null);
        sysPinCodeBean.setPostOfficeName(null);
        List<SysPinCodeTO> cityList=sysPinCodeService.getSysCityList();
        sysPinCodeBean.setCityList(cityList);
        sysPinCodeBean.setPostOfficeList(po);
    }
    
   
    
    public void saveSysPinCode() throws Exception{
        SysPinCodeTO sysPinCodeTO=null;
        RequestContext request=null;
        boolean flag;
        int val=0; 
        int i=0;
        try{
            request=RequestContext.getCurrentInstance();
            List<PostOfficeTO> postOfficeTO=sysPinCodeBean.getPostOfficeList();
           if(postOfficeTO==null||postOfficeTO.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PostOffice Name cannot be blank", ""));
            }
           else if(postOfficeTO!=null && !postOfficeTO.isEmpty()){
               for(PostOfficeTO ps : postOfficeTO){
                   if(ps.getPostOfficeName()==null || ps.getPostOfficeName().equals("")){
                       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PostOffice Name cannot be blank", ""));
                       val=1;
                       break;
                   }
               }
           }
           if(val==0){
                for(PostOfficeTO ps : postOfficeTO){
                    sysPinCodeTO=new SysPinCodeTO();
                    sysPinCodeTO.setCityID(sysPinCodeBean.getCityID());
                    sysPinCodeTO.setPinCode(sysPinCodeBean.getPinCode());
                    sysPinCodeTO.setPostOfficeName(ps.getPostOfficeName());
                    flag=validateSysPinCode(ps, false);
                    if(flag){
                    sysPinCodeService.saveSysPinCode(sysPinCodeTO);
                    i=1;
                    }
                    else{
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PostOffice Name "+ps.getPostOfficeName()+" already presents", ""));
                    }
              }
                    initialize();
                    if(i==1)
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "PinCode added successfully", ""));
                    request.execute(ISysPinCodeConstants.SYSPINCODE_ADD_DIALOG_HIDE);
                    request.execute(ISysPinCodeConstants.SYSPINCODE_DATATABLE_RESET);
                    request.execute(ISysPinCodeConstants.SYSPINCODE_PAGINATION_DATATABLE_RESET);
                    request.update("sysPinCodeListForm");
            }
           
        }catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }
    
    public void editSysPinCode(SysPinCodeTO sysPinCodeTO){
        try{
            List<SysPinCodeTO> sysCodeTOList=sysPinCodeService.getPostOfficeList(sysPinCodeTO.getPinCode());
            List<SysPinCodeTO> cityList=sysPinCodeService.getSysCityList();
            List<PostOfficeTO> postOfficeTOList=new ArrayList<PostOfficeTO>();
            for(SysPinCodeTO ss : sysCodeTOList){
                PostOfficeTO postOffice=new PostOfficeTO();
                postOffice.setPostOfficeName(ss.getPostOfficeName());
                postOffice.setPinCodeID(ss.getPinCodeID());
                postOfficeTOList.add(postOffice);
            }
            sysPinCodeBean.setCityList(cityList);
            sysPinCodeBean.setCityID(sysPinCodeTO.getCityID());
            sysPinCodeBean.setSysPinCodeTOList(sysCodeTOList);
            sysPinCodeBean.setPostOfficeList(postOfficeTOList);
            sysPinCodeBean.setPinCode(sysPinCodeTO.getPinCode());
        }catch(Exception ex){
            LOGGER_.error("", ex);
        }
    }
    
    public void updateSysPinCode(){
        SysPinCodeTO sysPinCodeTO=null;
        RequestContext requestContext=null;
        boolean flag;
        int i=0;
        try{
            requestContext=RequestContext.getCurrentInstance();
            List<PostOfficeTO> postOffice = sysPinCodeBean.getPostOfficeList();
            List<SysPinCodeTO> sysPinCodeList=sysPinCodeBean.getSysPinCodeTOList();
                for(PostOfficeTO ps : postOffice){
                    sysPinCodeTO=new SysPinCodeTO();
                    sysPinCodeTO.setPinCodeID(ps.getPinCodeID());
                    sysPinCodeTO.setCityID(sysPinCodeBean.getCityID());
                    sysPinCodeTO.setPinCode(sysPinCodeBean.getPinCode());
                    sysPinCodeTO.setPostOfficeName(ps.getPostOfficeName());
                    if(ps.getPostOfficeName()==null || ps.getPostOfficeName().equals("")){
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PostOffice Name cannot be blank", ""));
                        continue;
                    }
                    flag=validateSysPinCode(ps, true);
                    if(flag){
                        i=1;
                    sysPinCodeService.updateSysPinCode(sysPinCodeTO);
                    }
                    else{
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PostOffice Name "+ps.getPostOfficeName()+" already presents", ""));
                    }
                }
                if(i==1)
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "PinCode updated successfully", ""));
                initialize();
                requestContext.execute(ISysPinCodeConstants.SYSPINCODE_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysPinCodeConstants.SYSPINCODE_DATATABLE_RESET);
                requestContext.execute(ISysPinCodeConstants.SYSPINCODE_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysPinCodeListForm");
        }catch(Exception ex){
            LOGGER_.error("", ex);
        }
    }

    private boolean validateSysPinCode(PostOfficeTO postOffice, boolean edited) throws Exception {
        List<SysPinCodeTO> sysPinCodeTOList=null;
        try{
            if(edited){
                    sysPinCodeTOList=sysPinCodeService.getUpdatePinCodeList(postOffice.getPinCodeID());
                    for(SysPinCodeTO ss : sysPinCodeTOList){
                        if(ss.getPostOfficeName().trim().equalsIgnoreCase(postOffice.getPostOfficeName().trim())){
                            return false;
                        }
                    }   
            }
            else{
                    sysPinCodeTOList=sysPinCodeService.getSysPinCodeList();
                    for(SysPinCodeTO ss : sysPinCodeTOList){
                        if(ss.getPostOfficeName().trim().equalsIgnoreCase(postOffice.getPostOfficeName().trim())){
                            return false;
                        }
                    }
            }
        
        return true;
        }catch(Exception ex){
            LOGGER_.error("", ex);
            return false;
        }
    }
    
}
