package com.talentpact.ui.sysContentCategory.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysContentCategory.controller.SysContentCategoryController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@Named("sysContentCategoryBean")
@ConversationScoped
public class SysContentCategoryBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysContentCategoryController sysContentCategoryController;

    private Integer categoryID;

    private String categoryName;

    private List<SysContentCategoryTO> sysContentCategoryTOList;

    private boolean renderSysContentCategoryPopup;

    private boolean renderEditSysContentCategoryPopup;

    private boolean renderMessageBox;

    private Set<String> sysCategoryName;

    private Set<String> selectedCategoryName;

    private Boolean renderUpdateButton;

    private String duplicateCategoryName;

    private boolean renderErrorMessage;

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            sysContentCategoryController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * @return the categoryID
     */
    public Integer getCategoryID()
    {
        return categoryID;
    }

    /**
     * @param categoryID
     *            the categoryID to set
     */
    public void setCategoryID(Integer categoryID)
    {
        this.categoryID = categoryID;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName()
    {
        return categoryName;
    }

    /**
     * @param categoryName
     *            the categoryName to set
     */
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    /**
     * @return the sysContentCategoryTOList
     */
    public List<SysContentCategoryTO> getSysContentCategoryTOList()
    {
        return sysContentCategoryTOList;
    }

    /**
     * @param sysContentCategoryTOList
     *            the sysContentCategoryTOList to set
     */
    public void setSysContentCategoryTOList(List<SysContentCategoryTO> sysContentCategoryTOList)
    {
        this.sysContentCategoryTOList = sysContentCategoryTOList;
    }

    /**
     * @return the renderSysContentCategoryPopup
     */
    public boolean isRenderSysContentCategoryPopup()
    {
        return renderSysContentCategoryPopup;
    }

    /**
     * @param renderSysContentCategoryPopup
     *            the renderSysContentCategoryPopup to set
     */
    public void setRenderSysContentCategoryPopup(boolean renderSysContentCategoryPopup)
    {
        this.renderSysContentCategoryPopup = renderSysContentCategoryPopup;
    }

    /**
     * @return the renderEditSysContentCategoryPopup
     */
    public boolean isRenderEditSysContentCategoryPopup()
    {
        return renderEditSysContentCategoryPopup;
    }

    /**
     * @param renderEditSysContentCategoryPopup
     *            the renderEditSysContentCategoryPopup to set
     */
    public void setRenderEditSysContentCategoryPopup(boolean renderEditSysContentCategoryPopup)
    {
        this.renderEditSysContentCategoryPopup = renderEditSysContentCategoryPopup;
    }

    /**
     * @return the sysCategoryName
     */
    public Set<String> getSysCategoryName()
    {
        return sysCategoryName;
    }

    /**
     * @param sysCategoryName
     *            the sysCategoryName to set
     */
    public void setSysCategoryName(Set<String> sysCategoryName)
    {
        this.sysCategoryName = sysCategoryName;
    }

    /**
     * @return the renderMessageBox
     */
    public boolean isRenderMessageBox()
    {
        return renderMessageBox;
    }

    /**
     * @param renderMessageBox
     *            the renderMessageBox to set
     */
    public void setRenderMessageBox(boolean renderMessageBox)
    {
        this.renderMessageBox = renderMessageBox;
    }

    public Boolean getRenderUpdateButton()
    {
        return renderUpdateButton;
    }

    public void setRenderUpdateButton(Boolean renderUpdateButton)
    {
        this.renderUpdateButton = renderUpdateButton;
    }

    public String getDuplicateCategoryName()
    {
        return duplicateCategoryName;
    }

    public void setDuplicateCategoryName(String duplicateCategoryName)
    {
        this.duplicateCategoryName = duplicateCategoryName;
    }

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    public Set<String> getSelectedCategoryName()
    {
        return selectedCategoryName;
    }

    public void setSelectedCategoryName(Set<String> selectedCategoryName)
    {
        this.selectedCategoryName = selectedCategoryName;
    }

}