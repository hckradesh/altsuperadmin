package com.talentpact.ui.sysPeakRecurringType.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.altbenefits.to.SysPeakRecurringTypeTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysPeakRecurringType.controller.SysPeakRecurringTypeController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@SuppressWarnings("serial")
@Named("sysPeakRecurringTypeBean")
@ConversationScoped
public class SysPeakRecurringTypeBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysPeakRecurringTypeController sysPeakRecurringTypeController;

    private Long recurringTypeID;

    private String recurringTypeLabel;

    private boolean renderSysPeakRecurringTypePopup;

    private boolean renderEditSysPeakRecurringTypePopup;

    private boolean renderMessageBox;

    private Boolean renderUpdateButton;

    private String duplicateRecurringType;

    private boolean renderErrorMessage;

    private Set<String> sysPeakRecurringType;

    private Set<String> selectedPeakRecurringType;

    private List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList;

    @Override
    public void initialize()
    {
        try {
            sysPeakRecurringTypeController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    public SysPeakRecurringTypeController getSysPeakRecurringTypeController()
    {
        return sysPeakRecurringTypeController;
    }

    public void setSysPeakRecurringTypeController(SysPeakRecurringTypeController sysPeakRecurringTypeController)
    {
        this.sysPeakRecurringTypeController = sysPeakRecurringTypeController;
    }

    public Long getRecurringTypeID()
    {
        return recurringTypeID;
    }

    public void setRecurringTypeID(Long recurringTypeID)
    {
        this.recurringTypeID = recurringTypeID;
    }

    public String getRecurringTypeLabel()
    {
        return recurringTypeLabel;
    }

    public void setRecurringTypeLabel(String recurringTypeLabel)
    {
        this.recurringTypeLabel = recurringTypeLabel;
    }

    public boolean isRenderSysPeakRecurringTypePopup()
    {
        return renderSysPeakRecurringTypePopup;
    }

    public void setRenderSysPeakRecurringTypePopup(boolean renderSysPeakRecurringTypePopup)
    {
        this.renderSysPeakRecurringTypePopup = renderSysPeakRecurringTypePopup;
    }

    public boolean isRenderEditSysPeakRecurringTypePopup()
    {
        return renderEditSysPeakRecurringTypePopup;
    }

    public void setRenderEditSysPeakRecurringTypePopup(boolean renderEditSysPeakRecurringTypePopup)
    {
        this.renderEditSysPeakRecurringTypePopup = renderEditSysPeakRecurringTypePopup;
    }

    public boolean isRenderMessageBox()
    {
        return renderMessageBox;
    }

    public void setRenderMessageBox(boolean renderMessageBox)
    {
        this.renderMessageBox = renderMessageBox;
    }

    public Boolean getRenderUpdateButton()
    {
        return renderUpdateButton;
    }

    public void setRenderUpdateButton(Boolean renderUpdateButton)
    {
        this.renderUpdateButton = renderUpdateButton;
    }

    public String getDuplicateRecurringType()
    {
        return duplicateRecurringType;
    }

    public void setDuplicateRecurringType(String duplicateRecurringType)
    {
        this.duplicateRecurringType = duplicateRecurringType;
    }

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    public Set<String> getSysPeakRecurringType()
    {
        return sysPeakRecurringType;
    }

    public void setSysPeakRecurringType(Set<String> sysPeakRecurringType)
    {
        this.sysPeakRecurringType = sysPeakRecurringType;
    }

    public Set<String> getSelectedPeakRecurringType()
    {
        return selectedPeakRecurringType;
    }

    public void setSelectedPeakRecurringType(Set<String> selectedPeakRecurringType)
    {
        this.selectedPeakRecurringType = selectedPeakRecurringType;
    }

    public List<SysPeakRecurringTypeTO> getSysPeakRecurringTypeTOList()
    {
        return sysPeakRecurringTypeTOList;
    }

    public void setSysPeakRecurringTypeTOList(List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList)
    {
        this.sysPeakRecurringTypeTOList = sysPeakRecurringTypeTOList;
    }


}