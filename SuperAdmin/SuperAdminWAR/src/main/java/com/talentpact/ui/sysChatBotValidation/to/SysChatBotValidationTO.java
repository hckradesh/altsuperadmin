package com.talentpact.ui.sysChatBotValidation.to;

import java.io.Serializable;

public class SysChatBotValidationTO implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private Integer sequence;
	
	private Integer SysValidationID;
	
	private Integer sysValidationTypeID;
	
	private String paramValues;
	
	private Integer uiFormFieldID;

	
	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Integer getSysValidationID() {
		return SysValidationID;
	}

	public void setSysValidationID(Integer sysValidationID) {
		SysValidationID = sysValidationID;
	}

	public Integer getSysValidationTypeID() {
		return sysValidationTypeID;
	}

	public void setSysValidationTypeID(Integer sysValidationTypeID) {
		this.sysValidationTypeID = sysValidationTypeID;
	}

	public String getParamValues() {
		return paramValues;
	}

	public void setParamValues(String paramValues) {
		this.paramValues = paramValues;
	}

	public Integer getUiFormFieldID() {
		return uiFormFieldID;
	}

	public void setUiFormFieldID(Integer uiFormFieldID) {
		this.uiFormFieldID = uiFormFieldID;
	}



}// end of class