package com.talentpact.ui.sysEntity.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.talentpact.business.application.transport.output.SysEntityColumnTO;
import com.talentpact.business.application.transport.output.SysEntityTO;
import com.talentpact.business.application.transport.output.SysSubModuleTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysEntity.controller.SysEntityController;

/*
 * @author raghvendra.mishra
 */

@Named("sysEntityBean")
@ConversationScoped
public class SysEntityBean extends CommonBean implements Serializable,IDefaultAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	SysEntityController sysEntityController;
	
    private Integer entityId;
	
	private String name;
	
	private String description;

	private String tableName;
   
	private Integer tenantID;

	private Timestamp createdDate;

	private Timestamp modifiedDate;

	private Integer createdBy;

	private Integer modifiedBy;

	private String primaryKey;

	private String lableColumn;

	private Integer moduleId;
	
	private List<SysEntityTO> sysEntityTOList = null;
	private List<SysEntityTO> filterSysEntityTOList;
	
	private SysEntityTO sysEntityTO = new SysEntityTO();
    
	private Integer primaryKeyColumnID;
	
	private List<String> sysEntityTableNameList;
	
	private List<SysSubModuleTO> sysSubModuleList;
	
	private boolean renderSysColumnDataTable;
	
	private List<SysEntityColumnTO> sysEntityColumnList;
	
	private boolean sysEntityTableNameListReadonly;
	
	private boolean renderSaveButton;
	
	private boolean renderUpdateButton;
	
	private boolean selectTableEnable;
	
    @Override
    public void initialize() {
    	endConversation();
    	beginConversation();
		sysEntityController.initialize();
	}

    // Constructors

    /** default constructor */
    public SysEntityBean()
    {
    }

    /** minimal constructor */
    public SysEntityBean(String name, String tableName)
    {
        this.name = name;
        this.tableName = tableName;
    }

    public List<SysEntityTO> getFilterSysEntityTOList() {
		return filterSysEntityTOList;
	}
    public void setFilterSysEntityTOList(List<SysEntityTO> filterSysEntityTOList) {
		this.filterSysEntityTOList = filterSysEntityTOList;
	}
    /** full constructor */
    public SysEntityBean(String name, String description, String tableName)
    {
        this.name = name;
        this.description = description;
        this.tableName = tableName;
    }

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getTenantID() {
		return tenantID;
	}

	public void setTenantID(Integer tenantID) {
		this.tenantID = tenantID;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getLableColumn() {
		return lableColumn;
	}

	public void setLableColumn(String lableColumn) {
		this.lableColumn = lableColumn;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public List<SysEntityTO> getSysEntityTOList() {
		return sysEntityTOList;
	}

	public void setSysEntityTOList(List<SysEntityTO> sysEntityTOList) {
		this.sysEntityTOList = sysEntityTOList;
	}

	public SysEntityTO getSysEntityTO() {
		return sysEntityTO;
	}

	public void setSysEntityTO(SysEntityTO sysEntityTO) {
		this.sysEntityTO = sysEntityTO;
	}

	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getPrimaryKeyColumnID() {
		return primaryKeyColumnID;
	}

	public void setPrimaryKeyColumnID(Integer primaryKeyColumnID) {
		this.primaryKeyColumnID = primaryKeyColumnID;
	}

	public List<String> getSysEntityTableNameList() {
		return sysEntityTableNameList;
	}

	public void setSysEntityTableNameList(List<String> sysEntityTableNameList) {
		this.sysEntityTableNameList = sysEntityTableNameList;
	}

	public List<SysSubModuleTO> getSysSubModuleList() {
		return sysSubModuleList;
	}

	public void setSysSubModuleList(List<SysSubModuleTO> sysSubModuleList) {
		this.sysSubModuleList = sysSubModuleList;
	}

	public boolean isRenderSysColumnDataTable() {
		return renderSysColumnDataTable;
	}

	public void setRenderSysColumnDataTable(boolean renderSysColumnDataTable) {
		this.renderSysColumnDataTable = renderSysColumnDataTable;
	}

	public List<SysEntityColumnTO> getSysEntityColumnList() {
		return sysEntityColumnList;
	}

	public void setSysEntityColumnList(List<SysEntityColumnTO> sysEntityColumnList) {
		this.sysEntityColumnList = sysEntityColumnList;
	}

	public boolean isSysEntityTableNameListReadonly() {
		return sysEntityTableNameListReadonly;
	}

	public void setSysEntityTableNameListReadonly(
			boolean sysEntityTableNameListReadonly) {
		this.sysEntityTableNameListReadonly = sysEntityTableNameListReadonly;
	}


	public boolean isRenderSaveButton() {
		return renderSaveButton;
	}

	public void setRenderSaveButton(boolean renderSaveButton) {
		this.renderSaveButton = renderSaveButton;
	}

	public boolean isRenderUpdateButton() {
		return renderUpdateButton;
	}

	public void setRenderUpdateButton(boolean renderUpdateButton) {
		this.renderUpdateButton = renderUpdateButton;
	}

	public boolean isSelectTableEnable() {
		return selectTableEnable;
	}

	public void setSelectTableEnable(boolean selectTableEnable) {
		this.selectTableEnable = selectTableEnable;
	}

}
