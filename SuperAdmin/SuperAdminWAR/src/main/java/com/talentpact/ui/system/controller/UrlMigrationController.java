/**
 * 
 */
package com.talentpact.ui.system.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altreports.constants.IConstant;
import com.alt.system.constant.InstanceMigrationIConstant;
import com.alt.system.transport.AltSyncUrlTO;
import com.alt.system.transport.SchemaTO;
import com.alt.system.transport.UrlTO;
import com.alt.system.transport.input.InstanceMigrationRequestTO;
import com.alt.system.transport.output.InstanceMigrationResponseTO;
import com.talentpact.business.service.system.InstanceMigrationService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.system.bean.UrlMigrationBean;

/**
 * @author javed.ali
 *
 */
@Named("urlMigrationController")
@ConversationScoped
public class UrlMigrationController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 14399071112168L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(UrlMigrationController.class);

    @Inject
    UrlMigrationBean urlMigrationBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    MenuBean menuBean;

    @EJB
    InstanceMigrationService instanceMigrationService;

    @Override
    public void initialize()
    {
        List<AltSyncUrlTO> altSyncUrlTOList = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            altSyncUrlTOList = instanceMigrationService.getAltSyncUrlIst();
            urlMigrationBean.setAltSyncUrlTOList(altSyncUrlTOList);
            menuBean.setPage("../system/altUrlList.xhtml");
            requestContext.execute(InstanceMigrationIConstant.ALT_SYNC_URL_DATATABLE_RESET);
            requestContext.execute(InstanceMigrationIConstant.ALT_SYNC_URL_PAGINATION_DATATABLE_RESET);
            urlMigrationBean.setSelectedAppID(null);
            urlMigrationBean.setSelectedDbConfigId(-1);
            urlMigrationBean.setSelectedEnvID(null);
            urlMigrationBean.setActive(false);
            

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }

    }

    /**
     * 
     * @return
     */
    private InstanceMigrationResponseTO fetchAllInfo()
    {
        List<SchemaTO> schemaTOList = null;
        InstanceMigrationResponseTO instanceMigrationResponseTO = null;
        try {
            urlMigrationBean.setSelectedAppID(null);
            urlMigrationBean.setRenderFormMessage(false);
            urlMigrationBean.setSelectedDbConfigId(null);
            urlMigrationBean.setSelectedEnvID(null);
            instanceMigrationResponseTO = instanceMigrationService.fetchAllInfo();
            urlMigrationBean.setSchemaTOList(schemaTOList);
            urlMigrationBean.setFilteredSchemaTOList(schemaTOList);
            urlMigrationBean.setRenderSchemaPanel(true);

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
        return instanceMigrationResponseTO;
    }

    /**
     * 
     */
    public void addUrl()
    {
        InstanceMigrationResponseTO instanceMigrationResponseTO = null;
        try {
            urlMigrationBean.setSelectedAppID(null);
            urlMigrationBean.setSelectedDbConfigId(-1);
            urlMigrationBean.setSelectedEnvID(null);
            urlMigrationBean.setActive(false);
            urlMigrationBean.setRenderFormMessage(true);
            urlMigrationBean.setSelectedUrlCompositeID(null);
            instanceMigrationResponseTO = fetchAllInfo();
            if (instanceMigrationResponseTO != null) {
                urlMigrationBean.setSysEnvTOList(instanceMigrationResponseTO.getSysEnvTOList());
                urlMigrationBean.setAppTOList(instanceMigrationResponseTO.getAppTOList());
                urlMigrationBean.setDatabaseConfigTOList(instanceMigrationResponseTO.getDatabaseConfigTOList());
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }


    public void fetchUrlList(Integer dbServerID, Long appID)
    {
        List<UrlTO> urlTOList = null;
        try {
            if (dbServerID > 0 && appID != null) {
                urlTOList = instanceMigrationService.fetchUrlList(dbServerID, appID);
                urlMigrationBean.setUrlTOList(urlTOList);
                urlMigrationBean.setSelectedUrlCompositeID(null);
            } 
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, InstanceMigrationIConstant.URL_LIST_ERROR_MESSAGE, ""));
            LOGGER_.error("", ex);
        }

    }

    public void saveAltUrl()
    {
        InstanceMigrationRequestTO instanceMigrationRequestTO = null;
        String compositeID = null;
        InstanceMigrationResponseTO instanceMigrationResponseTO = null;
        boolean valid = true;
        try {
            compositeID = urlMigrationBean.getSelectedUrlCompositeID();
            if (urlMigrationBean.getSelectedEnvID() <= 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Environment is required.", ""));
                valid = false;
            }

            if (urlMigrationBean.getSelectedAppID() <= 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "App Name is required.", ""));
                valid = false;
            }
            if (urlMigrationBean.getSelectedDbConfigId() <= 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Server Name is required.", ""));
                valid = false;
            }
            if (compositeID == "") {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Url is required.", ""));
                valid = false;
            }


            if (valid) {
                instanceMigrationRequestTO = new InstanceMigrationRequestTO();
                instanceMigrationRequestTO.setAppId(new Long(urlMigrationBean.getSelectedAppID()).intValue());
                instanceMigrationRequestTO.setDbConfigId(urlMigrationBean.getSelectedDbConfigId());
                instanceMigrationRequestTO.setEnvID(urlMigrationBean.getSelectedEnvID());
                if (compositeID != null) {
                    String arr[] = compositeID.split(IConstant.HASH_SPERATOR_STRING);
                    instanceMigrationRequestTO.setUrlName(arr[1]);
                    instanceMigrationRequestTO.setOrganizatonID(Integer.parseInt(arr[2]));
                }
                instanceMigrationResponseTO = instanceMigrationService.saveAltUrl(instanceMigrationRequestTO);

                if (instanceMigrationResponseTO != null) {
                    urlMigrationBean.setRenderFormMessage(true);
                    RequestContext.getCurrentInstance().execute(InstanceMigrationIConstant.ALT_SYNC_URL_DLG_BOX);
                    RequestContext.getCurrentInstance().execute(InstanceMigrationIConstant.ALT_SYNC_URL_PAGINATION_DATATABLE_RESET);
                    RequestContext.getCurrentInstance().execute(InstanceMigrationIConstant.ALT_SYNC_URL_DATATABLE_RESET);
                    RequestContext.getCurrentInstance().update(":urlForm");
                    urlMigrationBean.setAltSyncUrlTOList(instanceMigrationService.getAltSyncUrlIst());
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "url saved successfully.", ""));
                } else {
                    urlMigrationBean.setRenderFormMessage(true);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "error occured in saving url.", ""));
                }
            }
        } catch (Exception ex) {
            urlMigrationBean.setRenderFormMessage(true);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "error occured in saving url.", ""));
            LOGGER_.error("", ex);
        }

    }


}
