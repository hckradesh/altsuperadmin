package com.talentpact.ui.sysFieldType.to;

import java.io.Serializable;

public class SysFieldTypeTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer sysFieldTypeID;
    
	private String fieldType;

	public Integer getSysFieldTypeID() {
		return sysFieldTypeID;
	}

	public void setSysFieldTypeID(Integer sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	
	
	
}
