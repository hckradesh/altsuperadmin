package com.talentpact.ui.sysContentType.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.model.configuration.SysContentCategory;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysContentType.controller.SysContentTypeController;

/**
 * 
 * @author vivek.goyal
 *
 */


@Named("sysContentTypeBean")
@ConversationScoped
public class SysContentTypeBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    SysContentTypeController sysContentTypeController;

    private Integer contentCategoryID;

    private String categoryName;

    private Integer sysTypeID;

    private String sysType;

    private SysContentCategory sysContentCategory;

    private List<SysContentTypeTO> sysContentTypeTOList;

    private Set<SysContentTypeTO> contentCategoryList;

    private List<SysContentTypeTO> newContentCategoryTOList;

    private List<SysContentTypeTO> contentTypeFilteredList;

    private List<SysContentTypeTO> editContentCategoryTOList;

    private SysContentTypeTO selectedTypeTO;

    private boolean renderSysContentCategoryPopup;

    private boolean renderEditSysContentCategoryPopup;

    private Set<String> contentNameList;

    private Set<String> selectedContentNameList;

    private Integer duplicateContentCategoryID;

    private String duplicateSysType;

    private boolean renderErrorMessage;

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            sysContentTypeController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public Set<String> getContentNameList()
    {
        return contentNameList;
    }


    public void setContentNameList(Set<String> contentNameList)
    {
        this.contentNameList = contentNameList;
    }


    /**
     * @return the sysTypeID
     */
    public Integer getSysTypeID()
    {
        return sysTypeID;
    }

    /**
     * @param sysTypeID the sysTypeID to set
     */
    public void setSysTypeID(Integer sysTypeID)
    {
        this.sysTypeID = sysTypeID;
    }

    /**
     * @return the sysType
     */
    public String getSysType()
    {
        return sysType;
    }

    /**
     * @param sysType the sysType to set
     */
    public void setSysType(String sysType)
    {
        this.sysType = sysType;
    }

    /**
     * @return the sysContentCategory
     */
    public SysContentCategory getSysContentCategory()
    {
        return sysContentCategory;
    }

    /**
     * @param sysContentCategory the sysContentCategory to set
     */
    public void setSysContentCategory(SysContentCategory sysContentCategory)
    {
        this.sysContentCategory = sysContentCategory;
    }

    /**
     * @return the sysContentTypeTOList
     */
    public List<SysContentTypeTO> getSysContentTypeTOList()
    {
        return sysContentTypeTOList;
    }

    /**
     * @param sysContentTypeTOList the sysContentTypeTOList to set
     */
    public void setSysContentTypeTOList(List<SysContentTypeTO> sysContentTypeTOList)
    {
        this.sysContentTypeTOList = sysContentTypeTOList;
    }

    /**
     * @return the contentCategoryList
     */
    public Set<SysContentTypeTO> getContentCategoryList()
    {
        return contentCategoryList;
    }

    /**
     * @param contentCategoryList the contentCategoryList to set
     */
    public void setContentCategoryList(Set<SysContentTypeTO> contentCategoryList)
    {
        this.contentCategoryList = contentCategoryList;
    }

    /**
     * @return the newContentCategoryTOList
     */
    public List<SysContentTypeTO> getNewContentCategoryTOList()
    {
        return newContentCategoryTOList;
    }

    /**
     * @param newContentCategoryTOList the newContentCategoryTOList to set
     */
    public void setNewContentCategoryTOList(List<SysContentTypeTO> newContentCategoryTOList)
    {
        this.newContentCategoryTOList = newContentCategoryTOList;
    }

    /**
     * @return the renderSysContentCategoryPopup
     */
    public boolean isRenderSysContentCategoryPopup()
    {
        return renderSysContentCategoryPopup;
    }

    /**
     * @param renderSysContentCategoryPopup the renderSysContentCategoryPopup to set
     */
    public void setRenderSysContentCategoryPopup(boolean renderSysContentCategoryPopup)
    {
        this.renderSysContentCategoryPopup = renderSysContentCategoryPopup;
    }

    /**
     * @return the renderEditSysContentCategoryPopup
     */
    public boolean isRenderEditSysContentCategoryPopup()
    {
        return renderEditSysContentCategoryPopup;
    }

    /**
     * @param renderEditSysContentCategoryPopup the renderEditSysContentCategoryPopup to set
     */
    public void setRenderEditSysContentCategoryPopup(boolean renderEditSysContentCategoryPopup)
    {
        this.renderEditSysContentCategoryPopup = renderEditSysContentCategoryPopup;
    }

    public List<SysContentTypeTO> getContentTypeFilteredList()
    {
        return contentTypeFilteredList;
    }

    public void setContentTypeFilteredList(List<SysContentTypeTO> contentTypeFilteredList)
    {
        this.contentTypeFilteredList = contentTypeFilteredList;
    }

    /**
     * @return the contentCategoryID
     */
    public Integer getContentCategoryID()
    {
        return contentCategoryID;
    }

    /**
     * @param contentCategoryID the contentCategoryID to set
     */
    public void setContentCategoryID(Integer contentCategoryID)
    {
        this.contentCategoryID = contentCategoryID;
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName()
    {
        return categoryName;
    }

    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public List<SysContentTypeTO> getEditContentCategoryTOList()
    {
        return editContentCategoryTOList;
    }

    public void setEditContentCategoryTOList(List<SysContentTypeTO> editContentCategoryTOList)
    {
        this.editContentCategoryTOList = editContentCategoryTOList;
    }

    public SysContentTypeTO getSelectedTypeTO()
    {
        return selectedTypeTO;
    }

    public void setSelectedTypeTO(SysContentTypeTO selectedTypeTO)
    {
        this.selectedTypeTO = selectedTypeTO;
    }


    public Integer getDuplicateContentCategoryID()
    {
        return duplicateContentCategoryID;
    }


    public void setDuplicateContentCategoryID(Integer duplicateContentCategoryID)
    {
        this.duplicateContentCategoryID = duplicateContentCategoryID;
    }


    public String getDuplicateSysType()
    {
        return duplicateSysType;
    }


    public void setDuplicateSysType(String duplicateSysType)
    {
        this.duplicateSysType = duplicateSysType;
    }

    public Set<String> getSelectedContentNameList()
    {
        return selectedContentNameList;
    }

    public void setSelectedContentNameList(Set<String> selectedContentNameList)
    {
        this.selectedContentNameList = selectedContentNameList;
    }

}
