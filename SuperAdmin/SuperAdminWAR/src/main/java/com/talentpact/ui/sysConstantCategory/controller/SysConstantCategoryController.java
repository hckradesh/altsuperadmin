/**
 * 
 */
package com.talentpact.ui.sysConstantCategory.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.input.SysConstantCategoryRequestTO;
import com.talentpact.business.application.transport.output.SysConstantCategoryResponseTO;
import com.talentpact.business.dataservice.SysConstantCategory.SysConstantCategoryService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysConstantCategory.bean.SysConstantCategoryBean;

/**
 * @author pankaj.sharma1
 *
 */
@SuppressWarnings("serial")
@Named("sysConstantCategoryController")
@ConversationScoped
public class SysConstantCategoryController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysConstantCategoryController.class);


    @Inject
    MenuBean menuBean;

    @Inject
    SysConstantCategoryBean sysConstantCategoryBean;

    @Inject
    SysConstantCategoryService sysConstantCategoryService;

    @Inject
    UserSessionBean userSessionBean;

    @Override
    public void initialize()
    {
        List<SysConstantCategoryResponseTO> sysConstantCategoryTOList = null;
        try {
            sysConstantCategoryTOList = new ArrayList<SysConstantCategoryResponseTO>();
            sysConstantCategoryTOList = sysConstantCategoryService.getSysConstantCategoryList();
            if (sysConstantCategoryTOList != null && !sysConstantCategoryTOList.isEmpty()) {
                sysConstantCategoryBean.setSysConstantCategoryTOList(sysConstantCategoryTOList);
                menuBean.setPage("../sysConstantCategory/sysConstantCategory.xhtml");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    private void resetBean()
    {
        sysConstantCategoryBean.setRenderSysConstantCategoryPopup(true);
        sysConstantCategoryBean.setSysConstantCategoryName(null);

    }

    public void refreshPopUp(SysConstantCategoryResponseTO constantTO)
    {
        try {
            sysConstantCategoryBean.setSysConstantCategoryName(constantTO.getSysConstantCategoryName());
            sysConstantCategoryBean.setSysConstantCategoryID(constantTO.getSysConstantCategoryID());


        } catch (Exception e) {
        }
    }

    public void save()
        throws Exception
    {
        SysConstantCategoryRequestTO sysConstantCatReqTO = null;
        String sysConstantCategoryName = null;
        Boolean isSysConstantCategoryNameExist = null;
        RequestContext context = null;
        try {
            context = RequestContext.getCurrentInstance();
            sysConstantCategoryName = sysConstantCategoryBean.getSysConstantCategoryName().trim();
            //checking whether a SysConstant Category of this name exist or not 
            if (sysConstantCategoryName != null && !sysConstantCategoryName.equals("")) {
                isSysConstantCategoryNameExist = checkExistingSysConstantCategoryNameinList(sysConstantCategoryName, sysConstantCategoryBean.getSysConstantCategoryTOList());
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered  SysConstantCategory name is Invalid ", ""));
                return;
            }

            //end here 
            if (isSysConstantCategoryNameExist != null) {
                if (isSysConstantCategoryNameExist == false) {
                    sysConstantCatReqTO = new SysConstantCategoryRequestTO();
                    sysConstantCatReqTO.setSysConstantCategoryName(sysConstantCategoryBean.getSysConstantCategoryName());
                    sysConstantCatReqTO.setLoggedInUserId(userSessionBean.getUserID().intValue());

                    sysConstantCategoryService.saveSysConstantCategory(sysConstantCatReqTO);
                    initialize();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Entered  SysConstantCategory name has been saved", ""));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Category with name '" + sysConstantCategoryBean.getSysConstantCategoryName() + "' already exists", ""));
                    return;
                }
            }
            context.execute("PF('addSysConstantCategoryModal').hide();");
            context.update("sysConstantCategoryListForm");
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected Error occured", ""));

            context.execute("PF('sysConstantCategoryUpdateModal').hide();");
        }
    }


    public void update()
    {
        SysConstantCategoryRequestTO reqTO = null;
        String sysConstantCategoryName = null;
        Boolean isSysConstantCategoryNameExist = null;
        RequestContext context = null;

        try {
            context = RequestContext.getCurrentInstance();
            Integer catID = sysConstantCategoryBean.getSysConstantCategoryID();
            boolean isCatIDExist = checkCategoryIDinList(catID, sysConstantCategoryBean.getSysConstantCategoryTOList());
            sysConstantCategoryName = sysConstantCategoryBean.getSysConstantCategoryName().trim();

            if (sysConstantCategoryName != null && !sysConstantCategoryName.equals("")) {
                isSysConstantCategoryNameExist = checkSysConstantCategoryNameinList(sysConstantCategoryName.trim(), catID, sysConstantCategoryBean.getSysConstantCategoryTOList());
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered SysConstantCategory name is Invalid ", ""));
                return;
            }

            if (isSysConstantCategoryNameExist != null) {

                if (isCatIDExist == true) {
                    reqTO = new SysConstantCategoryRequestTO();
                    if (isSysConstantCategoryNameExist == false) {
                        reqTO.setSysConstantCategoryName(sysConstantCategoryName);
                    } else {
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Category with name '" + sysConstantCategoryBean.getSysConstantCategoryName().trim()
                                        + "' already exists.", ""));
                        return;
                    }
                    reqTO.setLoggedInUserId(userSessionBean.getUserID().intValue());
                    reqTO.setSysConstantCategoryID(catID);
                    sysConstantCategoryService.saveSysConstantCategory(reqTO);
                    initialize();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Updated", ""));
                }
            }
            context.execute("PF('sysConstantCategoryUpdateModal').hide();");
        } catch (Exception ex) {
            LOGGER_.warn("Error : " + ex.toString());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UnExpected Error", ""));
            context.execute("PF('sysConstantCategoryUpdateModal').hide();");

        }
    }


    public static boolean checkSysConstantCategoryNameinList(String catName, Integer catID, List<SysConstantCategoryResponseTO> catTOList)
    {

        for (SysConstantCategoryResponseTO cat : catTOList) {
            if (cat != null && cat.getSysConstantCategoryName() != null && catID != cat.getSysConstantCategoryID()
                    && (cat.getSysConstantCategoryName()).equalsIgnoreCase(catName.trim())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkExistingSysConstantCategoryNameinList(String catName, List<SysConstantCategoryResponseTO> catTOList)
    {
        if (catTOList == null || catTOList.isEmpty()) {
            return false;
        }

        for (SysConstantCategoryResponseTO cat : catTOList) {
            if (cat != null && cat.getSysConstantCategoryName() != null && (cat.getSysConstantCategoryName()).equalsIgnoreCase(catName.trim())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCategoryIDinList(Integer catID, List<SysConstantCategoryResponseTO> catTOList)
    {

        for (SysConstantCategoryResponseTO cat : catTOList) {
            if (cat != null && cat.getSysConstantCategoryID() != null && (cat.getSysConstantCategoryID()).equals(catID)) {
                return true;
            }
        }
        return false;
    }


}
