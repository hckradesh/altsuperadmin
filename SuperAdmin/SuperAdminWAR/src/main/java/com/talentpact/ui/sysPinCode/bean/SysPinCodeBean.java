package com.talentpact.ui.sysPinCode.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysPinCode.controller.SysPinCodeController;
import com.talentpact.ui.sysPinCode.to.SysPinCodeTO;
import com.talentpact.ui.sysPinCode.to.PostOfficeTO;


/**
 * 
 * @author prachi.bansal
 *
 */

@SuppressWarnings("serial")
@Named("sysPinCodeBean")
@ConversationScoped
public class SysPinCodeBean extends CommonBean implements Serializable, IDefaultAction{
    
    @Inject
    SysPinCodeController sysPinCodeController;
    
    private List<SysPinCodeTO> sysPinCodeTOList;
    
    private List<SysPinCodeTO> cityList;
    
    private SysPinCodeTO sysPinCodeTO;
    
    private Long pinCodeID;
    
    private String pinCode;
    
    private String postOfficeName;
    
    private Long cityID;
    
    private String cityName;

    private boolean renderErrorMessage;
    
    private List<PostOfficeTO> postOfficeList=new ArrayList<PostOfficeTO>();

   

    @Override
    public void initialize() {
        try {
        	endConversation();
            beginConversation();
            sysPinCodeController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public List<SysPinCodeTO> getSysPinCodeTOList() {
        return sysPinCodeTOList;
    }

    public void setSysPinCodeTOList(List<SysPinCodeTO> sysPinCodeTOList) {
        this.sysPinCodeTOList = sysPinCodeTOList;
    }

    public List<SysPinCodeTO> getCityList() {
        return cityList;
    }

    public void setCityList(List<SysPinCodeTO> cityList) {
        this.cityList = cityList;
    }

    public SysPinCodeTO getSysPinCodeTO() {
        return sysPinCodeTO;
    }

    public void setSysPinCodeTO(SysPinCodeTO sysPinCodeTO) {
        this.sysPinCodeTO = sysPinCodeTO;
    }

    public Long getPinCodeID() {
        return pinCodeID;
    }

    public void setPinCodeID(Long pinCodeID) {
        this.pinCodeID = pinCodeID;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPostOfficeName() {
        return postOfficeName;
    }

    public void setPostOfficeName(String postOfficeName) {
        this.postOfficeName = postOfficeName;
    }

    public Long getCityID() {
        return cityID;
    }

    public void setCityID(Long cityID) {
        this.cityID = cityID;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
    public boolean isRenderErrorMessage() {
        return renderErrorMessage;
    }



    public void setRenderErrorMessage(boolean renderErrorMessage) {
        this.renderErrorMessage = renderErrorMessage;
    }

    public List<PostOfficeTO> getPostOfficeList() {
        return postOfficeList;
    }

    
    public void setPostOfficeList(List<PostOfficeTO> postOfficeList) {
        this.postOfficeList = postOfficeList;
    }
    
    public void addPostOffice(){
        PostOfficeTO to= new PostOfficeTO();
        if(this.postOfficeList==null)
        {
            this.postOfficeList=new ArrayList<PostOfficeTO>();
        }
        this.postOfficeList.add(to);
    }
    
    public void removePostOffice(PostOfficeTO postOfficeTO){
        this.postOfficeList.remove(postOfficeTO);
    }
    
}
