/**
 * 
 */
package com.talentpact.ui.sysEntityColumn.dataservice;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.dataservice.DS.SysEntityColumnDS;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysEntityColumnDataService {

	@EJB
	private SysEntityColumnDS sysEntityColumnDS;
	
	/**
	 * @param sysEntityIDList
	 * @return
	 */
	public List<SysEntityColumnResponseTO> getSysEntityColumnsByEntityID(
			List<Integer> sysEntityIDList) throws Exception{
		return sysEntityColumnDS.getSysEntityColumnsByEntityID(sysEntityIDList);
	}

	
}
