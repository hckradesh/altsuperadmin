/**
 *
 */
package com.talentpact.ui.menu.controller;


import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.common.controller.LookupAction;
import com.talentpact.ui.login.bean.LoginBean;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysEntity.bean.SysEntityBean;
import com.talentpact.ui.tenants.newtenant.bean.NewTenantBean;
import com.talentpact.ui.tenants.organization.bean.NewOrganizationBean;

/**
 * @author radhamadhab.dalai
 *
 */
@Named("menuAction")
@SessionScoped
public class MenuAction implements IDefaultAction, Serializable
{

	private static final long serialVersionUID = -3155174902080354856L;

	@Inject
    MenuBean menuBean;

    @Inject
    LookupAction lookupAction;

    @Inject
    LoginBean loginBean;

    @Inject
    NewTenantBean newTenantBean;

    @Inject
    NewOrganizationBean newOrganizationBean;

    @Inject
    SysEntityBean sysEntityBean;


    public void openLeftMenuForOrg()
    {
        loginBean.setMenuName("Organisation");
        newOrganizationBean.initialize();
    }

    public void openLeftMenuForTenant()
    {
        loginBean.setMenuName("Tenant");
        newTenantBean.initialize();
    }

    public void openMenuRedisManager()
    {
        loginBean.setMenuName("RedisManager");
        menuBean.setPage(null);
    }

    public void openAdhocReportCreationPage()
    {
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/adhocReportMain.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openAdhocReportViewPage()
    {
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/viewAdhocReport.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openApplicationInputConfigurationPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/viewApplicationInput.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openConstantConfigurationPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/viewConstant.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openProcessingLogicTemplateConfigurationPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/viewProcessingLogicTemplate.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openOrgReportAssignmentPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/assignReportOrgMapping.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openOrgDerivedTableAssignmentPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/assignDerivedTableOrgMapping.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openActivateDeactivateReportsPage(){
    	loginBean.setMenuName("AdhocReporting");
        HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String[] split = origRequest.getRequestURL().toString().split("/");
        String[] split1 = split[2].split(":");
        menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/deactivateReports.jsf");
        menuBean.setPage("../adhocReport.xhtml");
    }

    public void openSysEntityEditCreationPage()
    {
        loginBean.setMenuName("SysEntity");
        sysEntityBean.initialize();
        menuBean.setPage("../sysEntity.xhtml");
    }

    public void openReportRowsLimitAssignmentPage() {
    	loginBean.setMenuName("AdhocReporting");
    	HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	String[] split = origRequest.getRequestURL().toString().split("/");
    	String[] split1 = split[2].split(":");
    	menuBean.setAdhocReportPage("https:" + "//" + split1[0] + "/" + split[3] + "/configuration/assignReportRowsLimit.jsf");
    	menuBean.setPage("../adhocReport.xhtml");
    }

    @Override
    public void initialize()
    {
        try {

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }


}
