package com.talentpact.ui.survey.user.common.transport.form.exception;

public class CustomFormException extends Exception {


    private static final long serialVersionUID = 1232139515253115828L;

    private String message;


    public CustomFormException(String message) {
        super(message);
    }

    public CustomFormException(String message, Exception e) {
        super(message, e);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
