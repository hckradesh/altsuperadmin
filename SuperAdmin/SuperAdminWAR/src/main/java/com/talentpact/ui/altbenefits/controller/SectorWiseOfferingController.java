/**
 * 
 */
package com.talentpact.ui.altbenefits.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.SectorVendorOfferingTO;
import com.alt.altbenefits.to.SysSectorTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.altbenefits.transport.output.BenefitResponseTO;
import com.alt.common.constants.CoreConstants;
import com.talentpact.business.common.constants.IBenefitsConstants;
import com.talentpact.business.service.altbenefits.SysSectorService;
import com.talentpact.business.service.altbenefits.SysSectorVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysVendorOfferingTemplateService;
import com.talentpact.ui.altbenefits.bean.SectorVendorOfferingBean;
import com.talentpact.ui.altbenefits.constant.IBenefitsConstant;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * @author javed.ali
 *
 */
@Named("sectorWiseOfferingController")
@ConversationScoped
public class SectorWiseOfferingController  extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 14399078423658L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SectorWiseOfferingController.class);
    
    @Inject
    SectorVendorOfferingBean sectorVendorOfferingBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    MenuBean menuBean;

    @Inject
    SysSectorVendorOfferingSequenceService sectorVendorOfferingSequenceService;

    @Inject
    SysVendorOfferingTemplateService sysVendorOfferingTemplateService;
    
   @EJB
    SysSectorService sectorService;

    @Override
    public void initialize()
    {
        BenefitResponseTO benefitResponseTO=null;
        Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap = null;
        try {
            sectorVendorOfferingBean.setSelectedSector(null);
            RequestContext requestContext = RequestContext.getCurrentInstance();
            benefitResponseTO = sectorVendorOfferingSequenceService.getBenefitsSectorBasedList();
            sectorVendorOfferingmap=benefitResponseTO.getSectorVendorOfferingmap();
            sectorVendorOfferingBean.setSectorVendorOfferingTOList(creatingGroupBasedListFromGroupVendoringMap(sectorVendorOfferingmap));
            sectorVendorOfferingBean.setSectorVendorOfferingmap(sectorVendorOfferingmap);
             menuBean.setPage(IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_HOME);
            requestContext.execute(IBenefitsConstants.SYS_SECTOR_VENDOR_DATATABLE_RESET);
            requestContext.execute(IBenefitsConstants.SYS_SECTOR_VENDOR_PAGINATION_DATATABLE_RESET);
            RequestContext.getCurrentInstance().execute("scrollUp()");
            
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }

    }
/**
 * 
 * @param sectorVendorOfferingmap
 * @return
 */
    private List<SectorVendorOfferingTO> creatingGroupBasedListFromGroupVendoringMap(Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap)
    {
        List<SectorVendorOfferingTO> sectorBasedBenefitSequenceTOList = null;
        SectorVendorOfferingTO sectorVendorOfferingTO = null;
        List<VendorOfferingTemplateTO> offeringTemplateTOList = null;
        String vendorOfferingCompositeName = null;
        String vendorCompositeName = null;
        try {
            sectorBasedBenefitSequenceTOList = new ArrayList<SectorVendorOfferingTO>();
            if (sectorVendorOfferingmap != null && !sectorVendorOfferingmap.isEmpty()) {
                for (Map.Entry<String, List<VendorOfferingTemplateTO>> entry : sectorVendorOfferingmap.entrySet()) {
                    sectorVendorOfferingTO = new SectorVendorOfferingTO();
                    sectorVendorOfferingTO.setSectorID(Integer.parseInt(entry.getKey().split(CoreConstants.AT_SPERATOR)[0]));
                    sectorVendorOfferingTO.setSectorLabel(entry.getKey().split(CoreConstants.AT_SPERATOR)[1]);
                    offeringTemplateTOList = entry.getValue();
                    if (offeringTemplateTOList != null && !offeringTemplateTOList.isEmpty()) {
                        vendorCompositeName = null;
                        vendorOfferingCompositeName = null;
                        for (VendorOfferingTemplateTO templateTO : offeringTemplateTOList) {
                            if (vendorCompositeName == null && vendorOfferingCompositeName == null) {
                                vendorCompositeName = templateTO.getVendorName();
                                vendorOfferingCompositeName = templateTO.getOfferTemplateName();
                            } else {
                                vendorCompositeName = vendorCompositeName + CoreConstants.COMA_SPERATOR + templateTO.getVendorName();
                                vendorOfferingCompositeName = vendorOfferingCompositeName + CoreConstants.COMA_SPERATOR + templateTO.getOfferTemplateName();
                            }
                        }
                        sectorVendorOfferingTO.setVendorOfferingCompositeName(vendorOfferingCompositeName);
                        sectorVendorOfferingTO.setVendorCompositeName(vendorCompositeName);
                    }
                    sectorBasedBenefitSequenceTOList.add(sectorVendorOfferingTO);
                }
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return sectorBasedBenefitSequenceTOList;

    }
    
    /**
     * redirecting to More Info Vendor
     * 
     * */
    public void redirectToVendorMoreInfo(BenefitVendorTO sysBenefitVendorTO)
    {
        try {
            sectorVendorOfferingBean.setSysBenefitVendorTO(sysBenefitVendorTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * redirecting to More Info VendorOffering
     * 
     * */
    public void redirectToVendoringOfferMoreInfo(VendorOfferingTemplateTO offeringTemplateTO)
    {
        try {
            sectorVendorOfferingBean.setVendorOfferingTemplateTO(offeringTemplateTO);
            if (offeringTemplateTO.getPeakRecurringDetailTO() != null) {
                sectorVendorOfferingBean.setRenderVendorOffering(true);
            } else {
                sectorVendorOfferingBean.setRenderVendorOffering(false);
            }
            sectorVendorOfferingBean.setPeakRecurringDetailTO(offeringTemplateTO.getPeakRecurringDetailTO());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
    

    /**
     * redirecting to Edit OfferingSeqencePage
     * 
     * */
    
    public void editSectorVendoringSequence(SectorVendorOfferingTO sectorVendorOfferingTO) throws Exception
    {
        Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap = null;
        List<VendorOfferingTemplateTO> systemOfferingTemplateTOList=null;
        List<VendorOfferingTemplateTO> existingOfferingTemplateTOList=null;
        List<VendorOfferingTemplateTO> newOfferingTemplateTOList=null;
        try {
            menuBean.setPage(IBenefitsConstant.EDIT_OFFERING_SEQUENCE_PAGE);
            sectorVendorOfferingmap = sectorVendorOfferingBean.getSectorVendorOfferingmap();
            systemOfferingTemplateTOList = sysVendorOfferingTemplateService.getOfferingTemplateList();
            existingOfferingTemplateTOList= sectorVendorOfferingmap.get(sectorVendorOfferingTO.getSectorID()+ CoreConstants.AT_SPERATOR+ sectorVendorOfferingTO.getSectorLabel());
            newOfferingTemplateTOList= processVendorOfferingTOList(cloneVendorOfferingList(existingOfferingTemplateTOList),systemOfferingTemplateTOList);
            sectorVendorOfferingBean.setSelectedVendorOfferingTemplateTOList(newOfferingTemplateTOList);
            sectorVendorOfferingBean.setSelectedSectorName(sectorVendorOfferingTO.getSectorLabel());
            sectorVendorOfferingBean.setSelectedSectorID(sectorVendorOfferingTO.getSectorID());
            } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
    /**
     * 
     * @param existingOfferingTemplateTOList
     * @param systemOfferingTemplateTOList
     * @return
     * @throws Exception
     */
    private List<VendorOfferingTemplateTO> processVendorOfferingTOList(List<VendorOfferingTemplateTO> existingOfferingTemplateTOList,
            List<VendorOfferingTemplateTO> systemOfferingTemplateTOList) throws Exception
    {
        List<VendorOfferingTemplateTO> newOfferingTemplateTOList=null;
        boolean newOffer=false;
        try{
            newOfferingTemplateTOList=existingOfferingTemplateTOList;
            for(VendorOfferingTemplateTO systemOffering:systemOfferingTemplateTOList){
                newOffer=true;
                for(VendorOfferingTemplateTO sectorOffering:existingOfferingTemplateTOList)
                {
                    if(sectorOffering.getVendorOfferingID().equals(systemOffering.getVendorOfferingID())){
                        newOffer=false;
                        break;
                    }
                    
                }
                if(newOffer){
                    newOfferingTemplateTOList.add(systemOffering);
                }
            }
            
        }catch (Exception e) {
           throw e;
        }
        return newOfferingTemplateTOList;
    }
    /**
     * redirecting to More Info OfferingSeqencePage
     * 
     * */
    public void redirectToSectorVendoringSequenceMoreInfo(SectorVendorOfferingTO sectorVendorOfferingTO)
    {
        Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap = null;
        try {
            menuBean.setPage(IBenefitsConstant.MORE_INFO_OFFERING_SEQUENCE_PAGE);
            sectorVendorOfferingmap = sectorVendorOfferingBean.getSectorVendorOfferingmap();
            sectorVendorOfferingBean.setSelectedVendorOfferingTemplateTOList(sectorVendorOfferingmap.get(sectorVendorOfferingTO.getSectorID()+ CoreConstants.AT_SPERATOR+ sectorVendorOfferingTO.getSectorLabel()));
            sectorVendorOfferingBean.setSelectedSectorID(sectorVendorOfferingTO.getSectorID());
            sectorVendorOfferingBean.setSelectedSectorName(sectorVendorOfferingTO.getSectorLabel());
            sectorVendorOfferingBean.setSelectedSectorBasedBenefitTO(sectorVendorOfferingTO);

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
    * redirecting to OfferingSeqenceHomePage
    * 
    * */
    public void redirectToHome()
    {
        menuBean.setPage(IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_HOME);
        sectorVendorOfferingBean.setSelectedSector(null);

    }

    public void renderVendorOfferingPanel(){
        sectorVendorOfferingBean.setRenderVendorOffering(true);
    }
    
    /**
     * redirecting to AddOfferingSeqencePage
     * 
     * */
    public void redirectToAddOfferingSeqencePage()
    {
        List<SysSectorTO> sysSectorTOList = null;
        List<VendorOfferingTemplateTO>  vendorOfferingTemplateTOList=null;
        Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap=null;
        try {
            sectorVendorOfferingmap=sectorVendorOfferingBean.getSectorVendorOfferingmap();
            menuBean.setPage(IBenefitsConstant.OFFERING_SEQUENCE_PAGE);
            sectorVendorOfferingBean.setRenderVendorOffering(false);
            vendorOfferingTemplateTOList = sysVendorOfferingTemplateService.getOfferingTemplateList();
                sectorVendorOfferingBean.setVendorOfferingTemplateTOList(vendorOfferingTemplateTOList);
                sysSectorTOList = sectorVendorOfferingBean.getSectorTOList();
            if (sysSectorTOList != null && !sysSectorTOList.isEmpty()) {
            } else {
                sysSectorTOList = sectorService.getSysSectorList();
                sectorVendorOfferingBean.setSectorTOList(sysSectorTOList);
            }
            
            if (sectorVendorOfferingmap != null && !sectorVendorOfferingmap.isEmpty()) {
                for (Iterator<SysSectorTO> sector = sysSectorTOList.iterator(); sector.hasNext();) {
                    SysSectorTO sectorTO = sector.next();
                    for (Map.Entry<String, List<VendorOfferingTemplateTO>> entry : sectorVendorOfferingmap.entrySet()) {
                        if (sectorTO.getSectorID().equals(Integer.parseInt(entry.getKey().split(CoreConstants.AT_SPERATOR)[0]))) {
                            sector.remove();
                            break;
                        }
                    }

                }
            }
            sectorVendorOfferingBean.setSectorTOList(sysSectorTOList);


        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

/**
 * 
 */
    public void saveSectorVendorOfferSequence(boolean edit)
    {
        Integer[] selectedSectors= null;
        BenefitRequestTO benefitRequestTO = null;
        String sequenceValue = null;
        String[] sectorVendorOfferSequenceValue = null;
        List<String> sequenceValueList = null;
        Map<Integer, Long> sectorVendorSequenceMap = null;
        try {
            selectedSectors = sectorVendorOfferingBean.getSelectedSector();
         if(!edit){
            if ((selectedSectors == null) || (selectedSectors.length == 0)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.NO_SECTOR_SELECTED, ""));
                RequestContext.getCurrentInstance().execute("scrollUp()");
               return;
            }
         }
            benefitRequestTO = new BenefitRequestTO();
            sequenceValueList = new ArrayList<String>();
            sequenceValue = sectorVendorOfferingBean.getSequenceValue();
            sectorVendorOfferSequenceValue = sequenceValue.split(",");
            for (String sequence : sectorVendorOfferSequenceValue) {
                sequenceValueList.add(sequence);
            }
            sectorVendorSequenceMap = fetchSequenceMapFromValue(sequenceValueList);
            benefitRequestTO.setSelectedSectors(selectedSectors);
            benefitRequestTO.setSectorVendorOfferSequenceMap(sectorVendorSequenceMap);
            benefitRequestTO.setEdit(edit);
            if(!edit){
            benefitRequestTO.setVendorOfferingTemplateTOList(sectorVendorOfferingBean.getVendorOfferingTemplateTOList());
            }else{
                benefitRequestTO.setSectorId(sectorVendorOfferingBean.getSelectedSectorID());
                benefitRequestTO.setVendorOfferingTemplateTOList(sectorVendorOfferingBean.getSelectedVendorOfferingTemplateTOList());
            }
            sectorVendorOfferingSequenceService.saveSectorVendorOfferSequence(benefitRequestTO);
            if(!edit){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_SAVE_SUCCESS, ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_UPDATE_SUCCESS, ""));
            }
            initialize();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            RequestContext.getCurrentInstance().execute("scrollUp()");
            if(!edit){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_SAVE_ERROR, ""));
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.SECTOR_BASED_OFFERING_SEQUENCE_UPDATE_ERROR, ""));
            }
                
        }
    }

    /**
     * 
     * @param sequenceValueList
     * @return
     */
    private Map<Integer, Long> fetchSequenceMapFromValue(List<String> sequenceValueList)
    {
        String[] sectorOfferSequenceValue = null;
        Map<Integer, Long> sequenceMap = null;
        String id = null;
        String value = null;
        try {
            sequenceMap = new LinkedHashMap<Integer, Long>();
            for (String s : sequenceValueList) {
                sectorOfferSequenceValue = s.split("_");
                if (sectorOfferSequenceValue != null) {
                    id = sectorOfferSequenceValue[0];
                    value = sectorOfferSequenceValue[1];
                    sequenceMap.put(Integer.parseInt(id), Long.parseLong(value));

                }
            }

            return sequenceMap;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    /**
     * 
     * @param offeringCodeTO
     */
    public void redirectToOfferCodeMoreInfo(OfferingCodeTO offeringCodeTO)
    {
        if (offeringCodeTO.isMaximumLimitApplicable()) {
            sectorVendorOfferingBean.setOfferingCodeMaximumLimitTO(offeringCodeTO.getCodeMaximumLimitTO());
            sectorVendorOfferingBean.setRenderMaximumLimitApplicableDetail(true);
        } else {
            sectorVendorOfferingBean.setRenderMaximumLimitApplicableDetail(false);
        }
        sectorVendorOfferingBean.setOfferingCodeTO(offeringCodeTO);
    }
    
    /**
     * 
     * @param offeringTemplateTOList
     * @return
     */
    private List<VendorOfferingTemplateTO> cloneVendorOfferingList(List<VendorOfferingTemplateTO> offeringTemplateTOList) throws CloneNotSupportedException
    {
        List<VendorOfferingTemplateTO> cloneOfferingList = new ArrayList<VendorOfferingTemplateTO>();
        try {
            if (cloneOfferingList != null) {
                for (VendorOfferingTemplateTO offer : offeringTemplateTOList) {
                    cloneOfferingList.add((VendorOfferingTemplateTO) offer.clone());

                }
            }
        } catch (CloneNotSupportedException cne) {
            LOGGER_.error("", cne);
            throw cne;
        }
        return cloneOfferingList;
    }

}
