package com.talentpact.ui.altbenefits.constant;

public interface IBenefitsConstant
{
    public static final String OFFERING_SEQUENCE_PAGE = "../altbenefits/addSectorBasedOfferingSequence.xhtml";
    public static final String HR_OFFERING_SEQUENCE_PAGE = "../altbenefits/addHrBasedOfferingSequence.xhtml";

    public static final String SECTOR_BASED_OFFERING_SEQUENCE_HOME = "../altbenefits/benefitSectorVendorSequence.xhtml";
    public static final String HR_BASED_OFFERING_SEQUENCE_HOME = "../altbenefits/benefitHrVendorSequence.xhtml";

    public static final String SECTOR_BASED_OFFERING_SEQUENCE_SAVE_SUCCESS = "Sector Vendor Offer sequence saved successfully.";
    public static final String SECTOR_BASED_OFFERING_SEQUENCE_UPDATE_SUCCESS = "Sector Vendor Offer sequence updated successfully.";
    public static final String SECTOR_BASED_OFFERING_SEQUENCE_SAVE_ERROR = "Unexpected error occured while saving vendor offer sequence.";
    public static final String SECTOR_BASED_OFFERING_SEQUENCE_UPDATE_ERROR = "Unexpected error occured while updating vendor offer sequence.";
    public static final String NO_ORG_SELECTED = "No Organization Selected. Please select organization first.";
    public static final String NO_SECTOR_SELECTED = "No Sector Selected. Please select atleast one sector.";
    public static final String MORE_INFO_HR_OFFERING_SEQUENCE_PAGE = "../altbenefits/moreInfoHrBasedOfferingSequence.xhtml";
    public static final String EDIT_OFFERING__HR_SEQUENCE_PAGE = "../altbenefits/editHrBasedOfferingSequence.xhtml";
    public static final String MORE_INFO_OFFERING_SEQUENCE_PAGE = "../altbenefits/moreInfoSectorBasedOfferingSequence.xhtml";
    public static final String EDIT_OFFERING_SEQUENCE_PAGE = "../altbenefits/editSectorBasedOfferingSequence.xhtml";
    public static final String NO_ORG_VENDOR_OFFER_GIVEN = "No Vendor Offering Found Form this organization.";
    public static final String ORG_BASED_OFFERING_SEQUENCE_SAVE_SUCCESS = "Organization based Vendor Offer sequence saved successfully.";
    public static final String ORG_BASED_OFFERING_SEQUENCE_UPDATE_SUCCESS = "Organization based Vendor Offer sequence updated successfully.";
    public static final String ORG_BASED_OFFERING_SEQUENCE_SAVE_ERROR = "Unexpected error occured while saving organization based vendor offer sequence.";
    public static final String ORG_BASED_OFFERING_SEQUENCE_UPDATE_ERROR = "Unexpected error occured while updating organization based vendor offer sequence.";
    public static final String OFFER_CODE_ACCESS_TYPE_STRING = "AccessType";
    public static final String OFFER_CODE_USAGE_DURATION_TYPE_STRING = "UsageDurationType";
    public static final String OFFER_CODE_PEAKRECURRING_TYPE_STRING = "PeakRecurringType";
    public static final String VENDOR_LOGO_PATH = "vendor_logo_upload_path";
    public static final String VENDOR_LOGO_SERVER_PATH = "vendor_logo_upload_server_path";
    public static final String BANNER_PATH = "banner_upload_file_path";
    public static final String BANNER_SERVER_PATH = "banner_upload_server_path";
    public static final String UPLOAD_INVALID_FILE_MSG="Invalid file. File format is not correct.";
    
/**
 *     
 *     
 *     
 *     
 */public static final String OFFERCODE_UPLOAD_PAGE_COLUMNCOUNT = "9";
 public static final String DATE_FORMAT = "MM/dd/yyyy";
    public static String FILE_SEPERATOR = "\\";
    public static String UPLOAD_STATUS = "Upload Status Summary";
    public static final String CONTENT_PATH_NAME="offercode_content_path";
    public static final String MASTER_OFFERCODE_UPLOAD_SUCCESS_MSG="Offercode data uploaded successfully.";
    public static final String MASTER_OFFERCODE_SAVE_MASTER_DATA_ERROR_MSG="Unexpected error occured while uploading Offercodes.";
    
    public static final String MASTER_OFFERCODE_UPLOAD_CONSTRAINTS_VOILATION_MSG="Constraints voilation occured. Masterdata upload rollback.";
    public static final String MASTER_OFFERCODE_UPLOAD_EMPTY_FILE_MSG="No File selected for upload.";
    public static final String MASTER_OFFERCODE_UPLOAD_PAGE="../altbenefits/uploadOfferCodes.xhtml";
    public static final String OFFERCODE_UPLOAD_PAGE_INVALID_HEADER="Invalid Header";
    public static final String OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN=" is missing, ";
    public static final String OFFERCODE_DUBLICATE_MESSAGE =" is already exist for this Vendor offering.";
    public static final String OFFERCODE_DUBLICATE_MESSAGE2 ="Dublicate Offercode entry for this offer code in excel.";
    public static final String OFFERCODE_UPLOAD_PAGE_VALID_RECORD="Valid Record";
    public static final String OFFERCODE_UPLOAD_PAGE_INVALID_RECORD="Invalid Record";
    public static final String MASTER_OFFERCODE_UPLOAD_INVALID_FILE_MSG="Invalid file. File format is not correct.";
    public static final String MASTER_OFFERCODE_UPLOAD_EXCESS_COLUMN_COUNT_MSG="File format is not correct. Column count exceeded the maximun column allowed";
    public static final String MASTER_OFFERCODE_UPLOAD_LESS_COLUMN_COUNT_MSG="File format is not correct. Some columns are missing";
    public static final String MASTER_OFFERCODE_UPLOAD_EMPTY_SHEET="Empty sheet uploaded. No data found in excel sheet.";

    
}
