package com.talentpact.ui.helper;

import java.util.Comparator;

import com.talentpact.business.application.transport.output.SysOfferingTypeTO;


public class SysOfferingCategoryComparator implements Comparator<SysOfferingTypeTO>
{
    public int compare(SysOfferingTypeTO e1, SysOfferingTypeTO e2)
    {
        return e1.getOfferingCategory().compareTo(e2.getOfferingCategory());
    }
}