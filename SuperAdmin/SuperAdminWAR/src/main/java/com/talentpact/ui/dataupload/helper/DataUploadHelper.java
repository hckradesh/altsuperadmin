/**
 * 
 */
package com.talentpact.ui.dataupload.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.alt.common.to.SelectItemTO;
import com.talentpact.ui.dataupload.constants.MessageLevel;


/**
 * @author shivam.kumar
 *
 */
public class DataUploadHelper implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 607025607332377755L;

    public List<SelectItem> getStaticSelectItem()
        throws Exception
    {
        List<SelectItem> dummyList = null;
        SelectItem selectItem = null;
        try {
            dummyList = new ArrayList<SelectItem>();
            selectItem = new SelectItem();
            selectItem.setEscape(true);
            selectItem.setLabel("Please Select");
            selectItem.setValue(0);
            dummyList.add(selectItem);
            return dummyList;
        } catch (Exception e) {
            throw new Exception();
        } finally {
        }
    }

    public List<SelectItem> getSelectItemIDList(List<SelectItemTO> selectItemTOList)
        throws Exception
    {
        List<SelectItem> selectItemList = null;
        SelectItem selectItem = null;
        try {
            selectItemList = new ArrayList<SelectItem>();
            selectItemList.addAll(getStaticSelectItem());
            if (selectItemTOList == null) {
                return selectItemList;
            }
            for (SelectItemTO selectItemTO : selectItemTOList) {
                selectItem = new SelectItem();
                selectItem.setEscape(true);
                selectItem.setLabel(selectItemTO.getItemlabel());
                selectItem.setValue(selectItemTO.getItemID());
                selectItemList.add(selectItem);
            }
            return selectItemList;
        } catch (Exception e) {
            throw new Exception();
        } finally {
        }
    }

    public static void showMessage(MessageLevel messageLevel, String StrongMsg, String msgDetail)
    {
        Severity severity = null;
        if (messageLevel.equals(MessageLevel.ERROR)) {
            severity = FacesMessage.SEVERITY_ERROR;
        } else if (messageLevel.equals(MessageLevel.FATAL)) {
            severity = FacesMessage.SEVERITY_FATAL;
        } else if (messageLevel.equals(MessageLevel.INFO)) {
            severity = FacesMessage.SEVERITY_INFO;
        } else if (messageLevel.equals(MessageLevel.WARN)) {
            severity = FacesMessage.SEVERITY_WARN;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, StrongMsg, msgDetail));
    }

}
