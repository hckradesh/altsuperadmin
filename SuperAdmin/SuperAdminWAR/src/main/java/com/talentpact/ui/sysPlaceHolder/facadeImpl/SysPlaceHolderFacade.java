/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;


import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysPlaceHolder.dataprovider.SysPlaceHolderDataProvider;
import com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPlaceHolderFacade implements SysPlaceHolderLocalFacade{

	@EJB
	private SysPlaceHolderDataProvider sysPlaceHolderDataProvider;


	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade#getAllPlaceHolderData()
	 */
	@Override
	public ServiceResponse getAllPlaceHolderData() 
			throws Exception {
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataProvider.getAllPlaceHolderData();
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}finally{

		}
		return serviceResponse;
	}


	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade#insertNewPlaceHolder(com.talentpact.business.common.service.input.ServiceRequest)
	 */
	
	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse insertNewPlaceHolder(ServiceRequest serviceRequest)
			throws Exception {
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataProvider.insertNewPlaceHolder(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}


	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade#deletePlaceHolderByID(com.talentpact.business.common.service.input.ServiceRequest)
	 */
	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse deletePlaceHolderByID(ServiceRequest serviceRequest)
			throws Exception {
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataProvider.deletePlaceHolderByID(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
		}finally{
			
		}
		
		return serviceResponse;
	}


	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade#updateNewPlaceHolder(com.talentpact.business.common.service.input.ServiceRequest)
	 */
	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse updateNewPlaceHolder(ServiceRequest serviceRequest)
			throws Exception {
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataProvider.updateNewPlaceHolder(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}finally{
			
		}
		
		return serviceResponse;
	}

}
