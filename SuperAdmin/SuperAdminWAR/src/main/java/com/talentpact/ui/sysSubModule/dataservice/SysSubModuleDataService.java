package com.talentpact.ui.sysSubModule.dataservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.talentpact.business.application.transport.output.SysSubModuleTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.model.SysSubModule;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.sysSubModule.constants.ISysSubModuleConstants;
import com.talentpact.ui.sysSubModule.dataprovider.SysSubModuleDataProvider;
import com.talentpact.ui.sysSubModule.dataprovider.DS.SysSubModuleDS;
import com.talentpact.ui.sysSubModule.dataprovider.converter.SysSubModuleConverter;
import com.talentpact.ui.sysSubModule.exception.SysSubModuleException;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysSubModuleDataService {

    private static final Logger LOGGER = Logger.getLogger(SysSubModuleDataService.class.toString());

    @EJB
    private SysSubModuleDataProvider sysSubModuleDataProvider;

    @EJB
    SysSubModuleDS sysSubModuleDS;

    /**
     * @param serviceRequest
     * @return
     */
    public ServiceResponse getParentModuleAndSubModules(ServiceRequest serviceRequest)
        throws Exception {
        return sysSubModuleDataProvider.getParentModuleAndSubModules(serviceRequest);
    }

    /**
     * 
     * @param serviceRequest
     * @return
     * @author raghvendra.mishra
     */
    public ServiceResponse getAllSysSubModuleList(ServiceRequest serviceRequest)
        throws SysSubModuleException {
        ServiceResponse serviceResponse = null;
        List<SysSubModuleResponseTO> sysSubModuleResponseTOList = null;
        List<SysSubModule> sysSubModuleList = null;
        SysSubModuleConverter sysSubModuleConverter = null;
        try {
            serviceResponse = new ServiceResponse();
            sysSubModuleConverter = new SysSubModuleConverter();
            sysSubModuleList = sysSubModuleDS.getAllSysSubModules();
            sysSubModuleResponseTOList = sysSubModuleConverter.convertSysSubModuleToDataTO(sysSubModuleList);
            serviceResponse.setResponseTransferObjectObjectList(sysSubModuleResponseTOList);
        } catch (Exception ex) {
            LOGGER.error("", ex);
            throw new SysSubModuleException(ISysSubModuleConstants.SYS_SUB_MODULE_RETRIEVE_ERROR_MSG);
        }
        return serviceResponse;
    }

    public List<SysSubModuleTO> getAllSysSubModuleTOList() {
        SysSubModuleTO sysSubModuleTO = null;
        List<SysSubModuleTO> sysSubModuleTOList = new ArrayList<SysSubModuleTO>();
        try {
            for (SysSubModule sysSubModule : sysSubModuleDS.getAllSysSubModules()) {
                sysSubModuleTO = new SysSubModuleTO();
                sysSubModuleTO.setModuleId(sysSubModule.getModuleId());
                sysSubModuleTO.setModuleName(sysSubModule.getModuleName());
                sysSubModuleTOList.add(sysSubModuleTO);
            }
        } catch (Exception e) {
        }
        return sysSubModuleTOList;
    }
}
