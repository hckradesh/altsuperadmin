/**
 * 
 */
package com.talentpact.ui.sysEntityColumn.dataservice.DS;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysEntity;
import com.talentpact.model.SysEntityColumn;
import com.talentpact.ui.sysEntityColumn.common.transport.input.SysEntityColumnRequestTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.constants.ISysEntityColumnConstants;
import com.talentpact.ui.sysEntityColumn.converter.SysEntityColumnConverter;
import com.talentpact.ui.sysEntityColumn.exception.SysEntityColumnException;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
public class SysEntityColumnDS extends AbstractDS<SysEntityColumn> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysEntityColumnDS.class);
	
	public SysEntityColumnDS(){
		super(SysEntityColumn.class);
	}
	
	/**
	 * {@link #getSysEntityColumnsByEntityID(List)}
	 * @return List<SysEntityColumnResponseTO>
	 * @param List<Integer> sysEntityIDList
	 * @throws Exception
	 * @author vaibhav.kashyap	
	 * */
	public List<SysEntityColumnResponseTO> getSysEntityColumnsByEntityID(
			List<Integer> sysEntityIDList) throws Exception {
		List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
		List<SysEntityColumn> sysEntityColumnEnityList = null;
		SysEntityColumnResponseTO sysEntityColumnResponseTO = null; 
		StringBuilder hqlQuery = null;
		Query query = null;
		try{
			hqlQuery = new StringBuilder();
			hqlQuery.append("from SysEntityColumn sec where sec.sysEntity.entityId in (:entityList)");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			query.setParameter("entityList", sysEntityIDList);
			sysEntityColumnEnityList = query.getResultList();
			if(sysEntityColumnEnityList!=null && 
					!sysEntityColumnEnityList.isEmpty()){
				sysEntityColumnResponseTOList = new ArrayList<SysEntityColumnResponseTO>();
				for(SysEntityColumn secObj : sysEntityColumnEnityList){
					sysEntityColumnResponseTO = 
							SysEntityColumnConverter.getSysEntityColumnResponseTOByEntity(secObj);
					if(sysEntityColumnResponseTO!=null)
						sysEntityColumnResponseTOList.add(sysEntityColumnResponseTO);					
				}
			}
		}catch(Exception ex){
			sysEntityColumnResponseTOList = null;
			throw ex;
		}
		return sysEntityColumnResponseTOList;
	}
	
	/**
	 * method to save SysEntityColumn
	 * @param sysEntityColumnRequestTO
	 * @return
	 * @throws SysEntityColumnException
	 * @author raghvendra.mishra
	 */
	public SysEntityColumn saveOrUpdateSysEntityColumn(SysEntityColumnRequestTO sysEntityColumnRequestTO) throws SysEntityColumnException{
		long entityColumnID = 0;
		SysEntityColumn sysEntityColumn = null;
		boolean saveUpdateFlag = false;
		try{
			if(sysEntityColumnRequestTO.getEntityColumnID() != null){
				entityColumnID = sysEntityColumnRequestTO.getEntityColumnID();
				sysEntityColumn = findSysEntityColumn(entityColumnID);
			}
			if(sysEntityColumn != null){
				sysEntityColumn.setEntityColumnName(sysEntityColumnRequestTO.getEntityColumnName());
				sysEntityColumn.setLabel(sysEntityColumnRequestTO.getLabel());
				sysEntityColumn.setDescription(sysEntityColumnRequestTO.getDescription());
				sysEntityColumn.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
				sysEntityColumn.setModifiedBy(sysEntityColumnRequestTO.getModifiedBy());
				sysEntityColumn.setEntityColumnType(sysEntityColumnRequestTO.getEntityColumnType());
				getEntityManager("TalentPact").merge(sysEntityColumn);
			}else{
				saveUpdateFlag = true;
				sysEntityColumn = new SysEntityColumn();
				sysEntityColumn.setSysEntityID(sysEntityColumnRequestTO.getSysEntityId());
				sysEntityColumn.setEntityColumnName(sysEntityColumnRequestTO.getEntityColumnName());
				sysEntityColumn.setdBColumnName(sysEntityColumnRequestTO.getDbColumnName());
				sysEntityColumn.setLabel(sysEntityColumnRequestTO.getLabel());
				sysEntityColumn.setDescription(sysEntityColumnRequestTO.getDescription());
				sysEntityColumn.setTenantId(sysEntityColumnRequestTO.getTenantID());
				sysEntityColumn.setCreatedDate(SQLDateHelper.getSqlTimeStamp());
				sysEntityColumn.setModifiedDate(SQLDateHelper.getSqlTimeStamp());
				sysEntityColumn.setCreatedBy(sysEntityColumnRequestTO.getCreatedBy());
				sysEntityColumn.setModifiedBy(sysEntityColumnRequestTO.getModifiedBy());
				sysEntityColumn.setEntityColumnType(sysEntityColumnRequestTO.getEntityColumnType());
				sysEntityColumn.setDbColumnType(sysEntityColumnRequestTO.getDbColumnType());
				getEntityManager("TalentPact").persist(sysEntityColumn);
			}
			return sysEntityColumn;
		}catch(Exception e){
			LOGGER.error("",e);
			if(saveUpdateFlag == true){
				throw new SysEntityColumnException(ISysEntityColumnConstants.SYSENTITYCOLUMN_SAVE_ERROR_MSG, e);
			}else{
				throw new SysEntityColumnException(ISysEntityColumnConstants.SYSENTITYCOLUMN_UPDATE_ERROR_MSG, e);
			}
		}
	}
	
	/**
	 * 
	 * @param entityColumnId
	 * @return
	 * @throws SysEntityColumnException
	 * @author raghvendra.mishra
	 */
	public SysEntityColumn findSysEntityColumn(long entityColumnID) throws SysEntityColumnException{
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysEntityColumn> sysEntityColumnList = null;
		SysEntityColumn result = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select sysEntityColumn from SysEntityColumn sysEntityColumn where sysEntityColumn.entityColumnID=:entityColumnID");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			query.setParameter("entityColumnID", entityColumnID);
			sysEntityColumnList = query.getResultList();
			if (sysEntityColumnList != null && sysEntityColumnList.size() > 0) {
				result = sysEntityColumnList.get(0);
			}
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityColumnException(ISysEntityColumnConstants.SYSENTITYCOLUMN_RETRIEVE_ERROR_MSG, e);
		}
		return result;
	}
	/**
	 * 
	 * @param sysEntity
	 * @return
	 * @throws SysEntityColumnException
	 * @author raghvendra.mishra
	 */
	public List<SysEntityColumn> getSysEntityColumnsByEntityID(SysEntity sysEntity) throws SysEntityColumnException{
		Query query = null;
		StringBuilder hqlQuery = null;
		List<SysEntityColumn> sysEntityColumnList = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("select sysEntityColumn from SysEntityColumn sysEntityColumn where sysEntityColumn.sysEntity=:sysEntity");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			query.setParameter("sysEntity", sysEntity);
			sysEntityColumnList = query.getResultList();
		} catch (Exception e) {
			LOGGER.error("",e);
			throw new SysEntityColumnException(ISysEntityColumnConstants.SYSENTITYCOLUMN_LIST_RETRIEVE_ERROR_MSG, e);
		}
		return sysEntityColumnList;
	}

}
