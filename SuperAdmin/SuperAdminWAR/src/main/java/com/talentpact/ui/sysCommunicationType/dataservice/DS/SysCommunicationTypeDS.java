/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.dataservice.DS;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysCommunicationType;
import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysCommunicationType.converter.SysCommunicationTypeConverter;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCommunicationTypeDS extends AbstractDS<SysCommunicationType>{

	public SysCommunicationTypeDS(){
		super(SysCommunicationType.class);
	}

	/**
	 * {@link #getAllCommType()} : fetches all communicationType
	 * @return List<SysCommunicationTypeResponseTO>
	 * @throws Exeption
	 * @author vaibhav.kashyap
	 * */
	public List<SysCommunicationTypeResponseTO> getAllCommType()
			throws Exception{
		List<SysCommunicationTypeResponseTO> sysCommunicationTypeResponseTOList 
		= null;
		List<SysCommunicationType> sysCommunicationTypeEntityList = null;
		SysCommunicationTypeResponseTO sysCommunicationTypeResponseTOObj = null;
		StringBuilder hqlQuery = null;
		Query query = null;
		try{
			hqlQuery = new StringBuilder();
			hqlQuery.append("from SysCommunicationType");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			sysCommunicationTypeEntityList = query.getResultList();
			if(sysCommunicationTypeEntityList!=null && 
					!sysCommunicationTypeEntityList.isEmpty()){
				sysCommunicationTypeResponseTOList = new  LinkedList<SysCommunicationTypeResponseTO>();
				for(SysCommunicationType commTypeObj : sysCommunicationTypeEntityList){
					sysCommunicationTypeResponseTOObj = SysCommunicationTypeConverter.getCommTypeResponseTOFromEntity(commTypeObj);
					if(sysCommunicationTypeResponseTOObj!=null){
						sysCommunicationTypeResponseTOList.add(sysCommunicationTypeResponseTOObj);
					}
				}
			}
		}catch(Exception ex){
			sysCommunicationTypeResponseTOList = null;
			throw ex;
		}
		
		return sysCommunicationTypeResponseTOList;
	}
}
