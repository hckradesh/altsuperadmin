/**
 * 
 */
package com.talentpact.ui.sysContentType.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.common.constants.ISysContentCategoryConstants;
import com.talentpact.business.common.constants.ISysContentTypeConstants;
import com.talentpact.business.dataservice.SysContentType.SysContentTypeService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.helper.SysContentCategoryComparator;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysContentType.bean.SysContentTypeBean;

/**
 * 
 * @author vivek.goyal
 *
 */

@Named("sysContentTypeController")
@ConversationScoped
public class SysContentTypeController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysContentTypeController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysContentTypeService sysContentTypeService;

    @Inject
    SysContentTypeBean sysContentTypeBean;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        List<SysContentTypeTO> sysContentTypeTOList = null;
        ServiceResponse response = null;
        Set<String> name = null;
        SysContentTypeTO sysContentTypeTO = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            resetSysContentType();
            sysContentTypeTOList = sysContentTypeService.getSysContentTypeList();
            sysContentTypeBean.setSysContentTypeTOList(sysContentTypeTOList);
            sysContentTypeBean.getSysContentTypeTOList();
            name = new HashSet<String>();
            for (SysContentTypeTO list : sysContentTypeTOList) {
                sysContentTypeTO = new SysContentTypeTO();
                sysContentTypeTO = list;
                name.add(sysContentTypeTO.getCategoryName());
            }
            sysContentTypeBean.setContentNameList(name);
            sysContentTypeBean.setRenderSysContentCategoryPopup(false);
            sysContentTypeBean.setContentCategoryID(-1);
            sysContentTypeBean.setSysType(null);
            sysContentTypeBean.setRenderErrorMessage(false);
            menuBean.setPage("../SysContentType/sysContentType.xhtml");
            requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_DATATABLE_RESET);
            requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysContentType()
    {
        //sysContentTypeBean.setSysContentTypeTOList(null);
        sysContentTypeBean.setRenderSysContentCategoryPopup(true);
        sysContentTypeBean.setContentCategoryID(-1);
        sysContentTypeBean.setSysType(null);
        sysContentTypeBean.setContentNameList(null);
        sysContentTypeBean.setSysContentTypeTOList(null);
        sysContentTypeBean.setSelectedContentNameList(null);
        getNewSysContentCategory();
    }

    public void getNewSysContentCategory()
    {
        List<SysContentTypeTO> sysContentCategoryList = null;
        try {
            sysContentCategoryList = sysContentTypeService.getNewSysContentCategoryList();
            sysContentTypeBean.setNewContentCategoryTOList(sysContentCategoryList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSysContentType()
    {
        SysContentTypeTO sysContentTypeTO = null;
        RequestContext requestContext = null;
        String category = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysContentTypeTO = new SysContentTypeTO();
            sysContentTypeBean.setRenderErrorMessage(true);
            sysContentTypeTO.setNewContentCategoryTOList(sysContentTypeBean.getNewContentCategoryTOList());
            sysContentTypeTO.setContentCategoryID(sysContentTypeBean.getContentCategoryID());
            sysContentTypeTO.setCategoryName(sysContentTypeBean.getCategoryName());
            sysContentTypeTO.setSysType(sysContentTypeBean.getSysType());
            for (int i = 0; i < sysContentTypeBean.getNewContentCategoryTOList().size(); i++) {
                if (sysContentTypeBean.getNewContentCategoryTOList().get(i).getContentCategoryID() == sysContentTypeBean.getContentCategoryID()) {
                    category = sysContentTypeBean.getNewContentCategoryTOList().get(i).getCategoryName();
                    System.out.println(category);
                }
            }
            if (validateSysContentType(sysContentTypeTO)) {
                sysContentTypeService.addSysContentType(sysContentTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Type saved successfully", ""));
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_ADD_DIALOG_HIDE);
                requestContext.update("sysContentTypeListForm");
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_DATATABLE_RESET);
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_PAGINATION_DATATABLE_RESET);
            } else {
                RequestContext.getCurrentInstance().update("addSysContentTypeForm");
            }
        } catch (Exception e) {
            e.printStackTrace();
            sysContentTypeBean.setRenderSysContentCategoryPopup(false);
            RequestContext.getCurrentInstance().update("addSysContentTypeForm");
        }
    }

    //To validate if the newly added SysContentType is already present in the system
    public boolean validateSysContentType(SysContentTypeTO sysContentTypeTO)
    {
        List<SysContentTypeTO> sysContentTypeTOList = null;
        int j = 0;
        try {
            if (sysContentTypeTO.getSysType().trim().equalsIgnoreCase("") || sysContentTypeTO.getSysType().trim().equalsIgnoreCase(null)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysType", ""));
                j = 1;
            } else {
                sysContentTypeTOList = sysContentTypeService.getSysContentTypeList();
                for (int i = 0; i < sysContentTypeTOList.size(); i++) {
                    if (sysContentTypeTOList.get(i).getContentCategoryID().equals(sysContentTypeTO.getContentCategoryID())
                            && sysContentTypeTOList.get(i).getSysType().equalsIgnoreCase(sysContentTypeTO.getSysType())) {
                        j = 1;
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + sysContentTypeTO.getSysType() + "' is already present under '"
                                        + sysContentTypeTOList.get(i).getCategoryName() + "'", ""));
                        sysContentTypeBean.setRenderErrorMessage(true);
                        break;
                    }
                }
            }
            if (j == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateEditSysContentCategory(SysContentTypeTO sysContentTypeTO)
    {
        List<SysContentTypeTO> sysContentTypeTOList = null;
        Set<SysContentTypeTO> sysContentCategoryList = null;
        try {
            sysContentCategoryList = new TreeSet<SysContentTypeTO>(new SysContentCategoryComparator());
            initialize();
            sysContentTypeBean.setSelectedTypeTO(sysContentTypeTO);
            sysContentTypeTOList = sysContentTypeBean.getSysContentTypeTOList();
            sysContentCategoryList.addAll(sysContentTypeTOList);
            sysContentTypeBean.setContentCategoryList(sysContentCategoryList);
            sysContentTypeBean.setContentCategoryID(sysContentTypeTO.getContentCategoryID());
            sysContentTypeBean.setSysTypeID(sysContentTypeTO.getSysTypeID());
            sysContentTypeBean.setSysType(sysContentTypeTO.getSysType());
            sysContentTypeBean.setDuplicateContentCategoryID(sysContentTypeTO.getContentCategoryID());
            sysContentTypeBean.setDuplicateSysType(sysContentTypeTO.getSysType());
        } catch (Exception e) {

        }
    }

    public void saveSysContentType()
    {
        SysContentTypeTO sysContentTypeTO = null;
        RequestContext requestContext = null;
        String category = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            //    sysContentTypeTO = sysContentTypeBean.getSelectedTypeTO();
            sysContentTypeTO = new SysContentTypeTO();
            sysContentTypeTO.setContentCategoryID(sysContentTypeBean.getContentCategoryID());
            sysContentTypeTO.setContentName(sysContentTypeBean.getCategoryName());
            if (sysContentTypeBean.getSysTypeID() != null) {
                sysContentTypeTO.setSysTypeID(sysContentTypeBean.getSysTypeID());
            }
            sysContentTypeTO.setSysType(sysContentTypeBean.getSysType());
            category = sysContentTypeTO.getCategoryName();
            if (sysContentTypeTO.getContentCategoryID().equals(sysContentTypeBean.getDuplicateContentCategoryID())
                    && sysContentTypeTO.getSysType().trim().equalsIgnoreCase(sysContentTypeBean.getDuplicateSysType())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes were made.", ""));
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_EDIT_DIALOG_HIDE);
            } else if (validateSysContentType(sysContentTypeTO)) {
                sysContentTypeService.saveSysContentType(sysContentTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Type updated successfully", ""));
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_DATATABLE_RESET);
                requestContext.execute(ISysContentTypeConstants.SYSCONTENTTYPE_PAGINATION_DATATABLE_RESET);
            } else {
                RequestContext.getCurrentInstance().update("sysContentTypeEditForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysContentCategoryConstants.SYSCONTENTCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
        }
    }
}