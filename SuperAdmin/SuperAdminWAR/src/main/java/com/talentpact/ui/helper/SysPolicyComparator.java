package com.talentpact.ui.helper;

import java.util.Comparator;

import com.talentpact.ui.sysPolicy.to.SysPolicyTO;


public class SysPolicyComparator implements Comparator<SysPolicyTO>
{
    public int compare(SysPolicyTO e1, SysPolicyTO e2)
    {
        return e1.getPolicyName().compareTo(e2.getPolicyName());
    }
}