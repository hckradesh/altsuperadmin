/**
 * 
 */
package com.talentpact.ui.survey.user.common.transport.util;

import java.util.Comparator;
import java.util.List;

import com.talentpact.remote.common.ResponseTransferObject;
import com.talentpact.ui.survey.user.common.transport.output.CellTrackerResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.QuestionColumnValueResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.QuestionColumnsResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.QuestionOptionResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.QuestionResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.SysComponentTypeResponseTO;



/**
 * @author vaibhav.kashyap
 *
 */
public class QuestionInsertionUtil extends ResponseTransferObject 
	implements Comparator<QuestionInsertionUtil> , Comparable<QuestionInsertionUtil>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1474577529989788876L;
	private Long questionID;
	private QuestionResponseTO questionResponseTO = null;
	private List<QuestionOptionResponseTO> questionOptionIDList;
	private List<QuestionColumnsResponseTO> questionColumIDList;
	private List<QuestionColumnValueResponseTO> questionColumnValueIDList;
	private List<List<QuestionColumnsResponseTO>> listOfListquestionColumnsResponseTOList;
	private List<SysComponentTypeResponseTO> sysComponentTypeID;
	private List<CellTrackerResponseTO> cellTrackerResponseTOList;
	
	
	public QuestionInsertionUtil(){
		
	}

	public Long getQuestionID() {
		return questionID;
	}

	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}

	public QuestionResponseTO getQuestionResponseTO() {
		return questionResponseTO;
	}

	public void setQuestionResponseTO(QuestionResponseTO questionResponseTO) {
		this.questionResponseTO = questionResponseTO;
	}

	public List<QuestionOptionResponseTO> getQuestionOptionIDList() {
		return questionOptionIDList;
	}

	public void setQuestionOptionIDList(
			List<QuestionOptionResponseTO> questionOptionIDList) {
		this.questionOptionIDList = questionOptionIDList;
	}

	public List<QuestionColumnsResponseTO> getQuestionColumIDList() {
		return questionColumIDList;
	}

	public void setQuestionColumIDList(
			List<QuestionColumnsResponseTO> questionColumIDList) {
		this.questionColumIDList = questionColumIDList;
  	}

	public List<QuestionColumnValueResponseTO> getQuestionColumnValueIDList() {
		return questionColumnValueIDList;
	}

	public void setQuestionColumnValueIDList(
			List<QuestionColumnValueResponseTO> questionColumnValueIDList) {
		this.questionColumnValueIDList = questionColumnValueIDList;
	}

	public List<SysComponentTypeResponseTO> getSysComponentTypeID() {
		return sysComponentTypeID;
	}

	public void setSysComponentTypeID(
			List<SysComponentTypeResponseTO> sysComponentTypeID) {
		this.sysComponentTypeID = sysComponentTypeID;
	}

	public List<List<QuestionColumnsResponseTO>> getListOfListquestionColumnsResponseTOList() {
		return listOfListquestionColumnsResponseTOList;
	}

	public void setListOfListquestionColumnsResponseTOList(
			List<List<QuestionColumnsResponseTO>> listOfListquestionColumnsResponseTOList) {
		this.listOfListquestionColumnsResponseTOList = listOfListquestionColumnsResponseTOList;
	}

	public List<CellTrackerResponseTO> getCellTrackerResponseTOList() {
		return cellTrackerResponseTOList;
	}

	public void setCellTrackerResponseTOList(
			List<CellTrackerResponseTO> cellTrackerResponseTOList) {
		this.cellTrackerResponseTOList = cellTrackerResponseTOList;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(QuestionInsertionUtil o1, QuestionInsertionUtil o2) {
		  /* return o1.questionID<o2.questionID ? -1 :
               o1.questionID == o2.questionID ? 0 : 1;*/
				return o1.questionResponseTO.getSequence()<o2.questionResponseTO.getSequence() ? -1 :
		               o1.questionResponseTO.getSequence() == o2.questionResponseTO.getSequence() ? 0 : 1;
	}

	
	/**
	 * Implementing comparable since Arrays.sort by default uses comparable for
	 * Sorting  
	 * */
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QuestionInsertionUtil o) {
		if(this.questionResponseTO.getSequence()==o.getQuestionResponseTO().getSequence()){
			return 0;
		}else{
			 return this.questionResponseTO.getSequence() > o.getQuestionResponseTO().getSequence() ? 1 : -1;

		}
	}

}
