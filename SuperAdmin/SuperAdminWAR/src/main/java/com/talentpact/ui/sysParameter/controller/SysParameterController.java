/**
 * 
 */
package com.talentpact.ui.sysParameter.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.output.SysParameterResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.dataservice.SysParameter.SysParameterService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysParameter.bean.SysParameterBean;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("sysParameterController")
@ConversationScoped
public class SysParameterController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */

    @Inject
    MenuBean menuBean;

    @Inject
    SysParameterBean sysParameterBean;

    @Inject
    SysParameterService sysParameterService;

    @Inject
    UserSessionBean userSessionBean;


    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        List<SysParameterResponseTO> sysParameterTOList = null;
        try {
            resetSysParameterForm(sysParameterBean);
            sysParameterTOList = sysParameterService.getListingData();
            sysParameterBean.setParamList(sysParameterTOList);
            sysParameterBean.setSysParameterResponseTO(new SysParameterResponseTO());
            menuBean.setPage("../sysParameter/sysParameter.xhtml");
            requestContext.execute("PF('sysParameterDataTable').clearFilters();");
            requestContext.execute("PF('sysParameterDataTable').getPaginator().setPage(0);");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void resetSysParameterForm(SysParameterBean sysParameterBean)
    {
        sysParameterBean.setParamList(null);
    }


    private void getDropDownList()
    {
        // TODO Auto-generated method stub
        List<SysParameterResponseTO> dataTypeDropDownList = null;
        dataTypeDropDownList = sysParameterService.dataTypeDropDownList();
        sysParameterBean.setParameterDataList(dataTypeDropDownList);
        sysParameterBean.setParamLabel(null);
        sysParameterBean.setParamDescripTion(null);
        sysParameterBean.setParameterDataTypeId(0);


    }


    public void saveSysParameter()
    {
        try {
            // TODO Auto-generated method stub
            List<SysParameterResponseTO> sysParameterTOList = null;
            String paramLabel = sysParameterBean.getParamLabel();
            String paramDescription = sysParameterBean.getParamDescripTion();
            Integer paramDataTypeID = sysParameterBean.getParameterDataTypeId();
            boolean statusOfInsert = false;
            sysParameterBean.getSysParameterResponseTO().setParamLabel(paramLabel);
            sysParameterBean.getSysParameterResponseTO().setParamDescription(paramDescription);
            sysParameterBean.getSysParameterResponseTO().setDataTypeId(paramDataTypeID);
            statusOfInsert = sysParameterService.saveSysParameter(sysParameterBean.getSysParameterResponseTO());
            if (statusOfInsert) {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "SysParameter Data Saved Successfully ", ""));
                RequestContext.getCurrentInstance().execute("PF('addSysParameterModal').hide();");
                sysParameterTOList = sysParameterService.getListingData();
                sysParameterBean.setParamList(sysParameterTOList);
                initialize();
                RequestContext.getCurrentInstance().update("SysParameter");


            } else {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "SysParameter Data NOT Saved Successfully ", ""));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    public void geteditData(SysParameterResponseTO sysParameterResponseTO)
    {
        List<SysParameterResponseTO> sysParameterTOList = null;
        boolean statusOfInsert = false;
        Integer paramID = sysParameterResponseTO.getParamID();
        sysParameterResponseTO.setParamID(paramID);
        sysParameterTOList = sysParameterService.geteditData(sysParameterResponseTO);
        List<SysParameterResponseTO> dataTypeDropDownList = null;
        dataTypeDropDownList = sysParameterService.dataTypeDropDownList();
        sysParameterBean.setParameterDataList(dataTypeDropDownList);
        if (sysParameterTOList != null) {
            sysParameterBean.setParamID(sysParameterTOList.get(0).getParamID());
            sysParameterBean.setParameterDataTypeId(sysParameterTOList.get(0).getDataTypeId());
            sysParameterBean.setParamLabel(sysParameterTOList.get(0).getParamLabel());
            sysParameterBean.setParamDescripTion(sysParameterTOList.get(0).getParamDescription());
            sysParameterBean.setDataTypeName(sysParameterTOList.get(0).getDataTypeName());
        }
        RequestContext.getCurrentInstance().update("editSysParameter");

    }


    public void editData()
    {
        try {
            List<SysParameterResponseTO> sysParameterTOList = null;
            boolean statusOfInsert = false;
            Integer paramID = sysParameterBean.getParamID();
            String paramLabel = sysParameterBean.getParamLabel();
            Integer dataTypeId = sysParameterBean.getParameterDataTypeId();
            String paramDesc = sysParameterBean.getParamDescripTion();
            sysParameterBean.getSysParameterResponseTO().setParamLabel(paramLabel);
            sysParameterBean.getSysParameterResponseTO().setDataTypeId(dataTypeId);
            sysParameterBean.getSysParameterResponseTO().setParamDescription(paramDesc);
            sysParameterBean.getSysParameterResponseTO().setParamID(paramID);
            statusOfInsert = sysParameterService.editData(sysParameterBean.getSysParameterResponseTO());
            if (statusOfInsert) {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "SysParameter Data Saved Successfully ", ""));
                RequestContext.getCurrentInstance().execute("PF('editSysParameterModal').hide();");
                //  sysParameterTOList = sysParameterService.getListingData();
                //  sysParameterBean.setParamList(sysParameterTOList);
                initialize();
                RequestContext.getCurrentInstance().update("SysParameter");

            } else {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, "SysParameter Data NOT Saved Successfully ", ""));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        RequestContext.getCurrentInstance().update("SysParameter");


    }

}
