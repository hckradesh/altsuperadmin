/**
 * 
 */
package com.talentpact.ui.sysEntityColumn.converter;

import java.util.ArrayList;
import java.util.List;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.model.SysEntityColumn;
import com.talentpact.ui.sysEntityColumn.common.transport.input.SysEntityColumnRequestTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;

/**
 * @author vaibhav.kashyap
 *
 */
public class SysEntityColumnConverter {

	/**
	 * @param secObj
	 * @return
	 */
	public static SysEntityColumnResponseTO getSysEntityColumnResponseTOByEntity(
			SysEntityColumn secObj) throws Exception{
		SysEntityColumnResponseTO sysEntityColumnResponseTO = null;
		try{
			if(secObj!=null){
				sysEntityColumnResponseTO = new SysEntityColumnResponseTO();
				if(secObj.getCreatedBy()>0)
					sysEntityColumnResponseTO.setCreatedBy(
							secObj.getCreatedBy());
				
				if(secObj.getCreatedDate()!=null)
					sysEntityColumnResponseTO.setCreatedDate(
							secObj.getCreatedDate());
				
				if(secObj.getdBColumnName()!=null && 
						!secObj.getdBColumnName().equals(""))
					sysEntityColumnResponseTO.setDbColumnName(
							secObj.getdBColumnName());
				
				if(secObj.getDbColumnType()!=null && 
						!secObj.getDbColumnType().equals(""))
					sysEntityColumnResponseTO.setDescription(secObj.getDbColumnType());
				
				if(secObj.getEntityColumnID()>0)
					sysEntityColumnResponseTO.setEntityColumnID(secObj.getEntityColumnID());
				
				if(secObj.getEntityColumnName()!=null &&
						!secObj.getEntityColumnName().equals(""))
					sysEntityColumnResponseTO.setEntityColumnName(
							secObj.getEntityColumnName());
				
				if(secObj.getEntityColumnType()!=null &&
						!secObj.getEntityColumnType().equals(""))
					sysEntityColumnResponseTO.setEntityColumnType(
							secObj.getEntityColumnType());
				
				if(secObj.getLabel()!=null &&
						!secObj.getLabel().equals(""))
					sysEntityColumnResponseTO.setLabel(secObj.getLabel());
				
				if(secObj.getModifiedBy()>0)
					sysEntityColumnResponseTO.setModifiedBy(secObj.getModifiedBy());
				
				if(secObj.getModifiedDate()!=null)
					sysEntityColumnResponseTO.setModifiedDate(secObj.getModifiedDate());
				
				if(secObj.getSysEntity()!=null)
					sysEntityColumnResponseTO.setSysEntityId(secObj.getSysEntityID());
				
				if(secObj.getTenantId()>0)
					sysEntityColumnResponseTO.setTenantID(secObj.getTenantId());
				
			}
		}catch(Exception ex){
			sysEntityColumnResponseTO = null;
			throw ex;
		}
		return sysEntityColumnResponseTO;
	}
	
	/**
	 * 
	 * @param sysEntityColumnList
	 * @return
	 * @author raghvendra.mishra
	 */
	public List<SysEntityColumnResponseTO> convertSysEntityColumnToDataTO(List<SysEntityColumn> sysEntityColumnList){
        List<SysEntityColumnResponseTO> resultList = null;
        SysEntityColumnResponseTO sysEntityColumnResponseTO = null;
        try {
            resultList = new ArrayList<SysEntityColumnResponseTO>();
            for (SysEntityColumn sysEntityColumn : sysEntityColumnList) {
            	sysEntityColumnResponseTO = new SysEntityColumnResponseTO();
            	sysEntityColumnResponseTO.setEntityColumnID(sysEntityColumn.getEntityColumnID());
            	sysEntityColumnResponseTO.setSysEntityId(sysEntityColumn.getSysEntityID());
            	sysEntityColumnResponseTO.setEntityColumnName(sysEntityColumn.getEntityColumnName());            	
            	sysEntityColumnResponseTO.setDbColumnName(sysEntityColumn.getdBColumnName());
            	sysEntityColumnResponseTO.setLabel(sysEntityColumn.getLabel());
            	sysEntityColumnResponseTO.setDescription(sysEntityColumn.getDescription());
            	sysEntityColumnResponseTO.setTenantID(sysEntityColumn.getTenantId());
            	sysEntityColumnResponseTO.setCreatedDate(SQLDateHelper.getSqlTimeStampFromDate(sysEntityColumn.getCreatedDate()));
            	sysEntityColumnResponseTO.setModifiedDate(SQLDateHelper.getSqlTimeStampFromDate(sysEntityColumn.getModifiedDate()));
            	sysEntityColumnResponseTO.setCreatedBy(sysEntityColumn.getCreatedBy());
            	sysEntityColumnResponseTO.setModifiedBy(sysEntityColumn.getModifiedBy());
            	sysEntityColumnResponseTO.setEntityColumnType(sysEntityColumn.getEntityColumnType());
            	sysEntityColumnResponseTO.setDbColumnType(sysEntityColumn.getDbColumnType());
                resultList.add(sysEntityColumnResponseTO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultList;

    }

	/**
	 * 
	 * @param sysEntityColumn
	 * @return
	 * @author raghvendra.mishra
	 */
    public SysEntityColumnRequestTO convert(SysEntityColumn sysEntityColumn){
    	SysEntityColumnRequestTO sysEntityColumnRequestTO = null;
        try {
        	sysEntityColumnRequestTO = new SysEntityColumnRequestTO();	
        	sysEntityColumnRequestTO.setSysEntityId(sysEntityColumn.getSysEntityID());
        	sysEntityColumnRequestTO.setEntityColumnName(sysEntityColumn.getEntityColumnName());
        	sysEntityColumnRequestTO.setDbColumnName(sysEntityColumn.getdBColumnName());
        	sysEntityColumnRequestTO.setLabel(sysEntityColumn.getLabel());
        	sysEntityColumnRequestTO.setDescription(sysEntityColumn.getDescription());
        	sysEntityColumnRequestTO.setTenantID(sysEntityColumn.getTenantId());
        	sysEntityColumnRequestTO.setCreatedDate(sysEntityColumn.getCreatedDate());
        	sysEntityColumnRequestTO.setModifiedDate(sysEntityColumn.getModifiedDate());
        	sysEntityColumnRequestTO.setCreatedBy(sysEntityColumn.getCreatedBy());
        	sysEntityColumnRequestTO.setModifiedBy(sysEntityColumn.getModifiedBy());
        	sysEntityColumnRequestTO.setEntityColumnType(sysEntityColumn.getEntityColumnType());
        	sysEntityColumnRequestTO.setDbColumnType(sysEntityColumn.getDbColumnType());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sysEntityColumnRequestTO;
    }
}
