/**
 * 
 */
package com.talentpact.ui.hrContentType.facaderemote;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;

/**
 * @author vaibhav.kashyap
 *
 */
@Local
public interface HrContentTypeLocalFacade {
	public ServiceResponse getHrContentTypeByContentCategory(ServiceRequest
			serviceRequest) throws Exception;
}
