package com.talentpact.ui.replicateFormSecurity.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.replicateFormSecurity.controller.ReplicateFormSecurityController;
import com.talentpact.ui.replicateFormSecurity.to.ReplicateFormSecurityTO;

/**
 * 
 * @author vivek.goyal
 *
 */


@SuppressWarnings("serial")
@Named("replicateFormSecurityBean")
@ConversationScoped
public class ReplicateFormSecurityBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    ReplicateFormSecurityController replicateFormSecurityController;

    private Integer sourceOrganizationID;

    private String sourceOrganizationName;

    private Integer destinationOrganizationID;

    private String destinationOrganizationName;

    private Integer moduleID;

    private String moduleName;

    private Integer sysModuleID;

    private List<ReplicateFormSecurityTO> sourceOrganizationTOList;

    private List<ReplicateFormSecurityTO> destinationOrganizationTOList;

    private List<ReplicateFormSecurityTO> sourceModuleTOList;

    private List<ReplicateFormSecurityTO> replicateFormSecurityHistoryTOList;

    private boolean formSecurityExists;

    private String userName;

    private Date createdDate;

    private boolean copyButtonVisible;

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            replicateFormSecurityController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public Integer getSourceOrganizationID()
    {
        return sourceOrganizationID;
    }

    public void setSourceOrganizationID(Integer sourceOrganizationID)
    {
        this.sourceOrganizationID = sourceOrganizationID;
    }

    public String getSourceOrganizationName()
    {
        return sourceOrganizationName;
    }

    public void setSourceOrganizationName(String sourceOrganizationName)
    {
        this.sourceOrganizationName = sourceOrganizationName;
    }

    public Integer getDestinationOrganizationID()
    {
        return destinationOrganizationID;
    }

    public void setDestinationOrganizationID(Integer destinationOrganizationID)
    {
        this.destinationOrganizationID = destinationOrganizationID;
    }

    public String getDestinationOrganizationName()
    {
        return destinationOrganizationName;
    }

    public void setDestinationOrganizationName(String destinationOrganizationName)
    {
        this.destinationOrganizationName = destinationOrganizationName;
    }

    public Integer getModuleID()
    {
        return moduleID;
    }

    public void setModuleID(Integer moduleID)
    {
        this.moduleID = moduleID;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public List<ReplicateFormSecurityTO> getSourceOrganizationTOList()
    {
        return sourceOrganizationTOList;
    }

    public void setSourceOrganizationTOList(List<ReplicateFormSecurityTO> sourceOrganizationTOList)
    {
        this.sourceOrganizationTOList = sourceOrganizationTOList;
    }

    public List<ReplicateFormSecurityTO> getDestinationOrganizationTOList()
    {
        return destinationOrganizationTOList;
    }

    public void setDestinationOrganizationTOList(List<ReplicateFormSecurityTO> destinationOrganizationTOList)
    {
        this.destinationOrganizationTOList = destinationOrganizationTOList;
    }

    public List<ReplicateFormSecurityTO> getSourceModuleTOList()
    {
        return sourceModuleTOList;
    }

    public void setSourceModuleTOList(List<ReplicateFormSecurityTO> sourceModuleTOList)
    {
        this.sourceModuleTOList = sourceModuleTOList;
    }

    public boolean isFormSecurityExists()
    {
        return formSecurityExists;
    }

    public void setFormSecurityExists(boolean formSecurityExists)
    {
        this.formSecurityExists = formSecurityExists;
    }

    public Integer getSysModuleID()
    {
        return sysModuleID;
    }

    public void setSysModuleID(Integer sysModuleID)
    {
        this.sysModuleID = sysModuleID;
    }

    public List<ReplicateFormSecurityTO> getReplicateFormSecurityHistoryTOList()
    {
        return replicateFormSecurityHistoryTOList;
    }

    public void setReplicateFormSecurityHistoryTOList(List<ReplicateFormSecurityTO> replicateFormSecurityHistoryTOList)
    {
        this.replicateFormSecurityHistoryTOList = replicateFormSecurityHistoryTOList;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public boolean isCopyButtonVisible()
    {
        return copyButtonVisible;
    }

    public void setCopyButtonVisible(boolean copyButtonVisible)
    {
        this.copyButtonVisible = copyButtonVisible;
    }
}
