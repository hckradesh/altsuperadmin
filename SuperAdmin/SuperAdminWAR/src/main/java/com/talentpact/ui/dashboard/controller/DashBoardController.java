package com.talentpact.ui.dashboard.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.antlr.grammar.v3.ANTLRv3Parser.exceptionGroup_return;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.hris.group.constants.IGroupConstants;
import com.talentpact.business.service.dashboard.DashBoardService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboard.bean.DashBoardBean;
import com.talentpact.ui.dashboard.constant.IDashnoardConstant;
import com.talentpact.ui.dashboard.helper.DashboardHelper;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.dashboard.transport.output.ModuleTO;
import com.talentpact.ui.menu.bean.MenuBean;

@Named("dashBoardController")
@ConversationScoped
public class DashBoardController extends CommonController implements Serializable, IDefaultAction
{
	private static final long serialVersionUID = -3191247914833006910L;
	private static final Logger LOGGER_ = LoggerFactory.getLogger(DashBoardController.class);


    @Inject
    UserSessionBean userSessionBean;

    @Inject
    MenuBean menuBean;
    
    @EJB
    DashBoardService dashBoardService ;
    
    @Inject
    DashBoardBean dashBoardBean ;
    
    
    
    
    @Override
    public void initialize()
    {
    	List<DashBoardTO> dashBoardTOList = null ;
    	List<ModuleTO> moduleList = null ;
        try {
        	  dashBoardTOList =  dashBoardService.getSysDashBoard();
        	  moduleList  = dashBoardService.getModileList();
        	 // dashBoardBean.setModuleList(moduleList);
        	  DashboardHelper.filteredModuleList(dashBoardBean , moduleList);
        	  DashboardHelper.assigModuleNameAndColumn(dashBoardTOList , moduleList , dashBoardBean);
        	  dashBoardBean.setDashBoardTO(new DashBoardTO());
        	  dashBoardBean.setDashBoardTOList(dashBoardTOList);
        	  
        	  menuBean.setPage(IDashnoardConstant.HR_DASHBOARD_LIST);
        	   
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }
    
    public void updateDashBoard(DashBoardTO dashBoardTO)
    {
    	dashBoardBean.setDashBoardTO(dashBoardTO);
    	dashBoardBean.setSaveFlag(false);
    	getSubModuleList(dashBoardTO.getParentModuleID());
    }
    public void createSysDashBoard()
    {
    	System.out.println("creation method is called");
    	DashBoardTO dashBoardTO = new DashBoardTO();
    	dashBoardBean.setDashBoardTO(dashBoardTO);
    	dashBoardBean.setModuleList(new ArrayList<ModuleTO>());
    	dashBoardBean.setSaveFlag(true);
    }
    public void getSubModuleList(Integer parentModuleID)
    {
    	     List<ModuleTO> childMenuList  = new ArrayList<ModuleTO>();
    	     List<ModuleTO> allChildModuleList = dashBoardBean.getAllChildModuleList();
    	     for(ModuleTO mto :allChildModuleList)
    	     {
    	    	   int parentModuleIdc = mto.getParentModuleID();
    	    	   if(parentModuleIdc==parentModuleID){
    	    		   childMenuList.add(mto);
    	    	   }
    	     }
    	     dashBoardBean.setModuleList(childMenuList);
    }
    public void saveDashBoard(boolean newOperationFlag)
    throws Exception
    {
	     try {
	    	 DashBoardTO dashBoardTO = dashBoardBean.getDashBoardTO();
    	     List<DashBoardTO> dashBoardTOList = dashBoardBean.getDashBoardTOList();
    	     if(newOperationFlag==true)
    	     {
    	       dashBoardTO.setRowPosition(dashBoardTOList.size() + 1);
    	     }
    	     dashBoardService.saveDashBoard(dashBoardTO);
    	     initialize();
    	     
    	     if (newOperationFlag) {
                 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IDashnoardConstant.DASHBOARD_SUCCESS_ADD, ""));
             } else {
                 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IDashnoardConstant.DASHBOARD_SUCCESS_UPDATE, ""));
             }
    	     RequestContext.getCurrentInstance().update("dashBoardListForm");//PF('dashBoardEditModal').hide();
    	     RequestContext.getCurrentInstance().execute("PF('dashBoardEditModal').hide();");
		} catch (Exception e) {
			  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IGroupConstants.UNEXPECTED_ERROR_MSG, ""));
	          RequestContext.getCurrentInstance().execute("scrollUp()");
	          e.printStackTrace();
		}
    	     
    }

   
}
