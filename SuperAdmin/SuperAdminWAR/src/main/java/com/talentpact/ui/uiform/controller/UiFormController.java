/**
 * 
 */
package com.talentpact.ui.uiform.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.menu.MenuModel;

import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.business.application.transport.output.SysResourceBundleNewResponseTO;
import com.talentpact.business.application.transport.output.SysRuleResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldGroupResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.common.constants.IUIFormConstants;
import com.talentpact.business.dataservice.UiForm.UiFormService;
import com.talentpact.business.dataservice.sysChatBotValidation.SysChatBotValidationService;
import com.talentpact.business.dataservice.sysChatBotValidationType.SysChatBotValidationTypeService;
import com.talentpact.model.SysBundle;
import com.talentpact.model.worflow.UiFormField;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.menu.transport.MenuResponseTO;
import com.talentpact.ui.sysChatBotValidation.to.ChatBotColumnValidationMapperTO;
import com.talentpact.ui.sysChatBotValidation.to.SysChatBotValidationTO;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;
import com.talentpact.ui.uiform.bean.UIFormBean;
import com.talentpact.ui.uiform.bean.UIFormFieldBean;
import com.talentpact.ui.uiform.bean.UIFormGroupBean;
import com.talentpact.ui.uiform.bean.UploadFieldsBean;
import com.talentpact.ui.uiform.helper.FormField;
import com.talentpact.ui.uiform.helper.HomeHelper;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("uiFormController")
@ConversationScoped
public class UiFormController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */

    @Inject
    MenuBean menuBean;

    @Inject
    UIFormBean uIFormBean;

    @Inject
    UploadFieldsBean uploadFieldsBean;

    @Inject
    UiFormService uiFormService;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    UIFormGroupBean uiFormGroupBean;

    @Inject
    UIFormFieldBean uiFormFieldBean;

    UiFormField uiFormField;

    UiFormFieldResponseTO uiFormFieldResponseTO;
    
    @Inject
	SysChatBotValidationTypeService sysChatBotValidationTypeService;
    
    @Inject
	SysChatBotValidationService sysChatBotValidationService;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        List<UiFormResponseTO> uiFormControllerTOList = null;
        try {
            resetUiForm(uIFormBean);
            uiFormControllerTOList = uiFormService.getDataList();
            bundleDropDown();
            fieldTypeDropDown();
            methodDropDown();
            getConfigurationValue();
            //  menuSequence();
            uIFormBean.setUiFormResponseTOList(uiFormControllerTOList);
            uIFormBean.setMenuAndSequneceCheck(true);
            uIFormBean.setUiFormResponseTO(new UiFormResponseTO());
            menuBean.setPage("../UIForm/UIForm.xhtml");
            requestContext.execute("PF('uiformDataTable').clearFilters();");
            requestContext.execute("PF('uiformDataTable').getPaginator().setPage(0);");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetUiForm(UIFormBean uIFormBean)
    {
        uIFormBean.setUiFormResponseTOList(null);
    }

    public void saveFormData()
        throws Exception
    {
        // TODO Auto-generated method stub
        RequestContext requestContext = RequestContext.getCurrentInstance();
        boolean result = false;
        boolean formNameExists = false;
        boolean isCustomComponentAllowed = false;
        boolean menu = false;
        boolean procResult = false;

        isCustomComponentAllowed = uIFormBean.isCustomComponentAllowed();
        menu = uIFormBean.getMenu();

        uIFormBean.getUiFormResponseTO().setFormName(uIFormBean.getFormName());

        if (uIFormBean.getPrimaryEntityID().intValue() == 0)
            uIFormBean.getUiFormResponseTO().setPrimaryEntityID(null);
        else
            uIFormBean.getUiFormResponseTO().setPrimaryEntityID(uIFormBean.getPrimaryEntityID());

        if (uIFormBean.getUrl() != null) {
            uIFormBean.getUiFormResponseTO().setUrl(uIFormBean.getUrl().trim());
        }
        if (uIFormBean.getUrl() == null || uIFormBean.getUrl().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setUrl(null);
        }

        if (uIFormBean.getParentFormID().intValue() == 0)
            uIFormBean.getUiFormResponseTO().setParentFormID(null);
        else
            uIFormBean.getUiFormResponseTO().setParentFormID(uIFormBean.getParentFormID());


        uIFormBean.getUiFormResponseTO().setTenantID(1);

        if (isCustomComponentAllowed) {
            uIFormBean.getUiFormResponseTO().setCustomComponentAllowed(true);
        } else {
            uIFormBean.getUiFormResponseTO().setCustomComponentAllowed(false);
        }

        uIFormBean.getUiFormResponseTO().setSequence(uIFormBean.getSequence());
        uIFormBean.getUiFormResponseTO().setIconPath(uIFormBean.getIconPath());
        uIFormBean.getUiFormResponseTO().setFloatingMenuIconPath(uIFormBean.getFloatingMenuIconPath());
        uIFormBean.getUiFormResponseTO().setInactiveIconPath(uIFormBean.getInactiveIconPath());

        if (uIFormBean.getIosSideMenu() == null || uIFormBean.getIosSideMenu().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setIosSideMenu(null);
        }
        if (uIFormBean.getIosSideMenu() != null) {
            uIFormBean.getUiFormResponseTO().setIosSideMenu(uIFormBean.getIosSideMenu().trim());
        }
        if (uIFormBean.getFragmentClass() != null) {
            uIFormBean.getUiFormResponseTO().setFragmentClass(uIFormBean.getFragmentClass().trim());
        }
        if (uIFormBean.getFragmentClass() == null || uIFormBean.getFragmentClass().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setFragmentClass(null);
        }

        uIFormBean.getUiFormResponseTO().setMenuCategory(uIFormBean.getMenuCategory());


        if (uIFormBean.getDefaultClassCall() != null) {
            uIFormBean.getUiFormResponseTO().setDefaultClassCall(uIFormBean.getDefaultClassCall());
        }
        if (uIFormBean.getDefaultClassCall() == null || uIFormBean.getDefaultClassCall().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setDefaultClassCall(null);
        }

        if (uIFormBean.getDisableBean() != null) {
            uIFormBean.getUiFormResponseTO().setDisableBean(uIFormBean.getDisableBean().trim());
        }
        if (uIFormBean.getDisableBean() == null || uIFormBean.getDisableBean().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setDisableBean(null);
        }

        if (uIFormBean.getDisableText() != null) {
            uIFormBean.getUiFormResponseTO().setDisableText(uIFormBean.getDisableText().trim());
        }
        if (uIFormBean.getDisableText() == null || uIFormBean.getDisableText().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setDisableText(null);
        }

        if (uIFormBean.getDisableUrl() != null) {
            uIFormBean.getUiFormResponseTO().setDisableUrl(uIFormBean.getDisableUrl().trim());
        }
        if (uIFormBean.getDisableUrl() == null || uIFormBean.getDisableUrl().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setDisableUrl(null);
        }

        uIFormBean.getUiFormResponseTO().setResourceDiscription(uIFormBean.getResourceDiscription());
        uIFormBean.getUiFormResponseTO().setResourceName(uIFormBean.getResourceName());

        if (uIFormBean.getSubModuleID() == null || uIFormBean.getSubModuleID() == 0) {
            uIFormBean.getUiFormResponseTO().setSubModuleID(uIFormBean.getModuleID());
        } else {
            uIFormBean.getUiFormResponseTO().setSubModuleID(uIFormBean.getSubModuleID());
        }

        boolean checkSequence = uiFormService.checkSequenceExists(uIFormBean.getUiFormResponseTO().getParentFormID(),uIFormBean.getUiFormResponseTO().getSequence(), 0);

        if (checkSequence == true) {


            if (menu) {
                uIFormBean.getUiFormResponseTO().setMenu(true);
            } else {
                uIFormBean.getUiFormResponseTO().setMenu(false);
            }

            int moduleSubmoduleID = 0;
            if (uIFormBean.getSubModuleID() == null || uIFormBean.getSubModuleID().intValue() == 0)
                moduleSubmoduleID = (uIFormBean.getModuleID());
            else
                moduleSubmoduleID = (uIFormBean.getSubModuleID());


            formNameExists = uiFormService.formNameExists(uIFormBean.getUiFormResponseTO().getFormName(),uIFormBean.getUiFormResponseTO().getFormID(), moduleSubmoduleID);

            if (!formNameExists) {

                Integer resourceID = uIFormBean.getResourceId();
                if (resourceID == null) {
                    uIFormBean.getUiFormResponseTO().setResourceId(0);
                }
                int sysResourceResult = uiFormService.insertToSysResourceForm(1, uIFormBean.getUiFormResponseTO().getResourceName(), uIFormBean.getUiFormResponseTO().getResourceDiscription());//bundle id is hard coded to save it as 1
                // int resourceIDSysResource = uiFormService.getresourceIDSysResource(uiFormResponseTO.getFormName());
                uIFormBean.getUiFormResponseTO().setResourceId(sysResourceResult);

                if (menu) {
                    //deleting menu Key from Redis if any new Form is added and IsMenu is  True in update and add case
                    uiFormService.deleteAllRedisKey();

                    int NumberOfTextBoxes = uIFormBean.getSystextBoxes().length;
                    //getting the title and formname from number of textboxes
                    //merge to uiform  

                    for (int i = 0; i < NumberOfTextBoxes; i++) {
                        String nameOFLang = uIFormBean.getSysBundle().get(i).getName();
                        Integer bundleId = uIFormBean.getSysBundle().get(i).getBundleId();
                        String textBoxData[] = uIFormBean.getSystextBoxes();
                        String textBoxDataTitle[] = uIFormBean.getSystextBoxesTitle();

                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, sysResourceResult);
                        if (checkCombinationResult == 0)// if combination does not exist then insert
                        {
                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], sysResourceResult, textBoxDataTitle[i]);
                        } else {
                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], sysResourceResult,
                                    checkCombinationResult, textBoxDataTitle[i]);

                        }

                    }
                }
                result = uiFormService.saveFormDatanew(uIFormBean.getUiFormResponseTO());// calling save
                                                                         // function
                                                                         // to save


                if (result) {

                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Data saved successfully", ""));
                    List<UiFormResponseTO> uiFormControllerTOList = uiFormService.getDataList();
                    uIFormBean.setUiFormResponseTOList(uiFormControllerTOList);
                    RequestContext.getCurrentInstance().execute("PF('UIFormAddModal').hide()");
                    RequestContext.getCurrentInstance().execute("scrollUp");
                    requestContext.execute("PF('uiformDataTable').clearFilters();");
                    requestContext.execute("PF('uiformDataTable').getPaginator().setPage(0);");


                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Data NOT saved ", ""));
                    RequestContext.getCurrentInstance().execute("scrollUp");
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mentioned form name already exists ", ""));
                RequestContext.getCurrentInstance().execute("scrollUp");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mentioned sequence already exists ", ""));
            RequestContext.getCurrentInstance().execute("scrollUp");
        }
    }

    public void addNewData()
    {
        // TODO Auto-generated method stub
        // List<UiFormResponseTO> subModuleList = null;
        List<UiFormResponseTO> moduleList = null;
        List<UiFormResponseTO> sysEntityList = null;
        List<UiFormResponseTO> parentFormList = null;
        List<UiFormResponseTO> subModuleList = null;
        try {
            // subModuleList = uiFormService.getSubModuleList();
            moduleList = uiFormService.getModuleList();
            sysEntityList = uiFormService.getSysEntityList();
            // parentFormList = uiFormService.getParentFormList();
            uIFormBean.setFormName(null);
            uIFormBean.setParentFormID(0);
            uIFormBean.setUrl(null);
            uIFormBean.setPrimaryEntityID(0);
            uIFormBean.setSysEntityList(sysEntityList);
            uIFormBean.setParentFormList(parentFormList);
            uIFormBean.setSubModuleList(subModuleList);
            uIFormBean.setIosSideMenu(null);
            uIFormBean.setModuleList(moduleList);
            uIFormBean.setMenu(false);
            uIFormBean.setSystextBoxes(null);
            uIFormBean.setSystextBoxesTitle(null);
            uIFormBean.setSysBundle(null);
            uIFormBean.setResourceDiscription(null);
            uIFormBean.setResourceName(null);
            uIFormBean.setIconPath(null);
            uIFormBean.setFloatingMenuIconPath(null);
            uIFormBean.setInactiveIconPath(null);
            uIFormBean.setMenuCategory(null);
            uIFormBean.setDefaultClassCall(null);
            uIFormBean.setSequence(null);
            uIFormBean.setSubModuleID(null);
            uIFormBean.setCustomComponentAllowed(false);
            uIFormBean.setMenuAndSequneceCheck(true);
            uIFormBean.setModuleID(null);
            uIFormBean.setFormID(0);
            uIFormBean.setDefaultClassCall(null);
            uIFormBean.setDisableBean(null);
            uIFormBean.setDisableText(null);
            uIFormBean.setDisableUrl(null);
            uIFormBean.setFragmentClass(null);
            getSysBundleForUiFormGroup();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void showData(UiFormResponseTO uiFormResponseTO)
    {
        String formName = uiFormResponseTO.getFormName();
        String entityname = uiFormResponseTO.getPrimaryEntityName();
        boolean customComponentAllowed = uiFormResponseTO.isCustomComponentAllowed();
        boolean menu = uiFormResponseTO.isMenu();
        String url = uiFormResponseTO.getUrl();
        Integer subModuleId = uiFormResponseTO.getSubModuleID();
        String parentForm = uiFormResponseTO.getParentFormName();
        Integer formId = uiFormResponseTO.getFormID();
        Integer resourceId = uiFormResponseTO.getResourceId();
        String resourceDiscription = uiFormResponseTO.getResourceDiscription();
        String resourceName = uiFormResponseTO.getResourceName();
        String iconPath = uiFormResponseTO.getIconPath();
        String floatingMenuIconPath = uiFormResponseTO.getFloatingMenuIconPath();
        String inactiveIconPath = uiFormResponseTO.getInactiveIconPath();
        String defaultClassCall = uiFormResponseTO.getDefaultClassCall();
        Integer menuCategory = uiFormResponseTO.getMenuCategory();
        Integer sequence = uiFormResponseTO.getSequence();
        Integer moduleId = uiFormResponseTO.getModuleID();
        String fragmentclass = uiFormResponseTO.getFragmentClass();
        List<UiFormResponseTO> subModuleList = null;
        List<UiFormResponseTO> moduleList = null;
        List<UiFormResponseTO> sysEntityList = null;
        List<UiFormResponseTO> parentFormList = null;
        String textbox[] = null;
        String titleData[] = null;
        //        String mobileConfiguration = uiFormResponseTO.getMobileConfiguration();
        //        String webConfiguration = uiFormResponseTO.getWebConfiguration();
        //        boolean groupParameter = uiFormResponseTO.getGroupParameter();


        int primaryEntityID = 0;


        if (uiFormResponseTO.getPrimaryEntityID() != null)
            primaryEntityID = uiFormResponseTO.getPrimaryEntityID();


        int subModuleID = 0;
        if (uiFormResponseTO.getSubModuleID() != null)
            subModuleID = uiFormResponseTO.getSubModuleID();
        if (uiFormResponseTO.getModuleID() != null)
            subModuleID = uiFormResponseTO.getModuleID();


        int parentFormID = 0;
        if (uiFormResponseTO.getParentFormID() != null)
            parentFormID = uiFormResponseTO.getParentFormID();
        try {
            if (menu) { //this check make sure sysbundle not give null pointer
                List<SysResourceBundleNewResponseTO> resourceBundleResponseTO = uiFormService.getSysBundleDataForTextBox(resourceId);
                textbox = new String[resourceBundleResponseTO.size()];
                titleData = new String[resourceBundleResponseTO.size()];
                int j = 0;
                for (SysResourceBundleNewResponseTO i : resourceBundleResponseTO) {
                    textbox[j] = i.getValue();
                    titleData[j] = i.getTitle();
                    j++;
                }
            } else {
                getSysBundleForUiFormGroup(); // this will intialize the textboxes and title if ISmenu is not selected to true.s
            }
            if (moduleId == null) {
                moduleId = -1;
            }
            List<UiFormResponseTO> moduleListGet = uiFormService.getSubModuleListUpdate(subModuleID);
            if (moduleListGet != null && moduleListGet.size() > 0) {
                moduleId = moduleListGet.get(0).getSubModuleID();
            } else {
                moduleId = subModuleID;
                subModuleID = 0;
            }
            subModuleList = uiFormService.getSubModuleList(moduleId);
            moduleList = uiFormService.getModuleList();
            sysEntityList = uiFormService.getSysEntityList();
            parentFormList = uiFormService.getParentFormListByComType(subModuleID,moduleId);

            for (int i = 0; i < parentFormList.size(); i++) {

                if (parentFormList.get(i).getParentFormName().equalsIgnoreCase(uiFormResponseTO.getFormName())) {
                    parentFormList.remove(i);

                }

            }
            uIFormBean.setFormName(formName);
            uIFormBean.setPrimaryEntityName(entityname);
            uIFormBean.setCustomComponentAllowed(customComponentAllowed);
            uIFormBean.setMenuCategory(menuCategory);
            if (url != null) {
                uIFormBean.setUrl(url.trim());
            }
            uIFormBean.setSubModuleID(subModuleID);
            uIFormBean.setParentFormName(parentForm);
            uIFormBean.setFormID(formId);
            uIFormBean.setSubModuleList(subModuleList);
            uIFormBean.setModuleID(moduleId);
            uIFormBean.setModuleList(moduleList);
            uIFormBean.setSysEntityList(sysEntityList);
            uIFormBean.setParentFormList(parentFormList);
            uIFormBean.setParentFormID(parentFormID);
            uIFormBean.setPrimaryEntityID(primaryEntityID);
            uIFormBean.setResourceId(resourceId);
            uIFormBean.setResourceDiscription(resourceDiscription);
            uIFormBean.setResourceName(resourceName);
            uIFormBean.setIconPath(iconPath);
            uIFormBean.setFloatingMenuIconPath(floatingMenuIconPath);
            uIFormBean.setInactiveIconPath(inactiveIconPath);
            uIFormBean.setDefaultClassCall(defaultClassCall);
            uIFormBean.setMenuCategory(menuCategory);
            uIFormBean.setSequence(sequence);
            uIFormBean.setMenu(menu);
            uIFormBean.setFragmentClass(fragmentclass);
            uIFormBean.setIosSideMenu(uiFormResponseTO.getIosSideMenu());
            uIFormBean.setDisableUrl(uiFormResponseTO.getDisableUrl());
            uIFormBean.setDisableText(uiFormResponseTO.getDisableText());
            uIFormBean.setDisableBean(uiFormResponseTO.getDisableBean());
            if (menuCategory == 4) {
                uIFormBean.setMenuAndSequneceCheck(false);
            } else {
                uIFormBean.setMenuAndSequneceCheck(true);
            }
            if (menu) {
                uIFormBean.setSystextBoxes(textbox);
                uIFormBean.setSystextBoxesTitle(titleData);
            } else {
                //  uIFormBean.setSystextBoxes(null);
                //  uIFormBean.setSystextBoxesTitle(null);  
            }
            List<UiFormFieldGroupResponseTO> uiFormResponseTOList = null;

            uiFormResponseTOList = uiFormService.getFieldGroupData(formId);

            uiFormGroupBean.setUiFormResponseTOList(uiFormResponseTOList);
            menuBean.setPage("../UIForm/UIFormGroup.xhtml");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void cancelGroupPage()
    {

        List<UiFormResponseTO> uiFormControllerTOList = null;
        try {
            resetUiForm(uIFormBean);
            uiFormControllerTOList = uiFormService.getDataList();
            uIFormBean.setUiFormResponseTOList(uiFormControllerTOList);
            menuBean.setPage("../UIForm/UIForm.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cancelGroupFieldPage()
    {

        try {
            menuBean.setPage("../UIForm/UIFormGroup.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateFormGroupData()
    {
        String fieldGroupName = uiFormFieldBean.getFieldGroupName();
        Integer formID = uiFormGroupBean.getFormID();
        Integer groupID = uiFormGroupBean.getFieldGroupID();
        UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO = new UiFormFieldGroupResponseTO();
        uiFormFieldGroupResponseTO.setFieldGroupName(fieldGroupName);
        uiFormFieldGroupResponseTO.setFormID(formID);
        uiFormFieldGroupResponseTO.setTenantID(1);
        uiFormFieldGroupResponseTO.setFieldGroupID(groupID);
        boolean result = false;
        boolean groupNameExist = false;
        try {

            groupNameExist = uiFormService.groupnameExists(uiFormFieldGroupResponseTO.getFieldGroupName(), uiFormFieldGroupResponseTO.getFormID(), groupID);

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if (!groupNameExist) {


            try {
                result = uiFormService.updateFormGroupData(uiFormFieldGroupResponseTO);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }// calling save function to save

            if (result) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Group Data saved successfully", ""));
                List<UiFormFieldGroupResponseTO> uiFormResponseTOList = new LinkedList<UiFormFieldGroupResponseTO>();
                try {
                    uiFormResponseTOList = uiFormService.getFieldGroupData(formID);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                uiFormGroupBean.setUiFormResponseTOList(uiFormResponseTOList);
                RequestContext.getCurrentInstance().update("updateUIForm");


            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Group Data NOT saved ", ""));
                RequestContext.getCurrentInstance().update("updateUIForm");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Group Name Already Exists.", ""));
            RequestContext.getCurrentInstance().update("addUIFormGroup");
            RequestContext.getCurrentInstance().execute("scrollUp");
        }
    }

    public void updateFormData()
        throws Exception
    {
        // TODO Auto-generated method stub

        boolean result = false;
        boolean formNameExists = false;
        boolean earliergroupnameexsist = false;
        boolean isCustomComponentAllowed = false;
        boolean menu = false;
        String earlierformName;
        boolean procResult = false;
        isCustomComponentAllowed = uIFormBean.isCustomComponentAllowed();
        menu = uIFormBean.getMenu();
        uIFormBean.getUiFormResponseTO().setFormName(uIFormBean.getFormName());
        if (uIFormBean.getPrimaryEntityID().intValue() == 0) {
            uIFormBean.getUiFormResponseTO().setPrimaryEntityID(null);
        } else {
            uIFormBean.getUiFormResponseTO().setPrimaryEntityID(uIFormBean.getPrimaryEntityID());
        }
        if (uIFormBean.getUrl() != null) {
            uIFormBean.getUiFormResponseTO().setUrl(uIFormBean.getUrl().trim());
        }
        if (uIFormBean.getUrl().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setUrl(null);
        }
        if (uIFormBean.getParentFormID().intValue() == 0)
            uIFormBean.getUiFormResponseTO().setParentFormID(null);
        else
            uIFormBean.getUiFormResponseTO().setParentFormID(uIFormBean.getParentFormID());

        if (uIFormBean.getSubModuleID() == null || uIFormBean.getSubModuleID().intValue() == 0)
            uIFormBean.getUiFormResponseTO().setSubModuleID(uIFormBean.getModuleID());
        else
            uIFormBean.getUiFormResponseTO().setSubModuleID(uIFormBean.getSubModuleID());


        uIFormBean.getUiFormResponseTO().setTenantID(1);
        uIFormBean.getUiFormResponseTO().setFormID(uIFormBean.getFormID());


        if (isCustomComponentAllowed) {
            uIFormBean.getUiFormResponseTO().setCustomComponentAllowed(true);
        } else {
            uIFormBean.getUiFormResponseTO().setCustomComponentAllowed(false);
        }

        uIFormBean.getUiFormResponseTO().setResourceDiscription(uIFormBean.getResourceDiscription());
        uIFormBean.getUiFormResponseTO().setResourceName(uIFormBean.getResourceName());
        uIFormBean.getUiFormResponseTO().setSequence(uIFormBean.getSequence());
        uIFormBean.getUiFormResponseTO().setIconPath(uIFormBean.getIconPath());
        uIFormBean.getUiFormResponseTO().setFloatingMenuIconPath(uIFormBean.getFloatingMenuIconPath());
        uIFormBean.getUiFormResponseTO().setInactiveIconPath(uIFormBean.getInactiveIconPath());
        if (uIFormBean.getIosSideMenu() == null || uIFormBean.getIosSideMenu().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setIosSideMenu(null);
        }
        if (uIFormBean.getIosSideMenu() != null) {
            uIFormBean.getUiFormResponseTO().setIosSideMenu(uIFormBean.getIosSideMenu().trim());
        }
        if (uIFormBean.getFragmentClass() != null) {
            uIFormBean.getUiFormResponseTO().setFragmentClass(uIFormBean.getFragmentClass().trim());
        }
        if (uIFormBean.getFragmentClass() == null || uIFormBean.getFragmentClass().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setFragmentClass(null);
        }


        if (uIFormBean.getDefaultClassCall() != null) {
            uIFormBean.getUiFormResponseTO().setDefaultClassCall(uIFormBean.getDefaultClassCall());
        }
        if (uIFormBean.getDefaultClassCall() == null || uIFormBean.getDefaultClassCall().trim().equalsIgnoreCase("")) {
            uIFormBean.getUiFormResponseTO().setDefaultClassCall(null);
        }


        uIFormBean.getUiFormResponseTO().setMenuCategory(uIFormBean.getMenuCategory());

        if (uIFormBean.getDisableBean() != null) {
            uIFormBean.getUiFormResponseTO().setDisableBean(uIFormBean.getDisableBean().trim());
        }
        if (uIFormBean.getDisableBean() == null || (uIFormBean.getDisableBean().trim().equalsIgnoreCase(""))) {
            uIFormBean.getUiFormResponseTO().setDisableBean(null);
        }
        if (uIFormBean.getDisableText() != null)
            uIFormBean.getUiFormResponseTO().setDisableText(uIFormBean.getDisableText().trim());

        if (uIFormBean.getDisableText() == null || uIFormBean.getDisableText().trim().equalsIgnoreCase(""))
            uIFormBean.getUiFormResponseTO().setDisableText(null);


        if (uIFormBean.getDisableUrl() != null)
            uIFormBean.getUiFormResponseTO().setDisableUrl(uIFormBean.getDisableUrl());

        if (uIFormBean.getDisableUrl() == null || uIFormBean.getDisableUrl().trim().equalsIgnoreCase(""))
            uIFormBean.getUiFormResponseTO().setDisableUrl(null);

        boolean checkSequence = uiFormService.checkSequenceExists(uIFormBean.getUiFormResponseTO().getParentFormID(), uIFormBean.getUiFormResponseTO().getSequence(), uIFormBean.getUiFormResponseTO().getFormID());

        if (checkSequence == true) {
            int resourceIDSysResource = uIFormBean.getResourceId();
            uIFormBean.getUiFormResponseTO().setResourceId(resourceIDSysResource);
            int sysResourceResult = uiFormService.UpdateToSysResourceForm(1, uIFormBean.getUiFormResponseTO().getResourceName(), uIFormBean.getUiFormResponseTO().getResourceDiscription(), resourceIDSysResource);//bundle id is hard coded to save it as 1


            if (menu) {
                uIFormBean.getUiFormResponseTO().setMenu(true);
            } else {
                uIFormBean.getUiFormResponseTO().setMenu(false);
            }


            int NumberOfTextBoxes = uIFormBean.getSystextBoxes().length;


            //merge to uiform  
            if (menu) {
                //deleting menu Key from Redis if any new Form is added and IsMenu is  True in update and add case
                uiFormService.deleteAllRedisKey();

                for (int i = 0; i < NumberOfTextBoxes; i++) {
                    String nameOFLang = uIFormBean.getSysBundle().get(i).getName();
                    Integer bundleId = uIFormBean.getSysBundle().get(i).getBundleId();
                    String textBoxData[] = uIFormBean.getSystextBoxes();
                    String textBoxDataTitle[] = uIFormBean.getSystextBoxesTitle();
                    Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceIDSysResource);
                    if (checkCombinationResult == 0)// if combination does not exist then insert
                    {
                        boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceIDSysResource, textBoxDataTitle[i]);
                    } else {
                        boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceIDSysResource,
                                checkCombinationResult, textBoxDataTitle[i]);
                    }

                }
            }
            try {
                int moduleSubmoduleID = 0;
                if (uIFormBean.getSubModuleID() == null || uIFormBean.getSubModuleID().intValue() == 0)
                    moduleSubmoduleID = (uIFormBean.getModuleID());
                else
                    moduleSubmoduleID = (uIFormBean.getSubModuleID());

                formNameExists = uiFormService.formNameExists(uIFormBean.getUiFormResponseTO().getFormName(), uIFormBean.getUiFormResponseTO().getFormID(), moduleSubmoduleID);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            if (!formNameExists) {

                try {
                    result = uiFormService.updateFormDatanew(uIFormBean.getUiFormResponseTO());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }// calling save function to save


                if (result) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Data Updated successfully", ""));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Data NOT updated ", ""));
                }
            } else {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mentioned form name already exists ", ""));
                RequestContext.getCurrentInstance().execute("scrollUp");

            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mentioned Sequence number already exists ", ""));
        }
    }

    public void addNewGroupData()
    {
        uiFormGroupBean.setFormID(uIFormBean.getFormID());
        uiFormGroupBean.setFieldGroupName("");
        uiFormGroupBean.setFieldGroupID(0);
    }

    public void addNewFieldData()
    {
        // TODO Auto-generated method stub
        //uiFormFieldBean.setUiFormFieldResponseTOList(null);
        uiFormFieldBean.setClientID(null);
        uiFormFieldBean.setEntityFieldID(null);
        uiFormFieldBean.setAction(false);
        uiFormFieldBean.setCutomfieldRequired(false);
        uiFormFieldBean.setMaster(false);
        uiFormFieldBean.setAttachmentRequired(false);
        uiFormFieldBean.setApprovalRequired(false);
        uiFormFieldBean.setEffectiveDateRequired(false);
        uiFormFieldBean.setResourceID(null);
        uiFormFieldBean.setMandatory(false);
        uiFormFieldBean.setParentFormFieldID(null);
        uiFormFieldBean.setAttachmentEnabled(false);
        uiFormFieldBean.setRegex(null);
        uiFormFieldBean.setSystextBoxes(null);
        uiFormFieldBean.setSystextBoxesTitle(null);
        uiFormFieldBean.setMobileConfiguration(null);
        uiFormFieldBean.setWebConfiguration(null);
        uiFormFieldBean.setGroupParameter(false);
        uiFormFieldBean.setRuleID(null);
        uiFormFieldBean.setSysFieldTypeID(null);
        uiFormFieldBean.setChatBotConfiguration(null);
        uiFormFieldBean.setSysChatBotTextBoxes(null);
        uiFormFieldBean.setSysChatBotTextBoxesTitle(null);
        uiFormFieldBean.setChatBotResourceID(null);
        uiFormFieldBean.setSelectedSysChatBotValidationTypeList(null);
        uiFormFieldBean.setShowValidationDetailDataTable(false);
        uiFormFieldBean.setChatBotSequence(null);

        try {
            List<UiFormFieldResponseTO> uiParentFormFieldTOList = uiFormService.getParentFormFieldData(uiFormFieldBean.getFieldGroupID());
            List<UiFormFieldResponseTO> uiSysResourceResponseTOList = uiFormService.getSysResourceData();
            List<UiFormFieldResponseTO> uiSysEntityFieldResponseTOList = uiFormService.getEntityFieldData(uiFormFieldBean.getFieldGroupID());
            uiFormFieldBean.setUiParentFormFieldTOList(uiParentFormFieldTOList);
            uiFormFieldBean.setUiSysEntityFieldResponseTOList(uiSysEntityFieldResponseTOList);
            uiFormFieldBean.setUiSysResourceResponseTOList(uiSysResourceResponseTOList);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        getSysBundle();


    }

    public void showFieldData(UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO)
    {
        List<UiFormFieldResponseTO> uiFormControllerTOList = null;
        try {
            Integer formID = uiFormFieldGroupResponseTO.getFormID();
            String fieldGroupName = uiFormFieldGroupResponseTO.getFieldGroupName();
            Integer fieldGroupID = uiFormFieldGroupResponseTO.getFieldGroupID();
            uiFormFieldBean.setFieldGroupID(fieldGroupID);
            uiFormFieldBean.setFieldGroupName(fieldGroupName);

            uiFormGroupBean.setFieldGroupName(fieldGroupName);
            uiFormGroupBean.setFormID(formID);
            uiFormGroupBean.setFieldGroupID(fieldGroupID);
            uiFormControllerTOList = uiFormService.getFormGroupFieldData(fieldGroupID);
            //settng data to display it for uiformfield
            uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);

            menuBean.setPage("../UIForm/UIFormField.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveFormGroupData()
    {
        String fieldGroupName = uiFormGroupBean.getFieldGroupName();
        Integer formID = uiFormGroupBean.getFormID();
        Integer groupID = uiFormGroupBean.getFieldGroupID();
        UiFormFieldGroupResponseTO uiFormFieldGroupResponseTO = new UiFormFieldGroupResponseTO();
        uiFormFieldGroupResponseTO.setFieldGroupName(fieldGroupName);
        uiFormFieldGroupResponseTO.setFormID(formID);
        uiFormFieldGroupResponseTO.setTenantID(1);
        boolean procResult = false;

        boolean result = false;
        boolean groupNameExist = false;
        try {
            groupNameExist = uiFormService.groupnameExists(uiFormFieldGroupResponseTO.getFieldGroupName(), uiFormFieldGroupResponseTO.getFormID(), groupID);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if (!groupNameExist) {

            try {
                result = uiFormService.saveFormGroupData(uiFormFieldGroupResponseTO);

                if (result) {
                    procResult = uiFormService.callProcedure(uiFormFieldGroupResponseTO.getFormID());//calling procedure
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }// calling save function to save

            if (result && procResult) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Group Data added successfully", ""));
                List<UiFormFieldGroupResponseTO> uiFormResponseTOList = new LinkedList<UiFormFieldGroupResponseTO>();
                try {
                    uiFormResponseTOList = uiFormService.getFieldGroupData(formID);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                uiFormGroupBean.setUiFormResponseTOList(uiFormResponseTOList);
                RequestContext.getCurrentInstance().update("updateUIForm");
                RequestContext.getCurrentInstance().execute("PF('UIFormGroupAddModal').hide();");
                RequestContext.getCurrentInstance().execute("scrollUp");
            } else {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Group Data not added ", ""));
                RequestContext.getCurrentInstance().update("addUIFormGroup");
                RequestContext.getCurrentInstance().execute("scrollUp");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Group Name Already Exists.", ""));
            RequestContext.getCurrentInstance().update("addUIFormGroup");
            RequestContext.getCurrentInstance().execute("scrollUp");
        }
    }

    public void saveFormField()
    {
    	UiFormFieldResponseTO uiFormFieldResponseTO = new UiFormFieldResponseTO();
        // TODO Auto-generated method stub
        try {
            boolean showmesg = false;
            boolean clientExists = false;
            boolean procResult = false;
            int formFieldID = 0;
            uiFormFieldResponseTO.setFieldGroupID(uiFormFieldBean.getFieldGroupID());
            uiFormFieldResponseTO.setEntityFieldID(uiFormFieldBean.getEntityFieldID());
            uiFormFieldResponseTO.setOrganizationID(1);
            uiFormFieldResponseTO.setClientID(uiFormFieldBean.getClientID());
            uiFormFieldResponseTO.setTenantID(1);
            if (uiFormFieldBean.getEntityFieldID() == 0)
                uiFormFieldResponseTO.setEntityFieldID(null);
            else
                uiFormFieldResponseTO.setEntityFieldID(uiFormFieldBean.getEntityFieldID());

            uiFormFieldResponseTO.setAction(uiFormFieldBean.isAction());
            uiFormFieldResponseTO.setApprovalRequired(uiFormFieldBean.isApprovalRequired());
            uiFormFieldResponseTO.setAttachmentEnabled(uiFormFieldBean.isAttachmentEnabled());
            uiFormFieldResponseTO.setMaster(uiFormFieldBean.isMaster());
            uiFormFieldResponseTO.setAttachmentRequired(uiFormFieldBean.isAttachmentRequired());
            uiFormFieldResponseTO.setEffectiveDateRequired(uiFormFieldBean.isEffectiveDateRequired());
            uiFormFieldResponseTO.setGroupParameter(uiFormFieldBean.isGroupParameter());
            uiFormFieldResponseTO.setConfigurationValueMobile(uiFormFieldBean.getMobileConfiguration());
            uiFormFieldResponseTO.setConfigurationValueWeb(uiFormFieldBean.getWebConfiguration());
            
            if (uiFormFieldBean.getResourceID() == null || uiFormFieldBean.getResourceID() == 0)
                uiFormFieldResponseTO.setResourceID(null);
            else
                uiFormFieldResponseTO.setResourceID(uiFormFieldBean.getResourceID());
            uiFormFieldResponseTO.setMandatory(uiFormFieldBean.isMandatory());
            if (uiFormFieldBean.getParentFormFieldID() == 0)
                uiFormFieldResponseTO.setParentFormFieldID(null);
            else
                uiFormFieldResponseTO.setParentFormFieldID(uiFormFieldBean.getParentFormFieldID());
            uiFormFieldResponseTO.setRegex(uiFormFieldBean.getRegex());
            uiFormFieldResponseTO.setFormFieldID(uiFormFieldBean.getFormFieldID());
            int NumberOfTextBoxes = uiFormFieldBean.getSystextBoxes().length;
            

            if(uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("VIEWABLE")  || uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("EDITABLE_NONMANDATORY") || uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("EDITABLE_MANDATORY"))
            {
            	uiFormFieldResponseTO.setConfigurationValueChatBot(uiFormFieldBean.getChatBotConfiguration());
            	uiFormFieldResponseTO.setSysFieldTypeID(uiFormFieldBean.getSysFieldTypeID());
                uiFormFieldResponseTO.setRuleID(uiFormFieldBean.getRuleID());
                int NumberOfChatBotTextBoxes = uiFormFieldBean.getSysChatBotTextBoxes().length;
                if (uiFormFieldBean.getChatBotResourceID() == null || uiFormFieldBean.getChatBotResourceID() == 0)
                    uiFormFieldResponseTO.setChatBotResourceID(null);
                else
                    uiFormFieldResponseTO.setChatBotResourceID(uiFormFieldBean.getChatBotResourceID());
                if (uiFormFieldBean.getChatBotSequence() == 0)
                	 uiFormFieldResponseTO.setChatBotSequence(null);
                else
                     uiFormFieldResponseTO.setChatBotSequence(uiFormFieldBean.getChatBotSequence());
                
            	boolean checkBotSequence = uiFormService.checkBotSequenceExists(uiFormFieldBean.getFieldGroupID(),uiFormFieldBean.getChatBotSequence());
                if(!checkBotSequence)
                {
   	            clientExists = uiFormService.clientExists(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFormFieldID());
   	
   	            if (!clientExists) {
   	            	            	            	
   	                if (uiFormFieldResponseTO.getChatBotResourceID() != null) {
   	                    for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
   	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                        Integer chatBotResourceID = uiFormFieldResponseTO.getChatBotResourceID();
   	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, chatBotResourceID);
   	                        
   	                        String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
   	                        String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
   	                        
   	                        if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
   	                        	chatBotTextBoxData[i] = chatBotTextBoxData[0];
   	                        }
   	                        if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
   	                        	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
   	                        }
   	                        
   	                        
   	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
   	                        {
   	
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, chatBotTextBoxDataTitle[i]);
   	                        } else {
   	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, checkCombinationResult,
   	                            		chatBotTextBoxDataTitle[i]);
   	
   	                        }
   	
   	                    }
   	                    
   	                }
   	
   	                else {
   	                    StringBuffer clientIdtoSet1 = new StringBuffer(uiFormFieldBean.getClientID());
   	                    clientIdtoSet1.append("_CHATBOT");
   	                    Integer bundleId = 1;
   	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet1.toString());
   	                    if (sysResourceResult) {
   	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet1.toString());
   	                        uiFormFieldResponseTO.setChatBotResourceID(resourceIdToPutInSysResourceBundle);
   	                        for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
   	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                            String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
   	                            String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
   	                            
   	                            if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
   	                            	chatBotTextBoxData[i] = chatBotTextBoxData[0];
   	                            }
   	                            if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
   	                            	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
   	                            }
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], resourceIdToPutInSysResourceBundle,
   	                            		chatBotTextBoxDataTitle[i]);//same as inserted into in lines above code of IF
   	
   	                        }
   	                        
   	
   	                    }
   	                }
   	
   	                if (uiFormFieldResponseTO.getResourceID() != null) {
   	                    for (int i = 0; i < NumberOfTextBoxes; i++) {
   	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
   	                        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
   	                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
   	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
   	                        if (textBoxData[i] == null || textBoxData[i].equals("")) {
   	                            textBoxData[i] = textBoxData[0];
   	                        }
   	                        if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
   	                            textBoxDataTitle[i] = textBoxDataTitle[0];
   	                        }
   	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
   	                        {
   	
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
   	                        } else {
   	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
   	                                    textBoxDataTitle[i]);
   	
   	                        }
   	
   	                    }
   	                    formFieldID = uiFormService.saveFormFieldData(uiFormFieldResponseTO);
   	                }
   	
   	                else {
   	                    String clientIdtoSet = uiFormFieldBean.getClientID();
   	                    Integer bundleId = 1;
   	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
   	                    if (sysResourceResult) {
   	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
   	                        uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
   	                        for (int i = 0; i < NumberOfTextBoxes; i++) {
   	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                            String textBoxData[] = uiFormFieldBean.getSystextBoxes();
   	                            String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
   	                            if (textBoxData[i] == null || textBoxData[i].equals("")) {
   	                                textBoxData[i] = textBoxData[0];
   	                            }
   	                            if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
   	                                textBoxDataTitle[i] = textBoxDataTitle[0];
   	                            }
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceIdToPutInSysResourceBundle,
   	                                    textBoxDataTitle[i]);//same as inserted into in lines above code of IF
   	
   	                        }
   	                        //to insert the new resource id to UIFORMFIELD where it is null in this case which we get in this sysbundleResult.
   	
   	                        formFieldID = uiFormService.saveFormFieldData(uiFormFieldResponseTO);
   	                        Integer formId = uiFormService.getFormID(uiFormFieldResponseTO);
   	
   	                        if ( formFieldID > 0 ) {
   	                            procResult = uiFormService.callProcedure(formId);//calling procedure
   	                        }
   	
   	                    }
   	                }
   	
   	                if (formFieldID > 0 && procResult) {
   	                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Field Data added successfully", ""));
   	                    List<UiFormFieldResponseTO> uiFormControllerTOList = null;
   	                    uiFormControllerTOList = uiFormService.getFormGroupFieldData(uiFormFieldBean.getFieldGroupID());
   	                    //settng data to display it for uiformfield
   	                    uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);
   	                    
   	                	if (uiFormFieldResponseTO.getSysFieldTypeID() != null && uiFormFieldBean.getSelectedSysChatBotValidationTypeList() != null ) {
   	                		SysChatBotValidationTO sysChatBotValidationTO = null;
   	                	    
   	                	    String paramValues = null;
   	                	    if(uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser() != null && !uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser().isEmpty()){
   		                	    for ( ChatBotColumnValidationMapperTO f : uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser()) {
   		                	    	sysChatBotValidationTO = new SysChatBotValidationTO();
   		                	    	//The string builder used to construct the string
   		                	    	StringBuilder commaSepValueBuilder = new StringBuilder();
   		                	    	//Looping through the list
   		                	    	for ( int i = 0; i< f.getListOfParametersInputByUserForSelectedValidation().size(); i++){
   		                	    		
   		                    	      //append the value into the builder
   		                    	      commaSepValueBuilder.append(f.getListOfParametersInputByUserForSelectedValidation().get(i).getParamValues());
   		                    	       
   		                    	      //if the value is not the last element of the list
   		                    	      //then append the comma(,) as well
   		                    	      if ( i != f.getListOfParametersInputByUserForSelectedValidation().size()-1){
   		                    	        commaSepValueBuilder.append(", ");
   		                    	      }
   		                    	      
   		                    	    }
   		                	    	paramValues = commaSepValueBuilder.toString();
   		                	    	sysChatBotValidationTO.setSysValidationTypeID(f.getSysValidationTypeID());
   		                	    	sysChatBotValidationTO.setParamValues(paramValues);
   		                	    	sysChatBotValidationTO.setUiFormFieldID(formFieldID);
   		                		    sysChatBotValidationService.saveSysChatBotValidationType(sysChatBotValidationTO);
   		                		}
   	                		}
   	                	  }
   	                    
   	                    RequestContext.getCurrentInstance().update("updateUIForm");
   	                    RequestContext.getCurrentInstance().execute("PF('UIFormFieldAddModal').hide();");
   	                    RequestContext.getCurrentInstance().execute("scrollUp");
   	                } else {
   	                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Field Data not added", ""));
   	                    RequestContext.getCurrentInstance().update("updateUIForm");
   	                    RequestContext.getCurrentInstance().execute("scrollUp");
   	                }
   	            }
   	
   	            else {
   	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mentioned client name aleardy exists.", ""));
   	            }
   	            
	            }
	            else
		            {
		           	 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bot Sequence aleardy exists.", ""));
		           	 RequestContext.getCurrentInstance().execute("scrollUp");
		            }
	            }
                else{
                	clientExists = uiFormService.clientExists(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFormFieldID());
                	if (uiFormFieldResponseTO.getResourceID() != null) {
   	                    for (int i = 0; i < NumberOfTextBoxes; i++) {
   	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
   	                        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
   	                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
   	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
   	                        if (textBoxData[i] == null || textBoxData[i].equals("")) {
   	                            textBoxData[i] = textBoxData[0];
   	                        }
   	                        if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
   	                            textBoxDataTitle[i] = textBoxDataTitle[0];
   	                        }
   	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
   	                        {
   	
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
   	                        } else {
   	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
   	                                    textBoxDataTitle[i]);
   	
   	                        }
   	
   	                    }
   	                    formFieldID = uiFormService.saveFormFieldData(uiFormFieldResponseTO);
   	                }
   	
   	                else {
   	                    String clientIdtoSet = uiFormFieldBean.getClientID();
   	                    Integer bundleId = 1;
   	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
   	                    if (sysResourceResult) {
   	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
   	                        uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
   	                        for (int i = 0; i < NumberOfTextBoxes; i++) {
   	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
   	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
   	                            String textBoxData[] = uiFormFieldBean.getSystextBoxes();
   	                            String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
   	                            if (textBoxData[i] == null || textBoxData[i].equals("")) {
   	                                textBoxData[i] = textBoxData[0];
   	                            }
   	                            if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
   	                                textBoxDataTitle[i] = textBoxDataTitle[0];
   	                            }
   	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceIdToPutInSysResourceBundle,
   	                                    textBoxDataTitle[i]);//same as inserted into in lines above code of IF
   	
   	                        }
   	                        //to insert the new resource id to UIFORMFIELD where it is null in this case which we get in this sysbundleResult.
   	
   	                        formFieldID = uiFormService.saveFormFieldData(uiFormFieldResponseTO);
   	                        Integer formId = uiFormService.getFormID(uiFormFieldResponseTO);
   	
   	                        if ( formFieldID > 0 ) {
   	                            procResult = uiFormService.callProcedure(formId);//calling procedure
   	                        }
   	
   	                    }
   	                    
   	                 if (formFieldID > 0 && procResult) {
    	                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Field Data added successfully", ""));
    	                    List<UiFormFieldResponseTO> uiFormControllerTOList = null;
    	                    uiFormControllerTOList = uiFormService.getFormGroupFieldData(uiFormFieldBean.getFieldGroupID());
    	                    //settng data to display it for uiformfield
    	                    uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);
    	                    RequestContext.getCurrentInstance().update("updateUIForm");
       	                    RequestContext.getCurrentInstance().execute("PF('UIFormFieldAddModal').hide();");
       	                    RequestContext.getCurrentInstance().execute("scrollUp"); 
   	                 } 
   	              else {
 	                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Field Data not added", ""));
 	                    RequestContext.getCurrentInstance().update("updateUIForm");
 	                    RequestContext.getCurrentInstance().execute("scrollUp");
 	                }
   	                }
                }
            
             
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void updateFieldData()
    {
        // TODO Auto-generated method stub
    	
    	UiFormFieldResponseTO uiFormFieldResponseTO = new UiFormFieldResponseTO();
        try {
            boolean showmesg = false;
            boolean clientExists = false;
            uiFormFieldResponseTO.setFieldGroupID(uiFormFieldBean.getFieldGroupID());
            if (uiFormFieldBean.getEntityFieldID() == 0)
                uiFormFieldResponseTO.setEntityFieldID(null);
            else
                uiFormFieldResponseTO.setEntityFieldID(uiFormFieldBean.getEntityFieldID());
            uiFormFieldResponseTO.setOrganizationID(1);
            uiFormFieldResponseTO.setClientID(uiFormFieldBean.getClientID());
            uiFormFieldResponseTO.setTenantID(1);
            uiFormFieldResponseTO.setAction(uiFormFieldBean.isAction());
            uiFormFieldResponseTO.setApprovalRequired(uiFormFieldBean.isApprovalRequired());
            uiFormFieldResponseTO.setAttachmentEnabled(uiFormFieldBean.isAttachmentEnabled());
            uiFormFieldResponseTO.setMaster(uiFormFieldBean.isMaster());
            uiFormFieldResponseTO.setAttachmentRequired(uiFormFieldBean.isAttachmentRequired());
            uiFormFieldResponseTO.setEffectiveDateRequired(uiFormFieldBean.isEffectiveDateRequired());

            if (uiFormFieldBean.getResourceID() == 0 || uiFormFieldBean.getResourceID() == null)
                uiFormFieldResponseTO.setResourceID(null);
            else
                uiFormFieldResponseTO.setResourceID(uiFormFieldBean.getResourceID());

            uiFormFieldResponseTO.setMandatory(uiFormFieldBean.isMandatory());

            if (uiFormFieldBean.getParentFormFieldID() == 0)
                uiFormFieldResponseTO.setParentFormFieldID(null);
            else
                uiFormFieldResponseTO.setParentFormFieldID(uiFormFieldBean.getParentFormFieldID());

            uiFormFieldResponseTO.setRegex(uiFormFieldBean.getRegex());
            uiFormFieldResponseTO.setFormFieldID(uiFormFieldBean.getFormFieldID());
            uiFormFieldResponseTO.setConfigurationValueMobile(uiFormFieldBean.getMobileConfiguration());
            uiFormFieldResponseTO.setConfigurationValueWeb(uiFormFieldBean.getWebConfiguration());
            uiFormFieldResponseTO.setGroupParameter(uiFormFieldBean.isGroupParameter());
            
            if(uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("VIEWABLE")  || uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("EDITABLE_NONMANDATORY") || uiFormFieldBean.getChatBotConfiguration().equalsIgnoreCase("EDITABLE_MANDATORY"))
            {
            	uiFormFieldResponseTO.setConfigurationValueChatBot(uiFormFieldBean.getChatBotConfiguration());
            	uiFormFieldResponseTO.setSysFieldTypeID(uiFormFieldBean.getSysFieldTypeID());
                uiFormFieldResponseTO.setRuleID(uiFormFieldBean.getRuleID());
                int NumberOfChatBotTextBoxes = uiFormFieldBean.getSysChatBotTextBoxes().length;
                if (uiFormFieldBean.getChatBotResourceID() == null || uiFormFieldBean.getChatBotResourceID() == 0)
                    uiFormFieldResponseTO.setChatBotResourceID(null);
                else
                    uiFormFieldResponseTO.setChatBotResourceID(uiFormFieldBean.getChatBotResourceID());
                if (uiFormFieldBean.getChatBotSequence() == 0 || uiFormFieldBean.getChatBotSequence() == null)
               	 uiFormFieldResponseTO.setChatBotSequence(null);
               else
                    uiFormFieldResponseTO.setChatBotSequence(uiFormFieldBean.getChatBotSequence());
            
            if(uiFormFieldBean.getChatBotSequence() != null && uiFormService.getBotSequenceByFormFieldID(uiFormFieldBean.getFormFieldID()) != null && !uiFormFieldBean.getChatBotSequence().equals(uiFormService.getBotSequenceByFormFieldID(uiFormFieldBean.getFormFieldID()).intValue())){
            	boolean checkBotSequence = uiFormService.checkBotSequenceExists(uiFormFieldBean.getFieldGroupID(),uiFormFieldBean.getChatBotSequence());
                if(!checkBotSequence)
                {
    	            if (uiFormFieldResponseTO.getChatBotResourceID() != null) {
    	                for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
    	                    String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
    	                    Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
    	                    Integer chatBotResourceID = uiFormFieldResponseTO.getChatBotResourceID();
    	                    Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, chatBotResourceID);
    	                    
    	                    String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
    	                    String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
    	                    
    	                    if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
    	                    	chatBotTextBoxData[i] = chatBotTextBoxData[0];
    	                    }
    	                    if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
    	                    	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
    	                    }
    	                    
    	                    
    	                    if (checkCombinationResult == 0)// if cmbination does not exist then insert
    	                    {
    	
    	                        boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, chatBotTextBoxDataTitle[i]);
    	                    } else {
    	                        boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, checkCombinationResult,
    	                        		chatBotTextBoxDataTitle[i]);
    	
    	                    }
    	
    	                }
    	                
    	            }
    	
    	            else {
    	                StringBuffer clientIdtoSet1 = new StringBuffer(uiFormFieldBean.getClientID());
    	                clientIdtoSet1.append("_CHATBOT");
    	                Integer bundleId = 1;
    	                boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet1.toString());
    	                if (sysResourceResult) {
    	                    Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet1.toString());
    	                    uiFormFieldResponseTO.setChatBotResourceID(resourceIdToPutInSysResourceBundle);
    	                    for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
    	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
    	                        bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
    	                        String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
    	                        String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
    	                        
    	                        if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
    	                        	chatBotTextBoxData[i] = chatBotTextBoxData[0];
    	                        }
    	                        if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
    	                        	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
    	                        }
    	                        boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], resourceIdToPutInSysResourceBundle,
    	                        		chatBotTextBoxDataTitle[i]);//same as inserted into in lines above code of IF
    	
    	                    }
    	                    
    	
    	                }
    	            }
    	            
    	            clientExists = uiFormService.clientExists(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFormFieldID());
    	
    	            if (!clientExists) {
    	            	
    	            	int NumberOfTextBoxes = uiFormFieldBean.getSystextBoxes().length;
    	                if (uiFormFieldResponseTO.getResourceID() != null) {
    	                    for (int i = 0; i < NumberOfTextBoxes; i++) {
    	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
    	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
    	                        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
    	                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
    	                        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
    	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
    	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
    	                        {
    	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
    	                        } else {
    	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
    	                                    textBoxDataTitle[i]);
    	
    	                        }
    	
    	                    }
    	                    /*showmesg = uiFormService.UpdateFormFieldData(uiFormFieldResponseTO);*/
    	                } else {
    	                    String clientIdtoSet = uiFormFieldBean.getClientID();
    	                    Integer bundleId = 1;
    	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
    	                    if (sysResourceResult) {
    	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
    	                        uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
    	                        for (int i = 0; i < NumberOfTextBoxes; i++) {
    	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
    	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
    	                            String textBoxData[] = uiFormFieldBean.getSystextBoxes();
    	                            Integer resourceID = uiFormFieldResponseTO.getResourceID();
    	                            String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
    	                            Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
    	                            if (checkCombinationResult == 0)// if cmbination does not exist then insert
    	                            {
    	                                boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
    	                            } else {
    	                                boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
    	                                        textBoxDataTitle[i]);   	
    	                            }
    	                        }
    	                        //to insert the new resource id to UIFORMFIELD where it is null in this case which we get in this sysbundleResult.
    	
    	                    }
    	                }
    	                  	
    	            } 
    	            
    	            showmesg = uiFormService.UpdateFormFieldData(uiFormFieldResponseTO);
    	
    	            if (showmesg) {
    	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Field Data saved successfully", ""));
    	                List<UiFormFieldResponseTO> uiFormControllerTOList = null;
    	                List<SysChatBotValidationTO> sysChatBotValidationList = null;
    	                uiFormControllerTOList = uiFormService.getFormGroupFieldData(uiFormFieldBean.getFieldGroupID());
    	                //settng data to display it for uiformfield
    	                uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);
    	                sysChatBotValidationService.deleteSysBotValidation(uiFormFieldBean.getFormFieldID());
    	                if (uiFormFieldResponseTO.getSysFieldTypeID() != null && uiFormFieldBean.getSelectedSysChatBotValidationTypeList() != null ) {
                    		SysChatBotValidationTO sysChatBotValidationTO = null;
                    	    
                    	    String paramValues = null;
                    	    if(uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser() != null && !uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser().isEmpty()){
    	                	    for ( ChatBotColumnValidationMapperTO f : uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser()) {
    	                	    	sysChatBotValidationTO = new SysChatBotValidationTO();
    	                	    	//The string builder used to construct the string
    	                	    	StringBuilder commaSepValueBuilder = new StringBuilder();
    	                	    	//Looping through the list
    	                	    	for ( int i = 0; i< f.getListOfParametersInputByUserForSelectedValidation().size(); i++){
    	                	    		
    	                    	      //append the value into the builder
    	                    	      commaSepValueBuilder.append(f.getListOfParametersInputByUserForSelectedValidation().get(i).getParamValues());
    	                    	       
    	                    	      //if the value is not the last element of the list
    	                    	      //then append the comma(,) as well
    	                    	      if ( i != f.getListOfParametersInputByUserForSelectedValidation().size()-1){
    	                    	        commaSepValueBuilder.append(", ");
    	                    	      }
    	                    	      
    	                    	    }
    	                	    	paramValues = commaSepValueBuilder.toString();
    	                	    	sysChatBotValidationTO.setSysValidationTypeID(f.getSysValidationTypeID());
    	                	    	sysChatBotValidationTO.setParamValues(paramValues);
    	                	    	sysChatBotValidationTO.setUiFormFieldID(uiFormFieldBean.getFormFieldID());
    	                		    sysChatBotValidationService.saveSysChatBotValidationType(sysChatBotValidationTO);
    	                		}
                    		}
                    	  }
    	                
    	                RequestContext.getCurrentInstance().update("updateUIForm");
    	                RequestContext.getCurrentInstance().execute("PF('UIFormFieldUpdateModal').hide()");
    	            } else {
    	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Field Data NOT saved ", ""));
    	                RequestContext.getCurrentInstance().update("updateUIForm");
    	            }
    	           
                }
                
                else{
    	        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bot Sequence aleardy exists.", ""));
    	        	RequestContext.getCurrentInstance().execute("scrollUp");
    	        }
            }
            else{
            	
            	if (uiFormFieldResponseTO.getChatBotResourceID() != null) {
	                for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
	                    String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                    Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                    Integer chatBotResourceID = uiFormFieldResponseTO.getChatBotResourceID();
	                    Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, chatBotResourceID);
	                    
	                    String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
	                    String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
	                    
	                    if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
	                    	chatBotTextBoxData[i] = chatBotTextBoxData[0];
	                    }
	                    if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
	                    	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
	                    }
	                    
	                    if (checkCombinationResult == 0)// if cmbination does not exist then insert
	                    {	
	                        boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, chatBotTextBoxDataTitle[i]);
	                    } else {
	                        boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], chatBotResourceID, checkCombinationResult,
	                        		chatBotTextBoxDataTitle[i]);	
	                    }	
	                }
	             }
	
	            else {
	                StringBuffer clientIdtoSet1 = new StringBuffer(uiFormFieldBean.getClientID());
	                clientIdtoSet1.append("_CHATBOT");
	                Integer bundleId = 1;
	                boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet1.toString());
	                if (sysResourceResult) {
	                    Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet1.toString());
	                    uiFormFieldResponseTO.setChatBotResourceID(resourceIdToPutInSysResourceBundle);
	                    for (int i = 0; i < NumberOfChatBotTextBoxes; i++) {
	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                        bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                        String chatBotTextBoxData[] = uiFormFieldBean.getSysChatBotTextBoxes();
	                        String chatBotTextBoxDataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
	                        
	                        if (chatBotTextBoxData[i] == null || chatBotTextBoxData[i].equals("")) {
	                        	chatBotTextBoxData[i] = chatBotTextBoxData[0];
	                        }
	                        if (chatBotTextBoxDataTitle[i] == null || chatBotTextBoxDataTitle[i].equals("")) {
	                        	chatBotTextBoxDataTitle[i] = chatBotTextBoxDataTitle[0];
	                        }
	                        boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, chatBotTextBoxData[i], resourceIdToPutInSysResourceBundle,
	                        		chatBotTextBoxDataTitle[i]);//same as inserted into in lines above code of IF
	                    }                    
	                }
	            }
	            
	            clientExists = uiFormService.clientExists(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFormFieldID());
	
	            if (!clientExists) {            	
	            	int NumberOfTextBoxes = uiFormFieldBean.getSystextBoxes().length;
	                if (uiFormFieldResponseTO.getResourceID() != null) {
	                    for (int i = 0; i < NumberOfTextBoxes; i++) {
	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
	                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
	                        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
	                        {
	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
	                        } else {
	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
	                                    textBoxDataTitle[i]);
	                        }
	
	                    }
	                } 
	                else {
	                    String clientIdtoSet = uiFormFieldBean.getClientID();
	                    Integer bundleId = 1;
	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
	                    if (sysResourceResult) {
	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
	                        uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
	                        for (int i = 0; i < NumberOfTextBoxes; i++) {
	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                            String textBoxData[] = uiFormFieldBean.getSystextBoxes();
	                            Integer resourceID = uiFormFieldResponseTO.getResourceID();
	                            String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
	                            Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
	                            if (checkCombinationResult == 0)// if cmbination does not exist then insert
	                            {
	                                boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
	                            } else {
	                                boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
	                                        textBoxDataTitle[i]);
	                            }
	                        }
	                    }
	                }
	            } 
	            
	            showmesg = uiFormService.UpdateFormFieldData(uiFormFieldResponseTO);
	
	            if (showmesg) {
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Field Data saved successfully", ""));
	                List<UiFormFieldResponseTO> uiFormControllerTOList = null;
	                List<SysChatBotValidationTO> sysChatBotValidationList = null;
	                uiFormControllerTOList = uiFormService.getFormGroupFieldData(uiFormFieldBean.getFieldGroupID());
	                //settng data to display it for uiformfield
	                uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);
	                sysChatBotValidationService.deleteSysBotValidation(uiFormFieldBean.getFormFieldID());
	                if (uiFormFieldResponseTO.getSysFieldTypeID() != null && uiFormFieldBean.getSelectedSysChatBotValidationTypeList() != null ) {
                		SysChatBotValidationTO sysChatBotValidationTO = null;
                	    
                	    String paramValues = null;
                	    if(uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser() != null && !uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser().isEmpty()){
	                	    for ( ChatBotColumnValidationMapperTO f : uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser()) {
	                	    	sysChatBotValidationTO = new SysChatBotValidationTO();
	                	    	//The string builder used to construct the string
	                	    	StringBuilder commaSepValueBuilder = new StringBuilder();
	                	    	//Looping through the list
	                	    	for ( int i = 0; i< f.getListOfParametersInputByUserForSelectedValidation().size(); i++){
	                	    		
	                    	      //append the value into the builder
	                    	      commaSepValueBuilder.append(f.getListOfParametersInputByUserForSelectedValidation().get(i).getParamValues());
	                    	       
	                    	      //if the value is not the last element of the list
	                    	      //then append the comma(,) as well
	                    	      if ( i != f.getListOfParametersInputByUserForSelectedValidation().size()-1){
	                    	        commaSepValueBuilder.append(", ");
	                    	      }
	                    	      
	                    	    }
	                	    	paramValues = commaSepValueBuilder.toString();
	                	    	sysChatBotValidationTO.setSysValidationTypeID(f.getSysValidationTypeID());
	                	    	sysChatBotValidationTO.setParamValues(paramValues);
	                	    	sysChatBotValidationTO.setUiFormFieldID(uiFormFieldBean.getFormFieldID());
	                		    sysChatBotValidationService.saveSysChatBotValidationType(sysChatBotValidationTO);
	                		}
                		}
                	  }
	                
	                RequestContext.getCurrentInstance().update("updateUIForm");
	                RequestContext.getCurrentInstance().execute("PF('UIFormFieldUpdateModal').hide()");
	            } 
	            else {
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Field Data NOT saved ", ""));
	                RequestContext.getCurrentInstance().update("updateUIForm");
	            }
	           
            }
            }
            
            else{
            	
            	clientExists = uiFormService.clientExists(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFormFieldID());
            	if (!clientExists) {
	            	
	            	int NumberOfTextBoxes = uiFormFieldBean.getSystextBoxes().length;
	                if (uiFormFieldResponseTO.getResourceID() != null) {
	                    for (int i = 0; i < NumberOfTextBoxes; i++) {
	                        String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                        Integer bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
	                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
	                        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
	                        Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
	                        if (checkCombinationResult == 0)// if cmbination does not exist then insert
	                        {
	                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
	                        } else {
	                            boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
	                                    textBoxDataTitle[i]);
	
	                        }
	
	                    }
	                    /*showmesg = uiFormService.UpdateFormFieldData(uiFormFieldResponseTO);*/
	                } else {
	                    String clientIdtoSet = uiFormFieldBean.getClientID();
	                    Integer bundleId = 1;
	                    boolean sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
	                    if (sysResourceResult) {
	                        Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
	                        uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
	                        for (int i = 0; i < NumberOfTextBoxes; i++) {
	                            String nameOFLang = uiFormFieldBean.getSysBundle().get(i).getName();
	                            bundleId = uiFormFieldBean.getSysBundle().get(i).getBundleId();
	                            String textBoxData[] = uiFormFieldBean.getSystextBoxes();
	                            Integer resourceID = uiFormFieldResponseTO.getResourceID();
	                            String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
	                            Integer checkCombinationResult = uiFormService.checkCombinationofBundleIdandResource(bundleId, resourceID);
	                            if (checkCombinationResult == 0)// if cmbination does not exist then insert
	                            {
	                                boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, textBoxDataTitle[i]);
	                            } else {
	                                boolean sysbundleResultmerge = uiFormService.mergeToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceID, checkCombinationResult,
	                                        textBoxDataTitle[i]);   	
	                            }
	                        }
	                        //to insert the new resource id to UIFORMFIELD where it is null in this case which we get in this sysbundleResult.
	                    }
	                }
	                  	
	            } 
	            
	            showmesg = uiFormService.UpdateFormFieldData(uiFormFieldResponseTO);
	            if (showmesg) {
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form Field Data saved successfully", ""));
	                List<UiFormFieldResponseTO> uiFormControllerTOList = null;
	                List<SysChatBotValidationTO> sysChatBotValidationList = null;
	                uiFormControllerTOList = uiFormService.getFormGroupFieldData(uiFormFieldBean.getFieldGroupID());
	                //settng data to display it for uiformfield
	                uiFormFieldBean.setUiFormFieldResponseTOList(uiFormControllerTOList);
	                RequestContext.getCurrentInstance().update("updateUIForm");
	                RequestContext.getCurrentInstance().execute("PF('UIFormFieldUpdateModal').hide()");
	            }
	            else {
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "UI Form Field Data NOT saved ", ""));
	                RequestContext.getCurrentInstance().update("updateUIForm");
	            }   
            }
	    } catch (Exception e) {
            e.printStackTrace();
        }

    }

   public void showFieldData(UiFormFieldResponseTO uiFormFieldResponseTO)
    {
    	List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList = null;
        try {
            String clientID = uiFormFieldResponseTO.getClientID();
            Integer entityFieldID = uiFormFieldResponseTO.getEntityFieldID();
            Integer fieldGroupID = uiFormFieldResponseTO.getFieldGroupID();
            Integer formFieldID = uiFormFieldResponseTO.getFormFieldID();
            Integer parentFormFieldID = uiFormFieldResponseTO.getParentFormFieldID();
            String regex = uiFormFieldResponseTO.getRegex();
            Integer resourceID = uiFormFieldResponseTO.getResourceID();
            Integer chatBotResourceID = uiFormFieldResponseTO.getChatBotResourceID();
            String sysEntityField = uiFormFieldResponseTO.getSysEntityField();
            String sysResourceValue = uiFormFieldResponseTO.getSysResourceValue();
            boolean isAction = uiFormFieldResponseTO.isAction();
            boolean isApprovalRequired = uiFormFieldResponseTO.isApprovalRequired();
            boolean isAttachmentEnabled = uiFormFieldResponseTO.isAttachmentEnabled();
            boolean isAttachmentRequired = uiFormFieldResponseTO.isAttachmentRequired();
            boolean isEffectiveDate = uiFormFieldResponseTO.isEffectiveDateRequired();
            boolean isMandatory = uiFormFieldResponseTO.isMandatory();
            boolean isMaster = uiFormFieldResponseTO.isMaster();
            String configurationValueWeb = uiFormFieldResponseTO.getConfigurationValueWeb();
            String configurationValueMobile = uiFormFieldResponseTO.getConfigurationValueMobile();
            String ConfigurationValueBot = uiFormFieldResponseTO.getConfigurationValueChatBot();
            boolean groupParameter = uiFormFieldResponseTO.isGroupParameter();
            Integer chatBotSequence = uiFormFieldResponseTO.getChatBotSequence();

            uiFormFieldBean.setFieldGroupID(fieldGroupID);
            uiFormFieldBean.setEntityFieldID(entityFieldID);
            //uiFormFieldBean.setOrganizationID(uiFormFieldBean.getOrganizationID());
            uiFormFieldBean.setFormFieldID(formFieldID);
            uiFormFieldBean.setClientID(clientID);
            uiFormFieldBean.setTenantID(1);
            uiFormFieldBean.setAction(isAction);
            uiFormFieldBean.setApprovalRequired(isApprovalRequired);
            uiFormFieldBean.setAttachmentEnabled(isAttachmentEnabled);
            uiFormFieldBean.setMaster(isMaster);
            uiFormFieldBean.setAttachmentRequired(isAttachmentRequired);
            uiFormFieldBean.setEffectiveDateRequired(isEffectiveDate);
            uiFormFieldBean.setResourceID(resourceID);
            uiFormFieldBean.setMandatory(isMandatory);
            uiFormFieldBean.setParentFormFieldID(parentFormFieldID);
            uiFormFieldBean.setRegex(regex);
            uiFormFieldBean.setMobileConfiguration(configurationValueMobile);
            uiFormFieldBean.setWebConfiguration(configurationValueWeb);
            uiFormFieldBean.setChatBotConfiguration(ConfigurationValueBot);
            uiFormFieldBean.setGroupParameter(groupParameter);
            uiFormFieldBean.setSysFieldTypeID(uiFormFieldResponseTO.getSysFieldTypeID());
            uiFormFieldBean.setRuleID(uiFormFieldResponseTO.getRuleID());
            uiFormFieldBean.setChatBotResourceID(chatBotResourceID);
            uiFormFieldBean.setShowValidationDetailDataTable(true);
            uiFormFieldBean.setChatBotSequence(chatBotSequence);
            
            sysChatBotValidationTypeTOList = sysChatBotValidationTypeService.ChatBotValidationTypeDropdown(uiFormFieldResponseTO.getSysFieldTypeID());
            List<SysChatBotValidationTO> sysChatBotValidationList = null;
            List<Integer> SelectedSysChatBotValidationTypeList = new ArrayList<Integer>();
            List<ChatBotColumnValidationMapperTO> chatBotColumnValidationMapperTOList = new ArrayList<ChatBotColumnValidationMapperTO>();
            sysChatBotValidationList = sysChatBotValidationService.getSysChatBotValidationByUiFormFieldID(formFieldID);
            for(SysChatBotValidationTO scbvTO : sysChatBotValidationList){
            	SysChatBotValidationTypeTO validationTypeTO = new SysChatBotValidationTypeTO();
            	ChatBotColumnValidationMapperTO chatBotColumnValidationMapperTO = new ChatBotColumnValidationMapperTO();
            	SelectedSysChatBotValidationTypeList.add(scbvTO.getSysValidationTypeID());
            	chatBotColumnValidationMapperTO.setSysValidationTypeID(scbvTO.getSysValidationTypeID());
            	validationTypeTO = sysChatBotValidationTypeService.findSysChatBotValidationType(scbvTO.getSysValidationTypeID());
            	chatBotColumnValidationMapperTO.setSelectedValidationLabel(validationTypeTO.getLabel());
            	chatBotColumnValidationMapperTO.setRenderParameterInputs(new Boolean(true));
            	String[] animalsArray = scbvTO.getParamValues().split(",");
            	List<SysChatBotValidationTO> chatBotValidationTOList = new ArrayList<SysChatBotValidationTO>();
    			for (int i=0; i < validationTypeTO.getNoOfParameters(); i++){
    				SysChatBotValidationTO sCBVTO = new SysChatBotValidationTO();
    				sCBVTO.setSysValidationID(scbvTO.getSysValidationID());
    				sCBVTO.setParamValues(animalsArray[i]);
    				sCBVTO.setSysValidationTypeID(scbvTO.getSysValidationTypeID());
    				chatBotValidationTOList.add(sCBVTO);
				}
    			
    			chatBotColumnValidationMapperTO.setListOfParametersInputByUserForSelectedValidation(chatBotValidationTOList);
    			chatBotColumnValidationMapperTOList.add(chatBotColumnValidationMapperTO);
            }
            
            uiFormFieldBean.setSelectedSysChatBotValidationTypeList(SelectedSysChatBotValidationTypeList);
            uiFormFieldBean.setValidationsSelectedAtColumnLevelBYUser(chatBotColumnValidationMapperTOList);
            
                       
            if (isAction) {
                getConfigurationValueForActionField();
            } else {
                getConfigurationValue();
            }

            List<SysResourceBundleNewResponseTO> resourceBundleResponseTO = uiFormService.getSysBundleDataForTextBox(resourceID);
            String textbox[] = new String[resourceBundleResponseTO.size()];
            String titleData[] = new String[resourceBundleResponseTO.size()];
            int j = 0;
            for (SysResourceBundleNewResponseTO i : resourceBundleResponseTO) {
                textbox[j] = i.getValue();
                titleData[j] = i.getTitle();
                j++;
            }
            List<SysResourceBundleNewResponseTO> chatBotResourceBundleResponseTO = uiFormService.getSysBundleDataForTextBox(chatBotResourceID);
            String chatBotTextbox[] = new String[chatBotResourceBundleResponseTO.size()];
            String chatBotTitleData[] = new String[chatBotResourceBundleResponseTO.size()];
            int k = 0;
            for (SysResourceBundleNewResponseTO i : chatBotResourceBundleResponseTO) {
            	chatBotTextbox[k] = i.getValue();
            	chatBotTitleData[k] = i.getTitle();
                k++;
            }
            try {
                List<UiFormFieldResponseTO> uiParentFormFieldTOList = uiFormService.getParentFormFieldData(fieldGroupID);
                List<UiFormFieldResponseTO> uiSysResourceResponseTOList = uiFormService.getSysResourceData();
                List<UiFormFieldResponseTO> uiSysEntityFieldResponseTOList = uiFormService.getEntityFieldData(uiFormFieldBean.getFieldGroupID());
                if (uiParentFormFieldTOList != null) {
                    for (int i = 0; i < uiParentFormFieldTOList.size(); i++) {
                        if (uiParentFormFieldTOList.get(i).getFormFieldID().intValue() == formFieldID.intValue()) {
                            uiParentFormFieldTOList.remove(i);
                        }
                    }
                }
                uiFormFieldBean.setUiParentFormFieldTOList(uiParentFormFieldTOList);
                uiFormFieldBean.setUiSysEntityFieldResponseTOList(uiSysEntityFieldResponseTOList);
                uiFormFieldBean.setUiSysResourceResponseTOList(uiSysResourceResponseTOList);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            getSysBundle();
            uiFormFieldBean.setSystextBoxes(textbox);
            uiFormFieldBean.setSystextBoxesTitle(titleData);
            if(chatBotTextbox != null && chatBotTextbox.length > 0){
            	uiFormFieldBean.setSysChatBotTextBoxes(chatBotTextbox);
                uiFormFieldBean.setSysChatBotTextBoxesTitle(chatBotTitleData);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getSysBundle()
    {

        try {
            SysBundle sysBundle = new SysBundle();
            List<SysBundleResponseTO> sysBundleResponseTOList = null;

            sysBundleResponseTOList = uiFormService.getSysBundle();
            uiFormFieldBean.setSysBundle(sysBundleResponseTOList);
            if (sysBundleResponseTOList != null) {
                int size = sysBundleResponseTOList.size();
                String copydata[] = uiFormFieldBean.getSystextBoxes();
                copydata = new String[size];
                
                String copydataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
                copydataTitle = new String[size];
                
                String botCopydata[] = uiFormFieldBean.getSysChatBotTextBoxes();
                botCopydata = new String[size];
                
                String botCopydataTitle[] = uiFormFieldBean.getSysChatBotTextBoxesTitle();
                botCopydataTitle = new String[size];
                
                
                uiFormFieldBean.setSystextBoxes(copydata);
                uiFormFieldBean.setSysChatBotTextBoxes(botCopydata);
               
                uiFormFieldBean.setSystextBoxesTitle(copydataTitle);
                uiFormFieldBean.setSysChatBotTextBoxesTitle(botCopydataTitle);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void getSysBundleForUiFormGroup()
    {

        try {
            SysBundle SysBundle = new SysBundle();
            List<SysBundleResponseTO> sysBundleResponseTOList = null;

            sysBundleResponseTOList = uiFormService.getSysBundle();
            uIFormBean.setSysBundle(sysBundleResponseTOList);
            if (sysBundleResponseTOList != null) {
                int size = sysBundleResponseTOList.size();
                String copydata[] = uIFormBean.getSystextBoxes();
                copydata = new String[size];
                uIFormBean.setSystextBoxes(copydata);
                String copydataTitle[] = uIFormBean.getSystextBoxesTitle();
                copydataTitle = new String[size];
                uIFormBean.setSystextBoxesTitle(copydataTitle);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getParentFormID(Integer subModuleID)
    {

        try {
            List<UiFormResponseTO> parentFormList = uiFormService.getParentFormListByComType(subModuleID,uIFormBean.getModuleID());
            uIFormBean.setParentFormList(parentFormList);
            /*Integer sequence=0;
            sequence=uiFormService.getMaxSequence(moduleID, uIFormBean.getFormID());
            uIFormBean.setSequence(sequence);*/

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getSequenceFromParenetform(Integer ParentformID)
        throws Exception
    {
        // TODO Auto-generated method stub
        Integer sequence = 0;
        sequence = uiFormService.getMaxSequence(ParentformID, uIFormBean.getFormID());
        uIFormBean.setSequence(sequence);
    }

    public void copyValues()
    {
        int NumberOfTextBoxes = uiFormFieldBean.getSystextBoxes().length;
        String textBoxData[] = uiFormFieldBean.getSystextBoxes();
        String textBoxDataTitle[] = uiFormFieldBean.getSystextBoxesTitle();
        for (int i = 0; i < NumberOfTextBoxes; i++) {
            if (textBoxData[i] == null || textBoxData[i].equals("")) {
                textBoxData[i] = textBoxData[0];
            }
            if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
                textBoxDataTitle[i] = textBoxDataTitle[0];
            }


        }
        uiFormFieldBean.setSystextBoxes(textBoxData);
        uiFormFieldBean.setSystextBoxesTitle(textBoxDataTitle);
    }

    private void getResourceBundelData(Integer resourseId)
    {
        // TODO Auto-generated method stub
        try {

            List<SysResourceBundleNewResponseTO> resourceBundleResponseTO = uiFormService.getSysBundleDataForTextBox(resourseId);
            String textbox[] = new String[resourceBundleResponseTO.size()];
            String titleData[] = new String[resourceBundleResponseTO.size()];
            int j = 0;
            for (SysResourceBundleNewResponseTO i : resourceBundleResponseTO) {
                textbox[j] = i.getValue();
                titleData[j] = i.getTitle();
                j++;
            }
            getSysBundle();
            uiFormFieldBean.setSystextBoxes(textbox);
            uiFormFieldBean.setSystextBoxesTitle(titleData);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public void redirectToUploadPage(Integer formId)
    {
        try {
            uploadFieldsBean.setFormId(formId);
            uploadFieldsBean.setSummaryList(null);
            uploadFieldsBean.setMasterFormDataList(null);
            menuBean.setPage("../UIForm/UploadFields.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void downloadTemplate()
    {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("UIFORMFIELD");
        HSSFSheet hidden = workbook.createSheet("hidden");
        HSSFSheet hidden1 = workbook.createSheet("hidden1");
        HSSFFont boldFont = workbook.createFont();
        boldFont.setFontHeightInPoints((short) 9);
        boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // Setting Bold font
        HSSFCellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setFont(boldFont); // Attaching the font to the Style
        try {
            int coloumnsfromDB = 0;
            String bundleNameTitle[] = null;
            SysBundle SysBundle = new SysBundle();
            List<SysBundleResponseTO> sysBundleResponseTOList = null;
            sysBundleResponseTOList = uiFormService.getSysBundle();
            if (sysBundleResponseTOList != null) {
                coloumnsfromDB = sysBundleResponseTOList.size();
                bundleNameTitle = new String[coloumnsfromDB * 2];
                int k = 0;
                for (int i = 0; i < coloumnsfromDB; i++) {
                    bundleNameTitle[k] = sysBundleResponseTOList.get(i).getName();
                    k = k + 1;
                    bundleNameTitle[k] = sysBundleResponseTOList.get(i).getName() + " Title";
                    k = k + 1;
                }
            }

            int fixedHeadersize = Integer.parseInt(IUIFormConstants.FORM_UPLOAD_PAGE_COLUMNCOUNT);
            Object a[] = new Object[(coloumnsfromDB * 2) + fixedHeadersize];
            Object FixedValuesHeader[] = { "Group Name", "Entity Field", "Client Name", "IsAction", "IsMaster", "IsAttachmentRequired", "IsApprovalRequired",
                    "IsEffectiveDateRequired", "IsMandatory", "IsAttachmentEnabled", "ParentFormFieldName", "Regex" };
            int counter = 0;
            for (int fixedHeadersizes = 0; fixedHeadersizes < fixedHeadersize; fixedHeadersizes++) {
                a[fixedHeadersizes] = FixedValuesHeader[fixedHeadersizes];
                counter++;
            }
            int kk = 0;
            for (int j = counter; j < a.length; j++) {
                a[counter] = bundleNameTitle[kk];
                kk = kk + 1;
                counter = counter + 1;
            }
            HSSFRow row1 = sheet.createRow(0);
            for (int i = 0; i < a.length; i++) {
                // Firstly, add the column headings to the main sheet.
                HSSFCell cell1 = row1.createCell(i);
                cell1.setCellStyle(boldStyle);
                cell1.setCellValue((String) a[i]);

            }
            CellRangeAddressList addressList = new CellRangeAddressList(1, 1000, 3, 9);
            DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(new String[] { "TRUE", "FALSE" });
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
            dataValidation.setSuppressDropDownArrow(false);
            sheet.addValidationData(dataValidation);

            //to get group name data and give constaraint to sheet
            Integer formId = uploadFieldsBean.getFormId();
            Map<String, Integer> groupmap = uiFormService.getGroupNameForExcel(formId);
            String formName = uiFormService.getFormNameForExcel(formId);
            Object[] groupNames = groupmap.keySet().toArray();
            Map<String, Integer> map = uiFormService.getEntityNameForExcel(formId);
            Object[] entityNames = map.keySet().toArray();


            String groupNamearray[] = null;
            if (groupNames != null) {
                groupNamearray = new String[groupNames.length];
                for (int y = 0; y < groupNames.length; y++) {
                    groupNamearray[y] = groupNames[y].toString();
                }
            } else {
                groupNamearray[0] = ""; // in case no data for groupname dropdown will show blank value
            }

            String entityNamesarray[] = null;
            if (entityNames != null) {
                entityNamesarray = new String[entityNames.length];
                for (int y = 0; y < entityNames.length; y++) {
                    entityNamesarray[y] = entityNames[y].toString();

                }
            } else {
                entityNamesarray[0] = ""; // in case no data for groupname dropdown will show blank value
            }
            if (entityNames.length == 0) {
                entityNamesarray = new String[1];
                entityNamesarray[0] = "";
            }

            if (groupNames.length != 0) {
                for (int i = 0, length = groupNamearray.length; i < length; i++) {
                    String name = groupNamearray[i];
                    HSSFRow row = hidden.createRow(i);
                    HSSFCell cell = row.createCell(0);
                    cell.setCellValue(name);
                }
                Name namedCell = workbook.createName();
                namedCell.setNameName("hidden");
                namedCell.setRefersToFormula("hidden!$A$1:$A$" + groupNamearray.length);
                DVConstraint constraintgroupNamearray = DVConstraint.createFormulaListConstraint("hidden");
                CellRangeAddressList addressListgroupNamearray = new CellRangeAddressList(1, 1000, 0, 0);
                HSSFDataValidation validation = new HSSFDataValidation(addressListgroupNamearray, constraintgroupNamearray);
                workbook.setSheetHidden(1, true);
                sheet.addValidationData(validation);

                for (int i = 0, length = entityNamesarray.length; i < length; i++) {
                    String name = entityNamesarray[i];
                    HSSFRow row = hidden1.createRow(i);
                    HSSFCell cell = row.createCell(1);
                    cell.setCellValue(name);
                }
                Name namedCellentityNamesarray = workbook.createName();
                if (entityNamesarray.length != 0) {
                    namedCellentityNamesarray.setNameName("hidden1");
                    namedCellentityNamesarray.setRefersToFormula("hidden1!$B$1:$B$" + entityNamesarray.length);
                    DVConstraint constraintentityNamesarray = DVConstraint.createFormulaListConstraint("hidden1");
                    CellRangeAddressList addressListentityNamesarray = new CellRangeAddressList(1, 1000, 1, 1);
                    HSSFDataValidation validationentityNamesarray = new HSSFDataValidation(addressListentityNamesarray, constraintentityNamesarray);
                    workbook.setSheetHidden(2, true);
                    sheet.addValidationData(validationentityNamesarray);
                }

                try {
                    FacesContext fc = FacesContext.getCurrentInstance();
                    ExternalContext ec = fc.getExternalContext();
                    ec.responseReset();
                    ec.setResponseContentType("application/vnd.ms-excel");
                    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"UiFormField_" + formName + ".xls\"");
                    workbook.write(ec.getResponseOutputStream());
                    ec.getResponseOutputStream().flush();
                    ec.getResponseOutputStream().close();
                    fc.responseComplete();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Atleast one group should be present to download template."));

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void redirectToGroupPage()
    {
        RequestContext context = null;
        try {
            context = RequestContext.getCurrentInstance();
            // initialize();
            menuBean.setPage("../UIForm/UIFormGroup.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleFileUpload(FileUploadEvent event)
    {

        try {
            boolean isFileValid = false;
            Integer formId = uploadFieldsBean.getFormId();
            String formName = uiFormService.getFormNameForExcel(formId);
            if (formName == null) {

                uploadFieldsBean.setUploadedFile(null);
                uploadFieldsBean.setMasterFormDataList(null);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error occured during file upload. Please try again."));
                RequestContext.getCurrentInstance().update("dataUploadForm");
                RequestContext.getCurrentInstance().execute("scrollUp");
                return;
            }
            if (event.getFile().getFileName().contains("UiFormField_" + formName)) {
                isFileValid = copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
                if (isFileValid) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "", "File with name '" + event.getFile().getFileName() + "' Succesfully Uploaded"));
                    RequestContext.getCurrentInstance().update("dataUploadForm");
                    RequestContext.getCurrentInstance().execute("scrollUp");
                }
            } else {
                uploadFieldsBean.setUploadedFile(null);
                uploadFieldsBean.setMasterFormDataList(null);
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "File with name '" + event.getFile().getFileName() + "' is not the valid file for this form group."));
                RequestContext.getCurrentInstance().update("dataUploadForm");
                RequestContext.getCurrentInstance().execute("scrollUp");
            }

        } catch (Exception ex) {
            uploadFieldsBean.setUploadedFile(null);
            uploadFieldsBean.setMasterFormDataList(null);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error occured during file upload. Please try again."));
            RequestContext.getCurrentInstance().update("dataUploadForm");
            RequestContext.getCurrentInstance().execute("scrollUp");
            ex.printStackTrace();
        }
    }

    public boolean copyFile(String fileName, InputStream in)
    {
        String destinationPath = null;
        Boolean isExcelCorrect = false;

        try {
            destinationPath = getDestinationPath(fileName);
            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(destinationPath));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.close();
            out.flush();
            isExcelCorrect = getFieldListFromExcel(destinationPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isExcelCorrect;
    }

    private String getDestinationPath(String s)
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;
        String delims = "";
        Integer i = 0;

        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            resourceBundle = ResourceBundle.getBundle("alt");
            initialPath = resourceBundle.getString("UiFormField_path");

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception("Invalid path");
            }

            finalPath = initialPath + dateFormat.format(date);
            dir = new File(finalPath);
            dir.mkdir();
            destinationPath = finalPath + File.separator + s;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return destinationPath;
    }


    public boolean getFieldListFromExcel(String file2)
    {
        List<UiFormFieldResponseTO> formFieldList = null;
        UiFormFieldResponseTO form = null;
        FileInputStream fis = null;
        Workbook workbook1 = null;
        int rowNumber = 0;
        RequestContext context = null;
        boolean valid = true;
        List<String> headerList = null;
        String summary = null;
        String cellName = null;
        boolean validate = true;
        List<String> summaryList = null;
        int numberofcells = 0;
        List<SysBundleResponseTO> sysBundleResponseTOList = null;
        int coulmnCount = 0;
        try {

            Integer formId = uploadFieldsBean.getFormId();
            Map<String, Integer> groupmap = uiFormService.getGroupNameForExcel(formId);
            Object[] groupNames = groupmap.keySet().toArray();
            Map<String, Integer> mapEntity = uiFormService.getEntityNameForExcel(formId);
            Object[] entityNames = mapEntity.keySet().toArray();

            context = RequestContext.getCurrentInstance();
            formFieldList = new ArrayList<UiFormFieldResponseTO>();
            fis = new FileInputStream(file2);
            headerList = new ArrayList<String>();

            // Using XSSF for xlsx format, for xls use HSSF

            workbook1 = WorkbookFactory.create(fis);
            int numberOfSheets = workbook1.getNumberOfSheets();
            // looping over each workbook sheet
            for (int i = 0; i < 1; i++) { // it will iterate over only first sheet
                Sheet sheet = workbook1.getSheetAt(i);
                Iterator rowIterator = sheet.iterator();
                Integer GroupIdOfCurrentIteration = null;
                // iterating over each row
                while (rowIterator.hasNext()) {
                    form = new UiFormFieldResponseTO();
                    String sysTextBox[] = null;
                    String sysTextBoxTitle[] = null;
                    summary = null;
                    summaryList = new ArrayList<String>();
                    Row row = (Row) rowIterator.next();
                    if (rowNumber == 0) {
                        numberofcells = row.getLastCellNum();
                        List<SysBundleResponseTO> sysBundleResponseTOList1 = null;
                        sysBundleResponseTOList1 = uiFormService.getSysBundle();
                        int coloumnsfromDB1 = 0;
                        if (sysBundleResponseTOList1 != null) {
                            coloumnsfromDB1 = sysBundleResponseTOList1.size();
                        }
                        if (numberofcells != (Integer.parseInt(IUIFormConstants.FORM_UPLOAD_PAGE_COLUMNCOUNT)) + coloumnsfromDB1 * 2) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "File Format", "is wrong"));
                            valid = false;
                            validate = false;
                            uploadFieldsBean.setMasterFormDataList(null);
                            break;
                        }

                        Iterator<Cell> cellIterator = row.cellIterator();
                        while (cellIterator.hasNext()) {

                            Cell cell = (Cell) cellIterator.next();
                            if (cell.getCellType() != Cell.CELL_TYPE_BLANK && cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                headerList.add(cell.getStringCellValue());
                            } else {
                                headerList.add(IUIFormConstants.FORM_UPLOAD_PAGE_INVALID_HEADER);
                                valid = false;
                                break;
                            }
                            coulmnCount++;
                        }
                        headerList.add(IUIFormConstants.UPLOAD_STATUS);
                        rowNumber++;
                        continue;
                    } else {
                        sysTextBox = new String[(numberofcells - 12) / 2];
                        sysTextBoxTitle = new String[(numberofcells - 12) / 2];
                        Iterator cellIterator = row.cellIterator(); // Iterating over each cell (column wise) in a particular row.
                        while (cellIterator.hasNext()) {
                            Cell cell = (Cell) cellIterator.next();
                            try { // Cell with index 0 contains group name

                                Integer entityId = null;
                                if (cell.getColumnIndex() == 0) {
                                    cellName = FormField.GROUPNAME.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        GroupIdOfCurrentIteration = groupmap.get(cell.getStringCellValue());
                                        form.setFieldGroupID(GroupIdOfCurrentIteration);
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                // Cell with index 1 contains Grade Description
                                else if (cell.getColumnIndex() == 1) {
                                    cellName = FormField.ENTITYFIELD.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        //here in excel entity name is there we have to get entity id on the bais of entity name .
                                        form.setSysEntityField(cell.getStringCellValue());
                                        entityId = mapEntity.get(cell.getStringCellValue());
                                        form.setEntityFieldID(entityId);
                                        // form.setEntityFieldID(cell.getStringCellValue().in);
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                // Cell with index 2 contains Grade Unique Code

                                else if (cell.getColumnIndex() == 2) {
                                    cellName = FormField.CLIENTNAME.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        //we have to get the clint id and client name
                                        try {
                                            form.setClientID(cell.getStringCellValue());
                                        } catch (Exception e) {
                                            Double getI = cell.getNumericCellValue();
                                            int toint = getI.intValue();
                                            form.setClientID(String.valueOf(toint));
                                        }
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 3) {
                                    cellName = FormField.ISACTION.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setAction((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } /*else if (cell.getColumnIndex() == 4) {
                                    cellName = FormField.ISCUSTOMFIELDREQUIRED.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setCutomfieldRequired((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                  }*/else if (cell.getColumnIndex() == 4) {
                                    cellName = FormField.ISMASTER.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setMaster((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 5) {
                                    cellName = FormField.ISATTACHMENTREQUIRED.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setAttachmentRequired((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 6) {
                                    cellName = FormField.ISAPPROVALREQUIRED.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setApprovalRequired((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 7) {
                                    cellName = FormField.ISEFFECTIVEDATEREQUIRED.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setEffectiveDateRequired((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 8) {
                                    cellName = FormField.ISMANDATORY.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setMandatory((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 9) {
                                    cellName = FormField.ISATTACHMENTENABLED.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        form.setAttachmentEnabled((cell.getBooleanCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 10) {
                                    cellName = FormField.PARENTFORMFIELDNAME.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        Integer parentformID = uiFormService.getParentidforExcel(cell.getStringCellValue(), GroupIdOfCurrentIteration);
                                        //here we have to set parent form id on the basis of parent form name and group name
                                        if (parentformID == null) {
                                            validate = false;
                                            summaryList.add(FormField.PARENTFORMFIELDNAME.getFormField() + " does not exist.");
                                        }
                                        form.setParentFormFieldID(parentformID);
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                } else if (cell.getColumnIndex() == 11) {
                                    cellName = FormField.REGEX.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        try {
                                            form.setRegex((cell.getStringCellValue()));
                                        } catch (Exception e) {
                                            Double getI = cell.getNumericCellValue();
                                            int toint = getI.intValue();
                                            form.setRegex((String.valueOf(toint)));
                                        }
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }

                                else if (cell.getColumnIndex() >= 12 && coulmnCount > cell.getColumnIndex()) {
                                    //cellName = FormField.SYSTEXTBOXES.getFormField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        try {
                                            sysTextBox[(cell.getColumnIndex() - 12) / 2] = cell.getStringCellValue();
                                        } catch (Exception ex) {
                                            Double getI = cell.getNumericCellValue();
                                            int toint = getI.intValue();
                                            sysTextBox[(cell.getColumnIndex() - 12) / 2] = String.valueOf(toint);
                                        }
                                        // form.setSystextBoxes((cell.getStringCellValue()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                    if (cellIterator.hasNext()) {
                                        cell = (Cell) cellIterator.next();
                                        if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {

                                            try {
                                                sysTextBoxTitle[((cell.getColumnIndex() - 12) / 2)] = cell.getStringCellValue();
                                            } catch (Exception ex) {
                                                Double getI = cell.getNumericCellValue();
                                                int toint = getI.intValue();
                                                sysTextBoxTitle[(cell.getColumnIndex() - 12) / 2] = String.valueOf(toint);
                                            }

                                        } else {
                                            validate = false;
                                            summaryList.add(cellName + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                                        }
                                    }
                                }


                            } catch (IllegalStateException npe) {
                                validate = false;
                                summaryList.add(IUIFormConstants.FORM_UPLOAD_PAGE_INVALID_COLUMN_TYPE + " some columns." + cellName);

                            }

                            rowNumber++;
                        }

                        form.setSystextBoxes(sysTextBox);
                        form.setSystextBoxesTitle(sysTextBoxTitle);

                        if (summaryList.size() == 0) {
                            summary = IUIFormConstants.FORM_UPLOAD_PAGE_VALID_RECORD;
                        } else {
                            summary = IUIFormConstants.FORM_UPLOAD_PAGE_INVALID_RECORD;
                        }

                        form.setSummary(summary);
                        form.setSummaryList(summaryList);
                        sysBundleResponseTOList = uiFormService.getSysBundle();
                        form.setSysBundle(sysBundleResponseTOList);
                        formFieldList.add(form);
                    }

                    uploadFieldsBean.setListToFromExcel(formFieldList);

                }
            }

            if (valid) {
                uiFormFieldResponseTO = new UiFormFieldResponseTO();
                uiFormFieldResponseTO.setExcelHeaderList(headerList);
                if (formFieldList.isEmpty() || formFieldList.equals("") || formFieldList == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IUIFormConstants.MASTER_FORM_UPLOAD_EMPTY_SHEET, ""));
                    validate = false;
                    uploadFieldsBean.setMasterFormDataList(null);
                    RequestContext.getCurrentInstance().update("dataUploadForm");
                    RequestContext.getCurrentInstance().execute("scrollUp");

                } else {
                    boolean status = false;
                    boolean success = true;

                    boolean checkStatus = checkForMissingFields(formFieldList);
                    if (checkStatus) {
                        for (UiFormFieldResponseTO excelObject : formFieldList) {
                            status = saveFormFieldbyExcel(excelObject);
                            if (!status) {
                                excelObject.setSummary(uploadFieldsBean.getCopyMasterFormDataList().getSummary());
                                excelObject.setSummaryList(uploadFieldsBean.getCopyMasterFormDataList().getSummaryList());
                                success = false;
                            }
                        }

                        uploadFieldsBean.setSuccess(success);

                        boolean procResult = uiFormService.callProcedure(formId);//calling procedure

                        if (success && procResult) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IUIFormConstants.MASTER_FORM_UPLOAD_SUCCESS_MSG, ""));
                            uploadFieldsBean.setUploadedFile(null);
                            uploadFieldsBean.setMasterFormDataList(null);
                            RequestContext.getCurrentInstance().update("dataUploadForm");
                            RequestContext.getCurrentInstance().execute("scrollUp");
                        } else {
                            uploadFieldsBean.setMasterFormDataList(formFieldList);
                            FacesContext.getCurrentInstance().addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR, IUIFormConstants.MASTER_FORM_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
                            RequestContext.getCurrentInstance().update("dataUploadForm");
                            RequestContext.getCurrentInstance().execute("scrollUp");
                            validate = false;
                        }


                    } else {
                        uploadFieldsBean.setMasterFormDataList(formFieldList);
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, IUIFormConstants.MASTER_FORM_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
                        RequestContext.getCurrentInstance().update("dataUploadForm");
                        RequestContext.getCurrentInstance().execute("scrollUp");
                        validate = false;
                    }
                }


            }
        } catch (IOException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IUIFormConstants.MASTER_FORM_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
            e.printStackTrace();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IUIFormConstants.MASTER_FORM_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
            ex.printStackTrace();
        }
        return validate;
    }

    private boolean checkForMissingFields(List<UiFormFieldResponseTO> formList)
    {
        boolean validate = true;
        try {
            for (UiFormFieldResponseTO form : formList) {


                if (form.getSystextBoxes().length != 0) {
                    for (int hh = 0; hh < form.getSystextBoxes().length; hh++) {
                        if (form.getSystextBoxes()[0] == null) {
                            form.setSummary("Invalid Record");
                            form.getSummaryList().add(FormField.SYSTEXTBOXES.getFormField() + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                            validate = false;
                        }
                        break;
                    }

                }
                if (form.getSystextBoxesTitle().length != 0) {

                    for (int hh = 0; hh < form.getSystextBoxesTitle().length; hh++) {
                        if (form.getSystextBoxesTitle()[0] == null) {
                            form.setSummary("Invalid Record");
                            form.getSummaryList().add(FormField.SYSTEXTBOXESTITLE.getFormField() + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                            validate = false;
                        }
                        break;
                    }
                }

                if (form.getFieldGroupID() == null) {
                    form.setSummary("Invalid Record");
                    form.getSummaryList().add(FormField.GROUPNAME.getFormField() + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                    validate = false;
                }

                if (form.getClientID() == null) {
                    form.setSummary("Invalid Record");
                    form.getSummaryList().add(FormField.CLIENTNAME.getFormField() + IUIFormConstants.FORM_UPLOAD_PAGE_EMPTY_COLUMN);
                    validate = false;
                }

                boolean clientExists = uiFormService.clientExistsExcel(form.getClientID(), form.getFieldGroupID());

                if (clientExists) {
                    form.setSummary("Invalid Record");
                    form.getSummaryList().add(" Client named " + form.getClientID() + " " + "  is already exists ");
                    validate = false;

                }


            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return validate;
    }


    public boolean saveFormFieldbyExcel(UiFormFieldResponseTO uiFormFieldBeanByExcel)
    {
        // TODO Auto-generated method stub
        try {
            boolean showmesg = false;
            boolean clientExists = false;
            int formFieldID = 0;
            uiFormFieldResponseTO.setSummary(uiFormFieldBeanByExcel.getSummary());
            uiFormFieldResponseTO.setSummaryList(uiFormFieldBeanByExcel.getSummaryList());
            uiFormFieldResponseTO.setFieldGroupID(uiFormFieldBeanByExcel.getFieldGroupID());
            uiFormFieldResponseTO.setEntityFieldID(uiFormFieldBeanByExcel.getEntityFieldID());
            uiFormFieldResponseTO.setOrganizationID(1);
            if (uiFormFieldBeanByExcel.getClientID() == null) {
                uiFormFieldResponseTO.setSummary("Invalid Record");
                // uiFormFieldResponseTO.getSummaryList().add("Client Name is blank, Invalid data ");
                uploadFieldsBean.setCopyMasterFormDataList(uiFormFieldResponseTO);
                return false;
            }
            uiFormFieldResponseTO.setClientID(uiFormFieldBeanByExcel.getClientID());
            uiFormFieldResponseTO.setTenantID(1);
            if (uiFormFieldBeanByExcel.getEntityFieldID() == null)
                uiFormFieldResponseTO.setEntityFieldID(null);
            else
                uiFormFieldResponseTO.setEntityFieldID(uiFormFieldBeanByExcel.getEntityFieldID());
            uiFormFieldResponseTO.setAction(uiFormFieldBeanByExcel.isAction());
            uiFormFieldResponseTO.setApprovalRequired(uiFormFieldBeanByExcel.isApprovalRequired());
            uiFormFieldResponseTO.setAttachmentEnabled(uiFormFieldBeanByExcel.isAttachmentEnabled());
            uiFormFieldResponseTO.setMaster(uiFormFieldBeanByExcel.isMaster());
            uiFormFieldResponseTO.setAttachmentRequired(uiFormFieldBeanByExcel.isAttachmentRequired());
            uiFormFieldResponseTO.setEffectiveDateRequired(uiFormFieldBeanByExcel.isEffectiveDateRequired());
            if (uiFormFieldBeanByExcel.getResourceID() == null || uiFormFieldBeanByExcel.getResourceID() == 0)
                uiFormFieldResponseTO.setResourceID(null);
            else
                uiFormFieldResponseTO.setResourceID(uiFormFieldBeanByExcel.getResourceID());
            uiFormFieldResponseTO.setMandatory(uiFormFieldBeanByExcel.isMandatory());
            if (uiFormFieldBeanByExcel.getParentFormFieldID() == null)
                uiFormFieldResponseTO.setParentFormFieldID(null);
            else
                uiFormFieldResponseTO.setParentFormFieldID(uiFormFieldBeanByExcel.getParentFormFieldID());
            uiFormFieldResponseTO.setRegex(uiFormFieldBeanByExcel.getRegex());
            uiFormFieldResponseTO.setFormFieldID(uiFormFieldBeanByExcel.getFormFieldID());
            int NumberOfTextBoxes = uiFormFieldBeanByExcel.getSystextBoxes().length;


            clientExists = uiFormService.clientExistsExcel(uiFormFieldResponseTO.getClientID(), uiFormFieldResponseTO.getFieldGroupID());

            if (!clientExists) {

                String clientIdtoSet = uiFormFieldBeanByExcel.getClientID();
                Integer bundleId = 1;
                boolean sysResourceResult = false;
                if (uiFormFieldBeanByExcel.getSystextBoxes().length != 0 && uiFormFieldBeanByExcel.getSystextBoxesTitle().length != 0) {
                    for (int i = 0; i < NumberOfTextBoxes; i++) {
                        String textBoxDataTitle[] = uiFormFieldBeanByExcel.getSystextBoxesTitle();
                        String textBoxData[] = uiFormFieldBeanByExcel.getSystextBoxes();
                        if (textBoxDataTitle[0] != null && textBoxData[0] != null && uiFormFieldResponseTO.getFieldGroupID() != null) {
                            sysResourceResult = uiFormService.insertToSysResource(bundleId, clientIdtoSet);
                            break;
                        }
                    }
                } else {
                    return false;
                }
                if (sysResourceResult) {
                    Integer resourceIdToPutInSysResourceBundle = uiFormService.getresourceIdToPutInSysResourceBundle(bundleId, clientIdtoSet);
                    uiFormFieldResponseTO.setResourceID(resourceIdToPutInSysResourceBundle);
                    for (int i = 0; i < NumberOfTextBoxes; i++) {
                        String nameOFLang = uiFormFieldBeanByExcel.getSysBundle().get(i).getName();
                        bundleId = uiFormFieldBeanByExcel.getSysBundle().get(i).getBundleId();
                        String textBoxData[] = uiFormFieldBeanByExcel.getSystextBoxes();
                        Integer resourceID = uiFormFieldResponseTO.getResourceID();
                        String textBoxDataTitle[] = uiFormFieldBeanByExcel.getSystextBoxesTitle();
                        if (textBoxData[i] == null || textBoxData[i].equals("")) {
                            textBoxData[i] = textBoxData[0];
                        }
                        if (textBoxDataTitle[i] == null || textBoxDataTitle[i].equals("")) {
                            textBoxDataTitle[i] = textBoxDataTitle[0];
                        }

                        if (textBoxDataTitle[0] != null && textBoxData[0] != null) {
                            boolean sysbundleResult = uiFormService.insertToSysResourceBundleNew(nameOFLang, bundleId, textBoxData[i], resourceIdToPutInSysResourceBundle,
                                    textBoxDataTitle[i]);//same as inserted into in lines above code of IF
                        } else {
                            return false;
                        }
                    }
                    //to insert the new resource id to UIFORMFIELD where it is null in this case which we get in this sysbundleResult.
                    if (uiFormFieldResponseTO.getFieldGroupID() != null)
                    	formFieldID = uiFormService.saveFormFieldData(uiFormFieldResponseTO);
                }


                if ( formFieldID > 0) {
                    return true;
                } else {
                    uiFormFieldResponseTO.setSummary("Invalid Record");
                    if (uiFormFieldResponseTO.getClientID() != null) {
                        uiFormFieldResponseTO.getSummaryList().add("Invalid data for " + uiFormFieldResponseTO.getClientID() + " : some error occured");
                    } else {
                        uiFormFieldResponseTO.getSummaryList().add("Invalid data");
                    }
                    uploadFieldsBean.setCopyMasterFormDataList(uiFormFieldResponseTO);
                    return false;
                }
            }

            else {
                uiFormFieldResponseTO.setSummary("Invalid Record");
                uiFormFieldResponseTO.getSummaryList().add(" Client named " + uiFormFieldResponseTO.getClientID() + " " + "  is already exists ");
                uploadFieldsBean.setCopyMasterFormDataList(uiFormFieldResponseTO);
                return false;
            }

        } catch (Exception e) {
            uiFormFieldResponseTO.setSummary("Invalid Record");
            return false;
        }

    }


    public void redirectTosummaryList(List<String> summaryList)
    {
        uploadFieldsBean.setSummaryList(summaryList);

    }


    public void getSubModuleNameList(Integer MoiduleId)
    {
        List<UiFormResponseTO> subModuleList = null;
        List<UiFormResponseTO> parentFormList = null;
        try {
            subModuleList = uiFormService.getSubModuleList(MoiduleId);
            parentFormList = uiFormService.getParentFormListByComType(MoiduleId,uIFormBean.getModuleID());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        uIFormBean.setSubModuleList(subModuleList);
        uIFormBean.setParentFormList(parentFormList);
        RequestContext.getCurrentInstance().update("parentFormName");
    }

    public void changeMenuSequence(Integer menuCategoryNumber)
    {
        // TODO Auto-generated method stub
        if (menuCategoryNumber == 4) {

            uIFormBean.setMenu(false);
            uIFormBean.setSequence(null);
            uIFormBean.setMenuAndSequneceCheck(false);

        } else {
            uIFormBean.setMenuAndSequneceCheck(true);
        }

    }

    @SuppressWarnings("unused")
    public void menuSequence(Integer bundleId)
        throws Exception
    {
        MenuResponseTO menuResponseTO = null;
        MenuModel menuModel = null;
        Integer bundleID = null;
        try {
            bundleID = bundleId;
            menuResponseTO = new MenuResponseTO();
            menuResponseTO = (MenuResponseTO) uiFormService.doLogin(bundleID);
            menuModel = HomeHelper.getMenuModel(menuResponseTO.getMenuMap());
            userSessionBean.setUserMenuMap(menuResponseTO.getMenuMap());
            userSessionBean.setUserMenu1(menuModel);
        } catch (Exception e) {

        }

    }

    @SuppressWarnings("unused")
    public void bundleDropDown()
        throws Exception
    {

        List<SysBundleResponseTO> response = null;
        try {
            response = uiFormService.bundleDropDown();
            uIFormBean.setSysBundle(response);

        } catch (Exception e) {

        }

    }

    public void resetBundleIdDropdown()
    {
        try {
            uIFormBean.setBundleId(null);
            userSessionBean.setUserMenu1(null);
        } catch (Exception e) {

        }
    }

    public List<String> getConfigurationValue()
        throws Exception
    {

        List<String> configurationValuelist = null;
        String configurationValue = null;
        int i = 0;
        try {
            i = 4;
            configurationValuelist = new ArrayList<String>();

            while (i >= 1) {
                if (i == 4) {
                    configurationValue = "DENY";
                }
                if (i == 3) {
                    configurationValue = "VIEWABLE";
                }
                if (i == 2) {
                    configurationValue = "EDITABLE_NONMANDATORY";
                }
                if (i == 1) {
                    configurationValue = "EDITABLE_MANDATORY";
                }

                configurationValuelist.add(configurationValue);
                i--;
            }

            uiFormFieldBean.setConfigurationValuelist(configurationValuelist);

            return configurationValuelist;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    public List<String> getConfigurationValueForActionField()
        throws Exception
    {

        List<String> configurationValuelistForAction = null;
        String configurationValue = null;
        int i = 0;
        try {
            i = 2;
            configurationValuelistForAction = new ArrayList<String>();
            while (i >= 1) {
                if (i == 2) {
                    configurationValue = "DENY";
                }
                if (i == 1) {
                    configurationValue = "VIEWABLE";
                }
                configurationValuelistForAction.add(configurationValue);
                i--;
            }
            uiFormFieldBean.setConfigurationValuelist(configurationValuelistForAction);
            if (uiFormFieldBean.isAction() != true) {
                getConfigurationValue();
            }
            return configurationValuelistForAction;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    public void fieldTypeDropDown()
            throws Exception
        {

            List<SysFieldTypeResponseTO> response = null;
            try {
                response = uiFormService.fieldTypeDropDown();
                uiFormFieldBean.setSysFieldType(response);

            } catch (Exception e) {

            }

        }
    
    public void methodDropDown()
            throws Exception
        {

            List<SysRuleResponseTO> response = null;
            try {
                response = uiFormService.methodDropDown();
                uiFormFieldBean.setSysRule(response);

            } catch (Exception e) {

            }

        }

    public void changeChatBotConfiguration(String chatBotConfiguration) {
    	uiFormFieldBean.setChatBotConfiguration(chatBotConfiguration);
     }
    
    public void changeSysFieldType(Integer sysFieldTypeID) {
    	List<SysChatBotValidationTypeTO> response = null;
        try {
            response = sysChatBotValidationTypeService.ChatBotValidationTypeDropdown(sysFieldTypeID);
            uiFormFieldBean.setSysChatBotValidationTypeList(response);

        } catch (Exception e) {

        }
    	
     }
    
    public void onColumnLevelValidationSelection() throws Exception {

		if( uiFormFieldBean.getSelectedSysChatBotValidationTypeList() == null )
			return;
		else
			uiFormFieldBean.setShowValidationDetailDataTable(new Boolean(true));
		
		List<ChatBotColumnValidationMapperTO> selectedValidationResquestTOList = new ArrayList<ChatBotColumnValidationMapperTO>();
		
		for(Integer value : uiFormFieldBean.getSelectedSysChatBotValidationTypeList()) {
			SysChatBotValidationTypeTO validationTypeTO = new SysChatBotValidationTypeTO();
			
			ChatBotColumnValidationMapperTO chatBotColumnValidationMapperTO = new ChatBotColumnValidationMapperTO();
			validationTypeTO = sysChatBotValidationTypeService.findSysChatBotValidationType(value);
			chatBotColumnValidationMapperTO.setSelectedValidationLabel(validationTypeTO.getLabel() );
			chatBotColumnValidationMapperTO.setRenderParameterInputs(new Boolean(false));
			chatBotColumnValidationMapperTO.setSysValidationTypeID(validationTypeTO.getSysValidationTypeID());
					for (int i=0; i < validationTypeTO.getNoOfParameters(); i++){
						List<SysChatBotValidationTO> tempList = prepareAndFetchValidationParamInputList(
								validationTypeTO.getNoOfParameters(),
								validationTypeTO.getSysValidationTypeID());

						if (tempList != null && !tempList.isEmpty())
							chatBotColumnValidationMapperTO			
									.setListOfParametersInputByUserForSelectedValidation(tempList);

						chatBotColumnValidationMapperTO
								.setRenderParameterInputs(new Boolean(
										true));
					}
					
					selectedValidationResquestTOList
					.add(chatBotColumnValidationMapperTO);

					if (!selectedValidationResquestTOList.isEmpty())
						uiFormFieldBean
								.setValidationsSelectedAtColumnLevelBYUser(selectedValidationResquestTOList);
		}


	}

	public List<SysChatBotValidationTO> prepareAndFetchValidationParamInputList(
			Integer noOfParameters, Integer sysValidationID) throws Exception {

		if (noOfParameters == null || noOfParameters <= 0)
			return null;

		List<SysChatBotValidationTO> sysChatBotValidationTOList = null;
		SysChatBotValidationTO sysChatBotValidationTOObj = null;
		try {
			sysChatBotValidationTOList = new ArrayList<SysChatBotValidationTO>();
			for (int i = 0; i < noOfParameters; i++) {
				int j = i;
				sysChatBotValidationTOObj = new SysChatBotValidationTO();
				sysChatBotValidationTOObj.setSysValidationTypeID(sysValidationID);
						
				sysChatBotValidationTOObj.setSequence(++j);
				sysChatBotValidationTOList.add(sysChatBotValidationTOObj);
			}
		} catch (Exception ex) {
			sysChatBotValidationTOList = null;
			throw ex;
		}

		return sysChatBotValidationTOList;
	}
    
	public void saveValidationParameter()
			throws Exception {
		try {
			uiFormFieldBean.setShowValidationDetailDataTable(new Boolean(false));
			uiFormFieldBean.getValidationsSelectedAtColumnLevelBYUser();
		} catch (Exception ex) {
			throw ex;
		}
	}

}
