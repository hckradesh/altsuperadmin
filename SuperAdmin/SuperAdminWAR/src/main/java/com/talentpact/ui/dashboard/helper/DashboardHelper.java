package com.talentpact.ui.dashboard.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.ui.dashboard.bean.DashBoardBean;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.dashboard.transport.output.ModuleTO;


public class DashboardHelper {

	public static void assigModuleNameAndColumn(
			List<DashBoardTO> dashBoardTOList, List<ModuleTO> moduleList, DashBoardBean dashBoardBean)
	{
		   Map<Integer ,ModuleTO > moduleMap = new HashMap<Integer, ModuleTO>();
		   
		   for(ModuleTO to : moduleList){
			    moduleMap.put(to.getModuleID() , to);
		   }
		   for(DashBoardTO dto :dashBoardTOList)
		   {
			    Integer moduleId = dto.getModuleID();
			    if(moduleMap.containsKey(moduleId)){
			    	ModuleTO tpr = moduleMap.get(moduleId);
			    	if(tpr.getParentModuleID()==null){
				    	  	dto.setParentModuleName(tpr.getModuleName());
				    	  	dto.setParentModuleID(tpr.getModuleID());
				    	  	dto.setColumnPositionName(dashBoardBean.getColumnpositionmap().get(dto.getColumnPosition()));
			    	}else{
				    		ModuleTO pModuleTO =  moduleMap.get(tpr.getParentModuleID());
				    		dto.setParentModuleID(pModuleTO.getModuleID());
				    		dto.setParentModuleName(pModuleTO.getModuleName());
				    		dto.setModuleID(tpr.getModuleID());
				    		dto.setModuleName(tpr.getModuleName());
				    		dto.setColumnPositionName(dashBoardBean.getColumnpositionmap().get(dto.getColumnPosition()));
			    	}
			    }
			     
			   
		   }
	}

	public static void filteredModuleList(DashBoardBean dashBoardBean,
			List<ModuleTO> moduleList)
	{
		List<ModuleTO> parentMenuList = new ArrayList<ModuleTO>();
		List<ModuleTO> allChildMenuList = new ArrayList<ModuleTO>();
		for(ModuleTO mto :moduleList)
		{
			  Integer parentModuleId = mto.getParentModuleID();
			  if(parentModuleId==null)
			  {
				  parentMenuList.add(mto);
			  }else{
				  allChildMenuList.add(mto);
			  }
		}
		dashBoardBean.setParentModuleList(parentMenuList);
		dashBoardBean.setAllChildModuleList(allChildMenuList);
		
            		
	}
   
}
