/**
 * 
 */
package com.talentpact.ui.SysGuidedTourField;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysGuidedTourFieldResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysGuidedTourFieldBean")
@ConversationScoped
public class SysGuidedTourFieldBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 199876598978L;

    @Inject
    SysGuidedTourFieldController sysGuidedTourFieldController;

    private Integer guidedTourFieldId;

    private Integer sysFormId;

    private String selector;

    private String clickPath;

    private Boolean combine;

    private String sysForm;


    private List<SysGuidedTourFieldResponseTO> sysGuidedTourFieldResponseTOList;

    private List<SysGuidedTourFieldResponseTO> uiFormList;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysGuidedTourFieldController.initialize();
        } catch (Exception e) {
        }
    }

    public Integer getGuidedTourFieldId()
    {
        return guidedTourFieldId;
    }

    public void setGuidedTourFieldId(Integer guidedTourFieldId)
    {
        this.guidedTourFieldId = guidedTourFieldId;
    }

    public Integer getSysFormId()
    {
        return sysFormId;
    }

    public void setSysFormId(Integer sysFormId)
    {
        this.sysFormId = sysFormId;
    }

    public String getSelector()
    {
        return selector;
    }

    public void setSelector(String selector)
    {
        this.selector = selector;
    }

    public String getClickPath()
    {
        return clickPath;
    }

    public void setClickPath(String clickPath)
    {
        this.clickPath = clickPath;
    }

    public Boolean getCombine()
    {
        return combine;
    }

    public void setCombine(Boolean combine)
    {
        this.combine = combine;
    }

    public String getSysForm()
    {
        return sysForm;
    }

    public void setSysForm(String sysForm)
    {
        this.sysForm = sysForm;
    }

    public List<SysGuidedTourFieldResponseTO> getSysGuidedTourFieldResponseTOList()
    {
        return sysGuidedTourFieldResponseTOList;
    }

    public void setSysGuidedTourFieldResponseTOList(List<SysGuidedTourFieldResponseTO> sysGuidedTourFieldResponseTOList)
    {
        this.sysGuidedTourFieldResponseTOList = sysGuidedTourFieldResponseTOList;
    }

    public List<SysGuidedTourFieldResponseTO> getUiFormList()
    {
        return uiFormList;
    }

    public void setUiFormList(List<SysGuidedTourFieldResponseTO> uiFormList)
    {
        this.uiFormList = uiFormList;
    }


}
