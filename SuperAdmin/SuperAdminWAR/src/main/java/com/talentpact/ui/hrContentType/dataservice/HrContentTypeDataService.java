/**
 * 
 */
package com.talentpact.ui.hrContentType.dataservice;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;



import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;
import com.talentpact.ui.hrContentType.dataservice.DS.HrContentTypeDS;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class HrContentTypeDataService {

	@EJB
	private HrContentTypeDS hrContentTypeDS;
	/**
	 * @return
	 */
	public List<HrContentTypeResponseTO> getHrContentTypeByContentCategory(String contentCategory)
			throws Exception {
		return hrContentTypeDS.getHrContentTypeByContentCategory(contentCategory);
	}

}
