/**
 * 
 */
package com.talentpact.ui.rule;

/**
 * @author pankaj.sharma1
 * 
 */
public interface ISysRuleConstants {

	public static String SysRule_SAVE_DIALOG_HIDE = "PF('addRuleListModel').hide();";

	public static String SysRule_ADD_SUCCESS_MSG = "Rule Method created successfully.";

	public static String SysRule_ADD_ERROR_MSG = "Unexpected error occured while creating new Rule Method.";

	public static String SysRule_UPDATE_DIALOG_HIDE = "PF('ruleListEditModel').hide();";

	public static String SysRule_UPDATE_SUCCESS_MSG = "Rule Method details updated successfully.";

	public static String SysRule_UPDATE_ERROR_MSG = "Unexpected error occured while updating Rule Method details.";
	
	public static String SysRule_BOT_RULE = "BOTRULETYPE";

}
