/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.dataprovider;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysCommunicationType.dataservice.SysCommunicationTypeDataService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCommunicationTypeDataProvider {

	@EJB
	private SysCommunicationTypeDataService sysCommunicationTypeDataService = null;
	/**
	 * @return
	 */
	public ServiceResponse getAllCommType() throws Exception{
		ServiceResponse serviceResponse = null;
		List<SysCommunicationTypeResponseTO> sysCommunicationTypeResponseTOList = null;
		try{
			sysCommunicationTypeResponseTOList = 
					sysCommunicationTypeDataService.getAllCommType();
			if(sysCommunicationTypeResponseTOList!=null && 
					!sysCommunicationTypeResponseTOList.isEmpty()){
				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObjectObjectList(sysCommunicationTypeResponseTOList);
			}
		}catch(Exception ex){
			sysCommunicationTypeResponseTOList = null;
			serviceResponse = null;
			throw ex;
		}
		
		return serviceResponse;
	}

}
