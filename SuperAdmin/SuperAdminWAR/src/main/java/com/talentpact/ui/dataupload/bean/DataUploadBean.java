package com.talentpact.ui.dataupload.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dataupload.controller.DataUploadController;
import com.talentpact.ui.dataupload.to.SysUploadTemplateTypeTO;
import com.talentpact.ui.dataupload.to.TableColumnTO;
import com.talentpact.ui.dataupload.to.ValidationTypeTO;

/**
 * @author shivam.kumar
 * 
 */
@Named("dataUploadBean")
@ConversationScoped
public class DataUploadBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    DataUploadController dataUploadController;

    private List<SelectItem> templateTypeDropDown;

    List<SysUploadTemplateTypeTO> templateTypeTOList, templateTypeFilteredTOList;

    private Integer templateTypeID;

    private String templateTypeName;

    private Integer uploadTableID;

    private List<SelectItem> uploadTableDropDown;

    private List<SelectItem> uploadTableColumnDropDown;

    private List<TableColumnTO> tableColumnTOList;

    private String persistenceUnitName;

    private List<String> tableNames;

    private List<String> tableNamesFiltered;

    private boolean viewMode;

    private Boolean viewAddTempleteType;

    private String tableName;

    private List<TableColumnTO> columnList;

    private List<ValidationTypeTO> validationList;


    @PostConstruct
    public void init()
    {
        endConversation();
        beginConversation();
    }

    @Override
    public void initialize()
    {
        try {
            dataUploadController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public DataUploadController getDataUploadController()
    {
        return dataUploadController;
    }

    public void setDataUploadController(DataUploadController dataUploadController)
    {
        this.dataUploadController = dataUploadController;
    }

    public List<SelectItem> getTemplateTypeDropDown()
    {
        return templateTypeDropDown;
    }

    public boolean isViewMode()
    {
        return viewMode;
    }

    public void setViewMode(boolean viewMode)
    {
        this.viewMode = viewMode;
    }

    public List<String> getTableNamesFiltered()
    {
        return tableNamesFiltered;
    }

    public void setTableNamesFiltered(List<String> tableNamesFiltered)
    {
        this.tableNamesFiltered = tableNamesFiltered;
    }

    public void setTemplateTypeDropDown(List<SelectItem> templateTypeDropDown)
    {
        this.templateTypeDropDown = templateTypeDropDown;
    }

    public List<SysUploadTemplateTypeTO> getTemplateTypeTOList()
    {
        return templateTypeTOList;
    }

    public void setTemplateTypeTOList(List<SysUploadTemplateTypeTO> templateTypeTOList)
    {
        this.templateTypeTOList = templateTypeTOList;
    }

    public Integer getTemplateTypeID()
    {
        return templateTypeID;
    }

    public List<String> getTableNames()
    {
        return tableNames;
    }

    public void setTableNames(List<String> tableNames)
    {
        this.tableNames = tableNames;
    }

    public String getPersistenceUnitName()
    {
        return persistenceUnitName;
    }

    public void setPersistenceUnitName(String persistenceUnitName)
    {
        this.persistenceUnitName = persistenceUnitName;
    }

    public void setTemplateTypeID(Integer templateTypeID)
    {
        this.templateTypeID = templateTypeID;
    }

    public String getTemplateTypeName()
    {
        return templateTypeName;
    }

    public void setTemplateTypeName(String templateTypeName)
    {
        this.templateTypeName = templateTypeName;
    }

    public Integer getUploadTableID()
    {
        return uploadTableID;
    }


    public List<TableColumnTO> getTableColumnTOList()
    {
        return tableColumnTOList;
    }

    public void setTableColumnTOList(List<TableColumnTO> tableColumnTOList)
    {
        this.tableColumnTOList = tableColumnTOList;
    }

    public void setUploadTableID(Integer uploadTableID)
    {
        this.uploadTableID = uploadTableID;
    }

    public List<SelectItem> getUploadTableDropDown()
    {
        return uploadTableDropDown;
    }

    public List<SelectItem> getUploadTableColumnDropDown()
    {
        return uploadTableColumnDropDown;
    }

    public void setUploadTableColumnDropDown(List<SelectItem> uploadTableColumnDropDown)
    {
        this.uploadTableColumnDropDown = uploadTableColumnDropDown;
    }

    public void setUploadTableDropDown(List<SelectItem> uploadTableDropDown)
    {
        this.uploadTableDropDown = uploadTableDropDown;
    }

    public List<SysUploadTemplateTypeTO> getTemplateTypeFilteredTOList()
    {
        return templateTypeFilteredTOList;
    }

    public void setTemplateTypeFilteredTOList(List<SysUploadTemplateTypeTO> templateTypeFilteredTOList)
    {
        this.templateTypeFilteredTOList = templateTypeFilteredTOList;
    }

    public Boolean getViewAddTempleteType()
    {
        return viewAddTempleteType;
    }

    public void setViewAddTempleteType(Boolean viewAddTempleteType)
    {
        this.viewAddTempleteType = viewAddTempleteType;
    }

    public List<TableColumnTO> getColumnList()
    {
        return columnList;
    }

    public void setColumnList(List<TableColumnTO> columnList)
    {
        this.columnList = columnList;
    }

    public List<ValidationTypeTO> getValidationList()
    {
        return validationList;
    }

    public void setValidationList(List<ValidationTypeTO> validationList)
    {
        this.validationList = validationList;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }


}
