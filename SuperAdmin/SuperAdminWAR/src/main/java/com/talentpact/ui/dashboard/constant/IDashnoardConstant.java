package com.talentpact.ui.dashboard.constant;

public interface IDashnoardConstant
{
   // public static final String OFFERING_SEQUENCE_PAGE = "../altbenefits/addSectorBasedOfferingSequence.xhtml";
	 public static final String HR_DASHBOARD_LIST = "../dashboard/dashBoardList.xhtml";
	 public static final String DASHBOARD_SUCCESS_ADD ="DashBoard Item has been added to System Successfully.";
	 public static final String DASHBOARD_SUCCESS_UPDATE ="DashBoard Item has been updated to System Successfully.";
	 public static String UNEXPECTED_ERROR_MSG = "Unexpected error occured.";
}
