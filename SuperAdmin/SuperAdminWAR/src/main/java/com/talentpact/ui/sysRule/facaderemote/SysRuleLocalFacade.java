/**
 * 
 */
package com.talentpact.ui.sysRule.facaderemote;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;

/**
 * @author vaibhav.kashyap
 *
 */
@Local
public interface SysRuleLocalFacade {

	public ServiceResponse getAllRules() 
			throws Exception;
}
