/**
 * 
 */
package com.talentpact.ui.passwordPolicy.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.helper.ValidatorUtil;
import com.alt.contentType.transport.input.ContentTypeTO;
import com.alt.passwordPolicy.constants.IPasswordPolicyConstant;
import com.alt.passwordPolicy.constants.PasswordType;
import com.alt.passwordPolicy.to.PasswordPolicyTO;
import com.talentpact.business.dataservice.SysContentType.SysContentTypeService;
import com.talentpact.business.service.passwordpolicy.SysPasswordPolicyService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.passwordPolicy.bean.PasswordPolicyBean;

/**
 * 
 * @author javed.ali
 * 
 */
@Named("passwordPolicyController")
@ConversationScoped
public class PasswordPolicyController extends CommonController implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = 478933059112145734L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(PasswordPolicyController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    PasswordPolicyBean passwordPolicyBean;

    @Inject
    UserSessionBean userSessionBean;

    @EJB
    SysContentTypeService sysContentTypeService;

    @EJB
    SysPasswordPolicyService sysPasswordPolicyService;

    @Override
    public void initialize()
    {
        PasswordPolicyTO passwordPolicyTO = null;
        List<ContentTypeTO> frequencyTypeList = null;
        try {
            passwordPolicyBean.setShowHome(false);
            passwordPolicyTO = sysPasswordPolicyService.getPasswordPolicySetting();
            if (passwordPolicyTO == null) {
                passwordPolicyTO = new PasswordPolicyTO();
                passwordPolicyBean.setPasswordTypeList(PasswordType.getPasswordTypesAsList());
                passwordPolicyBean.setPasswordTypes(PasswordType.getPasswordTypes());
                passwordPolicyTO.setEdit(false);
                passwordPolicyTO.setMinimumCharacters("4");
                passwordPolicyTO.setNoOfAttempts("5");
                passwordPolicyTO.setShowCapchaAfter("2");
                passwordPolicyTO.setAllowRepeatOldPasswordAfter("1");
                passwordPolicyTO.setForcePasswordChangeFrequency("Quarterly");
                passwordPolicyTO.setResetPasswordOnFirstLogin(false);
                updatePasswordTypeList();

            } else {
                passwordPolicyBean.setPasswordTypes(passwordPolicyTO.getPasswordTypes());
                passwordPolicyBean.setPasswordTypeList(PasswordType.getPasswordTypesAsList());
                passwordPolicyBean.setRenderpasswordTypePanel(true);
                
            }
            passwordPolicyBean.setPasswordPolicyTO(passwordPolicyTO);
            passwordPolicyBean.setMinCharactersList(processCharacterList());
            passwordPolicyBean.setCharacterListForOldPasswordAttemp(processCharacterListForOldPasswordAttemp());
            passwordPolicyBean.setCharacterListForPasswordAttemp(processCharacterListForPasswordAttemp());
            frequencyTypeList = passwordPolicyBean.getPasswordChangeFrequency();
            if (frequencyTypeList == null) {
                frequencyTypeList = fetchFrequencyTypeList("RecurringType");
                passwordPolicyBean.setPasswordChangeFrequency(frequencyTypeList);
            }
        
            menuBean.setPage("../passwordPolicy/passwordPolicySetting.xhtml");
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }

    }

    /**
     * 
     * @param categoryName
     * @return
     */
    private List<ContentTypeTO> fetchFrequencyTypeList(String categoryName)
    {
        List<ContentTypeTO> frequencyTypeList = null;
        try {
            frequencyTypeList = sysContentTypeService.getSysContentTypeList(categoryName);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return frequencyTypeList;
    }

    private List<String> processCharacterList()
    {
        List<String> list = new ArrayList<String>();
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        return list;
    }

    private List<String> processCharacterListForOldPasswordAttemp()
    {
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        return list;
    }


    private List<String> processCharacterListForPasswordAttemp()
    {
        List<String> list = new ArrayList<String>();
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        return list;
    }

    /**
     * 
     * @param selectedPasswordTypes
     * @param passwordPolicyTO
     */



    public void updatePasswordTypeList()
    {
        String[] selectedPasswordTypes = null;
        Map<String, Boolean> passwordTypeMap = null;
        selectedPasswordTypes = passwordPolicyBean.getPasswordTypes();
        PasswordPolicyTO passwordPolicyTO = null;
        try {
            passwordPolicyTO = passwordPolicyBean.getPasswordPolicyTO();
            if (selectedPasswordTypes != null && selectedPasswordTypes.length != 0) {
                passwordTypeMap = new LinkedHashMap<String, Boolean>();
                for (int i = 0; i < selectedPasswordTypes.length; i++) {
                    if (!passwordPolicyTO.getPasswordTypeMap().containsKey(selectedPasswordTypes[i])) {
                        passwordTypeMap.put(selectedPasswordTypes[i], false);
                    } else {
                        passwordTypeMap.put(selectedPasswordTypes[i], passwordPolicyTO.getPasswordTypeMap().get(selectedPasswordTypes[i]));
                    }
                }
                passwordPolicyTO.setPasswordTypeMap(passwordTypeMap);
                passwordPolicyBean.setRenderpasswordTypePanel(true);
            } else {
                passwordPolicyBean.setRenderpasswordTypePanel(false);
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }



    public void buildPasswordPolicySetting()
    {
        passwordPolicyBean.setShowHome(true);
    }

    public void backToHome()
    {
        passwordPolicyBean.setShowHome(false);
    }

    /**
     * 
     */
    public void savePasswordPolicySetting()
    {
        try {
            if(passwordPolicyBean.getPasswordTypes()==null||passwordPolicyBean.getPasswordTypes().length==0){
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, IPasswordPolicyConstant.SYS_POASSWORD_TYPE_EMPTY_MSG, ""));
            } else if (passwordPolicyBean.getPasswordPolicyTO() == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nothing to save.", ""));
            } else if (ValidatorUtil.isNullorEmpty(passwordPolicyBean.getPasswordPolicyTO().getBlockingDuration())
                    || !ValidatorUtil.isNumeric(passwordPolicyBean.getPasswordPolicyTO().getBlockingDuration())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Blocking duration must be numeric", ""));
            }else{
            sysPasswordPolicyService.savePasswordPolicySetting(passwordPolicyBean.getPasswordPolicyTO(), userSessionBean.getUserID());
            if (passwordPolicyBean.getPasswordPolicyTO().isEdit()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IPasswordPolicyConstant.SYS_POASSWORD_POLICY_SAVE_SUCCESS_MSG, ""));

            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, IPasswordPolicyConstant.SYS_POASSWORD_POLICY_UPDATE_SUCCESS_MSG, ""));
            }
            initialize();
            }
        } catch (Exception e) {
            if (passwordPolicyBean.getPasswordPolicyTO().isEdit()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IPasswordPolicyConstant.SYS_POASSWORD_POLICY_SAVE_ERROR_MSG, ""));

            } else {
                FacesContext.getCurrentInstance()
                        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IPasswordPolicyConstant.SYS_POASSWORD_POLICY_UPDATE_ERROR_MSG, ""));

            }
            LOGGER_.error("", e);
        }
    }

}