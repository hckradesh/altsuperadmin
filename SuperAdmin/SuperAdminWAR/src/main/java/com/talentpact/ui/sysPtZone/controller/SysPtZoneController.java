package com.talentpact.ui.sysPtZone.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysPtZoneConstants;
import com.talentpact.business.dataservice.sysPtZone.SysPtZoneService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysPtZone.bean.SysPtZoneBean;
import com.talentpact.ui.sysPtZone.to.SysPtZoneTO;


/**
 * 
 * @author prachi.bansal
 *
 */

@Named("sysPtZoneController")
@ConversationScoped
public class SysPtZoneController extends CommonController implements Serializable, IDefaultAction {

    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPtZoneController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysPtZoneBean sysPtZoneBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    SysPtZoneService sysPtZoneService;


    public void initialize() {
        ServiceResponse response = null;
        try {
            sysPtZoneBean.setSysPtZoneTOList(sysPtZoneService.getSysPtZoneTOList());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../sysPtZone/sysPtZone.xhtml");
            requestContext.execute(ISysPtZoneConstants.SYSPTZONE_DATATABLE_RESET);
            requestContext.execute(ISysPtZoneConstants.SYSPTZONE_PAGINATION_DATATABLE_RESET);

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    public void resetSysPtZone()
        throws Exception {

        sysPtZoneBean.setPtZoneID(null);
        sysPtZoneBean.setPtZoneCode(null);
        sysPtZoneBean.setPtZoneName(null);
        sysPtZoneBean.setStateName(null);
        sysPtZoneBean.setStateID(null);
        sysPtZoneBean.setStateList(null);
        if (sysPtZoneService.getSysStateList() != null) {
            sysPtZoneBean.setStateList(sysPtZoneService.getSysStateList());
        }
    }

    public void saveSysPtZone()
        throws Exception {

        SysPtZoneTO sysPtZoneTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPtZoneTO = new SysPtZoneTO();
            sysPtZoneTO.setPtZoneCode(sysPtZoneBean.getPtZoneCode());
            sysPtZoneTO.setPtZoneName(sysPtZoneBean.getPtZoneName());
            sysPtZoneTO.setStateID(sysPtZoneBean.getStateID());
            if (validateSysPtZone(sysPtZoneTO, false)) {
                sysPtZoneService.saveSysPtZone(sysPtZoneTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysPtZone added successfully", ""));
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_ADD_DIALOG_HIDE);
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_DATATABLE_RESET);
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysPtZoneListForm");
            } else {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPtZoneConstants.SYSPTZONE_DUPLICATE_ERROR_MSG, ""));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPtZoneConstants.SYSPTZONE_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }
    }

    private boolean validateSysPtZone(SysPtZoneTO sysPtZoneTO, boolean edited) {
        List<SysPtZoneTO> sysPtZoneTOList = null;
        try {
            if (edited) {
                sysPtZoneTOList = sysPtZoneService.getUpdatedSysPtZoneTOList(sysPtZoneTO.getPtZoneID());
            } else {
                sysPtZoneTOList = sysPtZoneService.getSysPtZoneTOList();
            }
            for (SysPtZoneTO sysPtZone : sysPtZoneTOList) {
                if (sysPtZoneTO.getPtZoneCode().trim().equalsIgnoreCase(sysPtZone.getPtZoneCode().trim())) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            return false;
        }
    }

    public void editSysPtZone(SysPtZoneTO sysPtZoneTO) {
        
        try {
            
            sysPtZoneBean.setPtZoneName(sysPtZoneTO.getPtZoneName());
            sysPtZoneBean.setPtZoneCode(sysPtZoneTO.getPtZoneCode());
            sysPtZoneBean.setStateID(sysPtZoneTO.getStateID());
            sysPtZoneBean.setStateName(sysPtZoneTO.getStateName());
            sysPtZoneBean.setSysPtZoneTO(sysPtZoneTO);
            if (sysPtZoneService.getSysStateList() != null) {
                sysPtZoneBean.setStateList(sysPtZoneService.getSysStateList());
            }
          
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    public void updateSysPtZone() {
        SysPtZoneTO sysPtZoneTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPtZoneTO = new SysPtZoneTO();
            sysPtZoneTO.setPtZoneCode(sysPtZoneBean.getPtZoneCode());
            sysPtZoneTO.setPtZoneID(sysPtZoneBean.getSysPtZoneTO().getPtZoneID());
            sysPtZoneTO.setPtZoneName(sysPtZoneBean.getPtZoneName());
            sysPtZoneTO.setStateName(sysPtZoneBean.getStateName());
            if (sysPtZoneBean.getStateID() == 0 || sysPtZoneBean.getStateID() == null) {
                sysPtZoneTO.setStateID(sysPtZoneBean.getSysPtZoneTO().getStateID());
            } else {
                sysPtZoneTO.setStateID(sysPtZoneBean.getStateID());
            }

            if (validateSysPtZone(sysPtZoneTO, true)) {
                sysPtZoneService.updateSysPtZone(sysPtZoneTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysPtZone updated successfully", ""));
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_DATATABLE_RESET);
                requestContext.execute(ISysPtZoneConstants.SYSPTZONE_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysPtZoneListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "PtZone Code already present in the system.", ""));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPtZoneConstants.SYSPTZONE_UPDATE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }


    }

}
