/**
 * 
 */
package com.talentpact.ui.survey.user.utility;



/**
 * @author vaibhav.kashyap
 *
 */
public enum SurveyStatus {
	DESIGN_COMPLETED("Design completed",0), DESIGN_IN_PROGRESS("Design in progress",1), ACTIVATED("Activated",2) ,
	DE_ACTIVATED("De-Activated",3);
	
	public final int numericValue;
	public final String stringValue;                
	
	
	SurveyStatus(String stringValue , int numericValue ) {
		this.numericValue = numericValue;
		this.stringValue = stringValue;
	}
	
	public static int contains(String surveyStatus) {

		int value = -1;
		
	    for (SurveyStatus o : SurveyStatus.values()) {
	        if (o.name().equalsIgnoreCase(surveyStatus)){
	        	value = o.numericValue;
	            break;
	        }
	    }

	    return value;
	}
}
