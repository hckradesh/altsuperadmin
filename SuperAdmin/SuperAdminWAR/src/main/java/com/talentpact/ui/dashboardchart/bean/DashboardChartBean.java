package com.talentpact.ui.dashboardchart.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.insights.dashboardchart.to.DashboardChartTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboardchart.controller.DashboardChartController;

/**
 * 
 * @author javed.ali
 * 
 */


@Named("dashboardChartBean")
@ConversationScoped
public class DashboardChartBean extends CommonBean implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = 998074848121231271L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(DashboardChartBean.class);

    @Inject
    DashboardChartController dashboardChartController;


    private String dashboardName;

    private String styleClass;

    private boolean renderErrorMessage;

    private List<DashboardChartTO> sysDashboardTOList;


    public String getDashboardName()
    {
        return dashboardName;
    }


    public void setDashboardName(String dashboardName)
    {
        this.dashboardName = dashboardName;
    }


    public String getStyleClass()
    {
        return styleClass;
    }


    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }


    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }


    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }


    public List<DashboardChartTO> getSysDashboardTOList()
    {
        return sysDashboardTOList;
    }


    public void setSysDashboardTOList(List<DashboardChartTO> sysDashboardTOList)
    {
        this.sysDashboardTOList = sysDashboardTOList;
    }


    @Override
    public void initialize()
    {
        try {
            beginConversation();
            dashboardChartController.initialize();
        } catch (Exception e) {
            LOGGER_.error(e.getMessage(), e);
        }
    }

}