package com.talentpact.ui.sysmeshmodule.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.configuration.menu.to.ModuleTO;
import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysmeshmodule.controller.SysMeshModuleController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@SuppressWarnings("serial")
@Named("sysMeshModuleBean")
@ConversationScoped
public class SysMeshModuleBean extends CommonBean implements Serializable, IDefaultAction {
	@Inject
	SysMeshModuleController sysMeshModuleController;

	private int sysMeshModuleID;

	private String sysMeshModuleName;

	private String sysMeshModuleDescription;

	private int moduleID;

	private String moduleName;

	private List<SysMeshModuleTO> sysSubModuleTOList;

	private List<SysMeshModuleTO> sysMeshModuleList;

	private boolean renderErrorMessage;

	public SysMeshModuleController getSysMeshModuleController() {
		return sysMeshModuleController;
	}

	public void setSysMeshModuleController(SysMeshModuleController sysMeshModuleController) {
		this.sysMeshModuleController = sysMeshModuleController;
	}

	public int getSysMeshModuleID() {
		return sysMeshModuleID;
	}

	public void setSysMeshModuleID(int sysMeshModuleID) {
		this.sysMeshModuleID = sysMeshModuleID;
	}

	public String getSysMeshModuleName() {
		return sysMeshModuleName;
	}

	public void setSysMeshModuleName(String sysMeshModuleName) {
		this.sysMeshModuleName = sysMeshModuleName;
	}

	public String getSysMeshModuleDescription() {
		return sysMeshModuleDescription;
	}

	public void setSysMeshModuleDescription(String sysMeshModuleDescription) {
		this.sysMeshModuleDescription = sysMeshModuleDescription;
	}

	public int getModuleID() {
		return moduleID;
	}

	public void setModuleID(int moduleID) {
		this.moduleID = moduleID;
	}

	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			sysMeshModuleController.initialize();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public List<SysMeshModuleTO> getSysMeshModuleList() {
		return sysMeshModuleList;
	}

	public void setSysMeshModuleList(List<SysMeshModuleTO> sysMeshModuleList) {
		this.sysMeshModuleList = sysMeshModuleList;
	}

	public List<SysMeshModuleTO> getSysSubModuleTOList() {
		return sysSubModuleTOList;
	}

	public void setSysSubModuleTOList(List<SysMeshModuleTO> sysSubModuleTOList) {
		this.sysSubModuleTOList = sysSubModuleTOList;
	}
}