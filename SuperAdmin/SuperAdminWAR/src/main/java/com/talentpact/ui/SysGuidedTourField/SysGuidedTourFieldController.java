/**
 * 
 */
package com.talentpact.ui.SysGuidedTourField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.output.SysGuidedTourFieldResponseTO;
import com.talentpact.business.dataservice.SysGuidedTourField.SysGuidedTourFieldService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysGuidedTourFieldController")
@ConversationScoped
public class SysGuidedTourFieldController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    SysGuidedTourFieldService sysGuidedTourFieldService;

    @Inject
    SysGuidedTourFieldBean sysGuidedTourFieldBean;

    @Override
    public void initialize()
    {
        List<SysGuidedTourFieldResponseTO> sysGuidedTourFieldTOList = null;
        List<SysGuidedTourFieldResponseTO> uiFormList = null;

        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {
            uiFormList = new ArrayList<SysGuidedTourFieldResponseTO>();
            uiFormList = getUiFormDropDown();
            sysGuidedTourFieldBean.setUiFormList(uiFormList);
            sysGuidedTourFieldTOList = new ArrayList<SysGuidedTourFieldResponseTO>();
            sysGuidedTourFieldTOList = getSysGuidedTourFieldTOList();
            sysGuidedTourFieldBean.setSysGuidedTourFieldResponseTOList(sysGuidedTourFieldTOList);
            menuBean.setPage("../sysGuidedTourField/sysGuidedTourField.xhtml");

            requestContext.execute("PF('myDataTable').clearFilters();");
            requestContext.execute("PF('myDataTable').getPaginator().setPage(0);");
        } catch (Exception e) {
        } finally {

        }

    }

    public void refreshPopUp(SysGuidedTourFieldResponseTO to)
    {
        try {
            sysGuidedTourFieldBean.setGuidedTourFieldId(to.getGuidedTourFieldId());
            sysGuidedTourFieldBean.setClickPath(to.getClickPath());
            sysGuidedTourFieldBean.setCombine(to.getCombine());
            sysGuidedTourFieldBean.setSelector(to.getSelector());
            sysGuidedTourFieldBean.setSysFormId(to.getSysFormId());
            sysGuidedTourFieldBean.setSysForm(to.getSysForm());

        } catch (Exception e) {
        }
    }

    public void resetBean()
    {
        try {
            sysGuidedTourFieldBean.setGuidedTourFieldId(null);
            sysGuidedTourFieldBean.setClickPath(null);
            sysGuidedTourFieldBean.setCombine(null);
            sysGuidedTourFieldBean.setSelector(null);
            sysGuidedTourFieldBean.setSysFormId(null);
        } catch (Exception e) {

        }

    }


    public void save()
    {

        SysGuidedTourFieldResponseTO to = null;
        RequestContext context = null;
        Boolean isDuplicate = false;
        try {
            context = RequestContext.getCurrentInstance();
            if (sysGuidedTourFieldBean.getSelector() != null) {
                if (sysGuidedTourFieldBean.getSelector().trim().isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("newSysGuidedTourFieldForm:newSysGuidedTourField1",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a valid value for selector ", ""));
                    return;
                }

            }

            if (sysGuidedTourFieldBean.getClickPath() != null) {
                if (sysGuidedTourFieldBean.getSelector().trim().isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("newSysGuidedTourFieldForm:newSysGuidedTourField1",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a valid value for click Path ", ""));
                    return;
                }

            }


            to = new SysGuidedTourFieldResponseTO();
            to.setSysFormId(sysGuidedTourFieldBean.getSysFormId());
            to.setClickPath(sysGuidedTourFieldBean.getClickPath());
            to.setCombine(sysGuidedTourFieldBean.getCombine());
            to.setSelector(sysGuidedTourFieldBean.getSelector());
            //            isDuplicate = checkDublicateForSave(to);
            //            if (isDuplicate) {
            //                FacesContext.getCurrentInstance().addMessage("newSysGuidedTourFieldForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, to.getSelector() + " already exists", ""));
            //                return;
            //            } else {
            sysGuidedTourFieldService.save(to);

            initialize();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved Successfully", ""));
            context.execute("PF('sysGuidedTourFieldAddModal').hide();");
            context.update("sysGuidedTourFieldListForm");

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error occured", ""));
            context.execute("PF('sysGuidedTourFieldFormAddModal').hide();");
        } finally {
            to = null;
        }

    }

    public void update()
    {
        SysGuidedTourFieldResponseTO to = null;
        RequestContext context = null;
        Boolean isDuplicate = false;
        try {
            if (sysGuidedTourFieldBean.getSelector() != null) {
                if (sysGuidedTourFieldBean.getSelector().trim().isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("sysGuidedTourFieldUpdateForm:changeSysGuidedTourFieldDetails",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a valid value for selector ", ""));
                    return;
                }

            }

            if (sysGuidedTourFieldBean.getClickPath() != null) {
                if (sysGuidedTourFieldBean.getSelector().trim().isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("sysGuidedTourFieldUpdateForm:changeSysGuidedTourFieldDetails",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a valid value for click Path ", ""));
                    return;
                }

            }
            context = RequestContext.getCurrentInstance();
            to = new SysGuidedTourFieldResponseTO();

            to.setGuidedTourFieldId(sysGuidedTourFieldBean.getGuidedTourFieldId());
            to.setSysFormId(sysGuidedTourFieldBean.getSysFormId());
            to.setClickPath(sysGuidedTourFieldBean.getClickPath());
            to.setCombine(sysGuidedTourFieldBean.getCombine());
            to.setSelector(sysGuidedTourFieldBean.getSelector());
            //            isDuplicate = checkDublicateForUpdate(to);
            //            if (isDuplicate) {
            //                FacesContext.getCurrentInstance().addMessage("sysGuidedTourFieldFormUpdateForm",
            //                        new FacesMessage(FacesMessage.SEVERITY_ERROR, to.getSelector() + " already exists", ""));
            //                return;
            //
            //            } else {

            sysGuidedTourFieldService.save(to);
            context.execute("PF('myDataTable').clearFilters();");
            initialize();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, " Updated Succesfully", ""));
            context.execute("PF('sysGuidedTourFieldEditModal').hide();");
            context.update("sysGuidedTourFieldListForm");
            //  }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected error occured", ""));
            context.execute("PF('sysGuidedTourFieldEditModal').hide();");

        } finally {
            to = null;
        }
    }


    private List<SysGuidedTourFieldResponseTO> getSysGuidedTourFieldTOList()
    {
        List<SysGuidedTourFieldResponseTO> response = null;
        try {
            response = sysGuidedTourFieldService.getSysGuidedTourFieldResponseTOList();
        } catch (Exception e) {
        }
        return response;

    }

    private List<SysGuidedTourFieldResponseTO> getUiFormDropDown()
    {
        List<SysGuidedTourFieldResponseTO> response = null;
        try {
            response = sysGuidedTourFieldService.getUiFormDropDown();

        } catch (Exception e) {
        }
        return response;

    }

}
