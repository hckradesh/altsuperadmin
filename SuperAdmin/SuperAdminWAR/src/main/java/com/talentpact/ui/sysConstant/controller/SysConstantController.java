/**
 * 
 */
package com.talentpact.ui.sysConstant.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.input.SysConstantRequestTO;
import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.business.dataservice.SysConstant.SysConstantService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysConstant.bean.SysConstantBean;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysConstantController")
@ConversationScoped
public class SysConstantController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    SysConstantService sysConstantService;

    @Inject
    SysConstantBean sysConstantBean;

    @Override
    public void initialize()
    {
        List<SysConstantResponseTO> sysConstantTOList = null;
        List<SysConstantResponseTO> sysConstantCategoryDropDownTOList = null;
        try {

            sysConstantTOList = getSysConstantTypeList();
            sysConstantCategoryDropDownTOList = getSysConstantCategoryListDropDown();
            sysConstantBean.setConstantTOList(sysConstantTOList);
            sysConstantBean.setCategoryDropDownTOList(sysConstantCategoryDropDownTOList);
            menuBean.setPage("../sysConstant/sysConstant.xhtml");
        } catch (Exception e) {
        } finally {

        }

    }


    public void refreshPopUp(SysConstantResponseTO constantTO)
    {
        try {
            sysConstantBean.setSysConstantCategoryID(constantTO.getSysConstantCategoryID());
            sysConstantBean.setSysConstantCategoryName(constantTO.getSysConstantCategoryName());
            sysConstantBean.setSysConstantID(constantTO.getSysConstantID());
            sysConstantBean.setSysConstantName(constantTO.getSysConstantName());
            sysConstantBean.setStatus(constantTO.getStatus());

        } catch (Exception e) {
        }
    }


    public void resetBean()
    {
        try {
            sysConstantBean.setSysConstantCategoryName(null);
            sysConstantBean.setSysConstantName(null);
            sysConstantBean.setSysConstantCategoryID(null);
            sysConstantBean.setStatus(null);
        } catch (Exception e) {

        }

    }


    private List<SysConstantResponseTO> getSysConstantTypeList()
    {
        List<SysConstantResponseTO> response = null;
        try {
            response = sysConstantService.getSysConstantTypeList();
        } catch (Exception e) {
        }
        return response;

    }

    private List<SysConstantResponseTO> getSysConstantCategoryListDropDown()
    {
        List<SysConstantResponseTO> response = null;
        try {
            response = sysConstantService.getSysConstantCategoryListDropDown();
        } catch (Exception e) {
        }
        return response;

    }


    public void update()
    {
        SysConstantRequestTO constant = null;
        String constantName = null;
        Boolean isConstantNameExist = null;
        RequestContext context = null;
        List<String> list = null;
        try {
            list = new ArrayList<String>();
            list = getThemeListFromTpURL();
            context = RequestContext.getCurrentInstance();
            Integer constantID = sysConstantBean.getSysConstantID();
            boolean isConstantIDExist = checkConstantIDinList(constantID, sysConstantBean.getConstantTOList());
            constantName = sysConstantBean.getSysConstantName().trim();
            if (constantName != null && !constantName.equals("")) {
                isConstantNameExist = checkConstantNameinList(constantName.trim(), constantID, sysConstantBean.getConstantTOList());
            } else {
                FacesContext.getCurrentInstance().addMessage("sysConstantListUpdateForm:changeSysConstantDetails",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered SysConstant Name is Invalid ", ""));
                return;
            }

            if (isConstantNameExist != null) {
                if (isConstantIDExist == true) {

                    constant = new SysConstantRequestTO();
                    if (isConstantNameExist == false) {
                        constant.setSysConstantName(constantName);

                    } else {
                        FacesContext.getCurrentInstance().addMessage("changeSysConstantDetails",
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "SysConstant with name '" + sysConstantBean.getSysConstantName() + "' already exists", ""));
                        return;
                    }


                    constant.setSysConstantID(sysConstantBean.getSysConstantID());
                    constant.setSysConstantCategoryID(sysConstantBean.getSysConstantCategoryID());
                    constant.setSysConstantCategoryName(sysConstantBean.getSysConstantCategoryName());
                    constant.setSysConstantName(sysConstantBean.getSysConstantName());
                    constant.setStatus(sysConstantBean.getStatus());
                    if (sysConstantBean.getStatus() == false) {
                        if (list != null && !list.isEmpty()) {
                            for (String t : list) {
                                if (t != null) {
                                    if (t.equalsIgnoreCase(constant.getSysConstantName())) {
                                        FacesContext.getCurrentInstance().addMessage(
                                                "changeSysConstantDetails",
                                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "SysConstant with name '" + sysConstantBean.getSysConstantName()
                                                        + "' can not be updated as it is being used for one or more organizations", ""));
                                        return;
                                    }

                                }
                            }
                        }
                    }
                    sysConstantService.saveSysConstant(constant);
                    context.execute("PF('myDataTable').clearFilters();");
                    initialize();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysConstant Updated Succesfully", ""));
                    context.update("sysConstantListForm");
                }
            }


            context.execute("PF('sysConstantEditModal').hide();");
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error occured", ""));
            context.execute("PF('sysConstantEditModal').hide();");

        }
    }

    public void save()
    {
            SysConstantRequestTO constant = null;
            String constantName = null;
            Boolean isconstantNameExist = null;
            RequestContext context = null;

            try {

                context = RequestContext.getCurrentInstance();
                constantName = sysConstantBean.getSysConstantName().trim();


                if (constantName != null && !constantName.equals("")) {
                    isconstantNameExist = checkExistingSysConstantNameinList(constantName, sysConstantBean.getConstantTOList());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered SysConstant name is Invalid ", ""));
                    return;
                }


                if (isconstantNameExist != null) {
                    if (isconstantNameExist == false) {
                        constant = new SysConstantRequestTO();
                        constant.setSysConstantCategoryID(sysConstantBean.getSysConstantCategoryID());
                        constant.setSysConstantName(sysConstantBean.getSysConstantName());
                        constant.setStatus(sysConstantBean.getStatus());
                        sysConstantService.saveSysConstant(constant);
                        initialize();
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved Successfully", ""));
                    } else {
                        FacesContext.getCurrentInstance().addMessage("newSysConstantForm:newSysConstant1",
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "SysCOnstant with name '" + sysConstantBean.getSysConstantName() + "' already exists", ""));
                        return;
                    }
                }
                context.execute("PF('sysConstantAddModal').hide();");
                context.update("sysConstantListForm");
            } catch (Exception ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error occured", ""));
                context.execute("PF('sysConstantAddModal').hide();");
            }
    }

    public static boolean checkConstantNameinList(String constantName, Integer constantID, List<SysConstantResponseTO> constantTOList)
    {

        for (SysConstantResponseTO constant : constantTOList) {
            if (constant != null && constant.getSysConstantName() != null && constantID != constant.getSysConstantID()
                    && (constant.getSysConstantName()).equalsIgnoreCase(constantName.trim())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkExistingSysConstantNameinList(String constantName, List<SysConstantResponseTO> constantTOList)
    {
        if (constantTOList == null || constantTOList.isEmpty()) {
            return false;
        }

        for (SysConstantResponseTO constant : constantTOList) {
            if (constant != null && constant.getSysConstantName() != null && (constant.getSysConstantName()).equalsIgnoreCase(constantName.trim())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkConstantIDinList(Integer constantID, List<SysConstantResponseTO> constantTOList)
    {

        for (SysConstantResponseTO constant : constantTOList) {
            if (constant != null && constant.getSysConstantID() != null && (constant.getSysConstantID()).equals(constantID)) {
                return true;
            }
        }
        return false;
    }

    public List<String> getThemeListFromTpURL()
        throws Exception
    {
        List<String> serviceResponse = null;
        try {
            serviceResponse = new ArrayList<String>();
            serviceResponse = sysConstantService.getThemeListFromTpURL();
        }

        catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
        return serviceResponse;
    }

}
