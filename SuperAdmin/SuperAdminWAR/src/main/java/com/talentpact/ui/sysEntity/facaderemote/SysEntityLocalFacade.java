/**
 * 
 */
package com.talentpact.ui.sysEntity.facaderemote;

import javax.ejb.Local;

import com.talentpact.business.common.service.output.ServiceResponse;

/**
 * @author vaibhav.kashyap
 *
 */
@Local
public interface SysEntityLocalFacade {

	public ServiceResponse getAllEntities()
			throws Exception;
}
