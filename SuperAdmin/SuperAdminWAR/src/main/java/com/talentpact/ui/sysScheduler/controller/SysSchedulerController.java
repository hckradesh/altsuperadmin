package com.talentpact.ui.sysScheduler.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysSchedulerConstants;
import com.talentpact.business.dataservice.sysScheduler.SysSchedulerService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysScheduler.bean.SysSchedulerBean;
import com.talentpact.ui.sysScheduler.to.SysSchedulerTO;


/**
 * 
 * @author prachi.bansal
 *
 */

@Named("sysSchedulerController")
@ConversationScoped
public class SysSchedulerController  extends CommonController implements Serializable, IDefaultAction{
    
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysSchedulerController.class);
    
    @Inject
    MenuBean menuBean;
    
    @Inject
    UserSessionBean userSessionBean;

    @Inject
    SysSchedulerService sysSchedulerService;

    @Inject
    SysSchedulerBean sysSchedulerBean;
    
    @SuppressWarnings("unused")
    @Override
    public void initialize() {
       
        ServiceResponse response=null;
        try{
            sysSchedulerBean.setSysSchedulerTOList(sysSchedulerService.getSysScheduler());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../sysScheduler/sysScheduler.xhtml");
            requestContext.execute(ISysSchedulerConstants.SYSSCHEDULER_DATATABLE_RESET);
            requestContext.execute(ISysSchedulerConstants.SYSSCHEDULER_PAGINATION_DATATABLE_RESET);
        }catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
    
    public void resetSysScheduler()
    {
        sysSchedulerBean.setSysSchedulerTypeID(0);
        sysSchedulerBean.setSysSchedulerDescription(null);
        sysSchedulerBean.setSysSchedulerName(null);
        sysSchedulerBean.setSysSchedulerTOList(null);
    }
    
    public void saveSysScheduler()
            throws Exception
        {
            SysSchedulerTO sysSchedulerTO=null;
            RequestContext request=null;
            try{
                request=RequestContext.getCurrentInstance();
                sysSchedulerTO=new SysSchedulerTO();
                sysSchedulerTO.setSysSchedulerDescription(sysSchedulerBean.getSysSchedulerDescription());
                sysSchedulerTO.setSysSchedulerName(sysSchedulerBean.getSysSchedulerName());
                if(validateSysScheduler(sysSchedulerTO,false))
                {
                sysSchedulerService.saveSysScheduler(sysSchedulerTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysScheduler added successfully", ""));
                request.execute(ISysSchedulerConstants.SYSSCHEDULER_ADD_DIALOG_HIDE);
                request.execute(ISysSchedulerConstants.SYSSCHEDULER_DATATABLE_RESET);
                request.execute(ISysSchedulerConstants.SYSSCHEDULER_PAGINATION_DATATABLE_RESET);
                request.update("sysSchedulerListForm");
                }
                else 
                {   
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysSchedulerConstants.SYSSCHEDULER_DUPLICATE_ERROR_MSG , ""));
                    }
                    //request.execute(ISysSchedulerConstants.SYSSCHEDULER_DUPLICATE_ERROR_MSG);
                
                
                
            }
            catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysSchedulerConstants.SYSSCHEDULER_SAVE_ERROR_MSG, ""));
                LOGGER_.error("", e);
                throw e;
            }
        }
    
    private boolean validateSysScheduler(SysSchedulerTO sysSchedulerTO, boolean edited) throws Exception{
        List<SysSchedulerTO> sysSchedulerTOList=null;
        try{
            if(edited==false){
                sysSchedulerTOList=sysSchedulerService.getSysScheduler();
            }
            else{
                sysSchedulerTOList=sysSchedulerService.getUpdateSysSchedulerList(sysSchedulerTO.getSysSchedulerTypeID());
            }
            
            for(SysSchedulerTO sysScheduler : sysSchedulerTOList ){
                if(sysScheduler.getSysSchedulerName().trim().equalsIgnoreCase(sysSchedulerTO.getSysSchedulerName().trim())){
                    return false;
                }
            }  
            return true;
        }catch(Exception ex){
            LOGGER_.error("", ex);
            return false;
        }
 }

    public void editSysScheduler(SysSchedulerTO sysSchedulerTO){
        try{
            sysSchedulerBean.setSysSchedulerTO(sysSchedulerTO);
        }catch(Exception ex){
            LOGGER_.error("", ex);
        }
        
    }
    
    public void updateSysScheduler() throws Exception{
        SysSchedulerTO sysSchedulerTO=null;
        RequestContext requestContext=null;
        try{
            requestContext=RequestContext.getCurrentInstance();
            sysSchedulerTO=new SysSchedulerTO();
            sysSchedulerTO.setSysSchedulerTypeID(sysSchedulerBean.getSysSchedulerTO().getSysSchedulerTypeID());
            sysSchedulerTO.setSysSchedulerName(sysSchedulerBean.getSysSchedulerTO().getSysSchedulerName());
            sysSchedulerTO.setSysSchedulerDescription(sysSchedulerBean.getSysSchedulerTO().getSysSchedulerDescription());
            if(validateSysScheduler(sysSchedulerTO,true)){
                sysSchedulerService.updateSysScheduler(sysSchedulerTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysScheduler updated successfully", ""));
                requestContext.execute(ISysSchedulerConstants.SYSSCHEDULER_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysSchedulerConstants.SYSSCHEDULER_DATATABLE_RESET);
                requestContext.execute(ISysSchedulerConstants.SYSSCHEDULER_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysSchedulerListForm");
               }
            else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "SysScheduler already exists", ""));
            }
        }catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysSchedulerConstants.SYSSCHEDULER_UPDATE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }
    }
    
}