/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.facaderemote;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;

/**
 * @author vaibhav.kashyap
 *
 */
@Local
public interface SysCommunicationTypeLocalFacade {

	public ServiceResponse getAllCommType() 
			throws Exception;
}
