/**
 * 
 */
package com.talentpact.ui.tenants.modules.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.tenants.modules.bean.ModulesBean;

/**
 * @author radhamadhab.dalai
 *
 */

@ConversationScoped
@Named("modulesController")
public class ModulesController implements Serializable, IDefaultAction
{

    @Inject
    ModulesBean modulesBean;


    public void initialize()
    {
        List<SelectItem> itemList = null;
        SelectItem item = null;
        try {
            itemList = new ArrayList<SelectItem>();
            item = new SelectItem();
            item.setLabel("ON");
            item.setValue(1);
            itemList.add(item);

            item = new SelectItem();
            item.setLabel("OFF");
            item.setValue(2);
            itemList.add(item);

            modulesBean.setItemList(itemList);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void change()
    {
        int hrisValue = 0;
        try {

            hrisValue = modulesBean.getNumber();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
