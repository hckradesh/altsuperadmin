/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysCommunicationType.dataprovider.SysCommunicationTypeDataProvider;
import com.talentpact.ui.sysCommunicationType.facaderemote.SysCommunicationTypeLocalFacade;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCommunicationTypeFacade implements
		SysCommunicationTypeLocalFacade {

	@EJB
	private SysCommunicationTypeDataProvider sysCommunicationTypeDataProvider;
	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysCommunicationType.facaderemote.SysCommunicationTypeLocalFacade#getAllCommType()
	 */
	@Override
	public ServiceResponse getAllCommType() throws Exception {
		return sysCommunicationTypeDataProvider.getAllCommType();
	}

}
