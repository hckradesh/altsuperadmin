package com.talentpact.ui.sysWorkflowType.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.output.SysSubModuleTO;
import com.talentpact.business.application.transport.output.SysWorkflowStageTypeGroupTO;
import com.talentpact.business.application.transport.output.SysWorkflowStageTypeTO;
import com.talentpact.business.application.transport.output.SysWorkflowTypeTO;
import com.talentpact.business.application.transport.output.UIFormTO;
import com.talentpact.business.dataservice.SysWorkflowType.SysWorkflowTypeService;
import com.talentpact.business.dataservice.UiForm.UiFormService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysSubModule.dataservice.SysSubModuleDataService;
import com.talentpact.ui.sysWorkflowType.bean.SysWorkflowTypeBean;

@Named("sysWorkflowTypeController")
@ConversationScoped
public class SysWorkflowTypeController extends CommonController implements Serializable, IDefaultAction {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(SysWorkflowTypeController.class.getName());

    @Inject
    SysWorkflowTypeBean sysWorkflowTypeBean;

    @Inject
    SysWorkflowTypeService sysWorkflowTypeService;

    @Inject
    SysSubModuleDataService sysSubModuleDataService;

    @Inject
    UiFormService uiFormService;

    @Inject
    MenuBean menuBean;

    @Override
    public void initialize() {
        try {
            sysWorkflowTypeBean.setSysWorkflowTypeTO(new SysWorkflowTypeTO());
            sysWorkflowTypeBean.setSysSubModuleTOList(sysSubModuleDataService.getAllSysSubModuleTOList());
            sysWorkflowTypeBean.setSysWorkflowTypeTOList(sysWorkflowTypeService.getSysWorkflowTypeTOList());
            menuBean.setPage("../SysWorkflowType/sysWorkflowTypeList.xhtml");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void newSysWorkflowType() {
        sysWorkflowTypeBean.setSysWorkflowTypeTO(new SysWorkflowTypeTO());
    }

    public void addSysWorkflowType() {

        SysWorkflowTypeTO sysWorkflowTypeTO = sysWorkflowTypeBean.getSysWorkflowTypeTO();
        for (SysSubModuleTO sysSubModuleTO : sysWorkflowTypeBean.getSysSubModuleTOList()) {
            if (sysWorkflowTypeTO.getModuleId().equals(sysSubModuleTO.getModuleId())) {
                sysWorkflowTypeTO.setModuleName(sysSubModuleTO.getModuleName());
            }
        }

        if (!validateSysWorkflowType()) {
            return;
        }

        sysWorkflowTypeService.addSysWorkflowType(sysWorkflowTypeTO);
        sysWorkflowTypeTO.setSysWorkflowStageTypeTOs(new ArrayList<SysWorkflowStageTypeTO>());
        sysWorkflowTypeTO.setSysWorkflowStageTypeGroupTOs(new ArrayList<SysWorkflowStageTypeGroupTO>());
        sysWorkflowTypeBean.getSysWorkflowTypeTOList().add(sysWorkflowTypeTO);
        Collections.sort(sysWorkflowTypeBean.getSysWorkflowTypeTOList());

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('workflowTypeAddModal').hide()");
        requestContext.execute("PF('sysWorkflowTypeDataTable').clearFilters()");
        requestContext.update("sysWorkflowTypeListForm");
        FacesContext.getCurrentInstance().addMessage(
                "MainMessages",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Workflow type '" + sysWorkflowTypeTO.getWorkflowType() + "' now available in module '"
                        + sysWorkflowTypeTO.getModuleName() + "'", ""));
    }

    public void prepareEditing(SysWorkflowTypeTO sysWorkflowTypeTO) {
        try {
            sysWorkflowTypeBean.setSysWorkflowTypeTO((SysWorkflowTypeTO) sysWorkflowTypeTO.clone());
            sysWorkflowTypeBean.setSysWorkflowStageTypeTO(new SysWorkflowStageTypeTO());
            sysWorkflowTypeBean.setUiFormTOList(uiFormService.getUIFormTOsForModule(sysWorkflowTypeTO.getModuleId()));
        } catch (CloneNotSupportedException cnse) {
        }

        menuBean.setPage("../SysWorkflowType/sysWorkflowTypeEdit.xhtml");
    }

    public void editSysWorkfowType() {
        SysWorkflowTypeTO editWorkflowTypeTO = sysWorkflowTypeBean.getSysWorkflowTypeTO();

        for (SysSubModuleTO sysSubModuleTO : sysWorkflowTypeBean.getSysSubModuleTOList()) {
            if (editWorkflowTypeTO.getModuleId().equals(sysSubModuleTO.getModuleId())) {
                editWorkflowTypeTO.setModuleName(sysSubModuleTO.getModuleName());
                break;
            }
        }

        boolean isValid = validateSysWorkflowType();

        if (!isValid) {
            return;
        }

        sysWorkflowTypeService.updateSysWorkflowType(editWorkflowTypeTO);

        int indexToBeReplaced = -1;
        for (int i = 0; i < sysWorkflowTypeBean.getSysWorkflowTypeTOList().size(); i++) {
            if (editWorkflowTypeTO.getWorkflowTypeID().equals(sysWorkflowTypeBean.getSysWorkflowTypeTOList().get(i).getWorkflowTypeID())) {
                indexToBeReplaced = i;
                break;
            }
        }
        sysWorkflowTypeBean.getSysWorkflowTypeTOList().remove(indexToBeReplaced);
        sysWorkflowTypeBean.getSysWorkflowTypeTOList().add(indexToBeReplaced, editWorkflowTypeTO);
        Collections.sort(sysWorkflowTypeBean.getSysWorkflowTypeTOList());
        try {
            sysWorkflowTypeBean.setSysWorkflowTypeTO((SysWorkflowTypeTO) editWorkflowTypeTO.clone());
        } catch (CloneNotSupportedException cnse) {
        }

        FacesContext.getCurrentInstance().addMessage(
                "MainMessages",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Workflow type '" + editWorkflowTypeTO.getWorkflowType() + "' now available in module '"
                        + editWorkflowTypeTO.getModuleName() + "'", ""));
        backFromEditing();
    }

    public void backFromEditing() {
        menuBean.setPage("../SysWorkflowType/sysWorkflowTypeList.xhtml");
    }

    private boolean validateSysWorkflowType() {
        boolean isValid = true;

        SysWorkflowTypeTO newWorkflowTypeTO = sysWorkflowTypeBean.getSysWorkflowTypeTO();

        if (newWorkflowTypeTO.getModuleId() == 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select a module", ""));
            isValid = false;
        }
        if (newWorkflowTypeTO.getWorkflowType().trim().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a workflow type", ""));
            isValid = false;
        }

        for (SysWorkflowTypeTO sysWorkflowTypeTO : sysWorkflowTypeBean.getSysWorkflowTypeTOList()) {
            if (newWorkflowTypeTO.getModuleId().equals(sysWorkflowTypeTO.getModuleId()) && newWorkflowTypeTO.getWorkflowType().equals(sysWorkflowTypeTO.getWorkflowType())) {
                String errorMessage = null;
                if (newWorkflowTypeTO.getWorkflowTypeID() == null) {
                    errorMessage = "Module '" + newWorkflowTypeTO.getModuleName() + "' already has a workflow type '" + sysWorkflowTypeTO.getWorkflowType() + "'";
                } else {
                    errorMessage = "This combination of module and workflow type already exists";
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, ""));
                isValid = false;
            }
        }

        return isValid;
    }

    public void newSysWorkflowStageType() {
        SysWorkflowStageTypeTO sysWorkflowStageTypeTO = new SysWorkflowStageTypeTO();
        sysWorkflowStageTypeTO.setWorkflowTypeID(sysWorkflowTypeBean.getSysWorkflowTypeTO().getWorkflowTypeID());
        sysWorkflowTypeBean.setSysWorkflowStageTypeTO(sysWorkflowStageTypeTO);
        sysWorkflowTypeBean.setDialogHeaderPrefix("NEW");
        sysWorkflowTypeBean.setDialogButtonName("SAVE");
    }

    public void newSysWorkflowStageTypeGroup() {
        SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO = new SysWorkflowStageTypeGroupTO();
        sysWorkflowStageTypeGroupTO.setWorkflowTypeID(sysWorkflowTypeBean.getSysWorkflowTypeTO().getWorkflowTypeID());
        sysWorkflowTypeBean.setSysWorkflowStageTypeGroupTO(sysWorkflowStageTypeGroupTO);
        sysWorkflowTypeBean.setDialogHeaderPrefix("NEW");
        sysWorkflowTypeBean.setDialogButtonName("SAVE");
    }

    public void addOrUpdateSysWorkflowStageType() {

        SysWorkflowStageTypeTO sysWorkflowStageTypeTO = sysWorkflowTypeBean.getSysWorkflowStageTypeTO();
        for (UIFormTO uiFormTO : sysWorkflowTypeBean.getUiFormTOList()) {
            if (sysWorkflowStageTypeTO.getUiFormID().equals(uiFormTO.getFormID())) {
                sysWorkflowStageTypeTO.setFormName(uiFormTO.getFormName());
                break;
            }
        }

        if (sysWorkflowStageTypeTO.getWorkflowStageTypeGroupID() == 0) {
            sysWorkflowStageTypeTO.setWorkflowStageTypeGroupName(null);
        } else {
            for (SysWorkflowStageTypeGroupTO workflowStageTypeGroupTO : sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeGroupTOs()) {
                if (sysWorkflowStageTypeTO.getWorkflowStageTypeGroupID().equals(workflowStageTypeGroupTO.getWorkflowStageTypeGroupID())) {
                    sysWorkflowStageTypeTO.setWorkflowStageTypeGroupName(workflowStageTypeGroupTO.getWorkflowStageTypeGroupName());
                    break;
                }
            }
        }

        if (!validateSysWorkflowStageType()) {
            return;
        }

        if (sysWorkflowStageTypeTO.getWorkflowStageTypeID() != null) {
            Iterator<SysWorkflowStageTypeTO> stageTypesIterator = sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeTOs().iterator();
            while (stageTypesIterator.hasNext()) {
                if (stageTypesIterator.next().getWorkflowStageTypeID().equals(sysWorkflowStageTypeTO.getWorkflowStageTypeID())) {
                    stageTypesIterator.remove();
                    break;
                }
            }
        }

        sysWorkflowTypeService.addOrUpdateSysWorkflowStageType(sysWorkflowStageTypeTO);

        sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeTOs().add(sysWorkflowStageTypeTO);
        Collections.sort(sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeTOs());

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('newWorkflowStageTypeDialog').hide()");
        requestContext.execute("PF('sysWorkflowStageTypeDataTable').clearFilters()");
        requestContext.update("updateWorkflowTypeForm");

        FacesContext.getCurrentInstance().addMessage(
                "MainMessages",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Workflow stage type '" + sysWorkflowStageTypeTO.getWorkflowStageType() + "' now available for form '"
                        + sysWorkflowStageTypeTO.getFormName() + "'", ""));
    }

    public void addOrUpdateSysWorkflowStageTypeGroup() {

        SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO = sysWorkflowTypeBean.getSysWorkflowStageTypeGroupTO();

        if (!validateSysWorkflowStageTypeGroup()) {
            return;
        }

        if (sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID() != null) {
            Iterator<SysWorkflowStageTypeGroupTO> stageTypeGroupsIterator = sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeGroupTOs().iterator();
            while (stageTypeGroupsIterator.hasNext()) {
                if (stageTypeGroupsIterator.next().getWorkflowStageTypeGroupID().equals(sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID())) {
                    stageTypeGroupsIterator.remove();
                    break;
                }
            }
        }

        sysWorkflowTypeService.addOrUpdateSysWorkflowStageTypeGroup(sysWorkflowStageTypeGroupTO);

        sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeGroupTOs().add(sysWorkflowStageTypeGroupTO);
        Collections.sort(sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeGroupTOs());

        for (SysWorkflowStageTypeTO stageTypeTO : sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeTOs()) {
            if (stageTypeTO.getWorkflowStageTypeGroupID().equals(sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID())) {
                stageTypeTO.setWorkflowStageTypeGroupName(sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupName());
            }
        }

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('newWorkflowStageTypeGroupDialog').hide()");
        requestContext.execute("PF('sysWorkflowStageTypeGroupDataTable').clearFilters()");
        requestContext.update("updateWorkflowTypeForm");

        FacesContext.getCurrentInstance().addMessage(
                "MainMessages",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Workflow stage type group '" + sysWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupName()
                        + "' now available for workflow type '" + sysWorkflowTypeBean.getSysWorkflowTypeTO().getWorkflowType() + "'", ""));
    }

    private boolean validateSysWorkflowStageType() {
        boolean isValid = true;
        SysWorkflowStageTypeTO newWorkflowStageTypeTO = sysWorkflowTypeBean.getSysWorkflowStageTypeTO();

        if (newWorkflowStageTypeTO.getUiFormID() == 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select a form", ""));
            isValid = false;
        }
        if (newWorkflowStageTypeTO.getWorkflowStageType().trim().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a workflow stage type", ""));
            isValid = false;
        }

        if (isValid) {
            for (SysWorkflowStageTypeTO sysWorkflowStageTypeTO : sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeTOs()) {
                if (newWorkflowStageTypeTO.compareTo(sysWorkflowStageTypeTO) == 0) {
                    String errorMessage = null;
                    if (newWorkflowStageTypeTO.getWorkflowStageTypeID() == null) {
                        errorMessage = "Form '" + newWorkflowStageTypeTO.getFormName() + "' already has a stage type '" + newWorkflowStageTypeTO.getWorkflowStageType() + "'";
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, ""));
                        isValid = false;
                    } else if (newWorkflowStageTypeTO.getWorkflowStageTypeGroupID().equals(sysWorkflowStageTypeTO.getWorkflowStageTypeGroupID())) {
                        errorMessage = "This combination of form name , workflow stage type and group already exists";
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, ""));
                        isValid = false;
                    }
                }
            }
        }

        return isValid;
    }

    private boolean validateSysWorkflowStageTypeGroup() {
        boolean isValid = true;
        SysWorkflowStageTypeGroupTO newWorkflowStageTypeGroupTO = sysWorkflowTypeBean.getSysWorkflowStageTypeGroupTO();

        if (newWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupName().trim().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please enter a workflow stage type group", ""));
            isValid = false;
        }

        if (isValid) {
            for (SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO : sysWorkflowTypeBean.getSysWorkflowTypeTO().getSysWorkflowStageTypeGroupTOs()) {
                if (newWorkflowStageTypeGroupTO.compareTo(sysWorkflowStageTypeGroupTO) == 0) {
                    String errorMessage = null;
                    if (newWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupID() == null) {
                        errorMessage = "Workflow type '" + sysWorkflowTypeBean.getSysWorkflowTypeTO().getWorkflowType() + "' already has a stage type group '"
                                + newWorkflowStageTypeGroupTO.getWorkflowStageTypeGroupName() + "'";
                    } else {
                        errorMessage = "This workflow stage type group already exists";
                    }
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, ""));
                    isValid = false;
                }
            }
        }

        return isValid;
    }

    public void prepareEditing(SysWorkflowStageTypeTO sysWorkflowStageTypeTO) {
        try {
            sysWorkflowTypeBean.setSysWorkflowStageTypeTO((SysWorkflowStageTypeTO) sysWorkflowStageTypeTO.clone());
            sysWorkflowTypeBean.setDialogHeaderPrefix("EDIT");
            sysWorkflowTypeBean.setDialogButtonName("UPDATE");
        } catch (CloneNotSupportedException cnse) {
        }
    }

    public void prepareEditing(SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO) {
        try {
            sysWorkflowTypeBean.setSysWorkflowStageTypeGroupTO((SysWorkflowStageTypeGroupTO) sysWorkflowStageTypeGroupTO.clone());
            sysWorkflowTypeBean.setDialogHeaderPrefix("EDIT");
            sysWorkflowTypeBean.setDialogButtonName("UPDATE");
        } catch (CloneNotSupportedException cnse) {
        }
    }
}
