/**
 * 
 */
package com.talentpact.ui.sysMeshTagMapping.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.business.application.transport.output.SysMeshTagMappingTO;
import com.talentpact.business.application.transport.output.SysMeshTagType;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.constants.ISysContentCategoryConstants;
import com.talentpact.business.common.constants.ISysMeshTagMapping;
import com.talentpact.business.dataservice.SysContentCategory.SysContentCategoryService;
import com.talentpact.business.dataservice.SysMeshTagMapping.SysMeshTagMappingService;
import com.talentpact.business.dataservice.SysOfferingCategory.SysOfferingCategoryService;
import com.talentpact.model.SysMeshTagMapping;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysContentCategory.bean.SysContentCategoryBean;
import com.talentpact.ui.sysMeshTagMapping.bean.SysMeshTagMappingBean;


@Named("sysMeshTagMappingController")
@ConversationScoped
public class SysMeshTagMappingController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysMeshTagMappingController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysMeshTagMappingBean sysMeshTagMappingBean;

    @Inject
    SysMeshTagMappingService sysMeshTagMappingService;

    @Override
    public void initialize()
    {
    	List<SysMeshTagMappingTO> sysMeshTagMappingTOList = null;
        Set<String> name = null;
        SysMeshTagMappingTO sysMeshTagMappingTO = null;

        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
//            resetSysMeshTagMapping();
            sysMeshTagMappingTOList = sysMeshTagMappingService.getSysMeshTagMappingList();
            sysMeshTagMappingBean.setSysMeshTagMappingTOList(sysMeshTagMappingTOList);
            sysMeshTagMappingBean.setSysMeshModules(sysMeshTagMappingService.getSysMeshModules());
            sysMeshTagMappingBean.setSysMeshTagTypes(getSysMeshTagTypes());
            menuBean.setPage("../SysMeshTagMapping/sysMeshTagMapping.xhtml");
            requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_DATATABLE_RESET);
            requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public void resetSysMeshTagMapping() {
		sysMeshTagMappingBean.setSysMeshTagModuleID(null);
		sysMeshTagMappingBean.setSysMeshTagType(null);
	}   
	
	public void refreshPopUp(SysMeshTagMappingTO sysMeshTagMappingTO)
    {
	    try {
	        sysMeshTagMappingBean.setSysMeshModuleName(sysMeshTagMappingTO.getSysMeshTagModuleName());
	        sysMeshTagMappingBean.setSysMeshTagMappingID(sysMeshTagMappingTO.getSysMeshTagMappingID());
	        sysMeshTagMappingBean.setSysMeshTagModuleID(sysMeshTagMappingTO.getSysMeshModuleID());
	        sysMeshTagMappingBean.setSysMeshTagType(sysMeshTagMappingTO.getSysMeshTagType());
	        } catch (Exception e) {
	          e.printStackTrace();  	
	        }
    }
	
	public void updateSysMeshTagMapping()
    {
		SysMeshTagMappingTO sysMeshTagMappingTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysMeshTagMappingTO = new SysMeshTagMappingTO();
            sysMeshTagMappingTO.setSysMeshTagMappingID(sysMeshTagMappingBean.getSysMeshTagMappingID());
            sysMeshTagMappingTO.setSysMeshModuleID(sysMeshTagMappingBean.getSysMeshTagModuleID());
            sysMeshTagMappingTO.setSysMeshTagType(sysMeshTagMappingBean.getSysMeshTagType());
            String moduleName = validateSysMeshTagMapping(sysMeshTagMappingTO);
            if (moduleName == null) {
                sysMeshTagMappingService.editSysMeshTagMapping(sysMeshTagMappingTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysMeshTagMapping updated successfully.", ""));
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_DATATABLE_RESET);
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_PAGINATION_DATATABLE_RESET);
                RequestContext.getCurrentInstance().update("sysMeshTagMappingListForm");
                sysMeshTagMappingBean.setRenderErrorMessage(true);

            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "" + moduleName+" with TagType "+sysMeshTagMappingTO.getSysMeshTagType()
                                + " already exists.", ""));
                sysMeshTagMappingBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("sysMeshTagMappingEditForm");
            }
         } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysMeshTagMapping.SYSMESHTAGMAPPING_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysMeshTagMappingBean.setRenderSysMeshTagMappingPopup(false);
            requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("sysMeshTagMappingEditForm");
        }
    }
	
	public void addSysMeshTagMapping()
    {
        SysMeshTagMappingTO sysMeshTagMappingTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysMeshTagMappingTO = new SysMeshTagMappingTO();
            sysMeshTagMappingTO.setSysMeshModuleID(sysMeshTagMappingBean.getSysMeshTagModuleID());
            sysMeshTagMappingTO.setSysMeshTagModuleName(sysMeshTagMappingBean.getSysMeshModuleName());
            sysMeshTagMappingTO.setSysMeshTagType(sysMeshTagMappingBean.getSysMeshTagType());
            sysMeshTagMappingBean.setRenderErrorMessage(true);
            String modulename = validateSysMeshTagMapping(sysMeshTagMappingTO);
            if (modulename == null) {
            	sysMeshTagMappingService.addSysMeshTagMapping(sysMeshTagMappingTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysMeshTagMapping saved successfully.", ""));
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_ADD_DIALOG_HIDE);
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_DATATABLE_RESET);
                requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysMeshTagMappingListForm");
                sysMeshTagMappingBean.setRenderErrorMessage(true);
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "" + modulename+" with TagType "+sysMeshTagMappingTO.getSysMeshTagType() 
                                + " already exists.", ""));
                sysMeshTagMappingBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("addSysMeshTagMappingForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysMeshTagMapping.SYSMESHTAGMAPPING_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysMeshTagMappingBean.setRenderSysMeshTagMappingPopup(false);
            requestContext.execute(ISysMeshTagMapping.SYSMESHTAGMAPPING_ADD_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("addSysMeshTagMappingForm");
        }
    }
	
	private String validateSysMeshTagMapping(SysMeshTagMappingTO sysMeshTagMappingTO) {
		String moduleName = null;
	        try {
	        	moduleName = sysMeshTagMappingService.checkExist(sysMeshTagMappingTO.getSysMeshModuleID(), sysMeshTagMappingTO.getSysMeshTagType());
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return moduleName;
	}

	private List<SysMeshTagType> getSysMeshTagTypes()
	{
		List<SysMeshTagType> result = new ArrayList<>();
		SysMeshTagType sysMeshTagType = new SysMeshTagType();
		sysMeshTagType.setSysMeshTagTypeID(0);
		sysMeshTagType.setSysMeshTagTypeName("Inbound");
		result.add(sysMeshTagType);
		sysMeshTagType = new SysMeshTagType();
		sysMeshTagType.setSysMeshTagTypeID(1);
		sysMeshTagType.setSysMeshTagTypeName("Outbound");
		result.add(sysMeshTagType);
		return result;
	}
}