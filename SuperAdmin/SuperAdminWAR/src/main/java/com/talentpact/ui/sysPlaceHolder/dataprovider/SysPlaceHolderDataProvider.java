/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.dataprovider;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysPlaceHolder.dataservice.SysPlaceHolderDataService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPlaceHolderDataProvider {

	@EJB
	private SysPlaceHolderDataService sysPlaceHolderDataService;

	/**
	 * @return
	 */
	public ServiceResponse getAllPlaceHolderData() 
			throws Exception {
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataService.getAllPlaceHolderData();
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}finally{

		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse insertNewPlaceHolder(ServiceRequest serviceRequest) 
		throws Exception{
		ServiceResponse serviceResponse = null;
		try{
			serviceResponse = sysPlaceHolderDataService.insertNewPlaceHolder(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}finally{
			
		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse deletePlaceHolderByID(ServiceRequest serviceRequest) 
		throws Exception{
		ServiceResponse serviceResponse = null; 
		try{
			serviceResponse = sysPlaceHolderDataService.deletePlaceHolderByID(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
		}finally{
			
		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse updateNewPlaceHolder(ServiceRequest serviceRequest) 
			throws Exception {
		ServiceResponse serviceResponse = null; 
		try{
			serviceResponse = sysPlaceHolderDataService.updateNewPlaceHolder(serviceRequest);
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}finally{
			
		}
		return serviceResponse;
	}

}
