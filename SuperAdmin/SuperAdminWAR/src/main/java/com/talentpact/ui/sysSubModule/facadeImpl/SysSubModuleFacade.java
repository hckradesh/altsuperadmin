/**
 * 
 */
package com.talentpact.ui.sysSubModule.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.talentpact.business.common.facade.impl.CommonFacade;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.sysSubModule.ISysSubModuleRemote;
import com.talentpact.ui.sysSubModule.constants.ISysSubModuleConstants;
import com.talentpact.ui.sysSubModule.dataservice.SysSubModuleDataService;
import com.talentpact.ui.sysSubModule.exception.SysSubModuleException;
import com.talentpact.ui.sysSubModule.facaderemote.SysSubModuleLocalFacade;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value =  TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
@Local(SysSubModuleLocalFacade.class)
public class SysSubModuleFacade extends CommonFacade implements SysSubModuleLocalFacade{
    private static final Logger LOGGER = LoggerFactory.getLogger(SysSubModuleFacade.class);
	
    @EJB
	SysSubModuleDataService sysSubModuleDataService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.talentpact.ui.sysSubModule.facaderemote.SysSubModuleLocalFacade#
	 * getParentModuleAndSubModules
	 * (com.talentpact.business.common.service.input.ServiceRequest)
	 */
	@Override
	public ServiceResponse getParentModuleAndSubModules(
			ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		try {
			serviceResponse = sysSubModuleDataService
					.getParentModuleAndSubModules(serviceRequest);
		} catch (Exception ex) {
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}
	
	@Override
	public ServiceResponse getAllSysSubModuleList(ServiceRequest serviceRequest) throws SysSubModuleException {
        ServiceResponse response = null;
        try{
            response = sysSubModuleDataService.getAllSysSubModuleList(serviceRequest);
        }catch (Exception ex) {
            LOGGER.error("", ex);
            throw new SysSubModuleException(ISysSubModuleConstants.SYS_SUB_MODULE_RETRIEVE_ERROR_MSG);
        }
        return response;
    }
}
