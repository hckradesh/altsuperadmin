/**
 * 
 */
package com.talentpact.ui.tenants.organization.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.talentpact.business.application.transport.input.AppRequestTO;
import com.talentpact.business.application.transport.input.OrgAppPortalTreeRequestTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.output.AppResponseTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.tenants.transport.newtenant.input.TpURLTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tpModule.transport.input.TpModuleRequestTO;
import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.business.tpOrganization.transport.input.AppPortalTypeUrlRequestTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.ui.tenants.organization.bean.NewOrganizationBean;

/**
 * @author radhamadhab.dalai
 *
 */
public class OrganizationHelper
{


    public static List<SelectItem> convertAppsSelectItem(List<AppRequestTO> appTOList)
    {

        List<SelectItem> appList = null;
        SelectItem item = null;
        try {

            if (appTOList != null && appTOList.size() != 0) {

                appList = new ArrayList<SelectItem>();
                for (AppRequestTO appRequestTO : appTOList) {
                    item = new SelectItem();
                    item.setLabel(appRequestTO.getAppName());
                    item.setValue(appRequestTO.getAppId());
                    appList.add(item);
                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return appList;
    }


    public static List<SelectItem> convertMetaDataSelectItem(List<String> dbList)
    {

        List<SelectItem> dbSelectList = null;
        SelectItem item = null;
        try {

            if (dbList != null && dbList.size() != 0) {

                dbSelectList = new ArrayList<SelectItem>();
                for (String dbStr : dbList) {
                    item = new SelectItem();
                    item.setLabel(dbStr);
                    item.setValue(dbStr);
                    dbSelectList.add(item);
                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dbSelectList;
    }


    public static List<SelectItem> convertTenantTOSelectItem(List<NewTenantResponseTO> tenantTOList)
    {

        List<SelectItem> selectTenantTOList = null;
        SelectItem item = null;
        try {

            if (tenantTOList != null && tenantTOList.size() != 0) {

                selectTenantTOList = new ArrayList<SelectItem>();
                for (NewTenantResponseTO tenantTo : tenantTOList) {
                    item = new SelectItem();
                    item.setLabel(tenantTo.getTenantName());
                    item.setValue(tenantTo.getTenantID());
                    selectTenantTOList.add(item);
                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return selectTenantTOList;
    }


    /*Comvert User Input Into RequestTo for saving Org Data*/
    public static OrgAppPortalTreeRequestTO convertUserInputToRequestTO(OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO)
    {
        OrgAppPortalTreeRequestTO orgAppPortalTreeRequestTO = new OrgAppPortalTreeRequestTO();
        /*RequestTO List*/
        List<AppPortalTypeUrlRequestTO> portalReqTOList = new ArrayList<AppPortalTypeUrlRequestTO>();
        List<TpModuleRequestTO> moduleReqToList = new ArrayList<TpModuleRequestTO>();
        /*User Input  Data*/
        Map<Long, List<TpModuleResponseTO>> moduleInputToMap = orgAppPortalTreeResponseTO.getAppModuleMap();
        Map<Long, List<AppPortalTypeUrlResponseTO>> portalInputTOMap = orgAppPortalTreeResponseTO.getAppPortalUrlMap();

        Map<Long, AppResponseTO> appIdDatabaseMap = new HashMap<Long, AppResponseTO>();
        for (AppResponseTO appResTo : orgAppPortalTreeResponseTO.getAppList()) {
            appIdDatabaseMap.put(appResTo.getAppId(), appResTo);
        }

        AppPortalTypeUrlRequestTO appPortalTO = null;
        /*Traverse All App List*/
        for (Entry<Long, List<AppPortalTypeUrlResponseTO>> listEntry : portalInputTOMap.entrySet()) {
            for (AppPortalTypeUrlResponseTO portalTO : listEntry.getValue()) {

                appPortalTO = new AppPortalTypeUrlRequestTO();
                appPortalTO.setPortalTypeId(portalTO.getPortalTypeId());
                appPortalTO.setUrl(portalTO.getUrl());

                Long appId = listEntry.getKey();

                appPortalTO.setAppId(appId);
                appPortalTO.setLoginPage(portalTO.getLoginPage());
                appPortalTO.setHomePage(portalTO.getHomePage());
                appPortalTO.setLoginPageTheme(portalTO.getLoginPageTheme());
                appPortalTO.setDefaultTheme(portalTO.getDefualtTheme());
                AppResponseTO appResTo = null;
                if (portalTO.isPortalChecked()) {
                    appPortalTO.setPortalChecked(true);
                    appResTo = appIdDatabaseMap.get(appId);
                    appPortalTO.setDatabase(appResTo.getDatabase());
                    appPortalTO.setFavicon(appResTo.getFavicon());
                } else {
                    appPortalTO.setPortalChecked(false);
                }

                portalReqTOList.add(appPortalTO);


            }
        }

        /*Traverse All Module List*/
        TpModuleRequestTO moduleReTO = null;
        for (Entry<Long, List<TpModuleResponseTO>> moduleInputTOentry : moduleInputToMap.entrySet()) {
            for (TpModuleResponseTO moduleTO : moduleInputTOentry.getValue()) {

                moduleReTO = new TpModuleRequestTO();
                moduleReTO.setModuleID(moduleTO.getModuleID());
                moduleReTO.setAppID(moduleInputTOentry.getKey());
                if (moduleTO.isModuleChecked()) {
                    moduleReTO.setActive(true);
                } else {
                    moduleReTO.setActive(false);
                }
                moduleReqToList.add(moduleReTO);


            }
        }

        orgAppPortalTreeRequestTO.setAppPortalUrlList(portalReqTOList);
        orgAppPortalTreeRequestTO.setAppModuleList(moduleReqToList);
        return orgAppPortalTreeRequestTO;
    }


    public static List<SelectItem> convertAlredaySelectedAppsItem(List<AppRequestTO> appTOList)
    {

        List<SelectItem> appList = null;
        SelectItem item = null;
        try {

            if (appTOList != null && appTOList.size() > 0) {

                appList = new ArrayList<SelectItem>();
                for (AppRequestTO appRequestTO : appTOList) {
                    if (appRequestTO.getIsSelected()) {
                        item = new SelectItem();
                        item.setLabel(appRequestTO.getAppName());
                        item.setValue(appRequestTO.getAppId());
                        appList.add(item);
                    }

                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return appList;
    }


    public List<SelectItem> convertTPUrlSelectItem(List<TpURLTO> TpURLTOList)
    {

        List<SelectItem> tpURLList = null;
        SelectItem item = null;
        try {

            if (TpURLTOList != null && TpURLTOList.size() != 0) {

                tpURLList = new ArrayList<SelectItem>();
                for (TpURLTO tpURLTO : TpURLTOList) {

                    item = new SelectItem();

                    item.setLabel(tpURLTO.getUrl());
                    item.setValue(tpURLTO.getUrlID());
                    tpURLList.add(item);


                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tpURLList;
    }


    public static List<NewTenantResponseTO> markTenantTOSelected(List<NewTenantResponseTO> tenantTOList, Long tenantId)
    {
        if (tenantId != null) {
            for (NewTenantResponseTO tpTenantTO : tenantTOList) {

                if (tpTenantTO.getTenantID().equals(tenantId)) {
                    tpTenantTO.setIsSelected(true);
                }
            }
        }

        return tenantTOList;

    }


    public static List<SelectItem> convertValuesIntoSelectItemType(List<String> constantList)
    {

        List<SelectItem> constantSelectList = null;
        SelectItem item = null;
        try {

            if (constantList != null && constantList.size() != 0) {
                constantSelectList = new ArrayList<SelectItem>();
                for (String constantStr : constantList) {
                    item = new SelectItem();
                    item.setLabel(constantStr);
                    item.setValue(constantStr);
                    constantSelectList.add(item);


                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return constantSelectList;
    }


    public static Map<Long, List<SysMenuTO>> prepareMenuListForCache(List<SysMenuTO> sysMenuTOlist)
        throws Exception
    {
        Map<Long, List<SysMenuTO>> menuCacheMap = null;
        List<SysMenuTO> nullList = null;
        try {
            nullList = new ArrayList<SysMenuTO>();
            menuCacheMap = new HashMap<Long, List<SysMenuTO>>();
            if (sysMenuTOlist == null || sysMenuTOlist.isEmpty()) {
                return menuCacheMap;
            }
            for (SysMenuTO menu : sysMenuTOlist) {
                if (menu.getParentMenuID() == null) {
                    nullList.add(menu);
                }
            }

            menuCacheMap.put((long) 0, nullList);
            for (SysMenuTO menu : nullList) {
                prepareMenuMap(sysMenuTOlist, menu, menuCacheMap);
            }


            return menuCacheMap;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            menuCacheMap = null;
            nullList = null;
        }
    }


    private static void prepareMenuMap(List<SysMenuTO> sysMenuTOlist, SysMenuTO menu, Map<Long, List<SysMenuTO>> menuCacheMap)
        throws Exception
    {
        List<SysMenuTO> childList = null;

        try {
            childList = findChildMenuList(menu, sysMenuTOlist);
            if (childList == null | childList.isEmpty()) {
                menu.setChild(true);
                return;
            } else {
                menu.setChild(false);
                menuCacheMap.put(menu.getMenuID(), childList);
                for (SysMenuTO menu1 : childList) {
                    prepareMenuMap(sysMenuTOlist, menu1, menuCacheMap);
                }
            }

        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }

    }


    private static List<SysMenuTO> findChildMenuList(SysMenuTO menu, List<SysMenuTO> sysMenuTOlist)
        throws Exception
    {
        List<SysMenuTO> childList = null;
        try {
            childList = new ArrayList<SysMenuTO>();
            for (SysMenuTO menu1 : sysMenuTOlist) {
                if (menu1.getParentMenuID() != null) {
                    if (menu.getMenuID() == menu1.getParentMenuID()) {
                        childList.add(menu1);
                    }
                }
            }
            return childList;
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {

        }
    }


    public static TreeNode createMenuTree(Map<Long, List<SysMenuTO>> menuMap)
        throws Exception
    {
        TreeNode menuTree = null;
        TreeNode treeNode;
        try {
            menuTree = new DefaultTreeNode(null, null);
            List<SysMenuTO> menuTOList = menuMap.get(0L);
            if (menuTOList != null && !menuTOList.isEmpty()) {
                for (SysMenuTO menuTO : menuTOList) {
                    treeNode = createMenuTree1(menuTO, menuMap);
                    menuTree.getChildren().add(treeNode);
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return menuTree;
    }


    private static TreeNode createMenuTree1(SysMenuTO item, Map<Long, List<SysMenuTO>> menuMap)
        throws Exception
    {
        try {
            if (menuMap == null || menuMap.isEmpty()) {
                return null;
            }
            long parentId = item.getMenuID();
            List<SysMenuTO> menuTOlist = menuMap.get(parentId);
            if (!menuMap.containsKey(parentId)) {
                TreeNode node = new DefaultTreeNode(item, null);
                if (item.isSelected()) {
                    node.setSelected(true);
                }
                return node;
            }
            if (menuTOlist == null || menuTOlist.isEmpty()) {
                TreeNode node = new DefaultTreeNode(item, null);
                if (item.isSelected()) {
                    node.setSelected(true);
                }
                return node;
            } else {
                TreeNode treeNode = new DefaultTreeNode(item, null);
                if (item.isSelected()) {
                    treeNode.setSelected(true);
                }
                List<SysMenuTO> menuList = menuMap.get(parentId);
                for (SysMenuTO menuTO : menuList) {

                    TreeNode tmp = createMenuTree1(menuTO, menuMap);
                    treeNode.getChildren().add(tmp);
                }
                return treeNode;

            }
        } catch (Exception e) {

            throw e;
        }
    }


    public static void getSysMenuTOlistFromMenuTree(TreeNode menuTree, List<SysMenuTO> sysMenuTOlist, TreeNode[] selectedMenuNodes)
        throws Exception
    {

        List<TreeNode> childNodeList = null;
        try {
            if (menuTree == null) {
                return;
            }
            if (menuTree.isLeaf()) {
                SysMenuTO to = (SysMenuTO) menuTree.getData();
                if (isNodeSelected(menuTree, selectedMenuNodes)) {
                    to.setSelected(true);
                } else {
                    to.setSelected(false);
                }
                sysMenuTOlist.add(to);
                return;
            }

            childNodeList = menuTree.getChildren();
            for (TreeNode node : childNodeList) {
                getSysMenuTOlistFromMenuTree(node, sysMenuTOlist, selectedMenuNodes);
            }
            SysMenuTO to1 = (SysMenuTO) menuTree.getData();
            if (to1 != null) {
                if (isNodeSelectable(menuTree, selectedMenuNodes)) {
                    to1.setSelected(true);
                    menuTree.setSelected(true);
                } else {
                    to1.setSelected(false);
                    menuTree.setSelected(false);
                }
                sysMenuTOlist.add(to1);
            }


        } catch (Exception e) {
            throw e;
        }


    }


    private static boolean isNodeSelected(TreeNode menuTree, TreeNode[] selectedMenuNodes)
    {
        if (selectedMenuNodes == null || selectedMenuNodes.length == 0) {
            return false;
        } else {
            boolean flag = false;
            int idc = (int) ((SysMenuTO) menuTree.getData()).getMenuID();
            for (int i = 0; i < selectedMenuNodes.length; i++) {
                int id = (int) ((SysMenuTO) selectedMenuNodes[i].getData()).getMenuID();
                if (id == idc) {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

    }


    private static boolean isNodeSelectable(TreeNode menuTree, TreeNode[] selectedMenuNodes)
    {
        if (isNodeSelected(menuTree, selectedMenuNodes)) {
            return true;
        } else {
            List<TreeNode> childTreeNodeList = menuTree.getChildren();
            boolean flag = false;
            for (TreeNode node : childTreeNodeList) {

                if (isNodeSelected(node, selectedMenuNodes)) {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

    }


    public static Map<Long, TreeNode> convertIntoModuleTreeWithAppMap(Map<Long, List<TpModuleResponseTO>> appModuleMap, OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO)
        throws Exception
    {
        Map<Long, TreeNode> moduleTreeWithAppMap = null;
        //Long appId1 : portalTOMap.keySet()
        try {

            moduleTreeWithAppMap = new HashMap<Long, TreeNode>();
            //  orgAppPortalTreeResponseTO = newOrganizationBean.getOrgAppPortalTreeTO();
            List<AppResponseTO> appList = orgAppPortalTreeResponseTO.getAppList();
            for (AppResponseTO arpTO : appList) {

                List<TpModuleResponseTO> moduleList = appModuleMap.get(arpTO.getAppId());

                TreeNode moduleTree = new DefaultTreeNode(null, null);
                if (!(moduleList == null || moduleList.isEmpty())) {
                    List<TpModuleResponseTO> headModuleList = findHeadModuleListByApp(arpTO.getAppId(), moduleList);
                    for (TpModuleResponseTO head : headModuleList) {
                        List<TpModuleResponseTO> childList = getChildModuleList(head.getModuleID(), moduleList);
                        TreeNode hnode = new DefaultTreeNode(head, null);
                        for (TpModuleResponseTO tc : childList) {
                            TreeNode cnode = new DefaultTreeNode(tc, null);

                            if (tc.isModuleChecked()) {
                                cnode.setSelected(true);
                            } else {
                                cnode.setSelected(false);
                            }
                            hnode.getChildren().add(cnode);
                        }
                        if (isPartialOrFullSelected(hnode)) {
                            hnode.setSelected(true);
                            hnode.setPartialSelected(false);
                        } else {
                            hnode.setSelected(false);
                            hnode.setPartialSelected(true);
                        }
                        moduleTree.getChildren().add(hnode);
                    }
                }

                // moduleTreeWithAppMap.put(arpTO.getAppId(), moduleTree);
                arpTO.setTree(moduleTree);
                arpTO.setSelectedNodes(new TreeNode[0]);
            }


            return moduleTreeWithAppMap;
        } catch (Exception e) {
            throw e;
        }
    }

    private static boolean isPartialOrFullSelected(TreeNode tn)
    {
        int count = tn.getChildCount();
        if (count == 0) {
            return false;
        }
        List<TreeNode> childList = tn.getChildren();
        int selectedCount = 0;
        for (TreeNode tne : childList) {
            if (tne.isSelected() == true) {
                selectedCount++;
            }
        }

        if (count == selectedCount) {
            return true;
        } else {
            return false;
        }

    }

    private static List<TpModuleResponseTO> getChildModuleList(Long moduleID, List<TpModuleResponseTO> moduleList)
    {
        List<TpModuleResponseTO> childList = new ArrayList<TpModuleResponseTO>();
        for (TpModuleResponseTO m : moduleList) {
            if (m.getParentSysSubModuleID() != null) {
                int tc = m.getParentSysSubModuleID();
                if (moduleID.equals(new Long(tc)) == true) {
                    childList.add(m);
                }
            }
        }
        return childList;
    }

    private static List<TpModuleResponseTO> findHeadModuleListByApp(Long appId, List<TpModuleResponseTO> moduleList)
    {
        List<TpModuleResponseTO> selectedModuleList = new ArrayList<TpModuleResponseTO>();
        for (TpModuleResponseTO t : moduleList) {
            if (t.getParentSysSubModuleID() == null) {
                selectedModuleList.add(t);
            }
        }
        return selectedModuleList;
    }


    public static void getAppModuleMapFromModuleTreeMap(Map<Long, TreeNode> moduleTreeWithAppMap, Map<Long, List<TpModuleResponseTO>> appModuleMap,
            NewOrganizationBean newOrganizationBean)
        throws Exception
    {
        OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = newOrganizationBean.getOrgAppPortalTreeTO();

        List<AppResponseTO> appList = orgAppPortalTreeResponseTO.getAppList();

        try {

            for (AppResponseTO aprTO : appList) {

                List<TpModuleResponseTO> modueList = appModuleMap.get(aprTO.getAppId());
                if (!(modueList == null || modueList.isEmpty())) {


                    TreeNode moduleTree = aprTO.getTree();
                    //  Map<Long, TreeNode[]> selectedNodesByApp = newOrganizationBean.getSelectedNodeByApp();
                    TreeNode[] selectedNodes = aprTO.getSelectedNodes();

                    for (TpModuleResponseTO mto : modueList) {
                        Boolean isSelected = false;
                        isModuleSelectedInTree(mto, moduleTree, isSelected);
                        if (isSelected == true) {
                            mto.setModuleChecked(true);
                        } else {
                            mto.setModuleChecked(false);
                        }
                    }
                }
                //  moduleTree. 

            }

        } catch (Exception e) {
            throw e;
        }
    }

    private static void isModuleSelectedInTree(TpModuleResponseTO mto, TreeNode moduleTree, Boolean isSelected)
    {

        if (moduleTree == null) {
            return;
        }
        TpModuleResponseTO mData = (TpModuleResponseTO) moduleTree.getData();
        if (mData != null) {
            long ic = mto.getModuleID();
            long lc = mData.getModuleID();
            if (ic == lc) {
                if (moduleTree.isSelected() || moduleTree.isPartialSelected()) {
                    isSelected = true;
                    ;
                }
            }
        }
        List<TreeNode> childList = moduleTree.getChildren();
        if (childList == null || childList.isEmpty()) {
            return;
        } else {
            for (TreeNode tn : childList) {
                isModuleSelectedInTree(mto, tn, isSelected);
            }
        }
    }


    public static void RemoveNonModuleHeaderMenus(Map<Long, List<TpModuleResponseTO>> appModuleMap, Map<Long, List<SysMenuTO>> menuMap)
        throws Exception
    {
        Set<Long> selectedModuleSet = null;
        selectedModuleSet = new HashSet<Long>();
        try {

            for (Long appID : appModuleMap.keySet()) {
                List<TpModuleResponseTO> moduleList = appModuleMap.get(appID);
                List<TpModuleResponseTO> headModuleList = findHeadModuleListByApp(appID, moduleList);

                for (TpModuleResponseTO tmRTO : headModuleList) {
                    if (tmRTO.isModuleChecked()) {
                        selectedModuleSet.add(tmRTO.getModuleID());
                    }
                }
            }
            List<SysMenuTO> headerSysMenuTOList = menuMap.get(0L);
            List<SysMenuTO> newHeaderSysMenuTOList = new ArrayList<SysMenuTO>();
            for (SysMenuTO sysMenuTO : headerSysMenuTOList) {
                if (selectedModuleSet.contains(sysMenuTO.getModuleID())) {
                    newHeaderSysMenuTOList.add(sysMenuTO);
                }
            }
            menuMap.put(0L, newHeaderSysMenuTOList);

        } catch (Exception e) {
            throw e;
        }

    }


    public static void reassignModuleName(OrgAppPortalTreeResponseTO orgAppPortalTreeTO)
        throws Exception
    {
        List<AppResponseTO> appList = orgAppPortalTreeTO.getAppList();
        Map<Long, List<TpModuleResponseTO>> appModuleMap = orgAppPortalTreeTO.getAppModuleMap();


        try {

            for (AppResponseTO arpTO : appList) {
                List<TpModuleResponseTO> moduleList = appModuleMap.get(arpTO.getAppId());
                if (!(moduleList == null || moduleList.isEmpty())) {

                    for (TpModuleResponseTO tmRTO : moduleList) {

                        if (tmRTO.getParentSysSubModuleID() != null) {

                            String newName = getParentStringForModule(moduleList, tmRTO);
                            tmRTO.setModuleName(newName);

                        }
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        }


    }


    private static String getParentStringForModule(List<TpModuleResponseTO> moduleList, TpModuleResponseTO tmRTO)
    {
        String parentString = tmRTO.getModuleName();
        int id = tmRTO.getParentSysSubModuleID();
        for (TpModuleResponseTO tmrTO : moduleList) {
            if (tmrTO.getModuleID().equals(new Long(id))) {

                parentString = parentString + "(" + tmrTO.getModuleName() + ")";
                break;
            }
        }
        return parentString;
    }


    public static String isParentNotSelected(OrgAppPortalTreeResponseTO orgAppPortalTreeTO)
        throws Exception
    {
        String parentModuleString = null;
        List<AppResponseTO> appList = orgAppPortalTreeTO.getAppList();
        Map<Long, List<TpModuleResponseTO>> appModuleMap = orgAppPortalTreeTO.getAppModuleMap();
        try {
            for (AppResponseTO arpTO : appList) {
                List<TpModuleResponseTO> moduleList = appModuleMap.get(arpTO.getAppId());
                if (!(moduleList == null || moduleList.isEmpty())) {
                    for (TpModuleResponseTO tmRTO : moduleList) {

                        if (tmRTO.getParentSysSubModuleID() != null) {

                            // String newName = getParentStringForModule(moduleList, tmRTO);
                            //  tmRTO.setModuleName(newName);
                            if (tmRTO.isModuleChecked()) {
                                TpModuleResponseTO parentTO = getParentOfModule(moduleList, tmRTO);
                                if (parentTO != null) {
                                    if (parentTO.isModuleChecked() == false) {
                                        parentModuleString = parentTO.getModuleName();
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    if (parentModuleString != null) {
                        break;
                    }
                }
            }

            return parentModuleString;
        } catch (Exception e) {
            throw e;
        }
    }


    private static TpModuleResponseTO getParentOfModule(List<TpModuleResponseTO> moduleList, TpModuleResponseTO tmRTO)
    {
        TpModuleResponseTO parentTO = null;
        int id = tmRTO.getParentSysSubModuleID();
        for (TpModuleResponseTO tmrTO : moduleList) {
            if (tmrTO.getModuleID().equals(new Long(id))) {

                parentTO = tmrTO;
                break;
            }
        }
        return parentTO;
    }

    public static List<SelectItem> convertValuesIntoSelectItemTypeForLoginPageTheme(List<SysConstantAndSysConstantCategoryTO> constantList)
    {

        List<SelectItem> constantSelectList = null;
        SelectItem item = null;
        try {

            if (constantList != null && constantList.size() != 0) {
                constantSelectList = new ArrayList<SelectItem>();
                for (SysConstantAndSysConstantCategoryTO constantStr : constantList) {
                    item = new SelectItem();
                    item.setLabel(constantStr.getSysConstantName());
                    item.setValue(constantStr.getSysConstantName());
                    constantSelectList.add(item);


                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return constantSelectList;
    }

    public static List<SelectItem> convertValuesIntoSelectItemTypeForLoginPage(List<SysConstantAndSysConstantCategoryTO> constantList)
    {

        List<SelectItem> constantSelectList = null;
        SelectItem item = null;
        try {

            if (constantList != null && constantList.size() != 0) {
                constantSelectList = new ArrayList<SelectItem>();
                for (SysConstantAndSysConstantCategoryTO constantStr : constantList) {
                    item = new SelectItem();
                    item.setLabel(constantStr.getSysConstantName());
                    item.setValue(constantStr.getSysConstantName());
                    constantSelectList.add(item);


                }

            }//
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return constantSelectList;
    }
}
