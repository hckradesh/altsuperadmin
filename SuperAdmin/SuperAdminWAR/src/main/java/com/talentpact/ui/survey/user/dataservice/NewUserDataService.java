/**
 * 
 */
package com.talentpact.ui.survey.user.dataservice;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.auth.entity.TpOrganization;
import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.authorization.transport.input.AuthRequestTO;
import com.talentpact.business.dataservice.TPOrgDS;
import com.talentpact.ui.survey.user.common.transport.input.SurveyOrgRequestTO;
import com.talentpact.ui.survey.user.common.transport.input.SurveyUserRequestTO;
import com.talentpact.ui.survey.user.common.transport.output.SurveyOrgResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.SurveyUserResponseTO;
import com.talentpact.ui.survey.user.dataprovider.NewUserDataProvider;
import com.talentpact.ui.survey.user.dataprovider.DS.SurveyOrgDS;
import com.talentpact.ui.survey.user.dataprovider.DS.SurveyUserDS;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class NewUserDataService {
	private static Logger LOGGER_ = LoggerFactory
			.getLogger(NewUserDataProvider.class);

	@EJB
	SurveyOrgDS surveyOrgDS;

	@EJB
	SurveyUserDS surveyUserDS;

	@EJB
	TPOrgDS tPOrgDS;

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public SurveyOrgResponseTO createNewOrganization(
			SurveyOrgRequestTO surveyOrgRequestTO) throws Exception {
		SurveyOrgResponseTO surveyOrgResponseTO = null;
		try {
			surveyOrgResponseTO = surveyOrgDS
					.createNewOrganization(surveyOrgRequestTO);
		} catch (Exception ex) {
			LOGGER_.debug("Exception while creating new organization for the survey : DataProvider : "
					+ ex);
			surveyOrgResponseTO = null;
			throw ex;
		} finally {

		}

		return surveyOrgResponseTO;
	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public TpOrganization findTpOrganization(Long orgID) throws Exception {
		TpOrganization tpOrganization = null;
		try {
			tpOrganization = tPOrgDS.findTpOrganization(orgID);
		} catch (Exception ex) {
			tpOrganization = null;
			throw ex;
		}
		return tpOrganization;
	}

	/**
	 * @param surveyUserRequestTO
	 * @return
	 */
//	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
//	public SurveyUserResponseTO createNewUser(
//			SurveyUserRequestTO surveyUserRequestTO) throws Exception {
//		SurveyUserResponseTO surveyUserResponseTO = null;
//
//		try {
//			surveyUserResponseTO = surveyUserDS
//					.createNewUser(surveyUserRequestTO);
//		} catch (Exception ex) {
//			LOGGER_.debug("Exception thrown while creating new user : DataProvider : "
//					+ ex);
//			surveyUserResponseTO = null;
//			throw ex;
//		} finally {
//
//		}
//
//		return surveyUserResponseTO;
//	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public TpUser checkIfUserExists(AuthRequestTO authRequestTO)
			throws Exception {
		TpUser tpUser = null;
		try{
			tpUser = surveyUserDS.findUserByName(authRequestTO);
		}catch(Exception ex){
			tpUser = null;
			throw ex;
		}
		return tpUser;
	}

}
