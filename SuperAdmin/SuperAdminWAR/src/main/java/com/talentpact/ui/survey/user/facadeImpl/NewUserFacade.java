/**
 * 
 */
package com.talentpact.ui.survey.user.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.survey.user.facaderemote.NewUserFacadeRemote;
import com.talentpact.ui.survey.user.service.NewUserService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class NewUserFacade implements NewUserFacadeRemote{

	private static final Logger LOGGER_ = LoggerFactory.getLogger(NewUserFacade.class);
	
	@EJB
	NewUserService newUserService;
	
	/* (non-Javadoc)
	 * @see com.alt.survey.user.NewUserFacadeRemote#createSurveyUser(com.alt.common.service.input.ServiceRequest)
	 */
	
//	@Override
//	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
//	public ServiceResponse createSurveyUser(ServiceRequest serviceRequest) {
//		ServiceResponse serviceResponse = null;
//		try{
//			serviceResponse = newUserService.createSurveyUser(serviceRequest);
//		}catch(Exception ex){
//			LOGGER_.debug("Exception thrown while creating new user : "+ex);
//		}finally{
//			
//		}
//		
//		return serviceResponse;
//	}

}
