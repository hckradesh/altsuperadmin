package com.talentpact.ui.replicateFormSecurity.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.replicateFormSecurity.ReplicateFormSecurityService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.replicateFormSecurity.bean.ReplicateFormSecurityBean;
import com.talentpact.ui.replicateFormSecurity.to.ReplicateFormSecurityTO;

/**
 * 
 * @author vivek.goyal
 *
 */

@Named("replicateFormSecurityController")
@ConversationScoped
public class ReplicateFormSecurityController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(ReplicateFormSecurityController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    ReplicateFormSecurityService replicateFormSecurityService;

    @Inject
    ReplicateFormSecurityBean replicateFormSecurityBean;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        ServiceResponse response = null;
        List<ReplicateFormSecurityTO> sourceOrganizationTOList = null;
        try {
            resetReplicateFormSecurity();
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../ReplicateFormSecurity/replicateFormSecurity.xhtml");
            sourceOrganizationTOList = replicateFormSecurityService.getSourceOrganizationList();
            replicateFormSecurityBean.setSourceOrganizationTOList(sourceOrganizationTOList);
            replicateFormSecurityBean.setReplicateFormSecurityHistoryTOList(replicateFormSecurityService.getReplicateFormSecurityHistory());
            replicateFormSecurityBean.setCopyButtonVisible(false);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void resetReplicateFormSecurity()
    {
        replicateFormSecurityBean.setFormSecurityExists(false);
        replicateFormSecurityBean.setSourceOrganizationID(null);
        replicateFormSecurityBean.setSourceOrganizationName(null);
        replicateFormSecurityBean.setSourceOrganizationTOList(null);
        replicateFormSecurityBean.setDestinationOrganizationID(null);
        replicateFormSecurityBean.setDestinationOrganizationName(null);
        replicateFormSecurityBean.setSysModuleID(null);
        replicateFormSecurityBean.setModuleID(null);
        replicateFormSecurityBean.setModuleName(null);
        replicateFormSecurityBean.setDestinationOrganizationTOList(null);
        replicateFormSecurityBean.setSourceModuleTOList(null);
    }

    public void getDestinationOrganizationList()
        throws Exception
    {
        List<ReplicateFormSecurityTO> destinationOrganizationTOList = null;
        try {
            replicateFormSecurityBean.setDestinationOrganizationID(null);
            replicateFormSecurityBean.setDestinationOrganizationName(null);
            replicateFormSecurityBean.setSysModuleID(null);
            replicateFormSecurityBean.setModuleID(null);
            replicateFormSecurityBean.setModuleName(null);
            replicateFormSecurityBean.setSourceModuleTOList(null);
            destinationOrganizationTOList = replicateFormSecurityService.getDestinationOrganizationList(replicateFormSecurityBean.getSourceOrganizationID());
            replicateFormSecurityBean.setDestinationOrganizationTOList(destinationOrganizationTOList);
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void getSourceModuleList()
        throws Exception
    {
        List<ReplicateFormSecurityTO> sourceModuleTOList = null;
        try {
            replicateFormSecurityBean.setSysModuleID(null);
            replicateFormSecurityBean.setModuleID(null);
            replicateFormSecurityBean.setModuleName(null);
            sourceModuleTOList = replicateFormSecurityService.getSourceModuleList(replicateFormSecurityBean.getSourceOrganizationID(),
                    replicateFormSecurityBean.getDestinationOrganizationID());
            replicateFormSecurityBean.setSourceModuleTOList(sourceModuleTOList);
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void validateExistingFormSecurity()
        throws Exception
    {
        try {
            //set condition as false for overriding the value based on previously selected module
            replicateFormSecurityBean.setFormSecurityExists(false);
            boolean status = (replicateFormSecurityService.validateExistingFormSecurity(replicateFormSecurityBean.getDestinationOrganizationID(),
                    replicateFormSecurityBean.getSysModuleID()));
            replicateFormSecurityBean.setFormSecurityExists(status);

            RequestContext requestContext = RequestContext.getCurrentInstance();
            if (status) {
                requestContext.execute("PF('existingFormSecurityValidateModal').show();");
            } else {
                replicateFormSecurityBean.setCopyButtonVisible(true);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void deactivateExistingFormSecurity()
        throws Exception
    {
        try {
            boolean deactivateFormSecurity = false;
            deactivateFormSecurity = replicateFormSecurityService.deactivateFormSecurity(replicateFormSecurityBean.getDestinationOrganizationID(),
                    replicateFormSecurityBean.getSysModuleID());
            replicateFormSecurityBean.setFormSecurityExists(false);
            replicateFormSecurityBean.setCopyButtonVisible(true);
            if (deactivateFormSecurity) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "FormSecurity removed successfully", ""));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "There is some error", ""));
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void isWorkFlowEnabled()
        throws Exception
    {
        try {
            boolean isWorkFlowEnabled = false;
            isWorkFlowEnabled = replicateFormSecurityService.isWorkFlowEnabled(replicateFormSecurityBean.getSourceOrganizationID(), replicateFormSecurityBean.getSysModuleID());
            if (isWorkFlowEnabled) {
                RequestContext requestContext = RequestContext.getCurrentInstance();
                requestContext.execute("PF('confirmDlgBox').show();");
            } else {
                replicateFormSecurity();
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void replicateFormSecurity()
        throws Exception
    {
        try {
            boolean replicateFormSecurity = false;
            replicateFormSecurity = replicateFormSecurityService.replicateFormSecurity(replicateFormSecurityBean.getSourceOrganizationID(),
                    replicateFormSecurityBean.getDestinationOrganizationID(), replicateFormSecurityBean.getSysModuleID(), userSessionBean.getUserID());
            if (replicateFormSecurity) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Form Security copied successfully", ""));
                initialize();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "There is some error", ""));
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void removeCopyButton()
    {
        try {
            replicateFormSecurityBean.setCopyButtonVisible(false);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }
}