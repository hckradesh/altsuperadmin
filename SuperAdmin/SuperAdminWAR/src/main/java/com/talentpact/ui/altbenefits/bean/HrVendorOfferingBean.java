/**
 * 
 */
package com.talentpact.ui.altbenefits.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.HrVendorOfferingTO;
import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.OrganizationTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.SysSectorTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.talentpact.ui.altbenefits.controller.HrOfferingController;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;

/**
 * @author javed.ali
 *
 */
@Named(value = "hrVendorOfferingBean")
@ConversationScoped
public class HrVendorOfferingBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 14338909081123324L;

    @Inject
    HrOfferingController hrOfferingController;

    private List<HrVendorOfferingTO> hrVendorOfferingTOList;

    private List<OrganizationTO> orgTOList;

    private VendorOfferingTemplateTO vendorOfferingTemplateTO;

    private HrVendorOfferingTO selectedHrBasedBenefitTO;

    private PeakRecurringDetailTO peakRecurringDetailTO;

    private BenefitVendorTO sysBenefitVendorTO;

    private boolean active;

    private boolean renderFormMessage;

    private Map<Integer, Integer> hrOfferSequenceMap;

    private List<SysSectorTO> sectorTOList;

    private String selectedOrgName;

    private boolean renderMaximumLimitApplicableDetail;

    private OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO;

    private OfferingCodeTO offeringCodeTO;


    private String selectedSectorName;

    private Integer sectorID;

    private Long selectedOrgID;

    private String sequenceValue;

    private boolean renderVendorOffering;

    private List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList;

    private List<VendorOfferingTemplateTO> selectedVendorOfferingTemplateTOList;

    public String getSelectedSectorName()
    {
        return selectedSectorName;
    }

    public void setSelectedSectorName(String selectedSectorName)
    {
        this.selectedSectorName = selectedSectorName;
    }

    public List<VendorOfferingTemplateTO> getSelectedVendorOfferingTemplateTOList()
    {
        return selectedVendorOfferingTemplateTOList;
    }

    public void setSelectedVendorOfferingTemplateTOList(List<VendorOfferingTemplateTO> selectedVendorOfferingTemplateTOList)
    {
        this.selectedVendorOfferingTemplateTOList = selectedVendorOfferingTemplateTOList;
    }


    private Long selectedOrganization;

    private Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap;


    public PeakRecurringDetailTO getPeakRecurringDetailTO()
    {
        return peakRecurringDetailTO;
    }


    public BenefitVendorTO getSysBenefitVendorTO()
    {
        return sysBenefitVendorTO;
    }

    public VendorOfferingTemplateTO getVendorOfferingTemplateTO()
    {
        return vendorOfferingTemplateTO;
    }

    public void setPeakRecurringDetailTO(PeakRecurringDetailTO peakRecurringDetailTO)
    {
        this.peakRecurringDetailTO = peakRecurringDetailTO;
    }


    public void setSysBenefitVendorTO(BenefitVendorTO sysBenefitVendorTO)
    {
        this.sysBenefitVendorTO = sysBenefitVendorTO;
    }

    public void setVendorOfferingTemplateTO(VendorOfferingTemplateTO vendorOfferingTemplateTO)
    {
        this.vendorOfferingTemplateTO = vendorOfferingTemplateTO;
    }

    public Map<String, List<VendorOfferingTemplateTO>> getHrVendorOfferingmap()
    {
        return hrVendorOfferingmap;
    }

    public void setHrVendorOfferingmap(Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap)
    {
        this.hrVendorOfferingmap = hrVendorOfferingmap;
    }

    public Long getSelectedOrganization()
    {
        return selectedOrganization;
    }

    public void setSelectedOrganization(Long selectedOrganization)
    {
        this.selectedOrganization = selectedOrganization;
    }

    public List<VendorOfferingTemplateTO> getVendorOfferingTemplateTOList()
    {
        return vendorOfferingTemplateTOList;
    }

    public void setVendorOfferingTemplateTOList(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList)
    {
        this.vendorOfferingTemplateTOList = vendorOfferingTemplateTOList;
    }


    @Override
    public void initialize()
    {
        endConversation();
        beginConversation();
        hrOfferingController.initialize();
    }


    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isRenderFormMessage()
    {
        return renderFormMessage;
    }

    public void setRenderFormMessage(boolean renderFormMessage)
    {
        this.renderFormMessage = renderFormMessage;
    }


    public String getSequenceValue()
    {
        return sequenceValue;
    }

    public void setSequenceValue(String sequenceValue)
    {
        this.sequenceValue = sequenceValue;
    }

    public boolean isRenderVendorOffering()
    {
        return renderVendorOffering;
    }

    public void setRenderVendorOffering(boolean renderVendorOffering)
    {
        this.renderVendorOffering = renderVendorOffering;
    }


    public List<HrVendorOfferingTO> getHrVendorOfferingTOList()
    {
        return hrVendorOfferingTOList;
    }

    public void setHrVendorOfferingTOList(List<HrVendorOfferingTO> hrVendorOfferingTOList)
    {
        this.hrVendorOfferingTOList = hrVendorOfferingTOList;
    }

    public List<OrganizationTO> getOrgTOList()
    {
        return orgTOList;
    }

    public void setOrgTOList(List<OrganizationTO> orgTOList)
    {
        this.orgTOList = orgTOList;
    }

    public HrVendorOfferingTO getSelectedHrBasedBenefitTO()
    {
        return selectedHrBasedBenefitTO;
    }

    public void setSelectedHrBasedBenefitTO(HrVendorOfferingTO selectedHrBasedBenefitTO)
    {
        this.selectedHrBasedBenefitTO = selectedHrBasedBenefitTO;
    }

    public Map<Integer, Integer> getHrOfferSequenceMap()
    {
        return hrOfferSequenceMap;
    }

    public void setHrOfferSequenceMap(Map<Integer, Integer> hrOfferSequenceMap)
    {
        this.hrOfferSequenceMap = hrOfferSequenceMap;
    }

    public String getSelectedOrgName()
    {
        return selectedOrgName;
    }

    public void setSelectedOrgName(String selectedOrgName)
    {
        this.selectedOrgName = selectedOrgName;
    }

    public Long getSelectedOrgID()
    {
        return selectedOrgID;
    }

    public void setSelectedOrgID(Long selectedOrgID)
    {
        this.selectedOrgID = selectedOrgID;
    }

    public List<SysSectorTO> getSectorTOList()
    {
        return sectorTOList;
    }

    public void setSectorTOList(List<SysSectorTO> sectorTOList)
    {
        this.sectorTOList = sectorTOList;
    }

    public Integer getSectorID()
    {
        return sectorID;
    }

    public void setSectorID(Integer sectorID)
    {
        this.sectorID = sectorID;
    }

    public boolean isRenderMaximumLimitApplicableDetail()
    {
        return renderMaximumLimitApplicableDetail;
    }

    public void setRenderMaximumLimitApplicableDetail(boolean renderMaximumLimitApplicableDetail)
    {
        this.renderMaximumLimitApplicableDetail = renderMaximumLimitApplicableDetail;
    }

    public OfferingCodeMaximumLimitTO getOfferingCodeMaximumLimitTO()
    {
        return offeringCodeMaximumLimitTO;
    }

    public void setOfferingCodeMaximumLimitTO(OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO)
    {
        this.offeringCodeMaximumLimitTO = offeringCodeMaximumLimitTO;
    }

    public OfferingCodeTO getOfferingCodeTO()
    {
        return offeringCodeTO;
    }

    public void setOfferingCodeTO(OfferingCodeTO offeringCodeTO)
    {
        this.offeringCodeTO = offeringCodeTO;
    }
}
