package com.talentpact.ui.sysEntityColumn.exception;

public class SysEntityColumnException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message = null;

    public SysEntityColumnException()
    {
        super();
    }

    public SysEntityColumnException(String message)
    {
        super(message);
        this.message = message;
    }
    
    public SysEntityColumnException(String message, Exception e) {
        super(message, e);
    }

    public SysEntityColumnException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
