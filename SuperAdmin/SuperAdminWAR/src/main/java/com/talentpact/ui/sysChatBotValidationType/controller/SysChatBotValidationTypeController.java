package com.talentpact.ui.sysChatBotValidationType.controller;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.faces.context.FacesContext;

import java.io.Serializable;
import java.util.*;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.business.common.constants.ISysChatBotValidationTypeConstants;
import com.talentpact.business.dataservice.UiForm.UiFormService;
import com.talentpact.business.dataservice.sysChatBotValidationType.SysChatBotValidationTypeService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysChatBotValidationType.bean.SysChatBotValidationTypeBean;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;

@SuppressWarnings("serial")
@Named("sysChatBotValidationTypeController")
public class SysChatBotValidationTypeController extends CommonController
		implements Serializable, IDefaultAction {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationTypeController.class);

	@Inject
	MenuBean menuBean;

	@Inject
	UserSessionBean userSessionBean;

    @Inject
    UiFormService uiFormService;
	   
	@Inject
	SysChatBotValidationTypeBean sysChatBotValidationTypeBean;

	@Inject
	SysChatBotValidationTypeService sysChatBotValidationTypeService;

	@Override
	public void initialize() {
		try {

			ServiceResponse response = null;
			fieldTypeDropDown();
			sysChatBotValidationTypeBean
					.setSysChatBotValidationTypeTOList(sysChatBotValidationTypeService
							.getSysChatBotValidationTypeList());
			RequestContext requestContext = RequestContext.getCurrentInstance();
			menuBean.setPage("../sysChatBotValidationType/sysChatBotValidationType.xhtml");
			requestContext
					.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_DATATABLE_RESET);
			requestContext
					.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_PAGINATION_DATATABLE_RESET);
		} catch (Exception ex) {
			LOGGER_.error(ex.toString());
		}
	}

	public void addSysChatBotValidationType() {
		sysChatBotValidationTypeBean.setSysValidationTypeID(null);
		sysChatBotValidationTypeBean.setSysFieldTypeID(null);
		sysChatBotValidationTypeBean.setNoOfParameters(null);
		sysChatBotValidationTypeBean.setLabel(null);

	}

	public void editSysChatBotValidationType(
			SysChatBotValidationTypeTO sysChatBotValidationType) throws Exception {
		/*fieldTypeDropDown();*/
		
		sysChatBotValidationTypeBean.setSysValidationTypeID(sysChatBotValidationType.getSysValidationTypeID());
		sysChatBotValidationTypeBean.setSysFieldTypeID(sysChatBotValidationType.getSysFieldTypeID());
		sysChatBotValidationTypeBean.setSysChatBotValidationTypeTO(sysChatBotValidationType);
		
	}

	public void saveSysChatBotValidationType() throws Exception {
		SysChatBotValidationTypeTO sysChatBotValidationTypeTO = null;
		RequestContext request = null;
		try {
			request = RequestContext.getCurrentInstance();
			sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
			sysChatBotValidationTypeTO.setLabel(sysChatBotValidationTypeBean.getLabel());
			sysChatBotValidationTypeTO.setNoOfParameters(sysChatBotValidationTypeBean.getNoOfParameters());
			sysChatBotValidationTypeTO.setSysFieldTypeID(sysChatBotValidationTypeBean.getSysFieldTypeID());

			if (validateChatBotValidationType(sysChatBotValidationTypeTO, false)) {
				sysChatBotValidationTypeService.saveSysChatBotValidationType(sysChatBotValidationTypeTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,"SysChatBotValidationType added successfully",""));
				request.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_ADD_DIALOG_HIDE);
				request.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_DATATABLE_RESET);
				request.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_PAGINATION_DATATABLE_RESET);
				request.update("sysChatBotValidationTypeListForm");
			} else {
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_ERROR_MSG,""));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_SAVE_ERROR_MSG,""));
			LOGGER_.error("", e);
			throw e;
		}
	} // end of method

	public void updateSysChatBotValidationType() throws Exception {
		SysChatBotValidationTypeTO sysChatBotValidationTypeTO = null;
		RequestContext requestContext = null;
		try {
			requestContext = RequestContext.getCurrentInstance();
			sysChatBotValidationTypeTO = new SysChatBotValidationTypeTO();
			sysChatBotValidationTypeTO
					.setSysValidationTypeID(sysChatBotValidationTypeBean
							.getSysChatBotValidationTypeTO()
							.getSysValidationTypeID());
			sysChatBotValidationTypeTO.setLabel(sysChatBotValidationTypeBean
					.getSysChatBotValidationTypeTO().getLabel());
			sysChatBotValidationTypeTO
					.setNoOfParameters(sysChatBotValidationTypeBean
							.getSysChatBotValidationTypeTO()
							.getNoOfParameters());
			sysChatBotValidationTypeTO
					.setSysFieldTypeID(sysChatBotValidationTypeBean
							.getSysChatBotValidationTypeTO()
							.getSysFieldTypeID());
			if (validateChatBotValidationType(sysChatBotValidationTypeTO, true)) {
				sysChatBotValidationTypeService
						.updateSysChatBotValidationType(sysChatBotValidationTypeTO);
				initialize();
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"SysChatBotValidationType updated successfully",
										""));
				requestContext
						.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_EDIT_DIALOG_HIDE);
				requestContext
						.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_DATATABLE_RESET);
				requestContext
						.execute(ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_PAGINATION_DATATABLE_RESET);
				requestContext.update("sysChatBotValidationTypeListForm");
			} else {
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_ERROR_MSG,
										""));
			}
		} catch (Exception e) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									ISysChatBotValidationTypeConstants.SYSCHATBOTVALIDATIONTYPE_UPDATE_ERROR_MSG,
									""));
			LOGGER_.error("", e);
			throw e;
		}
	} // end of method

	@SuppressWarnings("unused")
	private boolean validateChatBotValidationType(
			SysChatBotValidationTypeTO sysChatBotValidationTypeTO,
			boolean edited) throws Exception {
		List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList = null;
		try {
			if (edited) {
				sysChatBotValidationTypeTOList = sysChatBotValidationTypeService
						.getUpdateSysChatBotValidationType(sysChatBotValidationTypeTO
								.getSysValidationTypeID());
			} else {
				sysChatBotValidationTypeTOList = sysChatBotValidationTypeService
						.getSysChatBotValidationTypeList();
			}
			if (sysChatBotValidationTypeTOList != null
					&& !sysChatBotValidationTypeTOList.isEmpty()) {
				for (SysChatBotValidationTypeTO sysFT : sysChatBotValidationTypeTOList) {
					/*
					 * if (sysFT.getFieldType().trim().equalsIgnoreCase(
					 * sysChatBotValidationTypeTO.getFieldType())) { return
					 * false; }
					 */
				}
			}
			return true;
		} catch (Exception ex) {
			LOGGER_.error("", ex);
			throw new Exception(ex);
		}
	}

	public void fieldTypeDropDown()
            throws Exception
        {

            List<SysFieldTypeResponseTO> response = null;
            try {
                response = uiFormService.fieldTypeDropDown();
                sysChatBotValidationTypeBean.setSysFieldTypeTOList(response);

            } catch (Exception e) {

            }

        }
}// end of class