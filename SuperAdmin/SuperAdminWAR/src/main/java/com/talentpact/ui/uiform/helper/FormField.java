package com.talentpact.ui.uiform.helper;

public enum FormField {

	 GROUPNAME("Group Name"), ENTITYFIELD ("Entity Field"), CLIENTNAME ("Client Name"), ISACTION("IsAction"), ISCUSTOMFIELDREQUIRED("IsCustomfieldRequired"), ISMASTER("IsMaster"), ISATTACHMENTREQUIRED("IsAttachmentRequired"),
     ISAPPROVALREQUIRED("IsApprovalRequired"), ISEFFECTIVEDATEREQUIRED("IsEffectiveDateRequired"), ISMANDATORY("IsMandatory"), ISATTACHMENTENABLED("IsAttachmentEnabled"), PARENTFORMFIELDNAME("ParentFormFieldName"), REGEX("Regex"),
     SYSTEXTBOXES("Value"),  SYSTEXTBOXESTITLE("Title"); 
	
	
	private String formField;

	private FormField(String s) {
	    formField = s;
	}

	public String getFormField() {
		return formField;
	}
}