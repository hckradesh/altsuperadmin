/**
 * 
 */
package com.talentpact.ui.altbenefits.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.common.constants.CoreConstants;
import com.alt.common.helper.CommonUtilHelper;
import com.alt.contentType.transport.input.ContentTypeTO;
import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.business.common.constants.IBenefitsConstants;
import com.talentpact.business.dataservice.SysContentType.SysContentTypeService;
import com.talentpact.business.dataservice.SysOfferingType.SysOfferingTypeService;
import com.talentpact.business.dataservice.sysPeakRecurringType.SysPeakRecurringTypeService;
import com.talentpact.business.service.altbenefits.HrVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysBenefitsVendorService;
import com.talentpact.business.service.altbenefits.SysSectorVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysVendorOfferingTemplateService;
import com.talentpact.ui.altbenefits.bean.SysBenefitsVendorBean;
import com.talentpact.ui.altbenefits.constant.IBenefitsConstant;
import com.talentpact.ui.altbenefits.helper.OfferCodeField;
import com.talentpact.ui.altbenefits.transport.SysBenefitVendorTO;
import com.talentpact.ui.altbenefits.transport.input.SaveOfferTempReqTO;
import com.talentpact.ui.altbenefits.transport.input.VendorOfferTemplateRequestTO;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * 
 * @author javed.ali
 * 
 */
@Named("sysBenefitsVendorController")
@ConversationScoped
public class SysBenefitsVendorController extends CommonController implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = 4789330591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysBenefitsVendorController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysBenefitsVendorBean sysBenefitsVendorBean;


    @Inject
    SysBenefitsVendorService sysBenefitsVendorService;

    @Inject
    SysOfferingTypeService sysOfferingTypeService;

    @Inject
    SysPeakRecurringTypeService sysPeakRecurringTypeService;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    SysVendorOfferingTemplateService sysVendorOfferingTemplateService;

    @EJB
    HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

    @EJB
    SysSectorVendorOfferingSequenceService sysSectorVendorOfferingSequenceService;

    @EJB
    SysContentTypeService sysContentTypeService;


    @Override
    public void initialize()
    {
        List<SysBenefitVendorTO> sysBenefitVendorTOList = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            sysBenefitVendorTOList = sysBenefitsVendorService.getSysBenefitsVendorList();
            sysBenefitsVendorBean.setSysBenefitVendorTOList(sysBenefitVendorTOList);
            sysBenefitsVendorBean.setRenderFormMessage(false);
            sysBenefitsVendorBean.setPeakRecurringDetailTO(null);
            sysBenefitsVendorBean.setRenderPeakDetails(false);
            sysBenefitsVendorBean.setVendorLogoFile(null);
            menuBean.setPage("../altbenefits/sysBenefitsVendor.xhtml");
            requestContext.execute(IBenefitsConstants.SYS_BENEFITS_VENDOR_DATATABLE_RESET);
            requestContext.execute(IBenefitsConstants.SYS_BENEFITS_VENDOR_PAGINATION_DATATABLE_RESET);

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }

    }

    void addSysBenefitsVendor()
    {
        sysBenefitsVendorBean.setSysBenefitVendorTO(new SysBenefitVendorTO());
        sysBenefitsVendorBean.setEditVendorForm(false);
        sysBenefitsVendorBean.setRenderFormMessage(true);
    }

    void editBenefitsVendor(SysBenefitVendorTO benefitVendorTO)
    {
        sysBenefitsVendorBean.setSysBenefitVendorTO(benefitVendorTO);
        sysBenefitsVendorBean.setEditVendorForm(true);
        sysBenefitsVendorBean.setRenderFormMessage(true);
    }

    /**
     * 
     * @param benefitVendorTO
     * @param edit
     */
    void saveBenefitVendor(SysBenefitVendorTO benefitVendorTO, boolean edit)
    {
        boolean flag = false;
        String file_location = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            if (!edit) {
                if (sysBenefitsVendorBean.getVendorLogoFile() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_LOGO_EMPTY, ""));
                    return;
                }

            }
            file_location = uploadVendorLogo(benefitVendorTO);
            if (file_location != null) {
                benefitVendorTO.setVendorLogo(file_location);
            }
                
                flag = sysBenefitsVendorService.saveSysBenefitsVendorDetail(benefitVendorTO);
                if (flag) {
                    if (!edit) {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstants.SYS_BENEFITS_VENDOR_SAVE_SUCCESS_MSG, ""));
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstants.SYS_BENEFITS_VENDOR_UPDATE_SUCCESS_MSG, ""));
                    }
                } else {
                    if (!edit) {
                        FacesContext.getCurrentInstance()
                                .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_SAVE_ERROR_MSG, ""));
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_UPDATE_ERROR_MSG, ""));
                    }
                }
            requestContext.execute(IBenefitsConstants.SYS_BENEFITS_VENDOR_ADD_DIALOG_HIDE);
            initialize();

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    /**
     * 
     * @param benefitVendorTO
     */
    void changeBenefitVendorStatus(SysBenefitVendorTO benefitVendorTO)
    {
        boolean flag = false;
        try {
            flag = sysBenefitsVendorService.changeTheStatusForBenefitsVendor(benefitVendorTO.getVendorID(), benefitVendorTO.getActive());
            if (flag) {
                hrVendorOfferingSequenceService.getAllEmpGroupBasedOfferingList();
                sysSectorVendorOfferingSequenceService.getAllSectorBasedOfferingList();
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstants.SYS_BENEFITS_VENDOR_CHANGE_STATUS_SUCCESS_MSG, ""));
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_CHANGE_STATUS_ERROR_MSG, ""));
            }
            initialize();

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    public void offeringTemplateInitilize(SysBenefitVendorTO benefitVendorTO)
        throws Exception
    {

        VendorOfferTemplateRequestTO vOffTemRequestTO = new VendorOfferTemplateRequestTO();
        vOffTemRequestTO.setSysBenefitVendorTO(benefitVendorTO);
        List<VendorOfferingTemplateTO> offeringTemlateTOlist = null;
        List<SysOfferingTypeTO> sysOfferingTypeTOList = null;
        sysBenefitsVendorBean.setSelectedVendorTO(benefitVendorTO);
        //just for temporary
        sysBenefitsVendorBean.setOfferingTemplateTO(new VendorOfferingTemplateTO());
        try {
            sysOfferingTypeTOList = sysOfferingTypeService.getSysOfferingTypeList();
            sysBenefitsVendorBean.setSysOfferingTypeTOList(sysOfferingTypeTOList);
            offeringTemlateTOlist = sysVendorOfferingTemplateService.getOfferingTemplateList(vOffTemRequestTO);
            sysVendorOfferingTemplateService.setAllHrOfferingTOList();
            sysBenefitsVendorBean.setOfferingTemlateTOlist(offeringTemlateTOlist);
            sysBenefitsVendorBean.setPeakRecurringDetailTO(null);
            sysBenefitsVendorBean.setRenderPeakDetails(false);
            sysBenefitsVendorBean.setRenderFormMessage(false);
            menuBean.setPage("../altbenefits/sysBenefitsVendorTemplate.xhtml");
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void opemEditDialogOfOfferTemplate()
    {
        VendorOfferingTemplateTO offeringTemplateTO = new VendorOfferingTemplateTO();
        offeringTemplateTO.setVendorID(sysBenefitsVendorBean.getSelectedVendorTO().getVendorID());
        sysBenefitsVendorBean.setOfferingTemplateTO(offeringTemplateTO);
        sysBenefitsVendorBean.setAddOfferTemplateFlag(true);
        sysBenefitsVendorBean.setUpdateOfferTemplateFlag(false);
        sysBenefitsVendorBean.setPeakRecurringDetailTO(null);
        sysBenefitsVendorBean.setRenderPeakDetails(false);
        sysBenefitsVendorBean.setOfferingPeakDetailActive(false);
        sysBenefitsVendorBean.setRenderFormMessage(true);

    }


    public void saveBenefitVendorTemplate(VendorOfferingTemplateTO offeringTemplateTO, boolean edit)
        throws Exception
    {
        String file_location = null;
        SaveOfferTempReqTO saveOfferTempReqTO = null;
        boolean updateFailed = false;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            saveOfferTempReqTO = new SaveOfferTempReqTO();
            if (!edit) {
                if (sysBenefitsVendorBean.getBannerFile() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_BANNER_EMPTY, ""));
                    return;
                }

            }
            file_location = uploadMasterData();
            if (edit && sysBenefitsVendorBean.getBannerFile() != null && file_location == null) {
                updateFailed = true;
            }
            if ((file_location != null && !edit) || (edit && !updateFailed)) {
                saveOfferTempReqTO.setOfferingTemplateTO(offeringTemplateTO);
                saveOfferTempReqTO.setPeakRecurringDetails(sysBenefitsVendorBean.isRenderPeakDetails());
                saveOfferTempReqTO.setPeakRecurringDetailTO(sysBenefitsVendorBean.getPeakRecurringDetailTO());
                saveOfferTempReqTO.setFileLocation(file_location);
                sysVendorOfferingTemplateService.saveVendorOfferTemplate(saveOfferTempReqTO);
                hrVendorOfferingSequenceService.changeTheStatusHrOfferingTemplate(saveOfferTempReqTO.getOfferingTemplateTO().getVendorOfferingID(), saveOfferTempReqTO
                        .getOfferingTemplateTO().getActive());
                hrVendorOfferingSequenceService.getAllEmpGroupBasedOfferingList();
                sysSectorVendorOfferingSequenceService.getAllSectorBasedOfferingList();
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstants.SYS_BENEFITS_VENDOR_OFFERING_TEMPLATE_SAVE_SUCCESS_MSG, ""));
                requestContext.execute(IBenefitsConstants.OFFER_TEMPLATE_EDIT_DIALOG_HIDE);
                offeringTemplateInitilize(sysBenefitsVendorBean.getSelectedVendorTO());
                sysBenefitsVendorBean.setRenderFormMessage(false);
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_VENDOR_OFFERING_TEMPLATE_SAVE_ERROR_MSG, ""));
                requestContext.execute(IBenefitsConstants.OFFER_TEMPLATE_EDIT_DIALOG_HIDE);
                sysBenefitsVendorBean.setRenderFormMessage(false);
                offeringTemplateInitilize(sysBenefitsVendorBean.getSelectedVendorTO());
            }
        } catch (IOException e) {
            LOGGER_.error("", e);
        }

    }

    /**
     * 
     * @param s
     * @return
     */
    private String getPath(String s)
        throws Exception
    {
        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String getPath = null;
        String finalPath = null;
        File dir = null;
        String serverPath = null;
        String fileLocation = null;
        boolean success = false;
        String returnPath = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(CoreConstants.DATETIME_FORMAT_STRING);
            resourceBundle = ResourceBundle.getBundle(CoreConstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(IBenefitsConstant.BANNER_PATH);
            serverPath = resourceBundle.getString(IBenefitsConstant.BANNER_SERVER_PATH);
            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception(CoreConstants.INVALID_PATH_STRING);
            }
            fileLocation = sysBenefitsVendorBean.getSelectedVendorTO().getVendorName() + File.separator + dateFormat.format(new Date());
            finalPath = initialPath + fileLocation;
            dir = new File(finalPath);
            if (!dir.exists()) {
                success = dir.mkdirs();
            }
            if (success) {
                LOGGER_.info("-----Directory created Successfully...!!------------: - " + dir);
                getPath = finalPath + File.separator + s;
                returnPath = getPath + CoreConstants.AT_SPERATOR + serverPath + CoreConstants.AT_SPERATOR + fileLocation + File.separator + s;
            } else {
                returnPath = null;
                LOGGER_.error("-----Unable to make directory...!!------------");
            }
        } catch (Exception ex) {
            throw ex;
        }
        return returnPath;
    }

    /**
     * 
     * @param event
     */
    public void bannerFileUpload(FileUploadEvent event)
    {
        UploadedFile uploadedFile = null;
        try {
            uploadedFile = event.getFile();
            if (uploadedFile != null && uploadedFile.getContentType().startsWith("image/")) {
                sysBenefitsVendorBean.setBannerFile(uploadedFile);
                sysBenefitsVendorBean.setUploadedBannerFileName(uploadedFile.getFileName());
            } else {
                sysBenefitsVendorBean.setBannerFile(null);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid file. File format is not correct.", ""));
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    /**
     * 
     * @throws Exception
     */
    public String uploadMasterData()
        throws Exception
    {
        UploadedFile uploadedFile = null;
        String path = null;
        String fileName = null;
        InputStream in = null;
        String serverPath = null;
        try {
            uploadedFile = sysBenefitsVendorBean.getBannerFile();
            if (uploadedFile != null) {
                fileName = uploadedFile.getFileName();
                in = uploadedFile.getInputstream();
                path = getPath(fileName);
                if (path != null) {
                    OutputStream out = new FileOutputStream(new File(path.split(CoreConstants.AT_SPERATOR)[0]));
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    while ((read = in.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    in.close();
                    out.close();
                    out.flush();
                    serverPath = path.split(CoreConstants.AT_SPERATOR)[1] + path.split(CoreConstants.AT_SPERATOR)[2];
                }
            }
        } catch (IOException e) {
            LOGGER_.error("", e);
            throw e;
        }

        return serverPath;
    }

    /**
     * 
     * @param offeringTemplateTO
     */
    public void updateOfferTemplate(VendorOfferingTemplateTO offeringTemplateTO)
    {
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        Map<Integer, String> hourListMap = null;
        Map<Integer, String> minuteListMap = null;
        List<SelectItem> hourList = null;
        List<SelectItem> minuteList = null;
        try {
            peakRecurringDetailTO = offeringTemplateTO.getPeakRecurringDetailTO();
            sysBenefitsVendorBean.setOfferingTemplateTO(offeringTemplateTO);
            sysBenefitsVendorBean.setAddOfferTemplateFlag(false);
            sysBenefitsVendorBean.setUpdateOfferTemplateFlag(true);
            if (peakRecurringDetailTO != null) {
                sysBenefitsVendorBean.setRenderPeakDetails(true);
                sysBenefitsVendorBean.setOfferingPeakDetailActive(true);
                peakRecurringDetailTO.setSysPeakRecurringTypeTOList(sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_PEAKRECURRING_TYPE_STRING));
                hourList = CommonUtilHelper.createHours();
                minuteList = CommonUtilHelper.createMinutes();
                hourListMap = processSelectList(hourList);
                minuteListMap = processSelectList(minuteList);
                peakRecurringDetailTO.setHourListMap(hourListMap);
                peakRecurringDetailTO.setMinuteListMap(minuteListMap);
                sysBenefitsVendorBean.setPeakRecurringDetailTO(peakRecurringDetailTO);
            } else {
                sysBenefitsVendorBean.setRenderPeakDetails(false);
            }
            sysBenefitsVendorBean.setRenderFormMessage(true);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    /**
     * 
     * @param selectList
     * @return
     */
    private Map<Integer, String> processSelectList(List<SelectItem> selectList)
    {
        Map<Integer, String> listMap = new LinkedHashMap<Integer, String>();
        for (SelectItem item : selectList) {
            listMap.put((Integer) item.getValue(), item.getLabel());
        }
        return listMap;
    }

    public void backToVendorsPage()
    {
        RequestContext context = null;
        context = RequestContext.getCurrentInstance();
        initialize();
        menuBean.setPage("../altbenefits/sysBenefitsVendor.xhtml");
        context.update("centerForm");
    }

    /**
     * 
     */
    public void renderOfferingPeakDetail()
    {
        List<ContentTypeTO> sysPeakRecurringTypeTOList = null;
        PeakRecurringDetailTO peakRecurringDetailTO = null;
        Map<Integer, String> hourListMap = null;
        Map<Integer, String> minuteListMap = null;
        List<SelectItem> hourList = null;
        List<SelectItem> minuteList = null;
        try {
            peakRecurringDetailTO = sysBenefitsVendorBean.getPeakRecurringDetailTO();
            if (!sysBenefitsVendorBean.isRenderPeakDetails()) {
                if (peakRecurringDetailTO == null) {
                    peakRecurringDetailTO = new PeakRecurringDetailTO();
                    sysPeakRecurringTypeTOList = sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_PEAKRECURRING_TYPE_STRING);
                    peakRecurringDetailTO.setSysPeakRecurringTypeTOList(sysPeakRecurringTypeTOList);
                    hourList = CommonUtilHelper.createHours();
                    minuteList = CommonUtilHelper.createMinutes();
                    hourListMap = processSelectList(hourList);
                    minuteListMap = processSelectList(minuteList);
                    peakRecurringDetailTO.setHourListMap(hourListMap);
                    peakRecurringDetailTO.setMinuteListMap(minuteListMap);
                    sysBenefitsVendorBean.setPeakRecurringDetailTO(peakRecurringDetailTO);
                }
                sysBenefitsVendorBean.setRenderPeakDetails(true);
            } else {
                sysBenefitsVendorBean.setRenderPeakDetails(false);
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    public void offeringCodeInitilize(VendorOfferingTemplateTO selectedVendorOfferingTemplateTO)
        throws Exception
    {
        List<OfferingCodeTO> offerringCodeTOlist = null;
        List<ContentTypeTO> offeringAccessTypeTOList = null;
        List<ContentTypeTO> maximumUsageDurationTypeTOList = null;
        RequestContext requestContext = null;
        List<String> accessTypeNameList = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            offeringAccessTypeTOList = sysBenefitsVendorBean.getOfferingAccessTypeTOList();
            maximumUsageDurationTypeTOList = sysBenefitsVendorBean.getMaximumUsageDurationTypeTOList();
            sysBenefitsVendorBean.setSelectedVendorOfferingTemplateTO(selectedVendorOfferingTemplateTO);
            OfferingCodeTO offeringCodeTO = new OfferingCodeTO();
            offeringCodeTO.setVendorOfferingID(sysBenefitsVendorBean.getSelectedVendorOfferingTemplateTO().getVendorOfferingID());
            sysBenefitsVendorBean.setOfferingCodeTO(offeringCodeTO);

            if (offeringAccessTypeTOList == null) {
                offeringAccessTypeTOList = sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_ACCESS_TYPE_STRING);
                sysBenefitsVendorBean.setOfferingAccessTypeTOList(offeringAccessTypeTOList);
            }
            if(maximumUsageDurationTypeTOList==null) {
            	maximumUsageDurationTypeTOList = sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_USAGE_DURATION_TYPE_STRING);
            	sysBenefitsVendorBean.setMaximumUsageDurationTypeTOList(maximumUsageDurationTypeTOList);
            }
            if (offeringAccessTypeTOList != null) {
                accessTypeNameList = new ArrayList<String>();
                for (ContentTypeTO typeTO : offeringAccessTypeTOList) {
                    accessTypeNameList.add(typeTO.getSysType());
                }
            }

            offerringCodeTOlist = sysVendorOfferingTemplateService.getOfferingCodeListByVendorOffering(selectedVendorOfferingTemplateTO.getVendorOfferingID());
            sysVendorOfferingTemplateService.setAllHrOfferingTOList();
            sysBenefitsVendorBean.setOfferingCodeTOList(offerringCodeTOlist);
            sysBenefitsVendorBean.setAccessTypeNameList(accessTypeNameList);
            sysBenefitsVendorBean.setFiltedredOfferingCodeTOlist(offerringCodeTOlist);
            sysBenefitsVendorBean.setRenderFormMessage(false);

            requestContext.execute(IBenefitsConstants.OFFER_CODE_DATATABLE_RESET);
            requestContext.execute(IBenefitsConstants.OFFER_CODE_PAGINATION_DATATABLE_RESET);
            menuBean.setPage("../altbenefits/offeringCodeList.xhtml");
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    /**
     * 
     * @param offeringCodeTO
     */
    public void renderMaximumLimitApplicableDetailPanel(OfferingCodeTO offeringCodeTO)
        throws Exception
    {
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        List<ContentTypeTO> maximumUsageDurationTypeTOList = null;
        try {
            offeringCodeMaximumLimitTO = offeringCodeTO.getCodeMaximumLimitTO();
            if (offeringCodeTO.isMaximumLimitApplicable()) {
                if (offeringCodeMaximumLimitTO == null) {
                    offeringCodeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                    offeringCodeMaximumLimitTO.setMaximumUsageTypeTOList(sysBenefitsVendorBean.getOfferingAccessTypeTOList());
                    maximumUsageDurationTypeTOList = offeringCodeMaximumLimitTO.getMaximumUsageDurationTypeTOList();
                    if (maximumUsageDurationTypeTOList == null) {
                        maximumUsageDurationTypeTOList = sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_USAGE_DURATION_TYPE_STRING);
                        offeringCodeMaximumLimitTO.setMaximumUsageDurationTypeTOList(maximumUsageDurationTypeTOList);
                    }
                    offeringCodeTO.setCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                    sysBenefitsVendorBean.setOfferingCodeTO(offeringCodeTO);
                }
                sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(true);
            } else {
                sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(false);
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
        }


    }

    /**
     * 
     */
    public void addOfferringCode()
    {
        sysBenefitsVendorBean.setEditOfferCodeForm(false);
        sysBenefitsVendorBean.setOfferingCodeMaximumLimitTO(null);
        sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(false);
        sysBenefitsVendorBean.setRenderFormMessage(true);
        OfferingCodeTO offeringCodeTO = new OfferingCodeTO();
        offeringCodeTO.setVendorOfferingID(sysBenefitsVendorBean.getSelectedVendorOfferingTemplateTO().getVendorOfferingID());
        sysBenefitsVendorBean.setOfferingCodeTO(offeringCodeTO);

    }

    /**
     * 
     * @param offeringCodeTO
     */
    public void redirectToOfferCodeMoreInfo(OfferingCodeTO offeringCodeTO)
    {
        if (offeringCodeTO.isMaximumLimitApplicable()) {
            sysBenefitsVendorBean.setOfferingCodeMaximumLimitTO(offeringCodeTO.getCodeMaximumLimitTO());
            sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(true);
        } else {
            sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(false);
        }
        sysBenefitsVendorBean.setOfferingCodeTO(offeringCodeTO);
    }

    /**
     * 
     * @param offeringCodeTO
     * @param edit
     * @throws Exception
     */
    public void saveOfferCode(OfferingCodeTO offeringCodeTO, boolean edit)
        throws Exception
    {
        RequestContext requestContext = null;
        List<OfferingCodeTO> offerringCodeTOlist = null;
        boolean valid = false;
        try {
            requestContext = RequestContext.getCurrentInstance();

            if (offeringCodeTO.isMaximumLimitApplicable() && offeringCodeTO.getCodeMaximumLimitTO() != null) {
                try {
                    Long.parseLong(offeringCodeTO.getCodeMaximumLimitTO().getMaximumUsageCount().toString());
                } catch (NumberFormatException nfe) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_MAXIMUM_COUNT_ERROR_MSG, ""));
                    return;
                }

            }
            offeringCodeTO.setEdit(edit);
            valid = validateOfferCode(offeringCodeTO, edit, cloneVendorOfferingCodeList(sysBenefitsVendorBean.getOfferingCodeTOList()));
            if (valid) {
                sysVendorOfferingTemplateService.saveOfferCode(offeringCodeTO);
                hrVendorOfferingSequenceService.getAllEmpGroupBasedOfferingList();
                sysSectorVendorOfferingSequenceService.getAllSectorBasedOfferingList();
                FacesContext.getCurrentInstance()
                        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstants.SYS_BENEFITS_OFFERING_CODE_SAVE_SUCCESS_MSG, ""));
                requestContext.execute(IBenefitsConstants.OFFER_CODE_DIALOG_HIDE);
                offerringCodeTOlist = sysVendorOfferingTemplateService.getOfferingCodeListByVendorOffering(offeringCodeTO.getVendorOfferingID());
                sysBenefitsVendorBean.setOfferingCodeTOList(offerringCodeTOlist);
                sysBenefitsVendorBean.setFiltedredOfferingCodeTOlist(offerringCodeTOlist);
                sysBenefitsVendorBean.setRenderFormMessage(false);
                requestContext.execute(IBenefitsConstants.OFFER_CODE_DATATABLE_RESET);
                requestContext.execute(IBenefitsConstants.OFFER_CODE_PAGINATION_DATATABLE_RESET);
            } else {
                sysBenefitsVendorBean.setRenderFormMessage(true);
                FacesContext.getCurrentInstance()
                        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, IBenefitsConstants.SYS_BENEFITS_DUPLICATE_OFFER_CODE_ERROR_MSG, ""));
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_OFFERING_CODE_SAVE_ERROR_MSG, ""));
            requestContext.execute(IBenefitsConstants.OFFER_CODE_DIALOG_HIDE);
            sysBenefitsVendorBean.setRenderFormMessage(false);
        }
    }

    /**\
     * 
     * @param offeringCode
     * @return
     */
    private boolean validateOfferCode(OfferingCodeTO selectedOfferingCodeTO, boolean edit, List<OfferingCodeTO> existingOfferringCodeTOlist)
        throws Exception
    {
        boolean valid = true;
        try {
            for (OfferingCodeTO offeringCodeTO : existingOfferringCodeTOlist) {
                if (offeringCodeTO.getOfferingCode().equalsIgnoreCase(selectedOfferingCodeTO.getOfferingCode().trim())) {
                    valid = false;
                    if (edit) {
                        if (offeringCodeTO.getOfferingCodeID().equals(selectedOfferingCodeTO.getOfferingCodeID())) {
                            valid = true;
                        }
                    }
                    break;
                }
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
        return valid;
    }

    public void updateOfferCode(OfferingCodeTO offeringCodeTO)
    {
        OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO = null;
        List<ContentTypeTO> maximumUsageDurationTypeTOList = null;
        try {
            offeringCodeMaximumLimitTO = offeringCodeTO.getCodeMaximumLimitTO();
            sysBenefitsVendorBean.setOfferingCodeTO(offeringCodeTO);
            sysBenefitsVendorBean.setEditOfferCodeForm(true);
            if (offeringCodeTO.isMaximumLimitApplicable() && offeringCodeMaximumLimitTO != null) {
                sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(true);
                sysBenefitsVendorBean.setOfferingCodeMaximumLimitTO(offeringCodeMaximumLimitTO);
                offeringCodeMaximumLimitTO.setMaximumUsageTypeTOList(sysBenefitsVendorBean.getOfferingAccessTypeTOList());
                maximumUsageDurationTypeTOList = offeringCodeMaximumLimitTO.getMaximumUsageDurationTypeTOList();
                if (maximumUsageDurationTypeTOList == null) {
                    maximumUsageDurationTypeTOList = sysContentTypeService.getSysContentTypeList(IBenefitsConstant.OFFER_CODE_USAGE_DURATION_TYPE_STRING);
                    offeringCodeMaximumLimitTO.setMaximumUsageDurationTypeTOList(maximumUsageDurationTypeTOList);
                }

            } else {
                sysBenefitsVendorBean.setRenderFormMessage(true);
                sysBenefitsVendorBean.setRenderMaximumLimitApplicableDetail(false);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    /**
     * redirecting to More Info VendorOffering
     * 
     * */
    public void redirectToVendoringOfferMoreInfo(VendorOfferingTemplateTO offeringTemplateTO)
    {
        try {
            sysBenefitsVendorBean.setVendorOfferingTemplateTO(offeringTemplateTO);
            if (offeringTemplateTO.isPeakBasedOffering()) {
                sysBenefitsVendorBean.setRenderVendorOffering(true);
            } else {
                sysBenefitsVendorBean.setRenderVendorOffering(false);
            }
            sysBenefitsVendorBean.setPeakRecurringDetailTO(offeringTemplateTO.getPeakRecurringDetailTO());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    @SuppressWarnings("deprecation")
    public void downloadTemplate()
    {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(OfferCodeField.OFFERCODE.getOfferCodeField());
        HSSFSheet hidden = workbook.createSheet("hidden");
        HSSFSheet hidden1 = workbook.createSheet("hidden1");
        HSSFSheet hidden2 = workbook.createSheet("hidden2");
        Date startDate = new Date("1/1/1970/");
        Date endDate = new Date("1/31/9999");
        List<String> headerList = null;
        int cellnum = 0;
        int columnIndex = 0;
        try {
            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            style.setFont(font);
            headerList = getHeaderList();
            HSSFRow row1 = sheet.createRow(0);
            for (String headName : headerList) {
                Cell cell1 = row1.createCell(cellnum++);
                cell1.setCellValue(headName);
                cell1.setCellStyle(style);
                sheet.setColumnWidth(columnIndex++, 5099);
            }
            CellRangeAddressList addressList = new CellRangeAddressList(1, 1000, 4, 5);
            DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(new String[] { "TRUE", "FALSE" });
            HSSFDataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
            dataValidation.setSuppressDropDownArrow(false);
            sheet.addValidationData(dataValidation);

            for (int i = 0; i < sysBenefitsVendorBean.getAccessTypeNameList().size(); i++) {
                String name = sysBenefitsVendorBean.getAccessTypeNameList().get(i);
                HSSFRow row = hidden.createRow(i);
                HSSFCell cell = row.createCell(0);
                cell.setCellValue(name);
            }
            Name namedCell = workbook.createName();
            namedCell.setNameName("hidden");
            namedCell.setRefersToFormula("hidden!$A$1:$A$" + sysBenefitsVendorBean.getAccessTypeNameList().size());
            DVConstraint constraintAccessTypeName = DVConstraint.createFormulaListConstraint("hidden");
            CellRangeAddressList addressListOfferCodeNamearray = new CellRangeAddressList(1, 1000, 1, 1);
            HSSFDataValidation validation = new HSSFDataValidation(addressListOfferCodeNamearray, constraintAccessTypeName);
            workbook.setSheetHidden(1, true);
            sheet.addValidationData(validation);

            List<ContentTypeTO> maximumUsageDurationTypeTOList = sysBenefitsVendorBean.getMaximumUsageDurationTypeTOList();
            for (int i = 0; i < maximumUsageDurationTypeTOList.size(); i++) {
                String name = maximumUsageDurationTypeTOList.get(i).getSysType();
                HSSFRow row = hidden1.createRow(i);
                HSSFCell cell = row.createCell(0);
                cell.setCellValue(name);
            }
            Name namedUsageDurationTypeList = workbook.createName();
            namedUsageDurationTypeList.setNameName("hidden1");
            namedUsageDurationTypeList.setRefersToFormula("hidden1!$A$1:$A$" + maximumUsageDurationTypeTOList.size());
            DVConstraint constraintNamedUsageDurationTypeList = DVConstraint.createFormulaListConstraint("hidden1");
            CellRangeAddressList addressUsageDurationType = new CellRangeAddressList(1, 1000, 6, 6);
            HSSFDataValidation validationUsageDurationType = new HSSFDataValidation(addressUsageDurationType, constraintNamedUsageDurationTypeList);
            workbook.setSheetHidden(2, true);
            sheet.addValidationData(validationUsageDurationType);

            SimpleDateFormat sdf = new SimpleDateFormat(IBenefitsConstant.DATE_FORMAT);
            CellRangeAddressList addressListDate = new CellRangeAddressList(1, 10000, 2, 3);
            DVConstraint dvConstraintDate = DVConstraint.createDateConstraint(DataValidationConstraint.OperatorType.BETWEEN, "01/01/1900", "12/31/9999",
                    IBenefitsConstant.DATE_FORMAT);
            HSSFDataValidation dataValidationDate = new HSSFDataValidation(addressListDate, dvConstraintDate);
            dataValidationDate.setSuppressDropDownArrow(false);
            dataValidationDate.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidationDate.createErrorBox("InValid Data", "Date Should be in in format " + IBenefitsConstant.DATE_FORMAT);
            sheet.addValidationData(dataValidationDate);

            CellRangeAddressList maximumCount = new CellRangeAddressList(1, 10000, 8, 8);
            DVConstraint dvConstraintCount = DVConstraint.createNumericConstraint(DataValidationConstraint.ValidationType.INTEGER, DataValidationConstraint.OperatorType.BETWEEN,
                    "1", "100");
            HSSFDataValidation dataValidationCount = new HSSFDataValidation(maximumCount, dvConstraintCount);
            dataValidationCount.setSuppressDropDownArrow(false);
            dataValidationCount.setErrorStyle(DataValidation.ErrorStyle.STOP);
            dataValidationCount.createErrorBox("InValid Data", "Maximum count should be numeric between 1 and 100");
            sheet.addValidationData(dataValidationCount);


            for (int i = 0; i < sysBenefitsVendorBean.getAccessTypeNameList().size(); i++) {
                String name = sysBenefitsVendorBean.getAccessTypeNameList().get(i);
                HSSFRow row = hidden2.createRow(i);
                HSSFCell cell = row.createCell(0);
                cell.setCellValue(name);
            }
            Name namedCell2 = workbook.createName();
            namedCell2.setNameName("hidden2");
            namedCell2.setRefersToFormula("hidden!$A$1:$A$" + sysBenefitsVendorBean.getAccessTypeNameList().size());
            DVConstraint constraintAccessTypeName2 = DVConstraint.createFormulaListConstraint("hidden2");
            CellRangeAddressList addressListOfferCodeNamearray2 = new CellRangeAddressList(1, 1000, 7, 7);
            HSSFDataValidation validation2 = new HSSFDataValidation(addressListOfferCodeNamearray2, constraintAccessTypeName2);
            workbook.setSheetHidden(3, true);
            sheet.addValidationData(validation2);


            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"OfferCode_" + sysBenefitsVendorBean.getSelectedVendorOfferingTemplateTO().getOfferTemplateName()
                    + ".xls\"");
            workbook.write(ec.getResponseOutputStream());
            ec.getResponseOutputStream().flush();
            ec.getResponseOutputStream().close();
            fc.responseComplete();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }


    /**
     * 
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event)
    {
        UploadedFile uploadedFile = null;
        try {
            uploadedFile = event.getFile();
            if (uploadedFile != null && uploadedFile.getContentType().equalsIgnoreCase(CoreConstants.EXCEL_CONTENT_TYPE)) {
                sysBenefitsVendorBean.setUploadedFile(uploadedFile);
                sysBenefitsVendorBean.setUploadedFileName(uploadedFile.getFileName());

            } else {
                sysBenefitsVendorBean.setUploadedFileName(null);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.UPLOAD_INVALID_FILE_MSG, ""));
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    /**
     * 
     * @param s
     * @return
     */
    private String getOfferCodePath(String s)
        throws Exception
    {
        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String getPath = null;
        String finalPath = null;
        File dir = null;
        String fileLocation = null;
        boolean success = false;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(CoreConstants.DATETIME_FORMAT_STRING);
            resourceBundle = ResourceBundle.getBundle(CoreConstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(IBenefitsConstant.CONTENT_PATH_NAME);
            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception(CoreConstants.INVALID_PATH_STRING);
            }
            fileLocation = sysBenefitsVendorBean.getSelectedVendorTO().getVendorName() + File.separator
                    + sysBenefitsVendorBean.getSelectedVendorOfferingTemplateTO().getOfferTemplateName() + File.separator + dateFormat.format(new Date());
            finalPath = initialPath + fileLocation;
            dir = new File(finalPath);
            if (!dir.exists()) {
                success = dir.mkdirs();
            }
            if (success) {
                LOGGER_.info("-----Directory created Successfully...!!------------: - " + dir);
                getPath = finalPath + File.separator + s;

            } else {
                getPath = null;
                LOGGER_.error("-----Unable to make directory...!!------------");
            }
        } catch (Exception e) {
            throw e;
        }
        return getPath;
    }


    /**
     * uploading master data
     */
    public void uploadOfferCodeMasterData()
        throws Exception
    {
        UploadedFile uploadedFile = null;
        String path = null;
        String fileName = null;
        InputStream in = null;
        try {
            uploadedFile = sysBenefitsVendorBean.getUploadedFile();
            if (uploadedFile != null) {
                fileName = uploadedFile.getFileName();
                in = uploadedFile.getInputstream();
                path = getOfferCodePath(fileName);
                OutputStream out = new FileOutputStream(new File(path));
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = in.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                in.close();
                out.close();
                out.flush();
                prepareOfferCodeListFromExcel(path);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_EMPTY_FILE_MSG, ""));
            }
        } catch (IOException e) {
            LOGGER_.error("", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstants.SYS_BENEFITS_OFFERING_CODE_SAVE_SUCCESS_MSG, ""));
        }
    }

    /**
     * redirecting to master page upload
     */
    public void offerCodeMasterDataUpload()
    {
        sysBenefitsVendorBean.setSummaryList(null);
        sysBenefitsVendorBean.setMasterOfferDataList(null);
        menuBean.setPage("../altbenefits/uploadOfferCodes.xhtml");
    }

    public void redirectTosummaryList(List<String> summaryList)
    {
        sysBenefitsVendorBean.setSummaryList(summaryList);

    }


    /**
     * 
     * @param file
     */
    public boolean prepareOfferCodeListFromExcel(String file)
    {
        List<OfferingCodeTO> offerCodeTOList = null;
        OfferingCodeTO offeringCodeTO = null;
        FileInputStream fis = null;
        Workbook workbook1 = null;
        int rowNumber = 0;
        boolean flag = false;
        boolean valid = true;
        List<String> headerList = null;
        String summary = null;
        String cellName = null;
        boolean validate = true;
        List<String> summaryList = null;
        String errorMseesage = null;
        OfferingCodeMaximumLimitTO codeMaximumLimitTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            headerList = new ArrayList<String>();
            offerCodeTOList = new ArrayList<OfferingCodeTO>();
            fis = new FileInputStream(file);
            workbook1 = WorkbookFactory.create(fis);
            int numberOfSheets = 1;
            for (int i = 0; i < numberOfSheets; i++) {
                Sheet sheet = workbook1.getSheetAt(i);
                Iterator<Row> rowIterator = sheet.iterator();
                if (!valid) {
                    break;
                }
                while (rowIterator.hasNext()) {
                    offeringCodeTO = new OfferingCodeTO();
                    summary = null;
                    summaryList = new ArrayList<String>();
                    Row row = (Row) rowIterator.next();
                    if (rowNumber == 0) {
                        int numberofcells = row.getLastCellNum();
                        if (numberofcells != Integer.parseInt(IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_COLUMNCOUNT)) {
                            if (numberofcells > Integer.parseInt(IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_COLUMNCOUNT)) {
                                errorMseesage = IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_EXCESS_COLUMN_COUNT_MSG;
                            } else {
                                errorMseesage = IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_LESS_COLUMN_COUNT_MSG;
                            }
                            valid = false;
                            break;
                        }
                        Iterator<Cell> cellIterator = row.cellIterator();
                        while (cellIterator.hasNext()) {
                            Cell cell = (Cell) cellIterator.next();
                            if (cell.getCellType() != Cell.CELL_TYPE_BLANK && cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                headerList.add(cell.getStringCellValue());
                            } else {
                                headerList.add(IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_INVALID_HEADER);
                                errorMseesage = IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_LESS_COLUMN_COUNT_MSG;
                                valid = false;
                                break;
                            }

                        }
                        headerList.add(IBenefitsConstant.UPLOAD_STATUS);
                        rowNumber++;
                    } else {
                        Iterator<Cell> cellIterator = row.cellIterator();
                        while (cellIterator.hasNext()) {
                            Cell cell = (Cell) cellIterator.next();
                            try {
                                /**
                                 * Cell with index 0 contains Offer code Name
                                 */
                                if (cell.getColumnIndex() == 0) {
                                    cellName = OfferCodeField.OFFERCODE.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setOfferingCode(cell.getStringCellValue());
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 1 contains offer code access type
                                 */
                                else if (cell.getColumnIndex() == 1) {
                                    cellName = OfferCodeField.ACCESSTYPE.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setAccessType(cell.getStringCellValue());
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 2 contains start date
                                 */
                                else if (cell.getColumnIndex() == 2) {
                                    cellName = OfferCodeField.STARTDATE.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setOfferingCodeStartDate(cell.getDateCellValue());
                                        offeringCodeTO.setOfferingCodeStartDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeStartDate()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 3 contains offer end date
                                 */
                                else if (cell.getColumnIndex() == 3) {
                                    cellName = OfferCodeField.ENDDATE.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setOfferingCodeEndDate(cell.getDateCellValue());
                                        offeringCodeTO.setOfferingCodeEndDateString(CommonUtilHelper.changeDateFormat_MMMddYYYY(offeringCodeTO.getOfferingCodeEndDate()));
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 4 contains offer Is active
                                 */
                                else if (cell.getColumnIndex() == 4) {
                                    cellName = OfferCodeField.ISACTIVE.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setActive(cell.getBooleanCellValue());
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 5 contains offer code Is Maximum limit applicable
                                 */
                                else if (cell.getColumnIndex() == 5) {
                                    cellName = OfferCodeField.ISMAXIMUMLIMIT.getOfferCodeField();
                                    if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                        offeringCodeTO.setMaximumLimitApplicable(cell.getBooleanCellValue());
                                    } else {
                                        validate = false;
                                        summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                    }
                                }
                                /**
                                 * Cell with index 6 contains offer code maximum usage type
                                 */
                                else if (cell.getColumnIndex() == 6) {
                                    cellName = OfferCodeField.MAXIMUMUSAGETYPE.getOfferCodeField();
                                    if (offeringCodeTO.isMaximumLimitApplicable()) {
                                        if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                            codeMaximumLimitTO = new OfferingCodeMaximumLimitTO();
                                            codeMaximumLimitTO.setMaximumUsageType(cell.getStringCellValue());
                                            for(ContentTypeTO contentTypeTO : sysBenefitsVendorBean.getMaximumUsageDurationTypeTOList()) {
                                            	if(contentTypeTO.getSysType().equals(cell.getStringCellValue())) {
                                            		codeMaximumLimitTO.setMaximumUsageTypeID(contentTypeTO.getSysTypeID());
                                            	}
                                            }
                                        } else {
                                            validate = false;
                                            summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                        }
                                    }

                                }
                                /**
                                 * Cell with index 7 contains offer code maximum usage duration type
                                 */
                                else if (cell.getColumnIndex() == 7) {
                                    cellName = OfferCodeField.MAXIMUMDURATIONTYPE.getOfferCodeField();
                                    if (offeringCodeTO.isMaximumLimitApplicable()) {
                                        if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                            codeMaximumLimitTO.setMaximumUsageDuration(cell.getStringCellValue());
                                            for(ContentTypeTO contentTypeTO : sysBenefitsVendorBean.getOfferingAccessTypeTOList()) {
                                            	if(contentTypeTO.getSysType().equals(cell.getStringCellValue())) {
                                            		codeMaximumLimitTO.setMaximumUsageDurationID(contentTypeTO.getSysTypeID());
                                            		offeringCodeTO.setAccessTypeID(contentTypeTO.getSysTypeID());
                                            	}
                                            }
                                        } else {
                                            validate = false;
                                            summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                        }
                                    }
                                }

                                /**
                                 * Cell with index 8 contains offer code maximum count
                                 */
                                else if (cell.getColumnIndex() == 8) {
                                    cellName = OfferCodeField.MAXIMUMCOUNT.getOfferCodeField();
                                    if (offeringCodeTO.isMaximumLimitApplicable()) {
                                        if (cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                                            codeMaximumLimitTO.setMaximumUsageCount(new Double(cell.getNumericCellValue()).longValue());
                                        } else {
                                            validate = false;
                                            summaryList.add(cellName + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                                        }
                                    }
                                }

                            } catch (IllegalStateException npe) {
                                validate = false;
                            }
                            rowNumber++;
                            offeringCodeTO.setCodeMaximumLimitTO(codeMaximumLimitTO);
                        }
                        if (summaryList.size() == 0) {
                            summary = IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_VALID_RECORD;
                        } else {
                            summary = IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_INVALID_RECORD;
                        }
                        if (offeringCodeTO.getOfferingCode() == null) {
                            validate = false;
                            summary = IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_INVALID_RECORD;
                            summaryList.add(OfferCodeField.OFFERCODE.getOfferCodeField() + IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_EMPTY_COLUMN);
                        }
                        offeringCodeTO.setSummary(summary);
                        offeringCodeTO.setSummaryList(summaryList);
                        offeringCodeTO.setVendorOfferingID(sysBenefitsVendorBean.getSelectedVendorOfferingTemplateTO().getVendorOfferingID());
                        offerCodeTOList.add(offeringCodeTO);
                    }
                    sysBenefitsVendorBean.setMasterOfferDataList(offerCodeTOList);
                }

            }

            if (valid) {
                sysBenefitsVendorBean.setExcelHeaderList(headerList);
                validate = hasDuplicates(offerCodeTOList);
                if (validate) {
                    validate = checkForDublicateOfferCode(cloneVendorOfferingCodeList(sysBenefitsVendorBean.getOfferingCodeTOList()), offerCodeTOList);
                    if (validate) {
                        if (offerCodeTOList.isEmpty()) {
                            FacesContext.getCurrentInstance().addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_EMPTY_SHEET, ""));
                        } else {
                            offerCodeTOList = sysVendorOfferingTemplateService.saveOfferCodeMasterUpload(offerCodeTOList, sysBenefitsVendorBean
                                    .getSelectedVendorOfferingTemplateTO().getVendorOfferingID());
                            sysBenefitsVendorBean.setMasterOfferDataList(offerCodeTOList);
                            hrVendorOfferingSequenceService.getAllEmpGroupBasedOfferingList();
                            sysSectorVendorOfferingSequenceService.getAllSectorBasedOfferingList();
                            FacesContext.getCurrentInstance().addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_SUCCESS_MSG, ""));
                            sysBenefitsVendorBean.setUploadedFile(null);
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.MASTER_OFFERCODE_UPLOAD_CONSTRAINTS_VOILATION_MSG, ""));
                }
            } else {
                sysBenefitsVendorBean.setMasterOfferDataList(new ArrayList<OfferingCodeTO>());
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMseesage, ""));
            }
            sysBenefitsVendorBean.setUploadedFile(null);
            sysBenefitsVendorBean.setUploadedFileName(null);
            sysBenefitsVendorBean.setMasterOfferDataList(offerCodeTOList);
        } catch (Exception e) {
            LOGGER_.error("", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.MASTER_OFFERCODE_SAVE_MASTER_DATA_ERROR_MSG, ""));
        }
        return flag;
    }

    /**
     * 
     * @param cloneVendorOfferingCodeList
     * @param vendorOfferingCodeList
     * @return
     */
    private boolean checkForDublicateOfferCode(List<OfferingCodeTO> cloneVendorOfferingCodeList, List<OfferingCodeTO> vendorOfferingCodeList)
    {
        boolean valid = true;
        try {
            for (OfferingCodeTO selectedOfferingCodeTO : vendorOfferingCodeList) {
                for (OfferingCodeTO offeringCodeTO : cloneVendorOfferingCodeList) {
                    if (offeringCodeTO.getOfferingCode().equalsIgnoreCase(selectedOfferingCodeTO.getOfferingCode().trim())) {
                        selectedOfferingCodeTO.getSummaryList().add(
                                OfferCodeField.OFFERCODE.getOfferCodeField() + ":- " + offeringCodeTO.getOfferingCode() + " " + IBenefitsConstant.OFFERCODE_DUBLICATE_MESSAGE);
                        selectedOfferingCodeTO.setSummary(IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_INVALID_RECORD);
                        valid = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            throw e;
        }
        return valid;

    }

    /**
     * 
     * @return
     */
    List<String> getHeaderList()
    {
        List<String> headerNames = new ArrayList<String>();
        headerNames.add(OfferCodeField.OFFERCODE.getOfferCodeField());
        headerNames.add(OfferCodeField.ACCESSTYPE.getOfferCodeField());
        headerNames.add(OfferCodeField.STARTDATE.getOfferCodeField() + " (" + IBenefitsConstant.DATE_FORMAT + ")");
        headerNames.add(OfferCodeField.ENDDATE.getOfferCodeField() + " (" + IBenefitsConstant.DATE_FORMAT + ")");
        headerNames.add(OfferCodeField.ISACTIVE.getOfferCodeField());
        headerNames.add(OfferCodeField.ISMAXIMUMLIMIT.getOfferCodeField());
        headerNames.add(OfferCodeField.MAXIMUMUSAGETYPE.getOfferCodeField());
        headerNames.add(OfferCodeField.MAXIMUMDURATIONTYPE.getOfferCodeField());
        headerNames.add(OfferCodeField.MAXIMUMCOUNT.getOfferCodeField());
        return headerNames;
    }

    /**
     * 
     * @param offeringTemplateTOList
     * @return
     */
    private List<OfferingCodeTO> cloneVendorOfferingCodeList(List<OfferingCodeTO> offeringCodeTOList)
        throws CloneNotSupportedException
    {
        List<OfferingCodeTO> cloneOfferingList = new ArrayList<OfferingCodeTO>();
        try {
            if (cloneOfferingList != null) {
                for (OfferingCodeTO offer : offeringCodeTOList) {
                    cloneOfferingList.add((OfferingCodeTO) offer.clone());

                }
            }
        } catch (CloneNotSupportedException cne) {
            LOGGER_.error("", cne);
            throw cne;
        }
        return cloneOfferingList;
    }

    /**
     * 
     * @param offeringCodeTOList
     * @return
     */
    private boolean hasDuplicates(List<OfferingCodeTO> offeringCodeTOList)
    {
        boolean valid = true;
        final List<String> usedNames = new ArrayList<String>();
        for (OfferingCodeTO offer : offeringCodeTOList) {
            final String offerCode = offer.getOfferingCode().trim();
            if (usedNames.contains(offerCode)) {
                offer.getSummaryList().add(IBenefitsConstant.OFFERCODE_DUBLICATE_MESSAGE2 + ":- " + offerCode);
                offer.setSummary(IBenefitsConstant.OFFERCODE_UPLOAD_PAGE_INVALID_RECORD);
                valid = false;
            }
            usedNames.add(offerCode);
        }

        return valid;
    }


    /**
     * 
     * @param s
     * @return
     */
    private String getVendorLogoPath(String s,SysBenefitVendorTO benefitVendorTO )
        throws Exception
    {
        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String getPath = null;
        String finalPath = null;
        File dir = null;
        String serverPath = null;
        String fileLocation = null;
        String returnPath = null;
        try {
            resourceBundle = ResourceBundle.getBundle(CoreConstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(IBenefitsConstant.VENDOR_LOGO_PATH);
            serverPath = resourceBundle.getString(IBenefitsConstant.VENDOR_LOGO_SERVER_PATH);
            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception(CoreConstants.INVALID_PATH_STRING);
            }
            fileLocation = benefitVendorTO.getVendorName() + benefitVendorTO.getVendorCode();
            finalPath = initialPath + fileLocation;
            dir = new File(finalPath);
            if (!dir.exists()) {
            dir.mkdirs();
            }
            getPath = finalPath + File.separator + s;
            returnPath = getPath + CoreConstants.AT_SPERATOR + serverPath + CoreConstants.AT_SPERATOR + fileLocation + File.separator + s;
        } catch (Exception ex) {
            throw ex;
        }
        return returnPath;
    }


    /**
     * 
     * @throws Exception
     */
    public String uploadVendorLogo(SysBenefitVendorTO benefitVendorTO)
        throws Exception
    {
        UploadedFile uploadedFile = null;
        String path = null;
        String fileName = null;
        InputStream in = null;
        String serverPath = null;
        try {
            uploadedFile = sysBenefitsVendorBean.getVendorLogoFile();
            if (uploadedFile != null) {
                fileName = uploadedFile.getFileName();
                in = uploadedFile.getInputstream();
                path = getVendorLogoPath(fileName,benefitVendorTO);
                if (path != null) {
                    OutputStream out = new FileOutputStream(new File(path.split(CoreConstants.AT_SPERATOR)[0]));
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    while ((read = in.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    in.close();
                    out.close();
                    out.flush();
                    serverPath = path.split(CoreConstants.AT_SPERATOR)[1] + path.split(CoreConstants.AT_SPERATOR)[2];
                }
            }
        } catch (IOException e) {
            LOGGER_.error("", e);
            throw e;
        }
        return serverPath;
    }


    /**
     * 
     * @param event
     */
    public void handleVendorLogoUpload(FileUploadEvent event)
    {
        UploadedFile uploadedFile = null;
        try {
            uploadedFile = event.getFile();
            if (uploadedFile != null &&  uploadedFile.getContentType().startsWith("image/")) {
                sysBenefitsVendorBean.setVendorLogoFile(uploadedFile);
                sysBenefitsVendorBean.setUploadedVendorLogoFileName(uploadedFile.getFileName());

            } else {
                sysBenefitsVendorBean.setVendorLogoFile(uploadedFile);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.UPLOAD_INVALID_FILE_MSG, ""));
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }


}