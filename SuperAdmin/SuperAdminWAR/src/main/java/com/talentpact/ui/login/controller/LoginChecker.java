package com.talentpact.ui.login.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.UserSessionBean;

@Named
@RequestScoped
public class LoginChecker {

    @Inject
    UserSessionBean userSessionBean;

    @PostConstruct
    public void check()
        throws IOException {
        if (userSessionBean.getUserID() == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/TalentPactAdminPortal/logout.jsf");
        }
    }

}
