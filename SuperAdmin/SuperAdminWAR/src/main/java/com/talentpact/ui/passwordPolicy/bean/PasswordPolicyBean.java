package com.talentpact.ui.passwordPolicy.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.contentType.transport.input.ContentTypeTO;
import com.alt.passwordPolicy.to.PasswordPolicyTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.passwordPolicy.controller.PasswordPolicyController;

/**
 * 
 * @author javed.ali
 * 
 */


@Named("passwordPolicyBean")
@ConversationScoped
public class PasswordPolicyBean extends CommonBean implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = -1817704848121231271L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(PasswordPolicyBean.class);

    @Inject
    PasswordPolicyController passwordPolicyController;

    private List<String> passwordTypeList;

    private boolean showHome;

    private List<String> characterListForPasswordAttemp;

    private List<String> characterListForOldPasswordAttemp;

    private PasswordPolicyTO passwordPolicyTO;

    private List<String> minCharactersList;

    private List<ContentTypeTO> passwordChangeFrequency;

    private boolean renderpasswordTypePanel;

    private boolean edit;

    private String[] passwordTypes;


    public boolean isShowHome()
    {
        return showHome;
    }

    public void setShowHome(boolean showHome)
    {
        this.showHome = showHome;
    }

    public String[] getPasswordTypes()
    {
        return passwordTypes;
    }

    public void setPasswordTypes(String[] passwordTypes)
    {
        this.passwordTypes = passwordTypes;
    }

    public boolean isEdit()
    {
        return edit;
    }

    public void setEdit(boolean edit)
    {
        this.edit = edit;
    }

    public boolean isRenderpasswordTypePanel()
    {
        return renderpasswordTypePanel;
    }

    public void setRenderpasswordTypePanel(boolean renderpasswordTypePanel)
    {
        this.renderpasswordTypePanel = renderpasswordTypePanel;
    }

    public List<ContentTypeTO> getPasswordChangeFrequency()
    {
        return passwordChangeFrequency;
    }

    public void setPasswordChangeFrequency(List<ContentTypeTO> passwordChangeFrequency)
    {
        this.passwordChangeFrequency = passwordChangeFrequency;
    }

    public PasswordPolicyTO getPasswordPolicyTO()
    {
        return passwordPolicyTO;
    }

    public void setPasswordPolicyTO(PasswordPolicyTO passwordPolicyTO)
    {
        this.passwordPolicyTO = passwordPolicyTO;
    }

    public List<String> getMinCharactersList()
    {
        return minCharactersList;
    }

    public List<String> getPasswordTypeList()
    {
        return passwordTypeList;
    }

    public void setMinCharactersList(List<String> minCharactersList)
    {
        this.minCharactersList = minCharactersList;
    }

    public void setPasswordTypeList(List<String> passwordTypeList)
    {
        this.passwordTypeList = passwordTypeList;
    }

    public List<String> getCharacterListForOldPasswordAttemp()
    {
        return characterListForOldPasswordAttemp;
    }

    public List<String> getCharacterListForPasswordAttemp()
    {
        return characterListForPasswordAttemp;
    }

    public void setCharacterListForOldPasswordAttemp(List<String> characterListForOldPasswordAttemp)
    {
        this.characterListForOldPasswordAttemp = characterListForOldPasswordAttemp;
    }

    public void setCharacterListForPasswordAttemp(List<String> characterListForPasswordAttemp)
    {
        this.characterListForPasswordAttemp = characterListForPasswordAttemp;
    }

    @Override
    public void initialize()
    {
        try {
            beginConversation();
            passwordPolicyController.initialize();
        } catch (Exception e) {
            LOGGER_.error(e.getMessage(), e);
        }
    }

}