/**
 * 
 */
package com.talentpact.ui.hrContentType.dataservice.DS;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.HrContentType;
import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;
import com.talentpact.ui.hrContentType.converter.HrContentTypeConverter;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value =  TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class HrContentTypeDS extends AbstractDS<HrContentType>{

	public HrContentTypeDS(){
		super(HrContentType.class);
	}
	
	public List<HrContentTypeResponseTO> getHrContentTypeByContentCategory(String contentCategory)
		throws Exception{
		
		if(contentCategory==null && contentCategory.equals(""))
			return null;
		
		List<HrContentTypeResponseTO> hrContentTypeResponseTOList = null;
		List<HrContentType> hrContentTypeList = null;
		HrContentTypeResponseTO hrContentTypeResponseTOObj = null;
		StringBuilder hqlQuery = null;
		Query query = null;
		try{
			hqlQuery = new StringBuilder();
			hqlQuery.append("from HrContentType hct where hct.contentCategory = :contentCat");
			query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
			query.setParameter("contentCat", contentCategory);
			hrContentTypeList = query.getResultList();
			if(hrContentTypeList!=null && !hrContentTypeList.isEmpty()){
				hrContentTypeResponseTOList = new LinkedList<HrContentTypeResponseTO>();
				for(HrContentType contentType : hrContentTypeList){
					hrContentTypeResponseTOObj = HrContentTypeConverter.getHrContentTypeResponseTOByEntity(
							contentType);
					if(hrContentTypeResponseTOObj!=null){
						hrContentTypeResponseTOList.add(hrContentTypeResponseTOObj);
					}
				}
			}
		}catch(Exception ex){
			hrContentTypeResponseTOList = null;
			throw ex;
		}
		
		return hrContentTypeResponseTOList;
	}
}
