/**
 * 
 */
package com.talentpact.ui.sysOfferingCategory.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.constants.ISysOfferingCategoryConstants;
import com.talentpact.business.dataservice.SysOfferingCategory.SysOfferingCategoryService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysOfferingCategory.bean.SysOfferingCategoryBean;

/**
 * 
 * @author vivek.goyal
 * 
 */


@Named("sysOfferingCategoryController")
@ConversationScoped
public class SysOfferingCategoryController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysOfferingCategoryController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysOfferingCategoryBean sysOfferingCategoryBean;

    @Inject
    SysOfferingCategoryService sysOfferingCategoryService;

    @Override
    public void initialize()
    {
        List<SysOfferingCategoryTO> sysOfferingCategoryTOList = null;
        Set<String> name = null;
        SysOfferingCategoryTO sysOfferingCategoryTO = null;

        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            resetSysOfferingCategory();
            sysOfferingCategoryTOList = sysOfferingCategoryService.getSysOfferingCategoryList();
            sysOfferingCategoryBean.setSysOfferingCategoryTOList(sysOfferingCategoryTOList);
            name = new HashSet<String>();
            for (SysOfferingCategoryTO list : sysOfferingCategoryTOList) {
                sysOfferingCategoryTO = new SysOfferingCategoryTO();
                sysOfferingCategoryTO = list;
                name.add(sysOfferingCategoryTO.getOfferingCategory());
            }

            sysOfferingCategoryBean.setSysOfferingCategory(name);
            menuBean.setPage("../SysOfferingCategory/sysOfferingCategory.xhtml");
            sysOfferingCategoryBean.setRenderSysOfferingCategoryPopup(false);
            sysOfferingCategoryBean.setRenderErrorMessage(false);
            requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_DATATABLE_RESET);
            requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysOfferingCategory()
    {
        sysOfferingCategoryBean.setRenderSysOfferingCategoryPopup(true);
        sysOfferingCategoryBean.setOfferingCategory(null);
        sysOfferingCategoryBean.setSysOfferingCategory(null);
        sysOfferingCategoryBean.setDescription(null);
        sysOfferingCategoryBean.setSysOfferingCategoryTOList(null);
        sysOfferingCategoryBean.setSelectedOfferingCategory(null);
    }

    public void addSysOfferingCategory()
    {
        SysOfferingCategoryTO sysOfferingCategoryTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysOfferingCategoryTO = new SysOfferingCategoryTO();
            sysOfferingCategoryTO.setOfferingCategory(sysOfferingCategoryBean.getOfferingCategory());
            sysOfferingCategoryTO.setDescription(sysOfferingCategoryBean.getDescription());
            sysOfferingCategoryBean.setRenderErrorMessage(true);

            if (validateNewSysOfferingCategory(sysOfferingCategoryTO, false)) {
                sysOfferingCategoryService.addSysOfferingCategory(sysOfferingCategoryTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Category saved successfully", ""));
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_ADD_DIALOG_HIDE);
                requestContext.update("sysOfferingCategoryListForm");
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_DATATABLE_RESET);
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_PAGINATION_DATATABLE_RESET);
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + sysOfferingCategoryTO.getOfferingCategory()
                                + "' is already present in the existing SysOfferingCategory list.", ""));
                sysOfferingCategoryBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("addSysOfferingCategoryForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysOfferingCategoryBean.setRenderSysOfferingCategoryPopup(false);
            requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_ADD_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("addSysOfferingCategoryForm");
        }
    }

    // To validate if the newly added SysOfferingCategory is already present in the system
    private boolean validateNewSysOfferingCategory(SysOfferingCategoryTO sysOfferingCategoryTO, boolean edit)
    {
        boolean valid = true;
        List<SysOfferingCategoryTO> sysOfferingCategoryTOList = null;
        try {
            if (sysOfferingCategoryTO.getOfferingCategory() == null || sysOfferingCategoryTO.getOfferingCategory().trim().equals("")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysOfferingCategory", ""));
                valid = false;
            } else {
                sysOfferingCategoryTOList=sysOfferingCategoryService.getSysOfferingCategoryList();
                for (SysOfferingCategoryTO offeringCategoryTO : sysOfferingCategoryTOList) {
                    if (offeringCategoryTO.getOfferingCategory().equalsIgnoreCase(sysOfferingCategoryTO.getOfferingCategory().trim())) {
                        valid = false;
                        if (edit) {
                            if (offeringCategoryTO.getOfferingCategoryID().equals(sysOfferingCategoryTO.getOfferingCategoryID())) {
                                valid = true;
                            }
                        }
                        break;
                    }
                }
            }
            return valid;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void refreshPopUp(SysOfferingCategoryTO sysOfferingCategoryTO)
    {
        try {
            sysOfferingCategoryBean.setOfferingCategoryID(sysOfferingCategoryTO.getOfferingCategoryID());
            sysOfferingCategoryBean.setOfferingCategory(sysOfferingCategoryTO.getOfferingCategory());
            sysOfferingCategoryBean.setDescription(sysOfferingCategoryTO.getDescription());
            sysOfferingCategoryBean.setRenderEditSysOfferingCategoryPopup(true);
            sysOfferingCategoryBean.setDuplicateCategoryName(sysOfferingCategoryTO.getOfferingCategory());
            sysOfferingCategoryBean.setDuplicateDescription(sysOfferingCategoryTO.getDescription());
        } catch (Exception e) {
        }
    }

    public void updateSysOfferingCategory()
    {
        SysOfferingCategoryTO sysOfferingCategoryTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysOfferingCategoryTO = new SysOfferingCategoryTO();
            sysOfferingCategoryTO.setOfferingCategoryID(sysOfferingCategoryBean.getOfferingCategoryID());
            sysOfferingCategoryTO.setOfferingCategory(sysOfferingCategoryBean.getOfferingCategory());
            sysOfferingCategoryTO.setDescription(sysOfferingCategoryBean.getDescription());
            if (validateNewSysOfferingCategory(sysOfferingCategoryTO, true)) {
                sysOfferingCategoryService.editSysOfferingCategory(sysOfferingCategoryTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Offering Category updated successfully", ""));
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_DATATABLE_RESET);
                requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_PAGINATION_DATATABLE_RESET);

            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + sysOfferingCategoryTO.getOfferingCategory()
                                + "' is already present in the existing SysOfferingCategory list.", ""));
                sysOfferingCategoryBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("sysOfferingCategoryEditForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysOfferingCategoryBean.setRenderSysOfferingCategoryPopup(false);
            requestContext.execute(ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("sysOfferingCategoryEditForm");
        }
    }
}