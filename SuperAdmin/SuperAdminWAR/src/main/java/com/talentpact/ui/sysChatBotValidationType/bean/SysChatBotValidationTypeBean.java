package com.talentpact.ui.sysChatBotValidationType.bean;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import java.io.Serializable;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysChatBotValidationType.controller.SysChatBotValidationTypeController;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;

@SuppressWarnings("serial")
@Named("sysChatBotValidationTypeBean")
@ConversationScoped
public class SysChatBotValidationTypeBean extends CommonBean implements
		Serializable, IDefaultAction {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(SysChatBotValidationTypeBean.class);

	@Inject
	SysChatBotValidationTypeController sysChatBotValidationTypeController;

	private List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList;

	private SysChatBotValidationTypeTO sysChatBotValidationTypeTO;
	
	private List<SysFieldTypeResponseTO> sysFieldTypeTOList;

	private Integer sysValidationTypeID;

	private Integer sysFieldTypeID;
	
	private String fieldType;

	private Integer noOfParameters;

	private String label;

	private Boolean renderErrorMessage;

	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			sysChatBotValidationTypeController.initialize();

		} catch (Exception ex) {
			LOGGER_.error(ex.toString());
		}
	}

	public Integer getSysValidationTypeID() {
		return this.sysValidationTypeID;
	}

	public void setSysValidationTypeID(Integer sysValidationTypeID) {
		this.sysValidationTypeID = sysValidationTypeID;
	}

	public Integer getSysFieldTypeID() {
		return this.sysFieldTypeID;
	}

	public void setSysFieldTypeID(Integer sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public Integer getNoOfParameters() {
		return this.noOfParameters;
	}

	public void setNoOfParameters(Integer noOfParameters) {
		this.noOfParameters = noOfParameters;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(Boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	public SysChatBotValidationTypeTO getSysChatBotValidationTypeTO() {
		return sysChatBotValidationTypeTO;
	}

	public void setSysChatBotValidationTypeTO(
			SysChatBotValidationTypeTO sysChatBotValidationTypeTO) {
		this.sysChatBotValidationTypeTO = sysChatBotValidationTypeTO;
	}

	public List<SysChatBotValidationTypeTO> getSysChatBotValidationTypeTOList() {
		return sysChatBotValidationTypeTOList;
	}

	public void setSysChatBotValidationTypeTOList(
			List<SysChatBotValidationTypeTO> sysChatBotValidationTypeTOList) {
		this.sysChatBotValidationTypeTOList = sysChatBotValidationTypeTOList;
	}

	public List<SysFieldTypeResponseTO> getSysFieldTypeTOList() {
		return sysFieldTypeTOList;
	}

	public void setSysFieldTypeTOList(
			List<SysFieldTypeResponseTO> sysFieldTypeTOList) {
		this.sysFieldTypeTOList = sysFieldTypeTOList;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	
	
}// end of class