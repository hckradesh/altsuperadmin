package com.talentpact.ui.sysChatBotValidationType.to;

import java.io.Serializable;

import com.alt.common.transport.interfs.IResponseTransferObject; 

public class SysChatBotValidationTypeTO implements Serializable { 

 private static final long serialVersionUID = 1L; 

  
	private Integer sysValidationTypeID;
	
	private Integer sysFieldTypeID;
	
	private String fieldType;
	
	private Integer noOfParameters;
	
	private String label;

	public Integer getSysValidationTypeID() {
		return sysValidationTypeID;
	}

	public void setSysValidationTypeID(Integer sysValidationTypeID) {
		this.sysValidationTypeID = sysValidationTypeID;
	}

	public Integer getSysFieldTypeID() {
		return sysFieldTypeID;
	}

	public void setSysFieldTypeID(Integer sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public Integer getNoOfParameters() {
		return noOfParameters;
	}

	public void setNoOfParameters(Integer noOfParameters) {
		this.noOfParameters = noOfParameters;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}


 

}//end of class