package com.talentpact.ui.sysEntityColumn.constants;

public interface ISysEntityColumnConstants {
	public static String SYSENTITYCOLUMN_SAVE_ERROR_MSG = "Error occured while saving SysEntityColumn.";

    public static String SYSENTITYCOLUMN_UPDATE_ERROR_MSG = "Error occured while updating SysEntityColumn.";
    
    public static String SYSENTITYCOLUMN_ADD_SUCCESS_MSG = "SysEntityColumn created successfully.";

    public static String SYSENTITYCOLUMN_UPDATE_SUCCESS_MSG = "SysEntityColumn updated successfully.";
    
    public static String SYSENTITY_ADD_DIALOG_HIDE = "PF('sysEntityAddModal').hide();";
    
    public static String SYSENTITY_EDIT_DIALOG_HIDE = "PF('sysEntityEditModal').hide();";
    
    public static String SYSENTITYCOLUMN_RETRIEVE_ERROR_MSG = "Error occured while retrieving SysEntityColumn on basis of entityColumnID";
    
    public static String SYSENTITYCOLUMN_LIST_RETRIEVE_ERROR_MSG = "Error occured while retrieving SysEntityColumn List on basis of sysEntityID";
    
}
