package com.talentpact.ui.meshRequestBlockingConfig.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.MeshRequestBlockingConfigTO;
import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.meshRequestBlockingConfig.controller.MeshRequestBlockingConfigController;


@Named("meshRequestBlockingConfigBean")
@ConversationScoped
public class MeshRequestBlockingConfigBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    MeshRequestBlockingConfigController meshRequestBlockingConfigController;

    private Integer blockingConfigID;

    private Integer requestTypeId;

    private Integer blockingRequestTypeId;

    private String requestTypeName;

    private String blockingRequestTypeName;

    private List<MeshRequestTypeTO> meshRequestTypes;

    private List<MeshRequestBlockingConfigTO> meshRequestBlockingConfigTOList;

    private Boolean renderUpdateButton;

    private boolean renderErrorMessage;

    private boolean renderMeshRequestBlockingConfigPopup;

    private boolean renderEditMeshRequestBlockingConfigPopup;

    private boolean renderMessageBox;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            meshRequestBlockingConfigController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public Integer getBlockingConfigID() {
        return blockingConfigID;
    }

    public void setBlockingConfigID(Integer blockingConfigID) {
        this.blockingConfigID = blockingConfigID;
    }

    public Integer getRequestTypeId() {
        return requestTypeId;
    }

    public void setRequestTypeId(Integer requestTypeId) {
        this.requestTypeId = requestTypeId;
    }

    public Integer getBlockingRequestTypeId() {
        return blockingRequestTypeId;
    }

    public void setBlockingRequestTypeId(Integer blockingRequestTypeId) {
        this.blockingRequestTypeId = blockingRequestTypeId;
    }

    public String getRequestTypeName() {
        return requestTypeName;
    }

    public void setRequestTypeName(String requestTypeName) {
        this.requestTypeName = requestTypeName;
    }

    public String getBlockingRequestTypeName() {
        return blockingRequestTypeName;
    }

    public void setBlockingRequestTypeName(String blockingRequestTypeName) {
        this.blockingRequestTypeName = blockingRequestTypeName;
    }

    public List<MeshRequestBlockingConfigTO> getMeshRequestBlockingConfigTOList() {
        return meshRequestBlockingConfigTOList;
    }

    public void setMeshRequestBlockingConfigTOList(List<MeshRequestBlockingConfigTO> meshRequestBlockingConfigTOList) {
        this.meshRequestBlockingConfigTOList = meshRequestBlockingConfigTOList;
    }

    public void setRenderErrorMessage(boolean b) {
        // TODO Auto-generated method stub

    }

    public Boolean getRenderUpdateButton() {
        return renderUpdateButton;
    }

    public void setRenderUpdateButton(Boolean renderUpdateButton) {
        this.renderUpdateButton = renderUpdateButton;
    }

    public boolean isRenderMeshRequestBlockingConfigPopup() {
        return renderMeshRequestBlockingConfigPopup;
    }

    public void setRenderMeshRequestBlockingConfigPopup(boolean renderMeshRequestBlockingConfigPopup) {
        this.renderMeshRequestBlockingConfigPopup = renderMeshRequestBlockingConfigPopup;
    }

    public boolean isRenderEditMeshRequestBlockingConfigPopup() {
        return renderEditMeshRequestBlockingConfigPopup;
    }

    public void setRenderEditMeshRequestBlockingConfigPopup(boolean renderEditMeshRequestBlockingConfigPopup) {
        this.renderEditMeshRequestBlockingConfigPopup = renderEditMeshRequestBlockingConfigPopup;
    }

    public boolean isRenderMessageBox() {
        return renderMessageBox;
    }

    public void setRenderMessageBox(boolean renderMessageBox) {
        this.renderMessageBox = renderMessageBox;
    }

    public boolean isRenderErrorMessage() {
        return renderErrorMessage;
    }

	public List<MeshRequestTypeTO> getMeshRequestTypes() {
		return meshRequestTypes;
	}

	public void setMeshRequestTypes(List<MeshRequestTypeTO> meshRequestTypes) {
		this.meshRequestTypes = meshRequestTypes;
	}

}