/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.converter;

import com.talentpact.model.SysPlaceholder;
import com.talentpact.ui.sysPlaceHolder.common.transport.output.SysPlaceHolderResponseTO;

/**
 * @author vaibhav.kashyap
 * 
 */
public class SysPlaceHolderConverter {

	/**
	 * @param placeHolderObj
	 * @return
	 */
	public static SysPlaceHolderResponseTO getSysPlaceHolderResponseFromSysPlaceHolder(
			SysPlaceholder placeHolderObj) throws Exception {
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		try {
			if (placeHolderObj != null) {
				sysPlaceHolderResponseTO = new SysPlaceHolderResponseTO();

				if (placeHolderObj.getSysPlaceholderID() != null
						&& placeHolderObj.getSysPlaceholderID() > 0)
					sysPlaceHolderResponseTO.setSysPlaceHolderID(placeHolderObj
							.getSysPlaceholderID());

				/*
				 * if(placeHolderObj.getEntityColumnID()!=null &&
				 * placeHolderObj.getEntityColumnID()>0)
				 * sysPlaceHolderResponseTO
				 * .setEntityColumnID(placeHolderObj.getEntityColumnID());
				 */
				if (placeHolderObj.getEntityColumn() != null
						&& placeHolderObj.getEntityColumn()
								.getEntityColumnID() > 0)
					sysPlaceHolderResponseTO.setEntityColumnID(placeHolderObj
							.getEntityColumn().getEntityColumnID());

				if (placeHolderObj.getPlaceHolder() != null
						&& !placeHolderObj.getPlaceHolder().equals(""))
					sysPlaceHolderResponseTO.setPlaceHolder(placeHolderObj
							.getPlaceHolder());

				if (placeHolderObj.getPlaceholderDesc() != null
						&& !placeHolderObj.getPlaceholderDesc().equals(""))
					sysPlaceHolderResponseTO.setPlaceholderDesc(placeHolderObj
							.getPlaceholderDesc());

				if (placeHolderObj.getColumnExpression() != null
						&& !placeHolderObj.getColumnExpression().equals(""))
					sysPlaceHolderResponseTO.setColumnExpression(placeHolderObj
							.getColumnExpression());

				if (placeHolderObj.getWhereCondition() != null
						&& !placeHolderObj.getWhereCondition().equals(""))
					sysPlaceHolderResponseTO.setWhereCondition(placeHolderObj
							.getWhereCondition());

				if (placeHolderObj.getEntityPath() != null
						&& !placeHolderObj.getEntityPath().equals(""))
					sysPlaceHolderResponseTO.setEntityPath(placeHolderObj
							.getEntityPath());

				if (placeHolderObj.getTenantID() != null
						&& placeHolderObj.getTenantID() > 0)
					sysPlaceHolderResponseTO.setTenantID(placeHolderObj
							.getTenantID());

				if (placeHolderObj.getModifiedBy() != null
						&& placeHolderObj.getModifiedBy() > 0)
					sysPlaceHolderResponseTO.setModifiedBy(placeHolderObj
							.getModifiedBy());

				if (placeHolderObj.getCreatedBy() != null
						&& placeHolderObj.getCreatedBy() > 0)
					sysPlaceHolderResponseTO.setCreatedBy(placeHolderObj
							.getCreatedBy());

				if (placeHolderObj.getPlaceholderTypeID() != null
						&& placeHolderObj.getPlaceholderTypeID() > 0)
					sysPlaceHolderResponseTO
							.setPlaceholderTypeID(placeHolderObj
									.getPlaceholderTypeID());

				if (placeHolderObj.getLevelTypeID() != null
						&& placeHolderObj.getLevelTypeID() > 0)
					sysPlaceHolderResponseTO.setLevelType(placeHolderObj
							.getLevelTypeID());

				if (placeHolderObj.getComplexityTypeID() != null
						&& placeHolderObj.getComplexityTypeID() > 0)
					sysPlaceHolderResponseTO.setComplexityType(placeHolderObj
							.getComplexityTypeID());

				if (placeHolderObj.getSysRule() != null) {
					sysPlaceHolderResponseTO.setRuleID(placeHolderObj
							.getSysRule().getRuleID());
					sysPlaceHolderResponseTO.setRuleName(placeHolderObj
							.getSysRule().getRuleMethod());
				}

				/*
				 * if(placeHolderObj.getSysCommunicationtype()!=null){
				 * sysPlaceHolderResponseTO
				 * .setCommTypeID(placeHolderObj.getSysCommunicationtype
				 * ().getCommTypeId());
				 * sysPlaceHolderResponseTO.setCommTypeName(
				 * placeHolderObj.getSysCommunicationtype().getCommType()); }
				 */
				if (placeHolderObj.getModuleID()!=null && placeHolderObj.getModuleID() > 0) {
					sysPlaceHolderResponseTO.setModuleID(placeHolderObj
							.getModuleID());
				}

				if (placeHolderObj.getModifiedDate() != null)
					sysPlaceHolderResponseTO.setModifiedDate(placeHolderObj
							.getModifiedDate());

				if (placeHolderObj.getCreatedDate() != null)
					sysPlaceHolderResponseTO.setCreatedDate(placeHolderObj
							.getCreatedDate());
			}
		} catch (Exception ex) {
			sysPlaceHolderResponseTO = null;
			throw ex;
		} finally {

		}
		return sysPlaceHolderResponseTO;
	}

}
