/**
 * 
 */
package com.talentpact.ui.rule.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.alt.contentType.transport.input.ContentTypeTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.dataservice.SysContentType.SysContentTypeService;
import com.talentpact.business.dataservice.sysRule.SysRuleService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.rule.ISysRuleConstants;
import com.talentpact.ui.rule.SysRuleListHelper;
import com.talentpact.ui.rule.bean.SysRuleBean;
import com.talentpact.ui.rule.to.RuleListRequestTO;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 * 
 */
@Named("sysRuleController")
@SessionScoped
public class SysRuleController extends CommonController implements IDefaultAction, Serializable
{

    private static final long serialVersionUID = 631095883134130990L;

    @Inject
    MenuBean menuBean;

    @Inject
    SysRuleBean sysRuleBean;

    @Inject
    SysRuleService sysRuleService;
    
    @Inject
    SysContentTypeService sysContentTypeService;

    @Override
    public void initialize()
    {
        List<RuleResponseTO> ruleTOList1 = null;
        List<RuleResponseTO> ruleTypeIDTOList = null;
        try {
            sysRuleBean.setRenderUpdateButton(false);
            sysRuleBean.setRenderAddNewRuleDialog(false);
            sysRuleBean.setRenderSaveButton(false);
            sysRuleBean.setRenderMandatoryRuleClass(false);
            ruleTOList1 = getSysRuleList();
            ruleTypeIDTOList = getRuleTypeIDList();
            sysRuleBean.setRuleTOList(ruleTOList1);
            sysRuleBean.setRuleTypeIDTOList(ruleTypeIDTOList);
            menuBean.setPage("../rule/rulelist.xhtml");
        } catch (Exception e) {
        } finally {
        	 RequestContext.getCurrentInstance().execute( "PF('myDataTable').clearFilters();");
             RequestContext.getCurrentInstance().execute( "PF('myDataTable').getPaginator().setPage(0);");

        }

    }

    private List<RuleResponseTO> getSysRuleList()
    {
        List<RuleResponseTO> serviceResponse = null;
        try {
            serviceResponse = sysRuleService.getSysRuleListByTenant();
        } catch (Exception e) {
        }
        return serviceResponse;

    }

    private List<RuleResponseTO> getRuleTypeIDList()
    {
        List<RuleResponseTO> serviceResponse = null;
        try {
            serviceResponse = sysRuleService.getRuleTypeIDList();
        } catch (Exception e) {
        }
        return serviceResponse;

    }

    public void refreshPopUp(RuleResponseTO ruleTO)
    {
        try {
            sysRuleBean.setRuleTypeID(ruleTO.getRuleTypeID());
            sysRuleBean.setRuleMethod(ruleTO.getRuleMethod());
            sysRuleBean.setClassPathConvention(ruleTO.getClassPathConvention());
            sysRuleBean.setDescription(ruleTO.getDescription());
            sysRuleBean.setMethodParameters(ruleTO.getMethodParameters());
            sysRuleBean.setMethodReturnType(ruleTO.getMethodReturnType());
            sysRuleBean.setRuleClass(ruleTO.getRuleClass());
            sysRuleBean.setRuleClassName(ruleTO.getRuleClassName());
            sysRuleBean.setRuleID(ruleTO.getRuleID());
            sysRuleBean.setRenderUpdateButton(true);
            changeRuleType(ruleTO.getRuleTypeID());
        } catch (Exception e) {
        }
    }


    public void ResetBean()
    {
        try {

            sysRuleBean.setRuleMethod(null);
            sysRuleBean.setClassPathConvention(null);
            sysRuleBean.setDescription(null);
            sysRuleBean.setMethodParameters(null);
            sysRuleBean.setMethodReturnType(null);
            sysRuleBean.setRuleClass(null);
            sysRuleBean.setRuleClassName(null);
            sysRuleBean.setRuleTypeID(0);
            sysRuleBean.setRenderAddNewRuleDialog(true);
            sysRuleBean.setRenderSaveButton(true);
            sysRuleBean.setRenderMandatoryRuleClass(false);

        } catch (Exception e) {

        }

    }

    public void update()
    {
        RuleListRequestTO rule = null;
        String ruleMethod = null;
        Boolean isRuleMethodExist = null;
        RequestContext context = null;

        try {
            context = RequestContext.getCurrentInstance();
            Integer ruleID = sysRuleBean.getRuleID();
            boolean isRuleIDExist = SysRuleListHelper.checkRuleIDinList(ruleID, sysRuleBean.getRuleTOList());
            ruleMethod = sysRuleBean.getRuleMethod().trim();
            if (ruleMethod != null && !ruleMethod.equals("")) {
                isRuleMethodExist = SysRuleListHelper.checkRuleMethodinList(ruleMethod.trim(), ruleID, sysRuleBean.getRuleTOList());
            } else {
                FacesContext.getCurrentInstance().addMessage("ruleListUpdateForm:changeRuleDetails",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered Rule Method is Invalid ", ""));
                return;
            }

            if (isRuleMethodExist != null) {
                if (isRuleIDExist == true) {

                    rule = new RuleListRequestTO();
                    if (isRuleMethodExist == false) {
                        rule.setRuleMethod(ruleMethod);

                    } else {
                        FacesContext.getCurrentInstance().addMessage("changeRuleDetails",
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Rule Method with name '" + sysRuleBean.getRuleMethod() + "' already exist", ""));
                        return;
                    }

                    rule.setRuleID(sysRuleBean.getRuleID());
                    rule.setClassPathConvention(sysRuleBean.getClassPathConvention());
                    rule.setDescription(sysRuleBean.getDescription());
                    rule.setMethodParameters(sysRuleBean.getMethodParameters());
                    rule.setMethodReturnType(sysRuleBean.getMethodReturnType());
                    rule.setRuleClass(sysRuleBean.getRuleClass());
                    rule.setRuleClassName(sysRuleBean.getRuleClassName());
                    rule.setRuleTypeID(sysRuleBean.getRuleTypeID());
                    sysRuleService.setSysRuleListByTenant(rule);
                    context.execute("PF('myDataTable').clearFilters();");
                    initialize();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysRuleConstants.SysRule_UPDATE_SUCCESS_MSG, ""));
                }
            }


            context.execute(ISysRuleConstants.SysRule_UPDATE_DIALOG_HIDE);
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysRuleConstants.SysRule_UPDATE_ERROR_MSG, ""));
            context.execute(ISysRuleConstants.SysRule_UPDATE_DIALOG_HIDE);

        }
    }

    public void save()

    {
        RuleListRequestTO rule = null;
        String ruleMethod = null;
        Boolean isRuleMethodExist = null;
        RequestContext context = null;
        try {
            context = RequestContext.getCurrentInstance();
            // checking whether a rule Method of this name exist or not
            ruleMethod = sysRuleBean.getRuleMethod().trim();

            if (ruleMethod != null && !ruleMethod.equals("")) {
                isRuleMethodExist = SysRuleListHelper.checkExistingRuleMethodinList(ruleMethod, sysRuleBean.getRuleTOList());
            } else {
                FacesContext.getCurrentInstance().addMessage("newRuleListForm:newRule1", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entered Rule Method is Invalid ", ""));
                return;
            }

            // end here

            if (isRuleMethodExist != null) {
                if (isRuleMethodExist == false) {
                    rule = new RuleListRequestTO();

                    rule.setRuleMethod(sysRuleBean.getRuleMethod());
                    rule.setClassPathConvention(sysRuleBean.getClassPathConvention());
                    rule.setDescription(sysRuleBean.getDescription());
                    rule.setMethodParameters(sysRuleBean.getMethodParameters());
                    rule.setMethodReturnType(sysRuleBean.getMethodReturnType());
                    rule.setRuleClass(sysRuleBean.getRuleClass());
                    rule.setRuleClassName(sysRuleBean.getRuleClassName());
                    rule.setRuleTypeID(sysRuleBean.getRuleTypeID());
                    sysRuleService.setSysRuleListByTenant(rule);
                    initialize();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysRuleConstants.SysRule_ADD_SUCCESS_MSG, ""));
                } else {
                    FacesContext.getCurrentInstance().addMessage("newRuleListForm:newRule1",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Rule Method with name '" + sysRuleBean.getRuleMethod() + "' already exists", ""));
                    return;
                }
            }
            context.execute(ISysRuleConstants.SysRule_SAVE_DIALOG_HIDE);
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysRuleConstants.SysRule_ADD_ERROR_MSG, ""));
            context.execute(ISysRuleConstants.SysRule_SAVE_DIALOG_HIDE);
        }
    }

    public void changeRuleType(Integer ruleTypeID) throws Exception {
    	sysRuleBean.setRuleTypeID(ruleTypeID);
    	ContentTypeTO contentTypeTO = null;
    	SysContentTypeTO sysContentTypeTO = null;
    	sysContentTypeTO = new SysContentTypeTO();
    	sysContentTypeTO.setSysTypeID(ruleTypeID);
    	contentTypeTO = sysContentTypeService.findSysContentType(sysContentTypeTO);
    	if(contentTypeTO != null && contentTypeTO.getSysType().equalsIgnoreCase(ISysRuleConstants.SysRule_BOT_RULE))
    		sysRuleBean.setRenderMandatoryRuleClass(true);
    	else
    		sysRuleBean.setRenderMandatoryRuleClass(false);
     }
}
