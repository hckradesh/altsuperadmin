package com.talentpact.ui.DSOTemplateConfigList.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.DSOTemplateConfigList.Controller.DSOTemplateConfigListController;
import com.talentpact.ui.DSPTemplateConfig.to.DSOTemplateConfigTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;


@SuppressWarnings("serial")
@Named("dsoTemplateConfigListBean")
@ConversationScoped
public class DSOTemplateConfigListBean extends CommonBean implements Serializable, IDefaultAction  {
	
@Inject 
DSOTemplateConfigListController dsoTemplateConfigListController;
	
	private List<DSOTemplateConfigTO> dsoTemplateConfigTOList;
	private List<SelectItem> hrOrgUnits;
	private int orgId;
	private int templateId;
	private String templateName;
	private int moduleID=0;
	private String msg;
	private String type;
	private boolean renderMessage;
	
	private String description;
	private String cc;
	private String bcc;
	private String to;
	private String content;
	private String subject;
	
	private boolean recipientTo;
	private boolean recipientCC;
	private boolean recipientBcc;
	private boolean renderupdateBtn;
	private boolean editDisable;
	
	private List<DSOPlaceHolderTO> placeHolderListTo;
	List<DSOPlaceHolderTO> phList;
	
    public int getModuleID() {
		return moduleID;
	}

	public void setModuleID(int moduleID) {
		this.moduleID = moduleID;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public List<SelectItem> getHrOrgUnits() {
		return hrOrgUnits;
	}

	public void setHrOrgUnits(List<SelectItem> hrOrgUnits) {
		this.hrOrgUnits = hrOrgUnits;
	}

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public List<DSOTemplateConfigTO> getDsoTemplateConfigTOList() {
		return dsoTemplateConfigTOList;
	}

	public void setDsoTemplateConfigTOList(
			List<DSOTemplateConfigTO> dsoTemplateConfigTOList) {
		this.dsoTemplateConfigTOList = dsoTemplateConfigTOList;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isRenderMessage() {
		return renderMessage;
	}

	public void setRenderMessage(boolean renderMessage) {
		this.renderMessage = renderMessage;
	}

	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<DSOPlaceHolderTO> getPlaceHolderListTo() {
		return placeHolderListTo;
	}

	public void setPlaceHolderListTo(List<DSOPlaceHolderTO> placeHolderListTo) {
		this.placeHolderListTo = placeHolderListTo;
	}

	public boolean isRecipientTo() {
		return recipientTo;
	}

	public void setRecipientTo(boolean recipientTo) {
		this.recipientTo = recipientTo;
	}

	public boolean isRecipientCC() {
		return recipientCC;
	}

	public void setRecipientCC(boolean recipientCC) {
		this.recipientCC = recipientCC;
	}

	public boolean isRecipientBcc() {
		return recipientBcc;
	}

	public void setRecipientBcc(boolean recipientBcc) {
		this.recipientBcc = recipientBcc;
	}
	
	public List<DSOPlaceHolderTO> getPhList() {
		return phList;
	}

	public void setPhList(List<DSOPlaceHolderTO> phList) {
		this.phList = phList;
	}

	public boolean isRenderupdateBtn() {
		return renderupdateBtn;
	}

	public void setRenderupdateBtn(boolean renderupdateBtn) {
		this.renderupdateBtn = renderupdateBtn;
	}

	public boolean isEditDisable() {
		return editDisable;
	}

	public void setEditDisable(boolean editDisable) {
		this.editDisable = editDisable;
	}

	@Override
	public void initialize(){
		endConversation();
		beginConversation();
		dsoTemplateConfigListController.initialize();
    }
}
