/**
 * 
 */
package com.talentpact.ui.tenants.newtenant.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.tenants.newtenant.controller.NewTenantController;

/**
 * @author radhamadhab.dalai
 *
 */
@Named("newTenantBean")
@SessionScoped
public class NewTenantBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    NewTenantController newTenantController;

    private String tenantName;

    private String tenantCode;

    private Integer statusID;

    private Date CreatedDate;

    private Date modifiedDate;

    private Date effectiveFrom;

    private Date effectiveTo;

    private Long createdBy;

    private Long ModifiedBy;

    private Date contractSignedDate;

    private Long tenantID;

    private Boolean updateFlag;

    private Boolean saveFlag;


    /**
     * @return the modifiedDate
     */
    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    /**
     * @param modifiedDate the modifiedDate to set
     */
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    /**
     * @return the createdBy
     */
    public Long getCreatedBy()
    {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(Long createdBy)
    {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedBy
     */
    public Long getModifiedBy()
    {
        return ModifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(Long modifiedBy)
    {
        ModifiedBy = modifiedBy;
    }

    /**
     * @return the contractSignedDate
     */
    public Date getContractSignedDate()
    {
        return contractSignedDate;
    }

    /**
     * @param contractSignedDate the contractSignedDate to set
     */
    public void setContractSignedDate(Date contractSignedDate)
    {
        this.contractSignedDate = contractSignedDate;
    }

    private List<NewTenantResponseTO> tenantResToList;

    private List<NewTenantResponseTO> filteredTenantList;


    private List<OrganizationrequestTO> orgList;

    /**
     * @return the orgList
     */
    public List<OrganizationrequestTO> getOrgList()
    {
        return orgList;
    }

    /**
     * @param orgList the orgList to set
     */
    public void setOrgList(List<OrganizationrequestTO> orgList)
    {
        this.orgList = orgList;
    }

    /**
     * @return the filteredOrgList
     */
    public List<OrganizationrequestTO> getFilteredOrgList()
    {
        return filteredOrgList;
    }

    /**
     * @param filteredOrgList the filteredOrgList to set
     */
    public void setFilteredOrgList(List<OrganizationrequestTO> filteredOrgList)
    {
        this.filteredOrgList = filteredOrgList;
    }

    private List<OrganizationrequestTO> filteredOrgList;


    /**
     * @return the filteredTenantList
     */
    public List<NewTenantResponseTO> getFilteredTenantList()
    {
        return filteredTenantList;
    }

    /**
     * @param filteredTenantList the filteredTenantList to set
     */
    public void setFilteredTenantList(List<NewTenantResponseTO> filteredTenantList)
    {
        this.filteredTenantList = filteredTenantList;
    }

    /**
     * @return the tenantResToList
     */
    public List<NewTenantResponseTO> getTenantResToList()
    {
        return tenantResToList;
    }

    /**
     * @param tenantResToList the tenantResToList to set
     */
    public void setTenantResToList(List<NewTenantResponseTO> tenantResToList)
    {
        this.tenantResToList = tenantResToList;
    }

    /**
     * @return the tenantName
     */
    public String getTenantName()
    {
        return tenantName;
    }

    /**
     * @param tenantName the tenantName to set
     */
    public void setTenantName(String tenantName)
    {
        this.tenantName = tenantName;
    }

    /**
     * @return the tenantCode
     */
    public String getTenantCode()
    {
        return tenantCode;
    }

    /**
     * @param tenantCode the tenantCode to set
     */
    public void setTenantCode(String tenantCode)
    {
        this.tenantCode = tenantCode;
    }

    /**
     * @return the statusID
     */
    public Integer getStatusID()
    {
        return statusID;
    }

    /**
     * @param statusID the statusID to set
     */
    public void setStatusID(Integer statusID)
    {
        this.statusID = statusID;
    }

    public void initialize()
    {
    	endConversation();
        beginConversation();
        newTenantController.initialize();

    }

    public Date getCreatedDate()
    {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        CreatedDate = createdDate;
    }

    public Date getEffectiveFrom()
    {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom)
    {
        this.effectiveFrom = effectiveFrom;
    }

    public Date getEffectiveTo()
    {
        return effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo)
    {
        this.effectiveTo = effectiveTo;
    }


    public void clearData()
    {
        this.tenantCode = null;
        this.statusID = null;
        this.CreatedDate = null;
        this.modifiedDate = null;
        this.effectiveFrom = null;
        this.effectiveTo = null;
        this.contractSignedDate = null;
        this.tenantName = null;
        this.tenantID = null;
    }

    public Long getTenantID()
    {
        return tenantID;
    }

    public void setTenantID(Long tenantID)
    {
        this.tenantID = tenantID;
    }

    public Boolean getUpdateFlag()
    {
        return updateFlag;
    }

    public Boolean getSaveFlag()
    {
        return saveFlag;
    }

    public void setUpdateFlag(Boolean updateFlag)
    {
        this.updateFlag = updateFlag;
    }

    public void setSaveFlag(Boolean saveFlag)
    {
        this.saveFlag = saveFlag;
    }


}
