package com.talentpact.ui.sysDuplicateParameter.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysDuplicateParameterConstants;
import com.talentpact.business.dataservice.sysDuplicateParameter.SysDuplicateParameterService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysDuplicateParameter.bean.SysDuplicateParameterBean;
import com.talentpact.ui.sysDuplicateParameter.to.SysDuplicateParameterTO;


/**
 * 
 * @author prachi.bansal
 *
 */

@Named("sysDuplicateParameterController")
@ConversationScoped
public class SysDuplicateParameterController extends CommonController implements Serializable, IDefaultAction {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysDuplicateParameterController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    SysDuplicateParameterBean sysDuplicateParameterBean;

    @Inject
    SysDuplicateParameterService sysDuplicateParameterService;


    @Override
    public void initialize() {
        ServiceResponse response = null;
        try {
            sysDuplicateParameterBean.setSysDuplicateParameterTOList(sysDuplicateParameterService.getSysDuplicateParameterList());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../sysDuplicateParameter/sysDuplicateParameter.xhtml");
            requestContext.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_DATATABLE_RESET);
            requestContext.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_PAGINATION_DATATABLE_RESET);

        } catch (Exception e) {
            LOGGER_.error("", e);
        }

    }

    public void resetSysDuplicateParameter() {
        sysDuplicateParameterBean.setSysDuplicateParameterID(0);
        sysDuplicateParameterBean.setSysDuplicateParameterName(null);
        sysDuplicateParameterBean.setSysDuplicateParameterTOList(null);
    }

    public void saveSysDuplicateParameter()
        throws Exception {
        SysDuplicateParameterTO sysDuplicateParameterTO = null;
        RequestContext request = null;
        try {
            request = RequestContext.getCurrentInstance();
            sysDuplicateParameterTO = new SysDuplicateParameterTO();
            sysDuplicateParameterTO.setSysDuplicateParameterName(sysDuplicateParameterBean.getSysDuplicateParameterName().trim());
            if (validateDuplicateParameterName(sysDuplicateParameterTO, false)) {
                sysDuplicateParameterService.saveSysDuplicateParameter(sysDuplicateParameterTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysDuplicateParameter added successfully", ""));
                request.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_ADD_DIALOG_HIDE);
                request.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_DATATABLE_RESET);
                request.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_PAGINATION_DATATABLE_RESET);
                request.update("sysDuplicateParameterListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_DUPLICATE_ERROR_MSG, ""));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }

    }

    private boolean validateDuplicateParameterName(SysDuplicateParameterTO sysDuplicateParameterTO, boolean edited)
        throws Exception {
        List<SysDuplicateParameterTO> sysDuplicateParameterTOList = null;
        try {
            if (edited) {
                sysDuplicateParameterTOList = sysDuplicateParameterService.getUpdateSysDuplicateParameters(sysDuplicateParameterTO.getSysDuplicateParameterID());
            } else {
                sysDuplicateParameterTOList = sysDuplicateParameterService.getSysDuplicateParameterList();
            }
            if (sysDuplicateParameterTOList != null && !sysDuplicateParameterTOList.isEmpty()) {
                for (SysDuplicateParameterTO sysDP : sysDuplicateParameterTOList) {
                    if (sysDP.getSysDuplicateParameterName().trim().equalsIgnoreCase(sysDuplicateParameterTO.getSysDuplicateParameterName())) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            throw new Exception(ex);
        }
    }

    public void editSysDuplicateParameter(SysDuplicateParameterTO sysDuplicateParameterTO) {
        try {
            sysDuplicateParameterBean.setSysDuplicateParameterTO(sysDuplicateParameterTO);
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }

    public void updateSysDuplicateParameter()
        throws Exception {
        SysDuplicateParameterTO sysDuplicateParameterTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysDuplicateParameterTO = new SysDuplicateParameterTO();
            sysDuplicateParameterTO.setSysDuplicateParameterID(sysDuplicateParameterBean.getSysDuplicateParameterTO().getSysDuplicateParameterID());
            sysDuplicateParameterTO.setSysDuplicateParameterName(sysDuplicateParameterBean.getSysDuplicateParameterTO().getSysDuplicateParameterName().trim());
            if (validateDuplicateParameterName(sysDuplicateParameterTO, true)) {
                sysDuplicateParameterService.updateSysDuplicateParameter(sysDuplicateParameterTO);
                initialize();
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_UPDATE_SUCCESS_MSG, ""));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysDuplicateParameter updated successfully", ""));
                requestContext.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_DATATABLE_RESET);
                requestContext.execute(ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysDuplicateParameterListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_DUPLICATE_ERROR_MSG, ""));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDuplicateParameterConstants.SYSDUPLICATEPARAMETER_UPDATE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }
    }

}
