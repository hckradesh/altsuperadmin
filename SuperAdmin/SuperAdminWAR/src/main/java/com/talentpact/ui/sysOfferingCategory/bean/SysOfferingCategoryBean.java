package com.talentpact.ui.sysOfferingCategory.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysOfferingCategory.controller.SysOfferingCategoryController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@SuppressWarnings("serial")
@Named("sysOfferingCategoryBean")
@ConversationScoped
public class SysOfferingCategoryBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysOfferingCategoryController sysOfferingCategoryController;

    private Long offeringCategoryID;

    private String offeringCategory;

    private String description;

    private boolean renderSysOfferingCategoryPopup;

    private boolean renderEditSysOfferingCategoryPopup;

    private boolean renderMessageBox;

    private Boolean renderUpdateButton;

    private String duplicateCategoryName;

    private String duplicateDescription;

    private boolean renderErrorMessage;

    private Set<String> sysOfferingCategory;

    private Set<String> selectedOfferingCategory;

    private List<SysOfferingCategoryTO> sysOfferingCategoryTOList;

    @Override
    public void initialize()
    {
        try {
            sysOfferingCategoryController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    public Long getOfferingCategoryID()
    {
        return offeringCategoryID;
    }


    public void setOfferingCategoryID(Long offeringCategoryID)
    {
        this.offeringCategoryID = offeringCategoryID;
    }


    public String getOfferingCategory()
    {
        return offeringCategory;
    }


    public void setOfferingCategory(String offeringCategory)
    {
        this.offeringCategory = offeringCategory;
    }


    public String getDescription()
    {
        return description;
    }


    public void setDescription(String description)
    {
        this.description = description;
    }


    public boolean isRenderSysOfferingCategoryPopup()
    {
        return renderSysOfferingCategoryPopup;
    }


    public void setRenderSysOfferingCategoryPopup(boolean renderSysOfferingCategoryPopup)
    {
        this.renderSysOfferingCategoryPopup = renderSysOfferingCategoryPopup;
    }


    public boolean isRenderEditSysOfferingCategoryPopup()
    {
        return renderEditSysOfferingCategoryPopup;
    }


    public void setRenderEditSysOfferingCategoryPopup(boolean renderEditSysOfferingCategoryPopup)
    {
        this.renderEditSysOfferingCategoryPopup = renderEditSysOfferingCategoryPopup;
    }


    public boolean isRenderMessageBox()
    {
        return renderMessageBox;
    }


    public void setRenderMessageBox(boolean renderMessageBox)
    {
        this.renderMessageBox = renderMessageBox;
    }


    public Boolean getRenderUpdateButton()
    {
        return renderUpdateButton;
    }


    public void setRenderUpdateButton(Boolean renderUpdateButton)
    {
        this.renderUpdateButton = renderUpdateButton;
    }


    public String getDuplicateCategoryName()
    {
        return duplicateCategoryName;
    }


    public void setDuplicateCategoryName(String duplicateCategoryName)
    {
        this.duplicateCategoryName = duplicateCategoryName;
    }


    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }


    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }


    public List<SysOfferingCategoryTO> getSysOfferingCategoryTOList()
    {
        return sysOfferingCategoryTOList;
    }

    public void setSysOfferingCategoryTOList(List<SysOfferingCategoryTO> sysOfferingCategoryTOList)
    {
        this.sysOfferingCategoryTOList = sysOfferingCategoryTOList;
    }

    public Set<String> getSysOfferingCategory()
    {
        return sysOfferingCategory;
    }

    public void setSysOfferingCategory(Set<String> sysOfferingCategory)
    {
        this.sysOfferingCategory = sysOfferingCategory;
    }

    public Set<String> getSelectedOfferingCategory()
    {
        return selectedOfferingCategory;
    }

    public void setSelectedOfferingCategory(Set<String> selectedOfferingCategory)
    {
        this.selectedOfferingCategory = selectedOfferingCategory;
    }

    public String getDuplicateDescription()
    {
        return duplicateDescription;
    }

    public void setDuplicateDescription(String duplicateDescription)
    {
        this.duplicateDescription = duplicateDescription;
    }

}