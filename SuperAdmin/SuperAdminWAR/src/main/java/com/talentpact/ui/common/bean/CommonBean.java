/**
 * 
 */
package com.talentpact.ui.common.bean;

import javax.enterprise.context.Conversation;
import javax.inject.Inject;

import org.apache.log4j.Logger;

/**
 * @author radhamadhab.dalai
 *
 */
public class CommonBean
{
    @Inject
    private Conversation conversation;
    
    private static final Logger LOGGER_ = Logger.getLogger(CommonBean.class.toString());

    public void beginConversation() {
		LOGGER_.warn("-------------- conversation id is : ---------- " + conversation.getId());
		if (conversation.isTransient()) {
			conversation.begin();
			conversation.setTimeout(3600000);
			LOGGER_.warn("-------------- conversation transiet is : ---------- " + conversation.getId());
		}
		LOGGER_.warn("-------------- conversation -- id is : ---------- " + conversation.getId());
	}

	public void endConversation() {
		if (!conversation.isTransient()) {
			conversation.end();
		}
	}

}
