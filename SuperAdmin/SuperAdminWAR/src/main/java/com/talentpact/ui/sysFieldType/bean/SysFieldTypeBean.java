package com.talentpact.ui.sysFieldType.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysFieldType.controller.SysFieldTypeController;
import com.talentpact.ui.sysFieldType.to.SysFieldTypeTO;

@SuppressWarnings("serial")
@Named("sysFieldTypeBean")
@ConversationScoped
public class SysFieldTypeBean extends CommonBean implements Serializable, IDefaultAction {

	@Inject
	SysFieldTypeController sysFieldTypeController;
	
	private Integer sysFieldTypeID;
    
	private String fieldType;
	
	private SysFieldTypeTO sysFieldTypeTO;
    
    private List<SysFieldTypeTO> sysFieldTypeTOList;
    
    private boolean renderErrorMessage;
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		try {
        	endConversation();
            beginConversation();
            sysFieldTypeController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public Integer getSysFieldTypeID() {
		return sysFieldTypeID;
	}

	public void setSysFieldTypeID(Integer sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public SysFieldTypeTO getSysFieldTypeTO() {
		return sysFieldTypeTO;
	}

	public void setSysFieldTypeTO(SysFieldTypeTO sysFieldTypeTO) {
		this.sysFieldTypeTO = sysFieldTypeTO;
	}

	public List<SysFieldTypeTO> getSysFieldTypeTOList() {
		return sysFieldTypeTOList;
	}

	public void setSysFieldTypeTOList(List<SysFieldTypeTO> sysFieldTypeTOList) {
		this.sysFieldTypeTOList = sysFieldTypeTOList;
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	
	
}
