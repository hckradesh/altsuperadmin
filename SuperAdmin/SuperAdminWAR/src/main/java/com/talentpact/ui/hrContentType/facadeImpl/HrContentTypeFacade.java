/**
 * 
 */
package com.talentpact.ui.hrContentType.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.hrContentType.dataprovider.HrContentTypeDataProvider;
import com.talentpact.ui.hrContentType.facaderemote.HrContentTypeLocalFacade;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class HrContentTypeFacade implements HrContentTypeLocalFacade{

	@EJB
	private HrContentTypeDataProvider hrContentTypeDataProvider;
	
	/* (non-Javadoc)
	 * @see com.talentpact.ui.hrContentType.facaderemote.HrContentTypeLocalFacade#getHrContentTypeByContentCategory(com.talentpact.business.common.service.input.ServiceRequest)
	 */
	@Override
	public ServiceResponse getHrContentTypeByContentCategory(
			ServiceRequest serviceRequest) throws Exception {
		return hrContentTypeDataProvider.getHrContentTypeByContentCategory();
	}

}
