/**
 * 
 */
package com.talentpact.ui.sysCongratulationAnnouncement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import com.talentpact.business.application.transport.output.SysCongratulationAnnouncementResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.dataservice.sysCongratulationAnnouncement.SysCongratulationAnnouncementService;
import com.talentpact.ui.common.constants.Iconstants;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysCongratulationAnnouncementController")
@ConversationScoped
public class SysCongratulationAnnouncementController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    SysCongratulationAnnouncementService sysCongratulationAnnouncementService;

    @Inject
    SysCongratulationAnnouncementBean sysCongratulationAnnouncementBean;

    @Override
    public void initialize()
    {
        List<SysCongratulationAnnouncementResponseTO> sysCongratulationAnnouncementTOList = null;

        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {
            getSysContentTypeDropDown();
            sysCongratulationAnnouncementTOList = new ArrayList<SysCongratulationAnnouncementResponseTO>();
            sysCongratulationAnnouncementTOList = getSysCongratulationAnnouncementTOList();
            sysCongratulationAnnouncementBean.setSysCongratulationAnnouncementResponseTOList(sysCongratulationAnnouncementTOList);
            menuBean.setPage("../sysCongratulationAnnouncement/sysCongratulationAnnouncement.xhtml");

            requestContext.execute("PF('myDataTable').clearFilters();");
            requestContext.execute("PF('myDataTable').getPaginator().setPage(0);");
        } catch (Exception e) {
        } finally {

        }

    }

    public void refreshPopUp(SysCongratulationAnnouncementResponseTO to)
    {
        try {
            sysCongratulationAnnouncementBean.setSysCongratulationAnnouncementID(to.getSysCongratulationAnnouncementID());
            sysCongratulationAnnouncementBean.setSysAnnouncementType(to.getSysAnnouncementType());
            sysCongratulationAnnouncementBean.setSysCongratulationAnnouncement(to.getSysCongratulationAnnouncement());
            sysCongratulationAnnouncementBean.setSysCongratulationUploadPath(to.getSysCongratulationUploadPath());

        } catch (Exception e) {
        }
    }

    public void resetBean()
    {
        try {
            sysCongratulationAnnouncementBean.setSysAnnouncementType(null);
            sysCongratulationAnnouncementBean.setSysCongratulationAnnouncement(null);
            sysCongratulationAnnouncementBean.setSysCongratulationUploadPath(null);
        } catch (Exception e) {

        }

    }

    private List<SysCongratulationAnnouncementResponseTO> getSysCongratulationAnnouncementTOList()
    {
        List<SysCongratulationAnnouncementResponseTO> response = null;
        try {
            response = sysCongratulationAnnouncementService.getSysCongratulationAnnouncementTOList();
        } catch (Exception e) {
        }
        return response;

    }

    public void save()
    {

        SysCongratulationAnnouncementResponseTO to = null;
        RequestContext context = null;
        Boolean isDuplicate = false;
        try {
            context = RequestContext.getCurrentInstance();


            to = new SysCongratulationAnnouncementResponseTO();
            to.setSysCongratulationAnnouncement(sysCongratulationAnnouncementBean.getSysCongratulationAnnouncement());
            to.setSysAnnouncementType(sysCongratulationAnnouncementBean.getSysAnnouncementType());
            to.setSysCongratulationUploadPath(sysCongratulationAnnouncementBean.getSysCongratulationUploadPath());
            isDuplicate = checkDublicateForSave(to);
            if (isDuplicate) {
                FacesContext.getCurrentInstance().addMessage("newSysCongratulationAnnouncementForm",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, to.getSysAnnouncementType() + " already exists", ""));
                return;
            } else {
                sysCongratulationAnnouncementService.save(to);

                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved Successfully", ""));


                context.execute("PF('sysCongratulationAnnouncementAddModal').hide();");

                context.update("sysCongratulationAnnouncementListForm");
            }

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error occured", ""));
            context.execute("PF('sysCongratulationAnnouncementAddModal').hide();");
        } finally {
            to = null;
        }

    }

    public void update()
    {
        SysCongratulationAnnouncementResponseTO to = null;
        RequestContext context = null;
        Boolean isDuplicate = false;
        try {
            context = RequestContext.getCurrentInstance();
            to = new SysCongratulationAnnouncementResponseTO();

            to.setSysCongratulationAnnouncement(sysCongratulationAnnouncementBean.getSysCongratulationAnnouncement());
            to.setSysAnnouncementType(sysCongratulationAnnouncementBean.getSysAnnouncementType());
            to.setSysCongratulationUploadPath(sysCongratulationAnnouncementBean.getSysCongratulationUploadPath());
            to.setSysCongratulationAnnouncementID(sysCongratulationAnnouncementBean.getSysCongratulationAnnouncementID());
            isDuplicate = checkDublicateForUpdate(to);
            if (isDuplicate) {
                FacesContext.getCurrentInstance().addMessage("sysCongratulationAnnouncementUpdateForm",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, to.getSysAnnouncementType() + " already exists", ""));
                return;

            } else {

                sysCongratulationAnnouncementService.save(to);
                context.execute("PF('myDataTable').clearFilters();");
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Announcement Updated Succesfully", ""));
                context.execute("PF('sysCongratulationAnnouncementEditModal').hide();");
                context.update("sysCongratulationAnnouncementListForm");
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected error occured", ""));
            context.execute("PF('sysCongratulationAnnouncementEditModal').hide();");

        } finally {
            to = null;
        }
    }

    public void handleImage(FileUploadEvent event)
    {

        try {
            String uploadPath = null;
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "File with name '" + event.getFile().getFileName() + "' Succesfully Uploaded", ""));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getDestinationPath(String s)
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;
        String serverPath = null;
        try {
            Date date = new Date();
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString("congratulation_logo_upload_path");
            serverPath = resourceBundle.getString("congratulation_logo_upload_server_path");

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception("Invalid path");
            }
            String s1 = s.substring(0, s.length() - 4);
            finalPath = initialPath + s1;

            dir = new File(finalPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            destinationPath = serverPath + s1 + "/" + s;
            sysCongratulationAnnouncementBean.setSysCongratulationUploadPath(destinationPath);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return finalPath;
    }

    public void copyFile(String fileName, InputStream in)
    {
        String destinationPath = null;

        try {
            destinationPath = getDestinationPath(fileName);
            OutputStream out = new FileOutputStream(new File(destinationPath + File.separator + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.close();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Boolean checkDublicateForSave(SysCongratulationAnnouncementResponseTO to)
    {
        List<SysCongratulationAnnouncementResponseTO> list = null;
        Boolean duplicate = false;
        try {
            list = new ArrayList<SysCongratulationAnnouncementResponseTO>();
            list = sysCongratulationAnnouncementBean.getSysCongratulationAnnouncementResponseTOList();
            if (list != null && !list.isEmpty()) {
                for (SysCongratulationAnnouncementResponseTO t : list) {
                    if (t.getSysAnnouncementType().equalsIgnoreCase(to.getSysAnnouncementType())) {
                        duplicate = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return duplicate;
    }


    public Boolean checkDublicateForUpdate(SysCongratulationAnnouncementResponseTO to)
    {
        List<SysCongratulationAnnouncementResponseTO> list = null;
        Boolean duplicate = false;
        List<SysCongratulationAnnouncementResponseTO> tempList = null;
        try {
            list = new ArrayList<SysCongratulationAnnouncementResponseTO>();
            tempList = new ArrayList<SysCongratulationAnnouncementResponseTO>();
            list = sysCongratulationAnnouncementBean.getSysCongratulationAnnouncementResponseTOList();
            if (list != null && !list.isEmpty()) {
                for (SysCongratulationAnnouncementResponseTO t : list) {
                    if (t.getSysCongratulationAnnouncementID().intValue() != to.getSysCongratulationAnnouncementID()) {
                        tempList.add(t);
                    }
                }

                for (SysCongratulationAnnouncementResponseTO l : tempList) {
                    if (l.getSysAnnouncementType().equalsIgnoreCase(to.getSysAnnouncementType())) {
                        duplicate = true;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return duplicate;
    }

    public void getSysContentTypeDropDown()
        throws Exception
    {
        List<SysContentTypeTO> cTOList = null;
        List<SysContentTypeTO> fTOList = null;
        try {
            cTOList = new ArrayList<SysContentTypeTO>();
            fTOList = new ArrayList<SysContentTypeTO>();
            cTOList = sysCongratulationAnnouncementService.getSysContentTypeDropDown();
            for (SysContentTypeTO t : cTOList) {
                if (!t.getSysType().equalsIgnoreCase("Announcement")) {
                    fTOList.add(t);
                }
            }
            sysCongratulationAnnouncementBean.setAnnouncementTypeTOList(fTOList);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
