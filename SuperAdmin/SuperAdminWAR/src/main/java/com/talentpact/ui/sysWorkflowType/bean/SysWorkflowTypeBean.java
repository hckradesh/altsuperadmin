package com.talentpact.ui.sysWorkflowType.bean;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysSubModuleTO;
import com.talentpact.business.application.transport.output.SysWorkflowStageTypeGroupTO;
import com.talentpact.business.application.transport.output.SysWorkflowStageTypeTO;
import com.talentpact.business.application.transport.output.SysWorkflowTypeTO;
import com.talentpact.business.application.transport.output.UIFormTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysWorkflowType.controller.SysWorkflowTypeController;

@Named("sysWorkflowTypeBean")
@ConversationScoped
public class SysWorkflowTypeBean extends CommonBean implements Serializable, IDefaultAction {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(SysWorkflowTypeBean.class.getName());

    @Inject
    SysWorkflowTypeController sysWorkflowTypeController;

    private SysWorkflowTypeTO sysWorkflowTypeTO;

    private List<SysWorkflowTypeTO> sysWorkflowTypeTOList;

    private List<SysSubModuleTO> sysSubModuleTOList;

    private SysWorkflowStageTypeTO sysWorkflowStageTypeTO;

    private SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO;

    private List<UIFormTO> uiFormTOList;

    private String dialogHeaderPrefix;

    private String dialogButtonName;

    public List<SysSubModuleTO> getSysSubModuleTOList() {
        return sysSubModuleTOList;
    }

    public void setSysSubModuleTOList(List<SysSubModuleTO> sysSubModuleTOList) {
        this.sysSubModuleTOList = sysSubModuleTOList;
    }

    public SysWorkflowTypeTO getSysWorkflowTypeTO() {
        return sysWorkflowTypeTO;
    }

    public void setSysWorkflowTypeTO(SysWorkflowTypeTO sysWorkflowTypeTO) {
        this.sysWorkflowTypeTO = sysWorkflowTypeTO;
    }

    public List<SysWorkflowTypeTO> getSysWorkflowTypeTOList() {
        return sysWorkflowTypeTOList;
    }

    public void setSysWorkflowTypeTOList(List<SysWorkflowTypeTO> sysWorkflowTypeTOList) {
        this.sysWorkflowTypeTOList = sysWorkflowTypeTOList;
    }

    @Override
    public void initialize() {
        try {
        	endConversation();
            beginConversation();
            sysWorkflowTypeController.initialize();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public SysWorkflowStageTypeTO getSysWorkflowStageTypeTO() {
        return sysWorkflowStageTypeTO;
    }

    public void setSysWorkflowStageTypeTO(SysWorkflowStageTypeTO sysWorkflowStageTypeTO) {
        this.sysWorkflowStageTypeTO = sysWorkflowStageTypeTO;
    }

    public SysWorkflowStageTypeGroupTO getSysWorkflowStageTypeGroupTO() {
        return sysWorkflowStageTypeGroupTO;
    }

    public void setSysWorkflowStageTypeGroupTO(SysWorkflowStageTypeGroupTO sysWorkflowStageTypeGroupTO) {
        this.sysWorkflowStageTypeGroupTO = sysWorkflowStageTypeGroupTO;
    }

    public List<UIFormTO> getUiFormTOList() {
        return uiFormTOList;
    }

    public void setUiFormTOList(List<UIFormTO> uiFormTOList) {
        this.uiFormTOList = uiFormTOList;
    }

    public String getDialogHeaderPrefix() {
        return dialogHeaderPrefix;
    }

    public void setDialogHeaderPrefix(String dialogHeaderPrefix) {
        this.dialogHeaderPrefix = dialogHeaderPrefix;
    }

    public String getDialogButtonName() {
        return dialogButtonName;
    }

    public void setDialogButtonName(String dialogButtonName) {
        this.dialogButtonName = dialogButtonName;
    }

}
