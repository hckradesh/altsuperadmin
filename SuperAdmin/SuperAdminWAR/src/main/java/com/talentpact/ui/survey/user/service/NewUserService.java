/**
 * 
 */
package com.talentpact.ui.survey.user.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.survey.user.dataprovider.NewUserDataProvider;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class NewUserService {

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(NewUserService.class);

	@EJB
	NewUserDataProvider newUserDataProvider;

	/**
	 * @param serviceRequest
	 * @return
	 */
//	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
//	public ServiceResponse createSurveyUser(ServiceRequest serviceRequest)
//			throws Exception {
//		ServiceResponse serviceResponse = null;
//		try {
//			serviceResponse = newUserDataProvider
//					.createSurveyUser(serviceRequest);
//		} catch (Exception ex) {
//			LOGGER_.debug("Exception thrown while creating new user for survey : "
//					+ ex);
//			throw ex;
//		} finally {
//
//		}
//		return null;
//	}

}
