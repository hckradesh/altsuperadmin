/**
 * 
 */
package com.talentpact.ui.common.phaselistener;


import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * @author seema.sharma
 *
 */
public class AdminListener implements PhaseListener
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void afterPhase(PhaseEvent event)
    {
        //        System.out.println(event.getPhaseId());
    }


    @Override
    public void beforePhase(PhaseEvent event)
    {

        //        System.out.println();

    }

    @Override
    public PhaseId getPhaseId()
    {
        return PhaseId.ANY_PHASE;
    }

}
