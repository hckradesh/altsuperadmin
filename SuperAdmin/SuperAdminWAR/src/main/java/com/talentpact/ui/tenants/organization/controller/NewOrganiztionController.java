/**
 * 
 */
package com.talentpact.ui.tenants.organization.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.New;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.alt.common.constants.CoreConstants;
import com.alt.security.transport.output.UiFormTO;
import com.alt.system.transport.TpAppTO;
import com.talentpact.business.application.loginserver.LoginServerTypeTO;
import com.talentpact.business.application.loginserver.SocialAppSettingTO;
import com.talentpact.business.application.transport.helper.UserListHelper;
import com.talentpact.business.application.transport.input.AltBenefitRequestTO;
import com.talentpact.business.application.transport.input.CheckListRequestTO;
import com.talentpact.business.application.transport.input.ContentListRequestTO;
import com.talentpact.business.application.transport.input.DashboardChartRequestTO;
import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.application.transport.input.HrContentRequestTO;
import com.talentpact.business.application.transport.input.HrModuleRequestTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.input.SysMenuRequestTO;
import com.talentpact.business.application.transport.input.SysProductAnnouncementRequestTO;
import com.talentpact.business.application.transport.input.SysRoleRequestTO;
import com.talentpact.business.application.transport.input.UIFormRequestTO;
import com.talentpact.business.application.transport.input.UserRequestTO;
import com.talentpact.business.application.transport.output.AltBenefitResponseTO;
import com.talentpact.business.application.transport.output.AppResponseTO;
import com.talentpact.business.application.transport.output.CheckListResponseTO;
import com.talentpact.business.application.transport.output.DashboardChartResponseTO;
import com.talentpact.business.application.transport.output.HrModuleTO;
import com.talentpact.business.application.transport.output.OrgAppPortalTreeResponseTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.SysMenuTO;
import com.talentpact.business.application.transport.output.SysProductAnnouncementTO;
import com.talentpact.business.application.transport.output.SysRoleTypeTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.business.application.transport.output.UserResponseTO;
import com.talentpact.business.common.cache.CacheUtil;
import com.talentpact.business.common.constants.IConstants;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.common.transport.IRequestTansportObject;
import com.talentpact.business.common.transport.IResponseTransportObject;
import com.talentpact.business.dataservice.dashboardChart.DashboardChartService;
import com.talentpact.business.service.altbenefits.HrVendorOfferingSequenceService;
import com.talentpact.business.service.dashboard.DashBoardService;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.business.tpOrganization.service.impl.OrganizationService;
import com.talentpact.business.tpOrganization.transport.input.OrganizationrequestTO;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.AppPortalTypeUrlResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.OrganizationResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.remote.checkList.ICheckListFacadeRemote;
import com.talentpact.remote.tpOrganization.INewOrganizationFacadeRemote;
import com.talentpact.ui.checklist.bean.CheckListBean;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.common.controller.LookupAction;
import com.talentpact.ui.dashboard.transport.input.CopyDashBoardReqTO;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.login.bean.LoginBean;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.survey.user.facaderemote.NewUserFacadeRemote;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.tenants.organization.bean.NewOrganizationBean;
import com.talentpact.ui.tenants.organization.helper.OrganizationHelper;

import javassist.expr.NewArray;

/**
 * @author radhamadhab.dalai
 * 
 */

@Named("newOrganiztionController")
@SessionScoped
public class NewOrganiztionController extends CommonController implements Serializable, IDefaultAction {

	private static final long serialVersionUID = 1L;

	@Inject
	CheckListBean checkListBean;

	@Inject
	NewOrganizationBean newOrganizationBean;

	@Inject
	MenuBean menuBean;

	@Inject
	UserSessionBean userSessionBean;

	@Inject
	LookupAction lookupAction;

	@Inject
	LoginBean loginBean;

	@EJB(beanName = "NewOrganizationFacade")
	INewOrganizationFacadeRemote iNewOrganizationFacadeRemote;

	@EJB(beanName = "CheckListFacade")
	ICheckListFacadeRemote iCheckListFacadeRemote;

	@EJB
	private NewUserFacadeRemote newUserFacadeRemote;

	@EJB
	HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

	@EJB
	DashBoardService dashBoardService;

	@EJB
	DashboardChartService dashBoardChartService;

	@EJB
	OrganizationService organizationService;

	private Boolean createNewOrg = false;

	int counter = 0;

	private Map<String, List<String>> orgKeyListCacheMap = new HashMap<String, List<String>>();

	static boolean wpstatus = false;

	private List<HrAppTO> selectedAppsBeforeUpdate = new ArrayList<>();

	private List<String> selectedActiveAppIdsBeforeUpdate = new ArrayList<>();
	private List<String> selectedInactiveAppIdsBeforeUpdate = new ArrayList<>();

	public void createNewOrganisation() {

		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		NewTenantResponseTO newTenantResponseTO = null;
		OrgAppPortalTreeResponseTO orgAppPortalTreeTO = null;

		try {
			/* Get All Tenants */
			serviceResponse = iNewOrganizationFacadeRemote.getAllTenants(serviceRequest);
			newTenantResponseTO = (NewTenantResponseTO) serviceResponse.getResponseTransportObject();
			List<NewTenantResponseTO> tenantListR = newTenantResponseTO.getTenantListTo();

			newOrganizationBean.setTenantList(tenantListR);
			/* Get All Apps */
			serviceResponse = iNewOrganizationFacadeRemote.getCompleteAppTree(serviceRequest);
			orgAppPortalTreeTO = (OrgAppPortalTreeResponseTO) serviceResponse.getResponseTransportObject();

			// create module app
			OrganizationHelper.reassignModuleName(orgAppPortalTreeTO);

			newOrganizationBean.setOrgAppPortalTreeTO(orgAppPortalTreeTO);
			populateConstants(orgAppPortalTreeTO.getSysConstantMap());
			List<SelectItem> tenantToList = OrganizationHelper
					.convertTenantTOSelectItem(orgAppPortalTreeTO.getTenantList());
			newOrganizationBean.setTenantResTOList(tenantToList);
			List<SelectItem> dbSelectlist = OrganizationHelper
					.convertMetaDataSelectItem(CacheUtil.getDatabaseMetadataInList());
			newOrganizationBean.setMetaDatabaseList(dbSelectlist);
			// selecting database for Tenant --system
			selectDatabaseForTenantId(1, orgAppPortalTreeTO);

			//
			createNewOrg = true;
			newOrganizationBean.setUpdateFlag(false);
			newOrganizationBean.setSaveFlag(true);
			newOrganizationBean.setEmployeeCount(null);
			newOrganizationBean.setLine1(null);
			newOrganizationBean.setCountry(null);
			newOrganizationBean.setState(null);
			newOrganizationBean.setDistrict(null);
			newOrganizationBean.setCity(null);
			newOrganizationBean.setZipCode(null);
			newOrganizationBean.setEmail(null);
			newOrganizationBean.setCountryID(null);
			newOrganizationBean.setStateID(null);
			newOrganizationBean.setDistrictID(null);
			newOrganizationBean.setCityID(null);
			newOrganizationBean.setZipCodeID(null);
			newOrganizationBean.setStateTOList(null);
			newOrganizationBean.setDistrictTOList(null);
			newOrganizationBean.setCityTOList(null);
			newOrganizationBean.setZipCodeTOList(null);
			newOrganizationBean.setOrgFinancialStartDate(null);
			newOrganizationBean.setGovtFinancialStartDate(null);
			newOrganizationBean.setLdapServer(null);
			newOrganizationBean.setLdapSearchBase(null);
			newOrganizationBean.setLdapDomain(null);
			newOrganizationBean.setSecretKey(null);
			newOrganizationBean.setLdapActive(false);
			newOrganizationBean.setLdapWindowsAuthentication(false);
			newOrganizationBean.setLdapLogoutUrl(null);
			newOrganizationBean.setSpServer(null);
			newOrganizationBean.setSpLogoutUrl(null);
			newOrganizationBean.setSpActive(false);

			// Getting System level bundle list
			List<SysBundleResponseTO> bundleList = iNewOrganizationFacadeRemote.getSysBundle();
			if (bundleList != null) {
				newOrganizationBean.setSysBundle(bundleList);
				for (SysBundleResponseTO o : bundleList) {
					if (o.getName().equalsIgnoreCase("EN")) {
						newOrganizationBean.setSelectedBundleIds(new ArrayList<Integer>());
						newOrganizationBean.getSelectedBundleIds().add(o.getBundleId());
						break;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		/* Clear The data when switching in same conversation */
		newOrganizationBean.clearData();
		menuBean.setPage("../neworganization.xhtml");

	}

	private void selectDatabaseForTenantId(int i, OrgAppPortalTreeResponseTO orgAppPortalTreeTO) throws Exception {
		List<AppResponseTO> appList = orgAppPortalTreeTO.getAppList();
		StringBuilder key = null;
		for (AppResponseTO to : appList) {
			key = new StringBuilder();
			key.append(IConstants.DATABASE_METADATA_START_KEY + to.getAppCode() + IConstants.UNDERSCORE + i);
			String dataBase = CacheUtil.getDatabaseByPersistanceKey(key.toString());
			if (dataBase == null || dataBase.isEmpty()) {
				to.setDatabase("");
			} else {
				to.setDatabase(dataBase);
			}
		}
	}

	private void selectDateForTenantId(int tenantId, OrgAppPortalTreeResponseTO orgAppPortalTreeTO) throws Exception {
		if (orgAppPortalTreeTO == null || orgAppPortalTreeTO.getTenantList() == null
				|| orgAppPortalTreeTO.getTenantList().isEmpty()) {
			return;
		}

		List<NewTenantResponseTO> dateList = orgAppPortalTreeTO.getTenantList();
		for (NewTenantResponseTO to : dateList) {
			if (tenantId == to.getTenantID().intValue()) {
				if (to.getContractSignedDate() != null) {
					newOrganizationBean.setContractSignedDate(to.getContractSignedDate());
				}
				if (to.getEffectiveFrom() != null) {
					newOrganizationBean.setEffectiveFromDate(to.getEffectiveFrom());
				}
				if (to.getEffectiveTo() != null) {
					newOrganizationBean.setEffectiveTODate(to.getEffectiveTo());
				}
			}
		}
	}

	public void initialize() {
		ServiceResponse serviceResponse = null;
		try {
			ServiceRequest serviceRequest = new ServiceRequest();
			serviceResponse = iNewOrganizationFacadeRemote.getAllOrganizations(serviceRequest);
			OrganizationResponseTO OrgTo = (OrganizationResponseTO) serviceResponse.getResponseTransportObject();
			if (OrgTo != null) {
				newOrganizationBean.setAllOrgList(OrgTo.getOrganizationResponseTOList());
				newOrganizationBean.setLoginServerTypeList(OrgTo.getLoginServerTypeTOList());
				newOrganizationBean.setPartnerAppList(OrgTo.getPartnerAppList());
				newOrganizationBean.setCustomAppList(OrgTo.getCustomAppList());
			}
			newOrganizationBean.setFilteredOrgList((null));
			menuBean.setPage("../organisation.xhtml");
			ResetMoreInfo();
			getCountryDropDown();
			getSectorDropDown();
			getModuleDropDownList();
			getSysContentTypeDropDown();
			newOrganizationBean.setWelcomeMail(false);
			newOrganizationBean.setSelectedCustomAppList(null);
			newOrganizationBean.setSelectedPartnerAppList(null);
		} catch (Exception ex) {
		}
		loginBean.setMenuName("Organisation");

	}

	public void ResetMoreInfo() {
		newOrganizationBean.setRenderLoginServerPanel(false);
		newOrganizationBean.setSelectedLoginServerList(null);
		newOrganizationBean.setEditLoginServer(false);
		newOrganizationBean.setSelectedLoginServerTypes(null);
		newOrganizationBean.setSocialAppSettingTOMap(null);
		newOrganizationBean.setRenderLoginServerPanel(false);
		newOrganizationBean.setSocialAppSettingTOList(new ArrayList<SocialAppSettingTO>());
		newOrganizationBean.setRenderLADP(false);
		newOrganizationBean.setSelectedLoginServerList(null);
		newOrganizationBean.setLeftLogo(null);
		newOrganizationBean.setRightLogo(null);
		newOrganizationBean.setFavicon(null);
		newOrganizationBean.setFooter(null);
		newOrganizationBean.setTitle(null);
		newOrganizationBean.setUsernameTxt(null);
		newOrganizationBean.setBanner(null);
		newOrganizationBean.setBtnTxt(null);
		newOrganizationBean.setSamlBtnTxt(null);
		newOrganizationBean.setSamlHeader(null);
		newOrganizationBean.setHeadingTxt(null);
		newOrganizationBean.setMetaData(null);
		newOrganizationBean.setResetPassword(null);
		newOrganizationBean.setPasswordTxt(null);
		newOrganizationBean.setActive(null);
		newOrganizationBean.setSectorID(null);
		newOrganizationBean.setMobileStatus(null);
		newOrganizationBean.setBackGroundImage(null);
		newOrganizationBean.setSelectedLanguages(null);
		newOrganizationBean.setWelcomeMail(false);
		newOrganizationBean.setDefaultLoginServer(CoreConstants.NONE_STRING);
		// newOrganizationBean.setOnBoardingVideoLinkList(null);
		newOrganizationBean.setPortalList(null);
		newOrganizationBean.setDashBoardChartTOlist(null);
		newOrganizationBean.setSelectedPortalID(-1);
		newOrganizationBean.setRenderSP(false);
		;

	}

	@SuppressWarnings("unused")
	public void save() {
		ServiceRequest serviceRequest = null;
		OrganizationrequestTO organizationRequestTO = null;
		String webPayInfo = null;
		String companyFolderName = null;
		String companyName = null;
		String compAddress = null;
		Integer citycode = null;
		String pincode = null;
		String compEmail = null;
		Date financialYear = null;
		String token = null;
		SimpleDateFormat dateformat = null;
		String fY = null;
		List<String> selectedLoginServerTypes = null;
		try {
			companyFolderName = newOrganizationBean.getOrgCode();
			companyName = newOrganizationBean.getOrgName();
			compAddress = newOrganizationBean.getLine1();
			// citycode = newOrganizationBean.getCityID();
			citycode = 1;
			pincode = newOrganizationBean.getZipCode();
			compEmail = newOrganizationBean.getEmail();
			financialYear = newOrganizationBean.getGovtFinancialStartDate();

			SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-YYYY");

			if (companyFolderName != null) {
				for (OrganizationResponseTO data : newOrganizationBean.getAllOrgList()) {
					if (data.getOrgCode().equalsIgnoreCase(companyFolderName) && data.getOrganizationId() != null
							&& (newOrganizationBean.getOrgId() == null || data.getOrganizationId()
									.longValue() != newOrganizationBean.getOrgId().longValue())) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Organisation Code  " + companyFolderName + " already Exists.", ""));
						return;
					}

				}

			}
			
			fY = dt1.format(financialYear);
			token = "abc";
			webPayInfo = companyFolderName + "~" + companyName + "~" + compAddress + "~" + citycode + "~" + pincode
					+ "~" + compEmail + "~" + fY + "~" + token;

			// check whether parent module is selected or not
			String parentModuleName = OrganizationHelper
					.isParentNotSelected(newOrganizationBean.getOrgAppPortalTreeTO());
			if (parentModuleName != null) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, parentModuleName + " is not selected.", ""));
				return;
			}

			/* Convert Inputs from UI Into RequestTO */
			OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = newOrganizationBean.getOrgAppPortalTreeTO();

			if (newOrganizationBean.getSelectedBundleIds() == null
					|| newOrganizationBean.getSelectedBundleIds().isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Please select bundle", ""));
				return;
			}

			organizationRequestTO = createOrganizationrequestTO();
			organizationRequestTO.setLdapServer(newOrganizationBean.getLdapServer());
			organizationRequestTO.setLdapSearchBase(newOrganizationBean.getLdapSearchBase());
			organizationRequestTO.setLdapDomain(newOrganizationBean.getLdapDomain());
			organizationRequestTO.setSecretKey(newOrganizationBean.getSecretKey());
			organizationRequestTO.setLdapActive(newOrganizationBean.isLdapActive());
			organizationRequestTO.setLdapWindowsAuthentication(newOrganizationBean.isLdapWindowsAuthentication());
			organizationRequestTO.setLdapLogoutUrl(newOrganizationBean.getLdapLogoutUrl());

			organizationRequestTO.setSpServer(newOrganizationBean.getSpServer());
			organizationRequestTO.setSpLogoutUrl(newOrganizationBean.getSpLogoutUrl());
			organizationRequestTO.setSpActive(newOrganizationBean.isSpActive());

			/**
			 * Login Server Settings Start
			 */
			List<LoginServerTypeTO> loginServerTypeTOList = newOrganizationBean.getLoginServerTypeList();
			Map<String, SocialAppSettingTO> socialAppSettingTOMap = newOrganizationBean.getSocialAppSettingTOMap();
			if (socialAppSettingTOMap == null || socialAppSettingTOMap.isEmpty()) {
				socialAppSettingTOMap = new HashMap<String, SocialAppSettingTO>();
			}
			selectedLoginServerTypes = newOrganizationBean.getSelectedLoginServerTypes();
			List<SocialAppSettingTO> socialAppSettingTOList = newOrganizationBean.getSocialAppSettingTOList();
			if (selectedLoginServerTypes != null && !selectedLoginServerTypes.isEmpty()) {
				for (LoginServerTypeTO loginType : loginServerTypeTOList) {
					for (String selectedLoginServerTypeId : selectedLoginServerTypes) {
						if (Integer.parseInt(selectedLoginServerTypeId) == loginType.getLoginTypeID()) {
							if (CoreConstants.LOGIN_SERVER_GOOGLE.equalsIgnoreCase(loginType.getTypeName())) {
								if (socialAppSettingTOList != null && !socialAppSettingTOList.isEmpty()) {
									for (SocialAppSettingTO appSettingTO : socialAppSettingTOList) {
										if (CoreConstants.LOGIN_SERVER_GOOGLE
												.equalsIgnoreCase(appSettingTO.getSocialAppType())) {
											socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_GOOGLE
													+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID(),
													appSettingTO);
											break;
										}
									}
								}
							} else if (CoreConstants.LOGIN_SERVER_FACEBOOK.equalsIgnoreCase(loginType.getTypeName())) {
								if (socialAppSettingTOList != null && !socialAppSettingTOList.isEmpty()) {
									for (SocialAppSettingTO appSettingTO : socialAppSettingTOList) {
										if (CoreConstants.LOGIN_SERVER_FACEBOOK
												.equalsIgnoreCase(appSettingTO.getSocialAppType())) {
											socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_FACEBOOK
													+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID(),
													appSettingTO);
											break;
										}
									}
								}
							} else if (CoreConstants.LOGIN_SERVER_LDAP.equalsIgnoreCase(loginType.getTypeName())) {
								if (newOrganizationBean.isRenderLADP()) {
									if ((newOrganizationBean.getLdapServer() != null
											&& !newOrganizationBean.getLdapServer().trim().equalsIgnoreCase(""))
											&& (newOrganizationBean.getLdapSearchBase() == null || newOrganizationBean
													.getLdapSearchBase().trim().equalsIgnoreCase(""))) {
										FacesContext.getCurrentInstance().addMessage(null,
												new FacesMessage(FacesMessage.SEVERITY_ERROR,
														"Please enter Ldap Search Base as you have entered Ldap Server value.",
														""));
										return;
									}

									if ((newOrganizationBean.getLdapServer() != null
											&& !newOrganizationBean.getLdapServer().trim().equalsIgnoreCase(""))
											&& (newOrganizationBean.getLdapDomain() == null || newOrganizationBean
													.getLdapDomain().trim().equalsIgnoreCase(""))) {
										FacesContext.getCurrentInstance().addMessage(null,
												new FacesMessage(FacesMessage.SEVERITY_ERROR,
														"Please enter Ldap domain as you have entered Ldap Server value.",
														""));
										return;
									}

									if (((newOrganizationBean.getLdapSearchBase() != null
											&& !newOrganizationBean.getLdapSearchBase().trim().equalsIgnoreCase(""))
											|| (newOrganizationBean.getLdapDomain() != null && !newOrganizationBean
													.getLdapDomain().trim().equalsIgnoreCase("")))
											&& (newOrganizationBean.getLdapServer() == null || newOrganizationBean
													.getLdapServer().trim().equalsIgnoreCase(""))) {
										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
												FacesMessage.SEVERITY_ERROR, "Please enter Ldap Server value.", ""));
										return;
									}

									organizationRequestTO.setLdapServer(newOrganizationBean.getLdapServer());
									organizationRequestTO.setLdapSearchBase(newOrganizationBean.getLdapSearchBase());
									organizationRequestTO.setLdapDomain(newOrganizationBean.getLdapDomain());
									organizationRequestTO.setSecretKey(newOrganizationBean.getSecretKey());
									organizationRequestTO.setLdapActive(newOrganizationBean.isLdapActive());
									organizationRequestTO.setLdapWindowsAuthentication(
											newOrganizationBean.isLdapWindowsAuthentication());
									organizationRequestTO.setLdapTypeID(loginType.getLoginTypeID());
									SocialAppSettingTO appSettingTO2 = new SocialAppSettingTO();
									appSettingTO2.setActive(newOrganizationBean.isLdapActive());
									socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_LDAP
											+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID(), appSettingTO2);

								}

							} else if (CoreConstants.LOGIN_SERVER_SP.equalsIgnoreCase(loginType.getTypeName())) {
								if (newOrganizationBean.isRenderSP()) {
									if ((newOrganizationBean.getSpServer() == null
											|| newOrganizationBean.getSpServer().trim().equalsIgnoreCase(""))) {
										FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
												FacesMessage.SEVERITY_ERROR, "Please enter SP Server value.", ""));
										return;
									}
									organizationRequestTO.setSpServer(newOrganizationBean.getSpServer());
									organizationRequestTO.setSpLogoutUrl(newOrganizationBean.getSpLogoutUrl());
									organizationRequestTO.setSpActive(newOrganizationBean.isSpActive());
									SocialAppSettingTO appSettingTO2 = new SocialAppSettingTO();
									appSettingTO2.setActive(newOrganizationBean.isSpActive());
									socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_SP + CoreConstants.AT_SPERATOR
											+ loginType.getLoginTypeID(), appSettingTO2);
								}

							}
						}
					}
				}
				organizationRequestTO.setSocialAppSettingTOMap(socialAppSettingTOMap);
				organizationRequestTO.setDefaultServer(newOrganizationBean.getDefaultLoginServer());
			} else {
				organizationRequestTO.setLdapActive(false);
			}

			/**
			 * Login Server Settings End
			 */

			// added by vikas
			organizationRequestTO.setOrgAppPortalTreeResponseTO(orgAppPortalTreeResponseTO);

			/* Start Cache Update */
			List<String> oldKeyList = orgKeyListCacheMap.get(newOrganizationBean.getOrgCode());
			organizationRequestTO.setOrgKeyListCacheList(oldKeyList);
			/* End Cache Update */

			organizationRequestTO.setOrganizationId(newOrganizationBean.getOrgId());

			organizationRequestTO.setMessengerEnabled(newOrganizationBean.isMessengerEnabled());
			organizationRequestTO.setMobileSessionTimeout(newOrganizationBean.getMobileSessionTimeout());

			serviceRequest = new ServiceRequest();
			serviceRequest.setRequestTansportObject(organizationRequestTO);
			ServiceResponse serviceResponse = iNewOrganizationFacadeRemote.saveOrganization(serviceRequest);
			OrganizationResponseTO orgTo = (OrganizationResponseTO) serviceResponse.getResponseTransportObject();
			newOrganizationBean.setOrgId(orgTo.getOrganizationId());

			// lavina
			List<String> selectedApps = new ArrayList<>();
			if (newOrganizationBean.getSelectedPartnerAppList() != null) {
				selectedApps.addAll(newOrganizationBean.getSelectedPartnerAppList());
				/*
				 * iNewOrganizationFacadeRemote.saveSelectedPartnerApp(
				 * newOrganizationBean.getSelectedPartnerAppList(),
				 * newOrganizationBean.getOrgId(), newOrganizationBean.getTenantID());
				 */
			}

			if (newOrganizationBean.getSelectedCustomAppList() != null) {
				selectedApps.addAll(newOrganizationBean.getSelectedCustomAppList());
				/*
				 * iNewOrganizationFacadeRemote.saveSelectedCustomApp(
				 * newOrganizationBean.getSelectedCustomAppList(),
				 * newOrganizationBean.getOrgId(), newOrganizationBean.getTenantID());
				 */
			}
			if (selectedApps != null) {

				iNewOrganizationFacadeRemote.saveSelectedApps(createFinalNewAppList(selectedApps,
						newOrganizationBean.getOrgId(), newOrganizationBean.getTenantID()));
			}

			if (newOrganizationBean.getSaveFlag()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Organisation Saved Succesfully. For bettter results,wait for 10 minutes ", ""));
				createOrganizationInWebPay(newOrganizationBean.getOrgAppPortalTreeTO().getAppModuleMap(), webPayInfo,
						companyFolderName);
			}
			if (newOrganizationBean.getUpdateFlag()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Organisation Updated SuccesfullyFor bettter results,wait for 10 minutes  ", ""));
				initialize();
				// p updateOrgForWebPay();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			RequestContext.getCurrentInstance().execute("scrollUp()");
		}
	}

	private OrganizationrequestTO createOrganizationrequestTO() {
		OrganizationrequestTO organizationRequestTO = new OrganizationrequestTO();
		organizationRequestTO.setWelcomeMail(newOrganizationBean.isWelcomeMail());
		organizationRequestTO.setOrgName(newOrganizationBean.getOrgName());
		organizationRequestTO.setOrgCode(newOrganizationBean.getOrgCode());
		organizationRequestTO.setDataLocation(newOrganizationBean.getDataLocation());
		organizationRequestTO.setSectorID(newOrganizationBean.getSectorID());
		organizationRequestTO.setStatusID(newOrganizationBean.getStatusID());
		organizationRequestTO.setTenantID(newOrganizationBean.getTenantID());
		organizationRequestTO.setContractSignedDate(newOrganizationBean.getContractSignedDate());
		organizationRequestTO.setEffectiveDateFrom(newOrganizationBean.getEffectiveFromDate());
		if (newOrganizationBean.getEffectiveTODate() != null) {
			organizationRequestTO.setEffectiveDateTO(newOrganizationBean.getEffectiveTODate());
		}
		organizationRequestTO.setDataLocation(newOrganizationBean.getDataLocation());
		organizationRequestTO.setEmployeeCount(newOrganizationBean.getEmployeeCount().intValue());
		organizationRequestTO.setCreatedBy(userSessionBean.getUserID());
		organizationRequestTO.setModifiedBy(userSessionBean.getUserID());
		organizationRequestTO.setLeftLogo(newOrganizationBean.getLeftLogo());
		organizationRequestTO.setRightLogo(newOrganizationBean.getRightLogo());
		organizationRequestTO.setPasswordTxt(newOrganizationBean.getPasswordTxt());
		organizationRequestTO.setUsernameTxt(newOrganizationBean.getUsernameTxt());
		organizationRequestTO.setFooter(newOrganizationBean.getFooter());
		organizationRequestTO.setBtnTxt(newOrganizationBean.getBtnTxt());
		organizationRequestTO.setBanner(newOrganizationBean.getBanner());
		organizationRequestTO.setHeadingTxt(newOrganizationBean.getHeadingTxt());
		organizationRequestTO.setTitle(newOrganizationBean.getTitle());
		organizationRequestTO.setSamlHeader(newOrganizationBean.getSamlHeader());
		organizationRequestTO.setSamlBtnTxt(newOrganizationBean.getSamlBtnTxt());
		organizationRequestTO.setBundleIds(newOrganizationBean.getSelectedBundleIds());
		organizationRequestTO.setLine1(newOrganizationBean.getLine1());
		organizationRequestTO.setCity(newOrganizationBean.getCity());
		organizationRequestTO.setCityID(newOrganizationBean.getCityID());
		organizationRequestTO.setState(newOrganizationBean.getState());
		organizationRequestTO.setCountry(newOrganizationBean.getCountry());
		organizationRequestTO.setZipCode(newOrganizationBean.getZipCode());
		organizationRequestTO.setEmail(newOrganizationBean.getEmail());
		organizationRequestTO.setDistrict(newOrganizationBean.getDistrict());
		organizationRequestTO.setSectorCode(newOrganizationBean.getSectorCode());
		organizationRequestTO.setRecurringPerMonthAmount(newOrganizationBean.getRecurringPerMonthAmount());
		organizationRequestTO.setSetupCost(newOrganizationBean.getSetupCost());
		organizationRequestTO.setCustomizationAmount(newOrganizationBean.getCustomizationAmount());
		organizationRequestTO.setOrgFinancialStartDate(newOrganizationBean.getOrgFinancialStartDate());
		organizationRequestTO.setGovtFinancialStartDate(newOrganizationBean.getGovtFinancialStartDate());
		organizationRequestTO.setMobileStatus(newOrganizationBean.getMobileStatus());
		organizationRequestTO.setBackGroundImage(newOrganizationBean.getBackGroundImage());
		organizationRequestTO.setContactNumber(newOrganizationBean.getContactNumber());
		organizationRequestTO.setExternalVideoPath(newOrganizationBean.getExternalVideoPath());
		organizationRequestTO.seteMail(newOrganizationBean.geteMail());
		organizationRequestTO.setVideoLinkPathList(newOrganizationBean.getOnBoardingVideoLinkList());
		organizationRequestTO.setSysContentTypeID(newOrganizationBean.getSysContentTypeID());
		organizationRequestTO.setLogedInUserId(userSessionBean.getUserID().intValue());
		organizationRequestTO.setBundleIds(newOrganizationBean.getSelectedBundleIds());
		organizationRequestTO.setAttendanceDistanceLimit(newOrganizationBean.getAttendanceDistanceLimit());
		organizationRequestTO.setExternalUrl(newOrganizationBean.getExternalUrl());
		return organizationRequestTO;
	}

	public void cancel() {
		try {
			initialize();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public void editOrganization(OrganizationResponseTO organizationResponseTO) {
		ServiceRequest serviceRequest = null;
		ServiceRequest serviceRequest1 = null;
		try {
			getFinancialYearList(organizationResponseTO.getOrganizationId());
			getGovtFinancialYearList(organizationResponseTO.getOrganizationId());
			List<AddressResponseTO> stateList = new ArrayList<AddressResponseTO>();
			List<AddressResponseTO> distList = new ArrayList<AddressResponseTO>();
			List<AddressResponseTO> cityList = new ArrayList<AddressResponseTO>();
			List<AddressResponseTO> zipList = new ArrayList<AddressResponseTO>();
			List<SectorCostResponseTO> sectorList = new ArrayList<SectorCostResponseTO>();
			List<String> defaultLoginServerList = new ArrayList<String>();
			List<String> selectedLoginServerTypes = new ArrayList<String>();
			if (organizationResponseTO == null) {
				/* Error message */
			}

			CheckListRequestTO checkListRequestTO = null;
			List<CheckListResponseTO> listCheckListResponseTO;
			serviceRequest = new ServiceRequest();

			OrganizationrequestTO organizationrequestTO = new OrganizationrequestTO();
			organizationrequestTO.setOrganizationId(organizationResponseTO.getOrganizationId());
			newOrganizationBean.setOrgId(organizationResponseTO.getOrganizationId());
			serviceRequest.setRequestTansportObject(organizationrequestTO);
			ServiceResponse response = iNewOrganizationFacadeRemote.getCompleteMergedAppTreeForOrg(serviceRequest);
			OrganizationResponseTO resTo = (OrganizationResponseTO) response.getResponseTransportObject();
			newOrganizationBean.setOrgTO(resTo);
			/* Save Old Organization Cache Key */
			Map<Long, List<AppPortalTypeUrlResponseTO>> portalMapTO = resTo.getOrgAppPortalTreeResponseTO()
					.getAppPortalUrlMap();

			// create module app
			OrganizationHelper.reassignModuleName(resTo.getOrgAppPortalTreeResponseTO());

			if (portalMapTO != null) {
				List<String> portalUrlList = new ArrayList<String>();
				for (Map.Entry<Long, List<AppPortalTypeUrlResponseTO>> portalEntry : portalMapTO.entrySet()) {
					String portalUrlStr = null;
					// newOrganizationBean.orgAppPortalTreeTO.appList
					for (AppPortalTypeUrlResponseTO portaTOCh : portalEntry.getValue()) {
						if (portaTOCh.isPortalChecked()) {
							portalUrlStr = portaTOCh.getPortalTypeName().concat("_").concat(portaTOCh.getUrl());
							portalUrlList.add(portalUrlStr);

						}
					}

				}
				orgKeyListCacheMap.put(resTo.getOrgCode(), portalUrlList);

			}
			/* End cache code */

			// orgAppPortalTreeResponseTO.getAppModuleMap();
			newOrganizationBean.setDefaultLoginServer(CoreConstants.NONE_STRING);
			newOrganizationBean.setAttendanceDistanceLimit(resTo.getAttendanceDistanceLimit());
			newOrganizationBean.setSysContentTypeID(resTo.getSysContentTypeID());
			newOrganizationBean.setStatusID(resTo.getStatusID());
			newOrganizationBean.setDataLocation(resTo.getDataLocation());
			newOrganizationBean.setTenantID(resTo.getTenantId());
			newOrganizationBean.setContractSignedDate(resTo.getContractSignedDate());
			newOrganizationBean.setEffectiveFromDate(resTo.getEffectiveDateFrom());
			newOrganizationBean.setEffectiveTODate(resTo.getEffectiveDateTO());
			newOrganizationBean.setOrgCode(resTo.getOrgCode());
			newOrganizationBean.setWelcomeMail(resTo.isWelcomeMail());
			newOrganizationBean.setEmployeeCount(resTo.getEmployeeCount());

			newOrganizationBean.setLine1(resTo.getLine1());
			newOrganizationBean.setCity(resTo.getCity());
			newOrganizationBean.setCityID(resTo.getCityID());
			newOrganizationBean.setState(resTo.getState());
			newOrganizationBean.setCountry(resTo.getCountry());
			newOrganizationBean.setZipCode(resTo.getZipCode());
			newOrganizationBean.setEmail(resTo.getEmail());
			newOrganizationBean.setDistrict(resTo.getDistrict());

			newOrganizationBean.setLdapServer(resTo.getLdapServer());
			newOrganizationBean.setLdapSearchBase(resTo.getLdapSearchBase());
			newOrganizationBean.setLdapDomain(resTo.getLdapDomain());
			newOrganizationBean.setLdapActive(resTo.isLdapActive());
			newOrganizationBean.setLdapWindowsAuthentication(resTo.isLdapWindowsAuthentication());
			newOrganizationBean.setMessengerEnabled(resTo.getMessengerEnabled());
			newOrganizationBean.setMobileSessionTimeout(resTo.getMobileSessionTimeout());
			newOrganizationBean.setLdapLogoutUrl(resTo.getLdapLogoutUrl());

			newOrganizationBean.setSpServer(resTo.getSpServer());
			newOrganizationBean.setSpLogoutUrl(resTo.getSpLogoutUrl());
			newOrganizationBean.setSpActive(resTo.isSpActive());

			stateList = getStateDropDown(resTo.getCountry());
			sectorList = getSectorDropDown();
			if (stateList == null || stateList.isEmpty()) {
				distList = null;
				cityList = null;
				zipList = null;
			} else {
				distList = getDistrictDropDown(resTo.getState());
			}
			if (distList == null || distList.isEmpty()) {
				cityList = null;
				zipList = null;
			} else {
				cityList = getCityDropDown(resTo.getDistrict());
			}
			if (cityList == null || cityList.isEmpty()) {
				zipList = null;
			} else {
				zipList = getZipCodeDropDown(resTo.getCityID());
			}
			newOrganizationBean.setStateTOList(stateList);
			newOrganizationBean.setDistrictTOList(distList);
			newOrganizationBean.setCityTOList(cityList);
			newOrganizationBean.setZipCodeTOList(zipList);

			newOrganizationBean.setRecurringPerMonthAmount(resTo.getRecurringPerMonthAmount());
			newOrganizationBean.setSetupCost(resTo.getSetupCost());
			newOrganizationBean.setCustomizationAmount(resTo.getCustomizationAmount());
			newOrganizationBean.setSectorID(resTo.getSectorId());

			newOrganizationBean.setOrgName(resTo.getOrgName());
			newOrganizationBean.setOrgAppPortalTreeTO(resTo.getOrgAppPortalTreeResponseTO());
			newOrganizationBean.setTenantList(resTo.getTenantTOList());
			newOrganizationBean.setOrgId(resTo.getOrganizationId());
			populateConstants(resTo.getOrgAppPortalTreeResponseTO().getSysConstantMap());
			List<SelectItem> tenantToList = OrganizationHelper
					.convertTenantTOSelectItem(resTo.getOrgAppPortalTreeResponseTO().getTenantList());
			newOrganizationBean.setTenantResTOList(tenantToList);
			List<SelectItem> dbSelectlist = OrganizationHelper
					.convertMetaDataSelectItem(CacheUtil.getDatabaseMetadataInList());
			newOrganizationBean.setMetaDatabaseList(dbSelectlist);
			// select databasees for this tenant
			selectDatabaseForTenantId(resTo.getTenantId().intValue(), resTo.getOrgAppPortalTreeResponseTO());
			/* accessing CheckList here * */

			checkListRequestTO = new CheckListRequestTO();

			checkListRequestTO.setOrganizationID(newOrganizationBean.getOrgId());

			serviceRequest1 = new ServiceRequest();
			serviceRequest1.setRequestTansportObject(checkListRequestTO);

			response = iCheckListFacadeRemote.getAllCheckListByOrganizationId(serviceRequest1);
			listCheckListResponseTO = (List) response.getListResponseTransportObject();

			checkListBean.setListCheckListResponseTO(listCheckListResponseTO);

			// get selected apps
			List<HrAppTO> selectedPartnerApps = iNewOrganizationFacadeRemote
					.getSelectedPartnerApps(newOrganizationBean.getOrgId(), newOrganizationBean.getTenantID());
			List<HrAppTO> selectedCustomApps = iNewOrganizationFacadeRemote
					.getSelectedCustomApps(newOrganizationBean.getOrgId(), newOrganizationBean.getTenantID());

			selectedActiveAppIdsBeforeUpdate.clear();
			selectedInactiveAppIdsBeforeUpdate.clear();
			newOrganizationBean.setSelectedPartnerAppList(getActiveApps(selectedPartnerApps));
			newOrganizationBean.setSelectedCustomAppList(getActiveApps(selectedCustomApps));

			// getting list of ufirom
			List<UiFormTO> selectLandingePagList = iNewOrganizationFacadeRemote
					.getLandingPageList(resTo.getOrganizationId());
			newOrganizationBean.setSelectLandingePagList(selectLandingePagList);

			// Getting System level bundle list
			List<SysBundleResponseTO> bundleList = iNewOrganizationFacadeRemote.getSysBundle();
			if (bundleList != null) {
				newOrganizationBean.setSysBundle(bundleList);

			}
			String selectedLanguagesName = "";
			List<Integer> orgbundleList = iNewOrganizationFacadeRemote.getOrgBundle(serviceRequest);
			if (orgbundleList != null && !orgbundleList.isEmpty()) {
				newOrganizationBean.setSelectedBundleIds(orgbundleList);
				Set<Integer> set = new HashSet<Integer>();
				for (Integer l : orgbundleList) {
					set.add(l);
				}
				for (Integer l : set) {
					for (SysBundleResponseTO t : bundleList) {
						if (l == t.getBundleId()) {
							selectedLanguagesName = selectedLanguagesName + t.getName() + ",";
						}
					}
				}
				newOrganizationBean
						.setSelectedLanguages(selectedLanguagesName.substring(0, selectedLanguagesName.length() - 1));
			} else {
				newOrganizationBean.setSelectedLanguages("Select Language");
			}

			createNewOrg = false;
			newOrganizationBean.setUpdateFlag(true);
			newOrganizationBean.setSaveFlag(false);
			boolean defaultServer = false;
			List<String> selectedLoginServerList = new ArrayList<String>();
			selectedLoginServerTypes = new ArrayList<String>();
			List<SocialAppSettingTO> socialAppSettingTOList = null;
			socialAppSettingTOList = new ArrayList<SocialAppSettingTO>();
			if (resTo.getSocialAppSettingTOMap() != null && !resTo.getSocialAppSettingTOMap().isEmpty()) {
				newOrganizationBean.setRenderLoginServerPanel(true);
				String arr[] = null;
				newOrganizationBean.setRenderLADP(false);
				for (Iterator<Map.Entry<String, SocialAppSettingTO>> it = resTo.getSocialAppSettingTOMap().entrySet()
						.iterator(); it.hasNext();) {
					newOrganizationBean.setRenderLoginServerPanel(true);
					Map.Entry<String, SocialAppSettingTO> entry = it.next();
					if (entry.getKey().contains(CoreConstants.LOGIN_SERVER_LDAP)) {
						if (entry.getValue().getIsdefault() != null && entry.getValue().getIsdefault()) {
							newOrganizationBean.setDefaultLoginServer(entry.getValue().getSocialAppType());
							defaultServer = true;
						}

						if (entry.getValue().isActive()) {
							selectedLoginServerList.add(entry.getValue().getSocialAppType());
							arr = entry.getKey().split(CoreConstants.AT_SPERATOR);
							selectedLoginServerTypes.add(arr[1]);
							newOrganizationBean.setRenderLADP(true);
						}
						it.remove();
					} else if (entry.getKey().contains(CoreConstants.LOGIN_SERVER_SP)) {
						if (entry.getValue().getIsdefault() != null && entry.getValue().getIsdefault()) {
							newOrganizationBean.setDefaultLoginServer(entry.getValue().getSocialAppType());
							defaultServer = true;
						}

						if (entry.getValue().isActive()) {
							selectedLoginServerList.add(entry.getValue().getSocialAppType());
							arr = entry.getKey().split(CoreConstants.AT_SPERATOR);
							selectedLoginServerTypes.add(arr[1]);
							newOrganizationBean.setRenderSP(true);
							newOrganizationBean.setSpServer(resTo.getSpServer());
							newOrganizationBean.setSpLogoutUrl(resTo.getSpLogoutUrl());
							newOrganizationBean.setSpActive(resTo.isSpActive());
						}
						it.remove();
					} else {
						if (entry.getValue().isActive()) {
							selectedLoginServerList.add(entry.getValue().getSocialAppType());
							arr = entry.getKey().split(CoreConstants.AT_SPERATOR);
							selectedLoginServerTypes.add(arr[1]);
							socialAppSettingTOList.add(entry.getValue());
						}

					}
				}

				newOrganizationBean.setSocialAppSettingTOMap(resTo.getSocialAppSettingTOMap());
				newOrganizationBean.setSelectedLoginServerTypes(selectedLoginServerTypes);
				newOrganizationBean.setSelectedLoginServerList(selectedLoginServerList);
				newOrganizationBean.setSocialAppSettingTOList(socialAppSettingTOList);
				if (!resTo.getSocialAppSettingTOMap().isEmpty())
					newOrganizationBean.setEditLoginServer(true);
				else
					newOrganizationBean.setEditLoginServer(false);

			}
			if (!defaultServer) {
				newOrganizationBean.setDefaultLoginServer(CoreConstants.NONE_STRING);
			}

			menuBean.setPage("../neworganization.xhtml");

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			RequestContext.getCurrentInstance().execute("scrollUp()");
		}
	}

	private List<String> getActiveApps(List<HrAppTO> selectedApps) {
		// get active selected apps
		List<String> activeApps = new ArrayList<>();
		for (HrAppTO app : selectedApps) {

			if (app.getIsActive() == true) {
				activeApps.add(String.valueOf(app.getAppId()));
				selectedActiveAppIdsBeforeUpdate.add(String.valueOf(app.getAppId()));
			} else {
				selectedInactiveAppIdsBeforeUpdate.add(String.valueOf(app.getAppId()));
			}
		}
		return activeApps;
	}

	/**
	 * getting all sys role type list
	 * 
	 * @param resTO
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked" })
	public void getCheckListDetail(CheckListResponseTO resTO){
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<SysRoleTypeTO> sysRoleTypeTOList = null;
		List<SysMenuTO> sysMenuTOList = null;
		List<SysContentTypeTO> sysContentTypeTOList = null;
		SysRoleRequestTO sysRoleRequestTO = null;
		HrModuleRequestTO hrModuleRequestTO = null;
		List<HrModuleTO> hrModuleTOList = null;
		String persistenceUnitCode = null;
		SysMenuRequestTO sysMenuRequestTO = null;
		Map<Long, List<SysMenuTO>> menuMap = null;
		TreeNode menuTree = null;
		List<UiFormResponseTO> sysCopyUITOList = null;
		SysProductAnnouncementRequestTO sysProductAnnouncementRequestTO = null;
		List<SysProductAnnouncementTO> sysProductAnnouncementTOList = null;

		try {
			userByTenant();
			if (resTO.getItemName().equals("COPY_ROLES")) {
				// -----------
				sysRoleRequestTO = new SysRoleRequestTO();
				sysRoleRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				sysRoleRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				sysRoleRequestTO.setTenantID(newOrganizationBean.getTenantID());
				sysRoleRequestTO.setCreatedBy(userSessionBean.getUserID());
				sysRoleRequestTO.setModifiedBy(userSessionBean.getUserID());
				persistenceUnitCode = IConstants.DATABASE_METADATA_START_KEY + IConstants.ORGANIZE_CODE
						+ IConstants.UNDERSCORE + newOrganizationBean.getTenantID();
				// ---------
				serviceRequest = new ServiceRequest();
				serviceRequest.setRequestTansportObject(sysRoleRequestTO);
				serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
				serviceResponse = iCheckListFacadeRemote.getAllSysRoleType(serviceRequest);
				sysRoleTypeTOList = (List<SysRoleTypeTO>) serviceResponse.getExtendedResponseTransportObjectList();
				newOrganizationBean.setSysRoleTypeList(sysRoleTypeTOList);
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyRoleForm");
				context.execute("PF('copyRoleDialog').show();");

			}
			if (resTO.getItemName().equals("COPY_MENU")) {

				sysMenuRequestTO = new SysMenuRequestTO();
				sysMenuRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				sysMenuRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				sysMenuRequestTO.setTenantID(newOrganizationBean.getTenantID());
				sysMenuRequestTO.setCreatedBy(userSessionBean.getUserID());
				sysMenuRequestTO.setModifiedBy(userSessionBean.getUserID());

				// ------
				serviceRequest = new ServiceRequest();
				serviceRequest.setRequestTansportObject(sysMenuRequestTO);
				serviceResponse = iCheckListFacadeRemote.getAllSysMenu(serviceRequest);
				sysMenuTOList = (List<SysMenuTO>) serviceResponse.getExtendedResponseTransportObjectList();
				newOrganizationBean.setSysMenuList(sysMenuTOList);
				menuMap = OrganizationHelper.prepareMenuListForCache(sysMenuTOList);
				// -----filter header menus on basis of moduleID
				OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = newOrganizationBean.getOrgAppPortalTreeTO();
				Map<Long, List<TpModuleResponseTO>> appModuleMap = orgAppPortalTreeResponseTO.getAppModuleMap();
				OrganizationHelper.RemoveNonModuleHeaderMenus(appModuleMap, menuMap);

				menuTree = OrganizationHelper.createMenuTree(menuMap);
				newOrganizationBean.setMenuTree(menuTree);
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyMenuForm");
				context.execute("PF('copyMenuDialog').show();");
			}
			if (resTO.getItemName().equals("CREATE_ADMIN")) {
				RequestContext context = RequestContext.getCurrentInstance();
				newOrganizationBean.setStatus(null);
				newOrganizationBean.setUserName(null);
				context.update("createAdminForm");
				context.execute("PF('createAdminDialog').show();");
			}
			if (resTO.getItemName().equals("COPY_CONTENT_TYPE")) {
				serviceRequest = new ServiceRequest();
				ContentListRequestTO contentListRequestTO = new ContentListRequestTO();
				contentListRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				serviceRequest.setRequestTansportObject(contentListRequestTO);
				serviceResponse = iCheckListFacadeRemote.getAllSysContentType(serviceRequest);
				sysContentTypeTOList = (List<SysContentTypeTO>) serviceResponse
						.getExtendedResponseTransportObjectList();
				newOrganizationBean.setSysContentTypeList(sysContentTypeTOList);
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyContentForm");
				context.execute("PF('copyContentDialog').show();");
			}

			if (resTO.getItemName().equals("COPY_FORM")) {
				// newOrganizationBean.setSysCopyUIList(newOrganizationBean.getSysCopyUIList1());
				serviceRequest = new ServiceRequest();
				UIFormRequestTO uIFormListRequestTO = new UIFormRequestTO();
				List<TpModuleResponseTO> tpModuleResponseTOList = new ArrayList<TpModuleResponseTO>();

				List<Long> moduleIDList = new ArrayList<Long>();
				Map<Long, List<TpModuleResponseTO>> appModuleMap1 = null;
				OrgAppPortalTreeResponseTO orgAppPortalTreeResponseTO = newOrganizationBean.getOrgAppPortalTreeTO();
				appModuleMap1 = orgAppPortalTreeResponseTO.getAppModuleMap();
				for (Object key : appModuleMap1.keySet()) {
					tpModuleResponseTOList.addAll(appModuleMap1.get(key));
				}
				for (TpModuleResponseTO tp : tpModuleResponseTOList) {
					if (tp.isModuleChecked() == true) {
						moduleIDList.add(tp.getModuleID());
					}
				}
				if (moduleIDList.isEmpty()) {
					FacesContext.getCurrentInstance().addMessage("activity", new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "Please assign at least one Module to the organization ", ""));
					return;
				} else {

					uIFormListRequestTO.setModuleIDList(moduleIDList);

					uIFormListRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					uIFormListRequestTO.setTenantID(newOrganizationBean.getTenantID().intValue());
					serviceRequest.setRequestTansportObject(uIFormListRequestTO);
					serviceResponse = iCheckListFacadeRemote.getAllUIForm(serviceRequest);
					sysCopyUITOList = (List<UiFormResponseTO>) serviceResponse.getExtendedResponseTransportObjectList();
					for (UiFormResponseTO uto : sysCopyUITOList) {
						if (uto.getHrFormID() != null) {
							uto.setAlreadyCopied(true);
						}
						if (uto.isAlreadyCopied())
							uto.setSelected(true);
					}
					newOrganizationBean.setSysCopyUIList(sysCopyUITOList);
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('copyFormWidgetVar').filter();");
					context.update("copyUIForm");
					// context.execute("PF('copyUIFormDialog').clearFilters();");
					context.execute("PF('copyUIFormDialog').show();");
				}

			}
			if (resTO.getItemName().equals("COPY_FORM_SECURITY")) {
				// -----------
				hrModuleRequestTO = new HrModuleRequestTO();

				hrModuleRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				hrModuleRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				hrModuleRequestTO.setTenantID(newOrganizationBean.getTenantID());
				hrModuleRequestTO.setCreatedBy(userSessionBean.getUserID());
				hrModuleRequestTO.setModifiedBy(userSessionBean.getUserID());

				// ---------
				serviceRequest = new ServiceRequest();
				serviceRequest.setRequestTansportObject(hrModuleRequestTO);
				serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
				serviceResponse = iCheckListFacadeRemote.getOrganizationModuleData(serviceRequest);

				hrModuleTOList = (List) serviceResponse.getResponseTransferObjectObjectList();
				newOrganizationBean.setHrModuleList(hrModuleTOList);

				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyFormSecurity");
				context.execute("PF('copyFormSecurityDialog').show();");

			}
			if (resTO.getItemName().equals("COPY_VENDOR_OFFERING")) {
				serviceRequest = new ServiceRequest();
				AltBenefitRequestTO benefitRequestTO = new AltBenefitRequestTO();
				benefitRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				benefitRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				benefitRequestTO.setTenantID(newOrganizationBean.getTenantID());
				benefitRequestTO.setCreatedBy(userSessionBean.getUserID());
				benefitRequestTO.setModifiedBy(userSessionBean.getUserID());
				serviceRequest.setRequestTansportObject(benefitRequestTO);
				serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
				ServiceResponse response = iCheckListFacadeRemote.getAllOfferingTemplateList(serviceRequest);
				if (response != null) {
					AltBenefitResponseTO benefitResponseTO = (AltBenefitResponseTO) response
							.getResponseTransportObject();
					if (benefitResponseTO != null) {
						newOrganizationBean
								.setVendorOfferingTemplateTOList(benefitResponseTO.getVendorOfferingTemplateTOList());
					}
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyVendorOffering");
				context.execute("PF('copyVendorOfferingDialog').show();");

			}
			if (resTO.getItemName().equals("COPY_DASHBOARD")) {
				serviceRequest = new ServiceRequest();
				AltBenefitRequestTO benefitRequestTO = new AltBenefitRequestTO();
				benefitRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				benefitRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				benefitRequestTO.setTenantID(newOrganizationBean.getTenantID());
				benefitRequestTO.setCreatedBy(userSessionBean.getUserID());
				benefitRequestTO.setModifiedBy(userSessionBean.getUserID());
				serviceRequest.setRequestTansportObject(benefitRequestTO);
				serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
				List<DashBoardTO> dashBoardTOlist = dashBoardService.getDashBoardsForOrg(serviceRequest);
				newOrganizationBean.setDashBoardTOlist(dashBoardTOlist);
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyDashBoardForm");// copyDashBoardForm
				context.execute("PF('copyDashBoardDialog').show();");
			}

			if (resTO.getItemName().equals("COPY_CHART_DASHBOARD")) {
				DashboardChartResponseTO dashboardChartResponseTO = organizationService.getPortalDetails();
				if (dashboardChartResponseTO != null) {
					newOrganizationBean.setPortalList(dashboardChartResponseTO.getPortalList());
				}
				newOrganizationBean.setShowChartDataList(false);
				newOrganizationBean.setSelectedPortalID(1);
				refreshDashboardChart();
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyDashBoardChartForm");// copyDashBoardChartForm
				context.execute("PF('copyDashBoardChartDialog').show();");
			}

			if (resTO.getItemName().equals("COPY_PRODUCT_ANNOUNCEMENT")) {
				// -----------
				sysProductAnnouncementRequestTO = new SysProductAnnouncementRequestTO();

				sysProductAnnouncementRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				sysProductAnnouncementRequestTO.setSysCheckListID(resTO.getSysCheckListID());
				sysProductAnnouncementRequestTO.setTenantID(newOrganizationBean.getTenantID());
				sysProductAnnouncementRequestTO.setCreatedBy(userSessionBean.getUserID());
				sysProductAnnouncementRequestTO.setModifiedBy(userSessionBean.getUserID());
				persistenceUnitCode = IConstants.DATABASE_METADATA_START_KEY + IConstants.ORGANIZE_CODE
						+ IConstants.UNDERSCORE + newOrganizationBean.getTenantID();
				// ---------
				serviceRequest = new ServiceRequest();
				serviceRequest.setRequestTansportObject(sysProductAnnouncementRequestTO);
				serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
				serviceResponse = iCheckListFacadeRemote.getAllSysProductAnnouncement(serviceRequest);
				sysProductAnnouncementTOList = (List<SysProductAnnouncementTO>) serviceResponse
						.getExtendedResponseTransportObjectList();
				newOrganizationBean.setSysProductAnnouncementTOList(sysProductAnnouncementTOList);
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("copyProductAnnouncementForm");
				context.execute("PF('copyProductAnnouncementDialog').show();");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void getSysMenuList(CheckListResponseTO resTO) throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<SysMenuTO> sysMenuTOList = null;
		try {
			serviceRequest = new ServiceRequest();
			serviceResponse = iCheckListFacadeRemote.getAllSysMenu(serviceRequest);
			sysMenuTOList = (List<SysMenuTO>) serviceResponse.getExtendedResponseTransportObjectList();
			newOrganizationBean.setSysMenuList(sysMenuTOList);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	public void copySysRoleList() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		SysRoleRequestTO sysRoleRequestTO = null;
		CheckListResponseTO checkResTO = null;
		String persistenceUnitCode = null;
		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			System.out.println(checkListItem);
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					sysRoleRequestTO = new SysRoleRequestTO();
					sysRoleRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					sysRoleRequestTO.setSysCheckListID(resTO.getSysCheckListID());
					sysRoleRequestTO.setTenantID(newOrganizationBean.getTenantID());
					sysRoleRequestTO.setCreatedBy(userSessionBean.getUserID());
					sysRoleRequestTO.setModifiedBy(userSessionBean.getUserID());
					checkResTO = resTO;
					break;

				}
			}
			// for sub action of copy_menu start here

			persistenceUnitCode = IConstants.DATABASE_METADATA_START_KEY + IConstants.ORGANIZE_CODE
					+ IConstants.UNDERSCORE + newOrganizationBean.getTenantID();
			sysRoleRequestTO.setSysRoleTypeTolist(newOrganizationBean.getSysRoleTypeList());
			serviceRequest = new ServiceRequest();
			serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
			serviceRequest.setRequestTansportObject(sysRoleRequestTO);
			serviceResponse = iCheckListFacadeRemote.copySysRoleList(serviceRequest);
			checkResTO.setSelected(true);
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Roles copied Succesfully ", ""));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			persistenceUnitCode = null;
		}
	}

	@SuppressWarnings("unused")
	public void copyContentList() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		HrContentRequestTO hrContentRequestTO = null;
		CheckListResponseTO checkResTO = null;

		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			System.out.println(checkListItem);
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					hrContentRequestTO = new HrContentRequestTO();
					hrContentRequestTO.setCategoryName(newOrganizationBean.getCategoryName());
					hrContentRequestTO.setContentCategoryID(newOrganizationBean.getContentCategoryID());
					hrContentRequestTO.setContentName(newOrganizationBean.getContentName());
					hrContentRequestTO.setCreatedBy(userSessionBean.getUserID());
					hrContentRequestTO.setModifiedBy(userSessionBean.getUserID());
					checkResTO = resTO;
					break;

				}
			}
			// for sub action of copy_menu start here
			hrContentRequestTO.setSysContentTypeTOlist(newOrganizationBean.getSysContentTypeList());
			serviceRequest = new ServiceRequest();
			serviceRequest.setRequestTansportObject(hrContentRequestTO);
			serviceResponse = iCheckListFacadeRemote.getAllSysContentType(serviceRequest);
			checkResTO.setSelected(true);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void copySysDashBoardList() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		CopyDashBoardReqTO copyDashBoardReqTO = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		CheckListResponseTO checkResTO = null;
		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			System.out.println(checkListItem);
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					checkResTO = resTO;
					break;

				}
			}

			serviceRequest = new ServiceRequest();
			copyDashBoardReqTO = new CopyDashBoardReqTO();
			copyDashBoardReqTO.setOrganizationID(newOrganizationBean.getOrgId());
			copyDashBoardReqTO.setSysCheckListID(checkResTO.getSysCheckListID());
			copyDashBoardReqTO.setTenantID(newOrganizationBean.getTenantID());
			copyDashBoardReqTO.setCreatedBy(userSessionBean.getUserID());
			copyDashBoardReqTO.setModifiedBy(userSessionBean.getUserID());
			copyDashBoardReqTO.setDashBoardTOList(newOrganizationBean.getDashBoardTOlist());
			serviceRequest.setRequestTansportObject((IRequestTansportObject) copyDashBoardReqTO);
			serviceResponse = dashBoardService.saveCopyDashBoard(serviceRequest);
			checkResTO.setSelected(true);
			FacesContext.getCurrentInstance().addMessage("activity", new FacesMessage(FacesMessage.SEVERITY_INFO,
					"DashBoard items have been copied successfullly.", ""));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Boolean getCreateNewOrg() {
		return this.createNewOrg;
	}

	public void setCreateNewOrg(Boolean createNewOrg) {
		this.createNewOrg = createNewOrg;
	}

	private void populateConstants(Map<String, List<String>> constantValues) throws Exception {
		if (constantValues != null && constantValues.size() > 0) {
			List<SysConstantAndSysConstantCategoryTO> loginPage1 = iNewOrganizationFacadeRemote
					.getSysConstantDropDown();
			List<SelectItem> selectLoginPageList1 = OrganizationHelper
					.convertValuesIntoSelectItemTypeForLoginPage(loginPage1);
			newOrganizationBean.setSelectLoginPageList(selectLoginPageList1);
			// List<String> loginPageList =
			// constantValues.get(IConstants.Login_Page);
			// List<SelectItem> selectLoginPageList =
			// OrganizationHelper.convertValuesIntoSelectItemType(loginPageList);

			// newOrganizationBean.setSelectLoginPageList(selectLoginPageList);

			List<String> homePageList = constantValues.get(IConstants.Home_Page);
			List<SelectItem> selectHomePageList = OrganizationHelper.convertValuesIntoSelectItemType(homePageList);

			newOrganizationBean.setSelectHomePageList(selectHomePageList);
			//
			List<SysConstantAndSysConstantCategoryTO> loginPageTheme = iNewOrganizationFacadeRemote
					.getSysConstantDropDown();
			// List<SysConstantAndSysConstantCategoryTO> loginPage1 =
			// iNewOrganizationFacadeRemote.getSysConstantDropDown();
			// List<SelectItem> selectLoginPageList1 =
			// OrganizationHelper.convertValuesIntoSelectItemTypeForLoginPage(loginPage1);
			// newOrganizationBean.setSelectLoginPageList(selectLoginPageList1);
			//
			// List<String> loginPageTheme =
			// constantValues.get(IConstants.Login_Page_Theme);
			// List<SelectItem> selectloginPageThemeList =
			// OrganizationHelper.convertValuesIntoSelectItemType(loginPageTheme);
			List<SelectItem> selectloginPageThemeList = OrganizationHelper
					.convertValuesIntoSelectItemTypeForLoginPageTheme(loginPageTheme);

			newOrganizationBean.setSelectloginPageThemeList(selectloginPageThemeList);

			List<String> defualtThemeList = constantValues.get(IConstants.Default_Theme);
			List<SelectItem> selectdefualtThemeList = OrganizationHelper
					.convertValuesIntoSelectItemType(defualtThemeList);

			newOrganizationBean.setSelectdefualtThemeList(selectdefualtThemeList);

			// getting list of ufirom
			List<UiFormTO> selectLandingePagList = iNewOrganizationFacadeRemote.getLandingPageList(0L);
			newOrganizationBean.setSelectLandingePagList(selectLandingePagList);

		}

	}

	@SuppressWarnings("unused")
	public void copySysMenuList() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		SysMenuRequestTO sysMenuRequestTO = null;
		CheckListResponseTO checkResTO = null;
		List<SysMenuTO> sysMenuTOlist = null;
		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			System.out.println(checkListItem);
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					sysMenuRequestTO = new SysMenuRequestTO();
					sysMenuRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					sysMenuRequestTO.setSysCheckListID(resTO.getSysCheckListID());
					sysMenuRequestTO.setTenantID(newOrganizationBean.getTenantID());
					sysMenuRequestTO.setCreatedBy(userSessionBean.getUserID());
					sysMenuRequestTO.setModifiedBy(userSessionBean.getUserID());
					checkResTO = resTO;
					break;

				}
			}
			// for sub action of copy_menu start here

			TreeNode[] selectedMenuNodes = newOrganizationBean.getSelectedMenuNodes();
			sysMenuTOlist = new ArrayList<SysMenuTO>();
			OrganizationHelper.getSysMenuTOlistFromMenuTree(newOrganizationBean.getMenuTree(), sysMenuTOlist,
					selectedMenuNodes);

			sysMenuRequestTO.setSysMenuTOList(sysMenuTOlist);
			serviceRequest = new ServiceRequest();
			serviceRequest.setRequestTansportObject(sysMenuRequestTO);
			serviceResponse = iCheckListFacadeRemote.copySysMenuList(serviceRequest);
			checkResTO.setSelected(true);
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Menus copied Succesfully ", ""));

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			serviceRequest = null;
			serviceResponse = null;
			listCheckListResponseTO = null;
			sysMenuRequestTO = null;
			checkResTO = null;
		}
	}

	// public void createNewSurveyUser(ServiceResponse serviceResponse)
	// throws Exception
	// {
	//
	// if (serviceResponse == null) {
	// return;
	// }
	//
	// ServiceRequest serviceRequest;
	// ServiceResponse userCreatedServiceResponse;
	// NewUserRequestTO newUserRequestTO = null;
	//
	// TpUserCreatedResponseTO tpUserCreatedResponseTO = null;
	// try {
	//
	// serviceRequest = new ServiceRequest();
	// tpUserCreatedResponseTO = (TpUserCreatedResponseTO)
	// serviceResponse.getResponseTransferObject();
	//
	// newUserRequestTO = new NewUserRequestTO();
	// newUserRequestTO.setSurveyUserID(tpUserCreatedResponseTO.getSurveyUserID());
	// newUserRequestTO.setUserName(tpUserCreatedResponseTO.getUserName());
	// newUserRequestTO.setTenantID(tpUserCreatedResponseTO.getTenantID());
	// newUserRequestTO.setOrganizationID(tpUserCreatedResponseTO.getOrganizationID());
	// newUserRequestTO.setAdminEnabled(true);
	//
	// serviceRequest.setRequestTransferObjectRevised(newUserRequestTO);
	// userCreatedServiceResponse =
	// newUserFacadeRemote.createSurveyUser(serviceRequest);
	// } catch (Exception ex) {
	// throw ex;
	// }
	// }

	public void saveUserNamePassword() {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		UserRequestTO user = null;
		String userName = null;
		Boolean isUserNameExist = null;
		RequestContext context = null;
		CheckListResponseTO checkResTO = null;
		OrganizationResponseTO resTo = null;
		String companyName = null;
		String userN = null;
		String password = null;
		String status = null;
		String token = null;
		String userInfoForWebPay = null;
		String firstSixCharCompanyName = null;
		String firstSixCharUserName = null;
		try {
			resTo = newOrganizationBean.getOrgTO();
			firstSixCharCompanyName = resTo.getOrgCode();
			userN = newOrganizationBean.getUserName().trim();

			if (userN.length() >= 6) {
				firstSixCharUserName = userN.substring(0, Math.min(userN.length(), 6));
			} else {
				firstSixCharUserName = userN;
			}
			password = newOrganizationBean.getPassword();
			status = newOrganizationBean.getStatus().toString();
			token = "abc";
			// userInfoForWebPay = firstSixCharCompanyName + "~"
			// +firstSixCharUserName+"~" + userN +"~"+ userN + "~" + password +
			// "~" + status + "~" + token;
			userInfoForWebPay = firstSixCharCompanyName + "~" + firstSixCharUserName + "~" + userN + "~" + userN + "~"
					+ password;
			userByTenant();
			checkResTO = new CheckListResponseTO();
			context = RequestContext.getCurrentInstance();
			userName = newOrganizationBean.getUserName().trim();
			isUserNameExist = false;
			/**
			 * checking whether a user of this name exist or not
			 */
			if (userName != null && !userName.equals("") && newOrganizationBean.getUserTOList() != null) {
				isUserNameExist = UserListHelper.checkUserNameinList(userName, newOrganizationBean.getUserTOList());
			}
			if (isUserNameExist == false) {
				user = new UserRequestTO();
				user.setStatusID(newOrganizationBean.getStatus());
				user.setUserName(userName);
				user.setPassword(newOrganizationBean.getPassword());
				serviceRequest = new ServiceRequest();
				serviceRequest.setRequestTansportObject(user);
				user.setOrganizationID(newOrganizationBean.getOrgId());
				user.setTenantID(newOrganizationBean.getTenantID());
				user.setUserID(userSessionBean.getUserID());
				serviceResponse = iCheckListFacadeRemote.setUserListByOrganization(serviceRequest);
				// createNewSurveyUser(serviceResponse);
				userByTenant();
				context.update("createAdminForm");
				checkResTO.setSelected(true);
				FacesContext.getCurrentInstance().addMessage("newAdmin",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Admin created Succesfully ", ""));
			} else {
				FacesContext.getCurrentInstance().addMessage("newAdmin", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"User with name '" + newOrganizationBean.getUserName().trim() + "' already exists", ""));
				return;
			}
			if (resTo.getWebPayStatus() == true) {
				for (List<TpModuleResponseTO> value : resTo.getOrgAppPortalTreeResponseTO().getAppModuleMap()
						.values()) {
					for (TpModuleResponseTO to : value) {
						if (to.getModuleName().equalsIgnoreCase("Payroll") && to.isModuleChecked() == true) {
							// create HTTP Client
							HttpClient httpClient = HttpClientBuilder.create().build();

							// Create new getRequest with below mentioned URL
							HttpGet getRequest = new HttpGet(
									"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/UserCreation?strDBName="
											+ firstSixCharCompanyName + "&userid=" + firstSixCharUserName + "&email="
											+ userN + "&name=" + userN + "&Pwd=" + password + "&status=" + status);

							// Execute your request and catch response
							HttpResponse response = httpClient.execute(getRequest);

							// Check for HTTP response code: 200 = success
							if (response.getStatusLine().getStatusCode() != 200) {
								throw new RuntimeException(
										"Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
							}

							// Get-Capture Complete body response
							BufferedReader br = new BufferedReader(
									new InputStreamReader((response.getEntity().getContent())));
							String output;
							System.out.println("============Output:============");

							// Simply iterate response and show on console.
							while ((output = br.readLine()) != null) {
								System.out.println(output);
							}
						}
					}
				}
			}
		} catch (Exception ex) {

		}
	}

	public void userByTenant() {
		ServiceRequest serviceRequest = null;
		ServiceResponse response = null;
		UserRequestTO user = null;
		try {
			user = new UserRequestTO();
			serviceRequest = new ServiceRequest();
			user.setOrganizationID(newOrganizationBean.getOrgId());
			user.setTenantID(newOrganizationBean.getTenantID());
			serviceRequest.setRequestTansportObject(user);
			response = iCheckListFacadeRemote.getUserByTenant(serviceRequest);

			List<UserResponseTO> adminUserTOList = new ArrayList<UserResponseTO>();
			if (response.getListResponseTransportObject() == null
					|| response.getListResponseTransportObject().isEmpty()) {
				adminUserTOList = null;
			} else {
				for (IResponseTransportObject obj : response.getListResponseTransportObject()) {
					UserResponseTO userObj = (UserResponseTO) obj;
					adminUserTOList.add(userObj);
				}
			}
			newOrganizationBean.setUserTOList(adminUserTOList);

		} catch (Exception ex) {
		} finally {

		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void copyVendorOfferings() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		AltBenefitRequestTO benefitRequestTO = null;
		CheckListResponseTO checkResTO = null;

		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					serviceRequest = new ServiceRequest();
					benefitRequestTO = new AltBenefitRequestTO();
					benefitRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					benefitRequestTO.setSysCheckListID(resTO.getSysCheckListID());
					benefitRequestTO.setTenantID(newOrganizationBean.getTenantID());
					benefitRequestTO.setCreatedBy(userSessionBean.getUserID());
					benefitRequestTO.setModifiedBy(userSessionBean.getUserID());
					checkResTO = resTO;
					break;
				}
			}
			benefitRequestTO.setVendorOfferingTemplateTOList(newOrganizationBean.getVendorOfferingTemplateTOList());
			serviceRequest.setRequestTansportObject(benefitRequestTO);
			iCheckListFacadeRemote.copyAllOfferingTemplateList(serviceRequest);
			checkResTO.setSelected(true);
			hrVendorOfferingSequenceService.getAllHrOfferingTOList();
			hrVendorOfferingSequenceService.getAllEmpGroupBasedOfferingList();
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Vendor Offering copied Succesfully ", ""));
		} catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error in copy Vendor Offering.", ""));
		}

	}

	public void copyProductAnnouncement() throws Exception {
		ServiceRequest serviceRequest = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		CheckListResponseTO checkResTO = null;
		SysProductAnnouncementRequestTO sysProductAnnouncementRequestTO = null;
		try {

			if (validateValidDates(newOrganizationBean.getSysProductAnnouncementTOList())) {

				Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
						.getRequestParameterMap();
				String checkListItem = params.get("checkListItem");
				System.out.println(checkListItem);
				listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
				for (CheckListResponseTO resTO : listCheckListResponseTO) {
					if (resTO.getItemName().equals(checkListItem)) {
						checkResTO = resTO;
						break;
					}
				}
				sysProductAnnouncementRequestTO = new SysProductAnnouncementRequestTO();
				serviceRequest = new ServiceRequest();
				sysProductAnnouncementRequestTO
						.setSysProductAnnouncementTOList(newOrganizationBean.getSysProductAnnouncementTOList());
				sysProductAnnouncementRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
				sysProductAnnouncementRequestTO.setTenantID(newOrganizationBean.getTenantID());
				sysProductAnnouncementRequestTO.setSysCheckListID(checkResTO.getSysCheckListID());

				serviceRequest.setRequestTansportObject(sysProductAnnouncementRequestTO);
				iCheckListFacadeRemote.copyHrProductAnnouncement(serviceRequest);
				checkResTO.setSelected(true);
				RequestContext.getCurrentInstance().execute("PF('copyProductAnnouncementDialog').hide();");
				FacesContext.getCurrentInstance().addMessage("activity",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Product Announement copied Succesfully", ""));
				RequestContext.getCurrentInstance().update("tabView:activityTabForm");

			} else {
				FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"ValidFrom cannot be greater than ValidTO date", ""));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("messages",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error in copying Product Announcement.", ""));
		}
	}

	private boolean validateValidDates(List<SysProductAnnouncementTO> sysProductAnnouncementTOList) {
		int i = 0;
		for (SysProductAnnouncementTO sysProductAnnouncementTO : sysProductAnnouncementTOList) {
			if (sysProductAnnouncementTO.getValidFrom().compareTo(sysProductAnnouncementTO.getValidTo()) > 0) {
				i = 1;
				break;
			}
		}

		if (i == 1) {
			return false;
		} else {
			return true;
		}

	}

	@SuppressWarnings("unused")
	public void copyUIFormList() throws Exception {
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		UIFormRequestTO uIFormRequestTO = null;
		CheckListResponseTO checkResTO = null;
		String persistenceUnitCode = null;
		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			System.out.println(checkListItem);
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					uIFormRequestTO = new UIFormRequestTO();
					uIFormRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					// uIFormRequestTO.setSysCheckListID(resTO.getSysCheckListID());
					uIFormRequestTO.setFormID(newOrganizationBean.getFormID());
					uIFormRequestTO.setCreatedBy(userSessionBean.getUserID().intValue());
					uIFormRequestTO.setModifiedBy(userSessionBean.getUserID().intValue());
					uIFormRequestTO.setTenantID(newOrganizationBean.getTenantID().intValue());
					checkResTO = resTO;
					break;

				}
			}
			// for sub action of copy_menu start here

			persistenceUnitCode = IConstants.DATABASE_METADATA_START_KEY + IConstants.ORGANIZE_CODE
					+ IConstants.UNDERSCORE + newOrganizationBean.getTenantID();
			uIFormRequestTO.setUiFormTOlist(newOrganizationBean.getSysCopyUIList());
			serviceRequest = new ServiceRequest();
			serviceRequest.setPersistenceUnitKey(persistenceUnitCode);
			serviceRequest.setRequestTansportObject(uIFormRequestTO);
			serviceResponse = iCheckListFacadeRemote.copyUIFormList(serviceRequest);
			checkResTO.setSelected(true);
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "UI Form copied Succesfully ", ""));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			persistenceUnitCode = null;
		}
	}

	public void changeDatabaseOnTenantChange() throws Exception {
		int tenantId = newOrganizationBean.getTenantID().intValue();
		selectDatabaseForTenantId(tenantId, newOrganizationBean.getOrgAppPortalTreeTO());
		selectDateForTenantId(tenantId, newOrganizationBean.getOrgAppPortalTreeTO());
	}

	public void getOrgMoreInfoList(AppPortalTypeUrlResponseTO portal) {
		try {
			newOrganizationBean.setSelectedPortal(portal);
			newOrganizationBean.setLeftLogo(newOrganizationBean.getSelectedPortal().getLeftLogo());
			newOrganizationBean.setRightLogo(newOrganizationBean.getSelectedPortal().getRightLogo());
			newOrganizationBean.setFooter(newOrganizationBean.getSelectedPortal().getFooter());
			newOrganizationBean.setTitle(newOrganizationBean.getSelectedPortal().getTitle());
			newOrganizationBean.setUsernameTxt(newOrganizationBean.getSelectedPortal().getUsernameTxt());
			newOrganizationBean.setBanner(newOrganizationBean.getSelectedPortal().getBanner());
			newOrganizationBean.setBtnTxt(newOrganizationBean.getSelectedPortal().getBtnTxt());
			newOrganizationBean.setSamlBtnTxt(newOrganizationBean.getSelectedPortal().getSamlBtnTxt());
			newOrganizationBean.setSamlHeader(newOrganizationBean.getSelectedPortal().getSamlHeader());
			newOrganizationBean.setHeadingTxt(newOrganizationBean.getSelectedPortal().getHeadingTxt());
			newOrganizationBean.setMetaData(newOrganizationBean.getSelectedPortal().getMetaData());
			newOrganizationBean.setResetPassword(newOrganizationBean.getSelectedPortal().getResetPassword());
			newOrganizationBean.setPasswordTxt(newOrganizationBean.getSelectedPortal().getPasswordTxt());
			newOrganizationBean.setMobileStatus(newOrganizationBean.getSelectedPortal().getMobileStatus());
			newOrganizationBean.setBackGroundImage(newOrganizationBean.getSelectedPortal().getBackGroundImage());
			newOrganizationBean.setContactNumber(newOrganizationBean.getSelectedPortal().getContactNumber());
			newOrganizationBean.seteMail(newOrganizationBean.getSelectedPortal().geteMail());
			newOrganizationBean.setExternalVideoPath(newOrganizationBean.getSelectedPortal().getExternalVideoPath());
			if (newOrganizationBean.getSelectedPortal().getOnBoardingVideoLinkList() != null
					&& !newOrganizationBean.getSelectedPortal().getOnBoardingVideoLinkList().isEmpty()) {
				newOrganizationBean.setOnBoardingVideoLinkList(
						newOrganizationBean.getSelectedPortal().getOnBoardingVideoLinkList());
			} else {
				newOrganizationBean.setOnBoardingVideoLinkList(null);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setValuesOnSelectedPortal() {
		AppPortalTypeUrlResponseTO portal = newOrganizationBean.getSelectedPortal();

		portal.setLeftLogo(newOrganizationBean.getLeftLogo());

		portal.setRightLogo(newOrganizationBean.getRightLogo());
		portal.setFooter(newOrganizationBean.getFooter());
		portal.setTitle(newOrganizationBean.getTitle());
		portal.setUsernameTxt(newOrganizationBean.getUsernameTxt());
		portal.setBanner(newOrganizationBean.getBanner());
		portal.setBtnTxt(newOrganizationBean.getBtnTxt());
		portal.setSamlBtnTxt(newOrganizationBean.getSamlBtnTxt());
		portal.setSamlHeader(newOrganizationBean.getSamlHeader());
		portal.setHeadingTxt(newOrganizationBean.getHeadingTxt());
		portal.setResetPassword(newOrganizationBean.getResetPassword());
		portal.setPasswordTxt(newOrganizationBean.getPasswordTxt());
		portal.setMobileStatus(newOrganizationBean.getMobileStatus());
		portal.setBackGroundImage(newOrganizationBean.getBackGroundImage());
		portal.setContactNumber(newOrganizationBean.getContactNumber());
		portal.setExternalVideoPath(newOrganizationBean.getExternalVideoPath());
		portal.seteMail(newOrganizationBean.geteMail());
		portal.setOnBoardingVideoLinkList(newOrganizationBean.getOnBoardingVideoLinkList());
	}

	public void getCountryDropDown() throws Exception {
		AddressResponseTO addressResponseTO = new AddressResponseTO();
		List<AddressResponseTO> serviceResponse = null;
		try {
			serviceResponse = iNewOrganizationFacadeRemote.getCountryDropDown();
			if (serviceResponse != null && !serviceResponse.isEmpty())
				newOrganizationBean.setCountryTOList(serviceResponse);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}

	}

	public void getSysContentTypeDropDown() throws Exception {
		SysContentTypeTO sysContentTypeTO = new SysContentTypeTO();
		List<SysContentTypeTO> serviceResponse = null;
		try {
			serviceResponse = iNewOrganizationFacadeRemote.getSysContentTypeDropDown();
			if (serviceResponse != null && !serviceResponse.isEmpty())
				newOrganizationBean.setSysContentTypeTOList(serviceResponse);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}

	}

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown() throws Exception {
		List<SysConstantAndSysConstantCategoryTO> serviceResponse = null;
		try {
			serviceResponse = iNewOrganizationFacadeRemote.getSysConstantCategoryDropDown();
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;
	}

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown() throws Exception {
		List<SysConstantAndSysConstantCategoryTO> serviceResponse = null;
		try {
			serviceResponse = iNewOrganizationFacadeRemote.getSysConstantDropDown();
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;
	}

	public List<AddressResponseTO> getStateDropDown(String country) throws Exception {
		AddressResponseTO addressResponseTO = new AddressResponseTO();
		List<AddressResponseTO> serviceResponse = null;
		try {
			serviceResponse = iNewOrganizationFacadeRemote.getStateDropDown(country);
			if (serviceResponse != null && !serviceResponse.isEmpty()) {
				newOrganizationBean.setStateTOList(serviceResponse);
			} else {
				newOrganizationBean.setStateTOList(null);
				newOrganizationBean.setDistrictTOList(null);
				newOrganizationBean.setCityTOList(null);
				newOrganizationBean.setZipCodeTOList(null);

				serviceResponse = null;
			}
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<AddressResponseTO> getDistrictDropDown(String state) throws Exception {
		AddressResponseTO addressResponseTO = new AddressResponseTO();
		List<AddressResponseTO> serviceResponse = null;
		try {
			if (state == null) {
				newOrganizationBean.setDistrictTOList(null);
				newOrganizationBean.setCityTOList(null);
				newOrganizationBean.setZipCodeTOList(null);
				serviceResponse = null;
			} else {
				serviceResponse = iNewOrganizationFacadeRemote.getDistrictDropDown(state);
				// if (serviceResponse != null && !serviceResponse.isEmpty())
				newOrganizationBean.setDistrictTOList(serviceResponse);
			}
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<AddressResponseTO> getCityDropDown(String dist) throws Exception {
		AddressResponseTO addressResponseTO = new AddressResponseTO();
		List<AddressResponseTO> serviceResponse = null;
		try {
			if (dist == null) {
				newOrganizationBean.setCityTOList(null);
				newOrganizationBean.setZipCodeTOList(null);
				serviceResponse = null;
			} else {
				serviceResponse = iNewOrganizationFacadeRemote.getCityDropDown(dist);
				// if (serviceResponse != null && !serviceResponse.isEmpty())
				newOrganizationBean.setCityTOList(serviceResponse);
			}
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<AddressResponseTO> getZipCodeDropDown(Integer cityID) throws Exception {
		AddressResponseTO addressResponseTO = new AddressResponseTO();
		List<AddressResponseTO> serviceResponse = null;

		try {
			if (cityID == null) {
				newOrganizationBean.setZipCodeTOList(null);
				serviceResponse = null;

			} else {
				serviceResponse = iNewOrganizationFacadeRemote.getZipCodeDropDown(cityID);
				// if (serviceResponse != null && !serviceResponse.isEmpty())
				newOrganizationBean.setZipCodeTOList(serviceResponse);
			}
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<SectorCostResponseTO> getSectorDropDown() throws Exception {
		SectorCostResponseTO responseTO = new SectorCostResponseTO();
		List<SectorCostResponseTO> serviceResponse = null;

		try {

			serviceResponse = iNewOrganizationFacadeRemote.getSectorDropDown();
			newOrganizationBean.setSectorTOList(serviceResponse);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<SysSubModuleResponseTO> getModuleDropDownList() throws Exception {
		SysSubModuleResponseTO responseTO = new SysSubModuleResponseTO();
		List<SysSubModuleResponseTO> serviceResponse = null;

		try {

			serviceResponse = iNewOrganizationFacadeRemote.getModuleListDropDown();
			newOrganizationBean.setModuleDropDownList(serviceResponse);
		}

		catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
		return serviceResponse;

	}

	public List<FinancialYearResponseTO> getFinancialYearList(Long orgId) {
		List<FinancialYearResponseTO> fYList = new ArrayList<FinancialYearResponseTO>();
		int orgID = orgId.intValue();
		try {
			fYList = iNewOrganizationFacadeRemote.getFinancialYearList();
			for (FinancialYearResponseTO fy : fYList) {
				if (fy.getOrganizationId() == orgID) {
					newOrganizationBean.setOrgFinancialStartDate(fy.getStartDate());
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}

		return fYList;

	}

	public void getGovtFinancialYearList(Long orgId) {
		List<FinancialYearResponseTO> fYList = new ArrayList<FinancialYearResponseTO>();
		int orgID = orgId.intValue();
		try {
			fYList = iNewOrganizationFacadeRemote.getGovtFinancialYearList();
			for (FinancialYearResponseTO gfy : fYList) {
				if (gfy.getOrganizationId() == orgID) {
					newOrganizationBean.setGovtFinancialStartDate(gfy.getStartDate());
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
	}

	public void createOrganizationInWebPay(Map<Long, List<TpModuleResponseTO>> getAppModuleMap, String webPayInfo,
			String orgC) {
		final String orgCode = orgC;
		int webPayCreationTrial = 0;
		try {

			for (List<TpModuleResponseTO> value : newOrganizationBean.getOrgAppPortalTreeTO().getAppModuleMap()
					.values()) {
				for (TpModuleResponseTO to : value) {
					if (to.getModuleName().equalsIgnoreCase("Payroll") && to.isModuleChecked() == true) {
						// create HTTP Client
						HttpClient httpClient = HttpClientBuilder.create().build();

						// Create new getRequest with below mentioned URL
						HttpGet getRequest = new HttpGet(
								"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/createCompany?comStr="
										+ webPayInfo);

						// Execute your request and catch response
						HttpResponse response = httpClient.execute(getRequest);

						// Check for HTTP response code: 200 = success
						if (response.getStatusLine().getStatusCode() != 200) {
							throw new RuntimeException(
									"Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
						}

						// Get-Capture Complete body response
						BufferedReader br = new BufferedReader(
								new InputStreamReader((response.getEntity().getContent())));
						String webPayCompCreation;
						System.out.println("============Output:============");

						// Simply iterate response and show on console.
						while ((webPayCompCreation = br.readLine()) != null) {
							System.out.println(webPayCompCreation);
						}

						do {
							new java.util.Timer().schedule(new java.util.TimerTask() {

								@Override
								public void run() {
									boolean status = true;
									wpstatus = orgStatusInWebPay(orgCode, status);

								}
							}, 300000);
							counter++;
						} while (wpstatus != true && counter != 3);

						if (counter == 3) {
							FacesContext.getCurrentInstance().addMessage(null,
									new FacesMessage(FacesMessage.SEVERITY_WARN,
											"PayRoll module could not be configured. Please contact your System Administrator",
											""));

						}
						counter = 0;
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

	}

	public Boolean orgStatusInWebPay(String orgCode, boolean status) {
		ServiceRequest serviceRequest = null;
		Boolean wpstatus = false;
		try {

			HttpClient httpClient1 = HttpClientBuilder.create().build();

			// Create new getRequest with below mentioned URL
			HttpGet getRequestForWebPayCompStatus = new HttpGet(
					"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/CompanyCheck?strDBName=" + orgCode);

			// Execute your request and catch response
			HttpResponse webPayCompStatus = httpClient1.execute(getRequestForWebPayCompStatus);

			// Check for HTTP response code: 200 = success
			if (webPayCompStatus.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException(
						"Failed : HTTP error code : " + webPayCompStatus.getStatusLine().getStatusCode());
			}

			// Get-Capture Complete body response
			BufferedReader br1 = new BufferedReader(new InputStreamReader((webPayCompStatus.getEntity().getContent())));
			String webPayCompStatus1;
			System.out.println("============Output:============");

			// Simply iterate response and show on console.
			while ((webPayCompStatus1 = br1.readLine()) != null) {
				System.out.println(webPayCompStatus1);
				if (webPayCompStatus1.equalsIgnoreCase("<string xmlns=\"http://tempuri.org/\">Y</string>")) {
					DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource is = new InputSource();
					is.setCharacterStream(new StringReader(webPayCompStatus1));

					Document doc = db.parse(is);
					NodeList nodes = doc.getElementsByTagName("string");
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						if (getCharacterDataFromElement(element).equalsIgnoreCase("Y")) {
							iNewOrganizationFacadeRemote.saveOrgWebPayStatus(orgCode, status);
							wpstatus = true;
						}
						System.out.println("string: " + getCharacterDataFromElement(element));
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return wpstatus;
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public void updateOrgForWebPay() {
		String webPayInfo = null;
		String companyName = null;
		String compAddress = null;
		Integer citycode = null;
		String pincode = null;
		String compEmail = null;
		Date financialYear = null;
		String token = null;
		SimpleDateFormat dateformat = null;
		String fY = null;
		OrganizationResponseTO resTo = null;
		try {
			final String companyFolderName = newOrganizationBean.getOrgCode();

			companyName = newOrganizationBean.getOrgName();
			compAddress = newOrganizationBean.getLine1();
			// citycode = newOrganizationBean.getCityID();
			citycode = 1;
			pincode = newOrganizationBean.getZipCode();
			compEmail = newOrganizationBean.getEmail();
			financialYear = newOrganizationBean.getGovtFinancialStartDate();
			SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-YYYY");
			fY = dt1.format(financialYear);
			token = "abc";
			webPayInfo = companyFolderName + "~" + companyName + "~" + compAddress + "~" + citycode + "~" + pincode
					+ "~" + compEmail + "~" + fY + "~" + token;
			resTo = newOrganizationBean.getOrgTO();
			if (resTo.getWebPayStatus() != true) {
				for (List<TpModuleResponseTO> value : newOrganizationBean.getOrgAppPortalTreeTO().getAppModuleMap()
						.values()) {
					for (TpModuleResponseTO to : value) {
						if (to.getModuleName().equalsIgnoreCase("Payroll") && to.isModuleChecked() == true) {
							// create HTTP Client
							HttpClient httpClient = HttpClientBuilder.create().build();

							// Create new getRequest with below mentioned URL
							HttpGet getRequest = new HttpGet(
									"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/createCompany?comStr="
											+ webPayInfo);

							// Execute your request and catch response
							HttpResponse response = httpClient.execute(getRequest);

							// Check for HTTP response code: 200 = success
							if (response.getStatusLine().getStatusCode() != 200) {
								throw new RuntimeException(
										"Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
							}

							// Get-Capture Complete body response
							BufferedReader br = new BufferedReader(
									new InputStreamReader((response.getEntity().getContent())));
							String webPayCompCreation;
							System.out.println("============Output:============");

							// Simply iterate response and show on console.
							while ((webPayCompCreation = br.readLine()) != null) {
								System.out.println(webPayCompCreation);
							}

							do {
								new java.util.Timer().schedule(new java.util.TimerTask() {

									@Override
									public void run() {
										boolean status = true;

										wpstatus = orgStatusInWebPay(companyFolderName, status);

									}
								}, 300000);
								counter++;

							} while (wpstatus != true && counter != 3);

							if (counter == 3) {
								FacesContext.getCurrentInstance().addMessage(null,
										new FacesMessage(FacesMessage.SEVERITY_WARN,
												"PayRoll module could not be configured. Please contact your System Administrator",
												""));

							}
							counter = 0;

						}

					}
				}
			} else if (resTo.getWebPayStatus() == true) {

				for (List<TpModuleResponseTO> value : newOrganizationBean.getOrgAppPortalTreeTO().getAppModuleMap()
						.values()) {
					for (TpModuleResponseTO to : value) {
						if (to.getModuleName().equalsIgnoreCase("Payroll") && to.isModuleChecked() == false) {
							// create HTTP Client
							HttpClient httpClient = HttpClientBuilder.create().build();
							HttpGet getRequest = new HttpGet(
									"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/UpdateCompany?strDBName="
											+ companyFolderName + "&CompName=" + companyName + "&citycode=" + citycode
											+ "&pincode=" + pincode + "&email=" + compEmail + "&add=" + compAddress);

							// Execute your request and catch response
							HttpResponse response = httpClient.execute(getRequest);

							// Check for HTTP response code: 200 = success
							if (response.getStatusLine().getStatusCode() != 200) {
								throw new RuntimeException(
										"Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
							}

							// Get-Capture Complete body response
							BufferedReader br = new BufferedReader(
									new InputStreamReader((response.getEntity().getContent())));
							String webPayCompCreation;
							System.out.println("============Output:============");

							// Simply iterate response and show on console.
							while ((webPayCompCreation = br.readLine()) != null) {
								System.out.println(webPayCompCreation);
							}

							do {
								new java.util.Timer().schedule(new java.util.TimerTask() {

									@Override
									public void run() {
										boolean status = false;
										wpstatus = orgStatusInWebPay(companyFolderName, status);
									}
								}, 300000);
								counter++;

							} while (wpstatus != true && counter != 3);

							if (counter == 3) {
								FacesContext.getCurrentInstance().addMessage(null,
										new FacesMessage(FacesMessage.SEVERITY_WARN,
												"PayRoll module could not be configured. Please contact your System Administrator",
												""));

							}
							counter = 0;

						} else if (to.getModuleName().equalsIgnoreCase("Payroll") && to.isModuleChecked() == true) {
							// create HTTP Client
							HttpClient httpClient = HttpClientBuilder.create().build();
							HttpGet getRequest = new HttpGet(
									"http://192.168.2.80/Payroll/webcomponet/wscompanyadmin.asmx/UpdateCompany?strDBName="
											+ companyFolderName + "&CompName=" + companyName + "&citycode=" + citycode
											+ "&pincode=" + pincode + "&email=" + compEmail + "&add=" + compAddress);

							// Execute your request and catch response
							HttpResponse response = httpClient.execute(getRequest);

							// Check for HTTP response code: 200 = success
							if (response.getStatusLine().getStatusCode() != 200) {
								throw new RuntimeException(
										"Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
							}

							// Get-Capture Complete body response
							BufferedReader br = new BufferedReader(
									new InputStreamReader((response.getEntity().getContent())));
							String webPayCompCreation;
							System.out.println("============Output:============");

							// Simply iterate response and show on console.
							while ((webPayCompCreation = br.readLine()) != null) {
								System.out.println(webPayCompCreation);
							}

							do {
								new java.util.Timer().schedule(new java.util.TimerTask() {

									@Override
									public void run() {
										boolean status = true;

										wpstatus = orgStatusInWebPay(companyFolderName, status);
									}
								}, 300000);
								counter++;

							} while (wpstatus != true && counter != 3);

							if (counter == 3) {
								FacesContext.getCurrentInstance().addMessage(null,
										new FacesMessage(FacesMessage.SEVERITY_WARN,
												"PayRoll module could not be configured. Please contact your System Administrator",
												""));

							}
							counter = 0;

						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateLoginServerPanel() {

		SocialAppSettingTO socialAppSettingTO = null;
		Map<String, SocialAppSettingTO> socialAppSettingTOMap = null;
		List<SocialAppSettingTO> socialAppSettingTOList = null;
		List<String> defaultLoginServerList = null;

		try {
			defaultLoginServerList = new ArrayList<String>();
			List<LoginServerTypeTO> loginServerTypeTOList = newOrganizationBean.getLoginServerTypeList();
			List<String> selectedLoginServerTypes = newOrganizationBean.getSelectedLoginServerTypes();
			if (selectedLoginServerTypes == null || selectedLoginServerTypes.isEmpty()) {
				newOrganizationBean.setRenderLoginServerPanel(false);
				newOrganizationBean.setSocialAppSettingTOMap(new HashMap<String, SocialAppSettingTO>());
			} else {
				if (newOrganizationBean.isEditLoginServer()) {
					socialAppSettingTOMap = newOrganizationBean.getSocialAppSettingTOMap();
					socialAppSettingTOList = new ArrayList<SocialAppSettingTO>();
				} else {
					socialAppSettingTOMap = new HashMap<String, SocialAppSettingTO>();
				}
				newOrganizationBean.setRenderLoginServerPanel(true);
				for (LoginServerTypeTO loginType : loginServerTypeTOList) {
					for (String selectedLoginServerTypeId : selectedLoginServerTypes) {
						if (Integer.parseInt(selectedLoginServerTypeId) == loginType.getLoginTypeID()) {
							defaultLoginServerList.add(loginType.getTypeName());
							if (loginType.getTypeName().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_GOOGLE)) {
								if (!newOrganizationBean.isEditLoginServer()) {
									socialAppSettingTO = new SocialAppSettingTO();
									socialAppSettingTO.setSocialAppType(CoreConstants.LOGIN_SERVER_GOOGLE);
									socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_GOOGLE
											+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID(),
											socialAppSettingTO);

								} else {
									socialAppSettingTO = socialAppSettingTOMap.get(CoreConstants.LOGIN_SERVER_GOOGLE
											+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID());
									socialAppSettingTOList.add(socialAppSettingTO);
								}
								newOrganizationBean.setRenderLADP(false);
								newOrganizationBean.setRenderSP(false);
							} else if (loginType.getTypeName().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_FACEBOOK)) {
								if (!newOrganizationBean.isEditLoginServer()) {
									socialAppSettingTO = new SocialAppSettingTO();
									socialAppSettingTO.setSocialAppType(CoreConstants.LOGIN_SERVER_FACEBOOK);
									socialAppSettingTOMap.put(CoreConstants.LOGIN_SERVER_FACEBOOK
											+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID(),
											socialAppSettingTO);
								} else {
									socialAppSettingTO = socialAppSettingTOMap.get(CoreConstants.LOGIN_SERVER_FACEBOOK
											+ CoreConstants.AT_SPERATOR + loginType.getLoginTypeID());
									socialAppSettingTOList.add(socialAppSettingTO);
								}
								newOrganizationBean.setRenderLADP(false);
								newOrganizationBean.setRenderSP(false);
							} else if (loginType.getTypeName().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_LDAP)) {
								newOrganizationBean.setRenderLADP(true);
								newOrganizationBean.setRenderSP(false);
							} else if (loginType.getTypeName().equalsIgnoreCase(CoreConstants.LOGIN_SERVER_SP)) {
								newOrganizationBean.setRenderSP(true);
							}
						}
					}
				}
				newOrganizationBean.setSocialAppSettingTOMap(socialAppSettingTOMap);
			}

			if (!newOrganizationBean.isEditLoginServer()) {
				if (socialAppSettingTOMap != null && !socialAppSettingTOMap.isEmpty()) {
					socialAppSettingTOList = new ArrayList<SocialAppSettingTO>();
					socialAppSettingTOList.addAll(socialAppSettingTOMap.values());
				}
			}
			newOrganizationBean.setSocialAppSettingTOList(socialAppSettingTOList);
			newOrganizationBean.setSelectedLoginServerList(defaultLoginServerList);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void copySysDashBoardChartList() throws Exception {
		ServiceRequest serviceRequest = null;
		List<CheckListResponseTO> listCheckListResponseTO = null;
		DashboardChartRequestTO dashboardChartRequestTO = null;
		CheckListResponseTO checkResTO = null;

		try {
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String checkListItem = params.get("checkListItem");
			listCheckListResponseTO = checkListBean.getListCheckListResponseTO();
			for (CheckListResponseTO resTO : listCheckListResponseTO) {
				if (resTO.getItemName().equals(checkListItem)) {
					serviceRequest = new ServiceRequest();
					dashboardChartRequestTO = new DashboardChartRequestTO();
					dashboardChartRequestTO.setSelectedPortalTypeID((int) newOrganizationBean.getSelectedPortalID());
					dashboardChartRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
					dashboardChartRequestTO.setSysCheckListID(resTO.getSysCheckListID());
					dashboardChartRequestTO.setTenantID(newOrganizationBean.getTenantID());
					dashboardChartRequestTO.setCreatedBy(userSessionBean.getUserID());
					dashboardChartRequestTO.setModifiedBy(userSessionBean.getUserID());
					checkResTO = resTO;
					break;
				}
			}
			dashboardChartRequestTO.setDashBoardChartTOlist(newOrganizationBean.getDashBoardChartTOlist());
			serviceRequest.setRequestTansportObject(dashboardChartRequestTO);
			iCheckListFacadeRemote.copyAllDashboardChartList(serviceRequest);
			checkResTO.setSelected(true);
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Dashboard Chart copied Succesfully ", ""));
		} catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error in copy Dashboard Chart.", ""));
		}

	}

	public void refreshDashboardChart() throws Exception {
		try {
			ServiceRequest serviceRequest = new ServiceRequest();
			DashboardChartRequestTO dashboardChartRequestTO = new DashboardChartRequestTO();
			dashboardChartRequestTO.setOrganizationID(newOrganizationBean.getOrgId());
			dashboardChartRequestTO.setSelectedPortalTypeID((int) newOrganizationBean.getSelectedPortalID());
			serviceRequest.setRequestTansportObject(dashboardChartRequestTO);
			ServiceResponse serviceResponse = iCheckListFacadeRemote.getAllDashboardChartList(serviceRequest);
			if (serviceResponse != null) {
				DashboardChartResponseTO dashboardChartResponseTO = (DashboardChartResponseTO) serviceResponse
						.getResponseTransportObject();
				if (dashboardChartResponseTO != null) {
					newOrganizationBean.setDashBoardChartTOlist(dashboardChartResponseTO.getSysDashboardChartTOList());
					newOrganizationBean.setShowChartDataList(true);
				}
			}
		} catch (Exception ex) {
			FacesContext.getCurrentInstance().addMessage("activity",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "fetching Dashboard Chart list.", ""));
		}

	}

	public List<HrAppTO> createFinalNewAppList(List<String> selectedAppIds, Long orgId, Long tenantId) {
		List<HrAppTO> finalSelectedApps = new ArrayList<>();

		for (String id : selectedAppIds) {
			if (selectedActiveAppIdsBeforeUpdate.contains(id)) {
				continue;
			} else if (selectedInactiveAppIdsBeforeUpdate.contains(id)) {
				// update to active
				HrAppTO app = new HrAppTO(Long.parseLong(id), orgId, tenantId, true);
				finalSelectedApps.add(app);
			} else {
				// create
				HrAppTO app = new HrAppTO(Long.parseLong(id), orgId, tenantId, true);
				finalSelectedApps.add(app);
			}
		}
		for (String id : selectedActiveAppIdsBeforeUpdate) {
			if (!selectedAppIds.contains(id)) {
				// update to in
				HrAppTO app = new HrAppTO(Long.parseLong(id), orgId, tenantId, false);
				finalSelectedApps.add(app);
			}
		}

		return finalSelectedApps;
	}

}
