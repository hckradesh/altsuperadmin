package com.talentpact.ui.altbenefits.helper;

/**
 * 
 * 
 * @author javed.ali
 * 
 */
public enum OfferCodeField {
    OFFERCODE("Offer Code"), ACCESSTYPE("Access Type"), STARTDATE("Start Date"), ENDDATE("End Date"), ISACTIVE("IsActive"), ISMAXIMUMLIMIT("IsMaximumLimit Applicable"), MAXIMUMUSAGETYPE(
            "Maximum Usage Type"), MAXIMUMDURATIONTYPE("Maximum Duration Type"), MAXIMUMCOUNT("Maximum Count");

    private String offerCodeField;

    private OfferCodeField(String offerCodeField)
    {
        this.offerCodeField = offerCodeField;
    }

    public String getOfferCodeField()
    {
        return offerCodeField;
    }
}
