/**
 * 
 */
package com.talentpact.ui.tpApp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.application.transport.input.TpAppRequestTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.dataservice.tpApp.TpAppService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.tpApp.bean.TpAppBean;

/**
 * @author pankaj.sharma1
 *
 */
@Named("tpAppController")
@ConversationScoped
public class TpAppController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    TpAppService tpAppService;

    @Inject
    TpAppBean tpAppBean;

    @Override
    public void initialize()
    {
        List<TpAppResponseTO> tpAppTOList = null;
        try {

            tpAppTOList = getTpAppTOListList();

            tpAppBean.setTpAppTOList(tpAppTOList);
            menuBean.setPage("../tpApp/tpApp.xhtml");
        } catch (Exception e) {
        } finally {

        }

    }

    private List<TpAppResponseTO> getTpAppTOListList()
    {
        List<TpAppResponseTO> response = null;
        try {
            response = tpAppService.getTpAppList();
        } catch (Exception e) {
        }
        return response;

    }

    public void refreshPopUp(TpAppResponseTO responseTO)
    {
        try {
            tpAppBean.setAppID(responseTO.getAppID());
            tpAppBean.setName(responseTO.getName());
            tpAppBean.setCode(responseTO.getCode());
            tpAppBean.setImage(responseTO.getImage());

        } catch (Exception e) {
        }
    }

    public void resetBean()
    {
        try {
            tpAppBean.setAppID(null);
            tpAppBean.setName(null);
            tpAppBean.setCode(null);
            tpAppBean.setImage(null);
        } catch (Exception e) {

        }

    }


    public void save()
    {
        {
            TpAppRequestTO requestTO = null;
            RequestContext context = null;
            Long appID = null;
            String name = null;
            String code = null;

            try {
                context = RequestContext.getCurrentInstance();
                name = tpAppBean.getName();
                code = tpAppBean.getCode();
                for (TpAppResponseTO list : tpAppBean.getTpAppTOList()) {
                    if (name.equals(list.getName())) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "this AppName exists", ""));
                        return;
                    }
                }

                for (TpAppResponseTO list : tpAppBean.getTpAppTOList()) {
                    if (code.equals(list.getCode())) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "this AppCode exists", ""));
                        return;
                    }
                }
                requestTO = new TpAppRequestTO();
                requestTO.setName(tpAppBean.getName());
                requestTO.setCode(tpAppBean.getCode());
                requestTO.setImage(tpAppBean.getImage());
                tpAppService.saveTpApp(requestTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved Successfully", ""));


                context.execute("PF('tpAppAddModal').hide();");
            } catch (Exception ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error occured", ""));
                context.execute("PF('tpAppAddModal').hide();");
            }
        }

    }

    public void update()
    {
        TpAppRequestTO requestTO = null;
        RequestContext context = null;
        Long appID = null;
        String name = null;
        String code = null;
        List<TpAppResponseTO> list1 = null;
        try {
            context = RequestContext.getCurrentInstance();
            requestTO = new TpAppRequestTO();
            list1 = new ArrayList<TpAppResponseTO>();
            name = tpAppBean.getName();
            code = tpAppBean.getCode();
            appID = tpAppBean.getAppID();
            for (TpAppResponseTO li : tpAppBean.getTpAppTOList()) {
                if (appID.equals(li.getAppID())) {
                    // do nothing
                } else {
                    list1.add(li);
                }
            }
            for (TpAppResponseTO l : list1) {
                if (name.equals(l.getName()) && code.equals(l.getCode())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "this combination already exists", ""));
                    return;
                }
            }

            requestTO.setAppID(tpAppBean.getAppID());
            requestTO.setName(tpAppBean.getName());
            requestTO.setImage(tpAppBean.getImage());
            requestTO.setCode(tpAppBean.getCode());


            tpAppService.saveTpApp(requestTO);
            context.execute("PF('myDataTable').clearFilters();");
            initialize();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "App details Updated Succesfully", ""));
            context.execute("PF('tpAppEditModal').hide();");
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected error occured", ""));
            context.execute("PF('tpAppEditModal').hide();");

        }
    }

}
