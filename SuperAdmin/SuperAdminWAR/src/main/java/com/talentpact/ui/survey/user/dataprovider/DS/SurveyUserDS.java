/**
 * 
 */
package com.talentpact.ui.survey.user.dataprovider.DS;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.auth.entity.TpUser;
import com.talentpact.business.authorization.transport.input.AuthRequestTO;
import com.talentpact.business.dataservice.AbstractDS;

/**
 * @author vaibhav.kashyap
 * 
 */

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SurveyUserDS extends AbstractDS<TpUser> {

	public static final Logger LOGGER_ = LoggerFactory
			.getLogger(TpUser.class);

	public SurveyUserDS() {
		super(TpUser.class);
	}


	/**
	 * @param surveyUserRequestTO
	 * @return
	 */
//	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
//	public SurveyUserResponseTO createNewUser(
//			SurveyUserRequestTO surveyUserRequestTO) throws Exception {
//		SurveyUser surveyUser = null;
//		SurveyUserResponseTO surveyUserResponseTO = null;
//		SurveyOrg surveyOrganization = null;
//		try {
//			if (surveyUserRequestTO != null) {
//				surveyUser = new SurveyUser();
//
//				surveyUser.setAdmin(surveyUserRequestTO.getAdmin());
//				surveyOrganization = getEntityManager("TalentPactFormEngine_New").find(SurveyOrg.class,
//						surveyUserRequestTO.getOrganizationID());
//				if (surveyOrganization != null) {
//					surveyUser.setSurveyOrg(surveyOrganization);
//				} else {
//					return null;
//				}
//				surveyUser.setUserName(surveyUserRequestTO.getUserName());
//				surveyUser.setUserSurveyID(surveyUserRequestTO
//						.getSurveyUserID());
//
//				getEntityManager("TalentPactFormEngine_New").persist(surveyUser);
//				surveyUserResponseTO = SurveyUserConverter
//						.getSurveyUserResponseTO(surveyUser);
//			}
//		} catch (Exception ex) {
//			LOGGER_.debug("Exception while creating new user : SurveyUserDS : "
//					+ ex);
//			surveyUserResponseTO = null;
//			throw ex;
//		} finally {
//
//		}
//
//		return surveyUserResponseTO;
//	}

	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public TpUser findUserByName(AuthRequestTO authRequestTO) throws Exception {
		Query query = null;
		TpUser user = null;
		List<TpUser> userList = null;
		StringBuilder hqlQuery = null;
		try {
			hqlQuery = new StringBuilder();
			hqlQuery.append("from TpUser t WHERE t.username = :username ");
			query = getEntityManager(authRequestTO.getPersistenceUnitName())
					.createQuery(hqlQuery.toString());
			query.setParameter("username", authRequestTO.getUserName());
			userList = query.getResultList();
			if (userList != null && !userList.isEmpty()) {
				user = userList.get(0);
			}
		} catch (Exception ex) {
			throw ex;
		}
		
		return user;
	}

}
