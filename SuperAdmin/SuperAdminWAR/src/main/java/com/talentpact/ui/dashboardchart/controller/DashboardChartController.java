/**
 * 
 */
package com.talentpact.ui.dashboardchart.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.dashboardChart.DashboardChartService;
import com.talentpact.insights.dashboardchart.to.DashboardChartTO;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboardchart.bean.DashboardChartBean;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * 
 * @author javed.ali
 * 
 */
@Named("dashboardChartController")
@ConversationScoped
public class DashboardChartController extends CommonController implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = 478933059112145734L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(DashboardChartController.class);


    @Inject
    DashboardChartBean dashboardChartBean;

    @Inject
    UserSessionBean userSessionBean;

    @EJB
    DashboardChartService dashboardChartService;

    @Inject
    MenuBean menuBean;

    @Override
    public void initialize()
    {
        List<DashboardChartTO> sysDashboardTOList = null;
        try {
            sysDashboardTOList = dashboardChartService.getSysDashboardList();
            dashboardChartBean.setSysDashboardTOList(sysDashboardTOList);
            dashboardChartBean.setRenderErrorMessage(false);
            menuBean.setPage("../dashboardChart/dashboardChartList.xhtml");
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error Occurred While fetching sydashboard list ", ""));
        }

    }
/**
 * 
 * @throws Exception
 */
    public void saveSysDashboard()
        throws Exception
    {
        DashboardChartTO dashboardChartTO = new DashboardChartTO();
        try {
            dashboardChartTO.setDashboardName(dashboardChartBean.getDashboardName());
            dashboardChartTO.setStyleClass(dashboardChartBean.getStyleClass());
            dashboardChartService.saveSysDashboard(dashboardChartTO);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dashboard Saved successfully.", ""));
            dashboardChartBean.setRenderErrorMessage(false);
            initialize();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error Occurred While saving dashboard ", ""));
            LOGGER_.error("", ex);
            dashboardChartBean.setRenderErrorMessage(false);
        }
    }

    /**
     * 
     */
    public void resetSysDashboard()
    {
        dashboardChartBean.setDashboardName(null);
        dashboardChartBean.setStyleClass(null);
        dashboardChartBean.setRenderErrorMessage(true);
    }

}