/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;
import com.talentpact.ui.rule.to.RuleResponseRevisedTO;
import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysPlaceHolder.common.transport.output.SysPlaceHolderResponseTO;
import com.talentpact.ui.sysPlaceHolder.controller.SysPlaceHolderController;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

/**
 * @author vaibhav.kashyap
 * 
 */
@Named("sysPlaceHolderBean")
@ConversationScoped
public class SysPlaceHolderBean extends CommonBean implements Serializable,
		IDefaultAction {

	/**
     * 
     */
	private static final long serialVersionUID = -1847322153212426002L;

	@Inject
	private SysPlaceHolderController sysPlaceHolderController;

	private String placeHolderName = "";
	private String placeHolderDescription = "";
	private String columnExpression = "";
	private String whereCondition = "";
	private String entityPath = "";
	private String contentTypeSelected = "";
	private String communicationType = "";
	private String sysPLaceHolderAddHeader = "";
	private String insertNewPlaceHolderCommandButtonValueText = "";
	private String subModuleName = "";

	private int ruleID;
	private int levelType;
	private int complexityType;
	private int commTypeID;
	private int moduleID;
	private int subModuleID;
	private int tenantID;
	private int placeHolderTypeID;
	private long sysPlaceHolderIDForDeletion;
	private long sysPlaceHolderIDForUpdation;

	private long entityColumnID;

	private List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
	private List<SysPlaceHolderResponseTO> filteredSysPlaceHolderResponseTOList = null;
	private List<RuleResponseRevisedTO> sysRuleResponseTOList = null;
	private List<SysCommunicationTypeResponseTO> sysCommunicationTypeResponseTOList = null;
	private List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
	private List<HrContentTypeResponseTO> hrContentTypeResponseTOList = null;
	private List<String> subModulesNamesList = null;

	private Map<Integer, List<SysSubModuleResponseTO>> parentModuleIDByChild = null;
	private Map<Integer, String> parentModuleIDByName = null;
	private Map<Integer, List<String>> childModuleIDByName = null;
	private Map<Integer, String> allModuleIDByName = null;

	private boolean sysplaceholderFormMessagesFlag;
	private boolean disableSubModuleSelection = true;

	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			sysPlaceHolderController.initialize();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

		}
	}

	public String getPlaceHolderName() {
		return placeHolderName;
	}

	public void setPlaceHolderName(String placeHolderName) {
		this.placeHolderName = placeHolderName;
	}

	public String getPlaceHolderDescription() {
		return placeHolderDescription;
	}

	public void setPlaceHolderDescription(String placeHolderDescription) {
		this.placeHolderDescription = placeHolderDescription;
	}

	public String getColumnExpression() {
		return columnExpression;
	}

	public void setColumnExpression(String columnExpression) {
		this.columnExpression = columnExpression;
	}

	public String getWhereCondition() {
		return whereCondition;
	}

	public void setWhereCondition(String whereCondition) {
		this.whereCondition = whereCondition;
	}

	public String getEntityPath() {
		return entityPath;
	}

	public void setEntityPath(String entityPath) {
		this.entityPath = entityPath;
	}

	public String getContentTypeSelected() {
		return contentTypeSelected;
	}

	public void setContentTypeSelected(String contentTypeSelected) {
		this.contentTypeSelected = contentTypeSelected;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	public String getSysPLaceHolderAddHeader() {
		return sysPLaceHolderAddHeader;
	}

	public void setSysPLaceHolderAddHeader(String sysPLaceHolderAddHeader) {
		this.sysPLaceHolderAddHeader = sysPLaceHolderAddHeader;
	}

	public String getInsertNewPlaceHolderCommandButtonValueText() {
		return insertNewPlaceHolderCommandButtonValueText;
	}

	public void setInsertNewPlaceHolderCommandButtonValueText(
			String insertNewPlaceHolderCommandButtonValueText) {
		this.insertNewPlaceHolderCommandButtonValueText = insertNewPlaceHolderCommandButtonValueText;
	}

	public String getSubModuleName() {
		return subModuleName;
	}

	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}

	public int getRuleID() {
		return ruleID;
	}

	public void setRuleID(int ruleID) {
		this.ruleID = ruleID;
	}

	public int getLevelType() {
		return levelType;
	}

	public void setLevelType(int levelType) {
		this.levelType = levelType;
	}

	public int getComplexityType() {
		return complexityType;
	}

	public void setComplexityType(int complexityType) {
		this.complexityType = complexityType;
	}

	public int getCommTypeID() {
		return commTypeID;
	}

	public void setCommTypeID(int commTypeID) {
		this.commTypeID = commTypeID;
	}

	public int getModuleID() {
		return moduleID;
	}

	public void setModuleID(int moduleID) {
		this.moduleID = moduleID;
	}

	public int getSubModuleID() {
		return subModuleID;
	}

	public void setSubModuleID(int subModuleID) {
		this.subModuleID = subModuleID;
	}

	public int getTenantID() {
		return tenantID;
	}

	public void setTenantID(int tenantID) {
		this.tenantID = tenantID;
	}

	public int getPlaceHolderTypeID() {
		return placeHolderTypeID;
	}

	public void setPlaceHolderTypeID(int placeHolderTypeID) {
		this.placeHolderTypeID = placeHolderTypeID;
	}

	public long getSysPlaceHolderIDForDeletion() {
		return sysPlaceHolderIDForDeletion;
	}

	public void setSysPlaceHolderIDForDeletion(long sysPlaceHolderIDForDeletion) {
		this.sysPlaceHolderIDForDeletion = sysPlaceHolderIDForDeletion;
	}

	public long getSysPlaceHolderIDForUpdation() {
		return sysPlaceHolderIDForUpdation;
	}

	public void setSysPlaceHolderIDForUpdation(long sysPlaceHolderIDForUpdation) {
		this.sysPlaceHolderIDForUpdation = sysPlaceHolderIDForUpdation;
	}

	public long getEntityColumnID() {
		return entityColumnID;
	}

	public void setEntityColumnID(long entityColumnID) {
		this.entityColumnID = entityColumnID;
	}

	public List<SysPlaceHolderResponseTO> getSysPlaceHolderResponseTOList() {
		return sysPlaceHolderResponseTOList;
	}

	public void setSysPlaceHolderResponseTOList(
			List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList) {
		this.sysPlaceHolderResponseTOList = sysPlaceHolderResponseTOList;
	}

	public List<SysPlaceHolderResponseTO> getFilteredSysPlaceHolderResponseTOList() {
		return filteredSysPlaceHolderResponseTOList;
	}

	public void setFilteredSysPlaceHolderResponseTOList(
			List<SysPlaceHolderResponseTO> filteredSysPlaceHolderResponseTOList) {
		this.filteredSysPlaceHolderResponseTOList = filteredSysPlaceHolderResponseTOList;
	}

	public List<RuleResponseRevisedTO> getSysRuleResponseTOList() {
		return sysRuleResponseTOList;
	}

	public void setSysRuleResponseTOList(
			List<RuleResponseRevisedTO> sysRuleResponseTOList) {
		this.sysRuleResponseTOList = sysRuleResponseTOList;
	}

	public List<SysCommunicationTypeResponseTO> getSysCommunicationTypeResponseTOList() {
		return sysCommunicationTypeResponseTOList;
	}

	public void setSysCommunicationTypeResponseTOList(
			List<SysCommunicationTypeResponseTO> sysCommunicationTypeResponseTOList) {
		this.sysCommunicationTypeResponseTOList = sysCommunicationTypeResponseTOList;
	}

	public List<SysEntityColumnResponseTO> getSysEntityColumnResponseTOList() {
		return sysEntityColumnResponseTOList;
	}

	public void setSysEntityColumnResponseTOList(
			List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList) {
		this.sysEntityColumnResponseTOList = sysEntityColumnResponseTOList;
	}

	public List<HrContentTypeResponseTO> getHrContentTypeResponseTOList() {
		return hrContentTypeResponseTOList;
	}

	public void setHrContentTypeResponseTOList(
			List<HrContentTypeResponseTO> hrContentTypeResponseTOList) {
		this.hrContentTypeResponseTOList = hrContentTypeResponseTOList;
	}

	public List<String> getSubModulesNamesList() {
		return subModulesNamesList;
	}

	public void setSubModulesNamesList(List<String> subModulesNamesList) {
		this.subModulesNamesList = subModulesNamesList;
	}

	public Map<Integer, List<SysSubModuleResponseTO>> getParentModuleIDByChild() {
		return parentModuleIDByChild;
	}

	public void setParentModuleIDByChild(
			Map<Integer, List<SysSubModuleResponseTO>> parentModuleIDByChild) {
		this.parentModuleIDByChild = parentModuleIDByChild;
	}

	public Map<Integer, String> getParentModuleIDByName() {
		return parentModuleIDByName;
	}

	public void setParentModuleIDByName(Map<Integer, String> parentModuleIDByName) {
		this.parentModuleIDByName = parentModuleIDByName;
	}

	public Map<Integer, List<String>> getChildModuleIDByName() {
		return childModuleIDByName;
	}

	public void setChildModuleIDByName(
			Map<Integer, List<String>> childModuleIDByName) {
		this.childModuleIDByName = childModuleIDByName;
	}

	public Map<Integer, String> getAllModuleIDByName() {
		return allModuleIDByName;
	}

	public void setAllModuleIDByName(Map<Integer, String> allModuleIDByName) {
		this.allModuleIDByName = allModuleIDByName;
	}

	public boolean isSysplaceholderFormMessagesFlag() {
		return sysplaceholderFormMessagesFlag;
	}

	public void setSysplaceholderFormMessagesFlag(
			boolean sysplaceholderFormMessagesFlag) {
		this.sysplaceholderFormMessagesFlag = sysplaceholderFormMessagesFlag;
	}

	public boolean isDisableSubModuleSelection() {
		return disableSubModuleSelection;
	}

	public void setDisableSubModuleSelection(boolean disableSubModuleSelection) {
		this.disableSubModuleSelection = disableSubModuleSelection;
	}
	
}
