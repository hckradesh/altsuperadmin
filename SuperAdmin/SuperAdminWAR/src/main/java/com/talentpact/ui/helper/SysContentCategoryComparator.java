package com.talentpact.ui.helper;

import java.util.Comparator;

import com.talentpact.business.application.transport.output.SysContentTypeTO;



public class SysContentCategoryComparator implements Comparator<SysContentTypeTO>{
   public int compare(SysContentTypeTO e1, SysContentTypeTO e2) {
        return e1.getCategoryName().compareTo(e2.getCategoryName());
    }
}  