package com.talentpact.ui.checklist.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.CheckListResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
@Named("checkListBean")
@SessionScoped
public class CheckListBean extends CommonBean implements Serializable
{
    List<CheckListResponseTO> listCheckListResponseTO;

    public List<CheckListResponseTO> getListCheckListResponseTO()
    {
        return listCheckListResponseTO;
    }

    public void setListCheckListResponseTO(List<CheckListResponseTO> listCheckListResponseTO)
    {
        this.listCheckListResponseTO = listCheckListResponseTO;
    }



}