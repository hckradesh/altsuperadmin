/**
 * 
 */
package com.talentpact.ui.sysRule.dataprovider;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.rule.to.RuleResponseRevisedTO;
import com.talentpact.ui.rule.to.RuleResponseTO;
import com.talentpact.ui.sysRule.dataservice.SysRuleDataService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysRuleDataProvider {

	@EJB
	private SysRuleDataService sysRuleDataService;
	/**
	 * @return
	 */
	public ServiceResponse getAllRules() throws Exception{
		ServiceResponse serviceResponse = null;
		List<RuleResponseTO> ruleResponseTOList = null;
		List<RuleResponseRevisedTO> ruleResponseRevisedTOList = null;
		RuleResponseRevisedTO tempObj;
 		try{
 			ruleResponseTOList = sysRuleDataService.getAllRules();
 			/**
 			 * below code is a bad practice and shouldn't be followed
 			 * */
 			if(ruleResponseTOList!=null && 
 					!ruleResponseTOList.isEmpty()){
 				ruleResponseRevisedTOList = new ArrayList<RuleResponseRevisedTO>();
 				for(RuleResponseTO temp : ruleResponseTOList){
 					tempObj = new RuleResponseRevisedTO();
 					if(temp.getRuleMethod()!=null)
 						tempObj.setRuleMethod(temp.getRuleMethod());
 					if(temp.getDescription()!=null)
 						tempObj.setDescription(temp.getDescription());
 					if(temp.getMethodParameters()!=null)
 						tempObj.setMethodParameters(temp.getMethodParameters());
 					if(temp.getMethodReturnType()!=null)
 						tempObj.setMethodReturnType(temp.getMethodReturnType());
 					if(temp.getRuleClass()!=null)
 						tempObj.setRuleClass(temp.getRuleClass());
 					if(temp.getRuleClassName()!=null)
 						tempObj.setRuleClassName(temp.getRuleClassName());
 					if(temp.getRuleID()!=null)
 						tempObj.setRuleID(temp.getRuleID());
 					if(temp.getRuleMethod()!=null)
 						tempObj.setRuleMethod(temp.getRuleMethod());
 					if(temp.getRuleTypeID()!=null)
 						tempObj.setRuleTypeID(temp.getRuleTypeID());
 					if(temp.getSysType()!=null)
 						tempObj.setSysType(temp.getSysType());
 					
 					ruleResponseRevisedTOList.add(tempObj);
 				}
 				serviceResponse = new ServiceResponse();
 				serviceResponse.setResponseTransferObjectObjectList(ruleResponseRevisedTOList);
 			}
		}catch(Exception ex){
			
		}
		return serviceResponse;
	}

}
