/**
 * 
 */
package com.talentpact.ui.sysPolicy.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.common.constants.ISysPolicyConstants;
import com.talentpact.business.dataservice.SysContentType.SysContentTypeService;
import com.talentpact.business.dataservice.sysPolicy.SysPolicyService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.helper.SysPolicyComparator;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysPolicy.bean.SysPolicyBean;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;

/**
 * 
 * @author vivek.goyal
 *
 */

@Named("sysPolicyController")
@ConversationScoped
public class SysPolicyController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @SuppressWarnings("unused")
    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPolicyController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysPolicyService sysPolicyService;

    @Inject
    SysPolicyBean sysPolicyBean;

    @Inject
    SysContentTypeService sysContentTypeService;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        List<SysContentTypeTO> sysContentTypeTOList = null;
        List<SysPolicyTO> sysPolicyTOList = null;
        ServiceResponse response = null;
        Set<String> name = null;
        SysPolicyTO sysPolicyTO = null;
        try {
            sysContentTypeTOList = new ArrayList<SysContentTypeTO>();
            RequestContext requestContext = RequestContext.getCurrentInstance();
            sysPolicyTOList = sysPolicyService.getSysPolicyList();
            sysContentTypeTOList = sysContentTypeService.getSysContentTypeList();
            sysPolicyBean.setSysPolicyTOList(sysPolicyTOList);
            sysPolicyBean.setSysContentTypeTOList(sysContentTypeTOList);
            sysPolicyBean.setRenderSysPolicyPopup(false);
            sysPolicyBean.setRenderErrorMessage(false);
            menuBean.setPage("../SysPolicy/sysPolicy.xhtml");
            requestContext.execute(ISysPolicyConstants.SYSPOLICY_DATATABLE_RESET);
            requestContext.execute(ISysPolicyConstants.SYSPOLICY_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void resetSysPolicy()
    {
        sysPolicyBean.setSysPolicyTOList(null);
        sysPolicyBean.setRenderSysPolicyPopup(true);
        sysPolicyBean.setModuleID(null);
        sysPolicyBean.setModuleName(null);
        sysPolicyBean.setEntityID(null);
        sysPolicyBean.setEntityName(null);
        sysPolicyBean.setPolicyName(null);
        sysPolicyBean.setPolicyCode(null);
        sysPolicyBean.setPolicyLabel(null);
        sysPolicyBean.setSysPolicyID(null);
        sysPolicyBean.setPolicyTypeID(null);
        getNewSysModule();
        getNewSysEntity();
    }

    public void getNewSysModule()
    {
        List<SysPolicyTO> sysModuleList = null;
        try {
            sysModuleList = sysPolicyService.getNewSysModuleList();
            sysPolicyBean.setSysModuleTOList(sysModuleList);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void getNewSysEntity()
    {
        List<SysPolicyTO> sysEntityList = null;
        try {
            sysEntityList = sysPolicyService.getNewSysEntityList();
            sysPolicyBean.setSysEntityTOList(sysEntityList);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    public void saveSysPolicy()
    {
        SysPolicyTO sysPolicyTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPolicyTO = new SysPolicyTO();
            sysPolicyBean.setRenderErrorMessage(true);
            sysPolicyTO.setModuleID(sysPolicyBean.getModuleID());
            sysPolicyTO.setEntityID(sysPolicyBean.getEntityID());
            sysPolicyTO.setPolicyCode(sysPolicyBean.getPolicyCode().replace(" ", ""));
            sysPolicyTO.setPolicyLabel(sysPolicyBean.getPolicyLabel());
            sysPolicyTO.setPolicyName(sysPolicyBean.getPolicyName());
            sysPolicyTO.setSysModuleTOList(sysPolicyBean.getSysModuleTOList());
            sysPolicyTO.setSysEntityTOList(sysPolicyBean.getSysEntityTOList());
            sysPolicyTO.setPolicyTypeID(sysPolicyBean.getPolicyTypeID());

            if (validateSysPolicy(sysPolicyTO)) {
                sysPolicyService.saveSysPolicy(sysPolicyTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Policy saved successfully", ""));
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_ADD_DIALOG_HIDE);
                requestContext.update("sysPolicyListForm");
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_DATATABLE_RESET);
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_PAGINATION_DATATABLE_RESET);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            sysPolicyBean.setRenderSysPolicyPopup(false);
        }
    }

    //To validate if the newly added SysPolicy is already present in the system
    public boolean validateSysPolicy(SysPolicyTO sysPolicyTO)
    {
        List<SysPolicyTO> sysPolicyTOList = null;
        int j = 0;
        try {
            if (sysPolicyTO.getPolicyCode().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyCode().trim().equalsIgnoreCase(null)
                    || sysPolicyTO.getPolicyLabel().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyLabel().trim().equalsIgnoreCase(null)
                    || sysPolicyTO.getPolicyName().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyName().trim().equalsIgnoreCase(null)) {
                j = 1;
                if (sysPolicyTO.getPolicyCode().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyCode().trim().equalsIgnoreCase(null)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysPolicyCode", ""));
                }
                if (sysPolicyTO.getPolicyLabel().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyLabel().trim().equalsIgnoreCase(null)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysPolicyLabel", ""));
                }
                if (sysPolicyTO.getPolicyName().trim().equalsIgnoreCase("") || sysPolicyTO.getPolicyName().trim().equalsIgnoreCase(null)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysPolicyName", ""));
                }
            } else {
                sysPolicyTOList = sysPolicyService.getSysPolicyList();
                for (int i = 0; i < sysPolicyTOList.size(); i++) {
                    if (sysPolicyTOList.get(i).getModuleID().equals(sysPolicyTO.getModuleID()) && sysPolicyTOList.get(i).getEntityID().equals(sysPolicyTO.getEntityID())
                            && sysPolicyTOList.get(i).getPolicyCode().equalsIgnoreCase(sysPolicyTO.getPolicyCode())
                            && !sysPolicyTOList.get(i).getSysPolicyID().equals(sysPolicyTO.getSysPolicyID())) {
                        j = 1;
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + sysPolicyTO.getPolicyCode() + "' PolicyCode is already present under the module '"
                                        + sysPolicyTOList.get(i).getModuleName() + "' tagged with the entity '" + sysPolicyTOList.get(i).getEntityName() + "'", ""));
                        sysPolicyBean.setRenderErrorMessage(true);
                        break;
                    }

                }

            }

            if (j == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            return false;
        }
    }

    public void updateEditSysPolicy(SysPolicyTO sysPolicyTO)
    {
        List<SysPolicyTO> sysPolicyTOList = null;
        Set<SysPolicyTO> sysPolicyList = null;
        try {
            sysPolicyList = new TreeSet<SysPolicyTO>(new SysPolicyComparator());
            initialize();
            getNewSysEntity();
            getNewSysModule();
            sysPolicyBean.setModuleID(sysPolicyTO.getModuleID());
            sysPolicyBean.setEntityID(sysPolicyTO.getEntityID());
            sysPolicyBean.setPolicyCode(sysPolicyTO.getPolicyCode().replace(" ", ""));
            sysPolicyBean.setPolicyLabel(sysPolicyTO.getPolicyLabel());
            sysPolicyBean.setPolicyName(sysPolicyTO.getPolicyName());
            sysPolicyBean.setSysPolicyID(sysPolicyTO.getSysPolicyID());
            sysPolicyBean.setPolicyTypeID(sysPolicyTO.getPolicyTypeID());

            sysPolicyBean.setDuplicateModuleID(sysPolicyTO.getModuleID());
            sysPolicyBean.setDuplicateEntityID(sysPolicyTO.getEntityID());
            sysPolicyBean.setDuplicatePolicyCode(sysPolicyTO.getPolicyCode().replace(" ", ""));
            sysPolicyBean.setDuplicatePolicyLabel(sysPolicyTO.getPolicyLabel());
            sysPolicyBean.setDuplicatePolicyName(sysPolicyTO.getPolicyName());
            sysPolicyBean.setDuplicatePolicyTypeID(sysPolicyTO.getPolicyTypeID());

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void updateSysPolicy()
    {
        SysPolicyTO sysPolicyTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPolicyTO = new SysPolicyTO();
            sysPolicyTO.setModuleID(sysPolicyBean.getModuleID());
            if (sysPolicyBean.getEntityID() != null && sysPolicyBean.getEntityID().intValue() != 0) {
                sysPolicyTO.setEntityID(sysPolicyBean.getEntityID());
            }
            sysPolicyTO.setPolicyCode(sysPolicyBean.getPolicyCode().replace(" ", ""));
            sysPolicyTO.setPolicyLabel(sysPolicyBean.getPolicyLabel());
            sysPolicyTO.setPolicyName(sysPolicyBean.getPolicyName());
            sysPolicyTO.setSysPolicyID(sysPolicyBean.getSysPolicyID());
            sysPolicyTO.setPolicyTypeID(sysPolicyBean.getPolicyTypeID());
            if (sysPolicyTO.getModuleID().equals(sysPolicyBean.getDuplicateModuleID()) && sysPolicyTO.getEntityID().equals(sysPolicyBean.getDuplicateEntityID())
                    && sysPolicyTO.getPolicyCode().trim().equalsIgnoreCase(sysPolicyBean.getDuplicatePolicyCode())
                    && sysPolicyTO.getPolicyLabel().trim().equalsIgnoreCase(sysPolicyBean.getDuplicatePolicyLabel())
                    && sysPolicyTO.getPolicyName().trim().equalsIgnoreCase(sysPolicyBean.getDuplicatePolicyName())
                    && sysPolicyTO.getPolicyTypeID().equals(sysPolicyBean.getDuplicatePolicyTypeID())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes were made.", ""));
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_EDIT_DIALOG_HIDE);
            } else if (validateSysPolicy(sysPolicyTO)) {
                sysPolicyService.updateSysPolicy(sysPolicyTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysPolicy updated successfully", ""));
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_DATATABLE_RESET);
                requestContext.execute(ISysPolicyConstants.SYSPOLICY_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysPolicyListForm");
            }


        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPolicyConstants.SYSPOLICY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
        }
    }
}