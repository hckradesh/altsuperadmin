/**
 * 
 */
package com.talentpact.ui.dataupload.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import org.primefaces.context.RequestContext;

import com.talentpact.business.dataservice.DataUploadService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dataupload.bean.DataUploadBean;
import com.talentpact.ui.dataupload.constants.MessageLevel;
import com.talentpact.ui.dataupload.helper.DataUploadHelper;
import com.talentpact.ui.dataupload.to.SysUploadTemplateTypeTO;
import com.talentpact.ui.dataupload.to.TableColumnTO;
import com.talentpact.ui.dataupload.to.ValidationTypeTO;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * @author shivam.kumar
 *
 */

@Named("dataUploadController")
@ConversationScoped
public class DataUploadController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    DataUploadService dataUploadService;

    @Inject
    DataUploadHelper dataUploadHelper;

    @Inject
    DataUploadBean dataUploadBean;


    @Override
    public void initialize()
    {
        List<SysUploadTemplateTypeTO> sysUploadTemplateTypeTOList = null;
        try {
            resetDataUpload(dataUploadBean);
            resetDataUploadEdit(dataUploadBean);
            dataUploadBean.setTableNames(null);
            dataUploadBean.setTableNamesFiltered(null);
            dataUploadBean.setPersistenceUnitName(null);
            sysUploadTemplateTypeTOList = dataUploadService.getSysUploadTemplateTypeList();
            dataUploadBean.setTemplateTypeTOList(sysUploadTemplateTypeTOList);
            menuBean.setPage("../dataUpload.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void viewTemplateType(SysUploadTemplateTypeTO sysUploadTemplateTypeTO)
    {
        List<TableColumnTO> tableColumnTOList = null;
        RequestContext context = null;
        try {
            context = RequestContext.getCurrentInstance();
            addTemplateType();
            tableColumnTOList = dataUploadService.getTemplateTypeDetail(sysUploadTemplateTypeTO);
            dataUploadBean.setTableColumnTOList(tableColumnTOList);
            dataUploadBean.setTemplateTypeName(sysUploadTemplateTypeTO.getTemplateTypeName());
            dataUploadBean.setTemplateTypeID(sysUploadTemplateTypeTO.getTemplateTypeID());
            dataUploadBean.setUploadTableID(sysUploadTemplateTypeTO.getUploadTableID());
            dataUploadBean.setViewMode(true);
            dataUploadBean.setViewAddTempleteType(null);
            dataUploadBean.setViewAddTempleteType(false);
            context.update("du");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addTemplateType()
    {
        List<SelectItem> uploadTableDropDown = null;
        RequestContext context = null;
        try {
            context = RequestContext.getCurrentInstance();
            dataUploadBean.setViewAddTempleteType(true);
            context.update("du");
            resetDataUploadEdit(dataUploadBean);
            uploadTableDropDown = dataUploadHelper.getSelectItemIDList(dataUploadService.getSysUploadTableList());
            dataUploadBean.setUploadTableDropDown(uploadTableDropDown);
            dataUploadBean.setViewMode(false);
            menuBean.setPage("../dataUploadEdit.xhtml");
            dataUploadBean.setColumnList(null);
            dataUploadBean.setTableName(null);
            dataUploadBean.setValidationList(dataUploadService.getSysValidationTypeList(false));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onTableSelection()
    {
        Integer uploadTableID = null;
        List<TableColumnTO> tableColumnTOList = null;
        try {
            dataUploadBean.setColumnList(null);
            dataUploadBean.setTableName(null);
            dataUploadBean.setTableColumnTOList(null);

            uploadTableID = dataUploadBean.getUploadTableID();
            if (uploadTableID == null || (uploadTableID != null && uploadTableID.intValue() <= 0)) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Select table properly.", "");
                return;
            }
            tableColumnTOList = dataUploadService.getTableColumnTOList(uploadTableID);
            dataUploadBean.setTableColumnTOList(tableColumnTOList);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void goBack()
    {
        try {
            initialize();
            menuBean.setPage("../dataUpload.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDataUpload(DataUploadBean dataUploadBean)
    {
        dataUploadBean.setTemplateTypeTOList(null);
        dataUploadBean.setTableNamesFiltered(null);
    }

    private void resetDataUploadEdit(DataUploadBean dataUploadBean)
    {
        dataUploadBean.setTemplateTypeDropDown(null);
        dataUploadBean.setTemplateTypeName(null);
        dataUploadBean.setUploadTableDropDown(null);
        dataUploadBean.setUploadTableID(0);
        dataUploadBean.setTableColumnTOList(null);
        dataUploadBean.setTableNamesFiltered(null);
    }

    /**
     * 
     */
    public void saveTemplateType()
    {
        SysUploadTemplateTypeTO sysUploadTemplateTypeTO = null;
        List<TableColumnTO> tableColumnTOList = null;
        try {
            tableColumnTOList = dataUploadBean.getTableColumnTOList();
            if (dataUploadBean.getTemplateTypeName() == null || dataUploadBean.getTemplateTypeName().trim().equals("")) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Please enter template type name.", "");
                return;
            }
            if (dataUploadBean.getUploadTableID() == null || dataUploadBean.getUploadTableID() == 0) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Please select a table.", "");
                return;
            }
            if (tableColumnTOList == null) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "No column reference found.", "");
                return;
            }
            if (dataUploadBean.getTemplateTypeTOList() != null && !dataUploadBean.getTemplateTypeTOList().isEmpty()) {
                for (SysUploadTemplateTypeTO to : dataUploadBean.getTemplateTypeTOList()) {
                    if (to.getTemplateTypeName() != null && to.getTemplateTypeName().equalsIgnoreCase(dataUploadBean.getTemplateTypeName())
                            && to.getTemplateTypeID().intValue() != dataUploadBean.getTemplateTypeID().intValue()) {
                        DataUploadHelper.showMessage(MessageLevel.ERROR, "Template name already exists.", "");
                        return;
                    }
                }
            }

            for (TableColumnTO tableColumnTO : tableColumnTOList) {
                if (tableColumnTO.getAttributeName() == null || tableColumnTO.getAttributeName().trim().equals("")) {
                    DataUploadHelper.showMessage(MessageLevel.ERROR, "Please enter attribute name for " + tableColumnTO.getColumnName(), "");
                    return;
                } else if (tableColumnTO.getValidationList() != null && !tableColumnTO.getValidationList().isEmpty()) {
                    String data = "";
                    for (ValidationTypeTO to : tableColumnTO.getValidationList()) {
                        data = data + (data.isEmpty() ? "" : ",") + String.valueOf(to.getValidationTypeID());
                    }
                    tableColumnTO.setValidationType(data);
                }
            }
            sysUploadTemplateTypeTO = new SysUploadTemplateTypeTO();
            sysUploadTemplateTypeTO.setTemplateTypeName(dataUploadBean.getTemplateTypeName());
            sysUploadTemplateTypeTO.setUploadTableID(dataUploadBean.getUploadTableID());
            dataUploadService.saveTemplateType(sysUploadTemplateTypeTO, tableColumnTOList);
            DataUploadHelper.showMessage(MessageLevel.INFO, "Template type saved successfully.", "");
            goBack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     */
    public void searchTable()
    {
        String persistenceUnitName = null;
        List<String> tableNames = null;
        try {
            dataUploadBean.setTableNames(null);
            dataUploadBean.setTableNamesFiltered(null);
            persistenceUnitName = dataUploadBean.getPersistenceUnitName();
            if (persistenceUnitName == null || persistenceUnitName.trim().equals("")) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Please properly enter persistence name.", "");
                return;
            }
            tableNames = dataUploadService.getTableNames(persistenceUnitName);
            dataUploadBean.setTableNames(tableNames);
            dataUploadBean.setTableNamesFiltered(tableNames);
            RequestContext.getCurrentInstance().execute("PF('tableList').clearFilters();");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getTableColumn(String tablename)
    {
        String persistenceUnitName = null;
        try {
            dataUploadBean.setColumnList(null);
            dataUploadBean.setTableName(null);
            dataUploadBean.setUploadTableID(null);
            dataUploadBean.setTableColumnTOList(null);

            if (tablename.equalsIgnoreCase("NONE")) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Please select database.", "");
                return;
            }
            persistenceUnitName = dataUploadBean.getPersistenceUnitName();
            if (persistenceUnitName == null || persistenceUnitName.trim().equals("")) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Please properly enter persistence name.", "");
                return;
            }
            
            if (dataUploadBean.getUploadTableDropDown() != null && !dataUploadBean.getUploadTableDropDown().isEmpty()) {
                for (SelectItem si : dataUploadBean.getUploadTableDropDown()) {
                    if (si.getLabel().equalsIgnoreCase(tablename)) {
                        dataUploadBean.setUploadTableID(Integer.parseInt(si.getValue().toString()));
                        break;
                    }
                }
            }
            if (dataUploadBean.getUploadTableID() == null) {
                List<TableColumnTO> colLst = dataUploadService.getTableColumnsNames(persistenceUnitName, tablename);
                if (colLst != null && !colLst.isEmpty()) {
                    dataUploadBean.setColumnList(colLst);
                    dataUploadBean.setTableName(tablename);
                }
            } else {
                List<TableColumnTO> colLst = dataUploadService.getExistingTableColumnsNames(persistenceUnitName, tablename);
                if (colLst != null && !colLst.isEmpty()) {
                    dataUploadBean.setColumnList(colLst);
                    dataUploadBean.setTableName(tablename);
                }
            }
            
            if (dataUploadBean.getColumnList() == null || (dataUploadBean.getColumnList() != null && dataUploadBean.getColumnList().isEmpty())) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "No Column to display.", "");
            }

        } catch (Exception e) {
            if (e.getCause() instanceof NoResultException) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "No primary key exists in target table.", "");
            } else {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "Some problem occur while adding table.", "");
            }
            e.printStackTrace();
        }
    }

    public void addValidation(Integer rowIndex)
    {
        List<TableColumnTO> columnList = null;
        List<ValidationTypeTO> validationList = null;
        try {
            columnList = dataUploadBean.getColumnList();
            validationList = columnList.get(rowIndex).getValidationList();
            validationList.add(new ValidationTypeTO());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeValidation(Integer rowIndex, Integer columnIndex)
    {
        List<TableColumnTO> columnList = null;
        List<ValidationTypeTO> validationList = null;
        try {
            columnList = dataUploadBean.getColumnList();
            validationList = columnList.get(rowIndex).getValidationList();
            if (validationList != null && !validationList.isEmpty()) {
                validationList.remove(columnIndex.intValue());
            }
            if (validationList == null) {
                validationList = new ArrayList<ValidationTypeTO>();
                validationList.add(new ValidationTypeTO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void saveValidationMapping()
    {
        List<TableColumnTO> columnLst = dataUploadBean.getColumnList();
        if (columnLst != null && !columnLst.isEmpty()) {
            for (TableColumnTO to : columnLst) {
                if (to.getValidationList() != null && !to.getValidationList().isEmpty()) {
                    if (to.getValidationSet() == null) {
                        to.setValidationSet(new HashSet<Integer>());
                    }
                    for (ValidationTypeTO innerTo : to.getValidationList()) {
                        if (innerTo.getValidationTypeID() > 0) {
                            to.getValidationSet().add(innerTo.getValidationTypeID());
                            String data = "";
                            if (to.getValidationType() == null) {
                                data = String.valueOf(innerTo.getValidationTypeID());
                            } else {
                                data = to.getValidationType() + (to.getValidationType().isEmpty() ? "" : ",") + String.valueOf(innerTo.getValidationTypeID());
                            }
                            to.setValidationType(data);
                        }
                    }
                }
            }
            boolean flag = dataUploadService.saveTableColumn(dataUploadBean.getPersistenceUnitName(), dataUploadBean.getTableName(), columnLst);
            if (flag) {
                dataUploadBean.setTableName(null);
                dataUploadBean.setUploadTableID(null);
                dataUploadBean.setTableColumnTOList(null);
                DataUploadHelper.showMessage(MessageLevel.INFO, "Data Saved successfully.", "");
            }
        } else {
            DataUploadHelper.showMessage(MessageLevel.ERROR, "No Column to save.", "");
            return;
        }
        dataUploadBean.setColumnList(null);
    }

    public void saveTableColumn()
    {
        SysUploadTemplateTypeTO sysUploadTemplateTypeTO = null;
        List<TableColumnTO> columnList = null;
        try {
            columnList = dataUploadBean.getColumnList();
            if (columnList == null) {
                DataUploadHelper.showMessage(MessageLevel.ERROR, "No column reference found.", "");
                return;
            }
            for (TableColumnTO tableColumnTO : columnList) {
                if (tableColumnTO.getValidationList() != null && !tableColumnTO.getValidationList().isEmpty()) {
                    String data = "";
                    for (ValidationTypeTO to : tableColumnTO.getValidationList()) {
                        if (to.getValidationTypeID() > 0) {
                            data = data + (data.isEmpty() ? "" : ",") + String.valueOf(to.getValidationTypeID());
                        }
                    }
                    data = data.replaceAll(" ", "");
                    tableColumnTO.setValidationType(data.trim());
                }
            }
            sysUploadTemplateTypeTO = new SysUploadTemplateTypeTO();
            sysUploadTemplateTypeTO.setTemplateTypeName(dataUploadBean.getTemplateTypeName());
            sysUploadTemplateTypeTO.setUploadTableID(dataUploadBean.getUploadTableID());
            String result = dataUploadService.insertTableAndColumn(dataUploadBean.getPersistenceUnitName(), dataUploadBean.getUploadTableID(), dataUploadBean.getTableName(),
                    columnList);
            if (result == null || (result != null && result.isEmpty())) {
                DataUploadHelper.showMessage(MessageLevel.INFO, "Columns saved successfully.", "");
                dataUploadBean.setColumnList(null);
                dataUploadBean.setTableName(null);
                dataUploadBean.setUploadTableID(null);
                goBack();
            } else {
                DataUploadHelper.showMessage(MessageLevel.ERROR, result, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
