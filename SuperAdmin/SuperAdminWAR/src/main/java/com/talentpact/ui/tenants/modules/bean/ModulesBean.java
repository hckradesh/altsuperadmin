/**
 * 
 */
package com.talentpact.ui.tenants.modules.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.tenants.modules.controller.ModulesController;

/**
 * @author radhamadhab.dalai
 *
 */
@ConversationScoped
@Named("modulesBean")
public class ModulesBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    ModulesController modulesController;

    private Integer number;

    private List<SelectItem> itemList;


    /**
     * @return the itemList
     */
    public List<SelectItem> getItemList()
    {
        return itemList;
    }


    /**
     * @param itemList the itemList to set
     */
    public void setItemList(List<SelectItem> itemList)
    {
        this.itemList = itemList;
    }


    /**
     * @return the number
     */
    public Integer getNumber()
    {
        return number;
    }


    /**
     * @param number the number to set
     */
    public void setNumber(Integer number)
    {
        this.number = number;
    }


    public void initialize()
    {
    	endConversation();
        beginConversation();
        modulesController.initialize();

    }


}
