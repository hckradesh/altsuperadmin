/**
 * 
 */
package com.talentpact.ui.sysEntity.dataservice;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.dataservice.sysEntity.SysEntityDS;
import com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO;


/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysEntityDataService {

	@EJB
	private SysEntityDS sysEntityDS;
	/**
	 * @return
	 */
	public List<SysEntityResponseTO> getAllEntities() throws Exception {
		return sysEntityDS.getAllEntities();
	}

}
