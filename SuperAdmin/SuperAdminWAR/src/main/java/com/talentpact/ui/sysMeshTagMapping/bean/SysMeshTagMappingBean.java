package com.talentpact.ui.sysMeshTagMapping.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagMappingTO;
import com.talentpact.business.application.transport.output.SysMeshTagType;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.model.SysMeshModule;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysContentCategory.controller.SysContentCategoryController;
import com.talentpact.ui.sysMeshTagMapping.controller.SysMeshTagMappingController;


@Named("sysMeshTagMappingBean")
@ConversationScoped
public class SysMeshTagMappingBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysMeshTagMappingController sysMeshTagMappingController;

    private Integer sysMeshTagMappingID;
    
    private Integer sysMeshTagModuleID;

    private String sysMeshTagType;
    
    private String sysMeshModuleName;
    
    private List<SysMeshModuleTO> sysMeshModules;
    
    private List<SysMeshTagType> sysMeshTagTypes;
    
    private List<SysMeshTagMappingTO> sysMeshTagMappingTOList;
    
    private Boolean renderUpdateButton;
    
    private boolean renderErrorMessage;
    
    private boolean renderSysMeshTagMappingPopup;

    private boolean renderEditSysMeshTagMappingPopup;

    private boolean renderMessageBox;

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            sysMeshTagMappingController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

	public Integer getSysMeshTagMappingID() {
		return sysMeshTagMappingID;
	}

	public void setSysMeshTagMappingID(Integer sysMeshTagMappingID) {
		this.sysMeshTagMappingID = sysMeshTagMappingID;
	}

	public Integer getSysMeshTagModuleID() {
		return sysMeshTagModuleID;
	}

	public void setSysMeshTagModuleID(Integer sysMeshTagModuleID) {
		this.sysMeshTagModuleID = sysMeshTagModuleID;
	}

	public String getSysMeshTagType() {
		return sysMeshTagType;
	}

	public void setSysMeshTagType(String sysMeshTagType) {
		this.sysMeshTagType = sysMeshTagType;
	}

	public String getSysMeshModuleName() {
		return sysMeshModuleName;
	}

	public void setSysMeshModuleName(String sysMeshModuleName) {
		this.sysMeshModuleName = sysMeshModuleName;
	}

	public List<SysMeshTagMappingTO> getSysMeshTagMappingTOList() {
		return sysMeshTagMappingTOList;
	}

	public void setSysMeshTagMappingTOList(List<SysMeshTagMappingTO> sysMeshTagMappingTOList) {
		this.sysMeshTagMappingTOList = sysMeshTagMappingTOList;
	}

	public List<SysMeshModuleTO> getSysMeshModules() {
		return sysMeshModules;
	}

	public void setSysMeshModules(List<SysMeshModuleTO> list) {
		this.sysMeshModules = list;
	}

	public List<SysMeshTagType> getSysMeshTagTypes() {
		return sysMeshTagTypes;
	}

	public void setSysMeshTagTypes(List<SysMeshTagType> list) {
		this.sysMeshTagTypes = list;
	}

	public void setRenderErrorMessage(boolean b) {
		// TODO Auto-generated method stub
		
	}

	public Boolean getRenderUpdateButton() {
		return renderUpdateButton;
	}

	public void setRenderUpdateButton(Boolean renderUpdateButton) {
		this.renderUpdateButton = renderUpdateButton;
	}

	public boolean isRenderSysMeshTagMappingPopup() {
		return renderSysMeshTagMappingPopup;
	}

	public void setRenderSysMeshTagMappingPopup(boolean renderSysMeshTagMappingPopup) {
		this.renderSysMeshTagMappingPopup = renderSysMeshTagMappingPopup;
	}

	public boolean isRenderEditSysMeshTagMappingPopup() {
		return renderEditSysMeshTagMappingPopup;
	}

	public void setRenderEditSysMeshTagMappingPopup(boolean renderEditSysMeshTagMappingPopup) {
		this.renderEditSysMeshTagMappingPopup = renderEditSysMeshTagMappingPopup;
	}

	public boolean isRenderMessageBox() {
		return renderMessageBox;
	}

	public void setRenderMessageBox(boolean renderMessageBox) {
		this.renderMessageBox = renderMessageBox;
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

}