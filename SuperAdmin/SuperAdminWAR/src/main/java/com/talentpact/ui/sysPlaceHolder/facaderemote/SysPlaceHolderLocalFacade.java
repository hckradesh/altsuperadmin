/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.facaderemote;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;

/**
 * @author vaibhav.kashyap
 *
 */
@Local
public interface SysPlaceHolderLocalFacade {

	public ServiceResponse getAllPlaceHolderData() 
			throws Exception;

	/**
	 * @param serviceRequest
	 * @return
	 */
	public ServiceResponse insertNewPlaceHolder(ServiceRequest serviceRequest)
		throws Exception;

	/**
	 * @param serviceRequest
	 * @return
	 */
	public ServiceResponse deletePlaceHolderByID(ServiceRequest serviceRequest)
		throws Exception;

	/**
	 * @param serviceRequest
	 * @return
	 */
	public ServiceResponse updateNewPlaceHolder(ServiceRequest serviceRequest)
		throws Exception;
	
}
