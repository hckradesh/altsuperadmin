/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.dataservice.DS;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysEntityColumn;
import com.talentpact.model.SysPlaceholder;
import com.talentpact.model.SysRule;
import com.talentpact.ui.sysPlaceHolder.common.transport.input.SysPlaceHolderRequestTO;
import com.talentpact.ui.sysPlaceHolder.common.transport.output.SysPlaceHolderResponseTO;
import com.talentpact.ui.sysPlaceHolder.converter.SysPlaceHolderConverter;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPlaceHolderDS extends AbstractDS<SysPlaceholder>
{

    /**
     * @param entityClass
     */
    public SysPlaceHolderDS()
    {
        super(SysPlaceholder.class);
    }

    /**
     * {@link #getAllPlaceHolderData()} : fetches all place holder data
     * 
     * @author vaibhav.kashyap
     * @return List<SysPlaceHolderResponseTO>
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public List<SysPlaceHolderResponseTO> getAllPlaceHolderData()
        throws Exception
    {
        List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
        List<SysPlaceholder> sysPlaceHolderList = null;
        SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
        StringBuilder hqlQuery = null;
        Query query = null;

        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysPlaceholder from SysPlaceholder sysPlaceholder join fetch sysPlaceholder.rule ");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            sysPlaceHolderList = query.getResultList();
            if (sysPlaceHolderList != null && !sysPlaceHolderList.isEmpty()) {
                sysPlaceHolderResponseTOList = new LinkedList<SysPlaceHolderResponseTO>();
                for (SysPlaceholder placeHolderObj : sysPlaceHolderList) {
                    sysPlaceHolderResponseTO = SysPlaceHolderConverter.getSysPlaceHolderResponseFromSysPlaceHolder(placeHolderObj);
                    if (sysPlaceHolderResponseTO != null && sysPlaceHolderResponseTO.getSysPlaceHolderID() > 0) {
                        sysPlaceHolderResponseTOList.add(sysPlaceHolderResponseTO);
                    }
                }
            }
        } catch (Exception ex) {
            sysPlaceHolderResponseTOList = null;
            throw ex;
        } finally {

        }
        return sysPlaceHolderResponseTOList;
    }

    /**
     * {@link #insertNewPlaceHolder(SysPlaceHolderRequestTO)} : inserts new
     * place holder
     * 
     * @author vaibhav.kashyap
     * @param sysPlaceHolderRequestTO
     * @return SysPlaceHolderResponseTO
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public SysPlaceHolderResponseTO insertNewPlaceHolder(SysPlaceHolderRequestTO sysPlaceHolderRequestTO)
        throws Exception
    {
        SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
        SysPlaceholder sysPlaceHolder = null;
        SysRule sysRule = null;
        SysEntityColumn sysEntityColumn = null;
        //SysCommunicationType sysCommunicationType = null;
        try {
            sysPlaceHolder = new SysPlaceholder();

            /*
             * sysCommunicationType =
             * getEntityManager("Talentpact").find(SysCommunicationType
             * .class,sysPlaceHolderRequestTO.getCommTypeID());
             * if(sysCommunicationType!=null)
             * sysPlaceHolder.setSysCommunicationtype(sysCommunicationType);
             */
            if (sysPlaceHolderRequestTO.getModuleID() != null && sysPlaceHolderRequestTO.getModuleID() > 0) {
                sysPlaceHolder.setModuleID(sysPlaceHolderRequestTO.getModuleID());
            }

            if (sysPlaceHolderRequestTO.getPlaceHolderName() != null && !sysPlaceHolderRequestTO.getPlaceHolderName().equals(""))
                sysPlaceHolder.setPlaceHolder(sysPlaceHolderRequestTO.getPlaceHolderName());

            if (sysPlaceHolderRequestTO.getPlaceHolderDescription() != null && !sysPlaceHolderRequestTO.getPlaceHolderDescription().equals(""))
                sysPlaceHolder.setPlaceholderDesc(sysPlaceHolderRequestTO.getPlaceHolderDescription());

            if (sysPlaceHolderRequestTO.getColumnExpression() != null && !sysPlaceHolderRequestTO.getColumnExpression().equals(""))
                sysPlaceHolder.setColumnExpression(sysPlaceHolderRequestTO.getColumnExpression());

            if (sysPlaceHolderRequestTO.getWhereCondition() != null && !sysPlaceHolderRequestTO.getWhereCondition().equals(""))
                sysPlaceHolder.setWhereCondition(sysPlaceHolderRequestTO.getWhereCondition());

            if (sysPlaceHolderRequestTO.getPlaceHolderTypeID() != null && sysPlaceHolderRequestTO.getPlaceHolderTypeID() > 0)
                sysPlaceHolder.setPlaceholderTypeID(sysPlaceHolderRequestTO.getPlaceHolderTypeID());

            if (sysPlaceHolderRequestTO.getLevelType() != null && sysPlaceHolderRequestTO.getLevelType() > 0)
                sysPlaceHolder.setLevelTypeID(sysPlaceHolderRequestTO.getLevelType());

            if (sysPlaceHolderRequestTO.getComplexityType() != null && sysPlaceHolderRequestTO.getComplexityType() > 0)
                sysPlaceHolder.setComplexityTypeID(sysPlaceHolderRequestTO.getComplexityType());

            if (sysPlaceHolderRequestTO.getEntityPath() != null && !sysPlaceHolderRequestTO.getEntityPath().equals(""))
                sysPlaceHolder.setEntityPath(sysPlaceHolderRequestTO.getEntityPath());

            sysEntityColumn = getEntityManager("TalentPact").find(SysEntityColumn.class, sysPlaceHolderRequestTO.getEntityColumnID());
            if (sysEntityColumn != null){
                sysPlaceHolder.setEntityColumnID(sysEntityColumn.getEntityColumnID());
                }
            sysRule = getEntityManager("Talentpact").find(SysRule.class, sysPlaceHolderRequestTO.getRuleID());
            if (sysRule != null)
                sysPlaceHolder.setSysRule(sysRule);

            sysPlaceHolder.setTenantID(sysPlaceHolderRequestTO.getTenantID());
            sysPlaceHolder.setModifiedBy(sysPlaceHolderRequestTO.getModifiedBy());
            sysPlaceHolder.setCreatedBy(sysPlaceHolderRequestTO.getCreatedBy());
            sysPlaceHolder.setCreatedDate(sysPlaceHolderRequestTO.getCreatedDate());
            sysPlaceHolder.setModifiedDate(sysPlaceHolderRequestTO.getModifiedDate());

            getEntityManager("Talentpact").persist(sysPlaceHolder);

            sysPlaceHolderResponseTO = SysPlaceHolderConverter.getSysPlaceHolderResponseFromSysPlaceHolder(sysPlaceHolder);

        } catch (Exception ex) {
            sysPlaceHolderResponseTO = null;
            throw ex;
        } finally {

        }
        return sysPlaceHolderResponseTO;
    }

    /**
     * {@link #deletePlaceHolderByID(Long)} : deleted place holder by ID
     * 
     * @author vaibhav.kashyap
     * @return Integer
     * @throws Exception
     * @param placeHolderID
     * */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public int deletePlaceHolderByID(Long placeHolderID)
        throws Exception
    {
        int deletedRowCount = -1;
        StringBuilder hqlQuery = null;
        Query query = null;
        SysPlaceholder sysPlaceHolderObj = null;
        try {
            /**
             * check if object being deleted exists or not
             * */
            sysPlaceHolderObj = getEntityManager("Talentpact").find(SysPlaceholder.class, placeHolderID);
            if (sysPlaceHolderObj == null)
                return -1;

            hqlQuery = new StringBuilder();
            hqlQuery.append("delete from SysPlaceholder sph where sph.sysPlaceholderID = :placeHolderID");
            query = getEntityManager("Talentpact").createQuery(hqlQuery.toString());
            query.setParameter("placeHolderID", placeHolderID);
            deletedRowCount = query.executeUpdate();
        } catch (Exception ex) {
            deletedRowCount = -1;
        } finally {

        }
        return deletedRowCount;
    }

    /**
     * {@link #updatePlaceHolder(SysPlaceHolderRequestTO)} : update existing
     * place holder
     * 
     * @param sysPlaceHolderRequestTO
     * @return SysPlaceHolderRequestTO
     * @throws Exception
     * @author vaibhav.kashyap
     */
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public SysPlaceHolderResponseTO updatePlaceHolder(SysPlaceHolderRequestTO sysPlaceHolderRequestTO)
        throws Exception
    {
        SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
        SysPlaceholder sysPlaceHolder = null;
        SysRule sysRule = null;
        SysEntityColumn sysEntityColumn = null;
        //SysCommunicationType sysCommunicationType = null;
        try {
            if (sysPlaceHolderRequestTO.getPlaceHolderID() != null) {
                sysPlaceHolder = getEntityManager("Talentpact").find(SysPlaceholder.class, sysPlaceHolderRequestTO.getPlaceHolderID());
                if (sysPlaceHolder == null) {
                    return null;
                }
            } else {
                return null;
            }

            /*sysCommunicationType = getEntityManager("Talentpact").find(
            		SysCommunicationType.class,
            		sysPlaceHolderRequestTO.getCommTypeID());
            if (sysCommunicationType != null)
            	sysPlaceHolder.setSysCommunicationtype(sysCommunicationType);*/
            if (sysPlaceHolderRequestTO.getModuleID() != null && sysPlaceHolderRequestTO.getModuleID() > 0) {
                sysPlaceHolder.setModuleID(sysPlaceHolderRequestTO.getModuleID());
            }


            if (sysPlaceHolderRequestTO.getPlaceHolderName() != null && !sysPlaceHolderRequestTO.getPlaceHolderName().equals(""))
                sysPlaceHolder.setPlaceHolder(sysPlaceHolderRequestTO.getPlaceHolderName());

            if (sysPlaceHolderRequestTO.getPlaceHolderDescription() != null && !sysPlaceHolderRequestTO.getPlaceHolderDescription().equals(""))
                sysPlaceHolder.setPlaceholderDesc(sysPlaceHolderRequestTO.getPlaceHolderDescription());

            if (sysPlaceHolderRequestTO.getColumnExpression() != null && !sysPlaceHolderRequestTO.getColumnExpression().equals(""))
                sysPlaceHolder.setColumnExpression(sysPlaceHolderRequestTO.getColumnExpression());

            if (sysPlaceHolderRequestTO.getWhereCondition() != null && !sysPlaceHolderRequestTO.getWhereCondition().equals(""))
                sysPlaceHolder.setWhereCondition(sysPlaceHolderRequestTO.getWhereCondition());

            if (sysPlaceHolderRequestTO.getPlaceHolderTypeID() != null && sysPlaceHolderRequestTO.getPlaceHolderTypeID() > 0)
                sysPlaceHolder.setPlaceholderTypeID(sysPlaceHolderRequestTO.getPlaceHolderTypeID());

            if (sysPlaceHolderRequestTO.getLevelType() != null && sysPlaceHolderRequestTO.getLevelType() > 0)
                sysPlaceHolder.setLevelTypeID(sysPlaceHolderRequestTO.getLevelType());

            if (sysPlaceHolderRequestTO.getComplexityType() != null && sysPlaceHolderRequestTO.getComplexityType() > 0)
                sysPlaceHolder.setComplexityTypeID(sysPlaceHolderRequestTO.getComplexityType());

            if (sysPlaceHolderRequestTO.getEntityPath() != null && !sysPlaceHolderRequestTO.getEntityPath().equals(""))
                sysPlaceHolder.setEntityPath(sysPlaceHolderRequestTO.getEntityPath());

            sysEntityColumn = getEntityManager("TalentPact").find(SysEntityColumn.class, sysPlaceHolderRequestTO.getEntityColumnID());
            if (sysEntityColumn != null)
                ;
            sysPlaceHolder.setEntityColumnID(sysEntityColumn.getEntityColumnID());

            sysRule = getEntityManager("Talentpact").find(SysRule.class, sysPlaceHolderRequestTO.getRuleID());
            if (sysRule != null)
                sysPlaceHolder.setSysRule(sysRule);

            sysPlaceHolder.setTenantID(sysPlaceHolderRequestTO.getTenantID());
            sysPlaceHolder.setModifiedBy(sysPlaceHolderRequestTO.getModifiedBy());
            sysPlaceHolder.setCreatedBy(sysPlaceHolderRequestTO.getCreatedBy());
            sysPlaceHolder.setCreatedDate(sysPlaceHolderRequestTO.getCreatedDate());
            sysPlaceHolder.setModifiedDate(sysPlaceHolderRequestTO.getModifiedDate());

            getEntityManager("Talentpact").merge(sysPlaceHolder);

            sysPlaceHolderResponseTO = SysPlaceHolderConverter.getSysPlaceHolderResponseFromSysPlaceHolder(sysPlaceHolder);

        } catch (Exception ex) {
            sysPlaceHolderResponseTO = null;
            throw ex;
        } finally {

        }
        return sysPlaceHolderResponseTO;

    }

}
