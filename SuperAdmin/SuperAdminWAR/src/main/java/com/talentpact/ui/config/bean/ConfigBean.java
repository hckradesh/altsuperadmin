/**
 * 
 */
package com.talentpact.ui.config.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.payrole.config.transport.output.ConfigResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.config.controller.ConfigController;


/**
 * @author Rahul.Chabba
 * 
 */

@SuppressWarnings("serial")
@Named("configBean")
@SessionScoped
public class ConfigBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    ConfigController configController;

   // @EJB(lookup = "java:global/AltAdminEAR/AltAdminEJB/CtcFacade!com.alt.payrole.ctc.facade.CtcFacadeRemote")
   // private CtcFacadeRemote ctcFacadeRemote;

    @Inject
    ConfigBean configBean;

    private List<ConfigResponseTO> response = new ArrayList<ConfigResponseTO>();

	public List<ConfigResponseTO> getResponse() {
		return response;
	}
	public void setResponse(List<ConfigResponseTO> response) {
		this.response = response;
	}


	@Override
    public void initialize()
    {
        // TODO Auto-generated method stub

        try {
        	
        	endConversation();
            beginConversation();
            // TODO Auto-generated method stub
            configController.initialize();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

}
