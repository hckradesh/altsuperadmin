/**
 * 
 */
package com.talentpact.ui.login.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * @author radhamadhab.dalai
 *
 */

@SessionScoped
@Named("loginBean")
public class LoginBean implements Serializable

{

    private String userName;

    private String password;

    private String level;

    private String menuName;

    /**
     * @return the level
     */
    public String getLevel()
    {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(String level)
    {
        this.level = level;
    }

    /**
         * @return the userName
         */
    public String getUserName()
    {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the menuName
     */
    public String getMenuName()
    {
        return menuName;
    }

    /**
     * @param menuName the menuName to set
     */
    public void setMenuName(String menuName)
    {
        this.menuName = menuName;
    }


}
