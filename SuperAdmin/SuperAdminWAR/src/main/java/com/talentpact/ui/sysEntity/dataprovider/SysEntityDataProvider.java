/**
 * 
 */
package com.talentpact.ui.sysEntity.dataprovider;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO;
import com.talentpact.ui.sysEntity.dataservice.SysEntityDataService;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysEntityColumn.dataservice.SysEntityColumnDataService;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysEntityDataProvider {

	@EJB
	private SysEntityDataService sysEntityDataService;
	
	@EJB
	private SysEntityColumnDataService sysEntityColumnDataService;
	
	/**
	 * @return
	 */
	public ServiceResponse getAllEntities() throws Exception{
		ServiceResponse serviceResponse = null;
		List<SysEntityResponseTO> sysEntityResponseTOList = null;
		List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList = null;
		List<Integer> sysEntityIDList = null;
		try{
			sysEntityResponseTOList = sysEntityDataService.getAllEntities();
			if(sysEntityResponseTOList!=null && 
					!sysEntityResponseTOList.isEmpty()){
				sysEntityIDList = new ArrayList<Integer>();
				/**
				 * prepare entity ID list
				 * */
				for(SysEntityResponseTO entityResponseTO : sysEntityResponseTOList){
					sysEntityIDList.add(entityResponseTO.getEntityId());
				}
				
				/**
				 * based on entity fetched, fetch entity column
				 * */
				sysEntityColumnResponseTOList = sysEntityColumnDataService.getSysEntityColumnsByEntityID(
						sysEntityIDList);
				if(sysEntityColumnResponseTOList!=null && 
						!sysEntityColumnResponseTOList.isEmpty()){
					serviceResponse = new ServiceResponse();
					serviceResponse.setResponseTransferObjectObjectList(sysEntityColumnResponseTOList);
				}
			}
			
		}catch(Exception ex){
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}

}
