/**
 * 
 */
package com.talentpact.ui.sysPayCodeParameterMapping.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysPayCodeParameterMappingResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysPayCodeParameterMapping.controller.SysPayCodeParameterMappingController;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysPayCodeParameterMappingBean")
@ConversationScoped
public class SysPayCodeParameterMappingBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 1998978L;

    @Inject
    SysPayCodeParameterMappingController sysPayCodeParameterMappingController;

    private Integer sysPayCodeID;

    private String sysPayCodeName;

    private Integer sysParameterID;

    private String sysParameterName;

    private Integer sysPayCodeParameterMappingID;

    private String sysPayCodeParameterMappingName;

    private List<SysPayCodeParameterMappingResponseTO> sysPayCodeParameterMappingTOList;

    private List<SysPayCodeParameterMappingResponseTO> payCodeDropDownTOList;

    private List<SysPayCodeParameterMappingResponseTO> parameterDropDownTOList;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysPayCodeParameterMappingController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public Integer getSysPayCodeID()
    {
        return sysPayCodeID;
    }

    public void setSysPayCodeID(Integer sysPayCodeID)
    {
        this.sysPayCodeID = sysPayCodeID;
    }

    public String getSysPayCodeName()
    {
        return sysPayCodeName;
    }

    public void setSysPayCodeName(String sysPayCodeName)
    {
        this.sysPayCodeName = sysPayCodeName;
    }

    public Integer getSysParameterID()
    {
        return sysParameterID;
    }

    public void setSysParameterID(Integer sysParameterID)
    {
        this.sysParameterID = sysParameterID;
    }

    public String getSysParameterName()
    {
        return sysParameterName;
    }

    public void setSysParameterName(String sysParameterName)
    {
        this.sysParameterName = sysParameterName;
    }

    public Integer getSysPayCodeParameterMappingID()
    {
        return sysPayCodeParameterMappingID;
    }

    public void setSysPayCodeParameterMappingID(Integer sysPayCodeParameterMappingID)
    {
        this.sysPayCodeParameterMappingID = sysPayCodeParameterMappingID;
    }

    public String getSysPayCodeParameterMappingName()
    {
        return sysPayCodeParameterMappingName;
    }

    public void setSysPayCodeParameterMappingName(String sysPayCodeParameterMappingName)
    {
        this.sysPayCodeParameterMappingName = sysPayCodeParameterMappingName;
    }

    public List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMappingTOList()
    {
        return sysPayCodeParameterMappingTOList;
    }

    public void setSysPayCodeParameterMappingTOList(List<SysPayCodeParameterMappingResponseTO> sysPayCodeParameterMappingTOList)
    {
        this.sysPayCodeParameterMappingTOList = sysPayCodeParameterMappingTOList;
    }

    public List<SysPayCodeParameterMappingResponseTO> getPayCodeDropDownTOList()
    {
        return payCodeDropDownTOList;
    }

    public void setPayCodeDropDownTOList(List<SysPayCodeParameterMappingResponseTO> payCodeDropDownTOList)
    {
        this.payCodeDropDownTOList = payCodeDropDownTOList;
    }

    public List<SysPayCodeParameterMappingResponseTO> getParameterDropDownTOList()
    {
        return parameterDropDownTOList;
    }

    public void setParameterDropDownTOList(List<SysPayCodeParameterMappingResponseTO> parameterDropDownTOList)
    {
        this.parameterDropDownTOList = parameterDropDownTOList;
    }


}
