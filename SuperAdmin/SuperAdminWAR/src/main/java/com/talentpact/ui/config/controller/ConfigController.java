/**
 * 
 */
package com.talentpact.ui.config.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.constants.CoreConstants;
import com.alt.payrole.config.transport.output.ConfigResponseTO;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.config.bean.ConfigBean;
import com.talentpact.ui.menu.bean.MenuBean;


/**
 * @author Rahul.Chabba
 * 
 */
@SuppressWarnings("serial")
@Named("configController")
@ConversationScoped
public class ConfigController extends CommonController implements Serializable, IDefaultAction
{
    private static final Logger LOGGER_ = LoggerFactory.getLogger(ConfigController.class);

   
    @Inject
    ConfigBean configBean;
    
    @Inject
    MenuBean menuBean;


     
    @Override
    public void initialize()
    {
        try {
            // TODO Auto-generated method stub
        	List<ConfigResponseTO> massterList = new ArrayList<ConfigResponseTO>();
    		// TODO Auto-generated method stub
        	Properties prop = new Properties();
    		String propFileName = "alt_en_US.properties";
    		String result = "";
    		InputStream inputStream;
    		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
    		
    		if (inputStream != null) {
    			prop.load(inputStream);
    		} else {
    			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
    		}
    		// get the property value and print it out
    		Enumeration enuKeys = prop.keys();
    		while (enuKeys.hasMoreElements()) {
    			ConfigResponseTO obj = new ConfigResponseTO();
    			String key = (String) enuKeys.nextElement();
    			String value = prop.getProperty(key);
    			obj.setPropertyKey(key);
    			obj.setPropertyValue(value);
    			massterList.add(obj);
    		} 
    		configBean.setResponse(massterList);
    		menuBean.setPage("../config/config.xhtml");
        } catch (Exception e) {
            // TODO Auto-generated catch block
        	LOGGER_.error(e.toString());
        }
    }

    
    public void save() {
		// TODO Auto-generated method stub
    	try {
    	List<ConfigResponseTO> list = new ArrayList<ConfigResponseTO>();
    	list = configBean.getResponse();
    	System.out.println(list);
    	Properties prop = new Properties();
    	for(ConfigResponseTO dataToWrite : list){
    		prop.setProperty(dataToWrite.getPropertyKey(), dataToWrite.getPropertyValue());
    	}
    	File file=null;
    	if(System.getProperty("os.name").equalsIgnoreCase("linux")){
			file = new File("../standalone/deployments/TalentPactAdminPortal.war/WEB-INF/classes/alt_en_US.properties");
		}else{
    	 file = new File("..\\standalone\\deployments\\TalentPactAdminPortal.war\\WEB-INF\\classes\\alt_en_US.properties");
		}
		FileOutputStream fileOut;
		fileOut = new FileOutputStream(file);
		prop.store(fileOut, " #(for Windows)	L:\\\\Alt_Production\\\\dataupload\\\\ \n  #(for Linux)	/alt/Alt_Production/dataupload/ \n \n\n Changes made on "+(new Date()) +"\n");


		FacesContext.getCurrentInstance().addMessage( null,new FacesMessage(FacesMessage.SEVERITY_INFO, "Property File updated successfully.", ""));
		 ResourceBundle resourceBundle  = resourceBundle = ResourceBundle.getBundle(CoreConstants.ALT_RESOURCE_BUNDLE_NAME);
		 resourceBundle.clearCache();
		
    	} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
    		FacesContext.getCurrentInstance().addMessage( null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "Property File Not updated successfully.", ""));
    		LOGGER_.error(e.toString());
		}catch(Exception ex){
			FacesContext.getCurrentInstance().addMessage( null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "Property File Not Updated.Due to some Error. Please try Again.", ""));
			LOGGER_.error(ex.toString());
		}
    	finally{
    		RequestContext.getCurrentInstance().execute("scrollUp()");
    	}
    	
	}
    
    public void cancel() {
		try{
    	// TODO Auto-generated method stub
		initialize();
		RequestContext.getCurrentInstance().execute("PF('configDataTable').getPaginator().setPage(0);");
		 RequestContext.getCurrentInstance().execute("PF('configDataTable').clearFilters();");
		 RequestContext.getCurrentInstance().update("config");
   	
		}
		catch(Exception ex ){
			LOGGER_.error(ex.toString());
		}
	}

    public void download() throws Exception {
		// TODO Auto-generated method stub
    	try{
    			int j=0;
    			File file=null;
    			   			
    			if(System.getProperty("os.name").equalsIgnoreCase("linux")){
    				file = new File("../standalone/deployments/TalentPactAdminPortal.war/WEB-INF/classes/alt_en_US.properties");
    			}else{
    	    	 file = new File("..\\standalone\\deployments\\TalentPactAdminPortal.war\\WEB-INF\\classes\\alt_en_US.properties");
    			}
    			
				FileInputStream fileOut;
				fileOut = new FileInputStream(file);
				BufferedInputStream bf = new BufferedInputStream(fileOut);
				HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
				response.setContentType("application/txt");
				response.setHeader("Content-disposition", "attachment;filename=alt_en_US.property");
				ServletOutputStream out =  response.getOutputStream();  
				while( ( j = bf.read()) != -1){
				out.write(j);
				}
				response.getOutputStream().flush();
				response.getOutputStream().close();
				FacesContext.getCurrentInstance().responseComplete();
		}catch(Exception ex){
			LOGGER_.error(ex.toString());
		}
    	
	}
    
}//class ends

