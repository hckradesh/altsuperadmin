package com.talentpact.ui.sysmeshtaghierarchy.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagHierarchyTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysOfferingCategory.controller.SysOfferingCategoryController;
import com.talentpact.ui.sysmeshtaghierarchy.controller.SysMeshTagHierarchyController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@SuppressWarnings("serial")
@Named("sysMeshTagHierarchyBean")
@ConversationScoped
public class SysMeshTagHierarchyBean extends CommonBean implements Serializable, IDefaultAction {
	@Inject
	SysMeshTagHierarchyController sysMeshTagHierarchyController;

	private int sysTagID;

	private String tagName;

	private String dataType;

	private boolean mandatory;

	private boolean uniqueSourceCodeNode;

	private boolean dependentUponItself;

	private String tagHierarchy;

	private boolean inboundSupported;

	private boolean outboundSupported;

	private boolean renderErrorMessage;

	private Set<String> sysMeshTagHierarchy;

	private Set<String> selectedMeshTagHierarchy;

	private List<SysMeshTagHierarchyTO> sysMeshTagHierarchyTOList;
    
    private List<SysMeshModuleTO> sysMeshModuleList;
    
    private List<SysMeshModuleTO> hierarchyList;
    
    private List<SysMeshModuleTO> parentHierarchyList;
    
    private List<SysMeshModuleTO> sysEntityList;
    
    private List<SysMeshModuleTO> sysEntityColumnList;
    
    private String moduleName;
    
    private int moduleID;
    
    private int parentID;
    
    private int dependentTagID;
    
    private boolean topLevelTag;
    
    private String resourceType;
    
    private int entityID;
    
    private int entityColumnID;
    
	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			sysMeshTagHierarchyController.initialize();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	public SysMeshTagHierarchyController getSysMeshTagHierarchyController() {
		return sysMeshTagHierarchyController;
	}

	public void setSysMeshTagHierarchyController(SysMeshTagHierarchyController sysMeshTagHierarchyController) {
		this.sysMeshTagHierarchyController = sysMeshTagHierarchyController;
	}

	public int getSysTagID() {
		return sysTagID;
	}

	public void setSysTagID(int sysTagID) {
		this.sysTagID = sysTagID;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isUniqueSourceCodeNode() {
		return uniqueSourceCodeNode;
	}

	public void setUniqueSourceCodeNode(boolean uniqueSourceCodeNode) {
		this.uniqueSourceCodeNode = uniqueSourceCodeNode;
	}

	public boolean isDependentUponItself() {
		return dependentUponItself;
	}

	public void setDependentUponItself(boolean dependentUponItself) {
		this.dependentUponItself = dependentUponItself;
	}

	public String getTagHierarchy() {
		return tagHierarchy;
	}

	public void setTagHierarchy(String tagHierarchy) {
		this.tagHierarchy = tagHierarchy;
	}

	public boolean isInboundSupported() {
		return inboundSupported;
	}

	public void setInboundSupported(boolean inboundSupported) {
		this.inboundSupported = inboundSupported;
	}

	public boolean isOutboundSupported() {
		return outboundSupported;
	}

	public void setOutboundSupported(boolean outboundSupported) {
		this.outboundSupported = outboundSupported;
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	public Set<String> getSysMeshTagHierarchy() {
		return sysMeshTagHierarchy;
	}

	public void setSysMeshTagHierarchy(Set<String> sysMeshTagHierarchy) {
		this.sysMeshTagHierarchy = sysMeshTagHierarchy;
	}

	public Set<String> getSelectedMeshTagHierarchy() {
		return selectedMeshTagHierarchy;
	}

	public void setSelectedMeshTagHierarchy(Set<String> selectedMeshTagHierarchy) {
		this.selectedMeshTagHierarchy = selectedMeshTagHierarchy;
	}

	public List<SysMeshTagHierarchyTO> getSysMeshTagHierarchyTOList() {
		return sysMeshTagHierarchyTOList;
	}

	public void setSysMeshTagHierarchyTOList(List<SysMeshTagHierarchyTO> sysMeshTagHierarchyTOList) {
		this.sysMeshTagHierarchyTOList = sysMeshTagHierarchyTOList;
	}

	public List<SysMeshModuleTO> getSysMeshModuleList() {
		return sysMeshModuleList;
	}

	public void setSysMeshModuleList(List<SysMeshModuleTO> sysMeshModuleList) {
		this.sysMeshModuleList = sysMeshModuleList;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public int getModuleID() {
		return moduleID;
	}

	public void setModuleID(int moduleID) {
		this.moduleID = moduleID;
	}

	public int getParentID() {
		return parentID;
	}

	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	public List<SysMeshModuleTO> getHierarchyList() {
		return hierarchyList;
	}

	public void setHierarchyList(List<SysMeshModuleTO> hierarchyList) {
		this.hierarchyList = hierarchyList;
	}

	public List<SysMeshModuleTO> getParentHierarchyList() {
		return parentHierarchyList;
	}

	public void setParentHierarchyList(List<SysMeshModuleTO> parentHierarchyList) {
		this.parentHierarchyList = parentHierarchyList;
	}

	public int getDependentTagID() {
		return dependentTagID;
	}

	public void setDependentTagID(int dependentTagID) {
		this.dependentTagID = dependentTagID;
	}

	public boolean isTopLevelTag() {
		return topLevelTag;
	}

	public void setTopLevelTag(boolean topLevelTag) {
		this.topLevelTag = topLevelTag;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public int getEntityID() {
		return entityID;
	}

	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}

	public int getEntityColumnID() {
		return entityColumnID;
	}

	public void setEntityColumnID(int entityColumnID) {
		this.entityColumnID = entityColumnID;
	}

	public List<SysMeshModuleTO> getSysEntityList() {
		return sysEntityList;
	}

	public void setSysEntityList(List<SysMeshModuleTO> sysEntityList) {
		this.sysEntityList = sysEntityList;
	}

	public List<SysMeshModuleTO> getSysEntityColumnList() {
		return sysEntityColumnList;
	}

	public void setSysEntityColumnList(List<SysMeshModuleTO> sysEntityColumnList) {
		this.sysEntityColumnList = sysEntityColumnList;
	}
}