/**
 * 
 */
package com.talentpact.ui.uiform.helper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.talentpact.ui.menu.transport.MenuTO;


/**
 * @author pankaj.sharma1
 *
 */
public class HomeHelper
{
    private static final Logger LOGGER_ = Logger.getLogger(HomeHelper.class.toString());

    public static MenuModel getMenuModel(Map<Long, List<MenuTO>> menuMap)
        throws Exception
    {
        MenuModel menuModel = null;
        Object obj = null;

        try {
            menuModel = new DefaultMenuModel();
            //HOME
            /// List<MenuTO> menuTOList1 = menuMap.get("0");
            List<MenuTO> menuTOList = menuMap.get(0L);
            if (menuTOList != null && !menuTOList.isEmpty()) {
                for (MenuTO menuTO : menuTOList) {
                    obj = createMenuTree1(menuTO, menuMap);
                    if (obj instanceof DefaultSubMenu) {
                        menuModel.addElement((DefaultSubMenu) obj);
                    } else if (obj instanceof DefaultMenuItem) {
                        menuModel.addElement((DefaultMenuItem) obj);
                    }
                }
            }
            return menuModel;
        } catch (Exception e) {
            LOGGER_.warning(e.toString());
            throw e;
        }
    }


    private static Object createMenuTree1(MenuTO item, Map<Long, List<MenuTO>> menuMap)
        throws Exception
    {
        DefaultMenuItem defaultMenuItem = null;
        try {
            if (menuMap == null || menuMap.isEmpty()) {
                return null;
            }
            long parentId = item.getMenuID();
            List<MenuTO> menuTOlist = menuMap.get(parentId);
            if (!menuMap.containsKey(parentId)) {
                defaultMenuItem = createMenuItem(item);
                return defaultMenuItem;
            }
            if (menuTOlist == null || menuTOlist.isEmpty()) {
                defaultMenuItem = createMenuItem(item);
                return defaultMenuItem;
            } else {
                DefaultSubMenu subMenu = new DefaultSubMenu(item.getMenuName());
                List<MenuTO> menuList = menuMap.get(parentId);
                for (MenuTO menu : menuList) {
                    Object tmp = createMenuTree1(menu, menuMap);
                    if (tmp instanceof DefaultSubMenu) {
                        subMenu.addElement((DefaultSubMenu) createMenuTree1(menu, menuMap));
                    } else {
                        subMenu.addElement((DefaultMenuItem) createMenuTree1(menu, menuMap));
                    }
                }
                return subMenu;
            }
        } catch (Exception e) {
            LOGGER_.warning(e.toString());
            throw e;
        }
    }

    /**
     * for creating menuitem tag.
     * @param menuLabel
     * @return
     */
    private static DefaultMenuItem createMenuItem(MenuTO menuTO)
    {
        DefaultMenuItem defaultMenuItem = null;
        try {
            defaultMenuItem = new DefaultMenuItem(menuTO.getMenuName());
            defaultMenuItem.setCommand("#{homeAction.menuClicked(" + "\"" + menuTO.getDefaultClass() + "\"" + "," + "\"" + menuTO.getUrl() + "\"" + "," + menuTO.getModuleID()
                    + ")}");
            defaultMenuItem.setUpdate(":centerForm");
            defaultMenuItem.setAjax(true);
            defaultMenuItem.setOnstart("PF('statusDialog').show()");
            defaultMenuItem.setOnsuccess("PF('statusDialog').hide()");
        } catch (Exception e) {
            // TODO: handle exception
        }
        return defaultMenuItem;
    }

    /**
     * method to find MenuTO by menuID from user specific menu map.
     * @param menuID
     * @param userMenuMap
     * @return
     * @throws Exception
     */
    public static MenuTO findMenuTO(long menuID, Map<String, List<MenuTO>> userMenuMap)
        throws Exception
    {
        MenuTO selectedMenuTO = null;
        Collection<List<MenuTO>> menuTOCollection = null;
        try {
            menuTOCollection = userMenuMap.values();
            if (menuTOCollection == null || menuTOCollection.isEmpty()) {
                return null;
            }
            for (List<MenuTO> menuTOList : menuTOCollection) {
                for (MenuTO menuTO : menuTOList) {
                    if (menuTO.getMenuID() == menuID) {
                        selectedMenuTO = menuTO;
                        break;
                    }
                }
                if (selectedMenuTO != null) {
                    break;
                }
            }
            return selectedMenuTO;
        } catch (Exception e) {
            throw e;
        }
    }

}
