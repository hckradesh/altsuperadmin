/**
 * 
 */
package com.talentpact.ui.helper;

import java.util.Comparator;

import com.talentpact.business.application.transport.output.SysConstantTypeTO;

/**
 * @author pankaj.sharma1
 *
 */
public class SysConstantCategoryCompartor implements Comparator<SysConstantTypeTO>
{
    public int compare(SysConstantTypeTO e1, SysConstantTypeTO e2)
    {
        return e1.getCategoryName().compareTo(e2.getCategoryName());
    }
}
