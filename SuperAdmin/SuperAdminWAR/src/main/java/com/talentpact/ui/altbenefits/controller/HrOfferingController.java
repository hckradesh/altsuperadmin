/**
 * 
 */
package com.talentpact.ui.altbenefits.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.HrVendorOfferingTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.OrganizationTO;
import com.alt.altbenefits.to.SysSectorTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.altbenefits.transport.input.BenefitRequestTO;
import com.alt.altbenefits.transport.output.BenefitResponseTO;
import com.alt.common.constants.CoreConstants;
import com.talentpact.business.common.constants.IBenefitsConstants;
import com.talentpact.business.service.altbenefits.HrVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysSectorService;
import com.talentpact.business.service.altbenefits.SysSectorVendorOfferingSequenceService;
import com.talentpact.business.service.altbenefits.SysVendorOfferingTemplateService;
import com.talentpact.business.tpOrganization.service.impl.OrganizationService;
import com.talentpact.ui.altbenefits.bean.HrVendorOfferingBean;
import com.talentpact.ui.altbenefits.constant.IBenefitsConstant;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;

/**
 * @author javed.ali
 *
 */
@Named("hrOfferingController")
@ConversationScoped
public class HrOfferingController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 14399071113668L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(HrOfferingController.class);

    @Inject
    HrVendorOfferingBean hrVendorOfferingBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    MenuBean menuBean;

    @Inject
    SysSectorVendorOfferingSequenceService sectorVendorOfferingSequenceService;

    @Inject
    SysVendorOfferingTemplateService sysVendorOfferingTemplateService;

    @EJB
    OrganizationService organizationService;

    @EJB
    HrVendorOfferingSequenceService hrVendorOfferingSequenceService;

    @EJB
    SysSectorService sectorService;

    @Override
    public void initialize()
    {
        BenefitResponseTO benefitResponseTO = null;
        Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap = null;
        try {
            hrVendorOfferingBean.setSectorID(-1);
            hrVendorOfferingBean.setSelectedOrganization(null);
            RequestContext requestContext = RequestContext.getCurrentInstance();
            benefitResponseTO = hrVendorOfferingSequenceService.getBenefitsHrBasedList();
            hrVendorOfferingmap = benefitResponseTO.getHrVendorOfferingmap();
            hrVendorOfferingBean.setHrVendorOfferingTOList(creatingGroupBasedListFromGroupVendoringMap(hrVendorOfferingmap));
            hrVendorOfferingBean.setHrVendorOfferingmap(hrVendorOfferingmap);
            menuBean.setPage(IBenefitsConstant.HR_BASED_OFFERING_SEQUENCE_HOME);
            requestContext.execute(IBenefitsConstants.HR_VENDOR_DATATABLE_RESET);
            requestContext.execute(IBenefitsConstants.HR_VENDOR_PAGINATION_DATATABLE_RESET);
            RequestContext.getCurrentInstance().execute("scrollUp()");

        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }

    }

    /**
     * 
     * @param sectorVendorOfferingmap
     * @return
     */
    private List<HrVendorOfferingTO> creatingGroupBasedListFromGroupVendoringMap(Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap)
    {
        List<HrVendorOfferingTO> hrBenefitSequenceTOList = null;
        HrVendorOfferingTO hrVendorOfferingTO = null;
        List<VendorOfferingTemplateTO> offeringTemplateTOList = null;
        String vendorOfferingCompositeName = null;
        String vendorCompositeName = null;
        try {
            hrBenefitSequenceTOList = new ArrayList<HrVendorOfferingTO>();
            if (hrVendorOfferingmap != null && !hrVendorOfferingmap.isEmpty()) {
                for (Map.Entry<String, List<VendorOfferingTemplateTO>> entry : hrVendorOfferingmap.entrySet()) {
                    hrVendorOfferingTO = new HrVendorOfferingTO();
                    hrVendorOfferingTO.setOrgID(Long.parseLong(entry.getKey().split(CoreConstants.AT_SPERATOR)[0]));
                    hrVendorOfferingTO.setOrgName(entry.getKey().split(CoreConstants.AT_SPERATOR)[2]);
                    hrVendorOfferingTO.setSectorName(entry.getKey().split(CoreConstants.AT_SPERATOR)[1]);
                    offeringTemplateTOList = entry.getValue();
                    if (offeringTemplateTOList != null && !offeringTemplateTOList.isEmpty()) {
                        vendorCompositeName = null;
                        vendorOfferingCompositeName = null;
                        for (VendorOfferingTemplateTO templateTO : offeringTemplateTOList) {
                            if (vendorCompositeName == null && vendorOfferingCompositeName == null) {
                                vendorCompositeName = templateTO.getVendorName();
                                vendorOfferingCompositeName = templateTO.getOfferTemplateName();
                            } else {
                                vendorCompositeName = vendorCompositeName + CoreConstants.COMA_SPERATOR + templateTO.getVendorName();
                                vendorOfferingCompositeName = vendorOfferingCompositeName + CoreConstants.COMA_SPERATOR + templateTO.getOfferTemplateName();
                            }
                        }
                        hrVendorOfferingTO.setVendorOfferingCompositeName(vendorOfferingCompositeName);
                        hrVendorOfferingTO.setVendorCompositeName(vendorCompositeName);
                    }
                    hrBenefitSequenceTOList.add(hrVendorOfferingTO);
                }
            }

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
        return hrBenefitSequenceTOList;

    }

    /**
     * redirecting to More Info Vendor
     * 
     * */
    public void redirectToVendorMoreInfo(BenefitVendorTO sysBenefitVendorTO)
    {
        try {
            hrVendorOfferingBean.setSysBenefitVendorTO(sysBenefitVendorTO);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * redirecting to More Info VendorOffering
     * 
     * */
    public void redirectToVendoringOfferMoreInfo(VendorOfferingTemplateTO offeringTemplateTO)
    {
        try {
            hrVendorOfferingBean.setVendorOfferingTemplateTO(offeringTemplateTO);
            if (offeringTemplateTO.getPeakRecurringDetailTO() != null) {
                hrVendorOfferingBean.setRenderVendorOffering(true);
            } else {
                hrVendorOfferingBean.setRenderVendorOffering(false);
            }
            hrVendorOfferingBean.setPeakRecurringDetailTO(offeringTemplateTO.getPeakRecurringDetailTO());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * redirecting to Edit OfferingSeqencePage
     * 
     * */

    public void editHrVendoringSequence(HrVendorOfferingTO hrVendorOfferingTO)
        throws Exception
    {
        Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap = null;
        List<VendorOfferingTemplateTO> hrOfferingTemplateTOList = null;
        List<VendorOfferingTemplateTO> existingOfferingTemplateTOList = null;
        List<VendorOfferingTemplateTO> newOfferingTemplateTOList = null;

        try {
            menuBean.setPage(IBenefitsConstant.EDIT_OFFERING__HR_SEQUENCE_PAGE);
            hrVendorOfferingmap = hrVendorOfferingBean.getHrVendorOfferingmap();
            hrOfferingTemplateTOList = hrVendorOfferingSequenceService.getOfferingTemplateList(hrVendorOfferingTO.getOrgID());
            existingOfferingTemplateTOList = hrVendorOfferingmap.get(hrVendorOfferingTO.getOrgID() + CoreConstants.AT_SPERATOR + hrVendorOfferingTO.getSectorName()
                    + CoreConstants.AT_SPERATOR + hrVendorOfferingTO.getOrgName());
            newOfferingTemplateTOList = processVendorOfferingTOList(cloneVendorOfferingList(existingOfferingTemplateTOList), hrOfferingTemplateTOList);
            hrVendorOfferingBean.setSelectedVendorOfferingTemplateTOList(newOfferingTemplateTOList);
            hrVendorOfferingBean.setSelectedOrgName(hrVendorOfferingTO.getOrgName());
            hrVendorOfferingBean.setSelectedOrgID(hrVendorOfferingTO.getOrgID());
            hrVendorOfferingBean.setSelectedSectorName(hrVendorOfferingTO.getSectorName());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * 
     * @param existingOfferingTemplateTOList
     * @param systemOfferingTemplateTOList
     * @return
     * @throws Exception
     */
    private List<VendorOfferingTemplateTO> processVendorOfferingTOList(List<VendorOfferingTemplateTO> existingOfferingTemplateTOList,
            List<VendorOfferingTemplateTO> hrOfferingTemplateTOList)
        throws Exception
    {
        List<VendorOfferingTemplateTO> newOfferingTemplateTOList = null;
        boolean newOffer = false;
        try {
            newOfferingTemplateTOList = existingOfferingTemplateTOList;
            for (VendorOfferingTemplateTO hrOffering : hrOfferingTemplateTOList) {
                newOffer = true;
                for (VendorOfferingTemplateTO sectorOffering : existingOfferingTemplateTOList) {
                    if (sectorOffering.getHrVendorOfferingID().equals(hrOffering.getHrVendorOfferingID())) {
                        newOffer = false;
                        break;
                    }

                }
                if (newOffer) {
                    newOfferingTemplateTOList.add(hrOffering);
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return newOfferingTemplateTOList;
    }


    /**
     * redirecting to More Info OfferingSeqencePage
     * 
     * */
    public void redirectToHrVendoringSequenceMoreInfo(HrVendorOfferingTO hrVendorOfferingTO)
    {
        Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap = null;
        try {
            menuBean.setPage(IBenefitsConstant.MORE_INFO_HR_OFFERING_SEQUENCE_PAGE);
            hrVendorOfferingmap = hrVendorOfferingBean.getHrVendorOfferingmap();
            hrVendorOfferingBean.setSelectedVendorOfferingTemplateTOList(hrVendorOfferingmap.get(hrVendorOfferingTO.getOrgID() + CoreConstants.AT_SPERATOR
                    + hrVendorOfferingTO.getSectorName() + CoreConstants.AT_SPERATOR + hrVendorOfferingTO.getOrgName()));
            hrVendorOfferingBean.setSelectedOrgName(hrVendorOfferingTO.getOrgName());
            hrVendorOfferingBean.setSelectedOrgID(hrVendorOfferingTO.getOrgID());
            hrVendorOfferingBean.setSelectedHrBasedBenefitTO(hrVendorOfferingTO);
            hrVendorOfferingBean.setSelectedSectorName(hrVendorOfferingTO.getSectorName());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
    * redirecting to OfferingSeqenceHomePage
    * 
    * */
    public void redirectToHome()
    {
        menuBean.setPage(IBenefitsConstant.HR_BASED_OFFERING_SEQUENCE_HOME);
        hrVendorOfferingBean.setSelectedOrganization(null);

    }

    public void renderVendorOfferingPanel()
    {
        fetchVendorOfferingListByOrg();
    }

    /**
     * redirecting to AddOfferingSeqencePage
     * 
     * */
    public void redirectToAddOfferingSeqencePage()
    {
        List<SysSectorTO> sysSectorTOList = null;
        try {
            menuBean.setPage(IBenefitsConstant.HR_OFFERING_SEQUENCE_PAGE);
            hrVendorOfferingBean.setRenderVendorOffering(false);
            sysSectorTOList = hrVendorOfferingBean.getSectorTOList();
            if (sysSectorTOList != null && !sysSectorTOList.isEmpty()) {
            } else {
                sysSectorTOList = sectorService.getSysSectorList();
                hrVendorOfferingBean.setSectorTOList(sysSectorTOList);
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * 
     * 
     * */
    public void fetchVendorOfferingListByOrg()
    {
        List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList = null;
        Long selectedOrganizations = null;
        try {
            selectedOrganizations = hrVendorOfferingBean.getSelectedOrganization();
            vendorOfferingTemplateTOList = hrVendorOfferingSequenceService.getOfferingTemplateList(selectedOrganizations);
            if (vendorOfferingTemplateTOList != null && !vendorOfferingTemplateTOList.isEmpty()) {
                hrVendorOfferingBean.setVendorOfferingTemplateTOList(vendorOfferingTemplateTOList);
                hrVendorOfferingBean.setRenderVendorOffering(true);
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.NO_ORG_VENDOR_OFFER_GIVEN, ""));
                RequestContext.getCurrentInstance().execute("scrollUp()");
                hrVendorOfferingBean.setRenderVendorOffering(false);

            }
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * 
     * 
     * */
    public void fetchOrgList()
    {
        List<OrganizationTO> orgTOList = null;
        Map<String, List<VendorOfferingTemplateTO>> hrVendorOfferingmap = null;
        Integer sectorId = null;
        try {
            sectorId = hrVendorOfferingBean.getSectorID();
            hrVendorOfferingmap = hrVendorOfferingBean.getHrVendorOfferingmap();
            orgTOList = organizationService.getTpOrganizationList(sectorId);
            hrVendorOfferingBean.setOrgTOList(orgTOList);
            if (hrVendorOfferingmap != null && !hrVendorOfferingmap.isEmpty()) {
                for (Iterator<OrganizationTO> organozation = orgTOList.iterator(); organozation.hasNext();) {
                    OrganizationTO organizationTO = organozation.next();
                    for (Map.Entry<String, List<VendorOfferingTemplateTO>> entry : hrVendorOfferingmap.entrySet()) {
                        if (organizationTO.getOrgID().equals(Long.parseLong(entry.getKey().split(CoreConstants.AT_SPERATOR)[0]))) {
                            organozation.remove();
                            break;
                        }
                    }

                }
            }
            hrVendorOfferingBean.setOrgTOList(orgTOList);
            hrVendorOfferingBean.setSelectedOrganization(null);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }


    /**
     * 
     */
    public void saveHrVendorOfferSequence(boolean edit)
    {
        Long selectedOrganization = null;
        BenefitRequestTO benefitRequestTO = null;
        String sequenceValue = null;
        String[] hrVendorOfferSequenceValue = null;
        List<String> sequenceValueList = null;
        Map<Integer, Long> hrVendorSequenceMap = null;
        try {
            selectedOrganization = hrVendorOfferingBean.getSelectedOrganization();
            if (!edit) {
                if ((selectedOrganization == null)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.NO_ORG_SELECTED, ""));
                    RequestContext.getCurrentInstance().execute("scrollUp()");
                    return;
                }
            }
            benefitRequestTO = new BenefitRequestTO();
            sequenceValueList = new ArrayList<String>();
            sequenceValue = hrVendorOfferingBean.getSequenceValue();
            hrVendorOfferSequenceValue = sequenceValue.split(",");
            for (String sequence : hrVendorOfferSequenceValue) {
                sequenceValueList.add(sequence);
            }
            hrVendorSequenceMap = fetchSequenceMapFromValue(sequenceValueList);
            benefitRequestTO.setSelectedOrganization(selectedOrganization);
            benefitRequestTO.setHrVendorOfferSequenceMap(hrVendorSequenceMap);
            benefitRequestTO.setEdit(edit);
            if (!edit) {
                benefitRequestTO.setVendorOfferingTemplateTOList(hrVendorOfferingBean.getVendorOfferingTemplateTOList());
            } else {
                benefitRequestTO.setOrgID(hrVendorOfferingBean.getSelectedOrgID());
                benefitRequestTO.setVendorOfferingTemplateTOList(hrVendorOfferingBean.getSelectedVendorOfferingTemplateTOList());
            }
            hrVendorOfferingSequenceService.saveHrVendorOfferSequence(benefitRequestTO);
            if (!edit) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstant.ORG_BASED_OFFERING_SEQUENCE_SAVE_SUCCESS, ""));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, IBenefitsConstant.ORG_BASED_OFFERING_SEQUENCE_UPDATE_SUCCESS, ""));
            }
            initialize();
        } catch (Exception ex) {
            LOGGER_.error("", ex);
            RequestContext.getCurrentInstance().execute("scrollUp()");
            if (!edit) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.ORG_BASED_OFFERING_SEQUENCE_SAVE_ERROR, ""));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.ORG_BASED_OFFERING_SEQUENCE_UPDATE_ERROR, ""));
            }

        }
    }

    /**
     * 
     * @param sequenceValueList
     * @return
     */
    private Map<Integer, Long> fetchSequenceMapFromValue(List<String> sequenceValueList)
    {
        String[] hrOfferSequenceValue = null;
        Map<Integer, Long> sequenceMap = null;
        String id = null;
        String value = null;
        try {
            sequenceMap = new LinkedHashMap<Integer, Long>();
            for (String s : sequenceValueList) {
                hrOfferSequenceValue = s.split("_");
                if (hrOfferSequenceValue != null) {
                    id = hrOfferSequenceValue[0];
                    value = hrOfferSequenceValue[1];
                    sequenceMap.put(Integer.parseInt(id), Long.parseLong(value));

                }
            }

            return sequenceMap;
        } catch (Exception ex) {
            throw ex;
        }

    }

    /**
     * 
     * @param offeringCodeTO
     */
    public void redirectToOfferCodeMoreInfo(OfferingCodeTO offeringCodeTO)
    {
        if (offeringCodeTO.isMaximumLimitApplicable()) {
            hrVendorOfferingBean.setOfferingCodeMaximumLimitTO(offeringCodeTO.getCodeMaximumLimitTO());
            hrVendorOfferingBean.setRenderMaximumLimitApplicableDetail(true);
        } else {
            hrVendorOfferingBean.setRenderMaximumLimitApplicableDetail(false);
        }
        hrVendorOfferingBean.setOfferingCodeTO(offeringCodeTO);
    }

    /**
     * 
     * @param offeringTemplateTOList
     * @return
     */
    private List<VendorOfferingTemplateTO> cloneVendorOfferingList(List<VendorOfferingTemplateTO> offeringTemplateTOList) throws CloneNotSupportedException
    {
        List<VendorOfferingTemplateTO> cloneOfferingList = new ArrayList<VendorOfferingTemplateTO>();
        try {
            if (cloneOfferingList != null) {
                for (VendorOfferingTemplateTO offer : offeringTemplateTOList) {
                    cloneOfferingList.add((VendorOfferingTemplateTO) offer.clone());

                }
            }
        } catch (CloneNotSupportedException cne) {
            LOGGER_.error("", cne);
            throw cne;
        }
        return cloneOfferingList;
    }


}
