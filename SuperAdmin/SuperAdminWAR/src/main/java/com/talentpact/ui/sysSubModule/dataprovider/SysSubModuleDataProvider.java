/**
 * 
 */
package com.talentpact.ui.sysSubModule.dataprovider;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysSubModule.common.transport.input.SysSubModuleRequestTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.sysSubModule.dataprovider.DS.SysSubModuleDS;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value =  TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysSubModuleDataProvider {

	@EJB
	private SysSubModuleDS sysSubModuleDS;

	/**
	 * @param serviceRequest
	 * @return
	 */
	public ServiceResponse getParentModuleAndSubModules(
			ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		SysSubModuleRequestTO sysSubModuleRequestTO = null;
		SysSubModuleResponseTO sysSubModuleResponseTO = null;
		try {
			sysSubModuleRequestTO = (SysSubModuleRequestTO) serviceRequest
					.getRequestTransferObjectRevised();
			sysSubModuleResponseTO = sysSubModuleDS
					.getParentModuleAndSubModules(sysSubModuleRequestTO);
			if(sysSubModuleResponseTO!=null){
				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObject(sysSubModuleResponseTO);
			}
		} catch (Exception ex) {
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}

}
