/**
 * 
 */
package com.talentpact.ui.tenants.newtenant.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.tenants.transport.newtenant.input.NewTenantRequestTO;
import com.talentpact.business.tenants.transport.newtenant.output.NewTenantResponseTO;
import com.talentpact.remote.newtenant.ICreateNewTenantRemote;
import com.talentpact.remote.tpOrganization.INewOrganizationFacadeRemote;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.common.controller.LookupAction;
import com.talentpact.ui.login.bean.LoginBean;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.tenants.newtenant.bean.NewTenantBean;

/**
 * @author radhamadhab.dalai
 *
 */

@ConversationScoped
@Named("newTenantController")
public class NewTenantController extends CommonController implements Serializable, IDefaultAction
{
    @Inject
    NewTenantBean newTenantBean;

    @Inject
    MenuBean menuBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    LookupAction lookupAction;

    @Inject
    LoginBean loginBean;


    @EJB(beanName = "CreateNewTenantFacade")
    ICreateNewTenantRemote iCreateNewTenantRemote;


    @EJB(beanName = "NewOrganizationFacade")
    INewOrganizationFacadeRemote iNewOrganizationFacadeRemote;


    /*Populate Tenant List*/
    public void initialize()
    {
        List<NewTenantResponseTO> tenantResToList = null;
        ServiceRequest serviceRequest = null;
        ServiceResponse serviceResponse = null;
        NewTenantResponseTO newTenantResponseTO = null;
        try {

            serviceResponse = iNewOrganizationFacadeRemote.getAllTenants(serviceRequest);
            newTenantResponseTO = (NewTenantResponseTO) serviceResponse.getResponseTransportObject();
            tenantResToList = newTenantResponseTO.getTenantListTo();
            newTenantBean.setTenantResToList(tenantResToList);
            newTenantBean.setFilteredTenantList(null);
            menuBean.setPage("../tenants.xhtml");


        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void save()
    {
        ServiceRequest serviceRequest = null;
        ServiceResponse serviceResponse = null;
        NewTenantRequestTO newTenantRequestTO = null;
        String tenantName = null;
        String tenantCode = null;
        Long tenantId = null;
        int statusCode = 0;
        Date fromDate = null;
        Date toDate = null;

        try {

            serviceRequest = new ServiceRequest();
            newTenantRequestTO = new NewTenantRequestTO();

            tenantName = newTenantBean.getTenantName();
            tenantCode = newTenantBean.getTenantCode();
            statusCode = newTenantBean.getStatusID();
            tenantId = newTenantBean.getTenantID();
            fromDate = newTenantBean.getEffectiveFrom();
            toDate = newTenantBean.getEffectiveTo();

            if (toDate != null) {
                if (toDate.before(fromDate)) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Effective To date cannot be earlier than Effective From date. ", ""));
                    return;
                }
            }


            if (tenantCode != null) {
                for (NewTenantResponseTO data : newTenantBean.getTenantResToList()) {
                    if (data.getTenantCode().equalsIgnoreCase(tenantCode) && data.getTenantID() != null
                            && (tenantId == null || data.getTenantID().longValue() != tenantId.longValue())) {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tenant Code  " + tenantCode.toUpperCase() + " already Exists.", ""));
                        return;
                    }

                }


            }


            newTenantRequestTO.setTenantID(tenantId);
            newTenantRequestTO.setTenantName(tenantName);
            newTenantRequestTO.setTenantCode(tenantCode);
            newTenantRequestTO.setStatusID(statusCode);
            if (newTenantBean.getEffectiveFrom() == null) {
                newTenantBean.setEffectiveFrom(new Date());
            }
            /* if (newTenantBean.getEffectiveTo() != null) {
                 newTenantBean.setEffectiveTo(newTenantBean.getEffectiveTo());
             }*/
            if (newTenantBean.getContractSignedDate() == null) {
                newTenantBean.setContractSignedDate(new Date());
            }
            if (newTenantBean.getEffectiveTo() != null) {
                if (newTenantBean.getEffectiveTo().before(newTenantBean.getEffectiveFrom())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EffectiveTO date can not be before EffectiveFrom date.", ""));
                    return;
                }

                if (newTenantBean.getEffectiveTo().before(newTenantBean.getContractSignedDate())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "EffectiveTO date can not be before ContractSign date.", ""));
                    return;
                }
            }

            newTenantRequestTO.setEffectiveFrom(newTenantBean.getEffectiveFrom());
            if (newTenantBean.getEffectiveTo() != null) {
                newTenantRequestTO.setEffectiveTo(newTenantBean.getEffectiveTo());
            }
            newTenantRequestTO.setCreatedBy(userSessionBean.getUserID());
            newTenantRequestTO.setModifiedBy(userSessionBean.getUserID());
            newTenantRequestTO.setModifiedDate(new Date());
            newTenantRequestTO.setCreatedDate(new Date());
            newTenantRequestTO.setContractSignedDate(newTenantBean.getContractSignedDate());

            serviceRequest.setRequestTansportObject(newTenantRequestTO);

            serviceResponse = iCreateNewTenantRemote.saveOrUpdateTenant(serviceRequest);
            if (newTenantBean.getSaveFlag() == true)
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Tenant Saved Succesfully ", ""));
            if (newTenantBean.getUpdateFlag() == true)
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Tenant Updated Succesfully ", ""));

            initialize();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            serviceRequest = null;
            serviceResponse = null;
            newTenantRequestTO = null;
            tenantName = null;
            tenantCode = null;

        }
    }


    public void editTenant(NewTenantResponseTO tenetResponseTO)
    {

        try {
            if (tenetResponseTO == null) {
                /*Error message*/
            }
            // tenetResponseTO.getTenantID();
            newTenantBean.setTenantID(tenetResponseTO.getTenantID());
            newTenantBean.setTenantName(tenetResponseTO.getTenantName());
            newTenantBean.setStatusID(tenetResponseTO.getStatusID());
            newTenantBean.setContractSignedDate(tenetResponseTO.getContractSignedDate());
            newTenantBean.setEffectiveFrom(tenetResponseTO.getEffectiveFrom());
            newTenantBean.setTenantCode(tenetResponseTO.getTenantCode());
            newTenantBean.setEffectiveTo(tenetResponseTO.getEffectiveTo());
            newTenantBean.setSaveFlag(false);
            newTenantBean.setUpdateFlag(true);
            menuBean.setPage("../newtenant.xhtml");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cancel()
    {

        try {
            initialize();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void createNew()
    {
        try {
            newTenantBean.clearData();
            newTenantBean.setSaveFlag(true);
            newTenantBean.setUpdateFlag(false);
            menuBean.setPage("../newtenant.xhtml");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
