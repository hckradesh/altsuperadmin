package com.talentpact.ui.sysEntity.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.business.application.transport.input.SysEntityRequestTO;
import com.talentpact.business.application.transport.output.SysEntityColumnTO;
import com.talentpact.business.application.transport.output.SysEntityResponseTO;
import com.talentpact.business.application.transport.output.SysEntityTO;
import com.talentpact.business.application.transport.output.SysSubModuleTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.model.SysEntity;
import com.talentpact.remote.sysEntity.ISysEntityRemote;
import com.talentpact.remote.sysEntityColumn.ISysEntityColumnRemote;
import com.talentpact.remote.sysModule.ISysModuleRemote;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.common.controller.LookupAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.menu.controller.MenuAction;
import com.talentpact.ui.sysEntity.bean.SysEntityBean;
import com.talentpact.ui.sysEntity.constants.ISysEntityConstants;
import com.talentpact.ui.sysEntityColumn.common.transport.input.SysEntityColumnRequestTO;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.sysSubModule.facaderemote.SysSubModuleLocalFacade;

@Named("sysEntityController")
@ConversationScoped
public class SysEntityController extends CommonController implements Serializable, IDefaultAction
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(SysEntityController.class.toString());

    @Inject
    SysEntityBean sysEntityBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    LookupAction lookupAction;

    @Inject
    MenuAction menuAction;

    @Inject
    MenuBean menuBean;

    @EJB(beanName = "SysEntityFacade")
    ISysEntityRemote iSysEntityRemote;

    @EJB(beanName = "SysEntityColumnFacade")
    ISysEntityColumnRemote iSysEntityColumnRemote;

    @EJB(beanName = "SysModuleFacade")
    ISysModuleRemote iSysModuleRemote;
    
    @EJB(beanName = "SysSubModuleFacade")
    SysSubModuleLocalFacade sysSubModuleLocalFacade;

    @Override
    public void initialize()
    {
        List<SysEntityTO> sysEntityTOList = null;
        try {
            SysEntityTO sysEntityTO = null;
            sysEntityTO = new SysEntityTO();
            sysEntityBean.setSysEntityTO(sysEntityTO);
            sysEntityTOList = getAllSysEntityList();
            sysEntityBean.setSysEntityTOList(sysEntityTOList);
            sysEntityBean.setRenderUpdateButton(false);
            sysEntityBean.setRenderSaveButton(false);
            sysEntityBean.setRenderSysColumnDataTable(false);
            sysEntityBean.setFilterSysEntityTOList(sysEntityTOList);
            sysEntityBean.setSelectTableEnable(true);
            RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
            RequestContext.getCurrentInstance().update("sysEntityListForm");
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    /**
     * adding SysEntity and SysEntityColumn
     * @param sysEntityTO
     * @author raghvendra.mishra
     */
    public void addSysEntity(SysEntityTO sysEntityTO)
    {
        ServiceRequest serviceRequest = null;
        SysEntityResponseTO sysEntityResponseTO = null;
        ServiceResponse serviceResponse = null;
        List<SysEntityColumnTO> sysEntityColumnListUI = null;
        List<SysEntityColumnTO> sysEntityColumnList = null;
        List<SysEntityResponseTO> sysEntityResponseTOList = null;
        List<SysEntityTO> sysEntityTOList = null;
        long sysEntityColumnID = 0;
        int entityID = 0;
        SysEntity sysEntity = null;
        try {
            sysEntityResponseTOList = new ArrayList<SysEntityResponseTO>();
            sysEntityTO.setCreatedBy((userSessionBean.getUserID()).intValue());
            sysEntityTO.setModifiedBy((userSessionBean.getUserID()).intValue());
            serviceRequest = getServiceRequest(sysEntityTO);
            serviceResponse = iSysEntityRemote.saveOrUpdateSysEntity(serviceRequest);
            sysEntityResponseTO = (SysEntityResponseTO) serviceResponse.getResponseTransportObject();
            entityID = sysEntityResponseTO.getEntityId();
            sysEntityResponseTOList.add(sysEntityResponseTO);
            sysEntityTOList = convertResponseTOInENtityTO(sysEntityResponseTOList);
            sysEntityTO = sysEntityTOList.get(0);
            //Adding SysEntityColumn
            sysEntityColumnListUI = sysEntityBean.getSysEntityColumnList();
            for (SysEntityColumnTO sysEntityColumnTO : sysEntityColumnListUI) {
                if (sysEntityColumnTO.isCheckBoxOption()) {
                    /*sysEntity = new SysEntity();
                    sysEntity.setEntityId(entityID);*/
                    sysEntityColumnTO.setSysEntityId(entityID);
                    sysEntityColumnTO.setTenantID(1);
                    sysEntityColumnID = addUpdateSysEntityColumn(sysEntityColumnTO);
                    if (sysEntityColumnTO.isPrimaryKey()) {
                        sysEntityResponseTOList = new ArrayList<SysEntityResponseTO>();
                        sysEntityTO.setPrimaryKeyColumnID(((Long) sysEntityColumnID).intValue());
                        sysEntityTO.setPrimaryKey(sysEntityColumnTO.getDbColumnName());
                        sysEntityTO.setLableColumn(sysEntityColumnTO.getLabel());
                        serviceRequest = getServiceRequest(sysEntityTO);
                        serviceResponse = iSysEntityRemote.saveOrUpdateSysEntity(serviceRequest);
                        sysEntityResponseTO = (SysEntityResponseTO) serviceResponse.getResponseTransportObject();
                        sysEntityResponseTOList.add(sysEntityResponseTO);
                        sysEntityTOList = convertResponseTOInENtityTO(sysEntityResponseTOList);
                        sysEntityTO = sysEntityTOList.get(0);
                    }
                }
            }
            sysEntityColumnList = getSysEntityColumnForEditOption(entityID);
            sysEntityBean.setSysEntityTOList(getAllSysEntityList());
            sysEntityBean.setSysEntityColumnList(sysEntityColumnList);
            sysEntityBean.setSysSubModuleList(getAllSysSubModuleList());
            sysEntityBean.setSysEntityTO(sysEntityTO);
            sysEntityBean.setSelectTableEnable(false);
            sysEntityBean.setRenderSaveButton(false);
            sysEntityBean.setRenderUpdateButton(true);
            sysEntityBean.setRenderSysColumnDataTable(true);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysEntityConstants.SYSENTITY_SYSCOLUMN_ADD_SUCCESS_MSG, ""));
            RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysEntityConstants.SYSENTITY_SYSCOLUMN_SAVE_ERROR_MSG, ""));
            LOGGER.error("", e);
            sysEntityBean.setRenderSaveButton(true);
            sysEntityBean.setRenderUpdateButton(false);
            sysEntityBean.setRenderSysColumnDataTable(true);
            RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
        }
    }

    /**
    * updating SysEntity and SysEntityColumn
    * @param sysEntityTO
    * @author raghvendra.mishra
    */
    public void updateSysEntity(SysEntityTO sysEntityTO)
    {
        ServiceRequest serviceRequest = null;
        ServiceResponse serviceResponse = null;
        SysEntityResponseTO sysEntityResponseTO = null;
        List<SysEntityColumnTO> sysEntityColumnListUI = null;
        List<SysEntityColumnTO> sysEntityColumnList = null;
        List<SysEntityResponseTO> sysEntityResponseTOList = null;
        List<SysEntityTO> sysEntityTOList = null;
        SysEntity sysEntity = null;
        long sysEntityColumnID = 0;
        int entityID = 0;
        try {
            sysEntityResponseTOList = new ArrayList<SysEntityResponseTO>();
            serviceRequest = getServiceRequest(sysEntityTO);
            sysEntityTO.setModifiedBy((userSessionBean.getUserID()).intValue());
            serviceResponse = iSysEntityRemote.saveOrUpdateSysEntity(serviceRequest);
            sysEntityResponseTO = (SysEntityResponseTO) serviceResponse.getResponseTransportObject();
            entityID = sysEntityResponseTO.getEntityId();
            sysEntityResponseTOList.add(sysEntityResponseTO);
            sysEntityTOList = convertResponseTOInENtityTO(sysEntityResponseTOList);
            sysEntityTO = sysEntityTOList.get(0);
            //Updating SysEntityColumn
            sysEntityColumnListUI = sysEntityBean.getSysEntityColumnList();
            for (SysEntityColumnTO sysEntityColumnTO : sysEntityColumnListUI) {
                if (sysEntityColumnTO.isCheckBoxOption()) {
                    /*ysEntity = new SysEntity();
                    sysEntity.setEntityId(entityID);*/
                    sysEntityColumnTO.setSysEntityId(entityID);
                    sysEntityColumnTO.setTenantID(1);
                    sysEntityColumnID = addUpdateSysEntityColumn(sysEntityColumnTO);
                    if (sysEntityColumnTO.isPrimaryKey()) {
                        sysEntityTO.setPrimaryKeyColumnID(((Long) sysEntityColumnID).intValue());
                        sysEntityTO.setPrimaryKey(sysEntityColumnTO.getDbColumnName());
                        sysEntityTO.setLableColumn(sysEntityColumnTO.getLabel());
                        serviceRequest = getServiceRequest(sysEntityTO);
                        serviceResponse = iSysEntityRemote.saveOrUpdateSysEntity(serviceRequest);
                        sysEntityResponseTO = (SysEntityResponseTO) serviceResponse.getResponseTransportObject();
                    }
                }
            }
            sysEntityColumnList = getSysEntityColumnForEditOption(entityID);
            sysEntityBean.setSysEntityTOList(getAllSysEntityList());
            sysEntityBean.setSysEntityColumnList(sysEntityColumnList);
            sysEntityBean.setSysSubModuleList(getAllSysSubModuleList());
            sysEntityBean.setSysEntityTO(sysEntityTO);
            sysEntityBean.setSysEntityTO(sysEntityTO);
            sysEntityBean.setSelectTableEnable(false);
            sysEntityBean.setRenderSaveButton(false);
            sysEntityBean.setRenderUpdateButton(true);
            sysEntityBean.setRenderSysColumnDataTable(true);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ISysEntityConstants.SYSENTITY_SYSCOLUMN_UPDATE_SUCCESS_MSG, ""));
            RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysEntityConstants.SYSENTITY_SYSCOLUMN_UPDATE_ERROR_MSG, ""));
            LOGGER.error("", e);
            sysEntityBean.setRenderSaveButton(false);
            sysEntityBean.setRenderUpdateButton(true);
            sysEntityBean.setRenderSysColumnDataTable(true);
            RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
        }
    }

    public ServiceRequest getServiceRequest(SysEntityTO sysEntityTO)
    {
        ServiceRequest serviceRequest = null;
        SysEntityRequestTO sysEntityRequestTO = null;
        serviceRequest = new ServiceRequest();
        sysEntityRequestTO = new SysEntityRequestTO();
        if (sysEntityTO.getEntityId() == null) {
            sysEntityRequestTO.setEntityId(null);
        } else {
            sysEntityRequestTO.setEntityId(sysEntityTO.getEntityId());
        }
        sysEntityRequestTO.setName(sysEntityTO.getName());
        sysEntityRequestTO.setDescription(sysEntityTO.getDescription());
        sysEntityRequestTO.setTableName(sysEntityTO.getTableName());
        sysEntityRequestTO.setTenantID(sysEntityTO.getTenantID());
        sysEntityRequestTO.setCreatedDate(sysEntityTO.getCreatedDate());
        sysEntityRequestTO.setModifiedDate(sysEntityTO.getModifiedDate());
        sysEntityRequestTO.setCreatedBy(sysEntityTO.getCreatedBy());
        sysEntityRequestTO.setModifiedBy(sysEntityTO.getModifiedBy());
        sysEntityRequestTO.setPrimaryKey(sysEntityTO.getPrimaryKey());
        sysEntityRequestTO.setLableColumn(sysEntityTO.getLableColumn());
        sysEntityRequestTO.setModuleId(sysEntityTO.getModuleId());
        sysEntityRequestTO.setPrimaryKeyColumnID(sysEntityTO.getPrimaryKeyColumnID());
        serviceRequest.setRequestTansportObject(sysEntityRequestTO);
        return serviceRequest;
    }


    private List<SysEntityTO> getAllSysEntityList()
    {
        ServiceRequest serviceRequest = null;
        ServiceResponse serviceResponse = null;
        SysEntityResponseTO sysEntityResponseTO = null;
        List<SysEntityResponseTO> sysEntityResponseTOList = null;
        List<SysEntityTO> sysEntityTOList = null;
        try {
            serviceRequest = new ServiceRequest();
            serviceResponse = iSysEntityRemote.getAllSysEntityList(serviceRequest);
            sysEntityResponseTO = (SysEntityResponseTO) serviceResponse.getResponseTransportObject();
            sysEntityResponseTOList = sysEntityResponseTO.getSysEntityResponseTOList();
            sysEntityTOList = convertResponseTOInENtityTO(sysEntityResponseTOList);
            //Comment SysEntityRequestTO and SysEntityResponseTO has to be changed according to new architecture
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return sysEntityTOList;

    }

    //This has to be moved in Converter
    private List<SysEntityTO> convertResponseTOInENtityTO(List<SysEntityResponseTO> sysEntityResponseTOList)
    {
        List<SysEntityTO> sysEntityTOList = null;
        SysEntityTO sysEntityTO = null;
        sysEntityTOList = new ArrayList<SysEntityTO>();
        Timestamp createdDate = null;
        Timestamp modifiedDate = null;
        try {
            if (sysEntityResponseTOList != null) {
                for (SysEntityResponseTO sysEntityResponseTO : sysEntityResponseTOList) {
                    sysEntityTO = new SysEntityTO();
                    sysEntityTO.setEntityId(sysEntityResponseTO.getEntityId());
                    sysEntityTO.setName(sysEntityResponseTO.getName());
                    sysEntityTO.setDescription(sysEntityResponseTO.getDescription());
                    sysEntityTO.setTableName(sysEntityResponseTO.getTableName());
                    sysEntityTO.setTenantID(sysEntityResponseTO.getTenantID());
                    if(sysEntityResponseTO.getCreatedDate() != null){
                		createdDate = SQLDateHelper.getSqlTimeStampFromDate(new Date(sysEntityResponseTO.getCreatedDate().getTime()));
                	}
                    sysEntityTO.setCreatedDate(createdDate);
                    if(sysEntityResponseTO.getModifiedDate() != null){
                		modifiedDate = SQLDateHelper.getSqlTimeStampFromDate(new Date(sysEntityResponseTO.getModifiedDate().getTime()));
                	}
                    sysEntityTO.setModifiedDate(modifiedDate);
                    sysEntityTO.setCreatedBy(sysEntityResponseTO.getCreatedBy());
                    sysEntityTO.setModifiedBy(sysEntityResponseTO.getModifiedBy());
                    sysEntityTO.setPrimaryKey(sysEntityResponseTO.getPrimaryKey());
                    sysEntityTO.setLableColumn(sysEntityResponseTO.getLableColumn());
                    sysEntityTO.setModuleId(sysEntityResponseTO.getModuleId());
                    sysEntityTOList.add(sysEntityTO);
                }
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return sysEntityTOList;
    }

    public void editSysEntity(SysEntityTO sysEntityTO)
    {
        String selectedTableName = null;
        Map<String, String> columnNameAndDataTypeMap = null;
        List<String> primaryKeyColumnNamesList = null;
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        List<String> tableNameList = null;
        tableNameList = new ArrayList<String>();
        tableNameList.add(sysEntityTO.getTableName());
        sysEntityBean.setSysEntityTableNameList(tableNameList);
        sysEntityBean.setSysEntityTO(sysEntityTO);
        sysEntityBean.setSysEntityTOList(getAllSysEntityList());
        sysEntityBean.setSysSubModuleList(getAllSysSubModuleList());
        sysEntityColumnTOList = getSysEntityColumnForEditOption(sysEntityTO.getEntityId());
        if (sysEntityColumnTOList == null) {
            selectedTableName = sysEntityBean.getSysEntityTO().getTableName();
            columnNameAndDataTypeMap = getColumnNamesAndDataTypeOfAllColumnsOfATable(selectedTableName);
            primaryKeyColumnNamesList = getTablesPrimaryKeyColumnNames(selectedTableName);
            sysEntityColumnTOList = convertColumnNameAndDataTypeListIntoSysEntityColumnTO(columnNameAndDataTypeMap, primaryKeyColumnNamesList);

        } else {
            sysEntityBean.setSelectTableEnable(false);
        }
        sysEntityBean.setSelectTableEnable(false);
        sysEntityBean.setSysEntityColumnList(sysEntityColumnTOList);
        sysEntityBean.setSysSubModuleList(getAllSysSubModuleList());
        sysEntityBean.setSysEntityTO(sysEntityTO);
        sysEntityBean.setRenderSaveButton(false);
        sysEntityBean.setRenderUpdateButton(true);
        sysEntityBean.setRenderSysColumnDataTable(true);
        menuBean.setPage("../sysEntityColumn.xhtml");
    }

    /**
     * setting the sysEntityColumn page
     */

    public void addSysEntitySysColumn()
    {
        List<String> tableNameListNotPresentInSysEntity = null;
        SysEntityTO sysEntityTO = null;
        try {
            sysEntityTO = new SysEntityTO();
            sysEntityBean.setSysEntityTO(sysEntityTO);
            sysEntityBean.setRenderSysColumnDataTable(false);
            sysEntityBean.setRenderSaveButton(false);
            sysEntityBean.setRenderUpdateButton(false);
            //To find all the tables from the Database
            tableNameListNotPresentInSysEntity = getAllTablesFromInformationSchema();
            sysEntityBean.setSysEntityTableNameList(tableNameListNotPresentInSysEntity);
            sysEntityBean.setSysSubModuleList(getAllSysSubModuleList());
            sysEntityBean.setSysEntityTableNameListReadonly(false);
            menuBean.setPage("../sysEntityColumn.xhtml");
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    public List<SysEntityColumnTO> getSysEntityColumnForEditOption(int sysEntityID)
    {
        String selectedTableName = null;
        Map<String, String> columnNameAndDataTypeMap = null;
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        List<SysEntityColumnTO> sysEntityColumnTOListDB = null;
        List<String> primaryKeyColumnNamesList = null;
        try {
            selectedTableName = sysEntityBean.getSysEntityTO().getTableName();
            columnNameAndDataTypeMap = getColumnNamesAndDataTypeOfAllColumnsOfATable(selectedTableName);
            sysEntityColumnTOListDB = getSysEntityColumnListOnBasisOfEntityID(sysEntityID);
            primaryKeyColumnNamesList = getTablesPrimaryKeyColumnNames(selectedTableName);
            sysEntityColumnTOList = convertColumnNameAndDataTypeListAndSysEntityColumnListUnionIntoSysEntityColumnTO(columnNameAndDataTypeMap, primaryKeyColumnNamesList,
                    sysEntityColumnTOListDB);
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return sysEntityColumnTOList;
    }

    /**
     * to find all table names from information schema
     * @return
     * @author raghvendra.mishra
     */
    private List<String> getAllTablesFromInformationSchema()
    {
        List<String> tableNameList = null;
        tableNameList = iSysEntityRemote.getAllTablesFromInformationSchema();
        return tableNameList;
    }

    /**
     * reload sysEntity page on cancel click
     */
    public void reloadSysEntity()
    {
        menuAction.openSysEntityEditCreationPage();
    }

    /**
     * calls on selection of table name in sysEntityColumn page
     */
    public void renderSysEntityColumnDiv()
    {
        String selectedTableName = null;
        SysEntityTO sysEntityTO = null;
        Map<String, String> columnNameAndDataTypeMap = null;
        List<String> primaryKeyColumnNamesList = null;
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        sysEntityTO = new SysEntityTO();
        selectedTableName = sysEntityBean.getSysEntityTO().getTableName();
        columnNameAndDataTypeMap = getColumnNamesAndDataTypeOfAllColumnsOfATable(selectedTableName);
        primaryKeyColumnNamesList = getTablesPrimaryKeyColumnNames(selectedTableName);
        sysEntityColumnTOList = convertColumnNameAndDataTypeListIntoSysEntityColumnTO(columnNameAndDataTypeMap, primaryKeyColumnNamesList);
        sysEntityBean.setSysEntityColumnList(sysEntityColumnTOList);
        sysEntityBean.getSysEntityTO().setName(selectedTableName);
        sysEntityBean.getSysEntityTO().setDescription(selectedTableName);
        sysEntityBean.setRenderSysColumnDataTable(true);
        sysEntityBean.setRenderSaveButton(true);
        sysEntityBean.setRenderUpdateButton(false);
        sysEntityBean.setRenderUpdateButton(false);
        sysEntityTO.setTableName(selectedTableName);
        sysEntityBean.setSysEntityTO(sysEntityTO);
        RequestContext.getCurrentInstance().update("addUpdateSysEntitySysColumnForm");
    }

    public List<SysEntityColumnTO> convertColumnNameAndDataTypeListIntoSysEntityColumnTO(Map<String, String> columnNameAndDataTypeMap, List<String> primaryKeyColumnNamesList)
    {
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        SysEntityColumnTO sysEntityColumnTO = null;
        sysEntityColumnTOList = new ArrayList<SysEntityColumnTO>();
        for (String key : columnNameAndDataTypeMap.keySet()) {
            sysEntityColumnTO = new SysEntityColumnTO();
            if (primaryKeyColumnNamesList.contains(key)) {
                sysEntityColumnTO.setPrimaryKey(true);
                sysEntityColumnTO.setCheckBoxOption(true);
            }
            //sysEntityColumnTO.setEntityColumnName(Character.toLowerCase(key.charAt(0)) + key.substring(1));
            sysEntityColumnTO.setDbColumnName(key);
            sysEntityColumnTO.setDbColumnType(columnNameAndDataTypeMap.get(key));
            sysEntityColumnTOList.add(sysEntityColumnTO);
        }
        return sysEntityColumnTOList;
    }

    public List<SysEntityColumnTO> convertColumnNameAndDataTypeListAndSysEntityColumnListUnionIntoSysEntityColumnTO(Map<String, String> columnNameAndDataTypeMap,
            List<String> primaryKeyColumnNamesList, List<SysEntityColumnTO> sysEntityColumnTOListDB)
    {
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        SysEntityColumnTO sysEntityColumnTO = null;
        Map<String, String> modifiedColumnAndDataTypeMap = null;
        sysEntityColumnTOList = new ArrayList<SysEntityColumnTO>();
        modifiedColumnAndDataTypeMap = new LinkedHashMap<String, String>(columnNameAndDataTypeMap);
        for (SysEntityColumnTO obj : sysEntityColumnTOListDB) {
            if (columnNameAndDataTypeMap.keySet().contains(obj.getDbColumnName())) {
                obj.setCheckBoxOption(true);
                obj.setColumnExistInSysEntityColumn(true);
                modifiedColumnAndDataTypeMap.remove(obj.getDbColumnName());
                if (primaryKeyColumnNamesList.contains(obj.getDbColumnName())) {
                    obj.setPrimaryKey(true);
                }
            }
        }
        for (String key : modifiedColumnAndDataTypeMap.keySet()) {
            sysEntityColumnTO = new SysEntityColumnTO();
            if (primaryKeyColumnNamesList.contains(key)) {
                sysEntityColumnTO.setPrimaryKey(true);
                sysEntityColumnTO.setCheckBoxOption(true);
            }
            sysEntityColumnTO.setEntityColumnName(Character.toLowerCase(key.charAt(0)) + key.substring(1));
            sysEntityColumnTO.setDbColumnName(key);
            sysEntityColumnTO.setDbColumnType(columnNameAndDataTypeMap.get(key));
            sysEntityColumnTOList.add(sysEntityColumnTO);
        }
        if (!sysEntityColumnTOList.isEmpty() && sysEntityColumnTOList.size() > 0) {
            sysEntityColumnTOListDB.addAll(sysEntityColumnTOList);
        }
        return sysEntityColumnTOListDB;
    }

    public List<String> getTablesPrimaryKeyColumnNames(String tableName)
    {
        List<String> primaryKeyColumnNamesList = null;
        try {
            primaryKeyColumnNamesList = iSysEntityRemote.getTablesPrimaryKeyColumnNames(tableName);
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return primaryKeyColumnNamesList;
    }

    public Map<String, String> getColumnNamesAndDataTypeOfAllColumnsOfATable(String tableName)
    {
        List<Object[]> columnNamesAndDataTypeList = null;
        Map<String, String> columnNamesAndDataTypeMap = null;
        try {
            columnNamesAndDataTypeList = iSysEntityRemote.getColumnNamesAndDataTypeOfAllColumnsOfATable(tableName);
            columnNamesAndDataTypeMap = new LinkedHashMap<String, String>();
            for (Object[] obj : columnNamesAndDataTypeList) {
                columnNamesAndDataTypeMap.put(obj[0].toString(), obj[1].toString());
            }
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return columnNamesAndDataTypeMap;
    }

    public void getSysEntityColumnList()
    {
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
    }

    public long addUpdateSysEntityColumn(SysEntityColumnTO sysEntityColumnTO)
    {
        ServiceRequest serviceRequest = null;
        SysEntityColumnResponseTO sysEntityColumnResponseTO = null;
        ServiceResponse serviceResponse = null;
        long sysEntityColumnID = 0;
        try {
            serviceRequest = getServiceRequestSysEntityColumn(sysEntityColumnTO);
            serviceResponse = iSysEntityColumnRemote.saveOrUpdateSysEntityColumn(serviceRequest);
            sysEntityColumnResponseTO = (SysEntityColumnResponseTO) serviceResponse.getResponseTransferObject();
            sysEntityColumnID = sysEntityColumnResponseTO.getEntityColumnID();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysEntityConstants.SYSENTITYCOLUMN_SAVE_ERROR_MSG, ""));
            LOGGER.error("", e);
        }
        return sysEntityColumnID;
    }

    public ServiceRequest getServiceRequestSysEntityColumn(SysEntityColumnTO sysEntityColumnTO)
    {
        ServiceRequest serviceRequest = null;
        SysEntityColumnRequestTO sysEntityColumnRequestTO = null;
        serviceRequest = new ServiceRequest();
        sysEntityColumnRequestTO = new SysEntityColumnRequestTO();
        if (sysEntityColumnTO.getEntityColumnID() != 0L) {
            sysEntityColumnRequestTO.setEntityColumnID(sysEntityColumnTO.getEntityColumnID());
        } else {
            sysEntityColumnRequestTO.setEntityColumnID(null);
        }
        sysEntityColumnRequestTO.setEntityColumnName(sysEntityColumnTO.getEntityColumnName());
        ;
        sysEntityColumnRequestTO.setDescription(sysEntityColumnTO.getDescription());
        sysEntityColumnRequestTO.setDbColumnName(sysEntityColumnTO.getDbColumnName());
        sysEntityColumnRequestTO.setLabel(sysEntityColumnTO.getLabel());
        sysEntityColumnRequestTO.setTenantID(sysEntityColumnTO.getTenantID());
        sysEntityColumnRequestTO.setCreatedBy((userSessionBean.getUserID()).intValue());
        sysEntityColumnRequestTO.setModifiedBy((userSessionBean.getUserID()).intValue());
        sysEntityColumnRequestTO.setEntityColumnType(sysEntityColumnTO.getEntityColumnType());
        sysEntityColumnRequestTO.setDbColumnType(sysEntityColumnTO.getDbColumnType());
        sysEntityColumnRequestTO.setSysEntityId(sysEntityColumnTO.getSysEntityId());
        serviceRequest.setRequestTransferObjectRevised(sysEntityColumnRequestTO);
        return serviceRequest;
    }

    //This has to be moved in Converter
    private List<SysEntityColumnTO> convertResponseTOInSysEntityColumnTO(List<SysEntityColumnResponseTO> sysEntityColumnResponseTOList)
    {
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        SysEntityColumnTO sysEntityColumnTO = null;
        sysEntityColumnTOList = new ArrayList<SysEntityColumnTO>();
        try {
            if (sysEntityColumnResponseTOList != null) {
                for (SysEntityColumnResponseTO sysEntityColumnResponseTO : sysEntityColumnResponseTOList) {
                    sysEntityColumnTO = new SysEntityColumnTO();
                    if (sysEntityColumnResponseTO.getEntityColumnID() != 0) {
                        sysEntityColumnTO.setCheckBoxOption(true);
                    }
                    sysEntityColumnTO.setEntityColumnID(sysEntityColumnResponseTO.getEntityColumnID());
                    sysEntityColumnTO.setSysEntityId(sysEntityColumnResponseTO.getSysEntityId());
                    sysEntityColumnTO.setEntityColumnName(sysEntityColumnResponseTO.getEntityColumnName());
                    sysEntityColumnTO.setDbColumnName(sysEntityColumnResponseTO.getDbColumnName());
                    sysEntityColumnTO.setLabel(sysEntityColumnResponseTO.getLabel());
                    sysEntityColumnTO.setDescription(sysEntityColumnResponseTO.getDescription());
                    sysEntityColumnTO.setTenantID(sysEntityColumnResponseTO.getTenantID());
                    sysEntityColumnTO.setCreatedDate(SQLDateHelper.getSqlTimeStampFromDate(sysEntityColumnResponseTO.getCreatedDate()));
                    sysEntityColumnTO.setModifiedDate(SQLDateHelper.getSqlTimeStampFromDate(sysEntityColumnResponseTO.getModifiedDate()));
                    sysEntityColumnTO.setCreatedBy(sysEntityColumnResponseTO.getCreatedBy());
                    sysEntityColumnTO.setModifiedBy(sysEntityColumnResponseTO.getModifiedBy());
                    sysEntityColumnTO.setEntityColumnType(sysEntityColumnResponseTO.getEntityColumnType());
                    sysEntityColumnTO.setDbColumnType(sysEntityColumnResponseTO.getDbColumnType());
                    sysEntityColumnTOList.add(sysEntityColumnTO);
                }
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return sysEntityColumnTOList;
    }
    
    private List<SysSubModuleTO> convertSysSubModuleResponseToEntityTO(List<SysSubModuleResponseTO> sysSubModuleResponseTOList)
    {
        List<SysSubModuleTO> sysSubModuleTOList = null;
        SysSubModuleTO sysSubModuleTO = null;
        sysSubModuleTOList = new ArrayList<SysSubModuleTO>();
        try {
            if (sysSubModuleTOList != null) {
                for (SysSubModuleResponseTO sysSubModuleResponseTO : sysSubModuleResponseTOList) {
                    sysSubModuleTO = new SysSubModuleTO();
                    
                    sysSubModuleTO.setModuleId(sysSubModuleResponseTO.getModuleId());
                    sysSubModuleTO.setModuleName(sysSubModuleResponseTO.getModuleName());
                    sysSubModuleTO.setModuleDesc(sysSubModuleResponseTO.getModuleDesc());
                    sysSubModuleTO.setParentModuleId(sysSubModuleResponseTO.getParentModuleId());
                    sysSubModuleTO.setAppId(sysSubModuleResponseTO.getAppId());
                    sysSubModuleTO.setTenantId(sysSubModuleResponseTO.getTenantId());
                    sysSubModuleTO.setCreatedBy(sysSubModuleResponseTO.getCreatedBy());
                    sysSubModuleTO.setCreatedDate(SQLDateHelper.getSqlTimeStampFromDate(sysSubModuleResponseTO.getCreatedDate()));
                    sysSubModuleTO.setModifiedBy(sysSubModuleResponseTO.getModifiedBy());
                    sysSubModuleTO.setModifiedDate(SQLDateHelper.getSqlTimeStampFromDate(sysSubModuleResponseTO.getModifiedDate()));               
                    sysSubModuleTO.setTenantId(sysSubModuleResponseTO.getTenantId());
                    sysSubModuleTOList.add(sysSubModuleTO);
                }
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return sysSubModuleTOList;
    }

    public List<SysEntityColumnTO> getSysEntityColumnListOnBasisOfEntityID(int sysEntityID)
        throws Exception
    {
        ServiceRequest serviceRequest = null;
        SysEntityColumnTO sysEntityColumnTO = null;
        SysEntity sysEntity = null;
        ServiceResponse serviceResponse = null;
        List<SysEntityColumnTO> sysEntityColumnTOList = null;
        List<SysEntityColumnResponseTO> result = null;
        /*sysEntity = new SysEntity();
        sysEntity.setEntityId(sysEntityID);*/
        sysEntityColumnTO = new SysEntityColumnTO();
        sysEntityColumnTO.setSysEntityId(sysEntityID);
        serviceRequest = getServiceRequestSysEntityColumn(sysEntityColumnTO);
        serviceResponse = iSysEntityColumnRemote.getAllSysEntityColumnListBySysEntityID(serviceRequest);
        result = (List<SysEntityColumnResponseTO>) serviceResponse.getResponseTransferObjectObjectList();
        sysEntityColumnTOList = convertResponseTOInSysEntityColumnTO(result);
        return sysEntityColumnTOList;
    }

    public List<SysSubModuleTO> getAllSysSubModuleList()
    {
        ServiceRequest serviceRequest = null;
        ServiceResponse serviceResponse = null;
        List<SysSubModuleResponseTO> sysSubModuleResponseTOList = null;
        List<SysSubModuleTO> sysSubModuleTOList = null;
        try {
            serviceRequest = new ServiceRequest();
            serviceResponse = sysSubModuleLocalFacade.getAllSysSubModuleList(serviceRequest);
            sysSubModuleResponseTOList = (List<SysSubModuleResponseTO>) (List) serviceResponse.getResponseTransferObjectObjectList();
            sysSubModuleTOList = convertSysSubModuleResponseToEntityTO(sysSubModuleResponseTOList);
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return sysSubModuleTOList;
    }
}
