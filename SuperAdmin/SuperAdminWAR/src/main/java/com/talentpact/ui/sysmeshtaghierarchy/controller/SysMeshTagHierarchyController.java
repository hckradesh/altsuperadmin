/**
 * 
 */
package com.talentpact.ui.sysmeshtaghierarchy.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysMeshTagHierarchyTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.constants.ISysMeshModuleConstants;
import com.talentpact.business.common.constants.ISysMeshTagHierarchyConstants;
import com.talentpact.business.common.constants.ISysOfferingCategoryConstants;
import com.talentpact.business.dataservice.SysMeshModule.SysMeshModuleService;
import com.talentpact.business.dataservice.SysMeshModule.SysMeshTagHierarchyService;
import com.talentpact.business.dataservice.SysOfferingCategory.SysOfferingCategoryService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysOfferingCategory.bean.SysOfferingCategoryBean;
import com.talentpact.ui.sysmeshtaghierarchy.bean.SysMeshTagHierarchyBean;

/**
 * 
 * @author vivek.goyal
 * 
 */


@Named("sysMeshTagHierarchyController")
@ConversationScoped
public class SysMeshTagHierarchyController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysMeshTagHierarchyController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysMeshTagHierarchyBean sysMeshTagHierarchyBean;

    @Inject
    SysMeshTagHierarchyService sysMeshTagHierarchyService;
    
    @Override
    public void initialize()
    {
        List<SysMeshTagHierarchyTO> sysMeshTagHierarchyTOList = null;
        Set<String> name = null;
        SysMeshTagHierarchyTO sysMeshTagHierarchyTO = null;

        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            sysMeshTagHierarchyTOList = sysMeshTagHierarchyService.getSysMeshTagHierarchyList();
            sysMeshTagHierarchyBean.setSysMeshTagHierarchyTOList(sysMeshTagHierarchyTOList);
            sysMeshTagHierarchyBean.setResourceType("None");
            name = new HashSet<String>();
            for (SysMeshTagHierarchyTO list : sysMeshTagHierarchyTOList) {
                sysMeshTagHierarchyTO = new SysMeshTagHierarchyTO();
                sysMeshTagHierarchyTO = list;
                name.add(sysMeshTagHierarchyTO.getTagName());
            }

            sysMeshTagHierarchyBean.setSysMeshTagHierarchy(name);
            menuBean.setPage("../sysmeshtaghierarchy/sysmeshtaghierarchy.xhtml");
            sysMeshTagHierarchyBean.setRenderErrorMessage(false);
            requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_DATATABLE_RESET);
            requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysMeshTagHierarchy()
    {
    	List<SysMeshModuleTO> sysMeshModuleList = null;
    	try {
    		sysMeshTagHierarchyBean.setSysTagID(0);
    	sysMeshTagHierarchyBean.setTagName(null);
    	sysMeshTagHierarchyBean.setSysMeshTagHierarchy(null);
    	sysMeshTagHierarchyBean.setDataType(null);
    	sysMeshTagHierarchyBean.setMandatory(false);
    	sysMeshTagHierarchyBean.setUniqueSourceCodeNode(false);
    	sysMeshTagHierarchyBean.setDependentUponItself(false);
    	sysMeshTagHierarchyBean.setTagHierarchy(null);
    	sysMeshTagHierarchyBean.setInboundSupported(false);
    	sysMeshTagHierarchyBean.setOutboundSupported(false);
    	sysMeshTagHierarchyBean.setSelectedMeshTagHierarchy(null);
    	sysMeshTagHierarchyBean.setSysMeshModuleList(sysMeshTagHierarchyService.getSysMeshModuleList());
    	sysMeshTagHierarchyBean.setHierarchyList(null);
    	sysMeshTagHierarchyBean.setModuleID(0);
    	sysMeshTagHierarchyBean.setParentID(0);
    	sysMeshTagHierarchyBean.setDependentTagID(0);
    	sysMeshTagHierarchyBean.setTopLevelTag(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    public void addSysMeshTagHierarchy() {
		SysMeshTagHierarchyTO sysMeshTagHierarchyTO = null;
		RequestContext requestContext = null;
		try {
			requestContext = RequestContext.getCurrentInstance();
			sysMeshTagHierarchyTO = new SysMeshTagHierarchyTO();
			sysMeshTagHierarchyBean.setRenderErrorMessage(true);
			setParameter(sysMeshTagHierarchyTO);
			
			if (validateSysMeshTagHierarchy(sysMeshTagHierarchyTO, false)) {
				sysMeshTagHierarchyService.saveSysMeshModule(sysMeshTagHierarchyTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"SysMeshTagHierarchy saved successfully.", ""));
				requestContext
						.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_ADD_DIALOG_HIDE);
				requestContext.update("sysMeshTagHierarchyListForm");
				sysMeshTagHierarchyBean.setRenderErrorMessage(true);
				requestContext
						.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_DATATABLE_RESET);
				requestContext
						.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_PAGINATION_DATATABLE_RESET);
			}
			else
			{
				sysMeshTagHierarchyBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("addSysMeshModule");
			}
		} catch (Exception e) {
			LOGGER_.error("", e);
			sysMeshTagHierarchyBean.setRenderErrorMessage(false);
		}
	}

	

	// To validate if the newly added Sys mesh module is already present in the system
	public boolean validateSysMeshTagHierarchy(SysMeshTagHierarchyTO sysMeshTagHierarchyTO, boolean edit) {
		List<SysMeshTagHierarchyTO> sysMeshModuleTOList = null;
		int j = 0;
		try {
			if (sysMeshTagHierarchyTO.getTagName() == null
					|| sysMeshTagHierarchyTO.getTagName().trim()
							.equalsIgnoreCase("")) {
				j = 1;
				if (sysMeshTagHierarchyTO.getTagName() == null
						|| sysMeshTagHierarchyTO.getTagName().trim()
								.equalsIgnoreCase("")) {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									" Please enter a valid Tag Name", ""));
				}
			} 
			else if(sysMeshTagHierarchyBean.getResourceType() != null && !sysMeshTagHierarchyBean.getResourceType().equals("None"))
			{
				if(sysMeshTagHierarchyTO.getResourceTypeID() == null)
				{
					FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									"'  Please select valid ResourceTypeID.'",
									""));
					
					sysMeshTagHierarchyBean.setRenderErrorMessage(true);
					
					j=1;
				}
			}
			else {
					if(sysMeshTagHierarchyService.validateSysMeshTagHierarchy(sysMeshTagHierarchyTO))
					{
						FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										"'"
												+ sysMeshTagHierarchyTO
														.getTagName()
												+ "'  tag already exists.'",
										""));
						
						sysMeshTagHierarchyBean.setRenderErrorMessage(true);
						
						j=1;
					}
					 sysMeshTagHierarchyBean.setRenderErrorMessage(true);
		             RequestContext.getCurrentInstance().update("sysMeshTagHierarchyEditForm");
			}
			if(j==1)
			{
				return false;
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			LOGGER_.error("", e);
			return false;
		}
	}

	private void setParameter(SysMeshTagHierarchyTO sysMeshTagHierarchyTO) {
		sysMeshTagHierarchyTO.setTagName(sysMeshTagHierarchyBean.getTagName());
		sysMeshTagHierarchyTO.setMandatory(sysMeshTagHierarchyBean.isMandatory());
		sysMeshTagHierarchyTO.setUniqueSourceCodeNode(sysMeshTagHierarchyBean.isUniqueSourceCodeNode());
		sysMeshTagHierarchyTO.setDependentUponItself(sysMeshTagHierarchyBean.isDependentUponItself());
		sysMeshTagHierarchyTO.setInboundSupported(sysMeshTagHierarchyBean.isInboundSupported());
		sysMeshTagHierarchyTO.setOutboundSupported(sysMeshTagHierarchyBean.isOutboundSupported());
		if(sysMeshTagHierarchyBean.getResourceType() != null && !sysMeshTagHierarchyBean.getResourceType().equals("None"))
		{
			if(sysMeshTagHierarchyBean.getResourceType().equals("Table") && sysMeshTagHierarchyBean.getEntityID() !=0)
			{
				sysMeshTagHierarchyTO.setResourceTypeID(sysMeshTagHierarchyBean.getEntityID());
			}
			else if(sysMeshTagHierarchyBean.getResourceType().equals("Column") && sysMeshTagHierarchyBean.getEntityColumnID() !=0)
			{
				sysMeshTagHierarchyTO.setResourceTypeID(sysMeshTagHierarchyBean.getEntityColumnID());
			}
		}
		sysMeshTagHierarchyTO.setModuleID(sysMeshTagHierarchyBean.getModuleID());
		if(!sysMeshTagHierarchyBean.isTopLevelTag())
		{
			sysMeshTagHierarchyTO.setParentID(sysMeshTagHierarchyBean.getParentID());
		}
		sysMeshTagHierarchyTO.setDependentTagID(sysMeshTagHierarchyBean.getDependentTagID());
		for(SysMeshModuleTO sysMeshModuleTO: sysMeshTagHierarchyBean.getHierarchyList())
		{
			if(sysMeshModuleTO.getModuleID().intValue() == sysMeshTagHierarchyBean.getParentID())
			{
				sysMeshTagHierarchyTO.setTagHierarchy(sysMeshModuleTO.getModuleName() + ">" + sysMeshTagHierarchyBean.getTagName());
			}
		}
		if(sysMeshTagHierarchyTO.getTagHierarchy() == null || sysMeshTagHierarchyTO.getTagHierarchy().trim().equals(""))
		{
			sysMeshTagHierarchyTO.setTagHierarchy(sysMeshTagHierarchyBean.getTagName());
		}
	}
	
	public void updateSysMeshTagHierarchy() {
        RequestContext requestContext = null;
        SysMeshTagHierarchyTO sysMeshTagHierarchyTO = new SysMeshTagHierarchyTO();
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysMeshTagHierarchyTO.setSysTagID(sysMeshTagHierarchyBean.getSysTagID());
            setParameter(sysMeshTagHierarchyTO);
            if (validateSysMeshTagHierarchy(sysMeshTagHierarchyTO, true)) {
            	sysMeshTagHierarchyService.editSysMeshModule(sysMeshTagHierarchyTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysMeshHierarchy updated successfully.", ""));
                requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_DATATABLE_RESET);
                requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_PAGINATION_DATATABLE_RESET);
                RequestContext.getCurrentInstance().update("sysMeshTagHierarchyListForm");
                sysMeshTagHierarchyBean.setRenderErrorMessage(true);

            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            //sysMeshTagHierarchyBean.setRenderSysOfferingCategoryPopup(false);
            requestContext.execute(ISysMeshTagHierarchyConstants.SYSMESHTAGHIERARCHY_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("sysMeshTagHierarchyEditForm");
        }
	}
	
	 
	public void updateEditSysMeshModule(SysMeshTagHierarchyTO sysMeshTagHierarchyTO) {
		 try {
			 	if(sysMeshTagHierarchyBean.getSysMeshModuleList() == null)
		    	 {
		    	 	sysMeshTagHierarchyBean.setSysMeshModuleList(sysMeshTagHierarchyService.getSysMeshModuleList());
		    	 } 
			 	 sysMeshTagHierarchyBean.setModuleID(sysMeshTagHierarchyTO.getModuleID());
				 sysMeshTagHierarchyBean.setSysTagID(sysMeshTagHierarchyTO.getSysTagID());
				 sysMeshTagHierarchyBean.setTagName(sysMeshTagHierarchyTO.getTagName());
				 if(sysMeshTagHierarchyTO.getParentID() != 0)
				 {
					 sysMeshTagHierarchyBean.setParentID(sysMeshTagHierarchyTO.getParentID());
					 sysMeshTagHierarchyBean.setTopLevelTag(false);
				 }
				 else
				 {
					 sysMeshTagHierarchyBean.setTopLevelTag(true);
					 sysMeshTagHierarchyBean.setParentID(0);
				 }
				 sysMeshTagHierarchyBean.setDependentTagID(sysMeshTagHierarchyTO.getDependentTagID());
				 sysMeshTagHierarchyBean.setMandatory(sysMeshTagHierarchyTO.isMandatory());
				 sysMeshTagHierarchyBean.setUniqueSourceCodeNode(sysMeshTagHierarchyTO.isUniqueSourceCodeNode());
				 sysMeshTagHierarchyBean.setDependentUponItself(sysMeshTagHierarchyTO.isDependentUponItself());
				 sysMeshTagHierarchyBean.setInboundSupported(sysMeshTagHierarchyTO.isInboundSupported());
				 sysMeshTagHierarchyBean.setOutboundSupported(sysMeshTagHierarchyTO.isOutboundSupported());
			
				 sysMeshTagHierarchyBean.setHierarchyList(sysMeshTagHierarchyService.getHierarchyList(sysMeshTagHierarchyTO.getModuleID(), sysMeshTagHierarchyTO.getSysTagID()));
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	}
	
	public void onMeshModuleChange()
	{
		try
		{
				sysMeshTagHierarchyBean.setHierarchyList(sysMeshTagHierarchyService.getHierarchyList(sysMeshTagHierarchyBean.getModuleID(), sysMeshTagHierarchyBean.getSysTagID()));
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	public void onResourceTypeChange()
	{
		try
		{
			sysMeshTagHierarchyBean.setEntityID(0);
			sysMeshTagHierarchyBean.setEntityColumnID(0);
			if(sysMeshTagHierarchyBean.getResourceType() != null && (sysMeshTagHierarchyBean.getResourceType().equalsIgnoreCase("Table") || sysMeshTagHierarchyBean.getResourceType().equalsIgnoreCase("Column")))
			{
				if(sysMeshTagHierarchyBean.getSysEntityList() == null)
				{
						sysMeshTagHierarchyBean.setSysEntityList(sysMeshTagHierarchyService.getSysEntityList());
				}
				
				if(sysMeshTagHierarchyBean.getResourceType().equalsIgnoreCase("Table"))
				{
					sysMeshTagHierarchyBean.setEntityColumnID(0);
				}
			}
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	public void onEntityChange()
	{
		try
		{
			sysMeshTagHierarchyBean.setEntityColumnID(0);
			if(sysMeshTagHierarchyBean.getEntityID() != 0)
			{
				sysMeshTagHierarchyBean.setSysEntityColumnList(sysMeshTagHierarchyService.getSysEntityColumnList(sysMeshTagHierarchyBean.getEntityID()));
				
			}
			else
			{
				sysMeshTagHierarchyBean.setSysEntityColumnList(null);
				sysMeshTagHierarchyBean.setEntityColumnID(0);
			}
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
}