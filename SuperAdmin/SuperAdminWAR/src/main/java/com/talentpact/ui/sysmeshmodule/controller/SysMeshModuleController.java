/**
 * 
 */
package com.talentpact.ui.sysmeshmodule.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysMeshModuleTO;
import com.talentpact.business.application.transport.output.SysOfferingCategoryTO;
import com.talentpact.business.common.constants.ISysMeshModuleConstants;
import com.talentpact.business.common.constants.ISysOfferingCategoryConstants;
import com.talentpact.business.dataservice.SysMeshModule.SysMeshModuleService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysmeshmodule.bean.SysMeshModuleBean;

/**
 * 
 * @author vivek.goyal
 * 
 */

@Named("sysMeshModuleController")
@ConversationScoped
public class SysMeshModuleController extends CommonController implements Serializable, IDefaultAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2937030591547477634L;

	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysMeshModuleController.class);

	@Inject
	MenuBean menuBean;

	@Inject
	SysMeshModuleBean sysMeshModuleBean;

	@Inject
	SysMeshModuleService sysMeshModuleService;

	@Override
	public void initialize() {
		List<SysMeshModuleTO> sysMeshModuleTOList = null;

		try {
			RequestContext requestContext = RequestContext.getCurrentInstance();
			sysMeshModuleTOList = sysMeshModuleService.getSysMeshModuleList();

			sysMeshModuleBean.setSysMeshModuleList(sysMeshModuleTOList);
			menuBean.setPage("../sysmeshmodule/sysmeshmodule.xhtml");
			sysMeshModuleBean.setRenderErrorMessage(false);

			// requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_DATATABLE_RESET);
			// requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_PAGINATION_DATATABLE_RESET);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveSysMeshModule() {
		SysMeshModuleTO sysMeshModuleTO = null;
		RequestContext requestContext = null;
		try {
			requestContext = RequestContext.getCurrentInstance();
			sysMeshModuleTO = new SysMeshModuleTO();
			sysMeshModuleBean.setRenderErrorMessage(true);
			sysMeshModuleTO.setModuleID(sysMeshModuleBean.getModuleID());
			sysMeshModuleTO.setSysMeshModuleName(sysMeshModuleBean.getSysMeshModuleName().trim());
			sysMeshModuleTO.setSysMeshModuleDescription(sysMeshModuleBean.getSysMeshModuleDescription().trim());

			if (validateSysMeshModule(sysMeshModuleTO, false)) {
				sysMeshModuleService.saveSysMeshModule(sysMeshModuleTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "SysMeshModule saved successfully.", ""));
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_ADD_DIALOG_HIDE);
				requestContext.update("sysMeshModuleForm");
				sysMeshModuleBean.setRenderErrorMessage(true);
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_DATATABLE_RESET);
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_PAGINATION_DATATABLE_RESET);
			} else {
				sysMeshModuleBean.setRenderErrorMessage(true);
				RequestContext.getCurrentInstance().update("addSysMeshModule");
			}
		} catch (Exception e) {
			LOGGER_.error("", e);
			sysMeshModuleBean.setRenderErrorMessage(false);
		}
	}

	// To validate if the newly added Sys mesh module is already present in the
	// system
	public boolean validateSysMeshModule(SysMeshModuleTO sysMeshModuleTO, boolean edit) {
		List<SysMeshModuleTO> sysMeshModuleTOList = null;
		int j = 0;
		try {
			if (sysMeshModuleTO.getSysMeshModuleName() == null
					|| sysMeshModuleTO.getSysMeshModuleName().trim().equalsIgnoreCase("")
					|| sysMeshModuleTO.getSysMeshModuleDescription() == null
					|| sysMeshModuleTO.getSysMeshModuleDescription().trim().equalsIgnoreCase("")
					|| sysMeshModuleTO.getModuleID() == null || sysMeshModuleTO.getModuleID() == 0) {
				j = 1;
				if (sysMeshModuleTO.getSysMeshModuleName() == null
						|| sysMeshModuleTO.getSysMeshModuleName().trim().equalsIgnoreCase("")) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid Module Name.", ""));
				}
				if (sysMeshModuleTO.getSysMeshModuleDescription() == null
						|| sysMeshModuleTO.getSysMeshModuleDescription().trim().equalsIgnoreCase("")) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							" Please enter a valid Module Description.", ""));
				}
				if (sysMeshModuleTO.getModuleID() == null || sysMeshModuleTO.getModuleID() == 0) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							" Please select valid Sys SubModule Name.", ""));
				}
			} else {
				if (sysMeshModuleService.validateSysMeshModuleList(sysMeshModuleTO)) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"'" + sysMeshModuleTO.getSysMeshModuleName() + "' module already exists.", ""));

					sysMeshModuleBean.setRenderErrorMessage(true);

					j = 1;
				}
				sysMeshModuleBean.setRenderErrorMessage(true);
				RequestContext.getCurrentInstance().update("MeshModuleEditForm");
			}
			if (j == 1) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			LOGGER_.error("", e);
			return false;
		}
	}

	public void updateSysMeshModule() {
		RequestContext requestContext = null;
		SysMeshModuleTO sysMeshModuleTO = new SysMeshModuleTO();
		try {
			requestContext = RequestContext.getCurrentInstance();
			sysMeshModuleTO.setSysMeshModuleID(sysMeshModuleBean.getSysMeshModuleID());
			sysMeshModuleTO.setSysMeshModuleName(sysMeshModuleBean.getSysMeshModuleName());
			sysMeshModuleTO.setSysMeshModuleDescription(sysMeshModuleBean.getSysMeshModuleDescription());
			sysMeshModuleTO.setModuleID(sysMeshModuleBean.getModuleID());
			if (validateSysMeshModule(sysMeshModuleTO, true)) {
				sysMeshModuleService.editSysMeshModule(sysMeshModuleTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "SysMeshModule updated successfully.", ""));
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_EDIT_DIALOG_HIDE);
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_DATATABLE_RESET);
				requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_PAGINATION_DATATABLE_RESET);
				RequestContext.getCurrentInstance().update("sysMeshModuleForm");
				sysMeshModuleBean.setRenderErrorMessage(true);

			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
			LOGGER_.error("", e);
			// sysMeshModuleBean.setRenderSysOfferingCategoryPopup(false);
			requestContext.execute(ISysMeshModuleConstants.SYSMESHMODULE_EDIT_DIALOG_HIDE);
			RequestContext.getCurrentInstance().update("MeshModuleEditForm");
		}
	}

	public void resetSysMeshModule() {
		try {
			sysMeshModuleBean.setSysMeshModuleID(0);
			sysMeshModuleBean.setSysMeshModuleName(null);
			sysMeshModuleBean.setSysMeshModuleDescription(null);
			sysMeshModuleBean.setModuleID(0);
			if (sysMeshModuleBean.getSysSubModuleTOList() == null) {
				sysMeshModuleBean.setSysSubModuleTOList(sysMeshModuleService.getSysSubModuleList());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateEditSysMeshModule(SysMeshModuleTO sysMeshModuleTO) {
		try {
			sysMeshModuleBean.setSysMeshModuleID(sysMeshModuleTO.getSysMeshModuleID());
			sysMeshModuleBean.setSysMeshModuleName(sysMeshModuleTO.getSysMeshModuleName());
			sysMeshModuleBean.setSysMeshModuleDescription(sysMeshModuleTO.getSysMeshModuleDescription());
			sysMeshModuleBean.setModuleID(sysMeshModuleTO.getModuleID());
			sysMeshModuleBean.setModuleName(sysMeshModuleTO.getModuleName());
			if (sysMeshModuleBean.getSysSubModuleTOList() == null) {
				sysMeshModuleBean.setSysSubModuleTOList(sysMeshModuleService.getSysSubModuleList());
			}

			// sysMeshModuleBean.setSysSubModuleTOList(sysMeshModuleService.getSysSubModuleList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}