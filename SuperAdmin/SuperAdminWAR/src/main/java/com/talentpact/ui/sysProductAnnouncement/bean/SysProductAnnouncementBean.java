package com.talentpact.ui.sysProductAnnouncement.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysProductAnnouncement.controller.SysProductAnnouncementController;
import com.talentpact.ui.sysProductAnnouncement.to.SysProductAnnouncementTO;

/**
 * 
 * @author vivek.goyal
 *
 */


@SuppressWarnings("serial")
@Named("sysProductAnnouncementBean")
@ConversationScoped
public class SysProductAnnouncementBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysProductAnnouncementController sysProductAnnouncementController;

    private Integer sysProductAnnouncementID;

    private String sysProductAnnouncement;

    private String sysProductAnnouncementLabel;

    private Date validFrom;

    private Date validTo;

    private Boolean active;

    private List<SysProductAnnouncementTO> sysProductAnnouncementTOList;

    private List<SysProductAnnouncementTO> organizationTOList;

    private boolean renderSysOfferingCategoryPopup;

    private boolean renderEditSysOfferingCategoryPopup;

    private boolean renderErrorMessage;

    private String status;

    private Date currentDate = new Date();

    private SysProductAnnouncementTO sysProductAnnouncementTO;

    private int[] selectedOrganizations;

    private int[] duplicateSelectedOrganizations;

    private Map<Integer, SysProductAnnouncementTO> organizationTOMap;

    private UploadedFile announcementBannerFile;

    private String uploadedAnnouncementBannerFileName;

    private String bannerUrl;

    private String announcementBanner;
    
    public String getAnnouncementBanner()
    {
        return announcementBanner;
    }
    public void setAnnouncementBanner(String announcementBanner)
    {
        this.announcementBanner = announcementBanner;
    }

    public String getBannerUrl()
    {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl)
    {
        this.bannerUrl = bannerUrl;
    }

    public UploadedFile getAnnouncementBannerFile()
    {
        return announcementBannerFile;
    }

    public void setAnnouncementBannerFile(UploadedFile announcementBannerFile)
    {
        this.announcementBannerFile = announcementBannerFile;
    }

    public void setUploadedAnnouncementBannerFileName(String uploadedAnnouncementBannerFileName)
    {
        this.uploadedAnnouncementBannerFileName = uploadedAnnouncementBannerFileName;
    }

    public String getUploadedAnnouncementBannerFileName()
    {
        return uploadedAnnouncementBannerFileName;
    }

    public SysProductAnnouncementTO getSysProductAnnouncementTO()
    {
        return sysProductAnnouncementTO;
    }

    public void setSysProductAnnouncementTO(SysProductAnnouncementTO sysProductAnnouncementTO)
    {
        this.sysProductAnnouncementTO = sysProductAnnouncementTO;
    }

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysProductAnnouncementController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public Integer getSysProductAnnouncementID()
    {
        return sysProductAnnouncementID;
    }

    public void setSysProductAnnouncementID(Integer sysProductAnnouncementID)
    {
        this.sysProductAnnouncementID = sysProductAnnouncementID;
    }

    public String getSysProductAnnouncement()
    {
        return sysProductAnnouncement;
    }

    public void setSysProductAnnouncement(String sysProductAnnouncement)
    {
        this.sysProductAnnouncement = sysProductAnnouncement;
    }

    public String getSysProductAnnouncementLabel()
    {
        return sysProductAnnouncementLabel;
    }

    public void setSysProductAnnouncementLabel(String sysProductAnnouncementLabel)
    {
        this.sysProductAnnouncementLabel = sysProductAnnouncementLabel;
    }

    public Date getValidFrom()
    {
        return validFrom;
    }

    public void setValidFrom(Date validFrom)
    {
        this.validFrom = validFrom;
    }

    public Date getValidTo()
    {
        return validTo;
    }

    public void setValidTo(Date validTo)
    {
        this.validTo = validTo;
    }

    public Boolean getActive()
    {
        return active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public List<SysProductAnnouncementTO> getSysProductAnnouncementTOList()
    {
        return sysProductAnnouncementTOList;
    }

    public void setSysProductAnnouncementTOList(List<SysProductAnnouncementTO> sysProductAnnouncementTOList)
    {
        this.sysProductAnnouncementTOList = sysProductAnnouncementTOList;
    }

    public boolean isRenderSysOfferingCategoryPopup()
    {
        return renderSysOfferingCategoryPopup;
    }

    public void setRenderSysOfferingCategoryPopup(boolean renderSysOfferingCategoryPopup)
    {
        this.renderSysOfferingCategoryPopup = renderSysOfferingCategoryPopup;
    }

    public boolean isRenderEditSysOfferingCategoryPopup()
    {
        return renderEditSysOfferingCategoryPopup;
    }

    public void setRenderEditSysOfferingCategoryPopup(boolean renderEditSysOfferingCategoryPopup)
    {
        this.renderEditSysOfferingCategoryPopup = renderEditSysOfferingCategoryPopup;
    }

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Date getCurrentDate()
    {
        return currentDate;
    }

    public List<SysProductAnnouncementTO> getOrganizationTOList()
    {
        return organizationTOList;
    }

    public void setOrganizationTOList(List<SysProductAnnouncementTO> organizationTOList)
    {
        this.organizationTOList = organizationTOList;
    }

    public int[] getSelectedOrganizations()
    {
        return selectedOrganizations;
    }

    public void setSelectedOrganizations(int[] selectedOrganizations)
    {
        this.selectedOrganizations = selectedOrganizations;
    }

    public Map<Integer, SysProductAnnouncementTO> getOrganizationTOMap()
    {
        return organizationTOMap;
    }

    public void setOrganizationTOMap(Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        this.organizationTOMap = organizationTOMap;
    }

    public int[] getDuplicateSelectedOrganizations()
    {
        return duplicateSelectedOrganizations;
    }

    public void setDuplicateSelectedOrganizations(int[] duplicateSelectedOrganizations)
    {
        this.duplicateSelectedOrganizations = duplicateSelectedOrganizations;
    }
}