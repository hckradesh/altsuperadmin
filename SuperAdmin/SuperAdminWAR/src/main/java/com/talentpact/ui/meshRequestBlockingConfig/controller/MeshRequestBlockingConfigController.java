/**
 *
 */
package com.talentpact.ui.meshRequestBlockingConfig.controller;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.MeshRequestBlockingConfigTO;
import com.talentpact.business.common.constants.IMeshRequestBlockingConfig;
import com.talentpact.business.dataservice.MeshRequestBlockingConfig.MeshRequestBlockingConfigService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.meshRequestBlockingConfig.bean.MeshRequestBlockingConfigBean;


@Named("meshRequestBlockingConfigController")
@ConversationScoped
public class MeshRequestBlockingConfigController extends CommonController implements Serializable, IDefaultAction {
    /**
     *
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(MeshRequestBlockingConfigController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    MeshRequestBlockingConfigBean meshRequestBlockingConfigBean;

    @Inject
    MeshRequestBlockingConfigService meshRequestBlockingConfigService;

    @Override
    public void initialize() {
        List<MeshRequestBlockingConfigTO> meshRequestBlockingConfigTOS = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
//            resetSysMeshTagMapping();
            meshRequestBlockingConfigTOS = meshRequestBlockingConfigService.getMeshRequestBlockingConfigList();
            meshRequestBlockingConfigBean.setMeshRequestBlockingConfigTOList(meshRequestBlockingConfigTOS);
            meshRequestBlockingConfigBean.setMeshRequestTypes(meshRequestBlockingConfigService.getMeshRequestTypeTOS());
            menuBean.setPage("../MeshRequestBlockingConfig/meshRequestBlockingConfig.xhtml");
            requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_DATATABLE_RESET);
            requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetMeshRequestBlockingConfig() {
    	meshRequestBlockingConfigBean.setBlockingRequestTypeId(null);
    	meshRequestBlockingConfigBean.setRequestTypeId(null);
    	meshRequestBlockingConfigBean.setRenderErrorMessage(false);
    }

    public void refreshPopUp(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO) {
        try {
            meshRequestBlockingConfigBean.setBlockingConfigID(meshRequestBlockingConfigTO.getBlockingConfigID());
            meshRequestBlockingConfigBean.setBlockingRequestTypeId(meshRequestBlockingConfigTO.getBlockingRequestTypeID());
            meshRequestBlockingConfigBean.setRequestTypeId(meshRequestBlockingConfigTO.getRequestTypeID());
            meshRequestBlockingConfigBean.setRequestTypeName(meshRequestBlockingConfigTO.getRequestTypeName());
            meshRequestBlockingConfigBean.setBlockingRequestTypeName(meshRequestBlockingConfigTO.getBlockingRequestTypeName());
            meshRequestBlockingConfigBean.setRenderErrorMessage(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMeshRequestBlockingConfig() {
        MeshRequestBlockingConfigTO meshRequestBlockingConfigTO = null;
        MeshRequestBlockingConfigTO validate = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            meshRequestBlockingConfigTO = new MeshRequestBlockingConfigTO();
            meshRequestBlockingConfigTO.setBlockingConfigID(meshRequestBlockingConfigBean.getBlockingConfigID());
            meshRequestBlockingConfigTO.setRequestTypeID(meshRequestBlockingConfigBean.getRequestTypeId());
            meshRequestBlockingConfigTO.setBlockingRequestTypeID(meshRequestBlockingConfigBean.getBlockingRequestTypeId());
            meshRequestBlockingConfigTO.setRequestTypeName(meshRequestBlockingConfigBean.getRequestTypeName());
            meshRequestBlockingConfigTO.setBlockingRequestTypeName(meshRequestBlockingConfigBean.getBlockingRequestTypeName());
            validate = validateMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
            if (validate == null) {
                meshRequestBlockingConfigService.editMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "MeshRequestBlockingConfig updated successfully.", ""));
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_EDIT_DIALOG_HIDE);
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_DATATABLE_RESET);
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_PAGINATION_DATATABLE_RESET);
                meshRequestBlockingConfigBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("meshRequestBlockingConfigListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "' RequestType " + validate.getRequestTypeName() + " with BlockingRequestType " + validate.getBlockingRequestTypeName()
                                + "' is already exists.", ""));
                meshRequestBlockingConfigBean.setRenderErrorMessage(true);
                RequestContext.getCurrentInstance().update("meshRequestBlockingConfigEditForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            meshRequestBlockingConfigBean.setRenderMeshRequestBlockingConfigPopup(false);
            requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("meshRequestBlockingConfigEditForm");
        }
    }

    public void addMeshRequestBlockingConfig() {
        MeshRequestBlockingConfigTO meshRequestBlockingConfigTO = null;
        MeshRequestBlockingConfigTO validate = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            meshRequestBlockingConfigTO = new MeshRequestBlockingConfigTO();
            meshRequestBlockingConfigTO.setBlockingConfigID(meshRequestBlockingConfigBean.getBlockingConfigID());
            meshRequestBlockingConfigTO.setRequestTypeID(meshRequestBlockingConfigBean.getRequestTypeId());
            meshRequestBlockingConfigTO.setBlockingRequestTypeID(meshRequestBlockingConfigBean.getBlockingRequestTypeId());
            meshRequestBlockingConfigTO.setRequestTypeName(meshRequestBlockingConfigBean.getRequestTypeName());
            meshRequestBlockingConfigTO.setBlockingRequestTypeName(meshRequestBlockingConfigBean.getBlockingRequestTypeName());
            validate = validateMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
            if (validate == null) {
                meshRequestBlockingConfigService.addMeshRequestBlockingConfig(meshRequestBlockingConfigTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "MeshRequestBlockingConfig saved successfully.", ""));
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_ADD_DIALOG_HIDE);
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_DATATABLE_RESET);
                requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_PAGINATION_DATATABLE_RESET);
                RequestContext.getCurrentInstance().update("meshRequestBlockingConfigListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "' RequestType " + validate.getRequestTypeName() + " with BlockingRequestType " + validate.getBlockingRequestTypeName()
                                + "' is already exists.", ""));
               
               RequestContext.getCurrentInstance().update("addMeshRequestBlockingConfigForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            meshRequestBlockingConfigBean.setRenderMeshRequestBlockingConfigPopup(false);
            requestContext.execute(IMeshRequestBlockingConfig.MESHREQUESTBLOCKINGCONFIG_ADD_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("addMeshRequestBlockingConfigForm");
        }
    }

    private MeshRequestBlockingConfigTO validateMeshRequestBlockingConfig(MeshRequestBlockingConfigTO meshRequestBlockingConfigTO) {
        MeshRequestBlockingConfigTO result = null;
        try {
            result = meshRequestBlockingConfigService.checkExists(meshRequestBlockingConfigTO.getRequestTypeID(), meshRequestBlockingConfigTO.getBlockingRequestTypeID());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}