/**
 * 
 */
package com.talentpact.ui.altbenefits.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.altbenefits.to.BenefitVendorTO;
import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.SectorVendorOfferingTO;
import com.alt.altbenefits.to.SysSectorTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.talentpact.ui.altbenefits.controller.SectorWiseOfferingController;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;

/**
 * @author javed.ali
 *
 */
@Named(value = "sectorVendorOfferingBean")
@ConversationScoped
public class SectorVendorOfferingBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 14338909052523324L;

    @Inject
    SectorWiseOfferingController sectorWiseOfferingController;

    private List<SectorVendorOfferingTO> sectorVendorOfferingTOList;

    private List<SysSectorTO> sectorTOList;

    private VendorOfferingTemplateTO vendorOfferingTemplateTO;

    private SectorVendorOfferingTO selectedSectorBasedBenefitTO;

    private PeakRecurringDetailTO peakRecurringDetailTO;

    private BenefitVendorTO sysBenefitVendorTO;

    private boolean active;

    private boolean renderMaximumLimitApplicableDetail;

    private OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO;

    private OfferingCodeTO offeringCodeTO;


    private boolean renderFormMessage;

    private Map<Integer, Integer> sectorOfferSequenceMap;


    private String selectedSectorName;

    private Integer selectedSectorID;

    private String sequenceValue;

    private boolean renderVendorOffering;

    private List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList;

    private List<VendorOfferingTemplateTO> selectedVendorOfferingTemplateTOList;

    public List<VendorOfferingTemplateTO> getSelectedVendorOfferingTemplateTOList()
    {
        return selectedVendorOfferingTemplateTOList;
    }

    public void setSelectedVendorOfferingTemplateTOList(List<VendorOfferingTemplateTO> selectedVendorOfferingTemplateTOList)
    {
        this.selectedVendorOfferingTemplateTOList = selectedVendorOfferingTemplateTOList;
    }


    private Integer[] selectedSector;

    private Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap;

    public Integer getSelectedSectorID()
    {
        return selectedSectorID;
    }

    public String getSelectedSectorName()
    {
        return selectedSectorName;
    }

    public void setSelectedSectorID(Integer selectedSectorID)
    {
        this.selectedSectorID = selectedSectorID;
    }

    public void setSelectedSectorName(String selectedSectorName)
    {
        this.selectedSectorName = selectedSectorName;
    }

    public PeakRecurringDetailTO getPeakRecurringDetailTO()
    {
        return peakRecurringDetailTO;
    }

    public SectorVendorOfferingTO getSelectedSectorBasedBenefitTO()
    {
        return selectedSectorBasedBenefitTO;
    }

    public BenefitVendorTO getSysBenefitVendorTO()
    {
        return sysBenefitVendorTO;
    }

    public VendorOfferingTemplateTO getVendorOfferingTemplateTO()
    {
        return vendorOfferingTemplateTO;
    }

    public void setPeakRecurringDetailTO(PeakRecurringDetailTO peakRecurringDetailTO)
    {
        this.peakRecurringDetailTO = peakRecurringDetailTO;
    }

    public void setSelectedSectorBasedBenefitTO(SectorVendorOfferingTO selectedSectorBasedBenefitTO)
    {
        this.selectedSectorBasedBenefitTO = selectedSectorBasedBenefitTO;
    }

    public void setSysBenefitVendorTO(BenefitVendorTO sysBenefitVendorTO)
    {
        this.sysBenefitVendorTO = sysBenefitVendorTO;
    }

    public void setVendorOfferingTemplateTO(VendorOfferingTemplateTO vendorOfferingTemplateTO)
    {
        this.vendorOfferingTemplateTO = vendorOfferingTemplateTO;
    }

    public Map<String, List<VendorOfferingTemplateTO>> getSectorVendorOfferingmap()
    {
        return sectorVendorOfferingmap;
    }

    public void setSectorVendorOfferingmap(Map<String, List<VendorOfferingTemplateTO>> sectorVendorOfferingmap)
    {
        this.sectorVendorOfferingmap = sectorVendorOfferingmap;
    }

    public Integer[] getSelectedSector()
    {
        return selectedSector;
    }

    public void setSelectedSector(Integer[] selectedSector)
    {
        this.selectedSector = selectedSector;
    }

    public List<VendorOfferingTemplateTO> getVendorOfferingTemplateTOList()
    {
        return vendorOfferingTemplateTOList;
    }

    public void setVendorOfferingTemplateTOList(List<VendorOfferingTemplateTO> vendorOfferingTemplateTOList)
    {
        this.vendorOfferingTemplateTOList = vendorOfferingTemplateTOList;
    }


    @Override
    public void initialize()
    {
        endConversation();
        beginConversation();
        sectorWiseOfferingController.initialize();
    }

    public List<SectorVendorOfferingTO> getSectorVendorOfferingTOList()
    {
        return sectorVendorOfferingTOList;
    }

    public void setSectorVendorOfferingTOList(List<SectorVendorOfferingTO> sectorVendorOfferingTOList)
    {
        this.sectorVendorOfferingTOList = sectorVendorOfferingTOList;
    }

    public List<SysSectorTO> getSectorTOList()
    {
        return sectorTOList;
    }

    public void setSectorTOList(List<SysSectorTO> sectorTOList)
    {
        this.sectorTOList = sectorTOList;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isRenderFormMessage()
    {
        return renderFormMessage;
    }

    public void setRenderFormMessage(boolean renderFormMessage)
    {
        this.renderFormMessage = renderFormMessage;
    }

    public Map<Integer, Integer> getSectorOfferSequenceMap()
    {
        return sectorOfferSequenceMap;
    }

    public void setSectorOfferSequenceMap(Map<Integer, Integer> sectorOfferSequenceMap)
    {
        this.sectorOfferSequenceMap = sectorOfferSequenceMap;
    }

    public String getSequenceValue()
    {
        return sequenceValue;
    }

    public void setSequenceValue(String sequenceValue)
    {
        this.sequenceValue = sequenceValue;
    }

    public boolean isRenderVendorOffering()
    {
        return renderVendorOffering;
    }

    public void setRenderVendorOffering(boolean renderVendorOffering)
    {
        this.renderVendorOffering = renderVendorOffering;
    }

    public SectorWiseOfferingController getSectorWiseOfferingController()
    {
        return sectorWiseOfferingController;
    }

    public void setSectorWiseOfferingController(SectorWiseOfferingController sectorWiseOfferingController)
    {
        this.sectorWiseOfferingController = sectorWiseOfferingController;
    }

    public boolean isRenderMaximumLimitApplicableDetail()
    {
        return renderMaximumLimitApplicableDetail;
    }

    public void setRenderMaximumLimitApplicableDetail(boolean renderMaximumLimitApplicableDetail)
    {
        this.renderMaximumLimitApplicableDetail = renderMaximumLimitApplicableDetail;
    }

    public OfferingCodeMaximumLimitTO getOfferingCodeMaximumLimitTO()
    {
        return offeringCodeMaximumLimitTO;
    }

    public void setOfferingCodeMaximumLimitTO(OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO)
    {
        this.offeringCodeMaximumLimitTO = offeringCodeMaximumLimitTO;
    }

    public OfferingCodeTO getOfferingCodeTO()
    {
        return offeringCodeTO;
    }

    public void setOfferingCodeTO(OfferingCodeTO offeringCodeTO)
    {
        this.offeringCodeTO = offeringCodeTO;
    }

}
