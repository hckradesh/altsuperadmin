/**
 * 
 */
package com.talentpact.ui.sysContentCategory.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.SysContentCategoryTO;
import com.talentpact.business.common.constants.ISysContentCategoryConstants;
import com.talentpact.business.dataservice.SysContentCategory.SysContentCategoryService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysContentCategory.bean.SysContentCategoryBean;

/**
 * 
 * @author vivek.goyal
 * 
 */


@Named("sysContentCategoryController")
@ConversationScoped
public class SysContentCategoryController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysContentCategoryController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysContentCategoryBean sysContentCategoryBean;

    @Inject
    SysContentCategoryService sysContentCategoryService;

    @Override
    public void initialize()
    {
        List<SysContentCategoryTO> sysContentCategoryTOList = null;
        Set<String> name = null;
        SysContentCategoryTO sysContentCategoryTO = null;

        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            resetSysContentCategory();
            sysContentCategoryTOList = sysContentCategoryService.getSysContentCategoryList();
            sysContentCategoryBean.setSysContentCategoryTOList(sysContentCategoryTOList);
            name = new HashSet<String>();
            for (SysContentCategoryTO list : sysContentCategoryTOList) {
                sysContentCategoryTO = new SysContentCategoryTO();
                sysContentCategoryTO = list;
                name.add(sysContentCategoryTO.getCategoryName());
            }

            sysContentCategoryBean.setSysCategoryName(name);
            menuBean.setPage("../SysContentCategory/sysContentCategory.xhtml");
            sysContentCategoryBean.setRenderSysContentCategoryPopup(false);
            sysContentCategoryBean.setRenderErrorMessage(false);
            requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_DATATABLE_RESET);
            requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysContentCategory()
    {
        // sysContentCategoryBean.setSysContentCategoryTOList(null);
        sysContentCategoryBean.setRenderSysContentCategoryPopup(true);
        sysContentCategoryBean.setCategoryName(null);
        sysContentCategoryBean.setSysCategoryName(null);
        sysContentCategoryBean.setSysContentCategoryTOList(null);
        sysContentCategoryBean.setSelectedCategoryName(null);
    }

    public void addSysContentCategory()
    {
        SysContentCategoryTO sysContentCategoryTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysContentCategoryTO = new SysContentCategoryTO();
            sysContentCategoryTO.setCategoryName(sysContentCategoryBean.getCategoryName());
            sysContentCategoryBean.setRenderErrorMessage(true);

            if (validateNewSysContentCategory(sysContentCategoryTO.getCategoryName())) {
                sysContentCategoryService.addSysContentCategory(sysContentCategoryTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Category saved successfully", ""));
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_ADD_DIALOG_HIDE);
                requestContext.update("sysContentCategoryListForm");
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_DATATABLE_RESET);
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_PAGINATION_DATATABLE_RESET);
            } else {

                RequestContext.getCurrentInstance().update("addSysContentCategoryForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysContentCategoryConstants.SYSCONTENTCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysContentCategoryBean.setRenderSysContentCategoryPopup(false);
            requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_ADD_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("addSysContentCategoryForm");
        }
    }

    // To validate if the newly added SysContentCategory is already present in
    // the system
    private boolean validateNewSysContentCategory(String categoryName)
    {
        categoryName = categoryName.trim();
        List<SysContentCategoryTO> sysContentCategoryTOList = null;
        int j = 0;
        try {
            if (categoryName.equalsIgnoreCase("") || categoryName.equalsIgnoreCase(null)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysContentCategory", ""));
                j = 1;
            } else {
                sysContentCategoryTOList = sysContentCategoryService.getSysContentCategoryList();
                for (int i = 0; i < sysContentCategoryTOList.size(); i++) {
                    if (sysContentCategoryTOList.get(i).getCategoryName().equalsIgnoreCase(categoryName)) {
                        j = 1;
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + categoryName + "' is already present in the existing SysContentCategory list.", ""));
                        sysContentCategoryBean.setRenderErrorMessage(true);
                        break;
                    }
                }
            }
            if (j != 1) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void refreshPopUp(SysContentCategoryTO sysContentCategoryTO)
    {
        try {
            sysContentCategoryBean.setCategoryID(sysContentCategoryTO.getContentCategoryID());
            sysContentCategoryBean.setCategoryName(sysContentCategoryTO.getCategoryName());
            sysContentCategoryBean.setRenderEditSysContentCategoryPopup(true);
            sysContentCategoryBean.setDuplicateCategoryName(sysContentCategoryTO.getCategoryName());
        } catch (Exception e) {
        }
    }

    public void updateSysContentCategory()
    {
        SysContentCategoryTO sysContentCategoryTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysContentCategoryTO = new SysContentCategoryTO();
            sysContentCategoryTO.setContentCategoryID(sysContentCategoryBean.getCategoryID());
            sysContentCategoryTO.setCategoryName(sysContentCategoryBean.getCategoryName());

            if ((sysContentCategoryBean.getDuplicateCategoryName()).trim().equalsIgnoreCase(sysContentCategoryTO.getCategoryName())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes were made.", ""));
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_EDIT_DIALOG_HIDE);
            } else if (validateNewSysContentCategory(sysContentCategoryTO.getCategoryName())) {
                sysContentCategoryService.editSysContentCategory(sysContentCategoryTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Category updated successfully", ""));
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_DATATABLE_RESET);
                requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_PAGINATION_DATATABLE_RESET);


            } else {
                RequestContext.getCurrentInstance().update("sysContentCategoryEditForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysContentCategoryConstants.SYSCONTENTCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysContentCategoryBean.setRenderSysContentCategoryPopup(false);
            requestContext.execute(ISysContentCategoryConstants.SYSCONTENTCATEGORY_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("sysContentCategoryEditForm");
        }
    }
}