package com.talentpact.ui.sysProductAnnouncement.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.constants.CoreConstants;
import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysOfferingCategoryConstants;
import com.talentpact.business.common.constants.ISysProductAnnouncementConstants;
import com.talentpact.business.dataservice.sysProductAnnouncement.SysProductAnnouncementService;
import com.talentpact.ui.altbenefits.constant.IBenefitsConstant;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysProductAnnouncement.bean.SysProductAnnouncementBean;
import com.talentpact.ui.sysProductAnnouncement.to.SysProductAnnouncementTO;
import com.talentpact.ui.tenants.organization.bean.NewOrganizationBean;

/**
 * 
 * @author vivek.goyal
 *
 */

@Named("sysProductAnnouncementController")
@ConversationScoped
public class SysProductAnnouncementController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysProductAnnouncementController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    UserSessionBean userSessionBean;

    @Inject
    SysProductAnnouncementService sysProductAnnouncementService;

    @Inject
    SysProductAnnouncementBean sysProductAnnouncementBean;

    @Inject
    NewOrganizationBean newOrganizationBean;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        ServiceResponse response = null;
        try {
            sysProductAnnouncementBean.setSysProductAnnouncementTOList(sysProductAnnouncementService.getSysProductAnnouncement());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../SysProductAnnouncement/sysProductAnnouncement.xhtml");
            requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_DATATABLE_RESET);
            requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void resetSysProductAnnouncement()
    {
        sysProductAnnouncementBean.setSysProductAnnouncementID(0);
        sysProductAnnouncementBean.setSysProductAnnouncement(null);
        sysProductAnnouncementBean.setSysProductAnnouncementLabel(null);
        sysProductAnnouncementBean.setValidFrom(null);
        sysProductAnnouncementBean.setValidTo(null);
        sysProductAnnouncementBean.setActive(false);
        sysProductAnnouncementBean.setStatus(null);
        sysProductAnnouncementBean.setOrganizationTOList(null);
        sysProductAnnouncementBean.setSelectedOrganizations(null);
        sysProductAnnouncementBean.setSysProductAnnouncementTO(null);
        sysProductAnnouncementBean.setAnnouncementBanner(null);
        sysProductAnnouncementBean.setAnnouncementBannerFile(null);
        sysProductAnnouncementBean.setBannerUrl(null);
        getAllOrganizationList();
    }

    public void saveSysProductAnnouncement()
        throws Exception
    {
        SysProductAnnouncementTO sysProductAnnouncementTO = null;
        RequestContext requestContext = null;
        String file_location = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysProductAnnouncementTO = new SysProductAnnouncementTO();
            sysProductAnnouncementTO.setSysProductAnnouncement(sysProductAnnouncementBean.getSysProductAnnouncement());
            sysProductAnnouncementTO.setSysProductAnnouncementLabel(sysProductAnnouncementBean.getSysProductAnnouncementLabel());
            sysProductAnnouncementTO.setValidFrom(sysProductAnnouncementBean.getValidFrom());
            sysProductAnnouncementTO.setValidTo(sysProductAnnouncementBean.getValidTo());
            
            sysProductAnnouncementTO.setActive(sysProductAnnouncementBean.getActive());
            sysProductAnnouncementTO.setSelectedOrganizations(sysProductAnnouncementBean.getSelectedOrganizations());
            sysProductAnnouncementTO.setOrganizationTOMap(sysProductAnnouncementBean.getOrganizationTOMap());
            sysProductAnnouncementTO.setBannerUrl(sysProductAnnouncementBean.getBannerUrl());
            file_location = uploadMasterData();
            sysProductAnnouncementTO.setAnnouncementBanner(file_location);
            if (validateSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncement(), false, sysProductAnnouncementTO.getActive())) {
                sysProductAnnouncementService.saveSysProductAnnouncement(sysProductAnnouncementTO, sysProductAnnouncementBean.getOrganizationTOMap());
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysProductAnnouncement added successfully", ""));
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_ADD_DIALOG_HIDE);
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_DATATABLE_RESET);
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysProductAnnouncementListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, sysProductAnnouncementTO.getSysProductAnnouncement() + " is already present in the system.", ""));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            throw e;
        }
    }

    public void editSysProductAnnouncement(SysProductAnnouncementTO sysProductAnnouncementTO)
    {
        try {
            sysProductAnnouncementBean.setSysProductAnnouncementTO(sysProductAnnouncementTO);
            getOrganizationList(sysProductAnnouncementTO.getSysProductAnnouncementID());
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
    }

    public void updateSysProductAnnouncement()
    {
        SysProductAnnouncementTO sysProductAnnouncementTO = null;
        RequestContext requestContext = null;
        boolean contains = false;
        List<Integer> results = new ArrayList<Integer>();
        String file_location = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysProductAnnouncementTO = new SysProductAnnouncementTO();
            sysProductAnnouncementTO.setSysProductAnnouncementID(sysProductAnnouncementBean.getSysProductAnnouncementTO().getSysProductAnnouncementID());
            sysProductAnnouncementTO.setSysProductAnnouncement(sysProductAnnouncementBean.getSysProductAnnouncementTO().getSysProductAnnouncement());
            sysProductAnnouncementTO.setSysProductAnnouncementLabel(sysProductAnnouncementBean.getSysProductAnnouncementTO().getSysProductAnnouncementLabel());
            sysProductAnnouncementTO.setValidFrom(sysProductAnnouncementBean.getSysProductAnnouncementTO().getValidFrom());
            sysProductAnnouncementTO.setValidTo(sysProductAnnouncementBean.getSysProductAnnouncementTO().getValidTo());
            if (sysProductAnnouncementBean.getSysProductAnnouncementTO().getValidTo().compareTo(sysProductAnnouncementBean.getSysProductAnnouncementTO().getValidFrom()) < 0) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, " Valid To date Should be greater than valid from date .", ""));
                return;
            }
            sysProductAnnouncementTO.setActive(sysProductAnnouncementBean.getSysProductAnnouncementTO().getActive());
            sysProductAnnouncementTO.setOrganizationTOMap(sysProductAnnouncementBean.getOrganizationTOMap());
            sysProductAnnouncementTO.setSelectedOrganizations(sysProductAnnouncementBean.getSelectedOrganizations());
            sysProductAnnouncementTO.setBannerUrl(sysProductAnnouncementBean.getSysProductAnnouncementTO().getBannerUrl());
            int[] duplicateSelectedOrganizations = new int[] { sysProductAnnouncementBean.getDuplicateSelectedOrganizations().length };
            duplicateSelectedOrganizations = sysProductAnnouncementBean.getDuplicateSelectedOrganizations();

            int[] selectedOrganizations = new int[] { sysProductAnnouncementBean.getSelectedOrganizations().length };
            selectedOrganizations = sysProductAnnouncementBean.getSelectedOrganizations();

            for (int i = 0; i < duplicateSelectedOrganizations.length; i++) {
                for (int j = 0; j < selectedOrganizations.length; j++) {
                    if (duplicateSelectedOrganizations[i] == selectedOrganizations[j]) {
                        contains = true;
                        break;
                    }
                }
                if (!contains) {
                    results.add(duplicateSelectedOrganizations[i]);
                } else {
                    contains = false;
                }
            }
            int[] ret = new int[results.size()];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = results.get(i);
            }
            sysProductAnnouncementTO.setDeletedOrganizations(ret);

            file_location = uploadMasterData();
            if (file_location != null)
                sysProductAnnouncementTO.setAnnouncementBanner(file_location);
            if (validateSysProductAnnouncement(sysProductAnnouncementTO.getSysProductAnnouncement(), true, sysProductAnnouncementTO.getActive())) {
                sysProductAnnouncementService.updateSysProductAnnouncement(sysProductAnnouncementTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysProductAnnouncement updated successfully", ""));
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_DATATABLE_RESET);
                requestContext.execute(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_PAGINATION_DATATABLE_RESET);
                requestContext.update("sysProductAnnouncementListForm");
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, sysProductAnnouncementTO.getSysProductAnnouncement() + " is already present in the system.", ""));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
        }
    }

    private boolean validateSysProductAnnouncement(String sysProductAnnouncement, Boolean isEdited, Boolean status)
    {
        Integer sysProductAnnouncementID = null;
        int j = 0;
        try {
            if (sysProductAnnouncementBean.getSysProductAnnouncementTO() != null) {
                sysProductAnnouncementID = sysProductAnnouncementBean.getSysProductAnnouncementTO().getSysProductAnnouncementID();
            }
            for (SysProductAnnouncementTO sysProductAnnouncementTO : sysProductAnnouncementBean.getSysProductAnnouncementTOList()) {
                if (isEdited) {
                    if (!sysProductAnnouncementTO.getSysProductAnnouncementID().equals(sysProductAnnouncementID)
                            && sysProductAnnouncementTO.getSysProductAnnouncement().trim().equalsIgnoreCase(sysProductAnnouncement.trim()) && sysProductAnnouncementTO.getActive()
                            && status) {
                        j = 1;
                        break;
                    }
                } else {
                    if (sysProductAnnouncementTO.getSysProductAnnouncement().trim().equalsIgnoreCase(sysProductAnnouncement.trim()) && sysProductAnnouncementTO.getActive()
                            && status) {
                        j = 1;
                        break;
                    }
                }
            }
            if (j == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            LOGGER_.error("", e);
            return false;
        }

    }

    //List of all the Organizations in the system
    public void getAllOrganizationList()
    {
        try {
            sysProductAnnouncementBean.setOrganizationTOMap(sysProductAnnouncementService.getAllOrganizationList());
        } catch (Exception e) {
            LOGGER_.error(e.getMessage());
        }
    }

    //List of Organizations where the corresponding SysProductAnnouncement is active
    public void getOrganizationList(Integer sysProductAnnouncemetID)
        throws Exception
    {
        int[] selectedOrgs;
        try {
            sysProductAnnouncementBean.setOrganizationTOMap(sysProductAnnouncementService.getEditOrganizationList(sysProductAnnouncemetID));
            selectedOrgs = findSelectedOrganization(sysProductAnnouncementBean.getOrganizationTOMap());
            sysProductAnnouncementBean.setSelectedOrganizations(selectedOrgs);
            sysProductAnnouncementBean.setDuplicateSelectedOrganizations(selectedOrgs);
        } catch (Exception e) {
            LOGGER_.error(e.getMessage());
            throw e;
        }
    }

    private int[] findSelectedOrganization(Map<Integer, SysProductAnnouncementTO> organizationTOMap)
    {
        List<Integer> selectedList = new ArrayList<Integer>();

        for (Entry<Integer, SysProductAnnouncementTO> mapVal : organizationTOMap.entrySet()) {
            SysProductAnnouncementTO sysProductAnnouncementTO = mapVal.getValue();
            if (sysProductAnnouncementTO.getOrganizationAnnouncementActive()) {
                selectedList.add(sysProductAnnouncementTO.getOrganizationID());
            }
        }
        int[] ret = new int[selectedList.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = selectedList.get(i);
        }
        return ret;
    }


    /**
     * 
     * @param event
     */
    public void handleAnnouncementBannerUpload(FileUploadEvent event)
    {
        UploadedFile uploadedFile = null;
        try {
            uploadedFile = event.getFile();
            if (uploadedFile != null && uploadedFile.getContentType().startsWith("image/")) {
                sysProductAnnouncementBean.setAnnouncementBannerFile(uploadedFile);
                sysProductAnnouncementBean.setUploadedAnnouncementBannerFileName(uploadedFile.getFileName());

            } else {
                sysProductAnnouncementBean.setAnnouncementBannerFile(uploadedFile);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, IBenefitsConstant.UPLOAD_INVALID_FILE_MSG, ""));
            }
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }


    /**
     * 
     * @param s
     * @return
     */
    private String getPath(String s)
        throws Exception
    {
        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String getPath = null;
        String finalPath = null;
        File dir = null;
        String serverPath = null;
        String fileLocation = null;
        boolean success = false;
        String returnPath = null;
        try {
            resourceBundle = ResourceBundle.getBundle(CoreConstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_BANNER_PATH);
            serverPath = resourceBundle.getString(ISysProductAnnouncementConstants.SYSPRODUCTANNOUNCEMENT_BANNER_SERVER_PATH);
            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath + File.separator;
                }
            } else {
                throw new Exception(CoreConstants.INVALID_PATH_STRING);
            }
            fileLocation = sysProductAnnouncementBean.getSysProductAnnouncement() == null ? sysProductAnnouncementBean.getSysProductAnnouncementTO().getSysProductAnnouncement()
                    : sysProductAnnouncementBean.getSysProductAnnouncement();
            finalPath = initialPath + fileLocation;
            dir = new File(finalPath);
            if (!dir.exists()) {
                success = dir.mkdirs();
            } else {
                success = true;/////path already exists.
            }
            if (success) {
                LOGGER_.info("-----Directory created Successfully...!!------------: - " + dir);
                getPath = finalPath + File.separator + s;
                returnPath = getPath + CoreConstants.AT_SPERATOR + serverPath + CoreConstants.AT_SPERATOR + fileLocation + File.separator + s;
            } else {
                returnPath = null;
                LOGGER_.error("-----Unable to make directory...!!------------");
            }
        } catch (Exception ex) {
            throw ex;
        }
        return returnPath;
    }

    /**
     * 
     * @throws Exception
     */
    public String uploadMasterData()
        throws Exception
    {
        UploadedFile uploadedFile = null;
        String path = null;
        String fileName = null;
        InputStream in = null;
        String serverPath = null;
        try {
            uploadedFile = sysProductAnnouncementBean.getAnnouncementBannerFile();
            if (uploadedFile != null) {
                fileName = uploadedFile.getFileName();
                in = uploadedFile.getInputstream();
                path = getPath(fileName);
                if (path != null) {
                    OutputStream out = new FileOutputStream(new File(path.split(CoreConstants.AT_SPERATOR)[0]));
                    int read = 0;
                    byte[] bytes = new byte[1024];
                    while ((read = in.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    in.close();
                    out.close();
                    out.flush();
                    serverPath = path.split(CoreConstants.AT_SPERATOR)[1] + path.split(CoreConstants.AT_SPERATOR)[2];
                }
            }
        } catch (IOException e) {
            LOGGER_.error("", e);
            throw e;
        }

        return serverPath;
    }

}