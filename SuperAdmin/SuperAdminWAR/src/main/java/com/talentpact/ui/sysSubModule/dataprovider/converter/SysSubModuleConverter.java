package com.talentpact.ui.sysSubModule.dataprovider.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alt.common.helper.SQLDateHelper;
import com.talentpact.model.SysSubModule;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

/**
 * @author vaibhav.kashyap
 * 
 */
public class SysSubModuleConverter {
    
    private static final Logger LOGGER = Logger.getLogger(SysSubModuleConverter.class.toString());

	public static SysSubModuleResponseTO getparentModuleAndSubModuleDetails(
			List<Object[]> resultArrayObjectList) throws Exception {
		SysSubModuleResponseTO sysSubModuleResponseTO = null;
		SysSubModuleResponseTO sysSubModuleResponseTOFinal = null;
		Map<Integer, List<SysSubModuleResponseTO>> parentModuleIDByChild = null;
		Map<Integer, String> parentModuleIDByName = null;
		Map<Integer, String> allModulesIDByName = null;
		List<SysSubModuleResponseTO> sysSubModulesResponseTOList = null;
		try {
			String errMsg = null;
			if (resultArrayObjectList == null
					|| resultArrayObjectList.isEmpty()) {
				errMsg = "Data not found in SysSubModule";
				throw new Exception(errMsg);
			}

			parentModuleIDByChild = new HashMap<Integer, List<SysSubModuleResponseTO>>();
			parentModuleIDByName = new HashMap<Integer, String>();
			allModulesIDByName = new HashMap<Integer, String>();

			Integer moduleId = null;
			String moduleName = null;
			Integer appId = null;
			Integer parentModuleId = null;
			Integer parentAppId = null;
			String parentModuleName = null;
			for (Object[] obj : resultArrayObjectList) {
				moduleId = obj[0] != null ? (Integer) obj[0] : null;
				moduleName = obj[1] != null ? (String) obj[1] : null;
				appId = obj[2] != null ? (Integer) obj[2] : null;

				parentModuleId = obj[3] != null ? (Integer) obj[3] : null;
				parentModuleName = obj[4] != null ? (String) obj[4] : null;
				parentAppId = obj[5] != null ? (Integer) obj[5] : null;

				if (moduleId == null || moduleName == null
						|| moduleName.trim().length() == 0 || appId == null) {
					errMsg = "Data not found for moduleId:" + moduleId
							+ ", moduleName:" + moduleName + ", appId:" + appId
							+ " in SysSubModule";
					throw new Exception(errMsg);
				}

				/**
				 * preparing list of all module id mapped by name
				 * */
				allModulesIDByName.put(new Integer(moduleId), new String(
						moduleName));

				if (parentModuleId == null) {
					if (!parentModuleIDByName
							.containsKey(new Integer(moduleId))) {
						parentModuleIDByName.put(new Integer(moduleId),
								new String(moduleName));

						sysSubModulesResponseTOList = new ArrayList<SysSubModuleResponseTO>();
						parentModuleIDByChild.put(new Integer(moduleId),
								sysSubModulesResponseTOList);
					}
				} else {
					if (parentModuleIDByChild.containsKey(new Integer(
							parentModuleId))) {
						SysSubModuleResponseTO sysSubModuleResponseTOTemp = new SysSubModuleResponseTO();
						sysSubModuleResponseTOTemp.setModuleId(new Integer(
								moduleId));
						sysSubModuleResponseTOTemp.setModuleName(new String(
								moduleName));
						sysSubModuleResponseTOTemp.setAppId(new Integer(appId));
						sysSubModuleResponseTOTemp
								.setParentModuleId(new Integer(parentModuleId));
						sysSubModuleResponseTOTemp
								.setParentModuleName(new String(
										parentModuleName));
						List<SysSubModuleResponseTO> sysSubModulesResponseTOListTemp = parentModuleIDByChild
								.get(new Integer(parentModuleId));
						sysSubModulesResponseTOListTemp
								.add(sysSubModuleResponseTOTemp);
					}
				}
			}

			if (parentModuleIDByName != null && !parentModuleIDByName.isEmpty()) {
				sysSubModuleResponseTOFinal = new SysSubModuleResponseTO();
				sysSubModuleResponseTOFinal
						.setParentModuleIDByName(parentModuleIDByName);
			}

			if (parentModuleIDByChild != null
					&& !parentModuleIDByChild.isEmpty()) {
				if (sysSubModuleResponseTOFinal != null) {
					sysSubModuleResponseTOFinal
							.setParentModuleIDByChild(parentModuleIDByChild);
				}
			}

			if (allModulesIDByName != null && !allModulesIDByName.isEmpty()) {
				if (sysSubModuleResponseTOFinal != null) {
					sysSubModuleResponseTOFinal
							.setAllModuleIDByName(allModulesIDByName);
				}
			}

		} catch (Exception ex) {
			sysSubModuleResponseTOFinal = null;
			throw ex;
		}
		return sysSubModuleResponseTOFinal;
	}
	
	/**
     * 
     * @param sysSubModuleList
     * @return
     * @author raghvendra.mishra
     */
    public List<SysSubModuleResponseTO> convertSysSubModuleToDataTO(List<SysSubModule> sysSubModuleList){
        List<SysSubModuleResponseTO> resultList = null;
        SysSubModuleResponseTO sysSubModuleResponseTO = null;
        try {
            resultList = new ArrayList<SysSubModuleResponseTO>();
            for (SysSubModule sysSubModule : sysSubModuleList) {
                sysSubModuleResponseTO = new SysSubModuleResponseTO();
                sysSubModuleResponseTO.setModuleId(sysSubModule.getModuleId());
                sysSubModuleResponseTO.setModuleName(sysSubModule.getModuleName());
                sysSubModuleResponseTO.setModuleDesc(sysSubModule.getModuleDesc());
                sysSubModuleResponseTO.setParentModuleId(sysSubModule.getParentModuleId());
                sysSubModuleResponseTO.setAppId(sysSubModule.getAppId());
                sysSubModuleResponseTO.setTenantId(sysSubModule.getTenantId());
                sysSubModuleResponseTO.setCreatedBy(sysSubModule.getCreatedBy());
                sysSubModuleResponseTO.setCreatedDate(SQLDateHelper.getSqlTimeStampFromDate(sysSubModule.getCreatedDate()));
                sysSubModuleResponseTO.setModifiedBy(sysSubModule.getModifiedBy());
                sysSubModuleResponseTO.setModifiedDate(SQLDateHelper.getSqlTimeStampFromDate(sysSubModule.getModifiedDate()));   
                if(sysSubModule.getParentModule() != null){
                sysSubModuleResponseTO.setParentModuleName(sysSubModule.getParentModule().getModuleName());
                }else{
                	sysSubModuleResponseTO.setParentModuleName(null);
                }
                sysSubModuleResponseTO.setTenantId(sysSubModule.getTenantId());
                resultList.add(sysSubModuleResponseTO);
            }
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return resultList;
    }
}
