/**
 * 
 */
package com.talentpact.ui.tpApp.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.constants.Iconstants;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.tpApp.controller.TpAppController;

/**
 * @author pankaj.sharma1
 *
 */
@Named("tpAppBean")
@ConversationScoped
public class TpAppBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 1998978L;

    @Inject
    TpAppController tpAppController;

    @Inject
    TpAppBean tpAppBean;

    private Long appID;

    private String name;

    private String code;

    private String image;

    List<TpAppResponseTO> tpAppTOList;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            tpAppController.initialize();
        } catch (Exception e) {
        }
    }

    public Long getAppID()
    {
        return appID;
    }

    public void setAppID(Long appID)
    {
        this.appID = appID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public List<TpAppResponseTO> getTpAppTOList()
    {
        return tpAppTOList;
    }

    public void setTpAppTOList(List<TpAppResponseTO> tpAppTOList)
    {
        this.tpAppTOList = tpAppTOList;
    }

    //    public void handleFileUploadForAppImage(FileUploadEvent event)
    //    {
    //
    //        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
    //        FacesContext.getCurrentInstance().addMessage(null, message);
    //        String destinationPath = null;
    //
    //        try {
    //            destinationPath = getDestinationPath();
    //            tpAppBean.setImage(destinationPath + event.getFile().getFileName());
    //            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
    //        } catch (Exception ex) {
    //            ex.printStackTrace();
    //        }
    //    }


    public void handleFileUploadForAppImage(FileUploadEvent event)
    {
        FacesMessage message = new FacesMessage("successful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;
        String[] destinationPathArr = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPath();
            destinationPath = destinationPathArr[1];
            tpAppBean.setImage(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private String[] getDestinationPath()
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;
        String[] path = null;
        String destinationPathforDB = null;
        String initialPathPathforDB = null;
        String finalPathforDB = null;

        try {
            path = new String[2];
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_APPIMAGE);

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length())) {
                    initialPath = initialPath + File.separator;
                    initialPathPathforDB = File.separator;
                }
            } else {
                throw new Exception("Invalid path");
            }

            //  finalPath = initialPath + dateFormat.format(date);
            finalPath = initialPath + "appImage";
            finalPathforDB = initialPathPathforDB + "appImage";
            dir = new File(finalPath);
            dir.mkdir();
            destinationPath = finalPath + File.separator;
            destinationPathforDB = finalPathforDB + File.separator;
            path[0] = destinationPath;
            path[1] = destinationPathforDB;


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return path;
    }

    private void copyFile(String fileName, InputStream in)
    {
        String[] destinationPathArr = null;
        String destinationPath = null;
        try {
            destinationPathArr = new String[2];
            destinationPathArr = getDestinationPath();
            destinationPath = destinationPathArr[0];
            OutputStream out = new FileOutputStream(new File(destinationPath + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
