/**
 * 
 */
package com.talentpact.ui.sysPayCodeParameterMapping.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.alt.hrPayrollParameterPolicy.transport.output.HrPayrollParameterPolicyResponseTO;
import com.talentpact.business.application.transport.input.SysConstantRequestTO;
import com.talentpact.business.application.transport.input.SysPayCodeParameterMappingRequestTO;
import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.business.application.transport.output.SysPayCodeParameterMappingResponseTO;
import com.talentpact.business.dataservice.sysPayCodeParameterMapping.SysPayCodeParameterMappingService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysPayCodeParameterMapping.bean.SysPayCodeParameterMappingBean;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysPayCodeParameterMappingController")
@ConversationScoped
public class SysPayCodeParameterMappingController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    @Inject
    MenuBean menuBean;

    @Inject
    SysPayCodeParameterMappingService sysPayCodeParameterMappingService;

    @Inject
    SysPayCodeParameterMappingBean sysPayCodeParameterMappingBean;

    @Override
    public void initialize()
    {
        List<SysPayCodeParameterMappingResponseTO> sysPayCodeParameterMappingTOList = null;
        List<SysPayCodeParameterMappingResponseTO> parameterDropDownTOList = null;
        List<SysPayCodeParameterMappingResponseTO> payCodeDropDownTOList = null;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {

            sysPayCodeParameterMappingTOList = getSysPayCodeParameterMappingList();
            parameterDropDownTOList = getSysParameterDropDownList();
            payCodeDropDownTOList = getSysPayCodeDropDownList();
            sysPayCodeParameterMappingBean.setSysPayCodeParameterMappingTOList(sysPayCodeParameterMappingTOList);
            sysPayCodeParameterMappingBean.setParameterDropDownTOList(parameterDropDownTOList);
            sysPayCodeParameterMappingBean.setPayCodeDropDownTOList(payCodeDropDownTOList);
            menuBean.setPage("../sysPayCodeParameterMapping/sysPayCodeParameterMapping.xhtml");

            requestContext.execute("PF('myDataTable').clearFilters();");
            requestContext.execute("PF('myDataTable').getPaginator().setPage(0);");
        } catch (Exception e) {
        } finally {

        }

    }

    public void refreshPopUp(SysPayCodeParameterMappingResponseTO constantTO)
    {
        try {
            sysPayCodeParameterMappingBean.setSysParameterID(constantTO.getSysParameterID());
            sysPayCodeParameterMappingBean.setSysParameterName(constantTO.getSysParameterName());
            sysPayCodeParameterMappingBean.setSysPayCodeID(constantTO.getSysPayCodeID());
            sysPayCodeParameterMappingBean.setSysPayCodeName(constantTO.getSysPayCodeName());
            sysPayCodeParameterMappingBean.setSysPayCodeParameterMappingID(constantTO.getSysPayCodeParameterMappingID());
            sysPayCodeParameterMappingBean.setSysPayCodeParameterMappingName(constantTO.getSysParameterName() + "-" + constantTO.getSysPayCodeName());

        } catch (Exception e) {
        }
    }


    public void resetBean()
    {
        try {
            sysPayCodeParameterMappingBean.setSysParameterID(null);
            sysPayCodeParameterMappingBean.setSysParameterName(null);
            sysPayCodeParameterMappingBean.setSysPayCodeID(null);
            sysPayCodeParameterMappingBean.setSysPayCodeName(null);
            sysPayCodeParameterMappingBean.setSysPayCodeParameterMappingID(null);
            sysPayCodeParameterMappingBean.setSysPayCodeParameterMappingName(null);
        } catch (Exception e) {

        }

    }

    private List<SysPayCodeParameterMappingResponseTO> getSysPayCodeParameterMappingList()
    {
        List<SysPayCodeParameterMappingResponseTO> response = null;
        try {
            response = sysPayCodeParameterMappingService.getSysPayCodeParameterMappingList();
        } catch (Exception e) {
        }
        return response;

    }

    private List<SysPayCodeParameterMappingResponseTO> getSysParameterDropDownList()
    {
        List<SysPayCodeParameterMappingResponseTO> response = null;
        try {
            response = sysPayCodeParameterMappingService.getSysParameterDropDownList();
        } catch (Exception e) {
        }
        return response;

    }

    private List<SysPayCodeParameterMappingResponseTO> getSysPayCodeDropDownList()
    {
        List<SysPayCodeParameterMappingResponseTO> response = null;
        try {
            response = sysPayCodeParameterMappingService.getSysPayCodeDropDownList();
        } catch (Exception e) {
        }
        return response;

    }

    public void save()
    {

        SysPayCodeParameterMappingRequestTO sysPayCodeParameterMappingRequestTO = null;
        RequestContext context = null;
        Integer sysParameterID = null;
        Integer sysPayCodeID = null;
        try {
            context = RequestContext.getCurrentInstance();
            sysParameterID = sysPayCodeParameterMappingBean.getSysParameterID();
            sysPayCodeID = sysPayCodeParameterMappingBean.getSysPayCodeID();

            if (sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingTOList() != null && !sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingTOList().isEmpty()) {
                for (SysPayCodeParameterMappingResponseTO list : sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingTOList()) {
                    if (sysParameterID.equals(list.getSysParameterID()) && sysPayCodeID.equals(list.getSysPayCodeID())) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "This combination already exists", ""));
                        return;
                    }
                }
            }
            sysPayCodeParameterMappingRequestTO = new SysPayCodeParameterMappingRequestTO();
            sysPayCodeParameterMappingRequestTO.setSysParameterID(sysPayCodeParameterMappingBean.getSysParameterID());
            sysPayCodeParameterMappingRequestTO.setSysPayCodeID(sysPayCodeParameterMappingBean.getSysPayCodeID());
            sysPayCodeParameterMappingService.saveSysPayCodeParameterMappingList(sysPayCodeParameterMappingRequestTO);
            initialize();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved Successfully", ""));


            context.execute("PF('sysPayCodeParameterMappingAddModal').hide();");
            
            context.update("sysPayCodeParameterMappingListForm");

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error occured", ""));
            context.execute("PF('sysPayCodeParameterMappingAddModal').hide();");
        } finally {
          //  context.update("sysPayCodeParameterMappingListForm");
        }

    }

    public void update()
    {
        SysPayCodeParameterMappingRequestTO sysPayCodeParameterMappingRequestTO = null;
        RequestContext context = null;
        List<Integer> list = null;
        boolean isUpdate = true;
        Integer sysParameterID = null;
        Integer sysPayCodeID = null;
        Integer sysPayCodeParameterMapping = null;
        List<SysPayCodeParameterMappingResponseTO> newList = null;
        try {
            context = RequestContext.getCurrentInstance();
            newList = new ArrayList<SysPayCodeParameterMappingResponseTO>();
            sysPayCodeParameterMappingRequestTO = new SysPayCodeParameterMappingRequestTO();
            list = new ArrayList<Integer>();
            list = sysPayCodeParameterMappingService.getSysPayCodeParameterMappingIdsFromHrPayrollParameterPolicy();
            sysPayCodeParameterMapping = sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingID();
            if (list != null) {
                for (Integer l : list) {
                    if (l.equals(sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingID())) {
                        isUpdate = false;
                    }
                }
            }
            if (isUpdate == false) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Can not be updated as it shall impact the organization level data", ""));
                return;
            }

            sysParameterID = sysPayCodeParameterMappingBean.getSysParameterID();
            sysPayCodeID = sysPayCodeParameterMappingBean.getSysPayCodeID();
            for (SysPayCodeParameterMappingResponseTO value : sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingTOList()) {
                if (value.getSysPayCodeParameterMappingID().equals(sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingID())) {
                    //do nothing
                } else {
                    newList.add(value);
                }
            }

            for (SysPayCodeParameterMappingResponseTO li : newList) {
                if (sysParameterID.equals(li.getSysParameterID()) && sysPayCodeID.equals(li.getSysPayCodeID())) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "This combination already exists", ""));
                    return;
                }
            }

            sysPayCodeParameterMappingRequestTO.setSysPayCodeParameterMappingID(sysPayCodeParameterMappingBean.getSysPayCodeParameterMappingID());
            sysPayCodeParameterMappingRequestTO.setSysPayCodeID(sysPayCodeParameterMappingBean.getSysPayCodeID());
            sysPayCodeParameterMappingRequestTO.setSysParameterID(sysPayCodeParameterMappingBean.getSysParameterID());

            sysPayCodeParameterMappingService.saveSysPayCodeParameterMappingList(sysPayCodeParameterMappingRequestTO);
            context.execute("PF('myDataTable').clearFilters();");
            initialize();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysPayCode and  SysParameter Mapping Updated Succesfully", ""));
            context.execute("PF('sysPayCodeParameterMappingEditModal').hide();");
            context.update("sysPayCodeParameterMappingListForm");
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected error occured", ""));
            context.execute("PF('sysPayCodeParameterMappingEditModal').hide();");

        } finally {
            //context.update("sysPayCodeParameterMappingListForm");
        }
    }


}
