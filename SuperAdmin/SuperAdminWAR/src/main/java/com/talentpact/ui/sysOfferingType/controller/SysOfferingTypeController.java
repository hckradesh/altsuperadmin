/**
 * 
 */
package com.talentpact.ui.sysOfferingType.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.business.common.constants.ISysOfferingCategoryConstants;
import com.talentpact.business.common.constants.ISysOfferingTypeConstants;
import com.talentpact.business.dataservice.SysOfferingType.SysOfferingTypeService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.helper.SysOfferingCategoryComparator;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysOfferingType.bean.SysOfferingTypeBean;

/**
 * 
 * @author vivek.goyal
 *
 */

@Named("sysOfferingTypeController")
@ConversationScoped
public class SysOfferingTypeController extends CommonController implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysOfferingTypeController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysOfferingTypeService sysOfferingTypeService;

    @Inject
    SysOfferingTypeBean sysOfferingTypeBean;

    @SuppressWarnings("unused")
    @Override
    public void initialize()
    {
        List<SysOfferingTypeTO> sysOfferingTypeTOList = null;
        ServiceResponse response = null;
        Set<String> name = null;
        SysOfferingTypeTO sysOfferingTypeTO = null;
        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            resetSysOfferingType();
            sysOfferingTypeTOList = sysOfferingTypeService.getSysOfferingTypeList();
            sysOfferingTypeBean.setOfferingTypeTOList(sysOfferingTypeTOList);
            name = new HashSet<String>();
            for (SysOfferingTypeTO list : sysOfferingTypeTOList) {
                sysOfferingTypeTO = new SysOfferingTypeTO();
                sysOfferingTypeTO = list;
                name.add(sysOfferingTypeTO.getOfferingCategory());
            }
            sysOfferingTypeBean.setOfferingTypeList(name);
            sysOfferingTypeBean.setRenderSysOfferingCategoryPopup(false);
            sysOfferingTypeBean.setOfferingCategoryID((long) -1);
            sysOfferingTypeBean.setOfferingType(null);
            sysOfferingTypeBean.setRenderErrorMessage(false);
            menuBean.setPage("../SysOfferingType/sysOfferingType.xhtml");
            requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_DATATABLE_RESET);
            requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_PAGINATION_DATATABLE_RESET);
            RequestContext.getCurrentInstance().update("sysOfferingTypeListForm");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysOfferingType()
    {
        sysOfferingTypeBean.setRenderSysOfferingCategoryPopup(true);
        sysOfferingTypeBean.setOfferingCategoryID((long) -1);
        sysOfferingTypeBean.setOfferingType(null);
        sysOfferingTypeBean.setDescription(null);
        sysOfferingTypeBean.setOfferingTypeList(null);
        sysOfferingTypeBean.setOfferingTypeTOList(null);
        sysOfferingTypeBean.setSelectedOfferingTypeList(null);
        getNewSysOfferingCategory();
    }

    public void getNewSysOfferingCategory()
    {
        List<SysOfferingTypeTO> sysOfferingCategoryList = null;
        try {
            sysOfferingCategoryList = sysOfferingTypeService.getNewSysOfferingCategoryList();
            sysOfferingTypeBean.setNewOfferingCategoryTOList(sysOfferingCategoryList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSysOfferingType()
    {
        SysOfferingTypeTO sysOfferingTypeTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysOfferingTypeTO = new SysOfferingTypeTO();
            sysOfferingTypeBean.setRenderErrorMessage(true);
            sysOfferingTypeTO.setNewOfferingCategoryTOList(sysOfferingTypeBean.getNewOfferingCategoryTOList());
            sysOfferingTypeTO.setOfferingCategoryID(sysOfferingTypeBean.getOfferingCategoryID());
            sysOfferingTypeTO.setOfferingCategory(sysOfferingTypeBean.getOfferingCategory());
            sysOfferingTypeTO.setOfferingType(sysOfferingTypeBean.getOfferingType());
            sysOfferingTypeTO.setDescription(sysOfferingTypeBean.getDescription());

            if (validateSysOfferingType(sysOfferingTypeTO)) {
                sysOfferingTypeService.addSysOfferingType(sysOfferingTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysOfferingType saved successfully", ""));
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_ADD_DIALOG_HIDE);
                requestContext.update("sysOfferingTypeListForm");
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_DATATABLE_RESET);
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_PAGINATION_DATATABLE_RESET);
            } else {
                RequestContext.getCurrentInstance().update("addSysOfferingTypeForm");
            }
        } catch (Exception e) {
            e.printStackTrace();
            sysOfferingTypeBean.setRenderSysOfferingCategoryPopup(false);
            RequestContext.getCurrentInstance().update("addSysOfferingTypeForm");
        }
    }

    //To validate if the newly added SysOfferingType is already present in the system
    public boolean validateSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
    {
        List<SysOfferingTypeTO> sysOfferingTypeTOList = null;
        int j = 0;
        try {
            if (sysOfferingTypeTO.getOfferingType().trim().equalsIgnoreCase("") || sysOfferingTypeTO.getOfferingType().trim().equalsIgnoreCase(null)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid OfferingType", ""));
                j = 1;
            } else if (sysOfferingTypeTO.getOfferingCategoryID().equals(sysOfferingTypeBean.getDuplicateOfferingCategoryID())
                    && sysOfferingTypeTO.getOfferingType().trim().equalsIgnoreCase(sysOfferingTypeBean.getDuplicateOfferingType().trim())
                    && !sysOfferingTypeTO.getDescription().trim().equalsIgnoreCase(sysOfferingTypeBean.getDuplicateDescription().trim())) {
            }

            else {
                sysOfferingTypeTOList = sysOfferingTypeService.getSysOfferingTypeList();
                for (int i = 0; i < sysOfferingTypeTOList.size(); i++) {
                    if(sysOfferingTypeTO.getOfferingTypeID()!=sysOfferingTypeTOList.get(i).getOfferingTypeID()){
                        if (sysOfferingTypeTOList.get(i).getOfferingCategoryID() == sysOfferingTypeTO.getOfferingCategoryID()
                                && sysOfferingTypeTOList.get(i).getOfferingType().trim().equalsIgnoreCase(sysOfferingTypeTO.getOfferingType().trim())) {
                            j = 1;
                            FacesContext.getCurrentInstance().addMessage(
                                    null,
                                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + sysOfferingTypeTO.getOfferingType() + "' is already present under '"
                                            + sysOfferingTypeTOList.get(i).getOfferingCategory() + "'", ""));
                            sysOfferingTypeBean.setRenderErrorMessage(true);
                            break;
                        }
                    }
                }
            }
            if (j == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateEditSysOfferingType(SysOfferingTypeTO sysOfferingTypeTO)
    {
        List<SysOfferingTypeTO> sysOfferingTypeTOList = null;
        Set<SysOfferingTypeTO> sysOfferingCategoryList = null;
        try {
            sysOfferingCategoryList = new TreeSet<SysOfferingTypeTO>(new SysOfferingCategoryComparator());
            initialize();
            sysOfferingTypeTOList = sysOfferingTypeBean.getOfferingTypeTOList();
            sysOfferingCategoryList.addAll(sysOfferingTypeTOList);
            sysOfferingTypeBean.setOfferingCategoryList(sysOfferingCategoryList);
            sysOfferingTypeBean.setOfferingCategoryID(sysOfferingTypeTO.getOfferingCategoryID());
            sysOfferingTypeBean.setDescription(sysOfferingTypeTO.getDescription());
            sysOfferingTypeBean.setOfferingTypeID(sysOfferingTypeTO.getOfferingTypeID());
            sysOfferingTypeBean.setOfferingType(sysOfferingTypeTO.getOfferingType());
            sysOfferingTypeBean.setDuplicateOfferingCategoryID(sysOfferingTypeTO.getOfferingCategoryID());
            sysOfferingTypeBean.setDuplicateOfferingType(sysOfferingTypeTO.getOfferingType());
            sysOfferingTypeBean.setDuplicateDescription(sysOfferingTypeTO.getDescription());
        } catch (Exception e) {

        }
    }

    public void saveSysOfferingType()
    {
        SysOfferingTypeTO sysOfferingTypeTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysOfferingTypeTO = new SysOfferingTypeTO();
            sysOfferingTypeTO.setOfferingCategoryID(sysOfferingTypeBean.getOfferingCategoryID());
            sysOfferingTypeTO.setOfferingCategory(sysOfferingTypeBean.getOfferingCategory());
            if (sysOfferingTypeBean.getOfferingTypeID() != null) {
                sysOfferingTypeTO.setOfferingTypeID(sysOfferingTypeBean.getOfferingTypeID());
            }
            sysOfferingTypeTO.setOfferingType(sysOfferingTypeBean.getOfferingType());
            sysOfferingTypeTO.setDescription(sysOfferingTypeBean.getDescription());

            if (validateSysOfferingType(sysOfferingTypeTO)) {
                sysOfferingTypeService.saveSysOfferingType(sysOfferingTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "SysOfferingType updated successfully", ""));
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_DATATABLE_RESET);
                requestContext.execute(ISysOfferingTypeConstants.SYSOFFERINGTYPE_PAGINATION_DATATABLE_RESET);
            } else {
                RequestContext.getCurrentInstance().update("sysOfferingTypeEditForm");
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysOfferingCategoryConstants.SYSOFFERINGCATEGORY_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
        }
    }
}