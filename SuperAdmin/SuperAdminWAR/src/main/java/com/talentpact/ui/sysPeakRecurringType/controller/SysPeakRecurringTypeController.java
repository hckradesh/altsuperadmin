/**
 * 
 */
package com.talentpact.ui.sysPeakRecurringType.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import com.alt.altbenefits.to.SysPeakRecurringTypeTO;
import com.talentpact.business.common.constants.ISysPeakRecurringTypeConstants;
import com.talentpact.business.dataservice.sysPeakRecurringType.SysPeakRecurringTypeService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysPeakRecurringType.bean.SysPeakRecurringTypeBean;

/**
 * 
 * @author vivek.goyal
 * 
 */


@Named("sysPeakRecurringTypeController")
@ConversationScoped
public class SysPeakRecurringTypeController extends CommonController implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = -2937030591547477634L;

    private static final Logger LOGGER_ = LoggerFactory.getLogger(SysPeakRecurringTypeController.class);

    @Inject
    MenuBean menuBean;

    @Inject
    SysPeakRecurringTypeBean sysPeakRecurringTypeBean;

    @Inject
    SysPeakRecurringTypeService sysPeakRecurringTypeService;

    @Override
    public void initialize()
    {
        List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList = null;
        Set<String> name = null;
        SysPeakRecurringTypeTO sysPeakRecurringTypeTO = null;

        try {
            RequestContext requestContext = RequestContext.getCurrentInstance();
            resetSysPeakRecurringType();
            sysPeakRecurringTypeTOList = sysPeakRecurringTypeService.getSysPeakRecurringTypeList();
            sysPeakRecurringTypeBean.setSysPeakRecurringTypeTOList(sysPeakRecurringTypeTOList);
            name = new HashSet<String>();
            for (SysPeakRecurringTypeTO list : sysPeakRecurringTypeTOList) {
                sysPeakRecurringTypeTO = new SysPeakRecurringTypeTO();
                sysPeakRecurringTypeTO = list;
                name.add(sysPeakRecurringTypeTO.getRecurringTypeLabel());
            }

            sysPeakRecurringTypeBean.setSysPeakRecurringType(name);
            menuBean.setPage("../SysPeakRecurringType/sysPeakRecurringType.xhtml");
            sysPeakRecurringTypeBean.setRenderSysPeakRecurringTypePopup(false);
            sysPeakRecurringTypeBean.setRenderErrorMessage(false);
            requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_DATATABLE_RESET);
            requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_PAGINATION_DATATABLE_RESET);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetSysPeakRecurringType()
    {
        sysPeakRecurringTypeBean.setRenderSysPeakRecurringTypePopup(true);
        sysPeakRecurringTypeBean.setRecurringTypeLabel(null);
        sysPeakRecurringTypeBean.setSysPeakRecurringType(null);
        sysPeakRecurringTypeBean.setSysPeakRecurringTypeTOList(null);
        sysPeakRecurringTypeBean.setSelectedPeakRecurringType(null);
    }

    public void addSysPeakRecurringType()
    {
        SysPeakRecurringTypeTO sysPeakRecurringTypeTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPeakRecurringTypeTO = new SysPeakRecurringTypeTO();
            sysPeakRecurringTypeTO.setRecurringTypeLabel(sysPeakRecurringTypeBean.getRecurringTypeLabel());
            sysPeakRecurringTypeBean.setRenderErrorMessage(true);

            if (validateNewSysPeakRecurringType(sysPeakRecurringTypeTO.getRecurringTypeLabel())) {
                sysPeakRecurringTypeService.addSysPeakRecurringType(sysPeakRecurringTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Content Category saved successfully", ""));
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_ADD_DIALOG_HIDE);
                requestContext.update("sysOfferingCategoryListForm");
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_DATATABLE_RESET);
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_PAGINATION_DATATABLE_RESET);
            } else {

                RequestContext.getCurrentInstance().update("addSysPeakRecurringTypeForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysPeakRecurringTypeBean.setRenderSysPeakRecurringTypePopup(false);
            requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_ADD_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("addSysPeakRecurringTypeForm");
        }
    }

    // To validate if the newly added SysPeakRecurringType is already present in the system
    private boolean validateNewSysPeakRecurringType(String categoryName)
    {
        categoryName = categoryName.trim();
        List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList = null;
        int j = 0;
        try {
            if (categoryName.equalsIgnoreCase("") || categoryName.equalsIgnoreCase(null)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, " Please enter a valid SysPeakRecurringType", ""));
                j = 1;
            } else {
                sysPeakRecurringTypeTOList = sysPeakRecurringTypeService.getSysPeakRecurringTypeList();
                for (int i = 0; i < sysPeakRecurringTypeTOList.size(); i++) {
                    if (sysPeakRecurringTypeTOList.get(i).getRecurringTypeLabel().equalsIgnoreCase(categoryName)) {
                        j = 1;
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "'" + categoryName + "' is already present in the existing SysPeakRecurringType list.", ""));
                        sysPeakRecurringTypeBean.setRenderErrorMessage(true);
                        break;
                    }
                }
            }
            if (j != 1) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void refreshPopUp(SysPeakRecurringTypeTO sysPeakRecurringTypeTO)
    {
        try {
            sysPeakRecurringTypeBean.setRecurringTypeID(sysPeakRecurringTypeTO.getRecurringTypeID());
            sysPeakRecurringTypeBean.setRecurringTypeLabel(sysPeakRecurringTypeTO.getRecurringTypeLabel());
            sysPeakRecurringTypeBean.setRenderEditSysPeakRecurringTypePopup(true);
            sysPeakRecurringTypeBean.setDuplicateRecurringType(sysPeakRecurringTypeTO.getRecurringTypeLabel());
        } catch (Exception e) {
        }
    }

    public void updateSysPeakRecurringType()
    {
        SysPeakRecurringTypeTO sysPeakRecurringTypeTO = null;
        RequestContext requestContext = null;
        try {
            requestContext = RequestContext.getCurrentInstance();
            sysPeakRecurringTypeTO = new SysPeakRecurringTypeTO();
            sysPeakRecurringTypeTO.setRecurringTypeID(sysPeakRecurringTypeBean.getRecurringTypeID());
            sysPeakRecurringTypeTO.setRecurringTypeLabel(sysPeakRecurringTypeBean.getRecurringTypeLabel());

            /*if ((sysPeakRecurringTypeBean.getDuplicateCategoryName()).trim().equalsIgnoreCase(sysPeakRecurringTypeTO.getOfferingCategory())
                    && sysPeakRecurringTypeBean.getDuplicateDescription().trim().equalsIgnoreCase(sysPeakRecurringTypeTO.getDescription())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No changes were made.", ""));
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_EDIT_DIALOG_HIDE);
            } else*/
            if (validateNewSysPeakRecurringType(sysPeakRecurringTypeTO.getRecurringTypeLabel())) {
                sysPeakRecurringTypeService.editSysPeakRecurringType(sysPeakRecurringTypeTO);
                initialize();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sys Offering Category updated successfully", ""));
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_EDIT_DIALOG_HIDE);
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_DATATABLE_RESET);
                requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_PAGINATION_DATATABLE_RESET);


            } else {
                RequestContext.getCurrentInstance().update("sysOfferingCategoryEditForm");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_SAVE_ERROR_MSG, ""));
            LOGGER_.error("", e);
            sysPeakRecurringTypeBean.setRenderSysPeakRecurringTypePopup(false);
            requestContext.execute(ISysPeakRecurringTypeConstants.SYSPEAKRECURRINGTYPE_EDIT_DIALOG_HIDE);
            RequestContext.getCurrentInstance().update("sysOfferingCategoryEditForm");
        }
    }
}