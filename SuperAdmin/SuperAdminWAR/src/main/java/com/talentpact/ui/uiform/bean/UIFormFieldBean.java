package com.talentpact.ui.uiform.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.survey.builder.transport.input.validations.QuestionColumnValidationMapperRequestTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysFieldTypeResponseTO;
import com.talentpact.business.application.transport.output.SysRuleResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysChatBotValidation.to.ChatBotColumnValidationMapperTO;
import com.talentpact.ui.sysChatBotValidationType.to.SysChatBotValidationTypeTO;
import com.talentpact.ui.uiform.controller.UiFormController;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("uiFormFieldBean")
@SessionScoped
public class UIFormFieldBean extends CommonBean implements Serializable, IDefaultAction
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    UiFormController uiFormController;

    private List<UiFormFieldResponseTO> uiFormFieldResponseTOList;

    private Integer formFieldID;

    private Integer fieldGroupID;

    private Integer entityFieldID;

    private Integer organizationID;

    private String clientID;

    private Integer tenantID;

    private Integer createdBy;

    private Integer modifiedBy;

    private Date createdDate;

    private Date modifiedDate;

    private boolean isAction;

    private boolean isCutomfieldRequired;

    private boolean isMaster;

    private boolean isAttachmentRequired;

    private boolean isApprovalRequired;

    private boolean isEffectiveDateRequired;

    private Integer resourceID;

    private boolean mandatory;

    private Integer parentFormFieldID;

    private boolean isAttachmentEnabled;

    private String regex;

    private String fieldGroupName;

    private String sysEntityField;

    private String sysResourceValue;

    private List<SysBundleResponseTO> sysBundle;

    private String systextBoxes[];

    private String systextBoxesTitle[];

    private UiFormFieldResponseTO selectedFormField;

    private List<UiFormFieldResponseTO> uiParentFormFieldTOList;

    private List<UiFormFieldResponseTO> uiSysResourceResponseTOList;

    private List<UiFormFieldResponseTO> uiSysEntityFieldResponseTOList;

    private List<String> configurationValuelist;

    private String mobileConfiguration;

    private String webConfiguration;

    private boolean groupParameter;
    
    private Integer sysFieldTypeID;
    
    private List<SysFieldTypeResponseTO> sysFieldType;
    
	private Integer ruleID;
    
    private List<SysRuleResponseTO> sysRule;
    
    private String chatBotConfiguration;
    
    private Integer chatBotResourceID;
    
    private String sysChatBotTextBoxes[];

    private String sysChatBotTextBoxesTitle[];
    
    private List<SysChatBotValidationTypeTO> sysChatBotValidationTypeList;
    
    private List<Integer> selectedSysChatBotValidationTypeList;
    
    private boolean showValidationDetailDataTable;
    
    private List<ChatBotColumnValidationMapperTO> validationsSelectedAtColumnLevelBYUser;

    private Integer chatBotSequence;
    
    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            uiFormController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public UiFormController getUiFormController()
    {
        return uiFormController;
    }

    public void setUiFormController(UiFormController uiFormController)
    {
        this.uiFormController = uiFormController;
    }

    public List<UiFormFieldResponseTO> getUiFormFieldResponseTOList()
    {
        return uiFormFieldResponseTOList;
    }

    public void setUiFormFieldResponseTOList(List<UiFormFieldResponseTO> uiFormFieldResponseTOList)
    {
        this.uiFormFieldResponseTOList = uiFormFieldResponseTOList;
    }

    public Integer getFormFieldID()
    {
        return formFieldID;
    }

    public void setFormFieldID(Integer formFieldID)
    {
        this.formFieldID = formFieldID;
    }

    public Integer getFieldGroupID()
    {
        return fieldGroupID;
    }

    public void setFieldGroupID(Integer fieldGroupID)
    {
        this.fieldGroupID = fieldGroupID;
    }

    public Integer getEntityFieldID()
    {
        return entityFieldID;
    }

    public void setEntityFieldID(Integer entityFieldID)
    {
        this.entityFieldID = entityFieldID;
    }

    public Integer getOrganizationID()
    {
        return organizationID;
    }

    public void setOrganizationID(Integer organizationID)
    {
        this.organizationID = organizationID;
    }

    public String getClientID()
    {
        return clientID;
    }

    public void setClientID(String clientID)
    {
        this.clientID = clientID;
    }

    public Integer getTenantID()
    {
        return tenantID;
    }

    public void setTenantID(Integer tenantID)
    {
        this.tenantID = tenantID;
    }

    public Integer getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy)
    {
        this.createdBy = createdBy;
    }

    public Integer getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    public boolean isAction()
    {
        return isAction;
    }

    public void setAction(boolean isAction)
    {
        this.isAction = isAction;
    }

    public boolean isCutomfieldRequired()
    {
        return isCutomfieldRequired;
    }

    public void setCutomfieldRequired(boolean isCutomfieldRequired)
    {
        this.isCutomfieldRequired = isCutomfieldRequired;
    }

    public boolean isMaster()
    {
        return isMaster;
    }

    public void setMaster(boolean isMaster)
    {
        this.isMaster = isMaster;
    }

    public boolean isAttachmentRequired()
    {
        return isAttachmentRequired;
    }

    public void setAttachmentRequired(boolean isAttachmentRequired)
    {
        this.isAttachmentRequired = isAttachmentRequired;
    }

    public boolean isApprovalRequired()
    {
        return isApprovalRequired;
    }

    public void setApprovalRequired(boolean isApprovalRequired)
    {
        this.isApprovalRequired = isApprovalRequired;
    }

    public boolean isEffectiveDateRequired()
    {
        return isEffectiveDateRequired;
    }

    public void setEffectiveDateRequired(boolean isEffectiveDateRequired)
    {
        this.isEffectiveDateRequired = isEffectiveDateRequired;
    }

    public Integer getResourceID()
    {
        return resourceID;
    }

    public void setResourceID(Integer resourceID)
    {
        this.resourceID = resourceID;
    }

    public boolean isMandatory()
    {
        return mandatory;
    }

    public void setMandatory(boolean mandatory)
    {
        this.mandatory = mandatory;
    }

    public Integer getParentFormFieldID()
    {
        return parentFormFieldID;
    }

    public void setParentFormFieldID(Integer parentFormFieldID)
    {
        this.parentFormFieldID = parentFormFieldID;
    }

    public boolean isAttachmentEnabled()
    {
        return isAttachmentEnabled;
    }

    public void setAttachmentEnabled(boolean isAttachmentEnabled)
    {
        this.isAttachmentEnabled = isAttachmentEnabled;
    }

    public String getRegex()
    {
        return regex;
    }

    public void setRegex(String regex)
    {
        this.regex = regex;
    }

    public String getFieldGroupName()
    {
        return fieldGroupName;
    }

    public void setFieldGroupName(String fieldGroupName)
    {
        this.fieldGroupName = fieldGroupName;
    }

    public String getSysEntityField()
    {
        return sysEntityField;
    }

    public void setSysEntityField(String sysEntityField)
    {
        this.sysEntityField = sysEntityField;
    }

    public String getSysResourceValue()
    {
        return sysResourceValue;
    }

    public void setSysResourceValue(String sysResourceValue)
    {
        this.sysResourceValue = sysResourceValue;
    }

    public List<SysBundleResponseTO> getSysBundle()
    {
        return sysBundle;
    }

    public void setSysBundle(List<SysBundleResponseTO> sysBundle)
    {
        this.sysBundle = sysBundle;
    }

    public String[] getSystextBoxes()
    {
        return systextBoxes;
    }

    public void setSystextBoxes(String[] systextBoxes)
    {
        this.systextBoxes = systextBoxes;
    }

    public String[] getSystextBoxesTitle()
    {
        return systextBoxesTitle;
    }

    public void setSystextBoxesTitle(String[] systextBoxesTitle)
    {
        this.systextBoxesTitle = systextBoxesTitle;
    }

    public UiFormFieldResponseTO getSelectedFormField()
    {
        return selectedFormField;
    }

    public void setSelectedFormField(UiFormFieldResponseTO selectedFormField)
    {
        this.selectedFormField = selectedFormField;
    }

    public List<UiFormFieldResponseTO> getUiParentFormFieldTOList()
    {
        return uiParentFormFieldTOList;
    }

    public void setUiParentFormFieldTOList(List<UiFormFieldResponseTO> uiParentFormFieldTOList)
    {
        this.uiParentFormFieldTOList = uiParentFormFieldTOList;
    }

    public List<UiFormFieldResponseTO> getUiSysResourceResponseTOList()
    {
        return uiSysResourceResponseTOList;
    }

    public void setUiSysResourceResponseTOList(List<UiFormFieldResponseTO> uiSysResourceResponseTOList)
    {
        this.uiSysResourceResponseTOList = uiSysResourceResponseTOList;
    }

    public List<UiFormFieldResponseTO> getUiSysEntityFieldResponseTOList()
    {
        return uiSysEntityFieldResponseTOList;
    }

    public void setUiSysEntityFieldResponseTOList(List<UiFormFieldResponseTO> uiSysEntityFieldResponseTOList)
    {
        this.uiSysEntityFieldResponseTOList = uiSysEntityFieldResponseTOList;
    }

    public List<String> getConfigurationValuelist()
    {
        return configurationValuelist;
    }

    public void setConfigurationValuelist(List<String> configurationValuelist)
    {
        this.configurationValuelist = configurationValuelist;
    }

    public String getMobileConfiguration()
    {
        return mobileConfiguration;
    }

    public void setMobileConfiguration(String mobileConfiguration)
    {
        this.mobileConfiguration = mobileConfiguration;
    }

    public String getWebConfiguration()
    {
        return webConfiguration;
    }

    public void setWebConfiguration(String webConfiguration)
    {
        this.webConfiguration = webConfiguration;
    }

    public boolean isGroupParameter()
    {
        return groupParameter;
    }

    public void setGroupParameter(boolean groupParameter)
    {
        this.groupParameter = groupParameter;
    }

	
    public List<SysFieldTypeResponseTO> getSysFieldType() {
		return sysFieldType;
	}

	public void setSysFieldType(List<SysFieldTypeResponseTO> sysFieldType) {
		this.sysFieldType = sysFieldType;
	}

	public List<SysRuleResponseTO> getSysRule() {
		return sysRule;
	}

	public void setSysRule(List<SysRuleResponseTO> sysRule) {
		this.sysRule = sysRule;
	}

	public Integer getSysFieldTypeID() {
		return sysFieldTypeID;
	}

	public void setSysFieldTypeID(Integer sysFieldTypeID) {
		this.sysFieldTypeID = sysFieldTypeID;
	}

	public Integer getRuleID() {
		return ruleID;
	}

	public void setRuleID(Integer ruleID) {
		this.ruleID = ruleID;
	}

	public String getChatBotConfiguration() {
		return chatBotConfiguration;
	}

	public void setChatBotConfiguration(String chatBotConfiguration) {
		this.chatBotConfiguration = chatBotConfiguration;
	}

	public String[] getSysChatBotTextBoxes() {
		return sysChatBotTextBoxes;
	}

	public void setSysChatBotTextBoxes(String[] sysChatBotTextBoxes) {
		this.sysChatBotTextBoxes = sysChatBotTextBoxes;
	}

	public String[] getSysChatBotTextBoxesTitle() {
		return sysChatBotTextBoxesTitle;
	}

	public void setSysChatBotTextBoxesTitle(String[] sysChatBotTextBoxesTitle) {
		this.sysChatBotTextBoxesTitle = sysChatBotTextBoxesTitle;
	}

	public Integer getChatBotResourceID() {
		return chatBotResourceID;
	}

	public void setChatBotResourceID(Integer chatBotResourceID) {
		this.chatBotResourceID = chatBotResourceID;
	}

	public List<SysChatBotValidationTypeTO> getSysChatBotValidationTypeList() {
		return sysChatBotValidationTypeList;
	}

	public void setSysChatBotValidationTypeList(
			List<SysChatBotValidationTypeTO> sysChatBotValidationTypeList) {
		this.sysChatBotValidationTypeList = sysChatBotValidationTypeList;
	}



	public List<Integer> getSelectedSysChatBotValidationTypeList() {
		return selectedSysChatBotValidationTypeList;
	}

	public void setSelectedSysChatBotValidationTypeList(
			List<Integer> selectedSysChatBotValidationTypeList) {
		this.selectedSysChatBotValidationTypeList = selectedSysChatBotValidationTypeList;
	}

	public boolean isShowValidationDetailDataTable() {
		return showValidationDetailDataTable;
	}

	public void setShowValidationDetailDataTable(
			boolean showValidationDetailDataTable) {
		this.showValidationDetailDataTable = showValidationDetailDataTable;
	}

	public List<ChatBotColumnValidationMapperTO> getValidationsSelectedAtColumnLevelBYUser() {
		return validationsSelectedAtColumnLevelBYUser;
	}

	public void setValidationsSelectedAtColumnLevelBYUser(
			List<ChatBotColumnValidationMapperTO> validationsSelectedAtColumnLevelBYUser) {
		this.validationsSelectedAtColumnLevelBYUser = validationsSelectedAtColumnLevelBYUser;
	}

	public Integer getChatBotSequence() {
		return chatBotSequence;
	}

	public void setChatBotSequence(Integer chatBotSequence) {
		this.chatBotSequence = chatBotSequence;
	}

	
	
}
