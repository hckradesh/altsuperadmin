/**
 * 
 */
package com.talentpact.ui.survey.user.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.model.formenginenew.SurveyOrg;
import com.talentpact.model.formenginenew.SysSurvey;
import com.talentpact.ui.survey.user.common.transport.output.SurveyOrgResponseTO;
import com.talentpact.ui.survey.user.common.transport.output.SysSurveyResponseTO;



/**
 * @author vaibhav.kashyap
 *
 */
public class SurveyOrgConverter {

	private static Logger LOGGER_ = LoggerFactory.getLogger(SurveyOrgConverter.class);
	
	/**
	 * {@link #getSurveyOrganizationsDetails(SurveyOrg)} : called when new organization records are
	 * persisted
	 * @param SurveyOrg
	 * @return SurveyOrgResponseTO
	 * @author vaibhav.kashyap
	 * @throws Exception
	 * */
	public static SurveyOrgResponseTO getSurveyOrganizationsDetails(SurveyOrg resultSet)
		throws Exception{
		
		SurveyOrgResponseTO responseTO = null;
		List<SysSurveyResponseTO> sysSurveyResponseTOList = null;
		SysSurveyResponseTO sysSurveyResponseTO = null;
		try{
			if(resultSet!=null){
				responseTO = new SurveyOrgResponseTO();
				responseTO.setOrganizationalID(resultSet.getOrganizationID());
				responseTO.setOrganizationName(resultSet.getName());
				responseTO.setTenantID(resultSet.getTenantID());
				
				if(resultSet.getSysSurveys()!=null){
					sysSurveyResponseTOList = new ArrayList<SysSurveyResponseTO>();
					Set<SysSurvey> surveys = resultSet.getSysSurveys();
					for(SysSurvey temp : surveys){
						sysSurveyResponseTO = new SysSurveyResponseTO();
						sysSurveyResponseTO.setName(temp.getName());
						sysSurveyResponseTOList.add(sysSurveyResponseTO);
					}
					
					responseTO.setSysSurveys(sysSurveyResponseTOList);
				}
								
			}
		}catch(Exception ex){
			throw ex;
		}finally{
			
		}
		
		return responseTO;
	}
	
	/**
	 * {@link #getSurveyOrgResponseIfAreadyExists(SurveyOrg)} : called to prepare
	 * dummy response object when organization is already found in records
	 * @param SurveyOrg
	 * @throws Exception
	 * @return SurveyOrgResponseTO
	 * @author vaibhav.kashyap
	 * */
	public static SurveyOrgResponseTO getSurveyOrgResponseIfAreadyExists(
			SurveyOrg orgRawObj) throws Exception{
		SurveyOrgResponseTO surveyOrgResponseTO = null;
		try{
			if(orgRawObj!=null){
				surveyOrgResponseTO = new SurveyOrgResponseTO();
				surveyOrgResponseTO.setOrganizationalID(orgRawObj.getOrganizationID());
				surveyOrgResponseTO.setOrganizationName(orgRawObj.getName());
				surveyOrgResponseTO.setTenantID(orgRawObj.getTenantID());
			}
		}catch(Exception ex){
			throw ex;
		}
		
		return surveyOrgResponseTO;
	}
}
