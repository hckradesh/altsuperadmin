/**
 * 
 */
package com.talentpact.ui.survey.user.dataprovider.DS;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.formenginenew.SurveyOrg;
import com.talentpact.ui.survey.user.common.transport.input.SurveyOrgRequestTO;
import com.talentpact.ui.survey.user.common.transport.output.SurveyOrgResponseTO;
import com.talentpact.ui.survey.user.converter.SurveyOrgConverter;



/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SurveyOrgDS extends AbstractDS<SurveyOrg> {
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(SurveyOrgDS.class);
	
	public SurveyOrgDS(){
		super(SurveyOrg.class);
	}

	/**
	 * @param surveyOrgRequestTO
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public SurveyOrgResponseTO createNewOrganization(
			SurveyOrgRequestTO surveyOrgRequestTO) throws Exception{
		SurveyOrgResponseTO surveyOrgResponseTO = null;
		SurveyOrg surveyOrg = null;
		java.util.Date utilDate = new java.util.Date();
	    java.sql.Date date = new java.sql.Date(utilDate.getTime());
		
	    try{
	    	
	    	if(surveyOrgRequestTO!=null){
	    		/**
	    		 * check if the organization details are already present in records
	    		 * if exists simply return dummy response object with details sent through
	    		 * request else save the details.
	    		 * */
	    		surveyOrg = getEntityManager("TalentPactFormEngine_New").find(SurveyOrg.class, surveyOrgRequestTO.getOrganizationID());
		    	if(surveyOrg==null){
		    		surveyOrg = new SurveyOrg();
		    		surveyOrg.setName(surveyOrgRequestTO.getOrganizationName());
		    		surveyOrg.setOrganizationID(surveyOrgRequestTO.getOrganizationID());
		    		surveyOrg.setTenantID(surveyOrgRequestTO.getTenantID());
		    		getEntityManager("TalentPactFormEngine_New").persist(surveyOrg);
		    		surveyOrgResponseTO = SurveyOrgConverter.getSurveyOrganizationsDetails(surveyOrg);
		    	}else{
		    		surveyOrgResponseTO = SurveyOrgConverter.getSurveyOrgResponseIfAreadyExists(surveyOrg);
		    	}
	    		
	    	}
		
	    }catch(Exception ex){
	    	LOGGER_.debug("Exception thrown while saving survey organization (createNewOrganization): "+ex);
			surveyOrgResponseTO = null;
			throw ex;
		}finally{
			
		}
		return surveyOrgResponseTO;
	}

}
