/**
 * 
 */
package com.talentpact.ui.system.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.alt.system.transport.AltSyncUrlTO;
import com.alt.system.transport.SchemaTO;
import com.alt.system.transport.SysDatabaseConfigTO;
import com.alt.system.transport.SysEnvTO;
import com.alt.system.transport.TpAppTO;
import com.alt.system.transport.UrlTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboard.transport.output.ModuleTO;
import com.talentpact.ui.system.controller.UrlMigrationController;

/**
 * @author javed.ali
 *
 */

@Named("urlMigrationBean")
@ConversationScoped
public class UrlMigrationBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 143546090834123124L;

    @Inject
    UrlMigrationController urlMigrationController;

    private List<UrlTO> urlTOList;

    private Integer selectedModuleId;

    private List<ModuleTO> moduleTOList;

    private List<SchemaTO> schemaTOList;

    private List<SchemaTO> filteredSchemaTOList;

    private List<AltSyncUrlTO> altSyncUrlTOList;

    private List<AltSyncUrlTO> filteredAltSyncUrlTOList;

    private List<TpAppTO> appTOList;

    private List<SysEnvTO> sysEnvTOList;

    private List<SysDatabaseConfigTO> databaseConfigTOList;

    private boolean renderModulePanel;

    private boolean renderSchemaPanel;

    private boolean active;

    private SchemaTO[] selectedSchemaList;

    private String selectedUrlCompositeID;

    private String selectedUrlID;

    private boolean success;

    private String successMessage;

    private boolean renderFormMessage;

    private Integer selectedEnvID;


    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getSelectedUrlID()
    {
        return selectedUrlID;
    }

    public void setSelectedUrlID(String selectedUrlID)
    {
        this.selectedUrlID = selectedUrlID;
    }

    public boolean isRenderFormMessage()
    {
        return renderFormMessage;
    }

    public void setRenderFormMessage(boolean renderFormMessage)
    {
        this.renderFormMessage = renderFormMessage;
    }

    public String getSuccessMessage()
    {
        return successMessage;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public void setSuccessMessage(String successMessage)
    {
        this.successMessage = successMessage;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public SchemaTO[] getSelectedSchemaList()
    {
        return selectedSchemaList;
    }

    public void setSelectedSchemaList(SchemaTO[] selectedSchemaList)
    {
        this.selectedSchemaList = selectedSchemaList;
    }

    public String getSelectedUrlCompositeID()
    {
        return selectedUrlCompositeID;
    }

    public void setSelectedUrlCompositeID(String selectedUrlCompositeID)
    {
        this.selectedUrlCompositeID = selectedUrlCompositeID;
    }

    private Integer selectedOrgID;

    private Long selectedAppID;

    private Integer selectedDbConfigId;

    public Integer getSelectedDbConfigId()
    {
        return selectedDbConfigId;
    }

    public void setSelectedDbConfigId(Integer selectedDbConfigId)
    {
        this.selectedDbConfigId = selectedDbConfigId;
    }

    public List<SchemaTO> getFilteredSchemaTOList()
    {
        return filteredSchemaTOList;
    }

    public void setFilteredSchemaTOList(List<SchemaTO> filteredSchemaTOList)
    {
        this.filteredSchemaTOList = filteredSchemaTOList;
    }

    public Integer getSelectedModuleId()
    {
        return selectedModuleId;
    }

    public void setSelectedModuleId(Integer selectedModuleId)
    {
        this.selectedModuleId = selectedModuleId;
    }

    @Override
    public void initialize()
    {
        endConversation();
        beginConversation();
        urlMigrationController.initialize();
    }


    public List<UrlTO> getUrlTOList()
    {
        return urlTOList;
    }


    public void setUrlTOList(List<UrlTO> urlTOList)
    {
        this.urlTOList = urlTOList;
    }


    public List<ModuleTO> getModuleTOList()
    {
        return moduleTOList;
    }


    public void setModuleTOList(List<ModuleTO> moduleTOList)
    {
        this.moduleTOList = moduleTOList;
    }


    public List<SchemaTO> getSchemaTOList()
    {
        return schemaTOList;
    }


    public void setSchemaTOList(List<SchemaTO> schemaTOList)
    {
        this.schemaTOList = schemaTOList;
    }


    public boolean isRenderModulePanel()
    {
        return renderModulePanel;
    }


    public void setRenderModulePanel(boolean renderModulePanel)
    {
        this.renderModulePanel = renderModulePanel;
    }


    public boolean isRenderSchemaPanel()
    {
        return renderSchemaPanel;
    }


    public void setRenderSchemaPanel(boolean renderSchemaPanel)
    {
        this.renderSchemaPanel = renderSchemaPanel;
    }

    public Integer getSelectedOrgID()
    {
        return selectedOrgID;
    }

    public void setSelectedOrgID(Integer selectedOrgID)
    {
        this.selectedOrgID = selectedOrgID;
    }

    public Long getSelectedAppID()
    {
        return selectedAppID;
    }

    public void setSelectedAppID(Long selectedAppID)
    {
        this.selectedAppID = selectedAppID;
    }

    public UrlMigrationController getUrlMigrationController()
    {
        return urlMigrationController;
    }

    public void setUrlMigrationController(UrlMigrationController urlMigrationController)
    {
        this.urlMigrationController = urlMigrationController;
    }

    public List<AltSyncUrlTO> getAltSyncUrlTOList()
    {
        return altSyncUrlTOList;
    }

    public void setAltSyncUrlTOList(List<AltSyncUrlTO> altSyncUrlTOList)
    {
        this.altSyncUrlTOList = altSyncUrlTOList;
    }

    public List<TpAppTO> getAppTOList()
    {
        return appTOList;
    }

    public void setAppTOList(List<TpAppTO> appTOList)
    {
        this.appTOList = appTOList;
    }

    public List<SysDatabaseConfigTO> getDatabaseConfigTOList()
    {
        return databaseConfigTOList;
    }

    public void setDatabaseConfigTOList(List<SysDatabaseConfigTO> databaseConfigTOList)
    {
        this.databaseConfigTOList = databaseConfigTOList;
    }

    public List<SysEnvTO> getSysEnvTOList()
    {
        return sysEnvTOList;
    }

    public void setSysEnvTOList(List<SysEnvTO> sysEnvTOList)
    {
        this.sysEnvTOList = sysEnvTOList;
    }

    public List<AltSyncUrlTO> getFilteredAltSyncUrlTOList()
    {
        return filteredAltSyncUrlTOList;
    }

    public void setFilteredAltSyncUrlTOList(List<AltSyncUrlTO> filteredAltSyncUrlTOList)
    {
        this.filteredAltSyncUrlTOList = filteredAltSyncUrlTOList;
    }

  

    public Integer getSelectedEnvID()
    {
        return selectedEnvID;
    }

    public void setSelectedEnvID(Integer selectedEnvID)
    {
        this.selectedEnvID = selectedEnvID;
    }
}
