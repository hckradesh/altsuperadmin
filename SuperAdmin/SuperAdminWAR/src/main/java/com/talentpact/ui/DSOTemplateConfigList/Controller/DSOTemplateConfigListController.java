package com.talentpact.ui.DSOTemplateConfigList.Controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.dataservice.sysDSOPlaceHolder.DSOTemplateConfigService;
import com.talentpact.business.dataservice.sysDSOPlaceHolder.SYSDSOPlaceHolderService;
import com.talentpact.ui.DSOTemplateConfigList.bean.DSOTemplateConfigListBean;
import com.talentpact.ui.DSPTemplateConfig.to.DSOTemplateConfigTO;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;

@SuppressWarnings("serial")
@Named("dsoTemplateConfigListController")
@ConversationScoped
public class DSOTemplateConfigListController extends CommonController implements Serializable, IDefaultAction{

	private static final Logger LOGGER_ = LoggerFactory.getLogger(DSOTemplateConfigListController.class);
	
	@Inject
    MenuBean menuBean;
	
	@Inject
    UserSessionBean userSessionBean;
	
	@Inject
	DSOTemplateConfigListBean dsoTemplateConfigListBean;
	
	@Inject
	DSOTemplateConfigService dsoTemplateConfigService;
	
	@Inject
	SYSDSOPlaceHolderService sysDSOPlaceHolderService;
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		ServiceResponse response = null;
        try {
        	dsoTemplateConfigListBean.setDsoTemplateConfigTOList(dsoTemplateConfigService.getDSOTemplateConfigList());
            RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfigList.xhtml");
          /*  requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_DATATABLE_RESET);
            requestContext.execute(ISysFieldTypeConstants.SYSFIELDTYPE_PAGINATION_DATATABLE_RESET);
*/
        } catch (Exception e) {
            LOGGER_.error("", e);
        }
	}
	
	public void create(){
		try{
			dsoTemplateConfigListBean.setRenderupdateBtn(false);
			dsoTemplateConfigListBean.setTemplateName(null);
			dsoTemplateConfigListBean.setDescription(null);
			dsoTemplateConfigListBean.setBcc(null);
			dsoTemplateConfigListBean.setCc(null);
			dsoTemplateConfigListBean.setTo(null);
			dsoTemplateConfigListBean.setSubject(null);
			dsoTemplateConfigListBean.setContent(null);
			dsoTemplateConfigListBean.setEditDisable(true);
			dsoTemplateConfigListBean.setPhList(sysDSOPlaceHolderService.getTextTypeDSOPlaceHolderList());
			menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfig.xhtml");
			
		}
		catch(Exception ex){
			LOGGER_.error("", ex.getMessage(), ex);
		}
	}
	
   public void cancelPlaceHolder() {
        try {

        } catch (Exception ex) {

        }

    }
    
    public void selectPlaceHolderTO() {

        List<DSOPlaceHolderTO> placeHolder = null;
        try {
        	dsoTemplateConfigListBean.setRecipientTo(true);
        	dsoTemplateConfigListBean.setRecipientCC(false);
        	dsoTemplateConfigListBean.setRecipientBcc(false);
        	placeHolder = sysDSOPlaceHolderService.getEmailTypeDSOPlaceHolderList();
            dsoTemplateConfigListBean.setPlaceHolderListTo(placeHolder);
        } catch (Exception ex) {

        }
    }

    public void selectPlaceHolderCC() {
        List<DSOPlaceHolderTO> placeHolder = null;
        try {
        	dsoTemplateConfigListBean.setRecipientTo(false);
        	dsoTemplateConfigListBean.setRecipientCC(true);
        	dsoTemplateConfigListBean.setRecipientBcc(false);
        	placeHolder = sysDSOPlaceHolderService.getEmailTypeDSOPlaceHolderList();
            dsoTemplateConfigListBean.setPlaceHolderListTo(placeHolder);
        } catch (Exception ex) {

        }
    }

    public void selectPlaceHolderBCC() {
        List<DSOPlaceHolderTO> placeHolder = null;
        try {
        	dsoTemplateConfigListBean.setRecipientTo(false);
        	dsoTemplateConfigListBean.setRecipientCC(false);
        	dsoTemplateConfigListBean.setRecipientBcc(true);
        	placeHolder = sysDSOPlaceHolderService.getEmailTypeDSOPlaceHolderList();
            dsoTemplateConfigListBean.setPlaceHolderListTo(placeHolder);
        } catch (Exception ex) {

        }
    }
    
    public void assignPlaceHolder() {
        String recipientAddress = "";
        List<DSOPlaceHolderTO> placeHolderListTo = null;
        try {
            placeHolderListTo = dsoTemplateConfigListBean.getPlaceHolderListTo();

            if (placeHolderListTo != null) {
                for (DSOPlaceHolderTO list : placeHolderListTo) {
                    if (list.getSelected()) {
                        recipientAddress = recipientAddress + list.getPlaceholderValue() + ", ";
                     }
                }
                
                if (dsoTemplateConfigListBean.isRecipientTo()) {
                	dsoTemplateConfigListBean.setTo(recipientAddress);
                }

                if (dsoTemplateConfigListBean.isRecipientCC()) {
                	dsoTemplateConfigListBean.setCc(recipientAddress);
                }

                if (dsoTemplateConfigListBean.isRecipientBcc()) {
                	dsoTemplateConfigListBean.setBcc(recipientAddress);
                }
            } //end of if placeholder
            
        } catch (Exception ex) {

        }

    }
    
    public void save() {
    	
    	RequestContext request = null;
    	
    	DSOTemplateConfigTO dsoTemplateConfigTO = null;
        int templateID = 0;
        String templateName = null;
        String configTO = null;
        String configCC = null;
        String configBCC = null;

        try {
        	
        	request = RequestContext.getCurrentInstance();
        	
        	if(dsoTemplateConfigListBean.getTemplateName() == null || dsoTemplateConfigListBean.getTemplateName().isEmpty() || dsoTemplateConfigListBean.getTemplateName().equals("")){
        		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Please enter Template name", ""));
    			        request.update("DSPTemplateComfig");
    			        return;
    				
    				
        		}
        	
        	if(dsoTemplateConfigListBean.getDsoTemplateConfigTOList() != null && !dsoTemplateConfigListBean.getDsoTemplateConfigTOList().isEmpty()){
        		for(DSOTemplateConfigTO obj : dsoTemplateConfigListBean.getDsoTemplateConfigTOList()){
    				if(obj.getTemplateName().equalsIgnoreCase(dsoTemplateConfigListBean.getTemplateName()))
    				{
    					FacesContext.getCurrentInstance().addMessage(
    							null,
    							new FacesMessage(FacesMessage.SEVERITY_ERROR,
    									"Duplicate template", ""));
    			        request.update("DSPTemplateComfig");
    			        return;
    				}
    				
        		}

				dsoTemplateConfigTO = new DSOTemplateConfigTO();
				templateName = dsoTemplateConfigListBean.getTemplateName();
	            if (templateName != null) {
	            	dsoTemplateConfigTO.setTemplateName(templateName);
	            }
	            dsoTemplateConfigTO.setDescription(dsoTemplateConfigListBean.getDescription());
	            //		sysTemplateConfigTO.setFrom(from);
	            configTO = dsoTemplateConfigListBean.getTo();
	            dsoTemplateConfigTO.setTo(configTO);
	            configCC = dsoTemplateConfigListBean.getCc();
	            dsoTemplateConfigTO.setCc(configCC);
	            configBCC = dsoTemplateConfigListBean.getBcc();
	            dsoTemplateConfigTO.setBcc(configBCC);
	            dsoTemplateConfigTO.setSubject(dsoTemplateConfigListBean.getSubject());
	            dsoTemplateConfigTO.setContent(dsoTemplateConfigListBean.getContent());
	            dsoTemplateConfigTO.setTenantID(1);
	            dsoTemplateConfigTO.setOrganizationID(1);
	            dsoTemplateConfigTO.setModuleName("DSO");
	            int ret = dsoTemplateConfigService.saveDSOTemplateConfig(dsoTemplateConfigTO);
	            if (ret == 0) {
	                System.out.print("Duplicate template");
	                FacesContext.getCurrentInstance().addMessage(
	    					null,
	    					new FacesMessage(FacesMessage.SEVERITY_ERROR,
	    							"Duplicate template", ""));
	                request.update("DSPTemplateComfig");
	            } else {
	            	menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfigList.xhtml");
	            	initialize();
	            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "save template successfully", ""));
	            	request.update("TemplateListForm");
	                
	            }
			
        	}
			if(dsoTemplateConfigListBean.getDsoTemplateConfigTOList() == null || dsoTemplateConfigListBean.getDsoTemplateConfigTOList().isEmpty() ){
				dsoTemplateConfigTO = new DSOTemplateConfigTO();
				templateName = dsoTemplateConfigListBean.getTemplateName();
	            if (templateName != null) {
	            	dsoTemplateConfigTO.setTemplateName(templateName);
	            }
	            dsoTemplateConfigTO.setDescription(dsoTemplateConfigListBean.getDescription());
	            //		sysTemplateConfigTO.setFrom(from);
	            configTO = dsoTemplateConfigListBean.getTo();
	            dsoTemplateConfigTO.setTo(configTO);
	            configCC = dsoTemplateConfigListBean.getCc();
	            dsoTemplateConfigTO.setCc(configCC);
	            configBCC = dsoTemplateConfigListBean.getBcc();
	            dsoTemplateConfigTO.setBcc(configBCC);
	            dsoTemplateConfigTO.setSubject(dsoTemplateConfigListBean.getSubject());
	            dsoTemplateConfigTO.setContent(dsoTemplateConfigListBean.getContent());
	            dsoTemplateConfigTO.setTenantID(1);
	            dsoTemplateConfigTO.setOrganizationID(1);
	            dsoTemplateConfigTO.setModuleName("DSO");
	            int ret = dsoTemplateConfigService.saveDSOTemplateConfig(dsoTemplateConfigTO);
	            if (ret == 0) {
	                System.out.print("Duplicate template");
	                FacesContext.getCurrentInstance().addMessage(
	    					null,
	    					new FacesMessage(FacesMessage.SEVERITY_ERROR,
	    							"Duplicate template", ""));
	                request.update("DSPTemplateComfig");
	            } else {
	            	menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfigList.xhtml");
	            	initialize();
	            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "save template successfully", ""));
	            	request.update("TemplateListForm");
	                
	            }
			}
			
				
			
            
        } catch (Exception ex) {

        }

    }

    public void cancel() {
        try {
            menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfigList.xhtml");
         } catch (Exception ex) {

        }

    }
    
    public void edit(DSOTemplateConfigTO dsoTemplateConfigTO) throws Exception{
    	try{
	    	dsoTemplateConfigListBean.setRenderupdateBtn(true);
	    	dsoTemplateConfigListBean.setEditDisable(true);
	    	dsoTemplateConfigListBean.setTemplateId(dsoTemplateConfigTO.getTemplateId());
		    dsoTemplateConfigListBean.setTemplateName(dsoTemplateConfigTO.getTemplateName());
			dsoTemplateConfigListBean.setDescription(dsoTemplateConfigTO.getDescription());
			dsoTemplateConfigListBean.setBcc(dsoTemplateConfigTO.getBcc());
			dsoTemplateConfigListBean.setCc(dsoTemplateConfigTO.getCc());
			dsoTemplateConfigListBean.setTo(dsoTemplateConfigTO.getTo());
			dsoTemplateConfigListBean.setSubject(dsoTemplateConfigTO.getSubject());
			dsoTemplateConfigListBean.setContent(dsoTemplateConfigTO.getContent());
			dsoTemplateConfigListBean.setPhList(sysDSOPlaceHolderService.getTextTypeDSOPlaceHolderList());
			menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfig.xhtml");
    	}
    	catch(Exception ex){
    		LOGGER_.error("", ex.getMessage(), ex);
    	}
	    	
    }
    
    @SuppressWarnings("unused")
	public void update() {
    	RequestContext request = null;
    	DSOTemplateConfigTO dsoTemplateConfigTO = null;
        String templateName = null;
        try {
        	request = RequestContext.getCurrentInstance();
        	
        	if(dsoTemplateConfigListBean.getTemplateName() == null || dsoTemplateConfigListBean.getTemplateName().isEmpty() || dsoTemplateConfigListBean.getTemplateName().equals("")){
        		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Please enter template name", ""));
    			        request.update("DSPTemplateComfig");
    			        return;
    				
    				
        		}
			
			dsoTemplateConfigTO = new DSOTemplateConfigTO();
            templateName = dsoTemplateConfigListBean.getTemplateName();
            if (templateName != null) {
            	dsoTemplateConfigTO.setTemplateName(templateName);
            }
            dsoTemplateConfigTO.setTemplateId(dsoTemplateConfigListBean.getTemplateId());
            dsoTemplateConfigTO.setDescription(dsoTemplateConfigListBean.getDescription());
            dsoTemplateConfigTO.setTo(dsoTemplateConfigListBean.getTo());
            dsoTemplateConfigTO.setCc(dsoTemplateConfigListBean.getCc());
            dsoTemplateConfigTO.setBcc(dsoTemplateConfigListBean.getBcc());
            dsoTemplateConfigTO.setSubject(dsoTemplateConfigListBean.getSubject());
            dsoTemplateConfigTO.setContent(dsoTemplateConfigListBean.getContent());
            dsoTemplateConfigTO.setModuleName("DSO");
            int ret = dsoTemplateConfigService.updateDSOTemplateConfig(dsoTemplateConfigTO);

            if (ret == 0) {
            	System.out.print("Duplicate template");
                FacesContext.getCurrentInstance().addMessage(
    					null,
    					new FacesMessage(FacesMessage.SEVERITY_ERROR,
    							"Duplicate template", ""));
                request.update("DSPTemplateComfig");

            } else {
            	System.out.print("Update template successfully");
            	initialize();
            	menuBean.setPage("../DSOTemplateConfig/DSOTemplateConfigList.xhtml");
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Update template successfully", ""));
            	request.update("TemplateListForm");
            }
		} catch (Exception ex) {
        	LOGGER_.error("", ex.getMessage(), ex);
        }

    }
}
