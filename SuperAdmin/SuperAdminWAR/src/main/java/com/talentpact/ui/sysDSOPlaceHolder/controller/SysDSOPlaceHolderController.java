package com.talentpact.ui.sysDSOPlaceHolder.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.common.service.output.ServiceResponse;
import com.talentpact.business.common.constants.ISysDSOPlaceHolderConstants;
import com.talentpact.business.dataservice.sysDSOPlaceHolder.SYSDSOPlaceHolderService;
import com.talentpact.ui.common.bean.UserSessionBean;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.sysDSOPlaceHolder.bean.SysDSOPlaceHolderBean;
import com.talentpact.ui.sysDSOPlaceHolder.to.DSOPlaceHolderTO;

@Named("sysDSOPlaceHolderController")
public class SysDSOPlaceHolderController extends CommonController implements Serializable, IDefaultAction  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER_ = LoggerFactory.getLogger(SysDSOPlaceHolderController.class);
	
	@Inject
    MenuBean menuBean;
	
	@Inject
    UserSessionBean userSessionBean;
	
	@Inject
	SysDSOPlaceHolderBean sysDSOPlaceHolderBean;
	
	@Inject
	SYSDSOPlaceHolderService sysDSOPlaceHolderService;

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		ServiceResponse response = null;
        try {
        	sysDSOPlaceHolderBean.setDSOPlaceHolderTOList(sysDSOPlaceHolderService.getDSOPlaceHolderList());
        	RequestContext requestContext = RequestContext.getCurrentInstance();
            menuBean.setPage("../SysDSOPlaceHolder/sysDSOPlaceHolder.xhtml");
            getPlaceHolderValue();
            requestContext.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_DATATABLE_RESET);
            requestContext.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_PAGINATION_DATATABLE_RESET);

        } catch (Exception e) {
            LOGGER_.error("", e);
        }
	}

	public void resetDSOPlaceHolder() {
		sysDSOPlaceHolderBean.setdSOPlaceholdersID(0);
		sysDSOPlaceHolderBean.setPlaceHolderLabel(null);
		sysDSOPlaceHolderBean.setPlaceHolderType(null);
		sysDSOPlaceHolderBean.setPlaceHolderValue(null);
		sysDSOPlaceHolderBean.setIsActive(false);
		sysDSOPlaceHolderBean.setDSOPlaceHolderTOList(null);
	}
	
	
	public void saveDSOPlaceHolder()
	        throws Exception {
		DSOPlaceHolderTO dsoPlaceHolderTO = null;
	        RequestContext request = null;
	        try {
	            request = RequestContext.getCurrentInstance();
	            dsoPlaceHolderTO = new DSOPlaceHolderTO();
	            dsoPlaceHolderTO.setPlaceholderLabel(sysDSOPlaceHolderBean.getPlaceHolderLabel());
	            dsoPlaceHolderTO.setPlaceholderType(sysDSOPlaceHolderBean.getPlaceHolderType());
	            dsoPlaceHolderTO.setPlaceholderValue(sysDSOPlaceHolderBean.getPlaceHolderValue());
	            dsoPlaceHolderTO.setIsActive(sysDSOPlaceHolderBean.getIsActive());
	            
	            if (validateDSOPlaceHolder(dsoPlaceHolderTO, false)) {
	            	sysDSOPlaceHolderService.saveDSOPlaceHolder(dsoPlaceHolderTO);
	                initialize();
	                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DSOPlaceHolder added successfully", ""));
	                request.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_ADD_DIALOG_HIDE);
	                request.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_DATATABLE_RESET);
	                request.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_PAGINATION_DATATABLE_RESET);
	                request.update("DSOPlaceHolderListForm");
	            } else {
	                FacesContext.getCurrentInstance().addMessage(null,
	                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_ERROR_MSG, ""));
	            }
	        } catch (Exception e) {
	            FacesContext.getCurrentInstance().addMessage(null,
	                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_SAVE_ERROR_MSG, ""));
	            LOGGER_.error("", e);
	            throw e;
	        }

	    }
	
	public void editDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO) {
        try {
        	
        	sysDSOPlaceHolderBean.setDsoPlaceHolderTO(dsoPlaceHolderTO);
        } catch (Exception ex) {
            LOGGER_.error("", ex);
        }
    }
	 public void updateDSOPlaceHolder()
		        throws Exception {
		 		DSOPlaceHolderTO dsoPlaceHolderTO = null;
		        RequestContext requestContext = null;
		        try {
		            requestContext = RequestContext.getCurrentInstance();
		            dsoPlaceHolderTO = new DSOPlaceHolderTO();
		            dsoPlaceHolderTO.setdSOPlaceholdersID(sysDSOPlaceHolderBean.getDsoPlaceHolderTO().getdSOPlaceholdersID());
		            dsoPlaceHolderTO.setPlaceholderLabel(sysDSOPlaceHolderBean.getDsoPlaceHolderTO().getPlaceholderLabel());
		            dsoPlaceHolderTO.setPlaceholderType(sysDSOPlaceHolderBean.getDsoPlaceHolderTO().getPlaceholderType());
		            dsoPlaceHolderTO.setPlaceholderValue(sysDSOPlaceHolderBean.getDsoPlaceHolderTO().getPlaceholderValue());
		            dsoPlaceHolderTO.setIsActive(sysDSOPlaceHolderBean.getDsoPlaceHolderTO().getIsActive());
		            if (validateDSOPlaceHolder(dsoPlaceHolderTO, true)) {
		            	sysDSOPlaceHolderService.updateDSOPlaceHolder(dsoPlaceHolderTO);
		                initialize();
		                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "DSOPlaceHolder updated successfully", ""));
		                requestContext.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_EDIT_DIALOG_HIDE);
		                requestContext.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_DATATABLE_RESET);
		                requestContext.execute(ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_PAGINATION_DATATABLE_RESET);
		                requestContext.update("DSOPlaceHolderListForm");
		            } else {
		                FacesContext.getCurrentInstance().addMessage(null,
		                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_ERROR_MSG, ""));
		            }
		        } catch (Exception e) {
		            FacesContext.getCurrentInstance().addMessage(null,
		                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ISysDSOPlaceHolderConstants.DSOPLACEHOLDER_UPDATE_ERROR_MSG, ""));
		            LOGGER_.error("", e);
		            throw e;
		        }
		    }
	 
	 private boolean validateDSOPlaceHolder(DSOPlaceHolderTO dsoPlaceHolderTO, boolean edited)
		        throws Exception {
		        List<DSOPlaceHolderTO> dsoPlaceHolderTOList = null;
		        try {
		            if (edited) {
		            	dsoPlaceHolderTOList = sysDSOPlaceHolderService.getUpdateDSOPlaceHolder(dsoPlaceHolderTO.getdSOPlaceholdersID());
		            } else {
		            	dsoPlaceHolderTOList = sysDSOPlaceHolderService.getDSOPlaceHolderList();
		            }
		            if (dsoPlaceHolderTOList != null && !dsoPlaceHolderTOList.isEmpty()) {
		                for (DSOPlaceHolderTO dph : dsoPlaceHolderTOList) {
		                    if (dph.getPlaceholderValue().trim().equalsIgnoreCase(dsoPlaceHolderTO.getPlaceholderValue())) {
		                        return false;
		                    }
		                }
		            }
		            return true;
		        } catch (Exception ex) {
		            LOGGER_.error("", ex);
		            throw new Exception(ex);
		        }
		    }

	    public List<String> getPlaceHolderValue()
	            throws Exception
	        {

	            List<String> placeHolderTypelist = null;
	            String configurationValue = null;
	            int i = 0;
	            try {
	                i = 2;
	                placeHolderTypelist = new ArrayList<String>();

	                while (i >= 1) {
	                    if (i == 2) {
	                        configurationValue = "TEXT";
	                    }
	                    if (i == 1) {
	                        configurationValue = "EMAIL";
	                    }

	                    placeHolderTypelist.add(configurationValue);
	                    i--;
	                }

	                sysDSOPlaceHolderBean.setPlaceHolderTypelist(placeHolderTypelist);

	                return placeHolderTypelist;
	            } catch (Exception e) {
	                e.printStackTrace();
	                throw new Exception(e);
	            }
	        }
}
