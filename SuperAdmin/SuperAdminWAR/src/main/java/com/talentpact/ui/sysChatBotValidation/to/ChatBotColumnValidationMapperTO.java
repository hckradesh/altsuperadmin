/**
 * 
 */
package com.talentpact.ui.sysChatBotValidation.to;

import java.util.List;

import com.alt.common.transport.impl.RequestTransferObject;

/**
 * @author vaibhav.kashyap
 * 
 */
public class ChatBotColumnValidationMapperTO extends
		RequestTransferObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer SysValidationTypeID;

	private Integer columnSequence;
	private Integer noOfParameters;

	private Boolean renderParameterInputs = false;

	/**
	 * Name of selected validation name
	 * */
	private String selectedValidationLabel = "";

	/**
	 * List consist of validation parameter as string value, validation type id
	 * for the selected validation from the edit column settings popup
	 * */
	List<SysChatBotValidationTO> listOfParametersInputByUserForSelectedValidation = null;

	public ChatBotColumnValidationMapperTO() {

	}

	public Integer getSysValidationTypeID() {
		return SysValidationTypeID;
	}

	public void setSysValidationTypeID(Integer sysValidationTypeID) {
		SysValidationTypeID = sysValidationTypeID;
	}

	public Integer getColumnSequence() {
		return columnSequence;
	}

	public void setColumnSequence(Integer columnSequence) {
		this.columnSequence = columnSequence;
	}

	public Integer getNoOfParameters() {
		return noOfParameters;
	}

	public void setNoOfParameters(Integer noOfParameters) {
		this.noOfParameters = noOfParameters;
	}

	public Boolean getRenderParameterInputs() {
		return renderParameterInputs;
	}

	public void setRenderParameterInputs(Boolean renderParameterInputs) {
		this.renderParameterInputs = renderParameterInputs;
	}

	public String getSelectedValidationLabel() {
		return selectedValidationLabel;
	}

	public void setSelectedValidationLabel(String selectedValidationLabel) {
		this.selectedValidationLabel = selectedValidationLabel;
	}

	public List<SysChatBotValidationTO> getListOfParametersInputByUserForSelectedValidation() {
		return listOfParametersInputByUserForSelectedValidation;
	}

	public void setListOfParametersInputByUserForSelectedValidation(
			List<SysChatBotValidationTO> listOfParametersInputByUserForSelectedValidation) {
		this.listOfParametersInputByUserForSelectedValidation = listOfParametersInputByUserForSelectedValidation;
	}

	

}
