package com.talentpact.ui.survey.user.common.transport.form.helper;

/**
 * 
 * 
 * @author javed.ali
 * 
 */
public enum CustomFormActionEnum {
    NEW("New"), UPDATE("Update"), DELETE("Delete"), NONE("None");

    private String action;

    private CustomFormActionEnum(String s)
    {
        action = s;
    }

    public String getCustomFormActionEnum()
    {
        return action;
    }
    
    public static boolean contains(String key) {
        for (CustomFormActionEnum c : CustomFormActionEnum.values()) {
            if (c.name().equals(key)) {
                return true;
            }
        }
        return false;
    }
}