/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.dataservice;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysPlaceHolder.common.transport.input.SysPlaceHolderRequestTO;
import com.talentpact.ui.sysPlaceHolder.common.transport.output.SysPlaceHolderResponseTO;
import com.talentpact.ui.sysPlaceHolder.dataservice.DS.SysPlaceHolderDS;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysPlaceHolderDataService {

	@EJB
	private SysPlaceHolderDS sysPlaceHolderDS;
	
	/**
	 * @return
	 */
	public ServiceResponse getAllPlaceHolderData() 
			throws Exception{
		ServiceResponse serviceResponse = null;
		SysPlaceHolderResponseTO responseTOObject = null;
		
		List<String> sysCommunicationTypeFilter = null;
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		try{
			responseTOObject = new SysPlaceHolderResponseTO();
			sysPlaceHolderResponseTOList = sysPlaceHolderDS.getAllPlaceHolderData();
			if(sysPlaceHolderResponseTOList!=null && 
					!sysPlaceHolderResponseTOList.isEmpty()){
				
				/**
				 * Communication type filter list preparation
				 * */
				sysCommunicationTypeFilter = new ArrayList<String>();
				for(SysPlaceHolderResponseTO obj : sysPlaceHolderResponseTOList){
					if(obj.getCommTypeName()!=null && 
							!obj.getCommTypeName().equals(""))
					sysCommunicationTypeFilter.add(new String(obj.getCommTypeName()));
				}
				
				if(!sysCommunicationTypeFilter.isEmpty())
					responseTOObject.setSysCommunicationTypeFilterList(sysCommunicationTypeFilter);
				
				responseTOObject.setSysPlaceHolderResponseTOObj(sysPlaceHolderResponseTOList);
					
				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObject(responseTOObject);
				/**
				 * write logic
				 * */
			}
		}catch(Exception ex){
			sysPlaceHolderResponseTOList = null;
			serviceResponse = null;
			throw ex;
		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse insertNewPlaceHolder(ServiceRequest serviceRequest)
		throws Exception{
		ServiceResponse serviceResponse = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		SysPlaceHolderRequestTO sysPlaceHolderRequestTO = null;
		try{
			sysPlaceHolderRequestTO = (SysPlaceHolderRequestTO)serviceRequest.getRequestTransferObjectRevised();
			if(sysPlaceHolderRequestTO!=null){
				sysPlaceHolderResponseTO = sysPlaceHolderDS.insertNewPlaceHolder(sysPlaceHolderRequestTO);
				if(sysPlaceHolderResponseTO!=null){
					serviceResponse = new ServiceResponse();
					serviceResponse.setResponseTransferObject(sysPlaceHolderResponseTO);
				}
			}
		}catch(Exception ex){
			
		}finally{
			
		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse deletePlaceHolderByID(ServiceRequest serviceRequest) 
		throws Exception{
		SysPlaceHolderRequestTO sysPlaceHolderRequestTO = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		ServiceResponse serviceResponse = null;
		
		int deletedRows = -1;
		Long placeHolderID = null;
		try{
			sysPlaceHolderRequestTO = (SysPlaceHolderRequestTO)serviceRequest.
					getRequestTransferObjectRevised();
			deletedRows = sysPlaceHolderDS.deletePlaceHolderByID(sysPlaceHolderRequestTO.getPlaceHolderID());
			/**
			 * since id is unique therefore single row
			 * deletion in expected
			 * */
			if(deletedRows==1){
				sysPlaceHolderResponseTO = new SysPlaceHolderResponseTO();
				sysPlaceHolderResponseTO.setDeletedRowCount(deletedRows);

				serviceResponse = new ServiceResponse();
				serviceResponse.setResponseTransferObject(sysPlaceHolderResponseTO);
			}else{
				return null;
			}
		}catch(Exception ex){
		}
		return serviceResponse;
	}

	/**
	 * @param serviceRequest
	 * @return
	 */
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public ServiceResponse updateNewPlaceHolder(ServiceRequest serviceRequest) throws Exception{
		ServiceResponse serviceResponse = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		SysPlaceHolderRequestTO sysPlaceHolderRequestTO = null;
		try{
			sysPlaceHolderRequestTO = (SysPlaceHolderRequestTO)serviceRequest.getRequestTransferObjectRevised();
			if(sysPlaceHolderRequestTO!=null){
				sysPlaceHolderResponseTO = sysPlaceHolderDS.updatePlaceHolder(sysPlaceHolderRequestTO);
				if(sysPlaceHolderResponseTO!=null){
					serviceResponse = new ServiceResponse();
					serviceResponse.setResponseTransferObject(sysPlaceHolderResponseTO);
				}
			}
		}catch(Exception ex){
			throw ex;
		}finally{
			
		}
		return serviceResponse;

	}

}
