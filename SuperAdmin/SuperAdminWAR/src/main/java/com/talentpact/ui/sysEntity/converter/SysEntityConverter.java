/**
 * 
 */
package com.talentpact.ui.sysEntity.converter;

import com.talentpact.model.SysEntity;
import com.talentpact.ui.sysEntity.common.transport.output.SysEntityResponseTO;

/**
 * @author vaibhav.kashyap
 *
 */
public class SysEntityConverter {

	/**
	 * @param entityObj
	 * @return
	 */
	public static SysEntityResponseTO getSysEntityResponseTOByEntity(
			SysEntity entityObj) throws Exception{
		SysEntityResponseTO sysEntityResponseTO = null;
		try{
			if(entityObj!=null){
				sysEntityResponseTO = new SysEntityResponseTO();
				
				if(entityObj.getEntityId()!=null && entityObj.getEntityId()>0)
					sysEntityResponseTO.setEntityId(entityObj.getEntityId());
				
				if(entityObj.getDescription()!=null && 
						!entityObj.getDescription().equals(""))
					sysEntityResponseTO.setDescription(entityObj.getDescription());
				
				if(entityObj.getLableColumn()!=null && 
						!entityObj.getLableColumn().equals(""))
					sysEntityResponseTO.setLableColumn(entityObj.getLableColumn());
				
				if(entityObj.getModuleId()!=null && 
						entityObj.getModuleId()>0)
					sysEntityResponseTO.setModuleId(entityObj.getModuleId());
				
				if(entityObj.getName()!=null && 
						!entityObj.getName().equals(""))
					sysEntityResponseTO.setName(entityObj.getName());
				
				if(entityObj.getPrimaryKey()!=null && 
						!entityObj.getPrimaryKey().equals(""))
					sysEntityResponseTO.setPrimaryKey(entityObj.getPrimaryKey());
				
				if(entityObj.getTableName()!=null && 
						!entityObj.getTableName().equals(""))
					sysEntityResponseTO.setTableName(entityObj.getTableName());
				
			}
		}catch(Exception ex){
			sysEntityResponseTO = null;
			throw ex;
		}
		return sysEntityResponseTO;
	}

}
