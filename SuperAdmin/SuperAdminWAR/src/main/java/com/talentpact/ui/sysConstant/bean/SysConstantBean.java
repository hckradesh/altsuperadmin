/**
 * 
 */
package com.talentpact.ui.sysConstant.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysConstantResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysConstant.controller.SysConstantController;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysConstantBean")
@ConversationScoped
public class SysConstantBean extends CommonBean implements Serializable, IDefaultAction
{

    /**
     * 
     */
    private static final long serialVersionUID = 1998978L;

    @Inject
    SysConstantController sysConstantController;

    private Integer sysConstantCategoryID;

    private String sysConstantCategoryName;

    private Integer sysConstantID;

    private String sysConstantName;

    private List<SysConstantResponseTO> constantTOList;

    private List<SysConstantResponseTO> categoryDropDownTOList;

    private Boolean status;


    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysConstantController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public Integer getSysConstantCategoryID()
    {
        return sysConstantCategoryID;
    }


    public void setSysConstantCategoryID(Integer sysConstantCategoryID)
    {
        this.sysConstantCategoryID = sysConstantCategoryID;
    }


    public String getSysConstantCategoryName()
    {
        return sysConstantCategoryName;
    }


    public void setSysConstantCategoryName(String sysConstantCategoryName)
    {
        this.sysConstantCategoryName = sysConstantCategoryName;
    }


    public Integer getSysConstantID()
    {
        return sysConstantID;
    }


    public void setSysConstantID(Integer sysConstantID)
    {
        this.sysConstantID = sysConstantID;
    }


    public String getSysConstantName()
    {
        return sysConstantName;
    }


    public void setSysConstantName(String sysConstantName)
    {
        this.sysConstantName = sysConstantName;
    }


    public List<SysConstantResponseTO> getConstantTOList()
    {
        return constantTOList;
    }


    public void setConstantTOList(List<SysConstantResponseTO> constantTOList)
    {
        this.constantTOList = constantTOList;
    }


    public List<SysConstantResponseTO> getCategoryDropDownTOList()
    {
        return categoryDropDownTOList;
    }


    public void setCategoryDropDownTOList(List<SysConstantResponseTO> categoryDropDownTOList)
    {
        this.categoryDropDownTOList = categoryDropDownTOList;
    }


    public Boolean getStatus()
    {
        return status;
    }


    public void setStatus(Boolean status)
    {
        this.status = status;
    }


}
