package com.talentpact.ui.sysSubModule.exception;

public class SysSubModuleException extends Exception
{
private static final long serialVersionUID = 1L;
    
    private String message = null;

    public SysSubModuleException()
    {
        super();
    }

    public SysSubModuleException(String message)
    {
        super(message);
        this.message = message;
    }
    
    public SysSubModuleException(String message, Exception e) {
        super(message, e);
    }

    public SysSubModuleException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
