/**
 * 
 */
package com.talentpact.ui.common.constants;

/**
 * @author radhamadhab.dalai
 *
 */
public interface Iconstants
{
    public static final String SystemLevel = "System";

    public static final String TenantLevel = "Tenant";

    public static final String OrganizationLevel = "Organization";

    public static final String CONTENT_PATH_NAME_FOR_ICON = "icon_content_path";

    static final String ALT_RESOURCE_BUNDLE_NAME = "alt";

    public static final String CONTENT_PATH_NAME_FOR_LOGO = "logo_content_path";

    public static final String CONTENT_PATH_NAME_FOR_APPIMAGE = "appImage_content_path";

    public static final String CONTENT_PATH_NAME_FOR_RECRUIT_LOGO_UPLOAD = "altRecruit_upload_logo_path";

    public static final String CONTENT_PATH_NAME_FOR_INFER_LOGO_UPLOAD = "altInfer_upload_logo_path";

    public static final String CONTENT_PATH_NAME_FOR_ADMIN_LOGO_UPLOAD = "altAdmin_upload_logo_path";

    public static final String CONTENT_PATH_NAME_FOR_RECRUIT_LOGO_ACCESS = "logo_access_path";

    public static final String CONTENT_PATH_NAME_FOR_INFER_LOGO_ACCESS = "logo_access_path";

    public static final String CONTENT_PATH_NAME_FOR_ADMIN_LOGO_ACCESS = "logo_access_path";
    
    public static final String CONTENT_PATH_NAME_FOR_CONGRATULATION_ANNOUNCEMENT = "congratulation_announcement_access_path";


}
