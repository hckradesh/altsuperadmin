package com.talentpact.ui.sysOfferingType.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.model.altbenefit.SysOfferingCategory;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysOfferingType.controller.SysOfferingTypeController;

/**
 * 
 * @author vivek.goyal
 *
 */


@Named("sysOfferingTypeBean")
@ConversationScoped
public class SysOfferingTypeBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    SysOfferingTypeController sysOfferingTypeController;

    private Long offeringTypeID;

    private String offeringType;

    private SysOfferingCategory sysOfferingCategory;

    private Long offeringCategoryID;

    private String offeringCategory;

    private List<SysOfferingTypeTO> offeringTypeTOList;

    private Set<SysOfferingTypeTO> offeringCategoryList;

    private List<SysOfferingTypeTO> newOfferingCategoryTOList;

    private List<SysOfferingTypeTO> offeringTypeFilteredList;

    private List<SysOfferingTypeTO> editOfferingCategoryTOList;

    private SysOfferingTypeTO selectedTypeTO;

    private boolean renderSysOfferingCategoryPopup;

    private boolean renderEditSysOfferingCategoryPopup;

    private Set<String> offeringTypeList;

    private Set<String> selectedOfferingTypeList;

    private Long duplicateOfferingCategoryID;

    private String duplicateOfferingType;

    private String duplicateDescription;

    private boolean renderErrorMessage;

    private String description;

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            sysOfferingTypeController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public String getOfferingType()
    {
        return offeringType;
    }

    public void setOfferingType(String offeringType)
    {
        this.offeringType = offeringType;
    }


    public List<SysOfferingTypeTO> getOfferingTypeTOList()
    {
        return offeringTypeTOList;
    }

    public void setOfferingTypeTOList(List<SysOfferingTypeTO> offeringTypeTOList)
    {
        this.offeringTypeTOList = offeringTypeTOList;
    }

    public Set<SysOfferingTypeTO> getOfferingCategoryList()
    {
        return offeringCategoryList;
    }

    public void setOfferingCategoryList(Set<SysOfferingTypeTO> offeringCategoryList)
    {
        this.offeringCategoryList = offeringCategoryList;
    }

    public List<SysOfferingTypeTO> getNewOfferingCategoryTOList()
    {
        return newOfferingCategoryTOList;
    }

    public void setNewOfferingCategoryTOList(List<SysOfferingTypeTO> newOfferingCategoryTOList)
    {
        this.newOfferingCategoryTOList = newOfferingCategoryTOList;
    }

    public List<SysOfferingTypeTO> getOfferingTypeFilteredList()
    {
        return offeringTypeFilteredList;
    }

    public void setOfferingTypeFilteredList(List<SysOfferingTypeTO> offeringTypeFilteredList)
    {
        this.offeringTypeFilteredList = offeringTypeFilteredList;
    }

    public List<SysOfferingTypeTO> getEditOfferingCategoryTOList()
    {
        return editOfferingCategoryTOList;
    }

    public void setEditOfferingCategoryTOList(List<SysOfferingTypeTO> editOfferingCategoryTOList)
    {
        this.editOfferingCategoryTOList = editOfferingCategoryTOList;
    }

    public SysOfferingTypeTO getSelectedTypeTO()
    {
        return selectedTypeTO;
    }

    public void setSelectedTypeTO(SysOfferingTypeTO selectedTypeTO)
    {
        this.selectedTypeTO = selectedTypeTO;
    }

    public Set<String> getOfferingTypeList()
    {
        return offeringTypeList;
    }

    public void setOfferingTypeList(Set<String> offeringTypeList)
    {
        this.offeringTypeList = offeringTypeList;
    }

    public Set<String> getSelectedOfferingTypeList()
    {
        return selectedOfferingTypeList;
    }

    public void setSelectedOfferingTypeList(Set<String> selectedOfferingTypeList)
    {
        this.selectedOfferingTypeList = selectedOfferingTypeList;
    }

    public String getDuplicateOfferingType()
    {
        return duplicateOfferingType;
    }

    public void setDuplicateOfferingType(String duplicateOfferingType)
    {
        this.duplicateOfferingType = duplicateOfferingType;
    }

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    public SysOfferingCategory getSysOfferingCategory()
    {
        return sysOfferingCategory;
    }

    public void setSysOfferingCategory(SysOfferingCategory sysOfferingCategory)
    {
        this.sysOfferingCategory = sysOfferingCategory;
    }

    public Long getOfferingCategoryID()
    {
        return offeringCategoryID;
    }

    public void setOfferingCategoryID(Long offeringCategoryID)
    {
        this.offeringCategoryID = offeringCategoryID;
    }

    public String getOfferingCategory()
    {
        return offeringCategory;
    }

    public void setOfferingCategory(String offeringCategory)
    {
        this.offeringCategory = offeringCategory;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getOfferingTypeID()
    {
        return offeringTypeID;
    }

    public void setOfferingTypeID(Long offeringTypeID)
    {
        this.offeringTypeID = offeringTypeID;
    }

    public Long getDuplicateOfferingCategoryID()
    {
        return duplicateOfferingCategoryID;
    }

    public void setDuplicateOfferingCategoryID(Long duplicateOfferingCategoryID)
    {
        this.duplicateOfferingCategoryID = duplicateOfferingCategoryID;
    }

    public boolean isRenderSysOfferingCategoryPopup()
    {
        return renderSysOfferingCategoryPopup;
    }

    public void setRenderSysOfferingCategoryPopup(boolean renderSysOfferingCategoryPopup)
    {
        this.renderSysOfferingCategoryPopup = renderSysOfferingCategoryPopup;
    }

    public boolean isRenderEditSysOfferingCategoryPopup()
    {
        return renderEditSysOfferingCategoryPopup;
    }

    public void setRenderEditSysOfferingCategoryPopup(boolean renderEditSysOfferingCategoryPopup)
    {
        this.renderEditSysOfferingCategoryPopup = renderEditSysOfferingCategoryPopup;
    }

    public String getDuplicateDescription()
    {
        return duplicateDescription;
    }

    public void setDuplicateDescription(String duplicateDescription)
    {
        this.duplicateDescription = duplicateDescription;
    }
}
