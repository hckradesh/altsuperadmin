package com.talentpact.ui.sysPtZone.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysPtZone.controller.SysPtZoneController;
import com.talentpact.ui.sysPtZone.to.SysPtZoneTO;

/**
/**
 * 
 * @author prachi.bansal
 *
 */

@SuppressWarnings("serial")
@Named("sysPtZoneBean")
@ConversationScoped
public class SysPtZoneBean extends CommonBean implements Serializable, IDefaultAction
{
    
    @Inject
    SysPtZoneController sysPtZoneController;
    
    private List<SysPtZoneTO> sysPtZoneTOList;
    
    private List<SysPtZoneTO> stateList;
    
    private Integer ptZoneID;
    
    private String ptZoneName;
    
    private String ptZoneCode;
    
    private Long stateID;
    
    private String stateName;
    
    private boolean renderErrorMessage;
    
    private SysPtZoneTO sysPtZoneTO;
    
    @Override
    public void initialize() {
        
        try {
        	endConversation();
            beginConversation();
            sysPtZoneController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   

    public Integer getPtZoneID() {
        return ptZoneID;
    }

    public void setPtZoneID(Integer ptZoneID) {
        this.ptZoneID = ptZoneID;
    }

    public String getPtZoneName() {
        return ptZoneName;
    }

    public void setPtZoneName(String ptZoneName) {
        this.ptZoneName = ptZoneName;
    }

    public String getPtZoneCode() {
        return ptZoneCode;
    }

    public void setPtZoneCode(String ptZoneCode) {
        this.ptZoneCode = ptZoneCode;
    }

    public Long getStateID() {
        return stateID;
    }

    public void setStateID(Long stateID) {
        this.stateID = stateID;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
    
    public List<SysPtZoneTO> getSysPtZoneTOList() {
        return sysPtZoneTOList;
    }

    public void setSysPtZoneTOList(List<SysPtZoneTO> sysPtZoneTOList) {
        this.sysPtZoneTOList = sysPtZoneTOList;
    }



    public boolean isRenderErrorMessage() {
        return renderErrorMessage;
    }



    public void setRenderErrorMessage(boolean renderErrorMessage) {
        this.renderErrorMessage = renderErrorMessage;
    }



    public List<SysPtZoneTO> getStateList() {
        return stateList;
    }



    public void setStateList(List<SysPtZoneTO> stateList) {
        this.stateList = stateList;
    }



    public SysPtZoneTO getSysPtZoneTO() {
        return sysPtZoneTO;
    }



    public void setSysPtZoneTO(SysPtZoneTO sysPtZoneTO) {
        this.sysPtZoneTO = sysPtZoneTO;
    }
    
    }