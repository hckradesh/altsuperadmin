package com.talentpact.ui.uiform.bean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.constants.Iconstants;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.uiform.controller.UiFormController;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("uiFormBean")
@SessionScoped
public class UIFormBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    UiFormController uiFormController;

    private List<UiFormResponseTO> uiFormResponseTOList;

    private Integer formID;

    private String formName;

    private Integer primaryEntityID;

    private String primaryEntityName; // came from SysEntity tabel

    private Integer tenantID;

    private String url;

    private Integer subModuleID;

    private Integer moduleID;

    private String subModuleName;

    private String parentFormName;

    private Integer parentFormID;

    private boolean customComponentAllowed;

    private boolean menu;

    private List<UiFormResponseTO> subModuleList;

    private List<UiFormResponseTO> moduleList;

    private List<UiFormResponseTO> sysEntityList;

    private List<UiFormResponseTO> parentFormList;

    private Integer resourceId;

    private String resourceName;

    private String resourceDiscription;

    private Integer menuCategory;

    private Integer sequence;

    private String iconPath;

    private String defaultClassCall;

    private List<SysBundleResponseTO> sysBundle;

    private String systextBoxes[];

    private String systextBoxesTitle[];

    private boolean menuAndSequneceCheck;

    private Integer bundleId;

    private String disableBean;

    private String disableText;

    private String disableUrl;

    private UploadedFile icon;

    private String fragmentClass;

    private String floatingMenuIconPath;

    private String inactiveIconPath;

    private String mobileConfiguration;

    private String webConfiguration;
    
    private String iosSideMenu;
    
    private UiFormResponseTO uiFormResponseTO;

    public String getIosSideMenu()
    {
        return iosSideMenu;
    }

    public void setIosSideMenu(String iosSideMenu)
    {
        this.iosSideMenu = iosSideMenu;
    }

    public String getDisableBean()
    {
        return disableBean;
    }

    public void setDisableBean(String disableBean)
    {
        this.disableBean = disableBean;
    }

    public String getDisableText()
    {
        return disableText;
    }

    public void setDisableText(String disableText)
    {
        this.disableText = disableText;
    }

    public String getDisableUrl()
    {
        return disableUrl;
    }

    public void setDisableUrl(String disableUrl)
    {
        this.disableUrl = disableUrl;
    }

    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            uiFormController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public boolean getMenu()
    {
        return menu;
    }

    public void setMenu(boolean menu)
    {
        this.menu = menu;
    }

    public boolean isCustomComponentAllowed()
    {
        return customComponentAllowed;
    }

    public void setCustomComponentAllowed(boolean customComponentAllowed)
    {
        this.customComponentAllowed = customComponentAllowed;
    }

    public UiFormController getUiFormController()
    {
        return uiFormController;
    }

    public void setUiFormController(UiFormController uiFormController)
    {
        this.uiFormController = uiFormController;
    }

    public List<UiFormResponseTO> getUiFormResponseTOList()
    {
        return uiFormResponseTOList;
    }

    public void setUiFormResponseTOList(List<UiFormResponseTO> uiFormResponseTOList)
    {
        this.uiFormResponseTOList = uiFormResponseTOList;
    }

    public Integer getFormID()
    {
        return formID;
    }

    public void setFormID(Integer formID)
    {
        this.formID = formID;
    }

    public String getFormName()
    {
        return formName;
    }

    public void setFormName(String formName)
    {
        this.formName = formName;
    }

    public Integer getPrimaryEntityID()
    {
        return primaryEntityID;
    }

    public void setPrimaryEntityID(Integer primaryEntityID)
    {
        this.primaryEntityID = primaryEntityID;
    }

    public String getPrimaryEntityName()
    {
        return primaryEntityName;
    }

    public void setPrimaryEntityName(String primaryEntityName)
    {
        this.primaryEntityName = primaryEntityName;
    }

    public Integer getTenantID()
    {
        return tenantID;
    }

    public void setTenantID(Integer tenantID)
    {
        this.tenantID = tenantID;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getParentFormName()
    {
        return parentFormName;
    }

    public void setParentFormName(String parentFormName)
    {
        this.parentFormName = parentFormName;
    }

    public List<UiFormResponseTO> getSubModuleList()
    {
        return subModuleList;
    }

    public void setSubModuleList(List<UiFormResponseTO> subModuleList)
    {
        this.subModuleList = subModuleList;
    }

    public List<UiFormResponseTO> getSysEntityList()
    {
        return sysEntityList;
    }

    public void setSysEntityList(List<UiFormResponseTO> sysEntityList)
    {
        this.sysEntityList = sysEntityList;
    }

    public List<UiFormResponseTO> getParentFormList()
    {
        return parentFormList;
    }

    public void setParentFormList(List<UiFormResponseTO> parentFormList)
    {
        this.parentFormList = parentFormList;
    }

    public Integer getParentFormID()
    {
        return parentFormID;
    }

    public void setParentFormID(Integer parentFormID)
    {
        this.parentFormID = parentFormID;
    }

    public Integer getResourceId()
    {
        return resourceId;
    }

    public void setResourceId(Integer resourceId)
    {
        this.resourceId = resourceId;
    }

    public List<SysBundleResponseTO> getSysBundle()
    {
        return sysBundle;
    }

    public void setSysBundle(List<SysBundleResponseTO> sysBundle)
    {
        this.sysBundle = sysBundle;
    }

    public String[] getSystextBoxes()
    {
        return systextBoxes;
    }

    public void setSystextBoxes(String[] systextBoxes)
    {
        this.systextBoxes = systextBoxes;
    }

    public String[] getSystextBoxesTitle()
    {
        return systextBoxesTitle;
    }

    public void setSystextBoxesTitle(String[] systextBoxesTitle)
    {
        this.systextBoxesTitle = systextBoxesTitle;
    }

    public Integer getSubModuleID()
    {
        return subModuleID;
    }

    public void setSubModuleID(Integer subModuleID)
    {
        this.subModuleID = subModuleID;
    }

    public String getResourceName()
    {
        return resourceName;
    }

    public void setResourceName(String resourceName)
    {
        this.resourceName = resourceName;
    }

    public String getResourceDiscription()
    {
        return resourceDiscription;
    }

    public void setResourceDiscription(String resourceDiscription)
    {
        this.resourceDiscription = resourceDiscription;
    }

    public Integer getMenuCategory()
    {
        return menuCategory;
    }

    public void setMenuCategory(Integer menuCategory)
    {
        this.menuCategory = menuCategory;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public String getIconPath()
    {
        return iconPath;
    }

    public void setIconPath(String iconPath)
    {
        this.iconPath = iconPath;
    }

    public String getDefaultClassCall()
    {
        return defaultClassCall;
    }

    public void setDefaultClassCall(String defaultClassCall)
    {
        this.defaultClassCall = defaultClassCall;
    }

    public String getSubModuleName()
    {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName)
    {
        this.subModuleName = subModuleName;
    }

    public List<UiFormResponseTO> getModuleList()
    {
        return moduleList;
    }

    public void setModuleList(List<UiFormResponseTO> moduleList)
    {
        this.moduleList = moduleList;
    }

    public Integer getModuleID()
    {
        return moduleID;
    }

    public void setModuleID(Integer moduleID)
    {
        this.moduleID = moduleID;
    }

    public boolean isMenuAndSequneceCheck()
    {
        return menuAndSequneceCheck;
    }

    public void setMenuAndSequneceCheck(boolean menuAndSequneceCheck)
    {
        this.menuAndSequneceCheck = menuAndSequneceCheck;
    }

    public Integer getBundleId()
    {
        return bundleId;
    }

    public void setBundleId(Integer bundleId)
    {
        this.bundleId = bundleId;
    }

    public UploadedFile getIcon()
    {
        return icon;
    }

    public void setIcon(UploadedFile icon)
    {
        this.icon = icon;
    }

    public String getFragmentClass()
    {
        return fragmentClass;
    }

    public void setFragmentClass(String fragmentClass)
    {
        this.fragmentClass = fragmentClass;
    }


    public String getFloatingMenuIconPath()
    {
        return floatingMenuIconPath;
    }

    public void setFloatingMenuIconPath(String floatingMenuIconPath)
    {
        this.floatingMenuIconPath = floatingMenuIconPath;
    }


    public String getInactiveIconPath()
    {
        return inactiveIconPath;
    }

    public void setInactiveIconPath(String inactiveIconPath)
    {
        this.inactiveIconPath = inactiveIconPath;
    }


    public String getMobileConfiguration()
    {
        return mobileConfiguration;
    }

    public void setMobileConfiguration(String mobileConfiguration)
    {
        this.mobileConfiguration = mobileConfiguration;
    }

    public String getWebConfiguration()
    {
        return webConfiguration;
    }

    public void setWebConfiguration(String webConfiguration)
    {
        this.webConfiguration = webConfiguration;
    }

    public void handleFileUpload(FileUploadEvent event)
    {

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;

        try {
            destinationPath = getDestinationPath();
            setIconPath(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleFileUploadForFloatingMenus(FileUploadEvent event)
    {

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;

        try {
            destinationPath = getDestinationPath();
            setFloatingMenuIconPath(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void handleFileUploadForInactiveIcon(FileUploadEvent event)
    {

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String destinationPath = null;

        try {
            destinationPath = getDestinationPath();
            setInactiveIconPath(destinationPath + event.getFile().getFileName());
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getDestinationPath()
    {

        ResourceBundle resourceBundle = null;
        String initialPath = null;
        String destinationPath = null;
        String finalPath = null;
        File dir = null;

        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            resourceBundle = ResourceBundle.getBundle(Iconstants.ALT_RESOURCE_BUNDLE_NAME);
            initialPath = resourceBundle.getString(Iconstants.CONTENT_PATH_NAME_FOR_ICON);

            if (initialPath != null && initialPath.trim().length() != 0) {
                if (initialPath.lastIndexOf(File.separator) != (initialPath.length() - 1)) {
                    initialPath = initialPath;
                }
            } else {
                throw new Exception("Invalid path");
            }

            //   finalPath = initialPath + dateFormat.format(date);
            finalPath = initialPath;
            dir = new File(finalPath);
            dir.mkdir();
            destinationPath = finalPath + File.separator;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return destinationPath;
    }

    private void copyFile(String fileName, InputStream in)
    {
        String destinationPath = null;
        try {
            destinationPath = getDestinationPath();
            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(destinationPath + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public UiFormResponseTO getUiFormResponseTO() {
        return uiFormResponseTO;
    }

    public void setUiFormResponseTO(UiFormResponseTO uiFormResponseTO) {
        this.uiFormResponseTO = uiFormResponseTO;
    }
   
}
