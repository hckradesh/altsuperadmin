/**
 * 
 */
package com.talentpact.ui.sysPlaceHolder.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.model.SysEntity;
import com.talentpact.remote.sysEntity.ISysEntityRemote;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;
import com.talentpact.ui.hrContentType.facaderemote.HrContentTypeLocalFacade;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.rule.to.RuleResponseRevisedTO;
import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysCommunicationType.facaderemote.SysCommunicationTypeLocalFacade;
import com.talentpact.ui.sysEntityColumn.common.transport.output.SysEntityColumnResponseTO;
import com.talentpact.ui.sysPlaceHolder.bean.SysPlaceHolderBean;
import com.talentpact.ui.sysPlaceHolder.common.transport.input.SysPlaceHolderRequestTO;
import com.talentpact.ui.sysPlaceHolder.common.transport.output.SysPlaceHolderResponseTO;
import com.talentpact.ui.sysPlaceHolder.facaderemote.SysPlaceHolderLocalFacade;
import com.talentpact.ui.sysRule.facaderemote.SysRuleLocalFacade;
import com.talentpact.ui.sysSubModule.common.transport.input.SysSubModuleRequestTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.sysSubModule.facaderemote.SysSubModuleLocalFacade;

/**
 * @author vaibhav.kashyap
 * 
 */
@Named("sysPlaceHolderController")
@RequestScoped
public class SysPlaceHolderController extends CommonController implements
		IDefaultAction {

	@Inject
	private SysPlaceHolderLocalFacade sysPlaceHolderLocalFacade;

	@Inject
	private SysCommunicationTypeLocalFacade sysCommunicationTypeLocalFacade;

	@Inject
	private HrContentTypeLocalFacade hrContentTypeLocalFacade;

	@Inject
	private SysRuleLocalFacade sysRuleLocalFacade;

	@Inject
	private SysSubModuleLocalFacade sysSubModuleLocalFacade;

	/**
	 * Change made by :
	 * 
	 * @author Raghavendra mishra
	 * */
	@EJB
	ISysEntityRemote iSysEntityRemote;

	@Inject
	private SysPlaceHolderBean sysPlaceHolderBean;

	@Inject
	private MenuBean menuBean;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.talentpact.ui.common.controller.IDefaultAction#initialize()
	 */
	@Override
	public void initialize() {
		ServiceResponse serviceResponse = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		try {
			sysPlaceHolderBean.setSysplaceholderFormMessagesFlag(true);

			/**
			 * fetch all communication types
			 * */
			/**
			 * call for the below method has been commented due to change in
			 * database design
			 * 
			 * @author vaibhav.kashyap Date : 17/12/2015
			 * */
			// getAllCommType();

			/**
			 * fetches all module and sub-module details
			 * */
			getParentModuleAndSubModules();

			/**
			 * all rules
			 * */
			getAllRules();

			/**
			 * all place holder type
			 * */
			getHrContentTypeByContentCategory();

			/**
			 * all entities
			 * */
			getAllEntities();

			serviceResponse = sysPlaceHolderLocalFacade.getAllPlaceHolderData();
			if (serviceResponse != null) {
				sysPlaceHolderResponseTO = (SysPlaceHolderResponseTO) serviceResponse
						.getResponseTransferObject();
				sysPlaceHolderResponseTOList = sysPlaceHolderResponseTO
						.getSysPlaceHolderResponseTOObj();
				if (sysPlaceHolderResponseTOList != null
						&& !sysPlaceHolderResponseTOList.isEmpty()) {
					sysPlaceHolderBean
							.setSysPlaceHolderResponseTOList(sysPlaceHolderResponseTOList);
					menuBean.setPage("../SysPlaceHolder/sysPlaceHolder.xhtml");
				}

			}
		} catch (Exception ex) {

		} finally {

		}
	}

	/**
	 * {@link #getAllEntities()} : get all entities from {@link SysEntity}
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void getAllEntities() throws Exception {
		ServiceResponse serviceResponse = null;
		List<SysEntityColumnResponseTO> sysEntityResponseTOList = null;
		try {
			serviceResponse = iSysEntityRemote.getAllEntities();
			if (serviceResponse != null) {
				sysEntityResponseTOList = (List<SysEntityColumnResponseTO>) serviceResponse
						.getResponseTransferObjectObjectList();
				if (sysEntityResponseTOList != null) {
					sysPlaceHolderBean
							.setSysEntityColumnResponseTOList(sysEntityResponseTOList);
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * {@link #getAllCommType()} : get commType from SysCommunication type
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void getAllCommType() throws Exception {
		ServiceResponse serviceResponse = null;
		List<SysCommunicationTypeResponseTO> sysCommunicationTypeResponseTOList = null;
		try {
			serviceResponse = sysCommunicationTypeLocalFacade.getAllCommType();
			if (serviceResponse != null) {
				sysCommunicationTypeResponseTOList = (List<SysCommunicationTypeResponseTO>) serviceResponse
						.getResponseTransferObjectObjectList();
				if (sysCommunicationTypeResponseTOList != null) {
					sysPlaceHolderBean
							.setSysCommunicationTypeResponseTOList(sysCommunicationTypeResponseTOList);
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * {@link #getParentModuleAndSubModules()}
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void getParentModuleAndSubModules() throws Exception {
		ServiceResponse serviceResponse = null;
		ServiceRequest serviceRequest = null;
		SysSubModuleResponseTO sysSubModuleResponseTO = null;
		SysSubModuleRequestTO sysSubModuleRequestTO = null;
		try {
			sysSubModuleRequestTO = new SysSubModuleRequestTO();
			sysSubModuleRequestTO.setTenantID(new Integer(1));
			serviceRequest = new ServiceRequest();
			serviceRequest
					.setRequestTransferObjectRevised(sysSubModuleRequestTO);
			serviceResponse = sysSubModuleLocalFacade
					.getParentModuleAndSubModules(serviceRequest);
			if (serviceResponse != null) {
				sysSubModuleResponseTO = (SysSubModuleResponseTO) serviceResponse
						.getResponseTransferObject();

				if (sysSubModuleResponseTO.getParentModuleIDByChild() != null
						&& !sysSubModuleResponseTO.getParentModuleIDByChild()
								.isEmpty()) {
					sysPlaceHolderBean
							.setParentModuleIDByChild(sysSubModuleResponseTO
									.getParentModuleIDByChild());
					prepareSubModuleDataForRendering(sysSubModuleResponseTO
							.getParentModuleIDByChild());
				}

				if (sysSubModuleResponseTO.getParentModuleIDByName() != null
						&& !sysSubModuleResponseTO.getParentModuleIDByName()
								.isEmpty())
					sysPlaceHolderBean
							.setParentModuleIDByName(sysSubModuleResponseTO
									.getParentModuleIDByName());

				/**
				 * all modules and submodules are listed under one map along
				 * with their IDs
				 * */
				if (sysSubModuleResponseTO.getAllModuleIDByName() != null
						&& !sysSubModuleResponseTO.getAllModuleIDByName()
								.isEmpty())
					sysPlaceHolderBean
							.setAllModuleIDByName(sysSubModuleResponseTO
									.getAllModuleIDByName());
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

	public static String findDataType(Object obj) {
		String className = "";
		if (obj.getClass().getSimpleName().equalsIgnoreCase("Integer")) {
			className = obj.getClass().getSimpleName();
		} else if (obj.getClass().getSimpleName().equalsIgnoreCase("String")) {
			className = obj.getClass().getSimpleName();
		} else if (obj.getClass().getSimpleName().equalsIgnoreCase("Long")) {
			className = obj.getClass().getSimpleName();
		} else if (obj.getClass().getSimpleName().equalsIgnoreCase("Boolean")) {
			className = obj.getClass().getSimpleName();
		}

		return className;
	}

	/**
	 * {@link #findAndSetSubModuleID()} : finds sub-modules associated with the
	 * selected module & returns the sub-module ID
	 * 
	 * @author vaibhav.kashyap
	 * @return Integer
	 * @throws Exception
	 * */
	public int findAndSetSubModuleID() throws Exception {
		int subModuleID = 0;
		Map<Integer, List<SysSubModuleResponseTO>> tempMap = sysPlaceHolderBean
				.getParentModuleIDByChild();
		if (tempMap != null && !tempMap.isEmpty()) {
			/**
			 * module ID here is parent module ID and we're looking for
			 * sub-module ID
			 * */
			if (sysPlaceHolderBean.getModuleID() > 0) {
				List<SysSubModuleResponseTO> responseList = sysPlaceHolderBean
						.getParentModuleIDByChild().get(
								sysPlaceHolderBean.getModuleID());
				if (responseList != null && !responseList.isEmpty()) {
					for (SysSubModuleResponseTO obj : responseList) {
						if (obj.getModuleName().equalsIgnoreCase(
								sysPlaceHolderBean.getSubModuleName()))
							subModuleID = obj.getModuleId();
					}
				}
			}
		}

		return subModuleID;
	}

	/**
	 * @param parentModuleIDByChild
	 */
	private void prepareSubModuleDataForRendering(
			Map<Integer, List<SysSubModuleResponseTO>> parentModuleIDByChild) {
		Map<Integer, List<String>> childModuleIDByName = new HashMap<Integer, List<String>>();
		List<String> nameOfChildValues = null;
		for (Integer key : parentModuleIDByChild.keySet()) {
			List<SysSubModuleResponseTO> tempResponseTOList = parentModuleIDByChild
					.get(key);
			nameOfChildValues = new ArrayList<String>();
			if (tempResponseTOList != null && !tempResponseTOList.isEmpty()) {

				for (SysSubModuleResponseTO tempObj : tempResponseTOList) {
					if (tempObj.getModuleName() != null
							&& !tempObj.getModuleName().equals(""))
						nameOfChildValues.add(new String(tempObj
								.getModuleName()));
				}

				if (!nameOfChildValues.isEmpty())
					childModuleIDByName.put(key, nameOfChildValues);
			}
		}

		if (!childModuleIDByName.isEmpty()) {
			sysPlaceHolderBean.setChildModuleIDByName(childModuleIDByName);
		}
	}

	/**
	 * {@link #findAndSetParentModuleIDForRendering(String)} : find & sets parent module ID
	 * through associated sub-module ID. Associated sub-module lists have to be traversed 
	 * for finding out the parent module ID.
	 * @param subModuleName
	 * @return Void
	 * @throws Exception
	 * @author vaibhav.kashyap
	 * */
	public void findAndSetParentModuleIDForRendering(String subModuleName)
			throws Exception {
		Map<Integer, List<SysSubModuleResponseTO>> mapToFetchParentID = null;
		try {
			mapToFetchParentID = sysPlaceHolderBean.getParentModuleIDByChild();

			if (mapToFetchParentID == null || mapToFetchParentID.isEmpty()
					|| subModuleName == null || subModuleName.equals(""))
				/**
				 * parent module ID not found therefore returning null
				 * */
				return;

			for (Integer parentModuleID : mapToFetchParentID.keySet()) {
				List<SysSubModuleResponseTO> sysSubModuleResponseList = mapToFetchParentID
						.get(parentModuleID);
				if (sysSubModuleResponseList != null
						&& !sysSubModuleResponseList.isEmpty()) {
					for (SysSubModuleResponseTO tempResponseObj : sysSubModuleResponseList) {
						if (tempResponseObj.getModuleName().equalsIgnoreCase(
								subModuleName)) {
							/**
							 * Now this is the parent module id for which
							 * sub-module is selected
							 * */
							sysPlaceHolderBean.setModuleID(parentModuleID);
							return;
						}
					}
				}
			}

		} catch (Exception ex) {

		}
	}

	public void onModuleSelection() throws Exception {

		int key = sysPlaceHolderBean.getModuleID();

		if (key > 0) {

			if (!sysPlaceHolderBean.getChildModuleIDByName().isEmpty()
					&& sysPlaceHolderBean.getChildModuleIDByName().containsKey(
							new Integer(key))) {
				sysPlaceHolderBean.setDisableSubModuleSelection(false);
				sysPlaceHolderBean.setSubModulesNamesList(sysPlaceHolderBean
						.getChildModuleIDByName().get(key));
			} else {
				sysPlaceHolderBean.setDisableSubModuleSelection(true);
				sysPlaceHolderBean.setSubModuleID(key);
				sysPlaceHolderBean.setSubModuleName(sysPlaceHolderBean
						.getParentModuleIDByName().get(new Integer(key)));
			}

		}
	}

	/**
	 * {@link #onSubModuleSelection()} : called on sub-module selection, from
	 * the update placeholder dialog box
	 * 
	 * @author vaibhav.kashyap
	 * @throws Exception
	 * */
	public void onSubModuleSelection() throws Exception {
		int subModuleID = findAndSetSubModuleID();
		if (subModuleID > 0) {
			sysPlaceHolderBean.setSubModuleID(subModuleID);
		}

	}

	/**
	 * {@link #getAllRules()} : get all rules from SysRule
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void getAllRules() throws Exception {
		ServiceResponse serviceResponse = null;
		try {
			serviceResponse = sysRuleLocalFacade.getAllRules();
			List<RuleResponseRevisedTO> list = null;
			if (serviceResponse != null) {
				list = (List<RuleResponseRevisedTO>) serviceResponse
						.getResponseTransferObjectObjectList();
				if (list != null) {
					sysPlaceHolderBean.setSysRuleResponseTOList(list);
				}
			}
		} catch (Exception ex) {
			throw ex;
		}

	}

	/**
	 * {@link #getHrContentTypeByContentCategory()} : gets list of place holder
	 * type
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void getHrContentTypeByContentCategory() throws Exception {
		ServiceResponse serviceResponse = null;
		List<HrContentTypeResponseTO> list = null;
		try {
			serviceResponse = hrContentTypeLocalFacade
					.getHrContentTypeByContentCategory(new ServiceRequest());
			if (serviceResponse != null) {
				list = (List<HrContentTypeResponseTO>) serviceResponse
						.getResponseTransferObjectObjectList();
				if (list != null) {
					sysPlaceHolderBean.setHrContentTypeResponseTOList(list);
				}
			}
		} catch (Exception ex) {
			throw ex;
		}

	}

	public boolean checkPlaceHolderSignature(String placeHolderName)
			throws Exception {
		boolean flag = false;
		char[] placeHolderCharArray = null;
		Integer[] signatureArray = new Integer[4];
		try {
			if (placeHolderName != null && !placeHolderName.equals("")) {
				placeHolderCharArray = placeHolderName.toCharArray();
				signatureArray[0] = (int) placeHolderCharArray[0];
				signatureArray[1] = (int) placeHolderCharArray[1];
				signatureArray[2] = (int) placeHolderCharArray[placeHolderCharArray.length - 1];
				signatureArray[3] = (int) placeHolderCharArray[placeHolderCharArray.length - 2];

				for (int i = 0; i < signatureArray.length; i++) {
					if (signatureArray[i] != 64) {
						flag = true;
						break;
					}
				}

			}
		} catch (Exception ex) {
			throw ex;
		}
		return flag;
	}

	/**
	 * {@link #checkIfAlreadyExists(String)} : method prevents user from
	 * entering place holder if it already exists
	 * 
	 * @return Boolean
	 * @param placeHolderName
	 * @author vaibhav.kashyap
	 * */
	public boolean checkIfAlreadyExists(String placeHolderName)
			throws Exception {

		/**
		 * return negative if input value found null
		 * */
		if (placeHolderName == null || placeHolderName.equals("")) {
			return false;
		}

		if (checkPlaceHolderSignature(placeHolderName)) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Place holder name starts and ends with @@", ""));

			return false;
		}

		boolean flag = true;
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		try {
			sysPlaceHolderResponseTOList = sysPlaceHolderBean
					.getSysPlaceHolderResponseTOList();
			if (sysPlaceHolderResponseTOList != null
					&& !sysPlaceHolderResponseTOList.isEmpty()) {
				/**
				 * returns false if same place holder already exists
				 * */
				for (SysPlaceHolderResponseTO placeHolderObj : sysPlaceHolderResponseTOList) {
					if (placeHolderObj.getPlaceHolder().equalsIgnoreCase(
							placeHolderName)) {
						flag = false;
						break;
					}
				}

				if (!flag) {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"Place holder already exists", ""));
					return false;
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}

		return flag;
	}

	/**
	 * {@link #updateExistingPlaceHolder(ServiceRequest)} : updates existing
	 * place holder
	 * 
	 * @return {@link ServiceResponse}
	 * @throws Exception
	 * @param serviceRequest
	 * @author vaibhav.kashyap
	 * */
	public ServiceResponse updateExistingPlaceHolder(
			ServiceRequest serviceRequest) throws Exception {
		ServiceResponse serviceResponse = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		try {
			serviceResponse = sysPlaceHolderLocalFacade
					.updateNewPlaceHolder(serviceRequest);
			if (serviceResponse != null) {
				sysPlaceHolderResponseTO = (SysPlaceHolderResponseTO) serviceResponse
						.getResponseTransferObject();
				if (sysPlaceHolderResponseTO != null)
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Place holder updated successfully", ""));
				sysPlaceHolderBean.setSysplaceholderFormMessagesFlag(false);
				updatePlaceHolderLocalData(sysPlaceHolderResponseTO, 3);
				RequestContext.getCurrentInstance().execute(
						"PF('sysPLaceHolderAddModal').hide();");
			}
		} catch (Exception ex) {
			serviceResponse = null;
			throw ex;
		}

		return serviceResponse;
	}

	/**
	 * {@link #insertNewPlaceHolder()} : inserts or updates new place holder
	 * 
	 * @throws Exception
	 * @return Void
	 * */
	public void insertNewPlaceHolder() throws Exception {
		/**
		 * In order to re-use the same form for inserting new place holder and
		 * updating any existing place holder , command is being prepared below
		 * before taking action on basis of text on button.
		 * */
		int command = -1;
		if (sysPlaceHolderBean.getInsertNewPlaceHolderCommandButtonValueText() != null
				&& !sysPlaceHolderBean
						.getInsertNewPlaceHolderCommandButtonValueText()
						.equals("")) {
			/**
			 * command 1 : saving new
			 * */
			if (sysPlaceHolderBean
					.getInsertNewPlaceHolderCommandButtonValueText()
					.equalsIgnoreCase("save")) {
				command = 1;
			} else if (sysPlaceHolderBean
					.getInsertNewPlaceHolderCommandButtonValueText()
					.equalsIgnoreCase("update")) {
				/**
				 * command 2 : update existing
				 * */
				command = 2;
			} else {
				return;
			}
		}

		String placeHolderName = "";
		SysPlaceHolderRequestTO sysPlaceHolderRequestTO = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		try {
			if (command > 0) {
				switch (command) {
				case 1:
					placeHolderName = sysPlaceHolderBean.getPlaceHolderName();

					/**
					 * first, check if place holder being inserted already
					 * exists
					 * */
					if (checkIfAlreadyExists(placeHolderName)) {
						sysPlaceHolderRequestTO = new SysPlaceHolderRequestTO();
						sysPlaceHolderRequestTO
								.setPlaceHolderName(sysPlaceHolderBean
										.getPlaceHolderName());
						sysPlaceHolderRequestTO
								.setPlaceHolderDescription(sysPlaceHolderBean
										.getPlaceHolderDescription());
						sysPlaceHolderRequestTO
								.setColumnExpression(sysPlaceHolderBean
										.getColumnExpression());
						sysPlaceHolderRequestTO
								.setWhereCondition(sysPlaceHolderBean
										.getWhereCondition());
						sysPlaceHolderRequestTO
								.setPlaceHolderTypeID(new Integer(
										sysPlaceHolderBean
												.getPlaceHolderTypeID()));
						/*
						 * sysPlaceHolderRequestTO.setCommTypeID(new Integer(
						 * sysPlaceHolderBean.getCommTypeID()));
						 */
						sysPlaceHolderRequestTO.setModuleID(sysPlaceHolderBean
								.getSubModuleID());
						sysPlaceHolderRequestTO.setLevelType(new Integer(
								sysPlaceHolderBean.getLevelType()));
						sysPlaceHolderRequestTO.setComplexityType(new Integer(
								sysPlaceHolderBean.getComplexityType()));
						sysPlaceHolderRequestTO.setRuleID(sysPlaceHolderBean
								.getRuleID());
						sysPlaceHolderRequestTO.setEntityColumnID(new Long(
								sysPlaceHolderBean.getEntityColumnID()));
						sysPlaceHolderRequestTO
								.setEntityPath(sysPlaceHolderBean
										.getEntityPath());
						sysPlaceHolderRequestTO.setTenantID(new Integer(1));
						sysPlaceHolderRequestTO.setModifiedBy(new Integer(1));
						sysPlaceHolderRequestTO.setCreatedBy(new Integer(1));
						sysPlaceHolderRequestTO.setCreatedDate(new Date());
						sysPlaceHolderRequestTO.setModifiedDate(new Date());

						serviceRequest = new ServiceRequest();
						serviceRequest
								.setRequestTransferObjectRevised(sysPlaceHolderRequestTO);
						serviceResponse = sysPlaceHolderLocalFacade
								.insertNewPlaceHolder(serviceRequest);
						if (serviceResponse != null) {
							sysPlaceHolderResponseTO = (SysPlaceHolderResponseTO) serviceResponse
									.getResponseTransferObject();
							if (sysPlaceHolderResponseTO != null)
								FacesContext
										.getCurrentInstance()
										.addMessage(
												null,
												new FacesMessage(
														FacesMessage.SEVERITY_INFO,
														"Place holder created successfully",
														""));
							sysPlaceHolderBean
									.setSysplaceholderFormMessagesFlag(false);
							updatePlaceHolderLocalData(
									sysPlaceHolderResponseTO, 1);
							RequestContext.getCurrentInstance().execute(
									"PF('sysPLaceHolderAddModal').hide();");
						}
					}

					break;
				case 2:
					sysPlaceHolderRequestTO = new SysPlaceHolderRequestTO();
					sysPlaceHolderRequestTO.setPlaceHolderID(sysPlaceHolderBean
							.getSysPlaceHolderIDForUpdation());
					sysPlaceHolderRequestTO
							.setPlaceHolderName(sysPlaceHolderBean
									.getPlaceHolderName());
					sysPlaceHolderRequestTO
							.setPlaceHolderDescription(sysPlaceHolderBean
									.getPlaceHolderDescription());
					sysPlaceHolderRequestTO
							.setColumnExpression(sysPlaceHolderBean
									.getColumnExpression());
					sysPlaceHolderRequestTO
							.setWhereCondition(sysPlaceHolderBean
									.getWhereCondition());
					sysPlaceHolderRequestTO.setPlaceHolderTypeID(new Integer(
							sysPlaceHolderBean.getPlaceHolderTypeID()));
					/*
					 * sysPlaceHolderRequestTO.setCommTypeID(new Integer(
					 * sysPlaceHolderBean.getCommTypeID()));
					 */
					sysPlaceHolderRequestTO.setModuleID(sysPlaceHolderBean
							.getSubModuleID());
					sysPlaceHolderRequestTO.setLevelType(new Integer(
							sysPlaceHolderBean.getLevelType()));
					sysPlaceHolderRequestTO.setComplexityType(new Integer(
							sysPlaceHolderBean.getComplexityType()));
					sysPlaceHolderRequestTO.setRuleID(sysPlaceHolderBean
							.getRuleID());
					sysPlaceHolderRequestTO.setEntityColumnID(new Long(
							sysPlaceHolderBean.getEntityColumnID()));
					sysPlaceHolderRequestTO.setEntityPath(sysPlaceHolderBean
							.getEntityPath());
					sysPlaceHolderRequestTO.setTenantID(new Integer(1));
					sysPlaceHolderRequestTO.setModifiedBy(new Integer(1));
					sysPlaceHolderRequestTO.setCreatedBy(new Integer(1));
					sysPlaceHolderRequestTO.setCreatedDate(new Date());
					sysPlaceHolderRequestTO.setModifiedDate(new Date());

					serviceRequest = new ServiceRequest();
					serviceRequest
							.setRequestTransferObjectRevised(sysPlaceHolderRequestTO);
					updateExistingPlaceHolder(serviceRequest);

					break;
				default:
					break;
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}
	}

	/**
	 * {@link #resetFields()} : reset fields after save and cancel
	 * 
	 * @author vaibhav.kashyap
	 * 
	 * */
	public void resetFields() {

		sysPlaceHolderBean.setPlaceHolderName(null);
		sysPlaceHolderBean.setPlaceHolderDescription(null);
		sysPlaceHolderBean.setColumnExpression(null);
		sysPlaceHolderBean.setWhereCondition(null);
		sysPlaceHolderBean.setPlaceHolderTypeID(0);
		sysPlaceHolderBean.setCommTypeID(0);
		sysPlaceHolderBean.setLevelType(0);
		sysPlaceHolderBean.setComplexityType(0);
		sysPlaceHolderBean.setRuleID(0);
		sysPlaceHolderBean.setEntityPath(null);
		sysPlaceHolderBean.setTenantID(new Integer(0));
		sysPlaceHolderBean.setModuleID(0);
		sysPlaceHolderBean.setSubModuleID(0);
		sysPlaceHolderBean.setSubModuleName("");
		sysPlaceHolderBean.setDisableSubModuleSelection(true);
		sysPlaceHolderBean.setEntityColumnID(0);
	}

	/**
	 * {@link #getLocalPlaceHolderByID(Long)} : returns SysPlaceHolder Response
	 * TO fetched on the basis of SysPlaceHolder ID
	 * 
	 * @return {@link SysSubModuleResponseTO}
	 * @param placeHolderID
	 * @throws Exception
	 * @author vaibhav.kashyap
	 * */
	public SysPlaceHolderResponseTO getLocalPlaceHolderByID(Long placeHolderID)
			throws Exception {
		SysPlaceHolderResponseTO sysPlaceHolderResponseTO = null;
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		try {
			sysPlaceHolderResponseTOList = sysPlaceHolderBean
					.getSysPlaceHolderResponseTOList();
			if (sysPlaceHolderResponseTOList != null
					&& !sysPlaceHolderResponseTOList.isEmpty()) {
				for (SysPlaceHolderResponseTO tempObj : sysPlaceHolderResponseTOList) {
					if (tempObj.getSysPlaceHolderID().longValue() == placeHolderID
							.longValue()) {
						sysPlaceHolderResponseTO = new SysPlaceHolderResponseTO();
						sysPlaceHolderResponseTO = tempObj;
						break;
					}
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}

		return sysPlaceHolderResponseTO;
	}

	/**
	 * {@link #updatePlaceHolderLocalData(SysPlaceHolderResponseTO)} : called on
	 * adding new or deleting or updating any existing place holder. <br>
	 * commands : <br>
	 * 1 - when any new place holder is added <br>
	 * 2 - when any existing is deleted <br>
	 * 3 - When any existing is updated
	 * 
	 * @param SysPlaceHolderResponseTO
	 * @return Void
	 * @throws Exception
	 * */
	public void updatePlaceHolderLocalData(
			SysPlaceHolderResponseTO sysPlaceHolderResponseTO, int command)
			throws Exception {
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		try {
			sysPlaceHolderResponseTOList = sysPlaceHolderBean
					.getSysPlaceHolderResponseTOList();
			if (sysPlaceHolderResponseTOList == null) {
				return;
			}

			switch (command) {
			case 1:
				/**
				 * when any new place holder is added
				 * */
				if (!sysPlaceHolderResponseTOList
						.contains(sysPlaceHolderResponseTO))
					sysPlaceHolderResponseTOList.add(sysPlaceHolderResponseTO);

				resetFields();
				sysPlaceHolderBean
						.setInsertNewPlaceHolderCommandButtonValueText("Save");
				sysPlaceHolderBean
						.setSysPLaceHolderAddHeader("NEW PLACE HOLDER");
				break;
			case 2:
				/**
				 * when any existing is deleted
				 * */

				if (!sysPlaceHolderResponseTOList.isEmpty()
						&& sysPlaceHolderResponseTOList
								.contains(sysPlaceHolderResponseTO))
					sysPlaceHolderResponseTOList
							.remove(sysPlaceHolderResponseTO);
					sysPlaceHolderBean.getFilteredSysPlaceHolderResponseTOList().remove(sysPlaceHolderResponseTO);
				break;
			case 3:
				/**
				 * when any existing is updated
				 * */
				List<SysPlaceHolderResponseTO> tempArrayList = null;
				if (!sysPlaceHolderResponseTOList.isEmpty()) {
					tempArrayList = new CopyOnWriteArrayList<SysPlaceHolderResponseTO>();
					for (SysPlaceHolderResponseTO obj : sysPlaceHolderResponseTOList) {
						if (obj.getSysPlaceHolderID().longValue() == sysPlaceHolderResponseTO
								.getSysPlaceHolderID().longValue()) {
							/**
							 * mark old object for deletion
							 * */
							obj.setMarkForDeletion(true);
						}
						tempArrayList.add(obj);
					}

					/**
					 * deep copy for deletion and adding updated placeholder
					 * object
					 * */
					for (SysPlaceHolderResponseTO obj : tempArrayList) {
						if (obj.getMarkForDeletion()) {
							tempArrayList.remove(obj);
							tempArrayList.add(sysPlaceHolderResponseTO);
							break;
						}
					}

					sysPlaceHolderBean
							.setSysPlaceHolderResponseTOList(tempArrayList);

					/**
					 * setting default button value and form header
					 * */
					sysPlaceHolderBean
							.setInsertNewPlaceHolderCommandButtonValueText("Save");
					sysPlaceHolderBean
							.setSysPLaceHolderAddHeader("NEW PLACE HOLDER");

					resetFields();
				}

				break;
			default:
				/**
				 * default logic
				 * */
				break;
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}
	}

	/**
	 * {@link #deletePlaceHolderByID(Long)} : deletes place holder by id
	 * 
	 * @param placeHolderID
	 * @return Void
	 * @author vaibhav.kashyap
	 * @throws Exception
	 * */
	public void deletePlaceHolderByID(Long placeHolderID) throws Exception {
		SysPlaceHolderRequestTO sysPlaceHolderRequestTO = null;
		ServiceRequest serviceRequest = null;
		ServiceResponse serviceResponse = null;
		try {
			if (placeHolderID != null && placeHolderID > 0) {
				sysPlaceHolderRequestTO = new SysPlaceHolderRequestTO();
				sysPlaceHolderRequestTO.setPlaceHolderID(placeHolderID);

				serviceRequest = new ServiceRequest();
				serviceRequest
						.setRequestTransferObjectRevised(sysPlaceHolderRequestTO);
				serviceResponse = sysPlaceHolderLocalFacade
						.deletePlaceHolderByID(serviceRequest);
				if (serviceResponse != null) {
					updatePlaceHolderLocalData(
							getLocalPlaceHolderByID(placeHolderID), 2);
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Place holder deleted successfully", ""));
					sysPlaceHolderBean.setSysplaceholderFormMessagesFlag(false);
					/**
					 * write logic if required
					 * */
				} else {
					FacesContext
							.getCurrentInstance()
							.addMessage(
									null,
									new FacesMessage(
											FacesMessage.SEVERITY_FATAL,
											"Conflict occurred with the reference constraint in database",
											""));
					sysPlaceHolderBean.setSysplaceholderFormMessagesFlag(false);

				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {

		}
	}

	/**
	 * {@link #openFormForNewPlaceHolder()} : called whenever form is opened a)
	 * sets up form header b) value for button c) resets all field values
	 * 
	 * @author vaibhav.kashyap
	 * */
	public void openFormForNewPlaceHolder() {
		sysPlaceHolderBean.setSysPLaceHolderAddHeader("NEW PLACE HOLDER");
		sysPlaceHolderBean
				.setInsertNewPlaceHolderCommandButtonValueText("Save");
		resetFields();
	}

	public void markPlaceHolderForDeletion(long placeHolderForDeletion) {
		if (placeHolderForDeletion > 0)
			sysPlaceHolderBean
					.setSysPlaceHolderIDForDeletion(placeHolderForDeletion);
	}

	/**
	 * {@link #markPlaceHolderForUpdation(long)} : updates place holder
	 * 
	 * @param placeHolderForUpdation
	 * @return Void
	 * @throws Exception
	 * @author vaibhav.kashyap
	 * */
	public void markPlaceHolderForUpdation(long placeHolderForUpdation)
			throws Exception {
		List<SysPlaceHolderResponseTO> sysPlaceHolderResponseTOList = null;
		SysPlaceHolderResponseTO sysPlaceHolderResponseTOObj = null;
		if (placeHolderForUpdation > 0) {
			resetFields();

			sysPlaceHolderBean.setDisableSubModuleSelection(false);
			sysPlaceHolderResponseTOObj = getLocalPlaceHolderByID(placeHolderForUpdation);

			if (sysPlaceHolderResponseTOObj != null) {

				/**
				 * update form's button value and header
				 * */
				sysPlaceHolderBean
						.setInsertNewPlaceHolderCommandButtonValueText("Update");
				sysPlaceHolderBean
						.setSysPLaceHolderAddHeader("UPDATE PLACE HOLDER");
				sysPlaceHolderBean
						.setSysPlaceHolderIDForUpdation(placeHolderForUpdation);

				/**
				 * set values for updation
				 * */
				if (sysPlaceHolderResponseTOObj.getPlaceHolder() != null
						&& !sysPlaceHolderResponseTOObj.getPlaceHolder()
								.equals(""))
					sysPlaceHolderBean
							.setPlaceHolderName(sysPlaceHolderResponseTOObj
									.getPlaceHolder());

				if (sysPlaceHolderResponseTOObj.getPlaceholderDesc() != null
						&& !sysPlaceHolderResponseTOObj.getPlaceholderDesc()
								.equals(""))
					sysPlaceHolderBean
							.setPlaceHolderDescription(sysPlaceHolderResponseTOObj
									.getPlaceholderDesc());

				if (sysPlaceHolderResponseTOObj.getColumnExpression() != null
						&& !sysPlaceHolderResponseTOObj.getColumnExpression()
								.equals(""))
					sysPlaceHolderBean
							.setColumnExpression(sysPlaceHolderResponseTOObj
									.getColumnExpression());

				if (sysPlaceHolderResponseTOObj.getWhereCondition() != null
						&& !sysPlaceHolderResponseTOObj.getWhereCondition()
								.equals(""))
					sysPlaceHolderBean
							.setWhereCondition(sysPlaceHolderResponseTOObj
									.getWhereCondition());

				if (sysPlaceHolderResponseTOObj.getPlaceholderTypeID() != null
						&& sysPlaceHolderResponseTOObj.getPlaceholderTypeID() > 0)
					sysPlaceHolderBean
							.setPlaceHolderTypeID(sysPlaceHolderResponseTOObj
									.getPlaceholderTypeID());

				/*
				 * if (sysPlaceHolderResponseTOObj.getCommTypeID() != null &&
				 * sysPlaceHolderResponseTOObj.getCommTypeID() > 0)
				 * sysPlaceHolderBean .setCommTypeID(sysPlaceHolderResponseTOObj
				 * .getCommTypeID());
				 */
				if (sysPlaceHolderResponseTOObj.getModuleID() != null
						&& sysPlaceHolderResponseTOObj.getModuleID() > 0) {
					String subModuleName = sysPlaceHolderBean
							.getAllModuleIDByName().get(
									sysPlaceHolderResponseTOObj.getModuleID());
					sysPlaceHolderBean.setSubModuleName(new String(
							subModuleName));

					findAndSetParentModuleIDForRendering(subModuleName);
				}

				if (sysPlaceHolderResponseTOObj.getLevelType() != null
						&& sysPlaceHolderResponseTOObj.getLevelType() > 0)
					sysPlaceHolderBean.setLevelType(sysPlaceHolderResponseTOObj
							.getLevelType());

				if (sysPlaceHolderResponseTOObj.getComplexityType() != null
						&& sysPlaceHolderResponseTOObj.getComplexityType() > 0)
					sysPlaceHolderBean
							.setComplexityType(sysPlaceHolderResponseTOObj
									.getComplexityType());

				if (sysPlaceHolderResponseTOObj.getRuleID() != null
						&& sysPlaceHolderResponseTOObj.getRuleID() > 0)
					sysPlaceHolderBean.setRuleID(sysPlaceHolderResponseTOObj
							.getRuleID());

				if (sysPlaceHolderResponseTOObj.getEntityPath() != null
						&& !sysPlaceHolderResponseTOObj.getEntityPath().equals(
								""))
					sysPlaceHolderBean
							.setEntityPath(sysPlaceHolderResponseTOObj
									.getEntityPath());

				if (sysPlaceHolderResponseTOObj.getTenantID() != null
						&& sysPlaceHolderResponseTOObj.getTenantID() > 0)
					sysPlaceHolderBean.setTenantID(sysPlaceHolderResponseTOObj
							.getTenantID());

			}

		}

	}
}
