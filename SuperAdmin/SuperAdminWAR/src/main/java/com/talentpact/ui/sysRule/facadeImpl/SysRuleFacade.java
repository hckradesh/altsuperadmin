/**
 * 
 */
package com.talentpact.ui.sysRule.facadeImpl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysRule.dataprovider.SysRuleDataProvider;
import com.talentpact.ui.sysRule.facaderemote.SysRuleLocalFacade;

/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysRuleFacade implements SysRuleLocalFacade{

	@EJB
	private SysRuleDataProvider sysRuleDataProvider;
	/* (non-Javadoc)
	 * @see com.talentpact.ui.sysRule.facaderemote.SysRuleLocalFacade#getAllRules()
	 */
	@Override
	public ServiceResponse getAllRules() throws Exception {
		return sysRuleDataProvider.getAllRules();
	}

}
