package com.talentpact.ui.dataupload.constants;

public enum MessageLevel {
    ERROR, FATAL, INFO, WARN;
}
