package com.talentpact.ui.uiform.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import com.talentpact.business.application.transport.input.UIFormRequestTO;
import com.talentpact.business.application.transport.output.UiFormFieldResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("uploadFieldsBean")
@SessionScoped
public class UploadFieldsBean  extends CommonBean implements Serializable,  IDefaultAction
{
    private Integer formId;

    private List<UiFormFieldResponseTO> listToFromExcel ;
    private List<UiFormFieldResponseTO> masterFormDataList;
    private UiFormFieldResponseTO copyMasterFormDataList;
    private String uploadedFileName;
    private UploadedFile uploadedFile;
    private List<String> excelHeaderList;
    private List<String> summaryList;
    private boolean success;
    
    @Override
    public void initialize()
    {
        // TODO Auto-generated method stub
        
    }


    public Integer getFormId()
    {
        return formId;
    }


    public void setFormId(Integer formId)
    {
        this.formId = formId;
    }


    public List<UiFormFieldResponseTO> getListToFromExcel()
    {
        return listToFromExcel;
    }


    public void setListToFromExcel(List<UiFormFieldResponseTO> listToFromExcel)
    {
        this.listToFromExcel = listToFromExcel;
    }


    public List<UiFormFieldResponseTO> getMasterFormDataList()
    {
        return masterFormDataList;
    }


    public void setMasterFormDataList(List<UiFormFieldResponseTO> masterFormDataList)
    {
        this.masterFormDataList = masterFormDataList;
    }


    public String getUploadedFileName()
    {
        return uploadedFileName;
    }


    public void setUploadedFileName(String uploadedFileName)
    {
        this.uploadedFileName = uploadedFileName;
    }


    public UploadedFile getUploadedFile()
    {
        return uploadedFile;
    }


    public void setUploadedFile(UploadedFile uploadedFile)
    {
        this.uploadedFile = uploadedFile;
    }


    public List<String> getExcelHeaderList()
    {
        return excelHeaderList;
    }


    public void setExcelHeaderList(List<String> excelHeaderList)
    {
        this.excelHeaderList = excelHeaderList;
    }


    public List<String> getSummaryList()
    {
        return summaryList;
    }


    public void setSummaryList(List<String> summaryList)
    {
        this.summaryList = summaryList;
    }


    public UiFormFieldResponseTO getCopyMasterFormDataList()
    {
        return copyMasterFormDataList;
    }


    public void setCopyMasterFormDataList(UiFormFieldResponseTO copyMasterFormDataList)
    {
        this.copyMasterFormDataList = copyMasterFormDataList;
    }


    public boolean isSuccess()
    {
        return success;
    }


    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    

}
