/**
 * 
 */
package com.talentpact.ui.menu.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;




import org.primefaces.model.menu.MenuModel;

import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.controller.MenuAction;
import com.talentpact.ui.menu.transport.MenuItemTO;

/**
 * @author radhamadhab.dalai
 *
 */
@Named("menuBean")
@SessionScoped
public class MenuBean implements Serializable, IDefaultAction
{
    @Inject
    MenuAction menuAction;

    private MenuModel model;

    private MenuModel menuBarmodel;

    private boolean showLeftMenu = false;

    private long topMenuID;

    private String page;

    private boolean showLeftLinks;

    private List<MenuItemTO> leftMenuItemTOLIst;

    private String testmessage;
    
    private String adhocReportPage;

    /**
     * @return the leftMenuItemTOLIst
     */
    public List<MenuItemTO> getLeftMenuItemTOLIst()
    {
        return leftMenuItemTOLIst;
    }

    /**
     * @param leftMenuItemTOLIst the leftMenuItemTOLIst to set
     */
    public void setLeftMenuItemTOLIst(List<MenuItemTO> leftMenuItemTOLIst)
    {
        this.leftMenuItemTOLIst = leftMenuItemTOLIst;
    }

    /**
     * @return the model
     */
    public MenuModel getModel()
    {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(MenuModel model)
    {
        this.model = model;
    }

    /**
     * @return the menuBarmodel
     */
    public MenuModel getMenuBarmodel()
    {
        return menuBarmodel;
    }

    /**
     * @param menuBarmodel the menuBarmodel to set
     */
    public void setMenuBarmodel(MenuModel menuBarmodel)
    {
        this.menuBarmodel = menuBarmodel;
    }

    /**
     * @return the showLeftMenu
     */
    public boolean isShowLeftMenu()
    {
        return showLeftMenu;
    }

    /**
     * @param showLeftMenu the showLeftMenu to set
     */
    public void setShowLeftMenu(boolean showLeftMenu)
    {
        this.showLeftMenu = showLeftMenu;
    }

    /**
     * @return the topMenuID
     */
    public long getTopMenuID()
    {
        return topMenuID;
    }

    /**
     * @param topMenuID the topMenuID to set
     */
    public void setTopMenuID(long topMenuID)
    {
        this.topMenuID = topMenuID;
    }

    /**
     * @return the page
     */
    public String getPage()
    {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(String page)
    {
        this.page = page;
    }

    /**
     * @return the showLeftLinks
     */
    public boolean isShowLeftLinks()
    {
        return showLeftLinks;
    }

    /**
     * @param showLeftLinks the showLeftLinks to set
     */
    public void setShowLeftLinks(boolean showLeftLinks)
    {
        this.showLeftLinks = showLeftLinks;
    }


    public void initialize()
    {
        try {

            menuAction.initialize();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the testmessage
     */
    public String getTestmessage()
    {
        return testmessage;
    }

    /**
     * @param testmessage the testmessage to set
     */
    public void setTestmessage(String testmessage)
    {
        this.testmessage = testmessage;
    }

	public String getAdhocReportPage() {
		return adhocReportPage;
	}

	public void setAdhocReportPage(String adhocReportPage) {
		this.adhocReportPage = adhocReportPage;
	}

}
