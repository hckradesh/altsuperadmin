package com.talentpact.ui.sysModule.exception;

public class SysModuleException extends Exception {

private static final long serialVersionUID = 1L;
    
    private String message = null;

    public SysModuleException()
    {
        super();
    }

    public SysModuleException(String message)
    {
        super(message);
        this.message = message;
    }
    
    public SysModuleException(String message, Exception e) {
        super(message, e);
    }

    public SysModuleException(Throwable cause)
    {
        super(cause);
    }

    @Override
    public String toString()
    {
        return message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
