package com.talentpact.ui.dashboard.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.tpModule.transport.output.TpModuleResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.dashboard.controller.DashBoardController;
import com.talentpact.ui.dashboard.transport.output.DashBoardTO;
import com.talentpact.ui.dashboard.transport.output.ModuleTO;


@Named(value = "dashBoardBean")
@ConversationScoped
public class DashBoardBean extends CommonBean implements Serializable, IDefaultAction
{
	
	private static final long serialVersionUID = 4761428921664579077L;
	
	@Inject
    DashBoardController dashBoardController;
	
	
    @Override
    public void initialize()
    {
        endConversation();
        beginConversation();
        dashBoardController.initialize();
    }
    
   private  List<DashBoardTO> dashBoardTOList ;
   private  List<DashBoardTO> filtedredDashBoardTOList ;
   private  static final Map<Integer , String> columnPositionMap;
   private DashBoardTO dashBoardTO;
   static
   {
	   columnPositionMap = new HashMap<Integer, String>();
	   columnPositionMap.put(1, "Left");
	   columnPositionMap.put(2, "Middle");
	   columnPositionMap.put(3, "Right");
       
   }
    private List<ModuleTO> moduleList ;
    private Map<Long , ModuleTO > moduleMap ;
    private List<ModuleTO> parentModuleList ;
    private List<ModuleTO> allChildModuleList ;
    private boolean saveFlag ;

	public List<DashBoardTO> getDashBoardTOList() {
		return dashBoardTOList;
	}

	public void setDashBoardTOList(List<DashBoardTO> dashBoardTOList) {
		this.dashBoardTOList = dashBoardTOList;
	}

	public List<DashBoardTO> getFiltedredDashBoardTOList() {
		return filtedredDashBoardTOList;
	}

	public void setFiltedredDashBoardTOList(
			List<DashBoardTO> filtedredDashBoardTOList) {
		this.filtedredDashBoardTOList = filtedredDashBoardTOList;
	}
	public DashBoardTO getDashBoardTO() {
		return dashBoardTO;
	}

	public void setDashBoardTO(DashBoardTO dashBoardTO) {
		this.dashBoardTO = dashBoardTO;
	}

	public List<ModuleTO> getModuleList() {
		return moduleList;
	}

	public void setModuleList(List<ModuleTO> moduleList) {
		this.moduleList = moduleList;
	}

	public Map<Long, ModuleTO> getModuleMap() {
		return moduleMap;
	}

	public void setModuleMap(Map<Long, ModuleTO> moduleMap) {
		this.moduleMap = moduleMap;
	}

	public static Map<Integer, String> getColumnpositionmap() {
		return columnPositionMap;
	}

	public List<ModuleTO> getParentModuleList() {
		return parentModuleList;
	}

	public void setParentModuleList(List<ModuleTO> parentModuleList) {
		this.parentModuleList = parentModuleList;
	}

	public List<ModuleTO> getAllChildModuleList() {
		return allChildModuleList;
	}

	public void setAllChildModuleList(List<ModuleTO> allChildModuleList) {
		this.allChildModuleList = allChildModuleList;
	}

	public boolean isSaveFlag() {
		return saveFlag;
	}

	public void setSaveFlag(boolean saveFlag) {
		this.saveFlag = saveFlag;
	}
	
	
   
}
