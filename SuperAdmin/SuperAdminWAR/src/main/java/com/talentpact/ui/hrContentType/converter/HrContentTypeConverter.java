/**
 * 
 */
package com.talentpact.ui.hrContentType.converter;

import com.talentpact.model.HrContentType;
import com.talentpact.ui.hrContentType.common.transport.output.HrContentTypeResponseTO;

/**
 * @author vaibhav.kashyap
 *
 */
public class HrContentTypeConverter {

	/**
	 * @param contentType
	 * @return
	 */
	public static HrContentTypeResponseTO getHrContentTypeResponseTOByEntity(
			HrContentType contentType) throws Exception{
		if(contentType==null)
			return null;
		
		HrContentTypeResponseTO hrContentTypeResponseTO = null;
		try{
			hrContentTypeResponseTO = new HrContentTypeResponseTO();
			hrContentTypeResponseTO.setTypeId(contentType.getTypeId());
			if(contentType.getType()!=null && 
					!contentType.getType().equals(""))
				hrContentTypeResponseTO.setType(contentType.getType());
			
			if(contentType.getContentCategory()!=null && 
					!contentType.getContentCategory().equals(""))
				hrContentTypeResponseTO.setContentCategory(
						contentType.getContentCategory());
		}catch(Exception ex){
			hrContentTypeResponseTO = null;
			throw ex;
		}
		return hrContentTypeResponseTO;

		}

}
