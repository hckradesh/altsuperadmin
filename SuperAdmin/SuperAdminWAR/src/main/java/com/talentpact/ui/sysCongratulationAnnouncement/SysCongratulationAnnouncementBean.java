/**
 * 
 */
package com.talentpact.ui.sysCongratulationAnnouncement;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysCongratulationAnnouncementResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;

/**
 * @author pankaj.sharma1
 *
 */
@Named("sysCongratulationAnnouncementBean")
@ConversationScoped
public class SysCongratulationAnnouncementBean extends CommonBean implements Serializable, IDefaultAction
{
    private static final long serialVersionUID = 199876598978L;

    @Inject
    SysCongratulationAnnouncementController sysCongratulationAnnouncementController;

    private Integer sysCongratulationAnnouncementID;

    private String sysAnnouncementType;

    private String sysCongratulationAnnouncement;

    private String sysCongratulationUploadPath;

    private String announcementType;

    private Integer announcementCode;


    private List<SysCongratulationAnnouncementResponseTO> sysCongratulationAnnouncementResponseTOList;

    private List<SysContentTypeTO> announcementTypeTOList;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysCongratulationAnnouncementController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public Integer getSysCongratulationAnnouncementID()
    {
        return sysCongratulationAnnouncementID;
    }

    public void setSysCongratulationAnnouncementID(Integer sysCongratulationAnnouncementID)
    {
        this.sysCongratulationAnnouncementID = sysCongratulationAnnouncementID;
    }


    public String getSysAnnouncementType()
    {
        return sysAnnouncementType;
    }

    public void setSysAnnouncementType(String sysAnnouncementType)
    {
        this.sysAnnouncementType = sysAnnouncementType;
    }

    public String getSysCongratulationAnnouncement()
    {
        return sysCongratulationAnnouncement;
    }

    public void setSysCongratulationAnnouncement(String sysCongratulationAnnouncement)
    {
        this.sysCongratulationAnnouncement = sysCongratulationAnnouncement;
    }

    public String getSysCongratulationUploadPath()
    {
        return sysCongratulationUploadPath;
    }

    public void setSysCongratulationUploadPath(String sysCongratulationUploadPath)
    {
        this.sysCongratulationUploadPath = sysCongratulationUploadPath;
    }

    public List<SysCongratulationAnnouncementResponseTO> getSysCongratulationAnnouncementResponseTOList()
    {
        return sysCongratulationAnnouncementResponseTOList;
    }

    public void setSysCongratulationAnnouncementResponseTOList(List<SysCongratulationAnnouncementResponseTO> sysCongratulationAnnouncementResponseTOList)
    {
        this.sysCongratulationAnnouncementResponseTOList = sysCongratulationAnnouncementResponseTOList;
    }

    public String getAnnouncementType()
    {
        return announcementType;
    }

    public void setAnnouncementType(String announcementType)
    {
        this.announcementType = announcementType;
    }

    public Integer getAnnouncementCode()
    {
        return announcementCode;
    }

    public void setAnnouncementCode(Integer announcementCode)
    {
        this.announcementCode = announcementCode;
    }

    public List<SysContentTypeTO> getAnnouncementTypeTOList()
    {
        return announcementTypeTOList;
    }

    public void setAnnouncementTypeTOList(List<SysContentTypeTO> announcementTypeTOList)
    {
        this.announcementTypeTOList = announcementTypeTOList;
    }


}
