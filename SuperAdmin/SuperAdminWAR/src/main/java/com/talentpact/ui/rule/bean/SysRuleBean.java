/**
 * 
 */
package com.talentpact.ui.rule.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.rule.controller.SysRuleController;
import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 * 
 */
@Named("sysRuleBean")
@ConversationScoped
public class SysRuleBean extends CommonBean implements Serializable,
		IDefaultAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1321212000L;

	@Inject
	SysRuleController sysRuleController;

	private List<RuleResponseTO> ruleTOList;

	private List<RuleResponseTO> ruleFilteredList;

	private List<RuleResponseTO> ruleTypeIDTOList;

	private String ruleMethod;

	private Integer ruleID;

	private Integer ruleTypeID;

	private String ruleClass;

	private String methodParameters;

	private String methodReturnType;

	private String classPathConvention;

	private String ruleClassName;

	private String description;

	private Boolean renderAddNewRuleDialog;

	private Boolean renderSaveButton;

	private boolean renderMessageBox;

	private Boolean renderUpdateButton;

	private Boolean renderUpdateRuleDialog;

	private Boolean renderRuleTypePopup;

	private Boolean renderEditRuleTypePopup;

	private String statusMessageDetail;
	
	private Boolean renderMandatoryRuleClass;

	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			sysRuleController.initialize();
		} catch (Exception e) {

		} finally {

		}

	}

	public List<RuleResponseTO> getRuleTOList() {
		return ruleTOList;
	}

	public void setRuleTOList(List<RuleResponseTO> ruleTOList) {
		this.ruleTOList = ruleTOList;
	}

	public List<RuleResponseTO> getRuleFilteredList() {
		return ruleFilteredList;
	}

	public void setRuleFilteredList(List<RuleResponseTO> ruleFilteredList) {
		this.ruleFilteredList = ruleFilteredList;
	}

	public List<RuleResponseTO> getRuleTypeIDTOList() {
		return ruleTypeIDTOList;
	}

	public void setRuleTypeIDTOList(List<RuleResponseTO> ruleTypeIDTOList) {
		this.ruleTypeIDTOList = ruleTypeIDTOList;
	}

	public String getRuleMethod() {
		return ruleMethod;
	}

	public void setRuleMethod(String ruleMethod) {
		this.ruleMethod = ruleMethod;
	}

	public Integer getRuleID() {
		return ruleID;
	}

	public void setRuleID(Integer ruleID) {
		this.ruleID = ruleID;
	}

	public Integer getRuleTypeID() {
		return ruleTypeID;
	}

	public void setRuleTypeID(Integer ruleTypeID) {
		this.ruleTypeID = ruleTypeID;
	}

	public String getRuleClass() {
		return ruleClass;
	}

	public void setRuleClass(String ruleClass) {
		this.ruleClass = ruleClass;
	}

	public String getMethodParameters() {
		return methodParameters;
	}

	public void setMethodParameters(String methodParameters) {
		this.methodParameters = methodParameters;
	}

	public String getMethodReturnType() {
		return methodReturnType;
	}

	public void setMethodReturnType(String methodReturnType) {
		this.methodReturnType = methodReturnType;
	}

	public String getClassPathConvention() {
		return classPathConvention;
	}

	public void setClassPathConvention(String classPathConvention) {
		this.classPathConvention = classPathConvention;
	}

	public String getRuleClassName() {
		return ruleClassName;
	}

	public void setRuleClassName(String ruleClassName) {
		this.ruleClassName = ruleClassName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getRenderAddNewRuleDialog() {
		return renderAddNewRuleDialog;
	}

	public void setRenderAddNewRuleDialog(Boolean renderAddNewRuleDialog) {
		this.renderAddNewRuleDialog = renderAddNewRuleDialog;
	}

	public Boolean getRenderSaveButton() {
		return renderSaveButton;
	}

	public void setRenderSaveButton(Boolean renderSaveButton) {
		this.renderSaveButton = renderSaveButton;
	}

	public boolean isRenderMessageBox() {
		return renderMessageBox;
	}

	public void setRenderMessageBox(boolean renderMessageBox) {
		this.renderMessageBox = renderMessageBox;
	}

	public Boolean getRenderUpdateButton() {
		return renderUpdateButton;
	}

	public void setRenderUpdateButton(Boolean renderUpdateButton) {
		this.renderUpdateButton = renderUpdateButton;
	}

	public Boolean getRenderUpdateRuleDialog() {
		return renderUpdateRuleDialog;
	}

	public void setRenderUpdateRuleDialog(Boolean renderUpdateRuleDialog) {
		this.renderUpdateRuleDialog = renderUpdateRuleDialog;
	}

	public Boolean getRenderRuleTypePopup() {
		return renderRuleTypePopup;
	}

	public void setRenderRuleTypePopup(Boolean renderRuleTypePopup) {
		this.renderRuleTypePopup = renderRuleTypePopup;
	}

	public Boolean getRenderEditRuleTypePopup() {
		return renderEditRuleTypePopup;
	}

	public void setRenderEditRuleTypePopup(Boolean renderEditRuleTypePopup) {
		this.renderEditRuleTypePopup = renderEditRuleTypePopup;
	}

	public String getStatusMessageDetail() {
		return statusMessageDetail;
	}

	public void setStatusMessageDetail(String statusMessageDetail) {
		this.statusMessageDetail = statusMessageDetail;
	}

	public Boolean getRenderMandatoryRuleClass() {
		return renderMandatoryRuleClass;
	}

	public void setRenderMandatoryRuleClass(Boolean renderMandatoryRuleClass) {
		this.renderMandatoryRuleClass = renderMandatoryRuleClass;
	}
	
	

}
