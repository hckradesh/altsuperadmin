/**
 * 
 */
package com.talentpact.ui.rule;

import java.util.List;

import com.talentpact.ui.rule.to.RuleResponseTO;

/**
 * @author pankaj.sharma1
 * 
 */
public class SysRuleListHelper {

	public static boolean checkRuleMethodinList(String ruleMethod,
			Integer ruleID, List<RuleResponseTO> ruleTOList) {

		for (RuleResponseTO rule : ruleTOList) {
			if (rule != null
					&& rule.getRuleMethod() != null
					&& ruleID != rule.getRuleID()
					&& (rule.getRuleMethod()).equalsIgnoreCase(ruleMethod
							.trim())) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkExistingRuleMethodinList(String ruleMethod,
			List<RuleResponseTO> ruleTOList) {

		if (ruleTOList == null || ruleTOList.isEmpty()) {
			return false;
		}

		for (RuleResponseTO rule : ruleTOList) {
			if (rule != null
					&& rule.getRuleMethod() != null
					&& (rule.getRuleMethod()).equalsIgnoreCase(ruleMethod
							.trim())) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkRuleIDinList(Integer ruleID,
			List<RuleResponseTO> ruleTOList) {

		for (RuleResponseTO rule : ruleTOList) {
			if (rule != null && rule.getRuleID() != null
					&& (rule.getRuleID()).equals(ruleID)) {
				return true;
			}
		}
		return false;
	}

}
