package com.talentpact.ui.sysDuplicateParameter.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysDuplicateParameter.controller.SysDuplicateParameterController;
import com.talentpact.ui.sysDuplicateParameter.to.SysDuplicateParameterTO;


/**
 * 
 * @author prachi.bansal
 *
 */

@SuppressWarnings("serial")
@Named("sysDuplicateParameterBean")
@ConversationScoped
public class SysDuplicateParameterBean extends CommonBean implements Serializable, IDefaultAction
{   
    @Inject
    SysDuplicateParameterController sysDuplicateParameterController;
    
    private Integer sysDuplicateParameterID;
    
    private String sysDuplicateParameterName;
    
    private SysDuplicateParameterTO sysDuplicateParameterTO;
    
    private List<SysDuplicateParameterTO> sysDuplicateParameterTOList;
    
    private boolean renderErrorMessage;

    @Override
    public void initialize() {
        try {
        	endConversation();
            beginConversation();
            sysDuplicateParameterController.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public Integer getSysDuplicateParameterID() {
        return sysDuplicateParameterID;
    }

    public void setSysDuplicateParameterID(Integer sysDuplicateParameterID) {
        this.sysDuplicateParameterID = sysDuplicateParameterID;
    }

    public String getSysDuplicateParameterName() {
        return sysDuplicateParameterName;
    }

    public void setSysDuplicateParameterName(String sysDuplicateParameterName) {
        this.sysDuplicateParameterName = sysDuplicateParameterName;
    }

    public SysDuplicateParameterTO getSysDuplicateParameterTO() {
        return sysDuplicateParameterTO;
    }

    public void setSysDuplicateParameterTO(SysDuplicateParameterTO sysDuplicateParameterTO) {
        this.sysDuplicateParameterTO = sysDuplicateParameterTO;
    }

    public List<SysDuplicateParameterTO> getSysDuplicateParameterTOList() {
        return sysDuplicateParameterTOList;
    }

    public void setSysDuplicateParameterTOList(List<SysDuplicateParameterTO> sysDuplicateParameterTOList) {
        this.sysDuplicateParameterTOList = sysDuplicateParameterTOList;
    }
    
    public boolean isRenderErrorMessage() {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage) {
        this.renderErrorMessage = renderErrorMessage;
    }
    }