/**
 * 
 */
package com.talentpact.ui.meshRequestType.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.business.common.constants.IMeshRequestTypeConstants;
import com.talentpact.business.dataservice.MeshRequestType.MeshRequestTypeService;
import com.talentpact.ui.common.controller.CommonController;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.menu.bean.MenuBean;
import com.talentpact.ui.meshRequestType.bean.MeshRequestTypeBean;

/**
 * 
 * @author vivek.goyal
 * 
 */

@Named("meshRequestTypeController")
@ConversationScoped
public class MeshRequestTypeController extends CommonController implements
		Serializable, IDefaultAction {
	/**
     * 
     */
	private static final long serialVersionUID = -2937030591547477634L;

	private static final Logger LOGGER_ = LoggerFactory
			.getLogger(MeshRequestTypeController.class);

	@Inject
	MenuBean menuBean;

	@Inject
	MeshRequestTypeBean meshRequestTypeBean;

	@Inject
	MeshRequestTypeService meshRequestTypeService;

	@Override
	public void initialize() {
		List<MeshRequestTypeTO> meshRequestTypeTOList = null;
		Set<String> name = null;
		MeshRequestTypeTO meshRequestTypeTO = null;

		try {
			meshRequestTypeTOList = meshRequestTypeService
					.getMeshRequestTypeList();
			meshRequestTypeBean.setMeshRequestTypeTOList(meshRequestTypeTOList);
			name = new HashSet<String>();
			for (MeshRequestTypeTO list : meshRequestTypeTOList) {
				meshRequestTypeTO = new MeshRequestTypeTO();
				meshRequestTypeTO = list;
				name.add(meshRequestTypeTO.getRequestTypeName());
			}
			meshRequestTypeBean.setMeshRequestType(name);
			menuBean.setPage("../MeshRequestType/meshRequestType.xhtml");
			meshRequestTypeBean.setRenderMeshRequestTypePopup(false);
			meshRequestTypeBean.setRenderErrorMessage(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void resetMeshRequestType() {
		meshRequestTypeBean.setRenderMeshRequestTypePopup(true);
		meshRequestTypeBean.setRequestTypeName(null);
		meshRequestTypeBean.setMeshRequestType(null);
		meshRequestTypeBean.setSelectedRequestTypeName(null);
	}

	public void addMeshRequestType() {
		MeshRequestTypeTO meshRequestTypeTO = null;
		RequestContext requestContext = null;
		try {
			requestContext = RequestContext.getCurrentInstance();
			meshRequestTypeTO = new MeshRequestTypeTO();
			meshRequestTypeTO.setRequestTypeName(meshRequestTypeBean
					.getRequestTypeName());
			meshRequestTypeBean.setRenderErrorMessage(true);

			if (validateNewMeshRequestType(meshRequestTypeTO, false)) {
				meshRequestTypeService.addMeshRequestType(meshRequestTypeTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"MeshRequestType saved successfully.", ""));
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_ADD_DIALOG_HIDE);
				requestContext.update("meshRequestTypeListForm");
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_DATATABLE_RESET);
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_PAGINATION_DATATABLE_RESET);
			} else {
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										"'"
												+ meshRequestTypeTO
														.getRequestTypeName()
												+ "' MeshRequestType already exists.",
										""));
				meshRequestTypeBean.setRenderErrorMessage(true);
				RequestContext.getCurrentInstance().update(
						"addMeshRequestTypeForm");
			}
		} catch (Exception e) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									IMeshRequestTypeConstants.MESHREQUESTTYPE_SAVE_ERROR_MSG,
									""));
			LOGGER_.error("", e);
			meshRequestTypeBean.setRenderMeshRequestTypePopup(false);
			requestContext
					.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_ADD_DIALOG_HIDE);
			RequestContext.getCurrentInstance()
					.update("addMeshRequestTypeForm");
		}
	}

	// To validate if the newly added MeshRequestType is already present in the
	// system
	private boolean validateNewMeshRequestType(
			MeshRequestTypeTO meshRequestTypeTO, boolean edit) {
		boolean valid = true;
		List<MeshRequestTypeTO> meshRequestTypeTOList = null;
		try {
			if (meshRequestTypeTO.getRequestTypeName() == null
					|| meshRequestTypeTO.getRequestTypeName().trim().equals("")) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								" Please enter a valid MeshRequestType.", ""));
				valid = false;
			} else {
				if (meshRequestTypeBean.getMeshRequestTypeTOList() != null) {
					meshRequestTypeTOList = meshRequestTypeBean
							.getMeshRequestTypeTOList();
				}
				for (MeshRequestTypeTO requestTypeNameTO : meshRequestTypeTOList) {
					if (requestTypeNameTO.getRequestTypeName()
							.equalsIgnoreCase(
									meshRequestTypeTO.getRequestTypeName()
											.trim())) {
						valid = false;
						if (edit) {
							if (requestTypeNameTO.getRequestTypeId().equals(
									meshRequestTypeTO.getRequestTypeId())) {
								valid = true;
							}
						}
						break;
					}
				}
			}
			return valid;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void refreshPopUp(MeshRequestTypeTO meshRequestTypeTO) {
		try {
			meshRequestTypeBean.setRequestTypeId(meshRequestTypeTO
					.getRequestTypeId());
			meshRequestTypeBean.setRequestTypeName(meshRequestTypeTO
					.getRequestTypeName());
			meshRequestTypeBean.setRenderEditMeshRequestTypePopup(true);
			meshRequestTypeBean.setDuplicateRequestTypeName(meshRequestTypeTO
					.getRequestTypeName());

		} catch (Exception e) {
		}
	}

	public void updateEditMeshRequestType(MeshRequestTypeTO meshRequestTypeTO)
			throws Exception {
		try {
			meshRequestTypeBean.setRequestTypeId(meshRequestTypeTO
					.getRequestTypeId());
			meshRequestTypeBean.setRequestTypeName(meshRequestTypeTO
					.getRequestTypeName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateMeshRequestType() {
		MeshRequestTypeTO meshRequestTypeTO = null;
		RequestContext requestContext = null;
		try {
			requestContext = RequestContext.getCurrentInstance();
			meshRequestTypeTO = new MeshRequestTypeTO();
			meshRequestTypeTO.setRequestTypeId(meshRequestTypeBean
					.getRequestTypeId());
			meshRequestTypeTO.setRequestTypeName(meshRequestTypeBean
					.getRequestTypeName());
			if (validateNewMeshRequestType(meshRequestTypeTO, true)) {
				meshRequestTypeService.editMeshRequestType(meshRequestTypeTO);
				initialize();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"MeshRequestType updated successfully.", ""));
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_EDIT_DIALOG_HIDE);
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_DATATABLE_RESET);
				requestContext
						.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_PAGINATION_DATATABLE_RESET);

			} else {
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										"'"
												+ meshRequestTypeTO
														.getRequestTypeName()
												+ "' MeshRequestType already exists.",
										""));
				meshRequestTypeBean.setRenderErrorMessage(true);
				RequestContext.getCurrentInstance().update(
						"meshRequestTypeEditForm");
			}
		} catch (Exception e) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									IMeshRequestTypeConstants.MESHREQUESTTYPE_SAVE_ERROR_MSG,
									""));
			LOGGER_.error("", e);
			meshRequestTypeBean.setRenderMeshRequestTypePopup(false);
			requestContext
					.execute(IMeshRequestTypeConstants.MESHREQUESTTYPE_EDIT_DIALOG_HIDE);
			RequestContext.getCurrentInstance().update(
					"meshRequestTypeEditForm");
		}
	}
}
