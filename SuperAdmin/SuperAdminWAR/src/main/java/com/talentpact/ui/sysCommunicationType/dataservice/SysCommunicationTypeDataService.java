/**
 * 
 */
package com.talentpact.ui.sysCommunicationType.dataservice;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.talentpact.ui.sysCommunicationType.common.transport.output.SysCommunicationTypeResponseTO;
import com.talentpact.ui.sysCommunicationType.dataservice.DS.SysCommunicationTypeDS;



/**
 * @author vaibhav.kashyap
 *
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysCommunicationTypeDataService {

	@EJB
	private SysCommunicationTypeDS sysCommunicationTypeDS = null;
	/**
	 * @return
	 */
	public List<SysCommunicationTypeResponseTO> getAllCommType() throws Exception{
		return sysCommunicationTypeDS.getAllCommType();
	}

}
