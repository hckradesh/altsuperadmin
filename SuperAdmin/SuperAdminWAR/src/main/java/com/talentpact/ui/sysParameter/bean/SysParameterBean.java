package com.talentpact.ui.sysParameter.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysParameterResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysParameter.controller.SysParameterController;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("sysParameterBean")
@SessionScoped
public class SysParameterBean extends CommonBean implements Serializable, IDefaultAction
{
    @Inject
    SysParameterController sysParameterController;


    @Override
    public void initialize()
    {
        try {
        	endConversation();
            beginConversation();
            sysParameterController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private List<SysParameterResponseTO> paramList;

    private Integer paramID;

    private String paramLabel;

    private Integer paramDataTypeID;

    private String paramDescripTion;

    private String dataTypeName;

    private Integer parameterDataTypeId;

    private List<SysParameterResponseTO> parameterDataList;
    
    private SysParameterResponseTO sysParameterResponseTO;

    public List<SysParameterResponseTO> getParamList()
    {
        return paramList;
    }

    public void setParamList(List<SysParameterResponseTO> paramList)
    {
        this.paramList = paramList;
    }

    public Integer getParamID()
    {
        return paramID;
    }

    public void setParamID(Integer paramID)
    {
        this.paramID = paramID;
    }

    public String getParamLabel()
    {
        return paramLabel;
    }

    public void setParamLabel(String paramLabel)
    {
        this.paramLabel = paramLabel;
    }

    public Integer getParamDataTypeID()
    {
        return paramDataTypeID;
    }

    public void setParamDataTypeID(Integer paramDataTypeID)
    {
        this.paramDataTypeID = paramDataTypeID;
    }

    public String getParamDescripTion()
    {
        return paramDescripTion;
    }

    public void setParamDescripTion(String paramDescripTion)
    {
        this.paramDescripTion = paramDescripTion;
    }

    public String getDataTypeName()
    {
        return dataTypeName;
    }

    public void setDataTypeName(String dataTypeName)
    {
        this.dataTypeName = dataTypeName;
    }

    public Integer getParameterDataTypeId()
    {
        return parameterDataTypeId;
    }

    public void setParameterDataTypeId(Integer parameterDataTypeId)
    {
        this.parameterDataTypeId = parameterDataTypeId;
    }

    public List<SysParameterResponseTO> getParameterDataList()
    {
        return parameterDataList;
    }

    public void setParameterDataList(List<SysParameterResponseTO> parameterDataList)
    {
        this.parameterDataList = parameterDataList;
    }

    public SysParameterResponseTO getSysParameterResponseTO() {
        return sysParameterResponseTO;
    }

    public void setSysParameterResponseTO(SysParameterResponseTO sysParameterResponseTO) {
        this.sysParameterResponseTO = sysParameterResponseTO;
    }

}
