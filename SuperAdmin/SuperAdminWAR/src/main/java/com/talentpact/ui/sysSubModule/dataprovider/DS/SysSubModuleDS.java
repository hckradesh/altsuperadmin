package com.talentpact.ui.sysSubModule.dataprovider.DS;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.talentpact.business.dataservice.AbstractDS;
import com.talentpact.model.SysSubModule;
import com.talentpact.ui.sysSubModule.common.transport.input.SysSubModuleRequestTO;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;
import com.talentpact.ui.sysSubModule.constants.ISysSubModuleConstants;
import com.talentpact.ui.sysSubModule.dataprovider.converter.SysSubModuleConverter;
import com.talentpact.ui.sysSubModule.exception.SysSubModuleException;

/**
 * @author vaibhav.kashyap
 * 
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class SysSubModuleDS extends AbstractDS<SysSubModule> {
    private static final Logger LOGGER = Logger.getLogger(SysSubModuleDS.class.toString());

    public SysSubModuleDS() {
        super(SysSubModule.class);
    }

    public SysSubModuleResponseTO getParentModuleAndSubModules(SysSubModuleRequestTO sysSubModuleRequestTO)
        throws Exception {

        if (sysSubModuleRequestTO == null)
            return null;

        SysSubModuleResponseTO sysSubModuleResponseTO = null;

        StringBuilder hqlQuery = null;
        Query query = null;

        List<Object[]> resultArrayObjectList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append(" select ");
            hqlQuery.append(" ssm.moduleId, ssm.moduleName, ssm.appId, ");
            hqlQuery.append(" pm.moduleId, pm.moduleName, pm.appId ");
            hqlQuery.append(" from ");
            hqlQuery.append(" SysSubModule ssm  ");
            hqlQuery.append(" left join ssm.parentModule pm ");
            hqlQuery.append(" where ");
            hqlQuery.append(" ssm.tenantId = :tenantId ");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            query.setParameter("tenantId", sysSubModuleRequestTO.getTenantID());
            resultArrayObjectList = query.getResultList();
            if (resultArrayObjectList != null && !resultArrayObjectList.isEmpty()) {
                sysSubModuleResponseTO = SysSubModuleConverter.getparentModuleAndSubModuleDetails(resultArrayObjectList);
            }
        } catch (Exception ex) {
            sysSubModuleResponseTO = null;
            throw ex;
        }

        return sysSubModuleResponseTO;
    }

    public List<SysSubModule> getAllSysSubModules()
        throws SysSubModuleException {
        Query query = null;
        StringBuilder hqlQuery = null;
        List<SysSubModule> sysSubModuleList = null;
        try {
            hqlQuery = new StringBuilder();
            hqlQuery.append("select sysSubModule from SysSubModule sysSubModule order by sysSubModule.moduleName asc");
            query = getEntityManager("TalentPact").createQuery(hqlQuery.toString());
            sysSubModuleList = query.getResultList();
        } catch (Exception ex) {
            LOGGER.error("", ex);
            throw new SysSubModuleException(ISysSubModuleConstants.SYS_SUB_MODULE_RETRIEVE_ERROR_MSG);
        }
        return sysSubModuleList;
    }
}
