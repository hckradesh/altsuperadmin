package com.talentpact.ui.meshRequestType.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.MeshRequestTypeTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.meshRequestType.controller.MeshRequestTypeController;

/**
 * 
 * @author vivek.goyal
 * 
 */

@SuppressWarnings("serial")
@Named("meshRequestTypeBean")
@ConversationScoped
public class MeshRequestTypeBean extends CommonBean implements Serializable,
		IDefaultAction {
	@Inject
	MeshRequestTypeController meshRequestTypeController;

	private Long requestTypeId;

	private String requestTypeName;

	private boolean renderMeshRequestTypePopup;

	private boolean renderEditMeshRequestTypePopup;

	private boolean renderMessageBox;

	private Boolean renderUpdateButton;

	private String duplicateRequestTypeName;

	private boolean renderErrorMessage;

	private Set<String> meshRequestType;

	private Set<String> selectedRequestTypeName;

	private List<MeshRequestTypeTO> meshRequestTypeTOList;

	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			meshRequestTypeController.initialize();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
	}

	public Long getRequestTypeId() {
		return requestTypeId;
	}

	public void setRequestTypeId(Long requestTypeId) {
		this.requestTypeId = requestTypeId;
	}

	public String getRequestTypeName() {
		return requestTypeName;
	}

	public void setRequestTypeName(String requestTypeName) {
		this.requestTypeName = requestTypeName;
	}

	public boolean isRenderMeshRequestTypePopup() {
		return renderMeshRequestTypePopup;
	}

	public void setRenderMeshRequestTypePopup(boolean renderMeshRequestTypePopup) {
		this.renderMeshRequestTypePopup = renderMeshRequestTypePopup;
	}

	public boolean isRenderEditMeshRequestTypePopup() {
		return renderEditMeshRequestTypePopup;
	}

	public void setRenderEditMeshRequestTypePopup(
			boolean renderEditMeshRequestTypePopup) {
		this.renderEditMeshRequestTypePopup = renderEditMeshRequestTypePopup;
	}

	public boolean isRenderMessageBox() {
		return renderMessageBox;
	}

	public void setRenderMessageBox(boolean renderMessageBox) {
		this.renderMessageBox = renderMessageBox;
	}

	public Boolean getRenderUpdateButton() {
		return renderUpdateButton;
	}

	public void setRenderUpdateButton(Boolean renderUpdateButton) {
		this.renderUpdateButton = renderUpdateButton;
	}

	public String getDuplicateRequestTypeName() {
		return duplicateRequestTypeName;
	}

	public void setDuplicateRequestTypeName(String duplicateRequestTypeName) {
		this.duplicateRequestTypeName = requestTypeName;
	}

	public boolean isRenderErrorMessage() {
		return renderErrorMessage;
	}

	public void setRenderErrorMessage(boolean renderErrorMessage) {
		this.renderErrorMessage = renderErrorMessage;
	}

	public List<MeshRequestTypeTO> getMeshRequestTypeTOList() {
		return meshRequestTypeTOList;
	}

	public void setMeshRequestTypeTOList(
			List<MeshRequestTypeTO> meshRequestTypeTOList) {
		this.meshRequestTypeTOList = meshRequestTypeTOList;
	}

	public Set<String> getMeshRequestType() {
		return meshRequestType;
	}

	public void setMeshRequestType(Set<String> meshRequestType) {
		this.meshRequestType = meshRequestType;
	}

	public Set<String> getSelectedRequestTypeName() {
		return selectedRequestTypeName;
	}

	public void setSelectedRequestTypeName(Set<String> selectedRequestTypeName) {
		this.selectedRequestTypeName = selectedRequestTypeName;
	}

}