package com.talentpact.ui.sysPolicy.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.sysPolicy.controller.SysPolicyController;
import com.talentpact.ui.sysPolicy.to.SysPolicyTO;

/**
 * 
 * @author vivek.goyal
 *
 */


@Named("sysPolicyBean")
@ConversationScoped
public class SysPolicyBean extends CommonBean implements Serializable, IDefaultAction
{

    @Inject
    SysPolicyController sysPolicyController;

    private Long sysPolicyID;

    private String policyCode;

    private String policyName;

    private String policyLabel;

    private Integer moduleID;

    private String moduleName;

    private Integer entityID;

    private String entityName;

    private Integer duplicateModuleID;

    private Integer duplicateEntityID;

    private String duplicatePolicyCode;

    private String duplicatePolicyName;

    private String duplicatePolicyLabel;


    private List<SysPolicyTO> sysPolicyTOList;

    private List<SysPolicyTO> editSysPolicyTOList;

    private List<SysPolicyTO> sysModuleTOList;

    private List<SysPolicyTO> sysEntityTOList;

    private boolean renderSysPolicyPopup;

    private boolean renderEditSysPolicyPopup;

    private String duplicateSysPolicy;

    private boolean renderErrorMessage;

    private List<SysContentTypeTO> sysContentTypeTOList;

    private Integer policyTypeID;

    private Integer duplicatePolicyTypeID;

    @Override
    public void initialize()
    {
        try {
            endConversation();
            beginConversation();
            sysPolicyController.initialize();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public List<SysPolicyTO> getSysPolicyTOList()
    {
        return sysPolicyTOList;
    }

    public void setSysPolicyTOList(List<SysPolicyTO> sysPolicyTOList)
    {
        this.sysPolicyTOList = sysPolicyTOList;
    }

    public List<SysPolicyTO> getEditSysPolicyTOList()
    {
        return editSysPolicyTOList;
    }

    public void setEditSysPolicyTOList(List<SysPolicyTO> editSysPolicyTOList)
    {
        this.editSysPolicyTOList = editSysPolicyTOList;
    }

    public boolean isRenderSysPolicyPopup()
    {
        return renderSysPolicyPopup;
    }

    public void setRenderSysPolicyPopup(boolean renderSysPolicyPopup)
    {
        this.renderSysPolicyPopup = renderSysPolicyPopup;
    }

    public boolean isRenderEditSysPolicyPopup()
    {
        return renderEditSysPolicyPopup;
    }

    public void setRenderEditSysPolicyPopup(boolean renderEditSysPolicyPopup)
    {
        this.renderEditSysPolicyPopup = renderEditSysPolicyPopup;
    }

    public String getDuplicateSysPolicy()
    {
        return duplicateSysPolicy;
    }

    public void setDuplicateSysPolicy(String duplicateSysPolicy)
    {
        this.duplicateSysPolicy = duplicateSysPolicy;
    }

    public boolean isRenderErrorMessage()
    {
        return renderErrorMessage;
    }

    public void setRenderErrorMessage(boolean renderErrorMessage)
    {
        this.renderErrorMessage = renderErrorMessage;
    }

    public Long getSysPolicyID()
    {
        return sysPolicyID;
    }

    public void setSysPolicyID(Long sysPolicyID)
    {
        this.sysPolicyID = sysPolicyID;
    }

    public String getPolicyCode()
    {
        return policyCode;
    }

    public void setPolicyCode(String policyCode)
    {
        this.policyCode = policyCode;
    }

    public String getPolicyName()
    {
        return policyName;
    }

    public void setPolicyName(String policyName)
    {
        this.policyName = policyName;
    }

    public String getPolicyLabel()
    {
        return policyLabel;
    }

    public void setPolicyLabel(String policyLabel)
    {
        this.policyLabel = policyLabel;
    }

    public Integer getModuleID()
    {
        return moduleID;
    }

    public void setModuleID(Integer moduleID)
    {
        this.moduleID = moduleID;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public Integer getEntityID()
    {
        return entityID;
    }

    public void setEntityID(Integer entityID)
    {
        this.entityID = entityID;
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public List<SysPolicyTO> getSysModuleTOList()
    {
        return sysModuleTOList;
    }

    public void setSysModuleTOList(List<SysPolicyTO> sysModuleTOList)
    {
        this.sysModuleTOList = sysModuleTOList;
    }

    public List<SysPolicyTO> getSysEntityTOList()
    {
        return sysEntityTOList;
    }

    public void setSysEntityTOList(List<SysPolicyTO> sysEntityTOList)
    {
        this.sysEntityTOList = sysEntityTOList;
    }

    public Integer getDuplicateModuleID()
    {
        return duplicateModuleID;
    }

    public void setDuplicateModuleID(Integer duplicateModuleID)
    {
        this.duplicateModuleID = duplicateModuleID;
    }

    public Integer getDuplicateEntityID()
    {
        return duplicateEntityID;
    }

    public void setDuplicateEntityID(Integer duplicateEntityID)
    {
        this.duplicateEntityID = duplicateEntityID;
    }

    public String getDuplicatePolicyCode()
    {
        return duplicatePolicyCode;
    }

    public void setDuplicatePolicyCode(String duplicatePolicyCode)
    {
        this.duplicatePolicyCode = duplicatePolicyCode;
    }

    public String getDuplicatePolicyName()
    {
        return duplicatePolicyName;
    }

    public void setDuplicatePolicyName(String duplicatePolicyName)
    {
        this.duplicatePolicyName = duplicatePolicyName;
    }

    public String getDuplicatePolicyLabel()
    {
        return duplicatePolicyLabel;
    }

    public void setDuplicatePolicyLabel(String duplicatePolicyLabel)
    {
        this.duplicatePolicyLabel = duplicatePolicyLabel;
    }

    public List<SysContentTypeTO> getSysContentTypeTOList()
    {
        return sysContentTypeTOList;
    }

    public void setSysContentTypeTOList(List<SysContentTypeTO> sysContentTypeTOList)
    {
        this.sysContentTypeTOList = sysContentTypeTOList;
    }

    public Integer getPolicyTypeID()
    {
        return policyTypeID;
    }

    public void setPolicyTypeID(Integer policyTypeID)
    {
        this.policyTypeID = policyTypeID;
    }

    public Integer getDuplicatePolicyTypeID()
    {
        return duplicatePolicyTypeID;
    }

    public void setDuplicatePolicyTypeID(Integer duplicatePolicyTypeID)
    {
        this.duplicatePolicyTypeID = duplicatePolicyTypeID;
    }


}
