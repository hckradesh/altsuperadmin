package com.talentpact.ui.uiform.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.UiFormFieldGroupResponseTO;
import com.talentpact.business.application.transport.output.UiFormResponseTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;
import com.talentpact.ui.uiform.controller.UiFormController;

/**
 * 
 * @author Rahul.Chabba
 * 
 */

@Named("uiFormGroupBean")
@SessionScoped
public class UIFormGroupBean extends CommonBean implements Serializable,	IDefaultAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	UiFormController uiFormController;

	private List<UiFormFieldGroupResponseTO> uiFormResponseTOList;

	private Integer fieldGroupID;
	 private String fieldGroupName;
	 private Integer formID;
	 private Integer tenantID;
	 private Integer createdBy;
	 private Integer modifiedBy;
	 private Date createdDate;
	 private Date modifiedDate;
    
	 
	 
	 
	@Override
	public void initialize() {
		try {
			endConversation();
			beginConversation();
			uiFormController.initialize();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public UiFormController getUiFormController() {
		return uiFormController;
	}

	public void setUiFormController(UiFormController uiFormController) {
		this.uiFormController = uiFormController;
	}

	public List<UiFormFieldGroupResponseTO> getUiFormResponseTOList() {
		return uiFormResponseTOList;
	}

	public void setUiFormResponseTOList(
			List<UiFormFieldGroupResponseTO> uiFormResponseTOList) {
		this.uiFormResponseTOList = uiFormResponseTOList;
	}

	public Integer getFieldGroupID() {
		return fieldGroupID;
	}

	public void setFieldGroupID(Integer fieldGroupID) {
		this.fieldGroupID = fieldGroupID;
	}

	public String getFieldGroupName() {
		return fieldGroupName;
	}

	public void setFieldGroupName(String fieldGroupName) {
		this.fieldGroupName = fieldGroupName;
	}

	public Integer getFormID() {
		return formID;
	}

	public void setFormID(Integer formID) {
		this.formID = formID;
	}

	public Integer getTenantID() {
		return tenantID;
	}

	public void setTenantID(Integer tenantID) {
		this.tenantID = tenantID;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	

    public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	

}
