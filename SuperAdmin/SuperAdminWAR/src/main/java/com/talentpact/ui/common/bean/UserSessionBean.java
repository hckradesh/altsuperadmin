/**
 * 
 */
package com.talentpact.ui.common.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.menu.MenuModel;

import com.talentpact.ui.menu.transport.MenuTO;


/**
 * @author radhamadhab.dalai
 *
 */
@Named("userSessionBean")
@SessionScoped
public class UserSessionBean implements Serializable
{

    private String userName;

    private Long userID;

    private String orgnizationName;

    private MenuModel userMenu1;

    Map<Long, List<MenuTO>> userMenuMap;

    /**
     * @return the userName
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * @return the userID
     */
    public Long getUserID()
    {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(Long userID)
    {
        this.userID = userID;
    }

    /**
     * @return the orgnizationName
     */
    public String getOrgnizationName()
    {
        return orgnizationName;
    }

    /**
     * @param orgnizationName the orgnizationName to set
     */
    public void setOrgnizationName(String orgnizationName)
    {
        this.orgnizationName = orgnizationName;
    }


    public MenuModel getUserMenu1()
    {
        return userMenu1;
    }

    public void setUserMenu1(MenuModel userMenu1)
    {
        this.userMenu1 = userMenu1;
    }

    public Map<Long, List<MenuTO>> getUserMenuMap()
    {
        return userMenuMap;
    }

    public void setUserMenuMap(Map<Long, List<MenuTO>> userMenuMap)
    {
        this.userMenuMap = userMenuMap;
    }


}
