package com.talentpact.ui.altbenefits.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import com.alt.altbenefits.to.OfferingCodeMaximumLimitTO;
import com.alt.altbenefits.to.OfferingCodeTO;
import com.alt.altbenefits.to.PeakRecurringDetailTO;
import com.alt.altbenefits.to.VendorOfferingTemplateTO;
import com.alt.contentType.transport.input.ContentTypeTO;
import com.talentpact.business.application.transport.output.SysOfferingTypeTO;
import com.talentpact.business.application.transport.output.SysPeakRecurringTypeTO;
import com.talentpact.ui.altbenefits.controller.SysBenefitsVendorController;
import com.talentpact.ui.altbenefits.transport.SysBenefitVendorTO;
import com.talentpact.ui.common.bean.CommonBean;
import com.talentpact.ui.common.controller.IDefaultAction;

/**
 * 
 * @author javed.ali
 * 
 */


@Named("sysBenefitsVendorBean")
@ConversationScoped
public class SysBenefitsVendorBean extends CommonBean implements Serializable, IDefaultAction
{

    private static final long serialVersionUID = -1817704848516251271L;

    @Inject
    SysBenefitsVendorController benefitsVendorController;

    private SysBenefitVendorTO sysBenefitVendorTO;

    private boolean renderVendorOffering;

    private VendorOfferingTemplateTO vendorOfferingTemplateTO;

    private VendorOfferingTemplateTO offeringTemplateTO;

    private List<SysBenefitVendorTO> sysBenefitVendorTOList;

    private List<SysBenefitVendorTO> filteredSysBenefitVendorTOList;

    private List<VendorOfferingTemplateTO> offeringTemlateTOlist;

    private List<VendorOfferingTemplateTO> filtedredOfferingTemlateTOlist;

    private VendorOfferingTemplateTO selectedVendorOfferingTemplateTO;

    private OfferingCodeTO offeringCodeTO;

    private List<OfferingCodeTO> offeringCodeTOList;

    private List<OfferingCodeTO> filtedredOfferingCodeTOlist;

    private List<ContentTypeTO> offeringAccessTypeTOList;

    private boolean editVendorForm;

    private boolean editOfferCodeForm;

    private boolean renderFormMessage;

    private UploadedFile bannerFile;

    private String uploadedBannerFileName;

    private List<SysOfferingTypeTO> sysOfferingTypeTOList = null;

    private SysBenefitVendorTO selectedVendorTO;

    private Boolean updateOfferTemplateFlag = false;

    private Boolean addOfferTemplateFlag = false;

    private List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList;

    private boolean offeringPeakDetailActive;

    private boolean renderPeakDetails;

    private PeakRecurringDetailTO peakRecurringDetailTO;

    private boolean renderMaximumLimitApplicableDetail;

    private OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO;

    private List<String> accessTypeNameList;

    private UploadedFile vendorLogoFile;

    private String uploadedVendorLogoFileName;

    private List<ContentTypeTO> maximumUsageDurationTypeTOList;
    
    public String getUploadedVendorLogoFileName()
    {
        return uploadedVendorLogoFileName;
    }

    public void setUploadedVendorLogoFileName(String uploadedVendorLogoFileName)
    {
        this.uploadedVendorLogoFileName = uploadedVendorLogoFileName;
    }

    public UploadedFile getVendorLogoFile()
    {
        return vendorLogoFile;
    }

    public void setVendorLogoFile(UploadedFile vendorLogoFile)
    {
        this.vendorLogoFile = vendorLogoFile;
    }

    public boolean isRenderMaximumLimitApplicableDetail()
    {
        return renderMaximumLimitApplicableDetail;
    }

    public void setRenderMaximumLimitApplicableDetail(boolean renderMaximumLimitApplicableDetail)
    {
        this.renderMaximumLimitApplicableDetail = renderMaximumLimitApplicableDetail;
    }

    public void setOfferingCodeMaximumLimitTO(OfferingCodeMaximumLimitTO offeringCodeMaximumLimitTO)
    {
        this.offeringCodeMaximumLimitTO = offeringCodeMaximumLimitTO;
    }

    public OfferingCodeMaximumLimitTO getOfferingCodeMaximumLimitTO()
    {
        return offeringCodeMaximumLimitTO;
    }

    public boolean isEditOfferCodeForm()
    {
        return editOfferCodeForm;
    }

    public void setEditOfferCodeForm(boolean editOfferCodeForm)
    {
        this.editOfferCodeForm = editOfferCodeForm;
    }

    public VendorOfferingTemplateTO getSelectedVendorOfferingTemplateTO()
    {
        return selectedVendorOfferingTemplateTO;
    }

    public void setSelectedVendorOfferingTemplateTO(VendorOfferingTemplateTO selectedVendorOfferingTemplateTO)
    {
        this.selectedVendorOfferingTemplateTO = selectedVendorOfferingTemplateTO;
    }

    public String getUploadedBannerFileName()
    {
        return uploadedBannerFileName;
    }

    public void setUploadedBannerFileName(String uploadedBannerFileName)
    {
        this.uploadedBannerFileName = uploadedBannerFileName;
    }

    @Override
    public void initialize()
    {
    	endConversation();
        beginConversation();
        benefitsVendorController.initialize();
    }


    public SysBenefitVendorTO getSysBenefitVendorTO()
    {
        return sysBenefitVendorTO;
    }


    public void setSysBenefitVendorTO(SysBenefitVendorTO sysBenefitVendorTO)
    {
        this.sysBenefitVendorTO = sysBenefitVendorTO;
    }


    public List<SysBenefitVendorTO> getSysBenefitVendorTOList()
    {
        return sysBenefitVendorTOList;
    }


    public void setSysBenefitVendorTOList(List<SysBenefitVendorTO> sysBenefitVendorTOList)
    {
        this.sysBenefitVendorTOList = sysBenefitVendorTOList;
    }


    public boolean isEditVendorForm()
    {
        return editVendorForm;
    }


    public void setEditVendorForm(boolean editVendorForm)
    {
        this.editVendorForm = editVendorForm;
    }


    public boolean isRenderFormMessage()
    {
        return renderFormMessage;
    }


    public void setRenderFormMessage(boolean renderFormMessage)
    {
        this.renderFormMessage = renderFormMessage;
    }


    public List<SysBenefitVendorTO> getFilteredSysBenefitVendorTOList()
    {
        return filteredSysBenefitVendorTOList;
    }


    public void setFilteredSysBenefitVendorTOList(List<SysBenefitVendorTO> filteredSysBenefitVendorTOList)
    {
        this.filteredSysBenefitVendorTOList = filteredSysBenefitVendorTOList;
    }


    public List<VendorOfferingTemplateTO> getOfferingTemlateTOlist()
    {
        return offeringTemlateTOlist;
    }


    public void setOfferingTemlateTOlist(List<VendorOfferingTemplateTO> offeringTemlateTOlist)
    {
        this.offeringTemlateTOlist = offeringTemlateTOlist;
    }


    public List<VendorOfferingTemplateTO> getFiltedredOfferingTemlateTOlist()
    {
        return filtedredOfferingTemlateTOlist;
    }


    public void setFiltedredOfferingTemlateTOlist(List<VendorOfferingTemplateTO> filtedredOfferingTemlateTOlist)
    {
        this.filtedredOfferingTemlateTOlist = filtedredOfferingTemlateTOlist;
    }


    public VendorOfferingTemplateTO getOfferingTemplateTO()
    {
        return offeringTemplateTO;
    }


    public void setOfferingTemplateTO(VendorOfferingTemplateTO offeringTemplateTO)
    {
        this.offeringTemplateTO = offeringTemplateTO;
    }


    public UploadedFile getBannerFile()
    {
        return bannerFile;
    }


    public void setBannerFile(UploadedFile bannerFile)
    {
        this.bannerFile = bannerFile;
    }


    public List<SysOfferingTypeTO> getSysOfferingTypeTOList()
    {
        return sysOfferingTypeTOList;
    }


    public void setSysOfferingTypeTOList(List<SysOfferingTypeTO> sysOfferingTypeTOList)
    {
        this.sysOfferingTypeTOList = sysOfferingTypeTOList;
    }


    public SysBenefitVendorTO getSelectedVendorTO()
    {
        return selectedVendorTO;
    }


    public void setSelectedVendorTO(SysBenefitVendorTO selectedVendorTO)
    {
        this.selectedVendorTO = selectedVendorTO;
    }


    public Boolean getUpdateOfferTemplateFlag()
    {
        return updateOfferTemplateFlag;
    }


    public void setUpdateOfferTemplateFlag(Boolean updateOfferTemplateFlag)
    {
        this.updateOfferTemplateFlag = updateOfferTemplateFlag;
    }


    public Boolean getAddOfferTemplateFlag()
    {
        return addOfferTemplateFlag;
    }


    public void setAddOfferTemplateFlag(Boolean addOfferTemplateFlag)
    {
        this.addOfferTemplateFlag = addOfferTemplateFlag;
    }


    public SysBenefitsVendorController getBenefitsVendorController()
    {
        return benefitsVendorController;
    }


    public void setBenefitsVendorController(SysBenefitsVendorController benefitsVendorController)
    {
        this.benefitsVendorController = benefitsVendorController;
    }


    public List<SysPeakRecurringTypeTO> getSysPeakRecurringTypeTOList()
    {
        return sysPeakRecurringTypeTOList;
    }


    public void setSysPeakRecurringTypeTOList(List<SysPeakRecurringTypeTO> sysPeakRecurringTypeTOList)
    {
        this.sysPeakRecurringTypeTOList = sysPeakRecurringTypeTOList;
    }


    public boolean isOfferingPeakDetailActive()
    {
        return offeringPeakDetailActive;
    }


    public void setOfferingPeakDetailActive(boolean offeringPeakDetailActive)
    {
        this.offeringPeakDetailActive = offeringPeakDetailActive;
    }


    public boolean isRenderPeakDetails()
    {
        return renderPeakDetails;
    }


    public void setRenderPeakDetails(boolean renderPeakDetails)
    {
        this.renderPeakDetails = renderPeakDetails;
    }


    public PeakRecurringDetailTO getPeakRecurringDetailTO()
    {
        return peakRecurringDetailTO;
    }


    public void setPeakRecurringDetailTO(PeakRecurringDetailTO peakRecurringDetailTO)
    {
        this.peakRecurringDetailTO = peakRecurringDetailTO;
    }

    public OfferingCodeTO getOfferingCodeTO()
    {
        return offeringCodeTO;
    }

    public void setOfferingCodeTO(OfferingCodeTO offeringCodeTO)
    {
        this.offeringCodeTO = offeringCodeTO;
    }

    public List<OfferingCodeTO> getOfferingCodeTOList()
    {
        return offeringCodeTOList;
    }

    public void setOfferingCodeTOList(List<OfferingCodeTO> offeringCodeTOList)
    {
        this.offeringCodeTOList = offeringCodeTOList;
    }

    public List<ContentTypeTO> getOfferingAccessTypeTOList()
    {
        return offeringAccessTypeTOList;
    }

    public void setOfferingAccessTypeTOList(List<ContentTypeTO> offeringAccessTypeTOList)
    {
        this.offeringAccessTypeTOList = offeringAccessTypeTOList;
    }

    public List<String> getAccessTypeNameList()
    {
        return accessTypeNameList;
    }

    public List<OfferingCodeTO> getFiltedredOfferingCodeTOlist()
    {
        return filtedredOfferingCodeTOlist;
    }

    public void setAccessTypeNameList(List<String> accessTypeNameList)
    {
        this.accessTypeNameList = accessTypeNameList;
    }

    public void setFiltedredOfferingCodeTOlist(List<OfferingCodeTO> filtedredOfferingCodeTOlist)
    {
        this.filtedredOfferingCodeTOlist = filtedredOfferingCodeTOlist;
    }

    public VendorOfferingTemplateTO getVendorOfferingTemplateTO()
    {
        return vendorOfferingTemplateTO;
    }


    public void setRenderVendorOffering(boolean renderVendorOffering)
    {
        this.renderVendorOffering = renderVendorOffering;
    }

    public void setVendorOfferingTemplateTO(VendorOfferingTemplateTO vendorOfferingTemplateTO)
    {
        this.vendorOfferingTemplateTO = vendorOfferingTemplateTO;
    }

    public boolean isRenderVendorOffering()
    {
        return renderVendorOffering;
    }

    /**
     * upload offerCodes Start
     * 
     */

    private Integer vendorOfferingId;

    private List<OfferingCodeTO> listToFromExcel;

    private List<OfferingCodeTO> masterOfferDataList;

    private OfferingCodeTO copyMasterFormDataList;

    private String uploadedFileName;

    private UploadedFile uploadedFile;

    private List<String> excelHeaderList;

    private List<String> summaryList;

    private boolean success;

    public Integer getVendorOfferingId()
    {
        return vendorOfferingId;
    }

    public void setVendorOfferingId(Integer vendorOfferingId)
    {
        this.vendorOfferingId = vendorOfferingId;
    }

    public List<OfferingCodeTO> getListToFromExcel()
    {
        return listToFromExcel;
    }

    public void setListToFromExcel(List<OfferingCodeTO> listToFromExcel)
    {
        this.listToFromExcel = listToFromExcel;
    }

    public List<OfferingCodeTO> getMasterOfferDataList()
    {
        return masterOfferDataList;
    }

    public void setMasterOfferDataList(List<OfferingCodeTO> masterOfferDataList)
    {
        this.masterOfferDataList = masterOfferDataList;
    }

    public OfferingCodeTO getCopyMasterFormDataList()
    {
        return copyMasterFormDataList;
    }

    public void setCopyMasterFormDataList(OfferingCodeTO copyMasterFormDataList)
    {
        this.copyMasterFormDataList = copyMasterFormDataList;
    }

    public String getUploadedFileName()
    {
        return uploadedFileName;
    }

    public void setUploadedFileName(String uploadedFileName)
    {
        this.uploadedFileName = uploadedFileName;
    }

    public UploadedFile getUploadedFile()
    {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile)
    {
        this.uploadedFile = uploadedFile;
    }

    public List<String> getExcelHeaderList()
    {
        return excelHeaderList;
    }

    public void setExcelHeaderList(List<String> excelHeaderList)
    {
        this.excelHeaderList = excelHeaderList;
    }

    public List<String> getSummaryList()
    {
        return summaryList;
    }

    public void setSummaryList(List<String> summaryList)
    {
        this.summaryList = summaryList;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

	public List<ContentTypeTO> getMaximumUsageDurationTypeTOList() {
		return maximumUsageDurationTypeTOList;
	}

	public void setMaximumUsageDurationTypeTOList(List<ContentTypeTO> maximumUsageDurationTypeTOList) {
		this.maximumUsageDurationTypeTOList = maximumUsageDurationTypeTOList;
	}


    /**
     * upload offerCodes End
     * 
     */

}