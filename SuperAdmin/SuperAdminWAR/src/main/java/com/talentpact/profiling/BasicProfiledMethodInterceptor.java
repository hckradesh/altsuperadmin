package com.talentpact.profiling;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;



/**
 * An interceptor for profiling methods that have the
 * <code>@Profiled</code> annotation.
 */
@Profiled
@Interceptor
public class BasicProfiledMethodInterceptor implements Serializable {
 
	private static final Logger LOG = Logger.getLogger(BasicProfiledMethodInterceptor.class.getName());
    /**
     * starts a stopwatch before a given method execution and stops it after the
     * method is proceeded. Then logs the elapsed time.
     *
     * @param context the invocation context.
     * @return the object produced by the method execution.
     * @throws Exception if an error occurs.
     */
    @AroundInvoke 
    public Object profile(InvocationContext context) throws Exception {
 
        // starts the stopwatch
        final long startTime = System.currentTimeMillis();
 
        // executes the method that is profiled
        Object o = context.proceed();
 
        // stops the stopwatch and computes elapsed time
        final long elapsedTime = System.currentTimeMillis() - startTime;
        Profiled example = getClass().getAnnotation(Profiled.class); //if you are inside the interceptor you can just use getClass(), but you have to get the class from somewhere
        int maxMs = example.ms();
 
        // logs method and time
        final String methodSignature = context.getMethod().toString();
        LOG.info("-----------AltAdmin-------  ProfiledMethod : #" +  methodSignature + "# in #" + elapsedTime + "#  ms -----------------");
        if(elapsedTime > maxMs){
        	LOG.severe("---------AltAdmin---------  OptimizeClass method  #" +  methodSignature + "# -----------------");
        }
 
        return o;
    }
}