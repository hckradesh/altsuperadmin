/**
 * 
 */
package com.talentpact.remote.tpOrganization;

import java.util.List;

import javax.ejb.Local;

import com.alt.security.transport.output.UiFormTO;
import com.talentpact.business.application.transport.input.HrAppTO;
import com.talentpact.business.application.transport.input.SysConstantAndSysConstantCategoryTO;
import com.talentpact.business.application.transport.output.CheckDuplicateValueTO;
import com.talentpact.business.application.transport.output.SysBundleResponseTO;
import com.talentpact.business.application.transport.output.SysContentTypeTO;
import com.talentpact.business.application.transport.output.TpAppResponseTO;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.tpOrganization.exception.OrganizationException;
import com.talentpact.business.tpOrganization.transport.output.AddressResponseTO;
import com.talentpact.business.tpOrganization.transport.output.FinancialYearResponseTO;
import com.talentpact.business.tpOrganization.transport.output.SectorCostResponseTO;
import com.talentpact.remote.common.IRemote;
import com.talentpact.ui.sysSubModule.common.transport.output.SysSubModuleResponseTO;

/**
 * @author radhamadhab.dalai
 *
 */
@Local
public interface INewOrganizationFacadeRemote extends IRemote {
	public ServiceResponse getAllTenants(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getAllApps(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getCompleteAppTree(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getCompleteMergedAppTreeForOrg(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse saveOrganization(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getTenant(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getAllOrganizations(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getPortalTypebyApp(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getAllModules(ServiceRequest serviceRequest) throws OrganizationException;

	public ServiceResponse getOrgMoreInfoListByAppIdOrgIdTenantID(ServiceRequest serviceRequest)
			throws OrganizationException;

	public List<SysBundleResponseTO> getSysBundle() throws OrganizationException;

	public List<Integer> getOrgBundle(ServiceRequest serviceRequest) throws OrganizationException;

	public List<AddressResponseTO> getCountryDropDown() throws OrganizationException;

	public List<AddressResponseTO> getStateDropDown(String country) throws OrganizationException;

	public List<AddressResponseTO> getDistrictDropDown(String state) throws OrganizationException;

	public List<AddressResponseTO> getCityDropDown(String dist) throws OrganizationException;

	public List<AddressResponseTO> getZipCodeDropDown(Integer cityID) throws OrganizationException;

	public List<SectorCostResponseTO> getSectorDropDown() throws OrganizationException;

	public List<FinancialYearResponseTO> getFinancialYearList() throws OrganizationException;

	public List<FinancialYearResponseTO> getGovtFinancialYearList() throws OrganizationException;

	public void saveOrgWebPayStatus(String orgCode, boolean status) throws OrganizationException;

	public List<SysSubModuleResponseTO> getModuleListDropDown() throws OrganizationException;

	public List<SysContentTypeTO> getSysContentTypeDropDown() throws OrganizationException;

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantCategoryDropDown() throws OrganizationException;

	public List<SysConstantAndSysConstantCategoryTO> getSysConstantDropDown() throws OrganizationException;

	public List<UiFormTO> getLandingPageList(Long long1) throws OrganizationException;

	public void saveSelectedPartnerApp(List<String> selectedPartnerAppList, Long orgId, Long tenantId);

	public void saveSelectedCustomApp(List<String> selectedCustomAppList, Long orgId, Long tenantId);
	
	public void saveSelectedApps(List<HrAppTO> createFinalAppList);

	public List<HrAppTO> getSelectedPartnerApps(Long orgId, Long tenantID);

	public List<HrAppTO> getSelectedCustomApps(Long orgId, Long tenantID);

	
}
