package com.talentpact.remote.authorization;

import javax.ejb.Local;

import com.talentpact.business.authorization.exception.AuthorizationException;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.common.IRemote;

/**
 * @author radhamadhab.dalai
 *
 */
@Local
public interface IAuthorizationRemote extends IRemote
{

    public ServiceResponse authorizeUser(ServiceRequest serviceRequest)
        throws AuthorizationException;

}
