package com.talentpact.remote.sysEntity;

import java.util.List;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysEntity.Exception.SysEntityException;

@Local
public interface ISysEntityRemote {
	public ServiceResponse updateModules(ServiceRequest serviceRequest)
	        throws SysEntityException;

	    public ServiceResponse saveOrUpdateSysEntity(ServiceRequest serviceRequest)
	        throws SysEntityException;

	    public ServiceResponse getAllSysEntityList(ServiceRequest serviceRequest)
	        throws SysEntityException;
	    
		public ServiceResponse getAllEntities() throws Exception;
		
		public List<String> getAllTablesFromInformationSchema();
		
		public List<Object[]> getColumnNamesAndDataTypeOfAllColumnsOfATable(String tableName) throws SysEntityException;
		
		public List<String> getTablesPrimaryKeyColumnNames(String tableName) throws SysEntityException;
		
		public ServiceResponse updatePrimaryKeyColumnDetailsInSysEntity(ServiceRequest serviceRequest) throws SysEntityException;
		
}
