package com.talentpact.remote.checkList;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.remote.common.IRemote;

@Local
public interface ICheckListFacadeRemote extends IRemote
{
    public ServiceResponse getAllCheckListByOrganizationId(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllSysRoleType(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllSysProductAnnouncement(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllSysMenu(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse updateAltCheckList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse copySysRoleList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllSysContentType(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse copySysMenuList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getUserByTenant(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllUIForm(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse copyUIFormList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse setUserListByOrganization(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getOrganizationModuleData(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse getAllOfferingTemplateList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse copyAllOfferingTemplateList(ServiceRequest serviceRequest)
        throws Exception;

    public ServiceResponse copyHrProductAnnouncement(ServiceRequest serviceRequest)
        throws Exception;
    
    public ServiceResponse getAllDashboardChartList(ServiceRequest serviceRequest)
            throws Exception;

        public ServiceResponse copyAllDashboardChartList(ServiceRequest serviceRequest)
            throws Exception;


}
