/**
 * 
 */
package com.talentpact.remote.newtenant;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.business.tenants.exception.TenantException;
import com.talentpact.remote.common.IRemote;

/**
 * @author radhamadhab.dalai
 *
 */

@Local
public interface ICreateNewTenantRemote extends IRemote
{

    public ServiceResponse updateModules(ServiceRequest serviceRequest)
        throws TenantException;

    public ServiceResponse saveOrUpdateTenant(ServiceRequest serviceRequest)
        throws TenantException;

    public ServiceResponse getAllTenants(ServiceRequest serviceRequest)
        throws TenantException;

}
