package com.talentpact.remote.sysModule;

import javax.ejb.Local;
import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysModule.exception.SysModuleException;

/**
 * 
 * @author raghvendra.mishra
 *
 */
@Local
public interface ISysModuleRemote {
	/**
	 * 
	 * @param serviceRequest
	 * @return
	 * @throws SysModuleException
	 * @author raghvendra.mishra
	 */
	public ServiceResponse getAllSysModuleList(ServiceRequest serviceRequest) throws SysModuleException;
}
