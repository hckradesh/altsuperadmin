package com.talentpact.remote.sysSubModule;

import javax.ejb.Local;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysSubModule.exception.SysSubModuleException;

    /**
     * 
     * @author raghvendra.mishra
     *
     */
    @Local
    public interface ISysSubModuleRemote {
        /**
         * 
         * @param serviceRequest
         * @return
         * @throws SysSubModuleException
         * @author raghvendra.mishra
         */
        public ServiceResponse getAllSysSubModuleList(ServiceRequest serviceRequest) throws SysSubModuleException;
    }
