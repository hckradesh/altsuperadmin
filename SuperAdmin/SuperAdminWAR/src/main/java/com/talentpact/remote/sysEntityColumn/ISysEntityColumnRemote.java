package com.talentpact.remote.sysEntityColumn;

import com.talentpact.business.common.service.input.ServiceRequest;
import com.talentpact.business.common.service.output.ServiceResponse;
import com.talentpact.ui.sysEntityColumn.exception.SysEntityColumnException;

public interface ISysEntityColumnRemote {

	    public ServiceResponse saveOrUpdateSysEntityColumn(ServiceRequest serviceRequest)
	        throws SysEntityColumnException;

	    public ServiceResponse getAllSysEntityColumnListBySysEntityID(ServiceRequest serviceRequest)
	        throws SysEntityColumnException;
}
