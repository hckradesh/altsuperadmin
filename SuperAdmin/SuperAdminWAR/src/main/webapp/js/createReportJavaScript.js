	function changeTab(tabViewWidget,index) {
		tabViewWidget.select(index);
		tabViewWidget.enable(index);
	}
	
	function setState(tabViewWidget,state,currentTab) {
		index=0;
		while(index<=state){
			tabViewWidget.enable(index);
			index++;
		}
		while(index<=3){
			tabViewWidget.disable(index);	
			index++;
		}
			
		tabViewWidget.select(currentTab);
	}
	
	function stepIDupdate() {
			$('.step-block ul li').each(function(ind) {
			$(this).attr('id', 'step' +ind);
		});	
	}
	
	function dragColumnToSelectedColumnDataTable() {
			
			$('#reportForm\\:tabView1\\:selectedColumnDataTable').find("th").droppable({
				
				drop: function(event, ui) {
					droppedColumnId = $(this).attr('id');
					draggedColumnId = ui.draggable.attr('id');
					
					selectedColumnReorder([
					     {name: 'droppedColumnId', value: droppedColumnId},
                         {name: 'draggedColumnId', value: draggedColumnId}
					]);
				}
			});
			
			$('#reportForm\\:tabView1\\:selectedFilterDataTable').find("th").droppable({
				
				drop: function(event, ui) {
					droppedColumnId = $(this).attr('id');
					draggedColumnId = ui.draggable.attr('id');
					
					selectedFilterReorder([
					     {name: 'droppedColumnId', value: droppedColumnId},
                         {name: 'draggedColumnId', value: draggedColumnId}
					]);
				}
			});
			
	        $('.dragpoint .ui-treenode-label').draggable({
	           helper: 'clone',
	           scope: 'columnToSelectedColumnDataTable',
	           zIndex: ++PrimeFaces.zindex
	        });
	 
	        $('.ui-datatable .droppointheader').droppable({
	           activeClass: 'ui-state-active',
	           hoverClass: 'ui-state-highlight',
	           tolerance: 'pointer',
	           scope: 'columnToSelectedColumnDataTable',
	           drop: function(event, ui) {
	               var property = ui.draggable.find('.ui-treenode-label').text(),
	               droppedColumnId = $(this).parents('th:first').attr('id'),
	               dropPos = $(this).hasClass('dropleft') ? 0 : 1;
					   
	               columnToSelectedColumnDataTable([
	                    {name: 'property', value:  property},
	                    {name: 'droppedColumnId', value: droppedColumnId},
	                    {name: 'dropPos', value: dropPos}
	               ]);
	               
				   $( ".droppoint" ).droppable( "disable" );
				   $('#reportForm\\:tabView1\\:buttonForOpeningColumnDialog').click();
	           }
	        });
			
			$('.droppoint').droppable({
		           activeClass: 'ui-state-active',
		           hoverClass: 'ui-state-highlight',
		           tolerance: 'pointer',
		           scope: 'columnToSelectedColumnDataTable',
		           drop: function(event, ui) {
		               var property = ui.draggable.find('.ui-treenode-label').text();
		               
		               columnToSelectedColumnDataTable([
		                 {name: 'property', value:  property}
		               ]);
		               
		               $('#reportForm\\:tabView1\\:buttonForOpeningColumnDialog').click();
		           }
		        });
	 }
	 
	$(document).ready(function(e) {	
	   	$(".stopDrag").on("dragdrop",function( event ) {
    		event.stopPropagation();
    	    event.preventDefault();
    	});
	    $(function() {
	    	dragColumnToSelectedColumnDataTable();
			stepIDupdate();
	    });
	   	
	});